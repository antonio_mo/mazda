﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Error.aspx.vb" Inherits="CotizadorContado_Conauto._Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Enlace roto</title>
     <style type="text/css">
            
            .Texto {
                font-size: 16pt;
                color: black;
                font-family:Tahoma;
                text-align:center;
                font-style: normal;
                font-weight:bold;
            }
             .Texto1 {
                font-size: 14pt;
                color: black;
                font-family: Tahoma;
                text-align:center;
                font-style: normal;
                font-weight:bold;
            }
     </style>
</head>
<body>
    <div style="width:100%;" align="center">        
        <table style="width:100%;" align="center">
            <tr>
				<td align="left"><IMG src="../Error/Imagenes/Grupo%20Conauto.jpg">
				</td>
				<td align="right"><IMG src="../Error/Imagenes/logo.jpg">
				</td>
			</tr>
            <tr align="center">
                <td colspan="2" style="height:50px">
                 &nbsp;
                </td>
            </tr>       
            <tr align="center">
                <td colspan="2">
                    <label runat="server" class="Texto">Por el momento no es posible completar la solicitud.</label>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <label runat="server" class="Texto1">Intente más tarde...</label>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>