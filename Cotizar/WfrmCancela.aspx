<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCancela.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCancela" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table2" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD colSpan="4">
						<asp:panel id="pnl_deeler" runat="server" Width="100%">
							<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="obligatorio" width="25%">Seleccione Distribuidor :</TD>
									<TD width="50%" colSpan="2">
										<asp:DropDownList id="cbo_deeler" runat="server" Width="100%" CssClass="combos_small"></asp:DropDownList></TD>
									<TD width="25%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="Header_Gris" width="25%">Busqueda</TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="15%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" CssClass="combos_small" Width="100%"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="80%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" CssClass="combos_small" Width="488px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="3">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="4">Marca</asp:ListItem>
										<asp:ListItem Value="5">Modelo</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
									</asp:checkboxlist>
									<asp:checkboxlist id="chkLCriterioM" runat="server" CssClass="combos_small" Width="164px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="2">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Text="Consultar"></asp:button></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="25%" colSpan="4">
						<asp:datagrid id="grd_cancelacion" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="1"
							AllowSorting="True" CssClass="datagrid">
							<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
							<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sel.">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="cbk_grd" runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Poliza" HeaderText="P&#243;liza"></asp:BoundColumn>
								<asp:BoundColumn DataField="num_contrato" HeaderText="No. Contrato">
									<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Serie" HeaderText="Serie"></asp:BoundColumn>
								<asp:BoundColumn DataField="Marca" HeaderText="Marca"></asp:BoundColumn>
								<asp:BoundColumn DataField="Modelo" ReadOnly="True" HeaderText="Modelo">
									<ItemStyle Wrap="False" Width="12%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="anio" ReadOnly="True" HeaderText="A&#241;o">
									<ItemStyle Wrap="False" Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="descripcion" ReadOnly="True" HeaderText="Descripci&#243;n">
									<ItemStyle Wrap="False" Width="18%"></ItemStyle>
								</asp:BoundColumn>
								<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
									HeaderText="Del." CommandName="Delete"></asp:ButtonColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Motivo :</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<asp:DropDownList id="cbo_motivo" runat="server" Width="100%" CssClass="combos_small"></asp:DropDownList></TD>
					<TD align="right">
						<asp:Label id="l1" runat="server" Width="100%" CssClass="negrita">P�liza</asp:Label></TD>
					<TD>
						<asp:Label id="lbl_poliza" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:Button id="cmd_agregar" runat="server" CssClass="boton" Text="Guardar"></asp:Button></TD>
					<TD></TD>
					<TD>
						<asp:Button id="Button1" runat="server" CssClass="boton" Text="Cancelar"></asp:Button></TD>
					<TD>
						<asp:TextBox id="txt_poliza" runat="server" Width="16px" Visible="False" Height="16px"></asp:TextBox>
						<asp:TextBox id="txt_serie" runat="server" Width="16px" Visible="False" Height="16px"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<TABLE id="Table4" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="2%" height="20" class="negrita_marco_color" vAlign="middle" align="center">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Cancelaci�n de 
							P�liza</FONT></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 103; LEFT: 352px; POSITION: absolute; TOP: 776px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
