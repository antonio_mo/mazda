<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmIPolizaZurich.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmIPolizaZurich"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 100; LEFT: 432px; POSITION: absolute; TOP: 1536px" runat="server"></cc1:msgbox>
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%">
						<asp:image id="imgmazda" runat="server" ImageUrl="..\Imagenes\logos\mazda.gif" ImageAlign="Baseline"></asp:image></TD>
					<TD width="50%" align="center">
						<asp:image id="Image3" runat="server" ImageAlign="Baseline" ImageUrl="..\Imagenes\logos\logo.jpg"></asp:image></TD>
					<TD align="right" width="25%"><asp:image id="Logo_Sistema" runat="server" ImageUrl="..\Imagenes\logos\zurich.gif" ImageAlign="Baseline"></asp:image></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="Header_tabla_centrado">P�LIZA DE SEGURO</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="small" width="25%">Identificaci�n</TD>
								<TD class="small" align="center" width="50%">Vigencia</TD>
								<TD class="small" width="25%"></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" colSpan="4">
									<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="Header_tabla_centrado" width="20%">P�liza N�mero</TD>
											<TD class="Header_tabla_centrado" align="center" width="10%">Item</TD>
											<TD class="Header_tabla_centrado" align="center" width="35%">Desde</TD>
											<TD class="Header_tabla_centrado" align="center" width="35%">Hasta</TD>
										</TR>
										<TR>
											<TD><asp:label id="lbl_poliza" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_item" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_desde" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_hasta" runat="server" Width="100%" CssClass="small"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro">Asegurar a</TD>
								<TD class="negrita" align="center">a las 12 horas (medio dia) del lugar de 
									expedici�n
								</TD>
								<TD class="obligatorio_negro"></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" colSpan="3" height="5">
									<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD vAlign="top" width="50%">
												<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro">Nombre :</TD>
														<TD colSpan="3"><asp:label id="lbl_nombre" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Domicilio :</TD>
														<TD colSpan="3"><asp:label id="lbl_dir" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">RFC :</TD>
														<TD><asp:label id="lbl_rfc" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">Tel. :&nbsp;
														</TD>
														<TD><asp:label id="lbl_tel" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Poblaci�n :</TD>
														<TD><asp:label id="lbl_poblacion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">C.P. :&nbsp;
														</TD>
														<TD><asp:label id="lbl_cp" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Beneficiario Preferente :</TD>
														<TD vAlign="top" colSpan="3"><asp:label id="lbl_bene" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="1%"></TD>
											<TD width="49%">
												<TABLE id="Table6" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro" width="40%">Fecha de emisi�n :</TD>
														<TD width="60%"><asp:label id="lbl_fecha" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Plan :</TD>
														<TD><asp:label id="lbl_plan" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Moneda :</TD>
														<TD><asp:label id="lbl2" runat="server" Width="100%" CssClass="small">Pesos</asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Agente :</TD>
														<TD><asp:label id="lbl3" runat="server" Width="100%" CssClass="small">1032 AON RISK SERVICES</asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Bid :</TD>
														<TD><asp:label id="lbl_deeler" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="small" colSpan="2">Por convenio expreso de ambas partes la aseguradora y 
															el asegurado, no proceder� a la aplicaci�n del doble deducible cuando el 
															conductor del vehiculo asegurado sea menor a 21 a�os, seg�n lo estipulado en 
															las condiciones generales dentro de de la secci�n de da�os materiales, los 
															dem�s t�rminos y condiciones quedan sin modificaci�n alguna</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="small" colSpan="3">
												<P>Art. 25 de la ley sobre el Contrato del Seguro. Si el contenido de la p�liza o 
													sus modificaciones no concordaren con la oferta, el asegurado podr� pedir la 
													rectificaci�n correspondiente dentro de los treinta d�as que sugan al d�a en 
													que reciba la p�liza.
													<BR>
													Transcurrido este plazo, se considerar�n aceptadas las estipulaciones de la 
													p�liza o de sus modificaciones.<BR>
													Zurich Compa�ia de Seguros, S.A. (que en lo sucesivo se demoniara "La 
													compa�is"), de acuerdo con las condiciones generales y particulares de cada una 
													de las secciones contratadas, expide este documento a favor de persona arriba 
													citada (quien en lo sucesivo se denominara "Asegurado"). Las sessiones y 
													coberturas contratadas son las que aparecen a continuaci�n.</P>
											</TD>
										</TR>
										<TR>
											<TD class="Linea_Arriba" vAlign="top">
												<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
													<TR>
														<TD class="Interiro_tabla_centro" colSpan="4">Materia asegurado</TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Aotim�vil :</TD>
														<TD colSpan="3"><asp:label id="lbl_auto" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Modelo :</TD>
														<TD><asp:label id="lbl_modelo" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro">Placas :</TD>
														<TD><asp:label id="lbl_placa" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Serie :</TD>
														<TD><asp:label id="lbl_serie" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro">Motor :</TD>
														<TD><asp:label id="lbl_motor" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Repuve&nbsp;:</TD>
														<TD><asp:label id="lbl_ranave" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro">Uso :</TD>
														<TD><asp:label id="lbl_uso" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" vAlign="top">Tipo de Carga :</TD>
														<TD vAlign="top"><asp:label id="lbl4" runat="server" Width="100%" CssClass="small">No Peligrosa</asp:label></TD>
														<TD class="obligatorio_negro" vAlign="top">Capacidad :</TD>
														<TD vAlign="top"><asp:label id="lbl_capacidad" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro"></TD>
														<TD></TD>
														<TD></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" colSpan="4"><asp:table id="tb_cobertura" runat="server" Width="100%" CssClass="combos_small" HorizontalAlign="Right"></asp:table></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro"></TD>
														<TD></TD>
														<TD></TD>
														<TD></TD>
													</TR>
												</TABLE>
											</TD>
											<TD class="Linea_Arriba" vAlign="top" colSpan="2">
												<TABLE id="Table9" height="100%" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD width="40%"></TD>
														<TD width="60%"></TD>
													</TR>
													<TR>
														<TD class="Interiro_tabla_centro" colSpan="3">Plan de pago</TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Aceptante :</TD>
														<TD><asp:label id="lbl_cliente" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Domicilio :</TD>
														<TD><asp:label id="lbl_dom" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">forma de pago :</TD>
														<TD><asp:label id="lbl5" runat="server" Width="100%" CssClass="small">Contado</asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Precio real :</TD>
														<TD><asp:label id="lbl_preal" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Precio promoci�n :</TD>
														<TD>
															<asp:label id="lbl_ppromocion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">No. contrato</TD>
														<TD>
															<asp:label id="lbl_contrato" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Condici�n de vehiculo :</TD>
														<TD><asp:label id="lbl_condicion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">
															<asp:label id="lbl_Recargo" runat="server" CssClass="small" Width="100%">Recargos :</asp:label></TD>
														<TD>
															<asp:label id="lbl_PagoRecargo" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Linea_Arriba" vAlign="top" colSpan="3"></TD>
										</TR>
										<TR>
											<TD class="header" vAlign="top" colSpan="3">Coberturas</TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="3"><asp:table id="tb_cob" runat="server" Width="100%" CssClass="combos_small" HorizontalAlign="Right"></asp:table></TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="3"><asp:label id="lbl_ley" runat="server" Width="100%" CssClass="small"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table10" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="linea_arriba" align="center" width="25%" colSpan="4"></TD>
										</TR>
										<TR>
											<TD class="obligatorio_negro" align="center" width="25%">Derecho de emisi�n</TD>
											<TD class="obligatorio_negro" align="center" width="25%">I.V.A.</TD>
											<TD class="obligatorio_negro" align="center" width="25%">Prima total</TD>
											<TD class="obligatorio_negro" align="center" width="25%"></TD>
										</TR>
										<TR>
											<TD align="center"><asp:label id="lbl_derecho" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_iva" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_total" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD align="center"></TD>
										</TR>
										<TR>
											<TD class="small" colSpan="3">
												<P>En caso de una&nbsp;p�rdida total la base del calculo de indemnizaci�n ser� el 
													Auto Nuevo, Valor Factura o Valor Comercial seg�n paquete adquirido, durante 
													los primeros 12 meses, a partir del mes treceavo aplicar� el valor comercial. 
													Los deducibles correspondientes se aplicar�n sobre (1) para los primeros 12 
													meses y a partir del mes treceavo sobre el valor Comercial al momento del 
													siniestro.
													<BR>
													ESTA POLIZA AMPARA UNICAMENTE LA VIGENCIA ARRIBA MENCIONADA POR LO QUE SI SU 
													FINANCIAMIENTO ES MAYOR A ESTA, LE RECOMENDAMOS QUE DEBE DE REALIZAR LA 
													RENOVACI�N CORRESPONDIENTE CON EL PROGRAMA DE FORD INSURE.<BR>
													Atenci�n a clientes AON RISK SERVICES D.F. 5387-6086, 01800-7176942
													<BR>
													Atenci�n a siniestros Tel.: 5284 1166, lada (sin costo): 01 800 800 3673
												</P>
											</TD>
											<TD vAlign="top" align="center">
												<TABLE class="linea_abajo" id="Table11" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD vAlign="top" align="center"><asp:image id="Image1" runat="server" ImageUrl="..\Imagenes\logos\Firma.gif" ImageAlign="Baseline"></asp:image></TD>
													</TR>
													<TR>
														<TD class="negrita" align="center">Antonio Borrajo Venegas</TD>
													</TR>
													<TR>
														<TD class="negrita" align="center">Dir. Negocios Esp. y Masivos</TD>
													</TR>
													<TR>
														<TD class="negrita" align="center">Zurich de m�xico</TD>
													</TR>
												</TABLE>
												<asp:label id="lbl_leyenda" runat="server" Width="100%" CssClass="small"></asp:label></TD>
										</TR>
										<TR>
											<TD class="linea_arriba"></TD>
											<TD class="linea_arriba"></TD>
											<TD class="linea_arriba"></TD>
											<TD class="linea_arriba" align="center"></TD>
										</TR>
										<TR>
											<TD class="small" align="center" colSpan="4">El asegurado bajo protesta de decir la 
												verdad manifesto que los recursos con los que se pagar� la prima de seguro son 
												de procedencia licita</TD>
										</TR>
										<TR>
											<TD class="small" colSpan="4">Zurich compa�ia de Seguros S.A. Boulevard manuel 
												Avila Camacho No. 126, Col. Lomas de Chapultepec, C.P. 11000, M�xico, D.F. Tel: 
												52 84 10 00 Fax: 52 84 10 20
											</TD>
										</TR>
										<TR>
											<TD class="negrita" align="center" colSpan="4">Un programa de seguros en alianza 
												con Zurich C�a. de seguros S.A.</TD>
										</TR>
										<TR>
											<TD></TD>
											<TD></TD>
											<TD></TD>
											<TD></TD>
										</TR>
									</TABLE>
									<asp:label id="lbl_version" runat="server" Width="100%"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2">
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="60%"></TD>
								<TD align="right" width="20%"><asp:imagebutton id="cmd_regresar" runat="server" ImageUrl="..\Imagenes\Botones\regresar.gif"></asp:imagebutton></TD>
								<TD align="right" width="20%"><asp:imagebutton id="cmd_imprimir" runat="server" ImageUrl="..\Imagenes\Botones\imprimir.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
