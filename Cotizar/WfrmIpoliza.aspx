<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmIpoliza.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmIpoliza" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="50%"></TD>
					<TD align="right" width="25%"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="Header_tabla_centrado">
						P�LIZA DE SEGURO</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="25%"></TD>
								<TD width="50%"></TD>
								<TD width="50%"></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" width="25%" colSpan="4">
									<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="obligatorio_negro" width="20%">P�liza N�mero</TD>
											<TD class="negrita" align="center" width="10%"></TD>
											<TD class="obligatorio_negro" align="center" width="35%">Desde</TD>
											<TD class="obligatorio_negro" align="center" width="35%">Hasta</TD>
										</TR>
										<TR>
											<TD><asp:label id="lbl_poliza" runat="server" CssClass="small" Width="100%"></asp:label></TD>
											<TD align="center" class="negrita"></TD>
											<TD align="center"><asp:label id="lbl_desde" runat="server" CssClass="small" Width="100%"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_hasta" runat="server" CssClass="small" Width="100%"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" colSpan="3" height="5">
									<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD vAlign="top" width="50%">
												<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro" width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Nombre :</TD>
														<TD colSpan="3"><asp:label id="lbl_nombre" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Domicilio :</TD>
														<TD colSpan="3"><asp:label id="lbl_dir" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">RFC :</TD>
														<TD><asp:label id="lbl_rfc" runat="server" CssClass="small" Width="100%"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">Tel. :&nbsp;
														</TD>
														<TD><asp:label id="lbl_tel" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Poblaci�n :</TD>
														<TD><asp:label id="lbl_poblacion" runat="server" CssClass="small" Width="100%"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">C.P. :&nbsp;
														</TD>
														<TD><asp:label id="lbl_cp" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Beneficiario Preferente :</TD>
														<TD vAlign="top" colSpan="3"><asp:label id="lbl_bene" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="1%"></TD>
											<TD width="49%" vAlign="top">
												<TABLE id="Table6" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro" width="40%" align="right">Fecha de emisi�n :</TD>
														<TD width="60%"><asp:label id="lbl_fecha" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" align="right">Paquete&nbsp;:</TD>
														<TD><asp:label id="lbl_plan" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" align="right">Moneda :</TD>
														<TD><asp:label id="lbl2" runat="server" CssClass="small" Width="100%">Pesos</asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" align="right">Vendedor&nbsp;:</TD>
														<TD>
															<asp:label id="lbl_deeler" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="small" colSpan="2"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Linea_Arriba" vAlign="top">
												<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
													<TR>
														<TD class="Interiro_tabla_centro" colSpan="4">Vehiculo asegurado</TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Autom�vil :</TD>
														<TD colSpan="3"><asp:label id="lbl_auto" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" style="HEIGHT: 17px">Modelo :</TD>
														<TD style="HEIGHT: 17px"><asp:label id="lbl_modelo" runat="server" CssClass="small" Width="100%"></asp:label></TD>
														<TD class="obligatorio_negro" style="HEIGHT: 17px">Placas :</TD>
														<TD><asp:label id="lbl_placa" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Serie :</TD>
														<TD><asp:label id="lbl_serie" runat="server" CssClass="small" Width="100%"></asp:label></TD>
														<TD class="obligatorio_negro">Motor :</TD>
														<TD><asp:label id="lbl_motor" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Repuve&nbsp;:</TD>
														<TD><asp:label id="lbl_ranave" runat="server" CssClass="small" Width="100%"></asp:label></TD>
														<TD class="obligatorio_negro">Uso :</TD>
														<TD><asp:label id="lbl_uso" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" vAlign="top">Tipo de Carga :</TD>
														<TD vAlign="top"><asp:label id="lbl4" runat="server" CssClass="small" Width="100%">No Peligrosa</asp:label></TD>
														<TD class="obligatorio_negro" vAlign="top">Capacidad :</TD>
														<TD vAlign="top"><asp:label id="lbl_capacidad" runat="server" CssClass="small" Width="100%"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro"></TD>
														<TD></TD>
														<TD></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" colSpan="4"><asp:table id="tb_cobertura" runat="server" CssClass="combos_small" Width="100%" HorizontalAlign="Right"></asp:table></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro"></TD>
														<TD></TD>
														<TD></TD>
														<TD></TD>
													</TR>
												</TABLE>
											</TD>
											<TD class="Linea_Arriba" vAlign="top" colSpan="2">
												<TABLE id="Table9" height="100%" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD width="40%"></TD>
														<TD width="60%"></TD>
													</TR>
													<TR>
														<TD class="Interiro_tabla_centro" colSpan="3">Plan de pago</TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" align="left">forma de pago :</TD>
														<TD><asp:label id="lbl_formapago" runat="server" CssClass="small" Width="100%">Contado</asp:label></TD>
													</TR>
													<TR>
														<TD colSpan="2"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Linea_Arriba" vAlign="top" colSpan="3"></TD>
										</TR>
										<TR>
											<TD class="Interiro_tabla_centro" vAlign="top" colSpan="3">Coberturas</TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="3"><asp:table id="tb_cob" runat="server" CssClass="combos_small" Width="100%" HorizontalAlign="Right"></asp:table></TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="3"><asp:label id="lbl_ley" runat="server" CssClass="small" Width="100%"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table10" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="linea_arriba" align="center" width="25%" colSpan="4"></TD>
										</TR>
										<TR>
											<TD class="obligatorio_negro" align="center" width="25%">Derecho de emisi�n</TD>
											<TD class="obligatorio_negro" align="center" width="25%">I.V.A.</TD>
											<TD class="obligatorio_negro" align="center" width="25%">Prima total</TD>
											<TD class="obligatorio_negro" align="center" width="25%"></TD>
										</TR>
										<TR>
											<TD align="center"><asp:label id="lbl_derecho" runat="server" CssClass="small" Width="100%"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_iva" runat="server" CssClass="small" Width="100%"></asp:label></TD>
											<TD align="center"><asp:label id="lbl_total" runat="server" CssClass="small" Width="100%"></asp:label></TD>
											<TD align="center"></TD>
										</TR>
									</TABLE>
									<asp:label id="lbl_version" runat="server" Width="100%"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2">
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="60%"></TD>
								<TD align="right" width="20%">
									<asp:imagebutton id="cmd_regresar" runat="server" ImageUrl="..\Imagenes\Botones\regresar.gif"></asp:imagebutton></TD>
								<TD align="right" width="20%"><asp:imagebutton id="cmd_imprimir" runat="server" ImageUrl="..\Imagenes\Botones\imprimir.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 102; LEFT: 304px; POSITION: absolute; TOP: 1264px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
