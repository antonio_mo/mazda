<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmImpCoti.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmImpCoti" %>
<%@ Register TagPrefix="uc1" TagName="WfrmUCobertura" Src="WfrmUCobertura.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 101; LEFT: 416px; POSITION: absolute; TOP: 760px" runat="server"></cc1:msgbox>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"><asp:image id="Logo_Aon" runat="server" ImageUrl="..\Imagenes\logos\vacio.gif"></asp:image></TD>
					<TD width="50%"></TD>
					<TD align="right" width="25%"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="Header_tabla_centrado">COTIZACI�N SEGURO</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2"></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="negrita" align="left" width="15%">Distribuidor :</TD>
								<TD width="35%"><asp:label id="lbl_distribuidor" runat="server" Width="100%" CssClass="small"></asp:label></TD>
								<TD class="obligatorio_negro" align="right" width="15%">Raz�n Social :</TD>
								<TD width="35%"><asp:label id="lbl_razon" runat="server" Width="100%" CssClass="small"></asp:label></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" align="left">Apellido Paterno :</TD>
								<TD><asp:label id="lbl_paterno" runat="server" Width="100%" CssClass="small"></asp:label></TD>
								<TD class="obligatorio_negro" align="right">Tel�fono :</TD>
								<TD><asp:label id="lbl_tel" runat="server" Width="100%" CssClass="small"></asp:label></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" style="HEIGHT: 1px" align="left">Apellido Materno</TD>
								<TD style="HEIGHT: 1px"><asp:label id="lbl_materno" runat="server" Width="100%" CssClass="small"></asp:label></TD>
								<TD class="obligatorio_negro" style="HEIGHT: 1px" align="right"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="obligatorio_negro" align="left">Nombre (s) :</TD>
								<TD><asp:label id="lbl_nombre" runat="server" Width="100%" CssClass="small"></asp:label></TD>
								<TD class="obligatorio_negro" align="right"></TD>
								<TD class="negrita"></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD class="Linea_Arriba" colSpan="4">
									<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="obligatorio_negro" align="left" width="15%">Fecha de Imp. :</TD>
											<TD width="20%"><asp:label id="lbl_fecha" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD class="obligatorio_negro" align="right" width="10%">Marca :</TD>
											<TD width="15%"><asp:label id="lbl_marca" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD class="obligatorio_negro" width="10%">
												<P align="right">A�o :</P>
											</TD>
											<TD width="25%"><asp:label id="lbl_anio" runat="server" Width="100%" CssClass="small"></asp:label></TD>
										</TR>
										<TR>
											<TD class="obligatorio_negro" align="left">Plazo&nbsp;P�liza :</TD>
											<TD><asp:label id="lbl_plazo" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD class="obligatorio_negro" align="right">Modelo :</TD>
											<TD><asp:label id="lbl_modelo" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD class="obligatorio_negro" align="right">Descripci�n :</TD>
											<TD><asp:label id="lbl_descripcion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
										</TR>
										<TR>
											<TD class="obligatorio_negro" align="left">Suma Aseg.&nbsp;:</TD>
											<TD><asp:label id="lbl_seguro" runat="server" Width="100%" CssClass="small"></asp:label></TD>
											<TD class="obligatorio_negro"></TD>
											<TD class="negrita"></TD>
											<TD class="obligatorio_negro" align="right"></TD>
											<TD><asp:label id="lbl_catalogo" runat="server" Width="100%" CssClass="small" Visible="False"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Linea_abajo" colSpan="6" height="5"></TD>
										</TR>
										<TR>
											<TD class="Linea_abajo" colSpan="6" height="5"><asp:datagrid id="grd_cobertura" runat="server" Width="100%" CssClass="datagrid" ShowFooter="True"
													AutoGenerateColumns="False" CellPadding="1" AllowSorting="True">
													<FooterStyle Wrap="False" CssClass="Footer_azul"></FooterStyle>
													<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
													<EditItemStyle Wrap="False"></EditItemStyle>
													<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
													<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
													<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
													<Columns>
														<asp:EditCommandColumn Visible="False" ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
															HeaderText="Sel." CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
															HeaderText="Del." CommandName="Delete">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
														</asp:ButtonColumn>
														<asp:BoundColumn Visible="False" DataField="cobertura"></asp:BoundColumn>
														<asp:BoundColumn DataField="descripcion_cob" ReadOnly="True" HeaderText="Cobertura">
															<ItemStyle Wrap="False" Width="18%"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Descripci&#243;n">
															<ItemStyle Wrap="False" Width="20%"></ItemStyle>
															<ItemTemplate>
																<asp:Label id=Label1 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob") %>'>Label</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox id=txt_grddesc runat="server" CssClass="combos_small" Width="100%" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Suma Aseg.">
															<ItemStyle Wrap="False" HorizontalAlign="Right" Width="12%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:Label id=Label2 runat="server" Width="100%" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>Label</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox id=txt_grdsuma runat="server" CssClass="combos_small" Width="70%" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="No. Fact.">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:Label id=Label3 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>Label</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox id=txt_grdfact runat="server" CssClass="combos_small" Width="70%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="cap" ReadOnly="True"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="SUMAASEG_COB"></asp:BoundColumn>
													</Columns>
													<PagerStyle Wrap="False"></PagerStyle>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD class="Linea_abajo" colSpan="6" height="5"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="4"><uc1:wfrmucobertura id="WfrmUCobertura1" runat="server"></uc1:wfrmucobertura></TD>
							</TR>
							<TR>
								<TD colSpan="4"><asp:label id="lbl_ley" runat="server" Width="100%" CssClass="small"></asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" colSpan="2">Cotizaci�n sujeta a cambios sin previo aviso</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio_negro" vAlign="top"><asp:label id="lbl_version" runat="server" Width="100%"></asp:label></TD>
					<TD vAlign="top" align="right" colSpan="2">
						<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="60%"><asp:textbox id="txt_insco" runat="server" Width="16px" Visible="False" Height="16px"></asp:textbox><asp:textbox id="txt_estatus" runat="server" Width="16px" Visible="False" Height="16px"></asp:textbox><asp:textbox id="txt_idpaquete" runat="server" Width="16px" Visible="False" Height="16px"></asp:textbox></TD>
								<TD align="right" width="20%"><asp:imagebutton id="cmd_regresar" runat="server" ImageUrl="..\Imagenes\Botones\regresar.gif"></asp:imagebutton></TD>
								<TD align="right" width="20%"><asp:imagebutton id="cmd_imprimir" runat="server" ImageUrl="..\Imagenes\Botones\imprimir.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
