Imports CN_Negocios

Partial Class WfrmRecotiza
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private ccalculo As New CnCalculo
    Private cprincipal As New CnPrincipal

    Private cuentaJava As Integer = 11
    Private arrTotal(0) As String
    Private arrPol(0) As String
    Private PaqSel As String = ""
    Private arrNombrePol(0) As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                cbo_plazo.Visible = False

                lbl_fechai.Text = Session("FechaInicio")
                txt_fecha.Text = Session("FechaInicio")

                If Not Request.QueryString("valcost") Is Nothing Then
                    txt_precio.Text = Request.QueryString("valcost")
                Else
                    txt_precio.Text = Session("precioEscrito")
                End If

                Select Case Session("contribuyente")
                    Case 0
                        lbl_persona.Text = "F�sica"
                    Case 1
                        lbl_persona.Text = "Act. Empresarial"
                    Case 2
                        lbl_persona.Text = "Moral"
                End Select

                txt_sumcob.Text = 0
                limpia()
                carga_datos_vehiculo()

                If Request.QueryString("cveAseg") Is Nothing Then
                    If Not Request.QueryString("valcost1") Is Nothing Then
                        txt_precio.Text = Request.QueryString("valcost1")
                        txt_precioF.Text = Request.QueryString("valcost1")

                        'forma pago da�os
                        Dim banSeg As Integer
                        If Session("seguro") = 0 Then
                            banSeg = 1
                        Else
                            banSeg = 0
                        End If
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))
                        Seg_financiado(rb_seguro)
                        If Session("seguro") = 2 Then
                            lbl_seguro.Text = "Financiado"
                        Else
                            lbl_seguro.Text = "Contado"
                        End If

                        carga_plazo_financiamiento()
                        carga_plazo()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(CInt(Session("plazo"))))
                        cbo_plazof.SelectedIndex = cbo_plazof.Items.IndexOf(cbo_plazof.Items.FindByValue(CInt(Session("plazoFinanciamiento"))))
                        txt_contrato.Text = Session("contrato")
                    Else
                        tipo_poliza(12)
                        Session("idgrdcontrato") = ccliente.carga_contrato(Session("bid"))
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(0))
                        Seg_financiado(rb_seguro)

                        carga_plazo_financiamiento()
                        carga_plazo()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
                        cbo_plazof.SelectedIndex = cbo_plazof.Items.IndexOf(cbo_plazof.Items.FindByValue(12))
                        Session("plazo") = 12
                        Session("plazoFinanciamiento") = 12

                    End If

                    'nuevo
                    strCadenaS = Session("CadenaCob")
                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    End If
                Else
                    'nuevo
                    If Not IsDate(Request.QueryString("strfecha")) Then
                        MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
                        Exit Sub
                    End If


                    Dim a1 As String = Session("banderafecha")
                    Dim a2 As String = Session("moneda")
                    Dim a3 As String = Request.QueryString("subramo")
                    Dim a4 As String = Session("anio")
                    Dim a5 As String = Session("modelo")
                    Dim a6 As String = Session("tipopol")
                    Dim a7 As String = Session("uso")
                    Dim a8 As String = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    Dim a9 As String = Session("EstatusV")
                    Dim a10 As String = Session("InterestRate")
                    Dim a11 As String = Request.QueryString("strfecha")
                    Dim a12 As String = Session("tiposeguro")
                    Dim a13 As String = Request.QueryString("cveplazo")
                    Dim a14 As String = txt_precio.Text
                    Dim a16 As String = Session("idgrdcontrato")
                    Dim a17 As String = Session("seguro")
                    Dim a18 As String = Session("bid")
                    Dim a19 As String = txt_sumcob.Text
                    Dim a20 As String = Session("vehiculo")
                    Dim a21 As String = Request.QueryString("cveAseg")
                    Dim a22 As String = Request.QueryString("cvepaquete")
                    Dim a23 As String = Session("programa")
                    Dim a24 As String = Session("fincon")
                    Dim a25 As String = Request.QueryString("cvefinancia")
                    Dim a26 As String = Request.QueryString("cveenganche")
                    Dim a27 As String = IIf(Session("programa") = 4 Or Session("programa") = 5, Session("CP"), "")
                    Dim a28 As String = Session("MarcaC")
                    Dim a29 As String = Session("intmarcabid")
                    Dim a30 As String = Request.QueryString("cveplazoFinancia")
                    Dim a31 As String = Request.QueryString("cvedesempleou")
                    Dim a32 As String = Session("iva")
                    Dim a33 As String = Session("version")

                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results

                    Session("DatosGenerales") = ""
                    Dim arregloG(23) As String
                    arregloG(0) = Session("moneda")
                    arregloG(1) = Request.QueryString("subramo")
                    arregloG(2) = Session("anio")
                    arregloG(3) = Session("modelo")
                    arregloG(4) = 1
                    arregloG(5) = Session("seguro")
                    arregloG(6) = Session("uso")
                    arregloG(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arregloG(8) = Session("EstatusV")
                    arregloG(9) = 0
                    arregloG(10) = Session("InterestRate")
                    arregloG(11) = Request.QueryString("strfecha")
                    arregloG(12) = Session("tiposeguro")
                    arregloG(13) = Request.QueryString("cveplazo")
                    arregloG(14) = txt_precio.Text
                    arregloG(15) = Session("idgrdcontrato")
                    arregloG(16) = Session("seguro")
                    arregloG(17) = Session("bid")
                    arregloG(18) = txt_sumcob.Text
                    arregloG(19) = Session("vehiculo")
                    arregloG(20) = Request.QueryString("cveAseg")
                    arregloG(21) = Request.QueryString("cvepaquete")
                    arregloG(22) = Session("programa")
                    arregloG(23) = Session("fincon")
                    Session("DatosGenerales") = arregloG

                    If Not IsDate(Request.QueryString("strfecha")) Then
                        MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
                        Exit Sub
                    End If
                    'manolito
                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("plazoFinanciamiento") = Request.QueryString("cveplazoFinancia")
                    Session("FechaInicio") = Request.QueryString("strfecha")

                    Session("precioEscrito") = txt_precio.Text
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Response.Redirect("..\cotizador\WfrmCliente.aspx?cvevida=" & Request.QueryString("cvevida") & "&cvedesempleo=" & Request.QueryString("cvedesempleo") & "&cvevidau=" & Request.QueryString("cvevidau") & "&cvedesempleou=" & Request.QueryString("cvedesempleou") & "")
                End If

                lbl_plazo.Text = cbo_plazo.SelectedItem.Text & " Meses"
                lbl_plazof.Text = cbo_plazof.SelectedItem.Text & " Meses"
                txt_contrato.Text = Session("contrato")


                If Request.QueryString("valcost1") Is Nothing Then
                    Dim i, j As Integer
                    Dim arr() As String

                    Dim dt As DataTable = ccliente.Carga_coberturas_Cotizacion(Session("idgrdcontrato"), _
                        Session("bid"), Session("programa"), Session("vehiculo"), _
                        Session("EstatusV"), Session("version"))

                    If dt.Rows.Count > 0 Then
                        grd_cobertura.DataSource = dt
                        grd_cobertura.DataBind()
                        grd_cobertura.Visible = True
                    Else
                        grd_cobertura.Visible = False
                    End If
                Else
                    carga_grd()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_plazo_financiamiento()
        Dim i As Integer = 0
        Dim arr As New ArrayList
        Dim max As Integer = 60
        arr.Add("-- Seleccione --")
        For i = 6 To max
            arr.Add(i)
        Next
        cbo_plazof.DataSource = arr
        cbo_plazof.DataBind()
        cbo_plazof.SelectedIndex = cbo_plazof.Items.IndexOf(cbo_plazof.Items.FindByValue(12))
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                'Session("seguro") = 2
                Session("tipopol") = "M"
                'Session("bUnidadfinanciada") = True
            Else
                'Session("seguro") = 0
                Session("tipopol") = "A"
                'Session("bUnidadfinanciada") = False
            End If
        Else
            Session("tipopol") = "A"
            'Session("seguro") = 0
            'Session("bUnidadfinanciada") = False
        End If
    End Sub

    Public Sub desabilita()
        txt_marca.Enabled = False
        txt_tipo.Enabled = False
        txt_subtipo.Enabled = False
        txt_modelo.Enabled = False
        txt_descripcion.Enabled = False
        txt_clasificacion.Enabled = False
    End Sub

    Public Sub limpia()
        txt_marca.Text = ""
        txt_tipo.Text = ""
        txt_subtipo.Text = ""
        txt_modelo.Text = ""
        txt_descripcion.Text = ""
        txt_clasificacion.Text = ""

        txt_grdcobertura.Text = ""
        txt_gdescripcion.Text = ""
        txt_ban.Text = 0
    End Sub

    Public Sub carga_datos_vehiculo()
        Try
            txt_marca.Text = Session("MarcaDesc")
            txt_modelo.Text = Session("anio")
            txt_tipo.Text = Session("modelo")
            txt_descripcion.Text = Session("vehiculoDesc")
            txt_subtipo.Text = Session("catalogo")
            txt_clasificacion.Text = IIf(Session("subramo") = 1, "Autom�vil", "Cami�n")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Public Sub carga_grd()
        Dim dt As DataTable = ccliente.carga_cobertura_bid(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), Session("programa"), Session("vehiculo"))
        If dt.Rows.Count > 0 Then
            grd_cobertura.DataSource = dt
            grd_cobertura.DataBind()
            grd_cobertura.Visible = True
        Else
            grd_cobertura.Visible = False
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub


    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Private Sub grd_cobertura_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub grd_cobertura_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_cobertura.ItemDataBound
        Dim dt As DataTable
        Dim intregion As Integer = 0
        Dim Dbvalor As Double = 0
        If e.Item.ItemType = ListItemType.Header Then
            cuentaJava = 11
            Dim cuenta As Integer = 7
            Dim i As Integer = 0
            Dim hp As HyperLink
            dt = ccliente.carga_cobertura_bid_aseguradora(Session("idgrdcontrato"), Session("bid"))
            If dt.Rows.Count > 0 Then
                ReDim arrTotal(dt.Rows.Count - 1)
                ReDim arrPol(dt.Rows.Count - 1)
                ReDim arrNombrePol(dt.Rows.Count - 1)
                For i = 0 To dt.Rows.Count - 1
                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                    hp = New HyperLink
                    hp.Text = dt.Rows(i)("descripcion_aseguradora")
                    hp.ImageUrl = "..\Imagenes\Logos\Mini_insure.png"
                    hp.CssClass = "negrita"
                    e.Item.Cells(cuenta).Controls.Add(hp)

                    arrPol(i) = dt.Rows(i)("id_aseguradora")
                    arrNombrePol(i) = dt.Rows(i)("descripcion_aseguradora")
                    grd_cobertura.Columns(cuenta).Visible = True
                    cuenta = cuenta + 1

                Next
            End If
        End If

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim recorrepoliza As Integer = 0
            Dim validarecorrido As Integer = 0
            Dim lblC As Label
            Dim hp As HyperLink
            Dim hpImagen As HyperLink
            Dim hpH As HyperLink
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim kz As Integer = 0
            Dim cuenta As Integer = 7
            Dim banderaVida As Integer = 0
            cuentaJava = cuentaJava + 1
            If IsNumeric(e.Item.Cells(2).Text) Then
                If e.Item.Cells(2).Text = 0 Then 'coberturas
                    e.Item.Cells(0).Text = ""
                    e.Item.Cells(1).Text = ""
                    dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), e.Item.Cells(3).Text, 0, Session("PaqSelCon"))
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            Dim ArrP1() As String = Split(Session("Calculos"), "*")
                            For kz = 0 To UBound(ArrP1)
                                Dim ArrP2() As String = ArrP1(kz).Split("|")
                                'nombre del paquete e id aseguradora
                                If ArrP2.IndexOf(ArrP2, CStr(dt.Rows(i)("id_aseguradora"))) = 0 And _
                                ArrP2.IndexOf(ArrP2, e.Item.Cells(3).Text) = 1 Then
                                    Exit For
                                End If
                            Next
                            If arrPol(recorrepoliza) = dt.Rows(i)("id_aseguradora") Then

                                If dt.Rows(i)("total_cob") > 0 Then
                                    validarecorrido = 0

                                    Dim DbRvalor As Double = 0
                                    arrTotal(i) = dt.Rows(i)("total_cob")
                                    Dbvalor = dt.Rows(i)("total_cob")

                                    Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(dt.Rows(i)("subramo"), dt.Rows(i)("id_aseguradora"), Session("bid"))
                                    If dtRegion.Rows.Count > 0 Then
                                        If Not dtRegion.Rows(0).IsNull("id_region") Then
                                            intregion = dtRegion.Rows(0)("id_region")
                                        End If
                                    End If
                                    banderaVida = 1
                                    hp = New HyperLink
                                    hp.Text = Dbvalor.ToString("c") & "&nbsp;&nbsp;"
                                    hp.CssClass = "negrita"
                                    hp.ForeColor = New System.Drawing.Color().Black
                                    hp.ToolTip = "Cobertura : " & e.Item.Cells(3).Text

                                    'If Session("nivel") = 0 Then
                                    '    If Session("CotiPaquete") > 0 Then
                                    '        If Session("CotiPaquete") = dt.Rows(i)("id_paquete") Then
                                    '            hpH = New HyperLink
                                    '            hpH.ToolTip = "Ayuda de Calculos"
                                    '            hpH.ImageUrl = "..\Imagenes\general\help.png"
                                    '            hpH.Attributes.Add("onclick", "Cuota('" & kz & "')")
                                    '            hpH.Width = Unit.Percentage(10)
                                    '            hpH.Style.Add("cursor", "pointer")
                                    '            e.Item.Cells(cuenta).Controls.Add(hpH)
                                    '        End If
                                    '    Else
                                    '        hpH = New HyperLink
                                    '        hpH.ToolTip = "Ayuda de Calculos"
                                    '        hpH.ImageUrl = "..\Imagenes\general\help.png"
                                    '        hpH.Attributes.Add("onclick", "Cuota('" & kz & "')")
                                    '        hpH.Width = Unit.Percentage(10)
                                    '        hpH.Style.Add("cursor", "pointer")
                                    '        e.Item.Cells(cuenta).Controls.Add(hpH)
                                    '    End If
                                    'End If

                                    If (Session("nivel") = 0 Or Session("nivel") = 1) Then
                                        If Session("CotiPaquete") > 0 Then
                                            If Session("CotiPaquete") = dt.Rows(i)("id_paquete") Then
                                                hp.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & cbo_plazo.SelectedValue & ",'" & e.Item.Cells(3).Text & "','" & cbo_plazo.SelectedItem.Text & "','" & dt.Rows(i)("subramo") & "','" & banderaVida & "','" & Session("contribuyente") & "','" & cbo_plazo.SelectedValue & "')")
                                                hp.Style.Add("cursor", "pointer")

                                                hpImagen = New HyperLink
                                                hpImagen.Text = Dbvalor.ToString("c")
                                                hpImagen.CssClass = "negrita"
                                                hpImagen.ForeColor = New System.Drawing.Color().Black
                                                hpImagen.ToolTip = "Cobertura : " & e.Item.Cells(3).Text
                                                hpImagen.ImageUrl = "..\Imagenes\Menu\printer.png"
                                                hpImagen.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & cbo_plazo.SelectedValue & ",'" & e.Item.Cells(3).Text & "','" & cbo_plazo.SelectedItem.Text & "','" & dt.Rows(i)("subramo") & "','" & banderaVida & "','" & Session("contribuyente") & "','" & cbo_plazo.SelectedValue & "')")
                                                hpImagen.Style.Add("cursor", "pointer")

                                                hp.Width = Unit.Percentage(70)
                                                hpImagen.Width = Unit.Percentage(10)

                                                e.Item.Cells(cuenta).Controls.Add(hp)
                                                e.Item.Cells(cuenta).Controls.Add(hpImagen)
                                            Else
                                                lblC = New Label
                                                lblC.Text = Dbvalor.ToString("c")
                                                lblC.CssClass = "negrita"

                                                e.Item.Cells(cuenta).Controls.Add(lblC)
                                            End If
                                        Else
                                            hp.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & cbo_plazo.SelectedValue & ",'" & e.Item.Cells(3).Text & "','" & cbo_plazo.SelectedItem.Text & "','" & dt.Rows(i)("subramo") & "','" & banderaVida & "','" & Session("contribuyente") & "','" & cbo_plazo.SelectedValue & "')")
                                            hp.Style.Add("cursor", "pointer")

                                            hpImagen = New HyperLink
                                            hpImagen.Text = Dbvalor.ToString("c")
                                            hpImagen.CssClass = "negrita"
                                            hpImagen.ForeColor = New System.Drawing.Color().Black
                                            hpImagen.ToolTip = "Cobertura : " & e.Item.Cells(3).Text
                                            hpImagen.ImageUrl = "..\Imagenes\Menu\printer.png"
                                            hpImagen.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & cbo_plazo.SelectedValue & ",'" & e.Item.Cells(3).Text & "','" & cbo_plazo.SelectedItem.Text & "','" & dt.Rows(i)("subramo") & "','" & banderaVida & "','" & Session("contribuyente") & "','" & cbo_plazo.SelectedValue & "')")
                                            hpImagen.Style.Add("cursor", "pointer")

                                            hp.Width = Unit.Percentage(70)
                                            hpImagen.Width = Unit.Percentage(10)

                                            e.Item.Cells(cuenta).Controls.Add(hp)
                                            e.Item.Cells(cuenta).Controls.Add(hpImagen)
                                        End If
                                    Else
                                        e.Item.Cells(cuenta).Controls.Add(hp)
                                    End If

                                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                Else
                                    e.Item.Cells(cuenta).Text = "-"
                                    e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                                End If
                            Else
                                validarecorrido = 1
                                e.Item.Cells(cuenta).Text = "-"
                                e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                            End If

                            recorrepoliza = recorrepoliza + 1
                            cuenta = cuenta + 1

                            If validarecorrido = 1 Then
                                i = i - 1
                                validarecorrido = 0
                            End If
                        Next
                    End If
                Else
                    'paquetes especiales
                    For i = 0 To UBound(Session("ArrPolValida"))
                        dt = ccliente.carga_cobertura_total_cob_aseguradora_ramo(Session("idgrdcontrato"), _
                               Session("bid"), e.Item.Cells(2).Text, Session("ArrPolValida")(i))
                        Dbvalor = 0
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0)("total_cob") > 0 _
                                And e.Item.Cells(12).Text = dt.Rows(0)("cap") _
                                And e.Item.Cells(13).Text = dt.Rows(0)("sumaaseg_cob") Then
                                Dbvalor = dt.Rows(0)("total_cob")
                                If Session("banderafecha") = 1 Then
                                    Dbvalor = Dbvalor * 1.16
                                Else
                                    Dbvalor = Dbvalor * 1.16
                                End If
                            End If
                        End If
                        e.Item.Cells(cuenta).CssClass = "negrita"
                        e.Item.Cells(cuenta).Attributes.Add("align", "center")
                        If Dbvalor <= 0 Then
                            e.Item.Cells(cuenta).Text = ""
                        Else
                            e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                        End If
                        cuenta = cuenta + 1
                    Next
                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            If arrTotal.Length > 0 Then
                e.Item.Cells(6).Text = "Total"
                e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Right
                Dim i As Integer = 0
                Dim cuenta As Integer = 7
                For i = 0 To arrTotal.Length - 1
                    Dbvalor = 0
                    Dbvalor = arrTotal(i)
                    e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                    cuenta = cuenta + 1
                Next
            End If
        End If
        Session("ArrPolValida") = arrPol
    End Sub

    Public Sub agrega_java(ByVal ckb As CheckBox, ByVal cuenta As Integer, ByVal intpaquete As String, ByVal bantipo As Integer)
        If bantipo = 1 Then
            ckb.Attributes.Add("Onclick", "ckb_vida('" & cuenta & "','" & intpaquete & "')")
        Else
            ckb.Attributes.Add("Onclick", "ckb_desempleo('" & cuenta & "','" & intpaquete & "')")
        End If
    End Sub

    Public Sub carga_plazo()
        Dim i As Integer = 0
        Dim arr As New ArrayList
        Dim max As Integer = 60
        arr.Add("-- Seleccione --")
        For i = 6 To max
            arr.Add(i)
        Next
        cbo_plazo.DataSource = arr
        cbo_plazo.DataBind()
        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
    End Sub

    Public Function tasa_std() As String
        Dim tipo As String = ""
        Dim valor As Double = 0
        Dim i As Integer
        '1 = si
        '2 = no
        tipo = "C"
        Session("taza") = 2
        Session("InterestRate") = "C"
        Return tipo
    End Function

    Private Sub cmd_regrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regrear.Click
        Response.Redirect("WfrmCotiza.aspx?valcost=" & txt_precio.Text & "")
    End Sub


    'Public Function quita_decimales(ByVal strcadena As String) As Integer
    '    Dim i As Integer = 0
    '    Dim salida As Integer
    '    Dim valor As String
    '    For i = 0 To strcadena.Length - 1
    '        valor = Mid(strcadena, i + 1, 1)
    '        If valor = "." Then
    '            Exit For
    '        Else
    '            salida = salida & CInt(valor)
    '        End If
    '        valor = ""
    '    Next
    '    Return salida
    'End Function


End Class
