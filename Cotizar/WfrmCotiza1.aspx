<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="uc1" TagName="WfrmUCobertura" Src="WfrmUCobertura.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotiza1.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotiza1" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function PonDiagonal()
		{
			var stCadena=document.cotiza.txt_fecha.value;
			if(stCadena.length == 2)
			{
				document.cotiza.txt_fecha.value=document.cotiza.txt_fecha.value+"/"
			}
			if(stCadena.length == 5)
			{
				document.cotiza.txt_fecha.value=document.cotiza.txt_fecha.value+"/"
			}	
		}
		function redireccion(intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago, tipoCarga)
		{
			
			/*if ((document.getElementById("txt_contrato").value)=='')
			{
				alert("El numero de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_contrato").focus();
				return 0
			}*/
			if ((document.getElementById("txt_fecha").value)=='')
			{
				alert("La fecha de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_fecha").focus();
				return 0
			}
			var uno = document.getElementById("txt_preciof").value;
			var strfecha1 = document.getElementById("txt_fecha").value;
			//var cvecontrato1 = document.getElementById("txt_contrato").value;
			var cvecontrato1 = 0
			dbprecio=0
			var path = "WfrmCotiza1.aspx?cveAseg="+intaseguradora+"&valcost="+uno+"&cveplazo="+intplazo+"&strfecha="+strfecha1+"&cveregion="+intregion+"&cvepaquete="+intpaquete+"&subramo="+subramo+"&contrato="+cvecontrato1+"&Recargo="+IntRecargo+"&NumPago="+IntNumPago+"&Pago1="+Pago+"&Pago2="+SubPago+"&TipoCarga="+tipoCarga+""
			//alert(path)
			window.location.href=path;
		}
		String.prototype.trim = function() 
		{
			return this.replace(',','');
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="cotiza" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Emisor de 
							P�lizas de&nbsp;Conauto&nbsp;&nbsp; </FONT>
					</TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="Obligatorio_negro" align="right" width="15%">Marca :</TD>
					<TD width="25%"><asp:label id="txt_marca" runat="server" CssClass="small" Width="100%"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right" width="15%">Modelo :</TD>
					<TD width="25%"><asp:label id="txt_modelo" runat="server" CssClass="small" Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" align="right">Tipo :</TD>
					<TD><asp:label id="txt_tipo" runat="server" CssClass="small" Width="100%"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right">Descripci�n :</TD>
					<TD><asp:textbox id="txt_descripcion" runat="server" CssClass="small" Width="100%" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" align="right">Precio :</TD>
					<TD><asp:label id="txt_precio" runat="server" CssClass="small" Width="100%"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right"><asp:label id="lbl_clasificacion" runat="server" Width="100%">Clasificaci�n :</asp:label></TD>
					<TD><asp:label id="txt_clasificacion" runat="server" CssClass="small" Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Contribuyente :</TD>
					<TD><asp:label id="lbl_persona" runat="server" CssClass="negrita" Width="100%"></asp:label></TD>
					<TD class="Obligatorio" align="right">
						<asp:label id="lbl_carga" runat="server" Width="100%">Tipo de Carga :</asp:label></TD>
					<TD>
						<asp:dropdownlist id="cbo_carga" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Plazo :</TD>
					<TD><asp:dropdownlist id="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;
						<asp:label id="Label5" runat="server" CssClass="negrita">Meses</asp:label></TD>
					<TD class="Obligatorio" align="right">Forma de Pago :</TD>
					<TD><asp:dropdownlist id="cbo_tipo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">1er. Pago :</TD>
					<TD><asp:label id="lbl_primero" runat="server" CssClass="negrita"></asp:label></TD>
					<TD class="Obligatorio" align="right">No. Pago :
					</TD>
					<TD><asp:dropdownlist id="cbo_recargo" runat="server" CssClass="combos_small" AutoPostBack="True" Enabled="False"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Recibos Sub :
					</TD>
					<TD><asp:label id="lbl_segundo" runat="server" CssClass="negrita"></asp:label></TD>
					<TD class="Obligatorio" align="right">Plazo del seguro :</TD>
					<TD><asp:label id="lbl_plazo" runat="server" CssClass="negrita"></asp:label><asp:label id="lbl_meses" runat="server" CssClass="negrita"> Meses</asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="25%"></TD>
								<TD width="25%"></TD>
								<TD class="Obligatorio_negro" align="right" width="25%"></TD>
								<TD width="25%"></TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="4">Para poder comprar la p�liza es necesario que 
									seleccione la impresora debajo de la aseguradora deseada *</TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="4"><asp:datagrid id="grd_cobertura" runat="server" CssClass="datagrid" Width="100%" AllowSorting="True"
										CellPadding="1" AutoGenerateColumns="False">
										<FooterStyle Wrap="False" CssClass="Footer_azul"></FooterStyle>
										<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
										<EditItemStyle Wrap="False"></EditItemStyle>
										<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
										<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
										<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
												HeaderText="Seleccionar" CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;" HeaderText="Eliminar"
												CommandName="Delete">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:ButtonColumn>
											<asp:BoundColumn Visible="False" DataField="cobertura"></asp:BoundColumn>
											<asp:BoundColumn DataField="descripcion_cob" ReadOnly="True" HeaderText="Cobertura">
												<ItemStyle Wrap="False" Width="13%"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n">
												<ItemStyle Wrap="False" Width="15%"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label1 runat="server" Width="100%" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grddesc runat="server" Width="100%" CssClass="combos_small" TextMode="MultiLine" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Suma Aseg.">
												<ItemStyle Wrap="False" HorizontalAlign="Right" Width="10%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label2 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdsuma runat="server" Width="100%" CssClass="combos_small" MaxLength="11" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="No. Fact.">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label3 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdfact runat="server" Width="100%" CssClass="combos_small" MaxLength="15" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="cap" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="SUMAASEG_COB"></asp:BoundColumn>
										</Columns>
										<PagerStyle Wrap="False"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<asp:table id="tb" runat="server" Width="100%" CssClass="negrita" HorizontalAlign="Right"></asp:table></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD><asp:button id="cmd_regrear" runat="server" CssClass="boton" Text="Regresar"></asp:button></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD><asp:button id="cmd_cotizar" runat="server" CssClass="boton" Text="Imprimir Cotizaci�n"></asp:button></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD><asp:radiobuttonlist id="rb_seguro" runat="server" CssClass="combos_small" Width="100%" RepeatDirection="Horizontal"
							AutoPostBack="True" Visible="False">
							<asp:ListItem Value="0">Financiado</asp:ListItem>
							<asp:ListItem Value="1">Contado</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD><asp:textbox id="txt_grdramo" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox><asp:textbox id="txt_grdcobertura" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox><asp:textbox id="txt_banseguro" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False">2</asp:textbox><asp:textbox id="txt_gdescripcion" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox><asp:textbox id="txt_sumcob" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox><asp:textbox id="txt_ban" runat="server" CssClass="combos_small" Width="16px" Height="16px" Visible="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD><asp:label id="txt_subtipo" runat="server" CssClass="small" Visible="False"></asp:label><asp:textbox id="txt_fecha" onkeyup="PonDiagonal()" tabIndex="10" runat="server" CssClass="combos_small"
							Width="0px" AutoPostBack="True" MaxLength="10"></asp:textbox><asp:textbox id="txt_precioF" runat="server" CssClass="combos_small" Width="0px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" height="10"></TD>
					<TD height="10"></TD>
					<TD class="Obligatorio_negro" height="10"></TD>
					<TD height="10"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD colSpan="2"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 103; LEFT: 264px; POSITION: absolute; TOP: 840px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
