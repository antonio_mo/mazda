<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmImpCotiza.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmImpCotiza"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmImpCotiza</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="..\Css\submodal.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="..\JavaScript\submodalsource.js" type="text/javascript"></script>
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD class="obligatorio" align="right"></TD>
					<TD colSpan="2">
						<asp:Label id="lbl_cotizacion" runat="server" CssClass="Interiro_tabla_centro" Width="100%"></asp:Label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">&nbsp;*Nombre del cliente :
					</TD>
					<TD><asp:textbox style="Z-INDEX: 0" id="txt_cliente" onkeypress="convierteMayusculas(event);" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="150"></asp:textbox></TD>
					<TD><asp:requiredfieldvalidator style="Z-INDEX: 0" id="RequiredFieldValidator1" runat="server" ErrorMessage="Datos del cliente"
							ControlToValidate="txt_cliente">*</asp:requiredfieldvalidator></TD>
					<TD><asp:table style="Z-INDEX: 0" id="tb_imp" runat="server" Width="100%" CssClass="negrita"></asp:table></TD>
				</TR>
				<%--<TR>
					<TD class="obligatorio" align="right">&nbsp;* Grupo-Integrante :</TD>
					<TD><asp:textbox style="Z-INDEX: 0" id="txt_grupo" onkeypress="javascript:onlyDigits(event,'decOK');"
							runat="server" Width="100%" CssClass="combos_small" MaxLength="7"></asp:textbox></TD>
					<TD><asp:requiredfieldvalidator style="Z-INDEX: 0" id="Requiredfieldvalidator2" runat="server" ErrorMessage="Grupo Integrante"
							ControlToValidate="txt_grupo">*</asp:requiredfieldvalidator></TD>
					<TD></TD>
				</TR>--%>
				<TR>
					<TD class="negrita" align="right">&nbsp;No. de serie :</TD>
					<TD><asp:textbox style="Z-INDEX: 0" id="txt_serie" onkeypress="convierteMayusculas(event);" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="17"></asp:textbox></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="negrita" align="right">Observaciones :</TD>
					<TD colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txt_observaciones" onkeypress="convierteMayusculas(event);" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="250" TextMode="MultiLine"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right"></TD>
					<TD colSpan="2" align="center"><asp:validationsummary style="Z-INDEX: 0" id="Validationsummary2" runat="server" Font-Size="12px" Font-Names="Tahoma"
							HeaderText="Los siguientes campos son obligatorios"></asp:validationsummary></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right"></TD>
					<TD align="center"><asp:button style="Z-INDEX: 0" id="cmd_guardar" runat="server" CssClass="boton" Text="Guardar"></asp:button></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="25%"></TD>
					<TD width="30%"></TD>
					<TD width="20%"></TD>
					<TD width="25%"></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox style="Z-INDEX: 102; POSITION: absolute; TOP: 416px; LEFT: 360px" id="MsgBox" runat="server"></cc1:MsgBox></form>
	</body>
</HTML>
