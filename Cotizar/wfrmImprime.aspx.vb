Imports CN_Negocios

Partial Class wfrmImprime
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private cgenera As New CnCotizador


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Dim dt As DataTable
        Dim i As Integer = 0

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Request.QueryString("url") Is Nothing Then
                    If Not Session("Bandera_Espera") Is Nothing Then
                        dt = cgenera.Consulta_contenedor(Session("Llave"), Session("LlaveC"))
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0)("estatus") = 0 Then
                                Response.Redirect("EsperaReportes.html?Bandera_Espera=" & Session("Bandera_Espera") & "")
                                Exit Sub
                            Else
                                For i = 0 To dt.Rows.Count - 1
                                    If Session("programa") = 9 Then
                                        If Not dt.Rows(i)("nombre_reporte") = "P" Then
                                            Lnk_imprime.Text = dt.Rows(i)("nombre_reporte")
                                            Lnk_imprime.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                                            Lnk_imprime.Target = "contenido"
                                        Else
                                            Lnk_imprime.Text = dt.Rows(i)("nombre_reporte")
                                            Lnk_imprime.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                                            Lnk_imprime.Target = "contenido"

                                            Lnk_imprime1.Text = "Carta de Renovaci�n"
                                            Lnk_imprime1.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_CartaRenovacion") & " &nomb=Carta de Renovaci�n"
                                            Lnk_imprime1.Target = "contenido"

                                            Lnk_imprime2.Text = "Comprobante de Pago"
                                            Lnk_imprime2.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_Recibo") & " &nomb=Comprobante de Pago"
                                            Lnk_imprime2.Target = "contenido"
                                        End If
                                    Else
                                        If i = 0 Then
                                            Lnk_imprime.Text = dt.Rows(i)("nombre_reporte")
                                            Lnk_imprime.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                                            Lnk_imprime.Target = "contenido"
                                        Else
                                            Lnk_imprime1.Text = dt.Rows(i)("nombre_reporte")
                                            Lnk_imprime1.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                                            Lnk_imprime1.Target = "contenido"
                                        End If
                                    End If
                                Next
                                If Session("aseguradora") = 1 Then
                                    Lnk_imprime1.Visible = True
                                Else
                                    Lnk_imprime1.Visible = False
                                End If
                            End If
                        End If
                    End If
                Else
                    Dim url As String = Request.QueryString("url")
                    Dim nombre As String = Request.QueryString("nomb")
                    'url = "C:/Inetpub/wwwroot/CotizadorContado_Conauto/" & url
                    url = "D:/Aplicaciones/CotizadorContado_Conauto/" & url
                    Session("nombrepdf") = nombre
                    Session("urlpdf") = url
                    sd.FileName = nombre
                    sd.File = url
                    sd.ShowDialogBox()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub lk_regresa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lk_regresa.Click
        If Session("ban") = 1 Then
            'Response.Redirect("WfrmCotiza1.aspx?valcost1=" & Session("precioEscrito") & "")
            Response.Redirect("WfrmClientesP.aspx?cveclicoti=" & Session("cliente") & "")
        Else
            Response.Redirect("WfrmImp.aspx?band=" & Session("ban") & "")
        End If
    End Sub
End Class
