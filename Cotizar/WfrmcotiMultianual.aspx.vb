Imports System.Threading
Imports Conectar_Contado
Imports Cn_Cotizador

Public Class WfrmcotiMultianual
    Inherits System.Web.UI.Page

    Private Reporte As New Conectar_Contado.inicializa_Contado
    Private ccliente As CnCotizador

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        If Request.QueryString("callback") <> Nothing And Request.QueryString("strmsg") <> Nothing Then
            Me.IsCallbackMsg(Request.QueryString("strmsg"))
        Else
            Me.IsCallback()
        End If

    End Sub

    Private Function IsCallbackMsg(ByVal strmsg As String) As Boolean
        Thread.Sleep(4000)
        Response.Write(strmsg)
        Response.Flush()
        Response.End()
        Return True
    End Function

    Private Function IsCallback() As Boolean
        If (Request.QueryString("callback") <> Nothing) Then

            Dim cadenaEnvio As String = ""
            Dim strpoliza As String = ""
            Dim llave As Integer
            Thread.Sleep(4000)

            ccliente = New CnCotizador(Session("DsnAcceso"))

            Session("LlaveC") = 0
            Dim version As String = ccliente.carga_version(Session("version"), IIf(Session("aseguradora") Is Nothing, 0, Session("aseguradora")))
            Try
                If Not Session("idpoliza") Is Nothing Then
                    strpoliza = ccliente.sacando_poliza(Session("idpoliza"))
                End If

                Session("Bandera_Espera") = 1
                If strpoliza = "" Then
                    llave = ccliente.inserta_contenedor_reportes(Session("bid"), "", "C")
                Else
                    llave = ccliente.inserta_contenedor_reportes(Session("bid"), strpoliza, "C")
                End If
                Session("Llave") = llave
                cadenaEnvio = "1|" & 1 & "|" & Session("cotiza") & "|" & _
                Session("bid") & "|" & Session("cliente") & "|" & Session("idgrdcontrato") & "|" & _
                Session("vehiculo") & "|" & Session("version") & "|" & Session("EstatusV") & "|" & _
                llave & "|" & Session("programa") & "|" & Session("anio") & "|" & Session("subramo") & "|" & _
                Replace(Session("MultArrPolisa"), "|", "+") & "|" & Session("seguro") & "|" & _
                Session("PaqSelCon") & "|" & Session("resultado")(5) & "|" & Session("cveporenganche")

                Reporte.carga(cadenaEnvio, "E")
                Response.Write("OK")
                Session("PostBack") = True
                'Response.Redirect("..\cotizar\EsperaReportes.html")
            Catch ex As Exception
                Response.Write("Error en la creaci�n de la cotizaci�n:")
            Finally
                Response.Flush()
                Response.End()
            End Try
            Return True
        End If
        Return False
    End Function

End Class
