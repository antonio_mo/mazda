<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmClientesP.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmClientesP" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="../Jscripts.js"></script>
		<script language="javascript" src="../JavaScript/Jscripts.js"></script>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table5" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Cotizaci�n 
							Prospecto</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" width="20%">Nombre (s) :
					</TD>
					<TD class="obligatorio" width="20%">Apellido Paterno :</TD>
					<TD class="obligatorio" width="20%">Apellido Materno :</TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="negrita" vAlign="top" width="20%" rowSpan="13"><asp:label id="lbl_cotizacion" runat="server" CssClass="rojobig" Width="100%" Visible="False"></asp:label><asp:table id="tb_imp" runat="server" CssClass="negrita" Width="100%"></asp:table></TD>
				</TR>
				<TR>
					<!--onkeypress="javascript:onlyAllLetters(event);" -->
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_nombre" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="25"></asp:textbox></TD>
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_apaterno" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="25"></asp:textbox></TD>
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_amaterno" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="25"></asp:textbox></TD>
					<TD><ONTRANET:SAVEDIALOG id="sd" runat="server"></ONTRANET:SAVEDIALOG></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top">Personalidad fiscal :</TD>
					<TD colSpan="2"><asp:radiobuttonlist id="rb_tipo" runat="server" CssClass="negrita" Width="100%" RepeatDirection="Horizontal"
							RepeatColumns="3" AutoPostBack="True">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist>
						<asp:label id="lbl_persona" runat="server" CssClass="negrita" Width="100%"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Razon Social :</TD>
					<TD colSpan="2"><asp:textbox onkeypress="convierteMayusculas();" id="txt_razon" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="100"></asp:textbox></TD>
					<TD><asp:textbox id="txt_clave" runat="server" CssClass="combos_small" Width="24px" Visible="False"></asp:textbox><asp:textbox id="txt_vida" runat="server" CssClass="combos_small" Width="24px" Visible="False"></asp:textbox><asp:textbox id="txt_desempleo" runat="server" CssClass="combos_small" Width="24px" Visible="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Tel�fonos :</TD>
					<TD colSpan="2"><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_tel" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="13"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:panel id="pnl_dir" runat="server">
							<TABLE id="Table6" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="Interiro_tabla_centro">Domicilio</TD>
									<TD></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colSpan="4">
										<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD class="obligatorio">C�digo Postal :</TD>
												<TD>
													<asp:textbox onkeypress="convierteMayusculas();" id="txt_cp" runat="server" Width="50%" CssClass="combos_small"
														MaxLength="5"></asp:textbox></TD>
												<TD>
													<asp:button id="cmd_buscar_cp" runat="server" CssClass="boton" Text="Buscar C.P."></asp:button></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD width="21%"></TD>
												<TD width="20%"></TD>
												<TD width="20%"></TD>
												<TD width="39%"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD class="obligatorio">Estado :</TD>
									<TD class="obligatorio">Provincia :</TD>
									<TD class="obligatorio"></TD>
									<TD class="obligatorio"></TD>
								</TR>
								<TR>
									<TD>
										<asp:Label id="lbl_estado" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
									<TD>
										<asp:Label id="lbl_provincia" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio"></TD>
									<TD colSpan="3"></TD>
								</TR>
								<TR>
									<TD width="21%"></TD>
									<TD width="25%"></TD>
									<TD width="25%"></TD>
									<TD width="29%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="negrita">Elabor� Cotizaci�n :</TD>
					<TD colSpan="3"><asp:textbox onkeypress="convierteMayusculas();" id="txt_elaboro" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="25"></asp:textbox></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="center"><asp:button id="cmd_cliente" runat="server" CssClass="boton" Text="Buscar Cliente"></asp:button></TD>
					<TD align="center"><asp:button id="cmd_imprimir" runat="server" CssClass="boton" Text="Generar Cotizaci�n"></asp:button></TD>
					<TD align="center"><asp:button id="cmd_agregar" runat="server" CssClass="boton" Text="Guardar y Continuar"></asp:button><asp:button id="cmd_modificar" runat="server" CssClass="boton" Text="Modificar y Continuar"></asp:button></TD>
					<TD align="center"><asp:button id="cmd_regresar" runat="server" CssClass="boton" Text="Regresar"></asp:button></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="4"><asp:panel id="pnl_clientes" runat="server" Width="100%" Height="176px">
										<DIV style="OVERFLOW: hidden; WIDTH: 100%; HEIGHT: 16px">
											<asp:datagrid id="grdcatalogo_fondo" style="POSITION: relative" runat="server" Width="500px" CssClass="Datagrid"
												CellPadding="1" AutoGenerateColumns="False">
												<FooterStyle Wrap="False" CssClass="Footer_azul"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="Interiro_tabla_centro" VerticalAlign="Top"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataField="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<DIV id="divScroll" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 120px" onscroll="DoScroll()">
											<asp:datagrid id="grdcatalogo" runat="server" Width="500px" CssClass="datagrid" CellPadding="1"
												AutoGenerateColumns="False" ShowHeader="False" AllowSorting="True">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataField="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD width="25%"></TD>
												<TD class="negrita" align="center" width="25%">Nombre Cliente :</TD>
												<TD class="negrita" align="center" width="5%"></TD>
												<TD class="negrita" width="25%"></TD>
												<TD width="20%"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD>
													<asp:TextBox onkeypress="convierteMayusculas();" id="txt_cliente" runat="server" Width="100%"
														CssClass="combos_small" MaxLength="15"></asp:TextBox></TD>
												<TD style="WIDTH: 20px"></TD>
												<TD></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD align="center">
													<asp:Button id="cmd_buscar" runat="server" CssClass="boton" Text="Consultar"></asp:Button></TD>
												<TD></TD>
												<TD></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 103; LEFT: 320px; POSITION: absolute; TOP: 928px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
