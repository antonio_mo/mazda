'Imports Emisor_Conauto
Imports CN_Negocios

Partial Class WfrmCliente
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private crfc As New CnRFC
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                If Session("programa") = 1 Or Session("programa") = 2 Then
                    cmd_clientep.Visible = False
                End If

                pnl_dir.Visible = True
                'viewstate("iva") = 0
                'para regresar valores
                If Not Request.QueryString("cvevida") Is Nothing Then
                    txt_vida.Text = Request.QueryString("cvevida")
                    ViewState("txtvida") = txt_vida.Text
                End If
                If Not Request.QueryString("cvedesempleo") Is Nothing Then
                    txt_desempleo.Text = Request.QueryString("cvedesempleo")
                    ViewState("txtdesempleo") = txt_desempleo.Text
                End If

                'solo en el caso de que se registre la poliza
                If Not Request.QueryString("cvevidau") Is Nothing Then
                    txt_vida.Text = txt_vida.Text & "/" & Request.QueryString("cvevidau")
                End If
                If Not Request.QueryString("cvedesempleou") Is Nothing Then
                    txt_desempleo.Text = txt_desempleo.Text & "/" & Request.QueryString("cvedesempleou")
                End If

                Dim arrx() As String = Session("ArrPoliza1")
                Dim arrz() As String = Session("ArrPoliza")

                limpia()
                desabilita()

                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(Session("contribuyente")))
                personalidad(rb_tipo)
                pnl_clientes.Visible = False
                txt_agencia.Text = Session("NombreDistribuidor")

                txt_cp.Text = Session("CodPostal")
                cmd_buscar_cp_Click(cmd_buscar_cp, Nothing)
                txt_cp.Enabled = False

                txt_beneficiario.Enabled = True
                txt_beneficiario.Visible = True

                If Not Session("cotizacion") = 0 Then
                    busqueda_cliente_cotizacion()
                End If
                SetFocus(txt_nombre)
            End If

            cmd_limpiar.Visible = True
            cmd_cliente.Visible = True
            txt_nombre.Enabled = True
            txt_materno.Enabled = True
            txt_paterno.Enabled = True
            txt_razon.Enabled = True

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub busqueda_cliente_cotizacion()
        txt_clave.Text = Session("cliente")
        viewstate("NombreCliente") = Session("NomCliente")
        If Session("contribuyente") <> 2 Then
            viewstate("TipoGrd") = "F"
        Else
            viewstate("TipoGrd") = "M"
        End If
        carga_clientep()
        pnl_clientes.Visible = False
        cmd_agregar.Visible = True
    End Sub

    Public Sub limpia()
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_fnacimiento.Text = ""
        txt_rfc.Text = ""
        txt_lugarN.Text = ""
        txt_ocupacion.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_contacto.Text = ""
        txt_rfcrazon.Text = ""
        txt_tel.Text = ""
        txt_empleado.Text = ""
        txt_agencia.Text = ""
        txt_cp.Text = ""
        txt_dir.Text = ""
        'lbl_provincia.Text = ""
        'lbl_estado.Text = ""
        txt_col.Text = ""
        txt_beneficiario.Text = ""
        txt_email.Text = ""
        txt_interior.Text = ""
        txt_exterior.Text = ""
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Select Case Session("contribuyente")
                Case 0
                    lbl_persona.Text = "F�sica"
                    rb_tipo.Visible = False
                Case 1
                    lbl_persona.Text = "Act. Empresarial"
                    rb_tipo.Visible = False
                Case 2
                    lbl_persona.Text = "Moral"
                    rb_tipo.Visible = False
            End Select

            If valor = 2 Then
                habilita()
                pnl_moral.Visible = True
                pnl_fisica.Visible = False
                SetFocus(txt_razon)
            Else
                desabilita()
                pnl_moral.Visible = False
                pnl_fisica.Visible = True
                SetFocus(txt_nombre)
            End If
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function


    Public Sub habilita()
        txt_razon.Enabled = True
        txt_legal.Enabled = True
        txt_contacto.Enabled = True
        txt_rfcrazon.Enabled = True
        txt_contacto.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_rfcrazon.Text = ""
        txt_contacto.CssClass = "combos_small"
        txt_razon.CssClass = "combos_small"
        txt_legal.CssClass = "combos_small"
        txt_rfcrazon.CssClass = "combos_small"

        pc_reporte.Enabled = False
        txt_nombre.Enabled = False
        txt_paterno.Enabled = False
        txt_materno.Enabled = False
        txt_rfc.Enabled = False
        txt_lugarN.Enabled = False
        txt_ocupacion.Enabled = False
        txt_fnacimiento.Enabled = False
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_rfc.Text = ""
        txt_fnacimiento.Text = ""
        txt_lugarN.Text = ""
        txt_ocupacion.Text = ""
        txt_nombre.CssClass = "negrita"
        txt_paterno.CssClass = "negrita"
        txt_materno.CssClass = "negrita"
        txt_rfc.CssClass = "negrita"
        txt_fnacimiento.CssClass = "negrita"
        txt_lugarN.CssClass = "negrita"
        txt_ocupacion.CssClass = "negrita"
    End Sub

    Public Sub desabilita()
        txt_razon.Enabled = False
        txt_legal.Enabled = False
        txt_contacto.Enabled = False
        txt_rfcrazon.Enabled = False
        txt_contacto.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_rfcrazon.Text = ""
        txt_contacto.CssClass = "negrita"
        txt_razon.CssClass = "negrita"
        txt_legal.CssClass = "negrita"
        txt_rfcrazon.CssClass = "negrita"

        pc_reporte.Enabled = True
        txt_lugarN.Enabled = True
        txt_nombre.Enabled = True
        txt_paterno.Enabled = True
        txt_materno.Enabled = True
        txt_rfc.Enabled = True
        txt_fnacimiento.Enabled = True
        txt_ocupacion.Enabled = True
        txt_fnacimiento.Enabled = True
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_rfc.Text = ""
        txt_fnacimiento.Text = ""
        txt_ocupacion.Text = ""
        txt_fnacimiento.Text = ""
        txt_lugarN.CssClass = "combos_small"
        txt_nombre.CssClass = "combos_small"
        txt_paterno.CssClass = "combos_small"
        txt_materno.CssClass = "combos_small"
        txt_rfc.CssClass = "combos_small"
        txt_fnacimiento.CssClass = "combos_small"
        txt_ocupacion.CssClass = "combos_small"
        txt_fnacimiento.CssClass = "combos_small"
    End Sub


    Private Sub rb_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tipo.SelectedIndexChanged
        personalidad(rb_tipo)
    End Sub

    Private Sub grdcatalogo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdcatalogo.SelectedIndexChanged
        Dim bandera As Integer = 0
        txt_clave.Text = 0
        viewstate("NombreCliente") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(0).Text
        viewstate("TipoGrd") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(3).Text
        If txt_tipocliente.Text = "1" Then
            bandera = carga_cliente()
        Else
            carga_clientep()
        End If
        If bandera = 1 Then
            pnl_clientes.Visible = True
            'evi 17/12/2014
            'cmd_agregar.Visible = False
            cmd_agregar.Visible = True
        Else
            pnl_clientes.Visible = False
            cmd_agregar.Visible = True
        End If
        'cmd_modificar.Visible = True
    End Sub

    Public Sub carga_clientep()
        Dim dt As DataTable = ccliente.DatosClienteP(Session("bid"), viewstate("NombreCliente"), viewstate("TipoGrd"))
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("nombreP") Then
                txt_nombre.Text = dt.Rows(0)("nombreP")
            Else
                txt_nombre.Text = ""
            End If
            If Not dt.Rows(0).IsNull("apaternoP") Then
                txt_paterno.Text = dt.Rows(0)("apaternoP")
            Else
                txt_paterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("amaternop") Then
                txt_materno.Text = dt.Rows(0)("amaternop")
            Else
                txt_materno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("rfcp") Then
                txt_rfc.Text = dt.Rows(0)("rfcp")
            Else
                txt_rfc.Text = ""
            End If
            If Not dt.Rows(0).IsNull("estado") Then
                lbl_estado.Text = dt.Rows(0)("estado")
            Else
                lbl_estado.Text = ""
            End If
            If Not dt.Rows(0).IsNull("provincia") Then
                lbl_provincia.Text = dt.Rows(0)("provincia")
            Else
                lbl_provincia.Text = ""
            End If

            If Not dt.Rows(0).IsNull("telefonop") Then
                txt_tel.Text = dt.Rows(0)("telefonop")
            Else
                txt_tel.Text = ""
            End If

            If Not dt.Rows(0).IsNull("NoExterior") Then
                txt_exterior.Text = dt.Rows(0)("NoExterior")
            Else
                txt_exterior.Text = ""
            End If

            If Not dt.Rows(0).IsNull("NoInterior") Then
                txt_interior = dt.Rows(0)("NoInterior")
            Else
                txt_interior.Text = ""
            End If

            If Not dt.Rows(0).IsNull("tipopersonap") Then
                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("tipopersonap")))
                If dt.Rows(0)("tipopersonap") = 2 Then
                    habilita()
                    If Not dt.Rows(0).IsNull("razon_socialp") Then
                        txt_razon.Text = dt.Rows(0)("razon_socialp")
                    Else
                        txt_razon.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("contactop") Then
                        txt_contacto.Text = dt.Rows(0)("contactop")
                    Else
                        txt_contacto.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("rfcp") Then
                        txt_rfcrazon.Text = dt.Rows(0)("rfcp")
                    Else
                        txt_rfcrazon.Text = ""
                    End If
                    txt_fnacimiento.Text = ""
                    txt_rfc.Text = ""
                Else
                    txt_razon.Text = ""
                    txt_contacto.Text = ""
                    txt_rfcrazon.Text = ""
                    txt_razon.Enabled = False
                    txt_legal.Enabled = False
                    txt_contacto.Enabled = False
                    txt_rfcrazon.Enabled = False
                    txt_contacto.CssClass = "negrita"
                    txt_razon.CssClass = "negrita"
                    txt_legal.CssClass = "negrita"
                    txt_rfcrazon.CssClass = "negrita"

                    pc_reporte.Enabled = True
                    txt_nombre.Enabled = True
                    txt_paterno.Enabled = True
                    txt_materno.Enabled = True
                    txt_rfc.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_ocupacion.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_nombre.CssClass = "combos_small"
                    txt_paterno.CssClass = "combos_small"
                    txt_materno.CssClass = "combos_small"
                    txt_rfc.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                    txt_ocupacion.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                End If
            End If
            

        End If
    End Sub

    Public Function carga_cliente() As Integer
        Dim bandera As Integer = 0
        Dim dt As DataTable = ccliente.carga_inf_cliente(Session("bid"), viewstate("NombreCliente"), viewstate("TipoGrd"))
        Try
            If (dt.Rows(0)("cp_cliente").ToString().Trim() = Session("CodPostal")) Then
                If dt.Rows(0).IsNull("nombre") Then
                    txt_nombre.Text = ""
                Else
                    txt_nombre.Text = dt.Rows(0)("nombre")
                End If
                If dt.Rows(0).IsNull("apaterno") Then
                    txt_paterno.Text = ""
                Else
                    txt_paterno.Text = dt.Rows(0)("apaterno")
                End If
                If dt.Rows(0).IsNull("amaterno") Then
                    txt_materno.Text = ""
                Else
                    txt_materno.Text = dt.Rows(0)("amaterno")
                End If
                If dt.Rows(0).IsNull("fnacimiento") Then
                    txt_fnacimiento.Text = ""
                Else
                    txt_fnacimiento.Text = dt.Rows(0)("fnacimiento")
                End If
                If dt.Rows(0).IsNull("rfc") Then
                    txt_rfc.Text = ""
                Else
                    txt_rfc.Text = dt.Rows(0)("rfc")
                End If

                If dt.Rows(0).IsNull("email") Then
                    txt_email.Text = ""
                Else
                    txt_email.Text = dt.Rows(0)("email")
                End If
                If Not dt.Rows(0).IsNull("per_fiscal") Then
                    rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("Per_Fiscal")))
                    If Not Session("contribuyente") = dt.Rows(0)("Per_Fiscal") Then
                        MsgBox.ShowMessage("La personalidad fisica no son iguales favor de verificarlo")
                        Return 1
                        Exit Function
                    End If
                    If dt.Rows(0)("Per_Fiscal") = 2 Then
                        habilita()
                        If Not dt.Rows(0).IsNull("razon_social") Then
                            txt_razon.Text = dt.Rows(0)("razon_social")
                        Else
                            txt_razon.Text = ""
                        End If
                        If Not dt.Rows(0).IsNull("contacto") Then
                            txt_contacto.Text = dt.Rows(0)("contacto")
                        Else
                            txt_contacto.Text = ""
                        End If
                        If Not dt.Rows(0).IsNull("rep_legal") Then
                            txt_legal.Text = dt.Rows(0)("rep_legal")
                        Else
                            txt_legal.Text = ""
                        End If
                        If Not dt.Rows(0).IsNull("rfc") Then
                            txt_rfcrazon.Text = dt.Rows(0)("rfc")
                        Else
                            txt_rfcrazon.Text = ""
                        End If
                        txt_rfc.Text = ""
                        txt_fnacimiento.Text = ""
                    Else
                        txt_razon.Text = ""
                        txt_contacto.Text = ""
                        txt_legal.Text = ""
                        txt_rfcrazon.Text = ""
                        txt_razon.Enabled = False
                        txt_legal.Enabled = False
                        txt_contacto.Enabled = False
                        txt_rfcrazon.Enabled = False
                        txt_contacto.CssClass = "negrita"
                        txt_razon.CssClass = "negrita"
                        txt_legal.CssClass = "negrita"
                        txt_rfcrazon.CssClass = "negrita"

                        pc_reporte.Enabled = True
                        txt_lugarN.Enabled = True
                        txt_nombre.Enabled = True
                        txt_paterno.Enabled = True
                        txt_materno.Enabled = True
                        txt_rfc.Enabled = True
                        txt_fnacimiento.Enabled = True
                        txt_ocupacion.Enabled = True
                        txt_fnacimiento.Enabled = True
                        txt_nombre.CssClass = "combos_small"
                        txt_paterno.CssClass = "combos_small"
                        txt_materno.CssClass = "combos_small"
                        txt_lugarN.CssClass = "combos_small"
                        txt_rfc.CssClass = "combos_small"
                        txt_fnacimiento.CssClass = "combos_small"
                        txt_ocupacion.CssClass = "combos_small"
                        txt_fnacimiento.CssClass = "combos_small"
                    End If
                    If dt.Rows(0).IsNull("telefono") Then
                        txt_tel.Text = ""
                    Else
                        txt_tel.Text = dt.Rows(0)("telefono")
                    End If
                    If dt.Rows(0).IsNull("datos_empleado") Then
                        txt_empleado.Text = ""
                    Else
                        txt_empleado.Text = dt.Rows(0)("datos_empleado")
                    End If
                    If dt.Rows(0).IsNull("agencia_empleado") Then
                        txt_agencia.Text = ""
                    Else
                        txt_agencia.Text = dt.Rows(0)("agencia_empleado")
                    End If
                    If dt.Rows(0).IsNull("calle_cliente") Then
                        txt_dir.Text = ""
                    Else
                        txt_dir.Text = dt.Rows(0)("calle_cliente")
                    End If
                    If dt.Rows(0).IsNull("ciudad_cliente") Then
                        lbl_provincia.Text = ""
                    Else
                        lbl_provincia.Text = dt.Rows(0)("ciudad_cliente")
                    End If
                    If dt.Rows(0).IsNull("estado_cliente") Then
                        lbl_estado.Text = ""
                    Else
                        lbl_estado.Text = dt.Rows(0)("estado_cliente")
                    End If
                    If Not txt_cp.Text = "" Then
                        txt_col.Text = dt.Rows(0)("colonia_cliente")
                    End If

                    If Not dt.Rows(0).IsNull("NoExterior") Then
                        txt_exterior.Text = dt.Rows(0)("NoExterior")
                    Else
                        txt_exterior.Text = ""
                    End If

                    If Not dt.Rows(0).IsNull("NoInterior") Then
                        txt_interior.Text = dt.Rows(0)("NoInterior")
                    Else
                        txt_interior.Text = ""
                    End If

                End If
            Else
                MsgBox.ShowMessage("El c�digo postal no concuerda con el del cliente")
            End If
            Return bandera
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_informacion() As Integer
        Dim bandera As Integer = 0

        If rb_tipo.SelectedValue <> 2 Then
            If txt_nombre.Text = "" Then
                MsgBox.ShowMessage("El Nombre del Cliente no puede quedar en blanco")
                bandera = 1
            End If
            If txt_paterno.Text = "" Then
                MsgBox.ShowMessage("El Apellido Paterno no puede quedar en blanco")
                bandera = 1
            End If
            If txt_rfc.Text = "" Then
                MsgBox.ShowMessage("El R.F.C. no puede quedar en blanco")
                bandera = 1
            End If
            If txt_fnacimiento.Text = "" Then
                MsgBox.ShowMessage("La fecha de nacimiento no puede quedar en blanco ")
                bandera = 1
            End If
            If DateDiff(DateInterval.Year, CType(txt_fnacimiento.Text, Date), Date.Now) < 18 Then
                MsgBox.ShowMessage("Debes ser mayor de edad para emitir una p�liza")
                bandera = 1
            End If
        Else
            If txt_razon.Text = "" Then
                MsgBox.ShowMessage("La razon social no puede quedar en blanco")
                bandera = 1
            End If
            If txt_legal.Text = "" Then
                MsgBox.ShowMessage("El representante Legal no puede quedar en blanco")
                bandera = 1
            End If
            If txt_contacto.Text = "" Then
                MsgBox.ShowMessage("El Contacto no puede quedar en blanco")
                bandera = 1
            End If
            If txt_rfcrazon.Text = "" Then
                MsgBox.ShowMessage("El R.F.C. no puede quedar en blanco")
                bandera = 1
            End If
        End If
        If Not txt_tel.Text = "" Then
            If txt_tel.Text.Length < 8 Then
                MsgBox.ShowMessage("El n�mero del Tel�fono debe de contener m�nimo 8 digitos")
                bandera = 1
            Else
                If txt_tel.Text.Length > 15 Then
                    MsgBox.ShowMessage("El n�mero del Tel�fono debe de contener como m�ximo 15 digitos")
                    bandera = 1
                End If
            End If
        End If
        If txt_tel.Text = "" Then
            MsgBox.ShowMessage("El Tel�fono no puede quedar en blanco")
            bandera = 1
        End If
        If txt_email.Text = "" Then
            MsgBox.ShowMessage("El correo no puede quedar en blanco")
            bandera = 1
        End If
        If txt_cp.Text = "" Then
            MsgBox.ShowMessage("El C.P. no puede quedar en blanco")
            bandera = 1
        End If
        If txt_dir.Text = "" Then
            MsgBox.ShowMessage("La Direcci�n no puede quedar en blanco")
            bandera = 1
        End If
        If lbl_provincia.Text = "" Then
            MsgBox.ShowMessage("La Ciudad no puede quedar en blanco")
            bandera = 1
        End If
        If txt_col.Text = "" Then
            MsgBox.ShowMessage("La colonia no puede quedar en blanco")
            bandera = 1
        End If
        If lbl_estado.Text = "" Then
            MsgBox.ShowMessage("El Estado no puede quedar en blanco")
            bandera = 1
        End If
        If txt_exterior.Text = "" Then
            MsgBox.ShowMessage("Favor de ingresar del numero exterior")
            bandera = 1
        End If
        If Not txt_email.Text = "" Then
            If Not cvalida.ValidarCorreo(txt_email.Text) Then
                MsgBox.ShowMessage("El correo no es correcto, Verifiquelo")
                bandera = 1
            End If
        End If
        Return bandera
    End Function

    Private Sub txt_fnacimiento_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_fnacimiento.TextChanged
        Dim ban As Integer = 0
        Dim fechas As String
        If Not txt_fnacimiento.Text = "" Then
            Dim fecha() As String = txt_fnacimiento.Text.Split("/")
            fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
            txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
            If Not (Session("programa") = 7 Or Session("programa") = 8) Or Session("programa") = 32 Then
                ban = ccliente.valida_periodo_fnacimiento(txt_fnacimiento.Text, Session("fecha"))
                Select Case ban
                    Case 1
                        MsgBox.ShowMessage("La edad m�nima es de 18 a�os.")
                        txt_fnacimiento.Text = ""
                        txt_rfc.Text = ""
                    Case 2
                        MsgBox.ShowMessage("La edad M�xima es de 69 a�os 11 meses")
                        txt_fnacimiento.Text = ""
                        txt_rfc.Text = ""
                End Select
            End If
        End If
    End Sub

    Public Function valida_fecha(ByVal strfecha As String) As String
        If strfecha.Length = 1 Then
            strfecha = "0" & strfecha
        End If
        Return strfecha
    End Function

    Private Sub pc_reporte_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pc_reporte.SelectionChanged
        Dim ban As Integer = 0
        Dim fechas As String


        If Not txt_fnacimiento.Text = "" Then
            Dim fecha() As String = txt_fnacimiento.Text.Split("/")
            fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
            txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)

            If Not (Session("programa") = 7 Or Session("programa") = 8) Then
                If Session("programa") = 32 Then
                    ban = ccliente.valida_periodo_fnacimiento(txt_fnacimiento.Text, Session("fecha"))
                    Select Case ban
                        Case 1
                            MsgBox.ShowMessage("Edad m�nima es de 18 a�os.")
                            txt_fnacimiento.Text = ""
                        Case 2
                            MsgBox.ShowMessage("Edad M�xima es de 69 a�os 11 meses")
                            txt_fnacimiento.Text = ""
                    End Select
                End If
            End If
        End If
    End Sub

    Public Sub determina_badenra_iva(ByVal strcp As String)
        viewstate("iva") = 0
        Try
            If ccliente.determina_banderas_X_programa(Session("programa"), Session("DatosGenerales")(20), 7) = 1 Then
                Dim i As Integer = 0
                Dim dt As DataTable = ccliente.busqueda_iva(cvalida.QuitaCaracteres(txt_cp.Text))
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        If Not dt.Rows(i).IsNull("iva") Then
                            If dt.Rows(i)("iva") <> 0.16 Then
                                viewstate("iva") = 1
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_buscar_cp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar_cp.Click
        Dim i As Integer = 0
        If txt_cp.Text = "" Then
            MsgBox.ShowMessage("Favor de escribir el codigo postal")
            Exit Sub
        End If
        If Not IsNumeric(txt_cp.Text) Then
            MsgBox.ShowMessage("el codigo postal debe de ser n�merico")
            Exit Sub
        End If
        Try
            Dim dt As DataTable = ccliente.busqueda_iva(cvalida.QuitaCaracteres(txt_cp.Text))
            If dt.Rows.Count > 0 Then
                pnl_dir.Visible = True
                If dt.Rows(0).IsNull("nombre_estado") Then
                    lbl_estado.Text = ""
                Else
                    lbl_estado.Text = dt.Rows(0)("nombre_estado")
                End If
                If dt.Rows(0).IsNull("nombre_provincia") Then
                    lbl_provincia.Text = ""
                Else
                    lbl_provincia.Text = dt.Rows(0)("nombre_provincia")
                End If
                For i = 0 To dt.Rows.Count - 1
                    If Not dt.Rows(i).IsNull("iva") Then
                        If dt.Rows(i)("iva") <> 0.16 Then
                            viewstate("iva") = 1
                            Exit For
                        End If
                    End If
                Next
            Else
                'pnl_dir.Visible = False
                MsgBox.ShowMessage("No existe informaci�n que mostrar")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_limpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_limpiar.Click
        limpia()
        pnl_clientes.Visible = False
        cmd_agregar.Visible = True
        'cmd_modificar.Visible = False
    End Sub

    Private Sub cmd_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cliente.Click
        Try
            txt_tipocliente.Text = 1
            If pnl_clientes.Visible = False Then
                pnl_clientes.Visible = True
                Dim dt As DataTable = ccliente.consulta_cliente(Session("bid"), txt_cliente.Text)
                If dt.Rows.Count > 0 Then
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                    Exit Sub
                End If
            Else
                pnl_clientes.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_agregar.Click
        Dim Dnacimiento As String = ""
        Dim asFecha() As String

        Try
            'If cbo_beneficiario.Visible = True Then
            'txt_beneficiario.Text = cbo_beneficiario.SelectedItem.Text.Trim
            'End If

            If txt_beneficiario.Text.Trim() = String.Empty Then
                MsgBox.ShowMessage("El Beneficiario Preferente es obligatorio.")
            End If
            txt_beneficiario.Text = txt_beneficiario.Text.Trim().ToUpper()

            cmd_buscar_cp_Click(sender, e)
            'evi 28/06/2010
            '''If Not cmd_agregar.Text = "Continuar" Then
            If valida_informacion() = 0 Then
                If rb_tipo.SelectedValue <> 2 Then
                    Dnacimiento = txt_fnacimiento.Text
                End If

                txt_clave.Text = ccliente.inserta_cliente(Session("bid"), _
                    cvalida.QuitaCaracteres(txt_nombre.Text, True, "#"), _
                    cvalida.QuitaCaracteres(txt_paterno.Text, True), _
                    cvalida.QuitaCaracteres(txt_materno.Text, True), _
                    txt_fnacimiento.Text, IIf(txt_rfc.Enabled = True, cvalida.QuitaCaracteres(txt_rfc.Text, True), _
                    cvalida.QuitaCaracteres(txt_rfcrazon.Text, 1)), _
                    "", "", _
                    rb_tipo.SelectedValue, cvalida.QuitaCaracteres(txt_razon.Text, True), _
                    cvalida.QuitaCaracteres(txt_legal.Text, True), _
                    cvalida.QuitaCaracteres(txt_contacto.Text, True), _
                    cvalida.QuitaCaracteres(txt_tel.Text, True), 1, _
                    cvalida.QuitaCaracteres(txt_empleado.Text, True), _
                    cvalida.QuitaCaracteres(txt_agencia.Text, True), _
                    cvalida.QuitaCaracteres(txt_cp.Text, True), _
                    cvalida.QuitaCaracteres(txt_dir.Text, True), _
                    cvalida.QuitaCaracteres(txt_col.Text, True), _
                    cvalida.QuitaCaracteres(lbl_provincia.Text, True), _
                    cvalida.QuitaCaracteres(lbl_estado.Text, True), _
                    Session("fecha"), _
                    cvalida.QuitaCaracteres(txt_beneficiario.Text, True), _
                    cvalida.QuitaCaracteres(txt_email.Text, False), _
                    0, _
                    cvalida.QuitaCaracteres(txt_interior.Text, True), _
                    cvalida.QuitaCaracteres(txt_exterior.Text, True))

                'IIf(cbo_beneficiario.SelectedValue = "", 0, cbo_beneficiario.SelectedValue), _
                MsgBox.ShowMessage("Los datos del cliente se registraron correctamente")
                cmd_agregar.Visible = True
                'cmd_modificar.Visible = True

                Session("contribuyente") = rb_tipo.SelectedValue

                Session("subramo") = Session("SubRamo") 'Session("DatosGenerales")(1)

                Session("cliente") = txt_clave.Text

                If Session("programa") = 30 And Session("Exclusion") = 0 Then
                    txt_clave.Text = ccliente.inserta_cliente(Session("bid"), _
                            "", _
                            "", _
                            "", _
                            "", _
                            "CON080707DD4", _
                            "", "", _
                            2, UCase(txt_beneficiario.Text), _
                            "", _
                            "", _
                            "52.28.70.00", 1, _
                            "", _
                            cvalida.QuitaCaracteres(txt_agencia.Text, True), _
                            "11590", _
                            "CALZ. GRAL. MARIANO ESCOBEDO", _
                            "ANZURES", _
                            "MIGUEL HIDALGO", _
                            "DISTRITO FEDERAL", _
                            Session("fecha"), _
                            UCase(txt_beneficiario.Text), _
                            "", _
                            2, _
                            "PISO 8", _
                            "NO. 498")
                    MsgBox.ShowMessage("Los datos del cliente se registraron correctamente")
                    cmd_agregar.Visible = True
                    'cmd_modificar.Visible = True

                    Session("subramo1") = Session("DatosGenerales1")(1)

                    Session("cliente1") = txt_clave.Text
                Else
                    'evi 16/12/2013 se agrega datos de Conauto
                    If Session("programa") = 29 And Session("Exclusion") = 0 Then
                        txt_clave.Text = ccliente.inserta_cliente(Session("bid"), _
                            "", _
                            "", _
                            "", _
                            "", _
                            "CON890518P51", _
                            "", "", _
                            2, UCase(txt_beneficiario.Text), _
                            "", _
                            "", _
                            "52.28.70.00", 1, _
                            "", _
                            cvalida.QuitaCaracteres(txt_agencia.Text, True), _
                            "11590", _
                            "CALZ. GRAL. MARIANO ESCOBEDO", _
                            "ANZURES", _
                            "MIGUEL HIDALGO", _
                            "DISTRITO FEDERAL", _
                            Session("fecha"), _
                            UCase(txt_beneficiario.Text), _
                            "", _
                            2, _
                            "PISO 8", _
                            "NO. 498")
                        MsgBox.ShowMessage("Los datos del cliente se registraron correctamente")
                        cmd_agregar.Visible = True
                        'cmd_modificar.Visible = True

                        Session("subramo1") = Session("DatosGenerales1")(1)
                        Session("cliente1") = txt_clave.Text
                    End If
                End If

                Dim arrx() As String = Session("ArrPoliza1")
                Dim arrz() As String = Session("ArrPoliza")

                If (Session("programa") = 4 Or Session("programa") = 5 _
                                   Or Session("programa") = 9 Or Session("programa") = 11) And Session("fincon") = "F" Then

                    Dim arrV() As String = txt_vida.Text.Split("/")
                    Dim arrD() As String = txt_desempleo.Text.Split("/")
                    If Session("ClienteCotiza") = 0 Then
                        Response.Redirect("WfrmImp.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
                        'Response.Redirect("wfrmdatosvehiculo.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
                    Else
                        Response.Redirect("WfrmImp.aspx?cveben=" & txt_beneficiario.Text & "")
                        'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
                    End If
                Else
                    Response.Redirect("WfrmImp.aspx?cveben=" & txt_beneficiario.Text & "")
                    'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
                End If
            End If

            ''''cmd_agregar.Text = "Continuar"

            ''''''If (Session("programa") = 4 Or Session("programa") = 5 Or Session("programa") = 9) And Session("fincon") = "C"   Then
            '''''If (Session("programa") = 4 Or Session("programa") = 5 _
            '''''    Or Session("programa") = 9 Or Session("programa") = 11) And Session("fincon") = "F" Then

            '''''    Dim arrV() As String = txt_vida.Text.Split("/")
            '''''    Dim arrD() As String = txt_desempleo.Text.Split("/")
            '''''    If Session("ClienteCotiza") = 0 Then
            '''''        Response.Redirect("WfrmDatosVehiculo.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
            '''''        'Response.Redirect("wfrmdatosvehiculo.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
            '''''    Else
            '''''        Response.Redirect("WfrmDatosVehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''''        'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''''    End If
            '''''Else
            '''''    Response.Redirect("WfrmDatosVehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''''    'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''''End If
            '''End If
            '''Else
            '''    'evi 28/06/2010
            '''    'If (Session("programa") = 4 Or Session("programa") = 5 Or Session("programa") = 9) And Session("fincon") = "C"   Then
            '''    If (Session("programa") = 4 Or Session("programa") = 5 _
            '''        Or Session("programa") = 9 Or Session("programa") = 11) And Session("fincon") = "F" Then

            '''        Dim arrV() As String = txt_vida.Text.Split("/")
            '''        Dim arrD() As String = txt_desempleo.Text.Split("/")
            '''        If Session("ClienteCotiza") = 0 Then
            '''            Response.Redirect("WfrmDatosVehiculo.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
            '''            'Response.Redirect("wfrmdatosvehiculo.aspx?cvevida=" & arrV(1) & "&cvedesempleo=" & arrD(1) & "&cveben=" & txt_beneficiario.Text & "")
            '''        Else
            '''            Response.Redirect("WfrmDatosVehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''            'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''        End If
            '''    Else
            '''        Response.Redirect("WfrmDatosVehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''        'Response.Redirect("wfrmdatosvehiculo.aspx?cveben=" & txt_beneficiario.Text & "")
            '''    End If
            '''End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regresar.Click
        If Session("programa") = 16 Then
            Dim arrV() As String = txt_vida.Text.Split("/")
            Dim arrD() As String = txt_desempleo.Text.Split("/")
            If Session("ClienteCotiza") = 0 Then
                Response.Redirect("WfrmCotizador2.aspx?valcost1=" & Session("precioEscrito") & "&cvevida=" & arrV(0) & "&cvedesempleo=" & arrD(0) & "")
            Else
                Response.Redirect("WfrmCotizador2.aspx?valcost1=" & Session("precioEscrito") & "cvevida=" & txt_vida.Text & "&cvedesempleo=" & txt_desempleo.Text & "")
            End If
        Else
            Response.Redirect("WfrmCotizador2.aspx?valcost1=" & Session("precioEscrito") & "")
        End If
    End Sub

    Private Sub cmd_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar.Click
        Dim dt As DataTable
        If txt_tipocliente.Text = "1" Then
            dt = ccliente.consulta_cliente(Session("bid"), cvalida.QuitaCaracteres(txt_cliente.Text, True))
        Else
            dt = ccliente.Consulta_cliente_prospecto(Session("bid"), cvalida.QuitaCaracteres(txt_cliente.Text, True))
        End If
        Try
            If dt.Rows.Count > 0 Then
                grdcatalogo_fondo.DataSource = dt
                grdcatalogo.DataSource = dt
                grdcatalogo.DataBind()
                grdcatalogo_fondo.DataBind()
                grdcatalogo_fondo.Visible = True
                grdcatalogo.Visible = True
            Else
                grdcatalogo_fondo.Visible = False
                grdcatalogo.Visible = False
                MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub txt_rfcrazon_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_rfcrazon.TextChanged
        Dim strcadena As String = ""
        Try
            strcadena = cvalida.valida_caracteres(txt_rfcrazon.Text, 1, 3, 1)
            If Not strcadena = "" Then
                txt_rfcrazon.Text = ""
                MsgBox.ShowMessage(strcadena)
                Exit Sub
            End If
            strcadena = cvalida.valida_caracteres(txt_rfcrazon.Text, 4, 6, 2)
            If Not strcadena = "" Then
                txt_rfcrazon.Text = ""
                MsgBox.ShowMessage(strcadena)
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub txt_rfc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_rfc.TextChanged

    End Sub

    Private Sub cmd_clientep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_clientep.Click
        Try
            txt_tipocliente.Text = "2"
            If pnl_clientes.Visible = False Then
                pnl_clientes.Visible = True
                Dim dt As DataTable = ccliente.Consulta_cliente_prospecto(Session("bid"), txt_cliente.Text)
                If dt.Rows.Count > 0 Then
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                    Exit Sub
                End If
            Else
                pnl_clientes.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub txt_nombre_TextChanged(sender As Object, e As EventArgs) Handles txt_nombre.TextChanged
        Try
            Dim ban As Integer = 0
            Dim fechas As String
            If Not txt_fnacimiento.Text = "" Then
                Dim fecha() As String = txt_fnacimiento.Text.Split("/")
                fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
                txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
                If Not (Session("programa") = 7 Or Session("programa") = 8) Then
                    If Session("programa") = 32 Then
                        ban = ccliente.valida_periodo_fnacimiento(txt_fnacimiento.Text, Session("fecha"))
                        Select Case ban
                            Case 1
                                MsgBox.ShowMessage("Edad m�nima es de 18 a�os.")
                                txt_fnacimiento.Text = ""
                            Case 2
                                MsgBox.ShowMessage("Edad M�xima es de 69 a�os 11 meses")
                                txt_fnacimiento.Text = ""
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub txt_paterno_TextChanged(sender As Object, e As EventArgs) Handles txt_paterno.TextChanged
        Try
            Dim ban As Integer = 0
            Dim fechas As String
            If Not txt_fnacimiento.Text = "" Then
                Dim fecha() As String = txt_fnacimiento.Text.Split("/")
                fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
                txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
                If Not (Session("programa") = 7 Or Session("programa") = 8) Then
                    If Session("programa") = 32 Then
                        ban = ccliente.valida_periodo_fnacimiento(txt_fnacimiento.Text, Session("fecha"))
                        Select Case ban
                            Case 1
                                MsgBox.ShowMessage("Edad m�nima es de 18 a�os.")
                                txt_fnacimiento.Text = ""
                            Case 2
                                MsgBox.ShowMessage("Edad M�xima es de 69 a�os 11 meses")
                                txt_fnacimiento.Text = ""
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub txt_materno_TextChanged(sender As Object, e As EventArgs) Handles txt_materno.TextChanged
        Try
            Dim ban As Integer = 0
            Dim fechas As String
            If Not txt_fnacimiento.Text = "" Then
                Dim fecha() As String = txt_fnacimiento.Text.Split("/")
                fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
                txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
                If Not (Session("programa") = 7 Or Session("programa") = 8) Then
                    If Session("programa") = 32 Then
                        ban = ccliente.valida_periodo_fnacimiento(txt_fnacimiento.Text, Session("fecha"))
                        Select Case ban
                            Case 1
                                MsgBox.ShowMessage("Edad m�nima es de 18 a�os.")
                                txt_fnacimiento.Text = ""
                            Case 2
                                MsgBox.ShowMessage("Edad M�xima es de 69 a�os 11 meses")
                                txt_fnacimiento.Text = ""
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class

