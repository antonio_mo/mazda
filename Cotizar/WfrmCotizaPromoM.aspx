﻿<%@ Page Async="true" Language="vb" AutoEventWireup="false" CodeBehind="WfrmCotizaPromoM.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotizaPromoM" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <title>WfrmCotizaPromoM</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
     <script language="javascript" src="..\JavaScript\Jscripts.js"></script>
    <link rel="stylesheet" type="text/css" href="..\Css\submodal.css">
    <script language="javascript" type="text/javascript" src="..\JavaScript\submodalsource.js"></script>
    <link rel="stylesheet" type="text/css" href="..\Css\Styles.css">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jsCotizarPromoM.js"></script>
    <script src="../Scripts/jsConautoAonM.js"></script>
    <script language="JScript">

        function redireccion(intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago) {
            var uno = document.getElementById("txt_precio").value;
            var strfecha1 = document.getElementById("txt_fecha").value;
            var cvecontrato1 = 0
            var tipoCarga = 0
            dbprecio = 0
            var path = "WfrmCotizaPromoM.aspx?cveAseg=" + intaseguradora + "&valcost=" + uno + "&cveplazo=" + intplazo + "&cveregion=" + intregion + "&cvepaquete=" + intpaquete + "&subramo=" + subramo + "&contrato=" + cvecontrato1 + "&Recargo=" + IntRecargo + "&NumPago=" + IntNumPago + "&Pago1=" + Pago + "&Pago2=" + SubPago + "&TipoCarga=" + tipoCarga + "&strfecha=" + strfecha1 + ""
            window.location.href = path;
        }
        String.prototype.trim = function () {
            return this.replace(',', '');
        }
        function Cliente(descripcion, modelo, anio, plazo, plazototal, marca, persona, estatus) {

            var precio = document.getElementById("txt_precio").value;
            var strfecha1 = document.getElementById("txt_fecha").value;
            var x = "WfrmImpCotiza.aspx?idaonh=" + descripcion + "&strmodelo=" + modelo + "&stranio=" + anio + "&dbSA=" + precio + "&idplazo=" + plazo + "&plazorestante=" + plazototal + "&strmarca=" + marca + "&strpersona=" + persona + "&strestatus=" + estatus + "&strfecha=" + strfecha1 + ""
            initPopUp();
            showPopWin(x, 600, 350, '<font color="#FFFFFF" face=arial><b>Datos del Cliente</b></font>', null);
        }      
        function Cobertura(intaseguradora, intpaquete, strModelo, strCp, strSumaAsegurada) {
            var x = "WfrmDetalleCobertura.aspx?cveAseg=" + intaseguradora + "&cvepaquete=" + intpaquete + "&Modelo=" + strModelo + "&CodigoPostal=" + strCp + "&SumaAsegurada=" + strSumaAsegurada            
            initPopUp();
            showPopWin(x, 600, 350, '<font color="#FFFFFF" face=arial><b>Detalle de la cobertura</b></font>', null);
        }
    </script>
</head>
<body ms_positioning="GridLayout">

    <form id="Form1" method="post" runat="server">

        <asp:HiddenField runat="server" ID="pathContext" />

        <hr style="z-index: 101; position: absolute; top: 40px; left: 8px" class="lineas" width="100%">
        <table style="z-index: 102; position: absolute; top: 48px; left: 8px" id="Table2" border="0"
            cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td class="obligatorio" align="right">Automovil :</td>
                <td>
                    <asp:Label ID="lbl_tipo" runat="server" Width="100%" CssClass="negrita">Nuevo</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td class="negrita" align="right">
                    <asp:HyperLink Style="z-index: 0" ID="hp_coti1" runat="server" CssClass="normal" Visible="False">Guarda&nbsp;cotización</asp:HyperLink>&nbsp;
						<asp:HyperLink Style="z-index: 0" ID="hp_coti" runat="server" CssClass="negrita" Visible="False"
                            ImageUrl="..\Imagenes\Menu\disk.png">Comentarios</asp:HyperLink></td>
                <td>
                    <asp:Label ID="lbl_cotizacion" runat="server" Width="100%" CssClass="obligatorio" Visible="False"></asp:Label></td>
            </tr>
            
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label ID="lblMarca" runat="server" CssClass="Obligatorio" Text="Marca :"></asp:Label>
                </td>
                <td>
                    <div class="m_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_marca" disabled="disabled" class="combos_small cbAonCbCotiza" sel="1" targetcontrolid="cbo_modelo:m_c:t_p_c" style="width: 100%; margin-top: 0px;" />
                </td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td class="negrita" align="right"></td>
                <td></td>
            </tr>
            
            <tr>
                <td class="obligatorio" height="20" align="right"></td>
                <td height="20"></td>
                <td style="width: 164px" class="obligatorio" height="20" align="right">&nbsp;</td>
                <td class="negrita" height="20" align="right"></td>
                <td height="20"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="obligatorio">
                    <asp:HyperLink Style="z-index: 0" ID="Hyperlink2" runat="server" CssClass="normal">Auto Entregado</asp:HyperLink></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td valign="top" rowspan="6">
                    <asp:Table Style="z-index: 0" ID="tb_seguroC" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Tipo :</td>
                <td>
                    <div class="t_p" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" ID="Image3" />
                    </div>
                    <select id="cbo_modelo" class="combos_small cbAonCbCotiza" style="width: 100%;" name="D1" targetcontrolid="cbo_anio:t_p_c:m_l_c" />

                </td>
                <td style="width: 164px; height: 20px" class="obligatorio" align="right"></td>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Modelo :</td>
                <td>
                    <div class="m_l" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" ID="Image4" />
                    </div>
                    <select id="cbo_anio" class="combos_small cbAonCbCotiza" style="width: 100%;" name="D2" targetcontrolid="cbo_descripcion:m_l_c:d_s_c" />

                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Descripción :</td>
                <td style="width: 436px; height: 22px" colspan="2">
                    <div class="d_s_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_descripcion" class="combos_small cbAonCbCotiza" style="width: 100%;" name="D3" targetcontrolid="cbo_uso:d_s_c:u_s_c" />

                <td>

                </td>
                <td style="height: 22px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label Style="z-index: 0" ID="Label1" runat="server">Uso :</asp:Label></td>
                <td>
                    <div class="u_s" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" ID="Image5" />
                    </div>
                    <select id="cbo_uso" class="combos_small cbAonCbCotiza" style="width: 100%;" name="D4" targetcontrolid="cbo_uso:u_s_c:" />

                </td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Precio de la unidad :</td>
                <td colspan="2">
                    <asp:TextBox Style="z-index: 0" ID="txt_precio" runat="server" Width="15%" CssClass="Texto_Cantidad" maxLength="7"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="REVtxtPrecio" runat="server" ControlToValidate="txt_precio" ErrorMessage="*Se deben ingresar solo números" ValidationExpression="[0-9]{5,7}"></asp:RegularExpressionValidator>
                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Tipo contribuyente :</td>
                <td style="width: 436px" colspan="2">
                    <asp:RadioButtonList ID="rb_tipo" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="3"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
                        <asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
                        <asp:ListItem Value="2">Moral</asp:ListItem>
                    </asp:RadioButtonList></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="center"></td>
                <td style="width: 164px" class="negrita" align="left">
                    &nbsp;</td>
                <td></td>
                <td></td>
                <td valign="top" rowspan="6">
                    <asp:Table Style="z-index: 0" ID="tb_seguro" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Código Postal</td>
                <td>
                    <asp:TextBox Style="z-index: 0" ID="txtCodigoPostal" onkeypress="javascript:onlyDigits(event,'decOK');"
                        runat="server" Width="30%" CssClass="Texto_Cantidad" MaxLength="5" AutoPostBack="True"></asp:TextBox>
                &nbsp;
                    <asp:Label ID="lblCodigoPostal" runat="server" Width="55%" CssClass="negrita"></asp:Label>
                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td>
                    <asp:RadioButtonList Style="z-index: 0" ID="rb_promocion" runat="server" Width="100%" CssClass="combos_small"
                        AutoPostBack="True" RepeatColumns="3" RepeatDirection="Horizontal" Visible="False">
                        <asp:ListItem Value="0" Enabled="False">Seguro Gratis</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">Multianual</asp:ListItem>
                    </asp:RadioButtonList></td>                
            </tr>
            <tr>
                <td class="obligatorio" align="right">&nbsp;</td>
                <td colspan="2" style="width: 436px; height: 22px">
                    <div class="d_s" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <asp:RegularExpressionValidator ID="REVtxtCP" runat="server" ControlToValidate="txtCodigoPostal" ValidationExpression="[0-9]{5}"></asp:RegularExpressionValidator>
                    </td>
                
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Plazos&nbsp;contratado :</td>
                <td class="negrita">
                    <asp:DropDownList Style="z-index: 0" ID="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:DropDownList><asp:Label Style="z-index: 0" ID="Label6" runat="server" CssClass="negrita">Meses</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td class="obligatorio" align="right">Fecha de&nbsp;entrega&nbsp;:</td>
                <td class="negrita">
                    <asp:TextBox Style="z-index: 0" ID="txt_fecha" runat="server" Width="45%" CssClass="combos_small"
                        AutoPostBack="True" MaxLength="10" ReadOnly="True"></asp:TextBox>&nbsp;&nbsp;
						<rjs:PopCalendar Style="z-index: 0" ID="pc_reporte" runat="server" Width="8px" AutoPostBack="False"
                            To-Increment="4" Height="8px" To-Today="True" BorderColor="Black" BorderWidth="1px" BackColor="Yellow"
                            TextMessage="La fecha es Incorrecta" Buttons="[<][m][y]  [>]" Culture="es-MX Español (México)"
                            RequiredDateMessage="La Fecha es Requerida" Fade="0.5" Move="True" ShowWeekend="True" Shadow="True"
                            InvalidDateMessage="Día Inválido" BorderStyle="Solid" Separator="/" Control="txt_fecha" ShowErrorMessage="False"
                            From-Today="True"></rjs:PopCalendar>
                </td>
                <td style="width: 164px" align="right"></td>
                <td>&nbsp;&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td style="height: 20px" class="obligatorio" align="right"></td>
                <td style="height: 20px" class="negrita"></td>
                <td style="width: 164px; height: 20px" align="left">
                    <asp:Button Style="z-index: 0" ID="cmd_CalPagos" runat="server" CssClass="boton" Text="Calcular Pagos"></asp:Button>
                    <%--<asp:button style="Z-INDEX: 0" id="btnCalPagos" runat="server" CssClass="boton" Text="Calcular Pagos 2"></asp:button>--%>
                </td>
                <td style="height: 20px"></td>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" class="normal" align="right">&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" class="normal" align="right">&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:Table ID="tb_aseguradora" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                    <cc1:MsgBox Style="z-index: 0" ID="MsgBox" runat="server"></cc1:MsgBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td>
                    <asp:RadioButtonList ID="rb_seguro" runat="server" Width="100%" CssClass="combos_small" Visible="False"
                        AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">Financiado</asp:ListItem>
                        <asp:ListItem Value="1">Contado</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td width="15%"></td>
                <td width="25%"></td>
                <td style="width: 164px" width="15%"></td>
                <td width="20%"></td>
                <td width="25%"></td>
            </tr>
        </table>
        <table style="z-index: 103; position: absolute; height: 24px; top: 8px; left: 8px" id="Table4"
            border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="negrita_marco_color" height="20" valign="middle" width="2%" align="center">
                    <asp:Image ID="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:Image></td>
                <td class="Interiro_tabla" width="98%" colspan="3"><font style="z-index: 0" size="0">&nbsp; 
							<font size="+0">Emisor de Pólizas</font></font></td>
            </tr>
        </table>

    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.cbAonCbCotiza').first().cotiza('fill', { dtValues: { p: 'f_l' } }).end();
        });</script>
</body>
</html>

