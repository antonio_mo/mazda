<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotiza.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotiza" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table2" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" align="right">Automovil :</TD>
					<TD>
						<asp:Label id="lbl_tipo" runat="server" CssClass="negrita" Width="100%">Nuevo</asp:Label></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Marca :</TD>
					<TD>
						<asp:dropdownlist id="cbo_marca" runat="server" CssClass="combos_small" AutoPostBack="True" Width="100%"></asp:dropdownlist></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Tipo :</TD>
					<TD>
						<asp:dropdownlist id="cbo_modelo" runat="server" CssClass="combos_small" AutoPostBack="True" Width="100%"></asp:dropdownlist></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Modelo :</TD>
					<TD>
						<asp:dropdownlist id="cbo_anio" runat="server" CssClass="combos_small" AutoPostBack="True" Width="100%"></asp:dropdownlist></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Descripci�n :
					</TD>
					<TD colSpan="3">
						<asp:dropdownlist id="cbo_descripcion" runat="server" CssClass="combos_small" AutoPostBack="True"
							Width="100%"></asp:dropdownlist></TD>
					<TD style="HEIGHT: 29px"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Uso :</TD>
					<TD>
						<asp:dropdownlist id="cbo_uso" runat="server" CssClass="combos_small" Width="100%"></asp:dropdownlist></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Tipo contribuyente :</TD>
					<TD colSpan="2">
						<asp:radiobuttonlist id="rb_tipo" runat="server" CssClass="combos_small" RepeatDirection="Horizontal"
							Width="100%" RepeatColumns="3">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Precio de la unidad :</TD>
					<TD>
						<asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_precio" runat="server"
							CssClass="Texto_Cantidad" Width="50%" MaxLength="8"></asp:textbox></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right">
						<asp:button id="cmd_continuar" runat="server" CssClass="boton" Text="Cotizar"></asp:button></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
					<TD width="20%"></TD>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table4" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Emisor de 
							P�lizas de Contado</FONT></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; LEFT: 312px; POSITION: absolute; TOP: 416px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
