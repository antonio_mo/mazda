Imports CN_Negocios

Partial Class WfrmUCobertura
    Inherits System.Web.UI.UserControl

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dth As DataTable
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            Dim dt As DataTable = ccliente.carga_vehiculo_homologado(Session("vehiculo"), Session("programa"), Session("anio"), Session("EstatusV"))
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("estatus") Then
                    ViewState("estatus") = ""
                Else
                    ViewState("estatus") = dt.Rows(0)("estatus")
                End If
                If dt.Rows(0).IsNull("catalogo") Then
                    ViewState("catalogo") = ""
                Else
                    ViewState("catalogo") = dt.Rows(0)("catalogo")
                End If
            End If


            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), ViewState("estatus"))
            If dth.Rows.Count > 0 Then
                If dth.Rows.Count > 1 Then
                    'paneles contado
                    If Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13 Then
                        carga_datos_cob_homologado()
                    Else
                        carga_datos_cob()
                    End If
                Else
                    'paneles contado
                    If Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13 Then
                        carga_datos_cob_unico_homologada()
                    Else
                        carga_datos_cob_unico()
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_cob_homologado()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim hp As HyperLink
        Dim hpI As HyperLink
        Dim lbl As Label
        Dim totalceldas As Integer = 6
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim ArrcoberturaRep() As String
        Dim NombrePaquete As String = ""
        Dim NombrePaqueteaux As String = ""

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))
            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    objcell.Text = "COBERTURAS DE CONTADO"
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                'objcell.Text = "COBERTURAS"
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                contador = contador + 1
                For i = contador To totalceldas - 1
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)
            End If

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), Session("programa"), Session("vehiculo"), 0, 0, 0, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        NombrePaqueteaux = ""
                    Else
                        NombrePaqueteaux = dt.Rows(j)("descripcion_paquete")
                    End If
                    If NombrePaquete <> NombrePaqueteaux Then
                        NombrePaquete = NombrePaqueteaux
                        objrow = New TableRow
                        objcell = New TableCell
                        objcell.Text = "COBERTURA"
                        objcell.CssClass = "Combos_small_celda"
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)

                        objcell = New TableCell
                        objcell.Text = NombrePaqueteaux
                        objcell.CssClass = "Combos_small_celda"
                        objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)

                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        objcell.ColumnSpan = totalceldas - (dth.Rows.Count + 1)
                        objrow.Controls.Add(objcell)

                        tb.Controls.Add(objrow)

                        Dim dtec As DataTable
                        'dtec = ccliente.encabezado_coberturas_inicio_homologada(viewstate("estatus"), dt.Rows(j)("id_paquete"), Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                        dtec = ccliente.encabezado_coberturas_inicio_homologada(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                        If dtec.Rows.Count > 0 Then
                            ReDim ArrcoberturaRep(dtec.Rows.Count - 1)
                            For i = 0 To dtec.Rows.Count - 1
                                objrow = New TableRow
                                objcell = New TableCell
                                hp = New HyperLink
                                hpI = New HyperLink
                                lbl = New Label
                                If dtec.Rows(i).IsNull("cobertura") Then
                                    strdescrcobertura = ""
                                    'objcell.Text = ""
                                    hp.Text = ""
                                    hpI.Text = ""
                                Else
                                    ''''strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    strdescrcobertura = dtec.Rows(i)("cobertura")
                                    If ArrcoberturaRep.LastIndexOf(ArrcoberturaRep, strdescrcobertura) < 0 Then
                                        ''''objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                        'objcell.Text = dtec.Rows(i)("cobertura")
                                        hp.Text = dtec.Rows(i)("cobertura") & "&nbsp;&nbsp;&nbsp;"
                                        If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales") Then
                                            lbl.Text = "&nbsp;&nbsp;&nbsp;" & "5%"
                                        End If
                                        If strdescrcobertura = "Robo Total" Then
                                            lbl.Text = "&nbsp;&nbsp;&nbsp;" & "10%"
                                        End If
                                        If Not dtec.Rows(i).IsNull("toltip") Then
                                            hpI.ImageUrl = "..\Imagenes\Menu\add.png"
                                            hp.ToolTip = dtec.Rows(i)("toltip")
                                            hpI.ToolTip = dtec.Rows(i)("toltip")
                                            hp.Style.Add("cursor", "pointer")
                                            hpI.Style.Add("cursor", "pointer")
                                        End If
                                        ArrcoberturaRep(i) = strdescrcobertura
                                    Else
                                        GoTo salta
                                    End If
                                End If
                                objcell.Controls.Add(hp)
                                objcell.Controls.Add(hpI)
                                If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                    objcell.Controls.Add(lbl)
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Left
                                objrow.Controls.Add(objcell)
                                Dim dtc As DataTable
                                For k = 0 To arraseg.Length - 1
                                    dtc = ccliente.carga_coberturas_inicio_homologada(viewstate("estatus"), _
                                        Session("version"), dt.Rows(j)("id_paquete"), _
                                        Session("bid"), Session("cotizacionP"), _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), _
                                        Session("vehiculo"))
                                    objcell = New TableCell
                                    If dtc.Rows.Count > 0 Then
                                        If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                            objcell.Text = "-"
                                        Else
                                            If Not dtc.Rows(0).IsNull("deducible") Then
                                                If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                    Dim deduc As Double = 0
                                                    Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                    If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") And viewstate("estatus") = "N" Then
                                                        Select Case ccliente.banfactura
                                                            Case 0
                                                                dbdedu = dbdedu * 100
                                                                'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                            Case 1
                                                                dbvalor = Session("precioEscrito")
                                                                objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                            Case 2
                                                                'paneles contado
                                                                If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                                    'otrs
                                                                    dbdedu = dbdedu * 100
                                                                    If ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                                        deduc = ccliente.determinando_deducpl_panel(arraseg(k), _
                                                                            dt.Rows(j)("id_paquete"), viewstate("estatus"), _
                                                                            IIf(Session("catalogo") = "0", viewstate("catalogo"), Session("catalogo")), Session("anio"), _
                                                                            Session("vehiculo"))
                                                                        'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%, " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & ", " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                    Else
                                                                        'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                                    End If
                                                                Else
                                                                    'contado Paneles
                                                                    If Session("EstatusV") = "N" Then
                                                                        dbvalor = Session("precioEscrito")
                                                                        objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                                    Else
                                                                        objcell.Text = "Valor Comercial"
                                                                    End If
                                                                End If
                                                        End Select
                                                    Else
                                                        'If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                        If Not (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                                            If dbdedu < 1 Then
                                                                dbdedu = dbdedu * 100
                                                                'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                                objcell.Text = Math.Round(dbdedu, 2) & "%"
                                                            Else
                                                                'dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                                objcell.Text = Math.Round(dbdedu, 2)
                                                            End If
                                                        Else
                                                            If Session("EstatusV") = "U" Then
                                                                objcell.Text = "Valor Comercial"
                                                            Else
                                                                objcell.Text = "-"
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                        dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                        objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                    End If
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        End If
                                    Else
                                        objcell.Text = "-"
                                    End If
                                    objcell.CssClass = "dt_celda"
                                    objcell.VerticalAlign = VerticalAlign.Middle
                                    objcell.HorizontalAlign = HorizontalAlign.Center
                                    objrow.Controls.Add(objcell)
                                Next
                                objcell = New TableCell
                                objcell.Text = ""
                                objcell.CssClass = "negrita"
                                objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                                objrow.Controls.Add(objcell)
                                tb.Controls.Add(objrow)
salta:
                            Next
                        End If
                    Else
                        NombrePaquete = NombrePaqueteaux
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_cob_unico_homologada()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 5
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim celdas As Integer
        Dim nombrePaqueteAux As String = ""
        Dim nombrePaquete As String = ""
        Dim cuenta As Integer = 0
        'Dim strpaquete As String = ""
        Dim totalpaquetes As Integer = 0
        Dim hp As HyperLink
        Dim hpI As HyperLink
        Dim lbl As Label

        tb.BorderWidth = Unit.Pixel(1)
        tb.GridLines = GridLines.Both

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))

            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    If Session("programa") = 9 Then
                        objcell.Text = "COBERTURAS MULTIANUAL"
                    Else
                        objcell.Text = "COBERTURAS DE CONTADO"
                        'objcell.Text = "COBERTURAS"
                    End If
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                    'objcell.Text = "COBERTURAS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                celdas = ccliente.cuenta_paquete_aseguradora_subramo_inicio(Session("bid"), Session("idgrdcontrato"))

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(celdas * 18.75)
                    objcell.ColumnSpan = celdas
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                For i = contador To totalceldas - 1 - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(25)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                    cuenta = cuenta + i
                Next
                tb.Controls.Add(objrow)
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = "COBERTURA"
            objcell.CssClass = "Combos_small_celda"
            objcell.VerticalAlign = VerticalAlign.Middle
            objcell.HorizontalAlign = HorizontalAlign.Center
            objrow.Controls.Add(objcell)

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), _
                    Session("programa"), Session("vehiculo"), Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                Dim ArrNomPaquete(dt.Rows.Count - 1, 1) As String
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        nombrePaqueteAux = ""
                    Else
                        nombrePaqueteAux = dt.Rows(j)("descripcion_paquete")
                        ArrNomPaquete(j, 0) = nombrePaqueteAux
                        ArrNomPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    End If
                    If nombrePaquete <> nombrePaqueteAux Then
                        objcell = New TableCell
                        objcell.Text = nombrePaqueteAux
                        objcell.CssClass = "Combos_small_celda"
                        'objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)
                        nombrePaquete = nombrePaqueteAux
                        totalpaquetes = totalpaquetes + 1
                    End If
                Next
                'For i = 0 To totalceldas - dt.Rows.Count - celdas
                For i = 0 To cuenta
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)

                nombrePaqueteAux = ""
                If dt.Rows.Count > 0 Then
                    Dim dtec As DataTable
                    'strpaquete
                    'dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                    dtec = ccliente.encabezado_coberturas_inicio_homologada(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                    If dtec.Rows.Count > 0 Then
                        Dim arrLimresp(dtec.Rows.Count - 1) As String
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            hp = New HyperLink
                            hpI = New HyperLink
                            lbl = New Label
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                                'objcell.Text = ""
                                hp.Text = ""
                                hpI.Text = ""
                            Else
                                If Session("programa") = 9 Then
                                    strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    objcell.Text = strdescrcobertura
                                Else
                                    strdescrcobertura = dtec.Rows(i)("cobertura")
                                    'objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    hp.Text = dtec.Rows(i)("cobertura") & "&nbsp;&nbsp;&nbsp;"
                                    If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales") Then
                                        lbl.Text = "&nbsp;&nbsp;&nbsp;" & "5%"
                                    End If
                                    If strdescrcobertura = "Robo Total" Then
                                        lbl.Text = "&nbsp;&nbsp;&nbsp;" & "10%"
                                    End If
                                    If Not dtec.Rows(i).IsNull("toltip") Then
                                        hpI.ImageUrl = "..\Imagenes\Menu\add.png"
                                        hp.ToolTip = dtec.Rows(i)("toltip")
                                        hpI.ToolTip = dtec.Rows(i)("toltip")
                                        hp.Style.Add("cursor", "pointer")
                                        hpI.Style.Add("cursor", "pointer")
                                    End If
                                End If
                                If arrLimresp.LastIndexOf(arrLimresp, strdescrcobertura) < 0 Then
                                    arrLimresp(i) = strdescrcobertura
                                Else
                                    GoTo salta
                                End If
                            End If
                            objcell.Controls.Add(hp)
                            objcell.Controls.Add(hpI)
                            If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                objcell.Controls.Add(lbl)
                            End If
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)


                            Dim dtc As DataTable
                            For j = 0 To (ArrNomPaquete.Length / 2) - 1
                                dtc = ccliente.carga_coberturas_inicio_homologada(viewstate("estatus"), _
                                    Session("version"), ArrNomPaquete(j, 1), _
                                    Session("bid"), Session("cotizacionP"), _
                                    arraseg(k), strdescrcobertura, ArrNomPaquete(j, 0), _
                                    Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), _
                                    Session("vehiculo"))
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim deduc As Double = 0
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") _
                                                    And viewstate("estatus") = "N" Then
                                                    Select Case ccliente.banfactura
                                                        Case 0
                                                            dbdedu = dbdedu * 100
                                                            'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                        Case 1
                                                            dbvalor = Session("precioEscrito")
                                                            objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                        Case 2
                                                            'paneles contado
                                                            If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                                'otrs
                                                                dbdedu = dbdedu * 100
                                                                If ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                                    deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                                            dt.Rows(j)("id_paquete"), _
                                                                            Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                                            1, Session("anio"), _
                                                                            Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                                    'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%, " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo") & ", " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                Else
                                                                    'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                                End If
                                                            Else
                                                                'contado Paneles
                                                                If Session("EstatusV") = "N" Then
                                                                    dbvalor = Session("precioEscrito")
                                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                                Else
                                                                    objcell.Text = "Valor Comercial"
                                                                End If
                                                            End If
                                                    End Select
                                                Else
                                                    'If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                    If Not (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                                        If dbdedu < 1 Then
                                                            dbdedu = dbdedu * 100
                                                            'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                            objcell.Text = Math.Round(dbdedu, 2) & "%"
                                                        Else
                                                            'dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                            objcell.Text = Math.Round(dbdedu, 2)
                                                        End If
                                                    Else
                                                        If Session("EstatusV") = "U" Then
                                                            objcell.Text = "Valor Comercial"
                                                        Else
                                                            objcell.Text = "-"
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.#0")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                            Next
                            tb.Controls.Add(objrow)
salta:
                        Next
                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        'objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                        objrow.Controls.Add(objcell)
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_cob_unico()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 5
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim celdas As Integer
        Dim deduc As Double = 0
        Dim nombrePaqueteAux As String = ""
        Dim nombrePaquete As String = ""
        Dim cuenta As Integer = 0
        'Dim strpaquete As String = ""
        Dim totalpaquetes As Integer = 0

        tb.BorderWidth = Unit.Pixel(1)
        tb.GridLines = GridLines.Both

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))

            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    If Session("programa") = 9 Then
                        objcell.Text = "COBERTURAS MULTIANUAL"
                    Else
                        objcell.Text = "COBERTURAS DE CONTADO"
                    End If
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                celdas = ccliente.cuenta_paquete_aseguradora_subramo_inicio(Session("bid"), Session("idgrdcontrato"))

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(celdas * 18.75)
                    objcell.ColumnSpan = celdas
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                For i = contador To totalceldas - 1 - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(25)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                    cuenta = cuenta + i
                Next
                tb.Controls.Add(objrow)
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = "COBERTURA"
            objcell.CssClass = "Combos_small_celda"
            objcell.VerticalAlign = VerticalAlign.Middle
            objcell.HorizontalAlign = HorizontalAlign.Center
            objrow.Controls.Add(objcell)

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), _
                    Session("programa"), Session("vehiculo"), Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                Dim ArrNomPaquete(dt.Rows.Count - 1, 1) As String
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        nombrePaqueteAux = ""
                    Else
                        nombrePaqueteAux = dt.Rows(j)("descripcion_paquete")
                        ArrNomPaquete(j, 0) = nombrePaqueteAux
                        ArrNomPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    End If
                    'If strpaquete = "" Then
                    '    strpaquete = dt.Rows(j)("id_paquete")
                    '    ArrPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    'Else
                    '    strpaquete = strpaquete & "," & dt.Rows(j)("id_paquete")
                    '    ArrPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    'End If
                    If nombrePaquete <> nombrePaqueteAux Then
                        objcell = New TableCell
                        objcell.Text = nombrePaqueteAux
                        objcell.CssClass = "Combos_small_celda"
                        'objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)
                        nombrePaquete = nombrePaqueteAux
                        totalpaquetes = totalpaquetes + 1
                    End If
                Next
                'For i = 0 To totalceldas - dt.Rows.Count - celdas
                For i = 0 To cuenta
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)

                nombrePaqueteAux = ""
                If dt.Rows.Count > 0 Then
                    Dim dtec As DataTable
                    'strpaquete
                    dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"), Session("Tipocarga"))
                    If dtec.Rows.Count > 0 Then
                        Dim arrLimresp(dtec.Rows.Count - 1) As String
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                                objcell.Text = ""
                            Else
                                If Session("programa") = 9 Then
                                    strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    objcell.Text = strdescrcobertura
                                Else
                                    strdescrcobertura = dtec.Rows(i)("cobertura")
                                    objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                End If
                                If arrLimresp.LastIndexOf(arrLimresp, strdescrcobertura) < 0 Then
                                    arrLimresp(i) = strdescrcobertura
                                Else
                                    GoTo salta
                                End If
                            End If
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)

                            'Dim arrPaquete() As String = Split(Session("PaqSelCon"), ",")

                            Dim dtc As DataTable
                            'For j = 0 To IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), totalpaquetes, dt.Rows.Count) - 1

                            'For j = 0 To arrPaquete.Length - 1
                            For j = 0 To (ArrNomPaquete.Length / 2) - 1

                                '''''For j = 0 To totalpaquetes - 1
                                'If Not ArrPaquete(j, 1) Is Nothing Then
                                'Dim dr As DataRow() = dt.Select("id_paquete ='" & ArrPaquete(j, 1) & "'")
                                'If nombrePaqueteAux <> dr(0)("descripcion_paquete") Then
                                'If Session("Programa") = 9 Then
                                'If totalpaquetes > 1 Or Session("Programa") = 9 Then
                                '    nombrePaqueteAux = dr(0)("descripcion_paquete")
                                'End If

                                'dt.Rows(i)("id_paquete")
                                'dt.Rows(i)("descripcion_paquete")

                                'dtc = ccliente.carga_coberturas_inicio(viewstate("estatus"), _
                                'Session("version"), _
                                'IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), strpaquete, dr(0)("id_paquete")), _
                                'Session("bid"), Session("cotizacionP"), _
                                'arraseg(k), strdescrcobertura, dr(0)("descripcion_paquete"), _
                                'Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                dtc = ccliente.carga_coberturas_inicio(viewstate("estatus"), _
                               Session("version"), ArrNomPaquete(j, 1), _
                               Session("bid"), Session("cotizacionP"), _
                               arraseg(k), strdescrcobertura, ArrNomPaquete(j, 0), _
                               Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("idgrdcontrato"))
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                dbdedu = dbdedu * 100
                                                If Session("intmarcabid") = 1 Then
                                                    If (strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Da�os Materiales") And _
                                                        ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                        'deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                        'IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), strpaquete, dr(0)("id_paquete")), _
                                                        '    Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                        '    1, Session("anio"), _
                                                        '    Session("vehiculo"), Session("resultado")(5))
                                                        deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                            dt.Rows(j)("id_paquete"), _
                                                            Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                            1, Session("anio"), _
                                                            Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                        If Session("tipopol") = "M" Or Session("tipopol") = "F" Then
                                                            Dim strdescripcion As String
                                                            Select Case dtc.Rows(0)("descripcion_tipo")
                                                                Case "Auto Nuevo"
                                                                    strdescripcion = "6"
                                                                Case "Valor Comercial"
                                                                    strdescripcion = "7"
                                                                Case "Valor Factura"
                                                                    strdescripcion = "7"
                                                            End Select
                                                            If dbdedu > 0 Then
                                                                objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%, " & strdescripcion & "% Deducible"
                                                            Else
                                                                objcell.Text = dtc.Rows(0)("descripcion_tipo") & " 0%, " & strdescripcion & "% Deducible"
                                                            End If
                                                        Else
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & " Deducible " & dbdedu & "% PT, " & Format(deduc, "##,##0") & "% PP" ' Deducible
                                                        End If
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                    End If
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.#0")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                                'Else
                                'nombrePaqueteAux = dr(0)("descripcion_paquete")
                                'End If
                                'End If
                            Next
                            tb.Controls.Add(objrow)
salta:
                        Next
                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        'objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                        objrow.Controls.Add(objcell)
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'emmanuell
    Public Sub carga_datos_cob()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 6
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim ArrcoberturaRep() As String
        Dim NombrePaquete As String = ""
        Dim NombrePaqueteaux As String = ""
        Dim deduc As Double = 0

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))
            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    objcell.Text = "COBERTURAS DE CONTADO"
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                contador = contador + 1
                For i = contador To totalceldas - 1
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)
            End If

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), Session("programa"), Session("vehiculo"), 0, 0, 0, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        NombrePaqueteaux = ""
                    Else
                        NombrePaqueteaux = dt.Rows(j)("descripcion_paquete")
                    End If
                    If NombrePaquete <> NombrePaqueteaux Then
                        NombrePaquete = NombrePaqueteaux
                        objrow = New TableRow
                        objcell = New TableCell
                        objcell.Text = "COBERTURA"
                        objcell.CssClass = "Combos_small_celda"
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)

                        objcell = New TableCell
                        objcell.Text = NombrePaqueteaux
                        objcell.CssClass = "Combos_small_celda"
                        objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)

                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        objcell.ColumnSpan = totalceldas - (dth.Rows.Count + 1)
                        objrow.Controls.Add(objcell)

                        tb.Controls.Add(objrow)

                        Dim dtec As DataTable
                        'dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), dt.Rows(j)("id_paquete"), Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                        dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"), Session("Tipocarga"))
                        If dtec.Rows.Count > 0 Then
                            ReDim ArrcoberturaRep(dtec.Rows.Count - 1)
                            For i = 0 To dtec.Rows.Count - 1
                                objrow = New TableRow
                                objcell = New TableCell
                                If dtec.Rows(i).IsNull("cobertura") Then
                                    strdescrcobertura = ""
                                    objcell.Text = ""
                                Else
                                    strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    If ArrcoberturaRep.LastIndexOf(ArrcoberturaRep, strdescrcobertura) < 0 Then
                                        objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                        ArrcoberturaRep(i) = strdescrcobertura
                                    Else
                                        GoTo salta
                                    End If
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Left
                                objrow.Controls.Add(objcell)

                                Dim dtc As DataTable
                                For k = 0 To arraseg.Length - 1
                                    'Session("version"), dt.Rows(j)("id_paquete"), _
                                    dtc = ccliente.carga_coberturas_inicio(viewstate("estatus"), _
                                        Session("version"), Session("PaqSelCon"), _
                                        Session("bid"), Session("cotizacionP"), _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("idgrdcontrato"))
                                    objcell = New TableCell
                                    If dtc.Rows.Count > 0 Then
                                        If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                            objcell.Text = "-"
                                        Else
                                            If Not dtc.Rows(0).IsNull("deducible") Then
                                                If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                    Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                    dbdedu = dbdedu * 100
                                                    'Session("subramo"), Session("programa"), viewstate("estatus"), _

                                                    If (strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Da�os Materiales") And _
                                                        ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                        deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                                dtc.Rows(j)("id_paquete"), _
                                                                dtc.Rows(j)("subramo"), Session("programa"), viewstate("estatus"), _
                                                                1, Session("anio"), _
                                                                Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & ", " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "% PT, " & deduc & "% PP" 'Deducible
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                    End If

                                                Else
                                                    If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                        dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                        objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                    End If
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        End If
                                    Else
                                        objcell.Text = "-"
                                    End If
                                    objcell.CssClass = "dt_celda"
                                    objcell.VerticalAlign = VerticalAlign.Middle
                                    objcell.HorizontalAlign = HorizontalAlign.Center
                                    objrow.Controls.Add(objcell)
                                Next
                                objcell = New TableCell
                                objcell.Text = ""
                                objcell.CssClass = "negrita"
                                objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                                objrow.Controls.Add(objcell)
                                tb.Controls.Add(objrow)
salta:
                            Next
                        End If
                    Else
                        NombrePaquete = NombrePaqueteaux
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function remplaza_cadena(ByVal strcadena As String) As String
        If strcadena = "Gastos Medicos (LUC)" _
        Or strcadena = "Gastos M�dicos (LUC)" _
        Or strcadena = "Gastos Medicos" Then
            'Or strcadena = "Gastos Medicos Ocupantes" Then
            strcadena = "Gastos M�dicos"
        End If
        Return strcadena.Trim
    End Function

End Class
