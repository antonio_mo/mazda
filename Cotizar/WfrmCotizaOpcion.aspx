<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotizaOpcion.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotizaOpcion"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmCotizaOpcion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
		<!-- HM -->
		<LINK rel="stylesheet" type="text/css" href="..\Css\submodal.css">
		<script language="javascript" type="text/javascript" src="..\JavaScript\submodalsource.js"></script>
		<!-- HM -->
		<script language="JScript">
		function redireccion(intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago, Aseg12)
		{
			/*if ((document.getElementById("txt_contrato").value)=='')
			{
				alert("El numero de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_contrato").focus();
				return 0
			}
			if ((document.getElementById("txt_fecha").value)=='')
			{
				alert("La fecha de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_fecha").focus();
				return 0
			}*/
			var uno = document.getElementById("txt_precio").value;
			//var strfecha1 = document.getElementById("txt_fecha").value;
			//var cvecontrato1 = document.getElementById("txt_contrato").value;
			var cvecontrato1 = 0
			var tipoCarga = 0
			dbprecio=0
			var path = "WfrmCotizaOpcion.aspx?cveAseg="+intaseguradora+"&valcost="+uno+"&cveplazo="+intplazo+"&cveregion="+intregion+"&cvepaquete="+intpaquete+"&subramo="+subramo+"&contrato="+cvecontrato1+"&Recargo="+IntRecargo+"&NumPago="+IntNumPago+"&Pago1="+Pago+"&Pago2="+SubPago+"&TipoCarga="+tipoCarga+"&Aseg12meses="+Aseg12+""
			//alert(path)
			window.location.href=path;
		}
		String.prototype.trim = function() 
		{
			return this.replace(',','');
		}
		function ValidaPrima(intaseguradora12, intaseguradoraMulti, intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago, Aseg12)
		{ 
			var uno = document.getElementById("txt_precio").value;
			var cvecontrato1 = 0
			var tipoCarga = 0
			dbprecio=0
			var x ="WfrmMsg.aspx?Aseg12meses_val="+intaseguradora12+"&cveAsegMulti_val="+intaseguradoraMulti+"&cveAseg="+intaseguradora+"&valcost="+uno+"&cveplazo="+intplazo+"&cveregion="+intregion+"&cvepaquete="+intpaquete+"&subramo="+subramo+"&contrato="+cvecontrato1+"&Recargo="+IntRecargo+"&NumPago="+IntNumPago+"&Pago1="+Pago+"&Pago2="+SubPago+"&TipoCarga="+tipoCarga+"&Aseg12meses="+Aseg12+""
			//var y ="WfrmCotizaOpcion.aspx?cveAseg=1&cveplazo=12"
			//alert(x);
			initPopUp();
			showPopWin(x, 600, 350, '<font color="#FFFFFF" face=arial><b>Recotizar prima</b></font>', null);
	    }   
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 101; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Emisor de 
							P�lizas</FONT></TD>
				</TR>
			</TABLE>
			<HR style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 8px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table2" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD class="obligatorio" align="right">Automovil :</TD>
					<TD><asp:label id="lbl_tipo" runat="server" Width="100%" CssClass="negrita">Nuevo</asp:label></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD vAlign="top" rowSpan="9"><asp:table id="tb_seguro" runat="server" Width="100%" GridLines="Both" CellSpacing="0" CellPadding="0"></asp:table></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Marca :</TD>
					<TD><asp:dropdownlist id="cbo_marca" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Tipo :</TD>
					<TD><asp:dropdownlist id="cbo_modelo" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Modelo :</TD>
					<TD><asp:dropdownlist id="cbo_anio" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Descripci�n :
					</TD>
					<TD colSpan="2"><asp:dropdownlist id="cbo_descripcion" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD style="HEIGHT: 29px"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right"><asp:label id="lbl_uso" runat="server">Uso :</asp:label></TD>
					<TD><asp:dropdownlist id="cbo_uso" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Tipo contribuyente :</TD>
					<TD colSpan="2"><asp:radiobuttonlist id="rb_tipo" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="3"
							RepeatDirection="Horizontal">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Tipo de Seguro :</TD>
					<TD colSpan="2"><asp:radiobuttonlist id="rb_promocion" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="2"
							RepeatDirection="Horizontal">
							<asp:ListItem Value="0" Selected="True">Seguro Gratis 24 y 36</asp:ListItem>
							<asp:ListItem Value="1">Seguro Gratis 48 y 60</asp:ListItem>
							<asp:ListItem Value="2">Plazo 24 y 36</asp:ListItem>
							<asp:ListItem Value="3">Plazo 48 y 60</asp:ListItem>
							<asp:ListItem Value="4">Plan anual 24 y 36</asp:ListItem>
							<asp:ListItem Value="5">Plan anual 48 y 60</asp:ListItem>
						</asp:radiobuttonlist></TD>
				    <td></td>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Precio de la unidad :</TD>
					<TD><asp:textbox id="txt_precio" onkeypress="javascript:onlyDigits(event,'decOK');" runat="server"
							Width="50%" CssClass="Texto_Cantidad" MaxLength="8"></asp:textbox></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita" align="right"></TD>
					<TD><asp:button id="cmd_cotizar" runat="server" CssClass="boton" Text="Cotizar"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right"></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="right">Plazos&nbsp;de Financiamiento:</TD>
					<TD class="negrita"><asp:dropdownlist id="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist>&nbsp;
						<asp:label id="Label5" runat="server" CssClass="negrita">Meses</asp:label>&nbsp;&nbsp;</TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="5"><asp:table id="tb_aseguradora" runat="server" Width="100%" GridLines="Both" CellSpacing="0"
							CellPadding="0"></asp:table></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD><asp:label id="lbl_plazo" runat="server" CssClass="negrita" Visible="False"></asp:label></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD><asp:radiobuttonlist id="rb_seguro" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"
							RepeatDirection="Horizontal" Visible="False">
							<asp:ListItem Value="0">Financiado</asp:ListItem>
							<asp:ListItem Value="1">Contado</asp:ListItem>
						</asp:radiobuttonlist></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="negrita"></TD>
					<TD align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
					<TD width="20%"></TD>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox style="Z-INDEX: 104; POSITION: absolute; TOP: 704px; LEFT: 424px" id="MsgBox" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
