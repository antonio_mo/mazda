<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>

<%@ Page Async="true" Language="vb" AutoEventWireup="false" CodeBehind="WfrmCotizaPromo.aspx.vb" Inherits="CotizadorContado_Conauto.WfrmCotizaPromo" %>

<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WfrmCotizaPromo</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language="javascript" src="..\JavaScript\Jscripts.js"></script>
    <link rel="stylesheet" type="text/css" href="..\Css\submodal.css">
    <script language="javascript" type="text/javascript" src="..\JavaScript\submodalsource.js"></script>
    <link rel="stylesheet" type="text/css" href="..\Css\Styles.css">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jsCotizarPromo.js"></script>

    <script src="../Scripts/jsConautoAon.js"></script>

    <script language="JScript">
        function redireccion(intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago) {
            /*if ((document.getElementById("txt_contrato").value)=='')
			{
				alert("El numero de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_contrato").focus();
				return 0
			}
			if ((document.getElementById("txt_fecha").value)=='')
			{
				alert("La fecha de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_fecha").focus();
				return 0
			}*/
            var uno = document.getElementById("txt_precio").value;
            var strfecha1 = document.getElementById("txt_fecha").value;
            //var strfecha1 = document.getElementById("txt_fecha").value;
            //var cvecontrato1 = document.getElementById("txt_contrato").value;
            var cvecontrato1 = 0
            var tipoCarga = 0
            dbprecio = 0
            var path = "WfrmCotizaPromo.aspx?cveAseg=" + intaseguradora + "&valcost=" + uno + "&cveplazo=" + intplazo + "&cveregion=" + intregion + "&cvepaquete=" + intpaquete + "&subramo=" + subramo + "&contrato=" + cvecontrato1 + "&Recargo=" + IntRecargo + "&NumPago=" + IntNumPago + "&Pago1=" + Pago + "&Pago2=" + SubPago + "&TipoCarga=" + tipoCarga + "&strfecha=" + strfecha1 + ""
            //alert(path)
            window.location.href = path;
        }
        String.prototype.trim = function () {
            return this.replace(',', '');
        }
        function Cliente(descripcion, modelo, anio, plazo, plazototal, marca, persona, estatus) {

            var precio = document.getElementById("txt_precio").value;
            var strfecha1 = document.getElementById("txt_fecha").value;
            //var x ="WfrmImpCotiza.aspx"
            var x = "WfrmImpCotiza.aspx?idaonh=" + descripcion + "&strmodelo=" + modelo + "&stranio=" + anio + "&dbSA=" + precio + "&idplazo=" + plazo + "&plazorestante=" + plazototal + "&strmarca=" + marca + "&strpersona=" + persona + "&strestatus=" + estatus + "&strfecha=" + strfecha1 + ""
            //alert(x);
            initPopUp();
            showPopWin(x, 600, 350, '<font color="#FFFFFF" face=arial><b>Datos del Cliente</b></font>', null);
        }
    </script>
</head>
<body ms_positioning="GridLayout">

    <form id="Form1" method="post" runat="server">

        <asp:HiddenField runat="server" ID="pathContext" />

        <hr style="z-index: 101; position: absolute; top: 40px; left: 8px" class="lineas" width="100%">
        <table style="z-index: 102; position: absolute; top: 48px; left: 8px" id="Table2" border="0"
            cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td class="obligatorio" align="right">Automovil :</td>
                <td>
                    <asp:Label ID="lbl_tipo" runat="server" Width="100%" CssClass="negrita">Nuevo</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td class="negrita" align="right">
                    <asp:HyperLink Style="z-index: 0" ID="hp_coti1" runat="server" CssClass="normal" Visible="False">Guarda cotizaci�n</asp:HyperLink>&nbsp;
						<asp:HyperLink Style="z-index: 0" ID="hp_coti" runat="server" CssClass="negrita" Visible="False"
                            ImageUrl="..\Imagenes\Menu\disk.png">Comentarios</asp:HyperLink></td>
                <td>
                    <asp:Label ID="lbl_cotizacion" runat="server" Width="100%" CssClass="obligatorio" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Marca :</td>
                <td>
                    <div class="m_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_marca" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_modeloC:m_c:t_p_c" style="width: 100%;" />
                </td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" height="20" align="right"></td>
                <td height="20"></td>
                <td style="width: 164px" class="obligatorio" height="20" align="right"></td>
                <td class="negrita" height="20" align="right"></td>
                <td height="20"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="obligatorio">
                    <asp:HyperLink Style="z-index: 0" ID="Hyperlink1" runat="server" CssClass="normal">Auto Contratado</asp:HyperLink></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td valign="top" rowspan="6">
                    <asp:Table Style="z-index: 0" ID="tb_seguroC" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td style="height: 20px" class="obligatorio" align="right">Tipo :</td>
                <td style="height: 20px">
                    <div class="t_p_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_modeloC" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_anioC:t_p_c:m_l_c" style="width: 100%;" />

                </td>
                <td style="width: 164px; height: 20px" class="obligatorio" align="right"></td>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Modelo :</td>
                <td>
                    <div class="m_l_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_anioC" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_descripcionC:m_l_c:d_s_c" style="width: 100%;" />

                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td style="height: 22px" class="obligatorio" align="right">Descripci�n :
                </td>
                <td style="width: 436px; height: 22px" colspan="2">
                    <div class="d_s_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_descripcionC" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_usoC:d_s_c:u_s_c" style="width: 100%;" />

                </td>
                <td style="height: 22px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label ID="lbl_uso" runat="server">Uso :</asp:Label></td>
                <td>
                    <div class="u_s_c" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_usoC" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_usoC:u_s_c:" style="width: 100%;" />

                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Monto Carta Cr�dito&nbsp;:</td>
                <td>
                    <asp:TextBox Style="z-index: 0" ID="txt_montocarta" onkeypress="javascript:onlyDigits(event,'decOK');"
                        runat="server" Width="50%" CssClass="Texto_Cantidad" MaxLength="9"></asp:TextBox></td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" height="20" align="center"></td>
                <td class="obligatorio" height="20" align="left"></td>
                <td class="obligatorio" height="20" align="center"></td>
                <td style="width: 164px" class="negrita" height="20" align="right"></td>
                <td height="20"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="center"></td>
                <td style="width: 164px" class="negrita" align="left">
                    <asp:HyperLink Style="z-index: 0" ID="Hyperlink2" runat="server" CssClass="normal">Auto Entregado</asp:HyperLink></td>
                <td></td>
                <td></td>
                <td valign="top" rowspan="6">
                    <asp:Table Style="z-index: 0" ID="tb_seguro" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Tipo :</td>
                <td>
                    <div class="t_p" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_modelo" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_anio:t_p:m_l" style="width: 100%;" />

                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Modelo :</td>
                <td>
                    <div class="m_l" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_anio" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_descripcion:m_l:d_s" style="width: 100%;" />

                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Descripci�n :</td>
                <td colspan="2">
                    <div class="d_s" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_descripcion" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_uso:d_s:u_s" style="width: 100%;" />

                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label Style="z-index: 0" ID="Label1" runat="server">Uso :</asp:Label></td>
                <td>
                    <div class="u_s" style="width: 100%; display: none; text-align: center;">
                        <asp:Image ImageUrl="~/imagen/loading.gif" runat="server" Width="22" />
                    </div>
                    <select id="cbo_uso" class="combos_small cbAonCbCotiza" targetcontrolid="cbo_uso:u_s:" style="width: 100%;" />

                </td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Precio de la unidad :</td>
                <td>
                    <asp:TextBox Style="z-index: 0" ID="txt_precio" onkeypress="javascript:onlyDigits(event,'decOK');"
                        runat="server" Width="50%" CssClass="Texto_Cantidad" MaxLength="9"></asp:TextBox></td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td></td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Tipo contribuyente :</td>
                <td style="width: 436px" colspan="2">
                    <asp:RadioButtonList ID="rb_tipo" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="3"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
                        <asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
                        <asp:ListItem Value="2">Moral</asp:ListItem>
                    </asp:RadioButtonList></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td></td>
                <td style="width: 164px" class="negrita" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Tipo de seguro&nbsp;:</td>
                <td class="negrita" align="left">
                    <asp:RadioButtonList Style="z-index: 0" ID="rb_promocion" runat="server" Width="100%" CssClass="combos_small"
                        AutoPostBack="True" RepeatColumns="3" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Selected="True">Seguro Gratis</asp:ListItem>
                        <asp:ListItem Value="1">Multianual</asp:ListItem>
                    </asp:RadioButtonList></td>
                <td style="width: 164px">
                    <asp:Button Style="z-index: 0" ID="cmd_cotizar" runat="server" targetcontrolid="v_l" CssClass="boton" Text="Cotizar"></asp:Button></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label Style="z-index: 0" ID="lbl_diferencia" runat="server" Visible="False">Diferencia :</asp:Label></td>
                <td class="negrita">
                    <asp:TextBox Style="z-index: 0" ID="txt_Diferencia" onkeypress="javascript:onlyDigits(event,'decOK');"
                        runat="server" Width="50%" CssClass="Texto_Cantidad" Visible="False" MaxLength="9" ReadOnly="True"></asp:TextBox></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" height="20" align="right"></td>
                <td class="negrita" height="20"></td>
                <td style="width: 164px" height="20" align="right"></td>
                <td height="20"></td>
                <td height="20"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Plazos&nbsp;contratado :</td>
                <td class="negrita">
                    <asp:DropDownList Style="z-index: 0" ID="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:DropDownList><asp:Label Style="z-index: 0" ID="Label6" runat="server" CssClass="negrita">Meses</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Cuotas anticipadas&nbsp;:</td>
                <td class="negrita">
                    <asp:DropDownList Style="z-index: 0" ID="cbo_cuotas" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:DropDownList><asp:Label Style="z-index: 0" ID="Label7" runat="server" CssClass="negrita">Meses</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Meses transcurridos&nbsp;:</td>
                <td class="negrita">
                    <asp:DropDownList Style="z-index: 0" ID="cbo_mesestrans" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:DropDownList><asp:Label Style="z-index: 0" ID="Label8" runat="server" CssClass="negrita">Meses</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">
                    <asp:Label Style="z-index: 0" ID="lblSeguroGratis" runat="server">Meses seguro gratis :</asp:Label></td>
                <td class="negrita">
                    <asp:DropDownList Style="z-index: 0" ID="cbo_mesesseggra" runat="server" CssClass="combos_small"></asp:DropDownList><asp:Label Style="z-index: 0" ID="lbl_MesSG" runat="server" CssClass="negrita">Meses</asp:Label></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Plazos Restantes :</td>
                <td class="negrita">
                    <asp:TextBox Style="z-index: 0" ID="txt_plazosrest" onkeypress="javascript:onlyDigits(event,'decOK');"
                        runat="server" Width="50%" CssClass="Texto_Cantidad" MaxLength="25" ReadOnly="True"></asp:TextBox>&nbsp;
						<asp:Label ID="Label5" runat="server" CssClass="negrita">Meses</asp:Label>&nbsp;&nbsp;</td>
                <td class="obligatorio" align="right">
                    <asp:HyperLink Style="z-index: 0" ID="Hyperlink3" runat="server" CssClass="obligatorio" Visible="False">Plazo total del seguro : </asp:HyperLink>&nbsp;</td>
                <td>
                    <asp:Label Style="z-index: 0" ID="lbl_plazo" runat="server" CssClass="negrita" Visible="False"></asp:Label></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" class="obligatorio" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right">Fecha de&nbsp;entrega&nbsp;:</td>
                <td class="negrita">
                    <asp:TextBox Style="z-index: 0" ID="txt_fecha" runat="server" Width="45%" CssClass="combos_small"
                        AutoPostBack="True" MaxLength="10" ReadOnly="True"></asp:TextBox>&nbsp;&nbsp;
						<rjs:PopCalendar Style="z-index: 0" ID="pc_reporte" runat="server" Width="8px" AutoPostBack="False"
                            To-Increment="4" Height="8px" To-Today="True" BorderColor="Black" BorderWidth="1px" BackColor="Yellow"
                            TextMessage="La fecha es Incorrecta" Buttons="[<][m][y]  [>]" Culture="es-MX Espa�ol (M�xico)"
                            RequiredDateMessage="La Fecha es Requerida" Fade="0.5" Move="True" ShowWeekend="True" Shadow="True"
                            InvalidDateMessage="D�a Inv�lido" BorderStyle="Solid" Separator="/" Control="txt_fecha" ShowErrorMessage="False"
                            From-Today="True"></rjs:PopCalendar>
                </td>
                <td style="width: 164px" align="right"></td>
                <td>&nbsp;&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td style="height: 20px" class="obligatorio" align="right"></td>
                <td style="height: 20px" class="negrita"></td>
                <td style="width: 164px; height: 20px" align="left">
                    <asp:Button Style="z-index: 0" ID="cmd_CalPagos" runat="server" CssClass="boton" Text="Calcular Pagos"></asp:Button>
                    <%--<asp:button style="Z-INDEX: 0" id="btnCalPagos" runat="server" CssClass="boton" Text="Calcular Pagos 2"></asp:button>--%>
                </td>
                <td style="height: 20px"></td>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" class="normal" align="right">&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="obligatorio" align="right"></td>
                <td class="negrita"></td>
                <td style="width: 164px" class="normal" align="right">&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:Table ID="tb_aseguradora" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                        GridLines="Both">
                    </asp:Table>
                    <cc1:MsgBox Style="z-index: 0" ID="MsgBox" runat="server"></cc1:MsgBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td>
                    <asp:RadioButtonList ID="rb_seguro" runat="server" Width="100%" CssClass="combos_small" Visible="False"
                        AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">Financiado</asp:ListItem>
                        <asp:ListItem Value="1">Contado</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="negrita"></td>
                <td style="width: 164px" align="right"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td width="15%"></td>
                <td width="25%"></td>
                <td style="width: 164px" width="15%"></td>
                <td width="20%"></td>
                <td width="25%"></td>
            </tr>
        </table>
        <table style="z-index: 103; position: absolute; height: 24px; top: 8px; left: 8px" id="Table4"
            border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="negrita_marco_color" height="20" valign="middle" width="2%" align="center">
                    <asp:Image ID="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:Image></td>
                <td class="Interiro_tabla" width="98%" colspan="3"><font style="z-index: 0" size="+0">&nbsp; 
							Emisor de P�lizas</font></td>
            </tr>
        </table>

    </form>

    <script type="text/javascript"> $(document).ready(function () { $('.cbAonCbCotiza').first().cotiza('fill', { dtValues: { p: 'f_l' } }).end() });</script>
</body>
</html>
