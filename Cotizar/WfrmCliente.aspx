<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCliente.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCliente" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="../JavaScript/Jscripts.js"></script>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		function PonDiagonal()
		{
			var stCadena=document.cliente.txt_fnacimiento.value;
			if (IsNumeric(stCadena))
			{
				if(stCadena.length == 2)
				{
					document.cliente.txt_fnacimiento.value=document.cliente.txt_fnacimiento.value+"/"
				}
				if(stCadena.length == 5)
				{
					document.cliente.txt_fnacimiento.value=document.cliente.txt_fnacimiento.value+"/"
				}	
			}
			else
			{
				document.cliente.txt_fnacimiento.value='';
			}
		}
		function IsNumeric(sText)
		{
			var ValidChars = "0123456789/";
			var IsNumber=true;
			var Char;
		 
			for (i = 0; i < sText.length && IsNumber == true; i++) 
			{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
				{
					IsNumber = false;
				}
			}
			return IsNumber;
	   }
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="cliente" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" width="100%">
			<TABLE id="Table5" style="Z-INDEX: 110; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Clientes&nbsp;</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="Interiro_tabla_centro" width="20%">Cliente :</TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
				</TR>
				<TR>
					<TD colSpan="5"><asp:panel id="pnl_fisica" runat="server">
							<TABLE id="Table8" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="obligatorio" align="center">*Nombre (s) :</TD>
									<TD class="obligatorio" align="center">*Apellido Paterno :</TD>
									<TD class="obligatorio" align="center">Apellido Materno :</TD>
									<TD class="obligatorio" align="center">*Fecha de Nacimiento dd/mm/yyyy</TD>
									<TD class="obligatorio" align="center">*R.F.C. :</TD>
								</TR>
								<TR>
									<TD>
										<asp:textbox id="txt_nombre" onkeypress="javascript:onlyUpperLetters(event);" runat="server" MaxLength="75"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD>
										<asp:textbox id="txt_paterno" onkeypress="javascript:onlyUpperLetters(event);" runat="server" MaxLength="75"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD>
										<asp:textbox id="txt_materno" onkeypress="javascript:onlyUpperLetters(event);" runat="server" MaxLength="75"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD>
										<asp:textbox id="txt_fnacimiento" tabIndex="1" onkeyup="PonDiagonal()" runat="server" MaxLength="10"
											CssClass="combos_small" Width="60%" AutoPostBack="True" ReadOnly="True"></asp:textbox>&nbsp;&nbsp;
										<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" AutoPostBack="True" Height="8px" From-Control="txt_fsiniestro"
											BorderColor="Black" BorderWidth="1px" BackColor="Yellow" TextMessage="La fecha es Incorrecta"
											Buttons="[<][m][y]  [>]" Culture="es-MX Espa�ol (M�xico)" RequiredDateMessage="La Fecha es Requerida"
											Fade="0.5" Move="True" ShowWeekend="True" Shadow="True" InvalidDateMessage="D�a Inv�lido" BorderStyle="Solid"
											Separator="/" Control="txt_fnacimiento" To-Today="True" ShowErrorMessage="False"></rjs:popcalendar></TD>
									<TD>
										<asp:textbox id="txt_rfc" onkeypress="convierteMayusculas();" runat="server" MaxLength="15" CssClass="combos_small"
											Width="70%"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="obligatorio"></TD>
									<TD>
										<asp:textbox id="txt_lugarN" onkeypress="convierteMayusculas();" runat="server" MaxLength="25"
											CssClass="combos_small" Width="100%" Visible="False"></asp:textbox></TD>
									<TD></TD>
									<TD>
										<asp:textbox id="txt_ocupacion" onkeypress="convierteMayusculas();" runat="server" MaxLength="25"
											CssClass="combos_small" Width="100%" Visible="False"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top">*Personalidad fiscal :</TD>
					<TD colSpan="2"><asp:radiobuttonlist id="rb_tipo" runat="server" CssClass="negrita" Width="100%" AutoPostBack="True"
							RepeatDirection="Horizontal" RepeatColumns="3">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist><asp:label id="lbl_persona" runat="server" CssClass="negrita" Width="100%"></asp:label></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="5"><asp:panel id="pnl_moral" runat="server">
							<TABLE id="Table7" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="obligatorio">*Razon Social :</TD>
									<TD colSpan="2">
										<asp:textbox id="txt_razon" onkeypress="convierteMayusculas();" runat="server" MaxLength="100"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio">*Representante Legal :</TD>
									<TD colSpan="2">
										<asp:textbox id="txt_legal" onkeypress="convierteMayusculas();" runat="server" MaxLength="100"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio">*Contacto :</TD>
									<TD colSpan="2">
										<asp:textbox id="txt_contacto" onkeypress="convierteMayusculas();" runat="server" MaxLength="100"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio">*R.F.C. :</TD>
									<TD>
										<asp:textbox id="txt_rfcrazon" onkeypress="convierteMayusculas();" runat="server" MaxLength="15"
											CssClass="combos_small" Width="70%" AutoPostBack="True"></asp:textbox></TD>
									<TD class="obligatorio">ABCYYMMDD</TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="obligatorio">*Tel�fonos :</TD>
					<TD><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_tel" runat="server" MaxLength="15"
							CssClass="combos_small" Width="100%"></asp:textbox></TD>
					<TD class="negrita" align="right"></TD>
					<TD><asp:textbox id="txt_tipocliente" runat="server" CssClass="negrita" Width="16px" Height="16px"
							Visible="False"></asp:textbox><asp:textbox onkeypress="convierteMayusculas();" id="txt_desempleo" runat="server" CssClass="negrita"
							Width="16px" Height="16px" Visible="False"></asp:textbox><asp:textbox id="txt_clave" runat="server" CssClass="negrita" Width="16px" Height="16px" Visible="False"></asp:textbox><asp:textbox onkeypress="javascript:onlyAllLetters(event);" id="txt_empleado" runat="server"
							CssClass="negrita" Width="16px" Height="16px" Visible="False"></asp:textbox><asp:textbox onkeypress="javascript:onlyAllLetters(event);" id="txt_agencia" runat="server" CssClass="negrita"
							Width="16px" Height="16px" Visible="False"></asp:textbox><asp:textbox onkeypress="convierteMayusculas();" id="txt_vida" runat="server" CssClass="negrita"
							Width="16px" Height="16px" Visible="False"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">*Email :</TD>
					<TD><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_email" runat="server" MaxLength="75"
							CssClass="combos_small" Width="100%"></asp:textbox></TD>
					<TD class="negrita" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">*Beneficiario Preferente :</TD>
					<TD colSpan="4"><asp:textbox id="txt_beneficiario" runat="server" MaxLength="150"
							CssClass="combos_small" Width="80%"></asp:textbox><%--<asp:dropdownlist id="cbo_beneficiario" runat="server" CssClass="combos_small" Width="80%"></asp:dropdownlist>--%>

					</TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Interiro_tabla_centro">Domicilio :</TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">C�digo Postal :</TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_cp" runat="server" MaxLength="5"
							CssClass="combos_small" Width="35%"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button id="cmd_buscar_cp" runat="server" CssClass="boton" Text="Buscar"></asp:button></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="5"><asp:panel id="pnl_dir" runat="server">
							<TABLE id="Table6" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="obligatorio">Estado :</TD>
									<TD class="obligatorio">Provincia / Delegaci�n :</TD>
									<TD class="obligatorio">Colonia :</TD>
									<TD class="obligatorio">Calle :</TD>
									<TD class="obligatorio">No. Exterior :</TD>
									<TD class="obligatorio">No. Interior :</TD>
								</TR>
								<TR>
									<TD>
										<asp:Label id="lbl_estado" runat="server" CssClass="negrita" Width="100%"></asp:Label></TD>
									<TD>
										<asp:Label id="lbl_provincia" runat="server" CssClass="negrita" Width="100%"></asp:Label></TD>
									<TD>
										<asp:textbox id="txt_col" onkeypress="convierteMayusculas(event);" runat="server" MaxLength="100"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD>
										<asp:textbox id="txt_dir" onkeypress="convierteMayusculas(event);" runat="server" MaxLength="100"
											CssClass="combos_small" Width="100%"></asp:textbox></TD>
									<TD style="margin-left: 80px">
										<asp:textbox id="txt_exterior" onkeypress="convierteMayusculas(event);" runat="server" MaxLength="6"
											CssClass="combos_small" Width="50%"></asp:textbox></TD>
									<TD>
										<asp:textbox id="txt_interior" onkeypress="convierteMayusculas(this);" runat="server" MaxLength="6"
											CssClass="combos_small" Width="50%"></asp:textbox></TD>
								</TR>
								<TR>
									<TD width="20%"></TD>
									<TD width="20%"></TD>
									<TD width="15%"></TD>
									<TD width="15%"></TD>
									<TD width="15%"></TD>
									<TD width="15%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="5">
						<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="HEIGHT: 23px" align="center" width="20%"><asp:button id="cmd_limpiar" runat="server" CssClass="boton" Text="Limpiar"></asp:button></TD>
								<TD style="HEIGHT: 23px" align="center" width="20%"><asp:button id="cmd_cliente" runat="server" CssClass="boton" Text="Buscar Cliente"></asp:button></TD>
								<TD style="HEIGHT: 23px" align="center" width="20%"><asp:button id="cmd_clientep" runat="server" CssClass="boton" Text="Cliente Prospecto"></asp:button></TD>
								<TD style="HEIGHT: 23px" align="center"><asp:button id="cmd_agregar" runat="server" CssClass="boton" Text="Guardar y Continuar"></asp:button></TD>
								<TD style="HEIGHT: 23px" align="center" width="20%"><asp:button id="cmd_regresar" runat="server" CssClass="boton" Visible="False" Text="Regresar"></asp:button></TD>
							</TR>
							<TR>
								<TD align="center"></TD>
								<TD align="center"></TD>
								<TD align="center"></TD>
								<TD align="center"></TD>
								<TD align="center"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="4"><asp:panel id="pnl_clientes" runat="server" Width="100%" Height="176px">
										<DIV style="WIDTH: 100%; HEIGHT: 18px; OVERFLOW: hidden">
											<asp:datagrid style="POSITION: relative" id="grdcatalogo_fondo" runat="server" CssClass="Datagrid"
												Width="500px" CellPadding="1" AutoGenerateColumns="False">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="Interiro_tabla_centro" VerticalAlign="Top"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="60%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataFormatString="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<DIV style="WIDTH: 100%; HEIGHT: 120px; OVERFLOW: auto" onscroll="DoScroll()" id="divScroll">
											<asp:datagrid id="grdcatalogo" runat="server" CssClass="datagrid" Width="500px" CellPadding="1"
												AutoGenerateColumns="False" ShowHeader="False" AllowSorting="True">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="60%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataField="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD width="25%"></TD>
												<TD class="obligatorio" width="50%">Nombre Cliente :</TD>
												<TD width="25%"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD>
													<asp:TextBox id="txt_cliente" onkeypress="convierteMayusculas();" runat="server" MaxLength="15"
														CssClass="combos_small" Width="100%"></asp:TextBox></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD align="center">
													<asp:Button id="cmd_buscar" runat="server" CssClass="boton" Text="Consultar"></asp:Button></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; POSITION: absolute; TOP: 1176px; LEFT: 248px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
