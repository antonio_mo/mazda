Imports CN_Negocios

Public Class WfrmMsg
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccalculo As New CnCalculo
    Private ccliente As New CnCotizador
    Private CdFraccion As CP_FRACCIONES.clsEmision

    '1 a�o valor factura >= 13
    '2 a�os valor factura <= 12

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        If Not Page.IsPostBack Then
            Dim IdAseg12 As Long = 0
            Dim IdAsegMulti As Long = 0

            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Request.QueryString("Aseg12meses_val") Is Nothing Then
                IdAseg12 = Request.QueryString("Aseg12meses")
            End If
            If Not Request.QueryString("cveAsegMulti_val") Is Nothing Then
                IdAsegMulti = Request.QueryString("cveAsegMulti_val")
            End If
            If Not Request.QueryString("cveAsegMulti_val") Is Nothing Then
                If IdAseg12 = IdAsegMulti Then 'ASeguradoras iguales
                    IdAseg12 = calculoaseguradora12meses(0, 0)
                    Dim Dt12 As DataTable = ccalculo.Prima12meses(Session("idcotizapanel12"))
                    If Dt12.Rows.Count > 0 Then
                        If Dt12.Rows(0).IsNull("url_imagenPeque") Then
                            Img12.ImageUrl = "../Imagenes/logos/Vacio.gif"
                        Else
                            Img12.ImageUrl = Dt12.Rows(0)("url_imagenPeque")
                        End If

                        If Session("Nivel") = 0 Then
                            'Label1.Text = "Aseguradora :" & Dt12.Rows(0)("descripcion_aseguradora") & " Prima: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                            lbl_12meses.Text = " Prima Total: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                        End If
                    End If
                    Dim DtMulti As DataTable = ccalculo.PrimaMultianual(Session("idcotizapanel"), IdAsegMulti)
                    If DtMulti.Rows.Count > 0 Then
                        If DtMulti.Rows(0).IsNull("url_imagenPeque") Then
                            ImgResto.ImageUrl = "../Imagenes/logos/Vacio.gif"
                        Else
                            ImgResto.ImageUrl = DtMulti.Rows(0)("url_imagenPeque")
                        End If

                        'Label2.Text = "Aseguradora :" & DtMulti.Rows(0)("descripcion_aseguradora") & " PrimaInicial: " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00") & "  PrimaConsecutiva: " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                        lbl_resto1.Text = " Prima Parcial : " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                        lbl_resto2.Text = " Prima Total : " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00")
                    End If
                Else
                    If IdAseg12 = 62 Then ' Qualitas 12 y Axa resto
                        Dim Dt12 As DataTable = ccalculo.Prima12meses(Session("idcotizapanel12"))
                        If Dt12.Rows.Count > 0 Then
                            If Dt12.Rows(0).IsNull("url_imagenPeque") Then
                                Img12.ImageUrl = "../Imagenes/logos/Vacio.gif"
                            Else
                                Img12.ImageUrl = Dt12.Rows(0)("url_imagenPeque")
                            End If

                            If Session("Nivel") = 0 Then
                                'Label1.Text = "Aseguradora :" & Dt12.Rows(0)("descripcion_aseguradora") & " Prima: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                                lbl_12meses.Text = " Prima Total: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                            End If
                        End If
                        IdAsegMulti = RecalculaValor(1) 'aseguradora AXA multianual (1 = nuevos paquetes >=13, 0 = paquetes iniciales <=12)
                        Dim DtMulti As DataTable = ccalculo.PrimaMultianual(Session("idcotizapanel"), IdAsegMulti)
                        If DtMulti.Rows.Count > 0 Then
                            If DtMulti.Rows(0).IsNull("url_imagenPeque") Then
                                ImgResto.ImageUrl = "../Imagenes/logos/Vacio.gif"
                            Else
                                ImgResto.ImageUrl = DtMulti.Rows(0)("url_imagenPeque")
                            End If
                            'Label2.Text = "/nAseguradora :" & DtMulti.Rows(0)("descripcion_aseguradora") & " PrimaInicial: " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00") & "  PrimaConsecutiva: " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                            lbl_resto1.Text = " Prima Parcial : " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                            lbl_resto2.Text = " Prima Total : " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00")
                        End If
                    Else
                        'Axa 12 y Qualitas resto , RecalculaValor() aseguradora AXA 12, RecalculaValor() aseguradora Qualitas multianual
                        IdAseg12 = calculoaseguradora12meses(1)
                        Dim Dt12 As DataTable = ccalculo.Prima12meses(Session("idcotizapanel12"))
                        If Dt12.Rows.Count > 0 Then
                            If Dt12.Rows(0).IsNull("url_imagenPeque") Then
                                Img12.ImageUrl = "../Imagenes/logos/Vacio.gif"
                            Else
                                Img12.ImageUrl = Dt12.Rows(0)("url_imagenPeque")
                            End If
                            If Session("Nivel") = 0 Then
                                'Label1.Text = "Aseguradora :" & Dt12.Rows(0)("descripcion_aseguradora") & " Prima: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                                lbl_12meses.Text = " Prima Total: " & Format(Dt12.Rows(0)("primatotal"), "$ #,#.00")
                            End If
                        End If
                        IdAsegMulti = RecalculaValor(1) 'aseguradora AXA multianual (1 = nuevos paquetes >=13, 0 = paquetes iniciales <=12)
                        Dim DtMulti As DataTable = ccalculo.PrimaMultianual(Session("idcotizapanel"), IdAsegMulti)
                        If DtMulti.Rows.Count > 0 Then
                            If DtMulti.Rows(0).IsNull("url_imagenPeque") Then
                                ImgResto.ImageUrl = "../Imagenes/logos/Vacio.gif"
                            Else
                                ImgResto.ImageUrl = DtMulti.Rows(0)("url_imagenPeque")
                            End If
                            'Label2.Text = "/nAseguradora :" & DtMulti.Rows(0)("descripcion_aseguradora") & " PrimaInicial: " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00") & "  PrimaConsecutiva: " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                            lbl_resto1.Text = " Prima Parcial : " & Format(DtMulti.Rows(0)("primaconsecutiva"), "$ #,#.00")
                            lbl_resto2.Text = " Prima Total : " & Format(DtMulti.Rows(0)("primatotal"), "$ #,#.00")
                        End If
                    End If
                End If
                'Label1.Text = Session("idcotizapanel12")
                'Label2.Text = Session("idcotizapanel")

                'RecalculaValor()
            End If
        End If
    End Sub

    Private Function RecalculaValor(ByVal TipoPrograma As Integer) As Integer
        tipo_poliza(Request.QueryString("cveplazo"))

        Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")
        Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
               Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
               1, _
               Session("tipopol"), _
               Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
               Session("EstatusV"), _
               0, Session("InterestRate"), Session("FechaIni"), _
               Session("tiposeguro"), Request.QueryString("cveplazo"), _
               Request.QueryString("valcost"), _
               Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
               0, Session("vehiculo"), Request.QueryString("cveAseg"), _
               Request.QueryString("cvepaquete"), Session("programa"), _
               Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
               Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
               Request.QueryString("TipoCarga"), 0, Session("TipoProducto"), _
               Request.QueryString("Aseg12meses"), TipoPrograma)
        'fin cambio micha

        Session("subsidios") = ccalculo.subsidios
        Session("ArrPoliza") = ccalculo.ArrPolisa
        Session("resultado") = ccalculo.Results
        Session("MultArrPolisa") = ccalculo.MultArrPolisa

        Session("DatosGenerales") = ""
        Dim arr(25) As String
        arr(0) = Session("moneda")
        arr(1) = Request.QueryString("subramo")
        arr(2) = Session("anio")
        arr(3) = Session("modelo")
        arr(4) = 1
        'arr(5) = Seg_financiado(rb_seguro)
        arr(5) = Seg_financiado_Seleccion(Session("seguro"))
        arr(6) = Session("uso")
        arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
        arr(8) = Session("EstatusV")
        arr(9) = 0
        arr(10) = Session("InterestRate")
        arr(11) = Session("FechaIni")
        arr(12) = Session("tiposeguro")
        arr(13) = Request.QueryString("cveplazo")
        arr(14) = Request.QueryString("valcost")
        arr(15) = Session("idgrdcontrato")
        arr(16) = Session("seguro")
        arr(17) = Session("bid")
        arr(18) = 0
        arr(19) = Session("vehiculo")
        arr(20) = Request.QueryString("cveAseg")
        arr(21) = Request.QueryString("cvepaquete")
        arr(22) = Session("programa")
        arr(23) = Session("fincon")
        arr(24) = Request.QueryString("Pago1")
        arr(25) = Request.QueryString("Pago2")
        Session("DatosGenerales") = arr

        'manolito

        Session("plazoFinanciamiento") = Request.QueryString("Recargo")
        Session("plazoRecargo") = Request.QueryString("NumPago")

        Session("region") = Request.QueryString("cveregion")
        Session("aseguradora") = Request.QueryString("cveAseg")
        Session("plazo") = Request.QueryString("cveplazo")
        Session("FechaInicio") = Session("FechaIni")
        Session("contrato") = Request.QueryString("cvecontrato")
        Session("precioEscrito") = Request.QueryString("valcost")
        Session("paquete") = Request.QueryString("cvepaquete")
        Session("Tipocarga") = Request.QueryString("TipoCarga")

        Dim uno As String = ccalculo.StrCadenaAseg
        Dim dos As String = ccalculo.StrSalidaPanleAseg
        Return calculo_general_aseguradora(Request.QueryString("Recargo"), Request.QueryString("cveplazo"))
    End Function

    Public Function calculoaseguradora12meses(ByVal TipoPrograma As Integer, Optional ByVal Recalcula As Integer = 1) As Integer
        Dim Aseguradora12 As Integer = 0
        Dim ValorIdPaquete As Integer = 0
        Dim i As Integer = 0
        Try
            Dim dt As DataTable = ccalculo.Promocion12meses(Session("idcotizapanel12"))
            If dt.Rows.Count > 0 Then

                If Recalcula = 0 Then
                    Dim arrbusqueda() As String = Split(Session("CadenaCob"), "|", )
                    If Not arrbusqueda Is Nothing Then
                        For i = 0 To arrbusqueda.Length - 1
                            Dim arrValor() As String = arrbusqueda(i).Split("*")
                            If arrValor(0) = dt.Rows(0)("Id_Aseguradora") Then
                                ValorIdPaquete = CInt(arrValor(1))
                                Exit For
                            End If
                        Next
                    End If
                End If

                Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                    dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                    1, "A", _
                    Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                    Session("EstatusV"), _
                    0, Session("InterestRate"), Session("FechaIni"), _
                    Session("tiposeguro"), 12, _
                    Request.QueryString("valcost"), _
                    Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                    0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                    IIf(Recalcula = 1, dt.Rows(0)("idpaquete"), ValorIdPaquete), Session("programa"), _
                    Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                    dt.Rows(0)("id_tipoRecargo"), 1, Request.QueryString("TipoCarga"), _
                    IIf(Session("Exclusion") = 0, 1, 0), , , TipoPrograma, , IIf(Recalcula = 1, 0, 1))
                'fin cambio micha

                Session("subsidios1") = ccalculo.subsidios
                Session("ArrPoliza1") = ccalculo.ArrPolisa
                Session("resultado1") = ccalculo.Results
                Session("MultArrPolisa1") = ccalculo.MultArrPolisa

                Session("DatosGenerales1") = ""
                Dim arr(25) As String
                arr(0) = Session("moneda")
                arr(1) = dt.Rows(0)("subramo")
                arr(2) = Session("anio")
                arr(3) = Session("modelo")
                arr(4) = 1
                'arr(5) = Seg_financiado(rb_seguro)
                arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                arr(6) = Session("uso")
                arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                arr(8) = Session("EstatusV")
                arr(9) = 0
                arr(10) = Session("InterestRate")
                arr(11) = Session("FechaIni")
                arr(12) = Session("tiposeguro")
                arr(13) = 12
                arr(14) = Request.QueryString("valcost") 'txt_precio.Text
                arr(15) = Session("idgrdcontrato")
                arr(16) = Session("seguro")
                arr(17) = Session("bid")
                arr(18) = 0
                arr(19) = Session("vehiculo")
                arr(20) = dt.Rows(0)("Id_aseguradora")
                arr(21) = dt.Rows(0)("idpaquete")
                arr(22) = Session("programa")
                arr(23) = Session("fincon")
                arr(24) = dt.Rows(0)("primatotal")
                arr(25) = dt.Rows(0)("primaconsecutiva")
                Session("DatosGenerales1") = arr

                'manolito
                Session("plazo1") = 12
                Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
                Session("plazoRecargo1") = 1
                Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
                Session("paquete1") = dt.Rows(0)("idpaquete")
                Session("subramo1") = dt.Rows(0)("subramo")

                Dim uno As String = ccalculo.StrCadenaAseg
                Dim dt12 As DataTable = ccalculo.Update_Cotizacion_panel_12meses(Session("idcotizapanel12"), ccalculo.StrCadenaAseg, "E")
                If dt12.Rows.Count > 0 Then
                    If Not dt12.Rows(0).IsNull("IdPaquete") Then
                        Session("DatosGenerales1")(21) = dt12.Rows(0)("IdPaquete")
                    End If

                    If Not dt12.Rows(0).IsNull("primatotal") Then
                        Session("DatosGenerales1")(24) = dt12.Rows(0)("primatotal")
                    End If

                    If Not dt12.Rows(0).IsNull("primaconsecutiva") Then
                        Session("DatosGenerales1")(25) = dt12.Rows(0)("primaconsecutiva")
                    End If

                    If Not dt12.Rows(0).IsNull("IdPaquete") Then
                        Session("paquete1") = dt12.Rows(0)("IdPaquete")
                    End If

                    If Not dt12.Rows(0).IsNull("id_aseguradora") Then
                        Aseguradora12 = dt12.Rows(0)("id_aseguradora")
                    End If
                End If
            End If

            Return Aseguradora12
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                Session("tipopol") = "M"
            Else
                Session("tipopol") = "A"
            End If
        Else
            Session("tipopol") = "A"
        End If
    End Sub

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Public Function calculo_general_aseguradora(ByVal IdRecargo As Integer, ByVal cboplazo As Integer) As Integer
        Dim arr() As String
        Dim Arr1() As String
        Dim i, x As Integer
        Dim strcadena As String = ""
        Dim strCadenaCal As String = ""
        Dim FechaTermina As String = ""
        Dim ds As DataSet

        Dim Tiporecargo As Integer = 0
        Dim strInserta As String = ""
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0

        Dim Aseguradora As Integer = 0


        Try

            strcadena = ccalculo.StrCadenaAseg
            strCadenaCal = ccalculo.StrSalidaPanleAseg

            Select Case IdRecargo
                Case 2 'Anual
                    Tiporecargo = 1
                Case 3 'Semestral
                    Tiporecargo = 4
                Case 4 'Trimestral
                    Tiporecargo = 3
                Case 5 'Mensual
                    Tiporecargo = 2
                Case Else
                    Tiporecargo = IdRecargo
            End Select

            FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), IIf(Session("Exclusion") = 0, cboplazo, cboplazo))

            Dim FechaInicio As String
            Dim arrFinicio() As String
            arrFinicio = Split(Session("FechaIni"), "/")
            If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0" Then
                FechaInicio = arrFinicio(0) & "/" & arrFinicio(1) & "/" & arrFinicio(2)
            Else
                FechaInicio = arrFinicio(1) & "/" & arrFinicio(0) & "/" & arrFinicio(2)
            End If

            Dim Results() As String
            Dim Results1() As String

            arr = strcadena.Split("|")
            Results = strCadenaCal.Split("|")

            For i = 0 To arr.Length - 1
                Arr1 = arr(i).Split("*")
                Results1 = Results(i).Split("*")

                If Arr1(0) = Results1(0) Then
                    ds = Nothing
                    CdFraccion = New CP_FRACCIONES.clsEmision
                    Select Case CInt(Arr1(0))
                        Case 25
                            ds = CdFraccion.FraccionesZurich(Session("FechaIni"), _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 59
                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 60
                            ds = CdFraccion.FraccionesGNP_Conauto(FechaInicio, _
                            FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 61
                            ds = CdFraccion.FraccionesAXA_Conauto(FechaInicio, _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 62
                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 63
                            ds = CdFraccion.FraccionesAXA_Conauto(FechaInicio, _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                    End Select

                    If ds.Tables(0).Rows.Count > 0 Then
                        For x = 0 To ds.Tables(0).Rows.Count - 1
                            Select Case x
                                Case 0
                                    Dbtotales1 = ds.Tables(0).Rows(x)("prima_total")
                                Case 1
                                    Dbtotales2 = ds.Tables(0).Rows(x)("prima_total")
                                    Exit For
                            End Select
                        Next
                    End If

                    If strInserta = "" Then
                        strInserta = Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                    Else
                        strInserta = strInserta & "|" & Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                    End If
                End If
            Next

            If Not strInserta = "" Then
                Dim dtP As DataTable = ccalculo.Act_Cotizacion_panel(Session("idcotizapanel"), strInserta)
                If dtP.Rows.Count > 0 Then
                    If Not dtP.Rows(0).IsNull("IdPaquete") Then
                        Session("DatosGenerales")(21) = dtP.Rows(0)("IdPaquete")
                    End If

                    If Not dtP.Rows(0).IsNull("PrimaConsecutiva") Then
                        Session("DatosGenerales")(24) = dtP.Rows(0)("PrimaConsecutiva")
                    End If

                    If Not dtP.Rows(0).IsNull("PrimaConsecutiva2") Then
                        Session("DatosGenerales")(25) = dtP.Rows(0)("PrimaConsecutiva2")
                    End If

                    If Not dtP.Rows(0).IsNull("IdPaquete") Then
                        Session("paquete") = dtP.Rows(0)("IdPaquete")
                    End If

                    If Not dtP.Rows(0).IsNull("id_aseguradora") Then
                        Aseguradora = dtP.Rows(0)("id_aseguradora")
                    End If
                End If
            End If

            Return Aseguradora
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function


End Class
