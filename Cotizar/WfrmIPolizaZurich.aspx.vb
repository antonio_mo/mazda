'..\Imagenes\logos\mazda.gif
Imports CN_Negocios
Imports Emisor_Conauto

Partial Class WfrmIPolizaZurich
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                If Session("programa") = 3 Then
                    imgmazda.ImageUrl = "..\Imagenes\logos\mazda.gif"
                Else
                    imgmazda.ImageUrl = "..\Imagenes\logos\Vacio.gif"
                End If

                If Not Request.QueryString("ban") Is Nothing Then
                    Session("ban") = Request.QueryString("ban")
                End If
                limpia()
                carga_informacion()

                'Dim dtDA As DataTable = ccliente.Datos_aseguradora_poliza(Session("aseguradora"))
                'If dtDA.Rows.Count > 0 Then
                'If Not dtDA.Rows(0).IsNull("nombre_completo") Then
                '    lbl_nomaseg.Text = dtDA.Rows(0)("nombre_completo")
                'End If
                'If Not dtDA.Rows(0).IsNull("direccion") Then
                '    lbl_diraseg.Text = dtDA.Rows(0)("direccion")
                'End If
                'If Not dtDA.Rows(0).IsNull("rfc") Then
                '    lbl_diraseg.Text = lbl_diraseg.Text & ", " & dtDA.Rows(0)("rfc")
                'End If
                'If Not dtDA.Rows(0).IsNull("poblacion") Then
                '    lbl_diraseg.Text = lbl_diraseg.Text & ", " & dtDA.Rows(0)("poblacion")
                'End If
                'If Not dtDA.Rows(0).IsNull("telefono") Then
                '    lbl_diraseg.Text = lbl_diraseg.Text & ", " & dtDA.Rows(0)("telefono")
                'End If
                'If Not dtDA.Rows(0).IsNull("cp") Then
                '    lbl_diraseg.Text = lbl_diraseg.Text & ", " & dtDA.Rows(0)("cp")
                'End If
                'If Not dtDA.Rows(0).IsNull("nombre_completo") Then
                '    lbl_leyaseg.Text = "Un programa de seguros " & dtDA.Rows(0)("nombre_completo")
                'End If
                ''''    If Not dtDA.Rows(0).IsNull("nombre_completo") Then
                ''''        lbl_nombre.Text = dtDA.Rows(0)("nombre_completo")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("nombre_completo") Then
                ''''        lbl_nombre.Text = dtDA.Rows(0)("nombre_completo")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("direccion") Then
                ''''        lbl_dir.Text = dtDA.Rows(0)("direccion")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("rfc") Then
                ''''        lbl_rfc.Text = dtDA.Rows(0)("rfc")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("poblacion") Then
                ''''        lbl_poblacion.Text = dtDA.Rows(0)("poblacion")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("beneficiario") Then
                ''''        lbl_bene.Text = dtDA.Rows(0)("beneficiario")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("telefono") Then
                ''''        lbl_tel.Text = dtDA.Rows(0)("telefono")
                ''''    End If
                ''''    If Not dtDA.Rows(0).IsNull("cp") Then
                ''''        lbl_cp.Text = dtDA.Rows(0)("cp")
                ''''    End If
                'End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        lbl_poliza.Text = ""
        lbl_desde.Text = ""
        lbl_hasta.Text = ""
        lbl_nombre.Text = ""
        lbl_dir.Text = ""
        lbl_rfc.Text = ""
        lbl_poblacion.Text = ""
        lbl_bene.Text = ""
        lbl_tel.Text = ""
        lbl_cp.Text = ""
        lbl_fecha.Text = ""
        lbl_plan.Text = ""
        lbl_deeler.Text = ""
        lbl_auto.Text = ""
        lbl_modelo.Text = ""
        lbl_serie.Text = ""
        lbl_ranave.Text = ""
        lbl_placa.Text = ""
        lbl_motor.Text = ""
        lbl_uso.Text = ""
        lbl_capacidad.Text = ""
        lbl_derecho.Text = ""
        lbl_iva.Text = ""
        lbl_total.Text = ""
        lbl_ley.Text = ""
        lbl_version.Text = ""
        lbl_cliente.Text = ""
        lbl_dom.Text = ""
        lbl_condicion.Text = ""
        lbl_preal.Text = ""
        lbl_ppromocion.Text = ""
    End Sub

    Public Sub carga_informacion()
        Dim total As Double = 0
        Dim cobaddf1 As Double = 0
        Dim cobaddf3 As Double = 0
        Dim cobaddp1 As Double = 0
        Dim cobaddp3 As Double = 0

        Dim dt As DataTable = ccliente.carga_reporte_poliza(Session("idpoliza"), Session("bid"), Session("cliente"), Session("idcontrato"), Session("aseguradora"))
        If dt.Rows.Count > 0 Then
            If dt.Rows(0).IsNull("poliza_multianual") Then
                lbl_poliza.Text = ""
            Else
                lbl_poliza.Text = dt.Rows(0)("poliza_multianual")
            End If
            If dt.Rows(0).IsNull("inicio") Then
                lbl_desde.Text = ""
            Else
                lbl_desde.Text = dt.Rows(0)("inicio")
            End If
            If dt.Rows(0).IsNull("fin") Then
                lbl_hasta.Text = ""
            Else
                lbl_hasta.Text = dt.Rows(0)("fin")
            End If
            If dt.Rows(0).IsNull("nom") Then
                lbl_nombre.Text = ""
            Else
                lbl_nombre.Text = dt.Rows(0)("nom")
            End If
            If dt.Rows(0).IsNull("dir") Then
                lbl_dir.Text = ""
            Else
                lbl_dir.Text = dt.Rows(0)("dir")
            End If
            If dt.Rows(0).IsNull("rfc") Then
                lbl_rfc.Text = ""
            Else
                lbl_rfc.Text = dt.Rows(0)("rfc")
            End If
            If dt.Rows(0).IsNull("poblacion") Then
                lbl_poblacion.Text = ""
            Else
                lbl_poblacion.Text = dt.Rows(0)("poblacion")
            End If
            If dt.Rows(0).IsNull("benef_pref") Then
                lbl_bene.Text = ""
            Else
                lbl_bene.Text = dt.Rows(0)("benef_pref")
            End If
            If dt.Rows(0).IsNull("telefono") Then
                lbl_tel.Text = ""
            Else
                lbl_tel.Text = dt.Rows(0)("telefono")
            End If
            If dt.Rows(0).IsNull("cp_cliente") Then
                lbl_cp.Text = ""
            Else
                lbl_cp.Text = dt.Rows(0)("cp_cliente")
            End If
            If dt.Rows(0).IsNull("fecha_registro") Then
                lbl_fecha.Text = ""
            Else
                lbl_fecha.Text = dt.Rows(0)("fecha_registro")
            End If
            If dt.Rows(0).IsNull("descripcion_paquete") Then
                lbl_plan.Text = ""
            Else
                lbl_plan.Text = dt.Rows(0)("descripcion_paquete")
            End If
            lbl_deeler.Text = Session("NombreDistribuidor")
            If dt.Rows(0).IsNull("autos") Then
                lbl_auto.Text = ""
            Else
                lbl_auto.Text = dt.Rows(0)("autos")
            End If
            If dt.Rows(0).IsNull("anio") Then
                lbl_modelo.Text = ""
            Else
                lbl_modelo.Text = dt.Rows(0)("anio")
            End If
            If dt.Rows(0).IsNull("serie") Then
                lbl_serie.Text = ""
            Else
                lbl_serie.Text = dt.Rows(0)("serie")
            End If
            If dt.Rows(0).IsNull("renave") Then
                lbl_ranave.Text = ""
            Else
                lbl_ranave.Text = dt.Rows(0)("renave")
            End If
            If dt.Rows(0).IsNull("placas") Then
                lbl_placa.Text = ""
            Else
                lbl_placa.Text = dt.Rows(0)("placas")
            End If
            If dt.Rows(0).IsNull("motor") Then
                lbl_motor.Text = ""
            Else
                lbl_motor.Text = dt.Rows(0)("motor")
            End If
            If dt.Rows(0).IsNull("uso") Then
                lbl_uso.Text = ""
            Else
                lbl_uso.Text = dt.Rows(0)("uso")
            End If
            If dt.Rows(0).IsNull("capacidad") Then
                lbl_capacidad.Text = ""
            Else
                lbl_capacidad.Text = dt.Rows(0)("capacidad")
            End If
            If dt.Rows(0).IsNull("nom") Then
                lbl_cliente.Text = ""
            Else
                lbl_cliente.Text = dt.Rows(0)("nom")
            End If
            If dt.Rows(0).IsNull("dir") Then
                lbl_dom.Text = ""
            Else
                lbl_dom.Text = dt.Rows(0)("dir")
            End If
            If Session("EstatusV") = "N" Then
                lbl_condicion.Text = "Nuevo"
            Else
                lbl_condicion.Text = "Usado"
            End If
            If dt.Rows(0).IsNull("primatotal") Then
                lbl_preal.Text = ""
            Else
                total = dt.Rows(0)("primatotal")
                lbl_preal.Text = Format(total, "$##,##0.00")
                'lbl_preal.Text = dt.Rows(0)("primatotal")
            End If
            If dt.Rows(0).IsNull("PrimTOTAL") Then
                lbl_ppromocion.Text = ""
            Else
                total = dt.Rows(0)("PrimTOTAL")
                lbl_ppromocion.Text = Format(total, "$##,##0.00")
            End If
            'If dt.Rows(0).IsNull("vehiculo") Then
            '    lbl_condicion.Text = ""
            'Else
            '    lbl_condicion.Text = dt.Rows(0)("vehiculo")
            'End If
            If dt.Rows(0).IsNull("der_pol") Then
                lbl_derecho.Text = ""
            Else
                total = dt.Rows(0)("der_pol")
                lbl_derecho.Text = Format(total, "$##,##0.00")
                'lbl_derecho.Text = dt.Rows(0)("der_pol")
            End If
            If dt.Rows(0).IsNull("ivaC") Then
                lbl_iva.Text = ""
            Else
                total = dt.Rows(0)("ivaC")
                lbl_iva.Text = Format(total, "$##,##0.00")
                'lbl_iva.Text = dt.Rows(0)("ivaC")
            End If
            If dt.Rows(0).IsNull("pt") Then
                lbl_total.Text = ""
            Else
                total = dt.Rows(0)("pt")
                lbl_total.Text = Format(total, "$##,##0.00")
                'lbl_total.Text = dt.Rows(0)("pt")
            End If
            If dt.Rows(0).IsNull("cadena") Then
                lbl_leyenda.Text = ""
            Else
                lbl_leyenda.Text = dt.Rows(0)("cadena")
            End If
            If dt.Rows(0).IsNull("Leyenda") Then
                lbl_ley.Text = ""
            Else
                lbl_ley.Text = dt.Rows(0)("Leyenda")
            End If
            If dt.Rows(0).IsNull("vigencia") Then
                lbl_version.Text = ""
            Else
                lbl_version.Text = "Tarifa  " & dt.Rows(0)("vigencia")
            End If
            'If dt.Rows(0).IsNull("Pn") Then
            '    lbl_neta.Text = ""
            'Else
            '    lbl_neta.Text = dt.Rows(0)("Pn")
            'End If

            If dt.Rows(0).IsNull("cobaddf1_m") Then
                cobaddf1 = 0
            Else
                cobaddf1 = dt.Rows(0)("cobaddf1_m")
            End If
            If dt.Rows(0).IsNull("cobaddf3_m") Then
                cobaddf3 = 0
            Else
                cobaddf3 = dt.Rows(0)("cobaddf3_m")
            End If
            If dt.Rows(0).IsNull("cobaddp1_m") Then
                cobaddp1 = 0
            Else
                cobaddp1 = dt.Rows(0)("cobaddp1_m")
            End If
            If dt.Rows(0).IsNull("cobaddp3_m") Then
                cobaddp3 = 0
            Else
                cobaddp3 = dt.Rows(0)("cobaddp3_m")
            End If
            If Session("programa") = 9 Then
                If dt.Rows(0).IsNull("num_contrato") Then
                    lbl_contrato.Text = ""
                Else
                    lbl_contrato.Text = dt.Rows(0)("num_contrato")
                End If
            End If
            If Session("tipopol") = "F" Then
                lbl_Recargo.Visible = True
                lbl_PagoRecargo.Visible = True
                total = dt.Rows(0)("PRIMANETA") * dt.Rows(0)("recargos")
                lbl_PagoRecargo.Text = Format(total, "$##,##0.00")
            Else
                lbl_Recargo.Visible = False
                lbl_PagoRecargo.Visible = False
            End If

            carga_coberturas(dt)
            llena_cob(dt.Rows(0)("id_version"), dt.Rows(0)("id_paquete"), _
            IIf(dt.Rows(0).IsNull("Pn"), 0, dt.Rows(0)("Pn")), _
            cobaddf1, cobaddf3, cobaddp1, cobaddp3, dt.Rows(0)("id_aseguradora"))

        End If
    End Sub

    Public Sub encabezados()
        Dim objrow As TableRow
        Dim objcell As TableCell
        objrow = New TableRow

        objcell = New TableCell
        objcell.Text = "Cobertura"
        objcell.CssClass = "combos_small"
        objcell.Width = Unit.Percentage(33)
        objrow.Controls.Add(objcell)

        objcell = New TableCell
        objcell.Text = "Descripci�n"
        objcell.CssClass = "combos_small"
        objcell.Width = Unit.Percentage(33)
        objrow.Controls.Add(objcell)

        objcell = New TableCell
        objcell.Text = "Factura"
        objcell.CssClass = "combos_small"
        objcell.Width = Unit.Percentage(33)
        objrow.Controls.Add(objcell)

        tb_cobertura.Controls.Add(objrow)
    End Sub

    Public Sub carga_coberturas(ByVal dt As DataTable)
        Dim i As Integer
        Dim objrow As TableRow
        Dim objcell As TableCell
        Try
            encabezados()
            If dt.Rows.Count > 0 Then
                For i = 0 To 2
                    objrow = New TableRow
                    Select Case i
                        Case 0
                            objcell = New TableCell
                            If dt.Rows(0).IsNull("nom1d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("nom1d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf1_d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf1_d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf1_f") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf1_f")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)
                        Case 1
                            objcell = New TableCell
                            If dt.Rows(0).IsNull("nom2d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("nom2d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf2_d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf2_d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf2_f") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf2_f")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)
                        Case 2
                            objcell = New TableCell
                            If dt.Rows(0).IsNull("nom3d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("nom3d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf3_d") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf3_d")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)

                            objcell = New TableCell
                            If dt.Rows(0).IsNull("cobaddf3_f") Then
                                objcell.Text = ""
                            Else
                                objcell.Text = dt.Rows(0)("cobaddf3_f")
                            End If
                            objcell.CssClass = "obligatorio_negro"
                            objrow.Controls.Add(objcell)
                    End Select
                    tb_cobertura.Controls.Add(objrow)
                Next
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub encabezados_cob()
        Dim objrow As TableRow
        Dim objcell As TableCell
        objrow = New TableRow

        objcell = New TableCell
        objcell.Text = "Coberturas"
        objcell.CssClass = "Interiro_tabla_centro"
        objcell.Width = Unit.Percentage(30)
        objrow.Controls.Add(objcell)

        objcell = New TableCell
        objcell.Text = "Limites"
        objcell.CssClass = "Interiro_tabla_centro"
        objcell.Width = Unit.Percentage(30)
        objrow.Controls.Add(objcell)

        objcell = New TableCell
        objcell.Text = "Deducibles"
        objcell.CssClass = "Interiro_tabla_centro"
        objcell.Width = Unit.Percentage(20)
        objrow.Controls.Add(objcell)

        objcell = New TableCell
        objcell.Text = "Primas"
        objcell.CssClass = "Interiro_tabla_centro"
        objcell.Width = Unit.Percentage(20)
        objrow.Controls.Add(objcell)

        tb_cob.Controls.Add(objrow)

    End Sub

    Public Sub llena_cob(ByVal intversion As Integer, ByVal intpaquete As Integer, ByVal pn As Double, _
        ByVal Equipo As Double, ByVal blindaje As Double, _
        ByVal sustituto As Double, ByVal rc As Double, ByVal intaseguradora As Integer)
        Dim deduc As Double = 0
        Dim total As Double = 0
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim banG As Integer = 0
        Dim cad() As String
        Dim banCantidad As Integer = 0

        'Dim dt As DataTable = ccliente.reporte_limpresp(intversion, intpaquete)
        Dim dt As DataTable = ccliente.reporte_limpresp_poliza(intversion, intpaquete, Session("bid"), Session("idgrdcontrato"), intaseguradora, Session("EstatusV"))
        Dim i, j As Integer
        Try
            If dt.Rows.Count > 0 Then
                encabezados_cob()
                For i = 0 To dt.Rows.Count - 1
                    objrow = New TableRow
                    For j = 0 To 3
                        objcell = New TableCell
                        If j = 3 Then
                            If dt.Rows(i)(j) > 0 Then
                                total = dt.Rows(i)(j)
                                If banG = 1 Then
                                    total = total * lbl_capacidad.Text
                                End If
                                Select Case banCantidad
                                    Case 1
                                        'equipo especial
                                        total = Equipo
                                    Case 2
                                        'blindaje
                                        total = blindaje
                                    Case 3
                                        'sustituto
                                        total = sustituto
                                    Case 4
                                        'rc
                                        total = rc
                                End Select
                                objcell.Text = Format(total, "$##,##0.##")
                            Else
                                objcell.Text = ""
                            End If
                        Else
                            If j = 1 And banG = 1 Then
                                total = dt.Rows(i)(j)
                                If Not Session("aseguradora") = 1 Then
                                    total = total * lbl_capacidad.Text
                                End If
                                objcell.Text = Format(total, "$##,##0.##")
                                banG = 0
                            Else
                                If dt.Rows(i).IsNull(j) Then
                                    objcell.Text = ""
                                Else
                                    If IsNumeric(dt.Rows(i)(j)) Then
                                        total = dt.Rows(i)(j)
                                        If Mid(dt.Rows(i)(j), 1, 1) = 0 Then
                                            total = total * 100
                                            If (dt.Rows(i)(0) = "Danos Materiales" Or dt.Rows(i)(0) = "Da�os Materiales") And _
                                                ccliente.determina_banderas_X_programa(Session("programa"), intaseguradora, 0) = 1 Then
                                                deduc = ccliente.determinando_deducpl(intaseguradora, intpaquete, _
                                                Session("subramo"), Session("programa"), Session("EstatusV"), _
                                                    Session("ban"), Session("anio"), Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                If Session("tipopol") = "M" Or Session("tipopol") = "F" Then
                                                    objcell.Text = Format((total), "##,###.##") & "% total, " & deduc & "% P. Parcial"
                                                Else
                                                    objcell.Text = Format((total), "##,###.##") & "% total, " & Format(deduc, "$##,##0.##") & " P. Parcial"
                                                End If
                                            Else
                                                objcell.Text = Format((total), "##,###.##") & " %"
                                            End If
                                        Else
                                            objcell.Text = Format(total, "$##,##0.##")
                                        End If
                                    Else
                                        If dt.Rows(i)(j) = "Valor Factura" Then
                                            'cad = lbl_leyenda.Text.Split("/")
                                            'total = cad(2)
                                            'objcell.Text = dt.Rows(i)(j) & "  " & Format(total, "$##,##0.00")
                                            objcell.Text = dt.Rows(i)(j)
                                        Else
                                            Select Case dt.Rows(i)(j)
                                                Case "Equipo Especial"
                                                    banCantidad = 1
                                                Case "Blindaje"
                                                    banCantidad = 2
                                                Case "Auto Sustituto"
                                                    banCantidad = 3
                                                Case "Ext. Res. Civil"
                                                    banCantidad = 4
                                            End Select
                                            objcell.Text = dt.Rows(i)(j)
                                        End If
                                        If Trim(dt.Rows(i)(j)) = "Gastos M�dicos (LUC)" _
                                            Or Trim(dt.Rows(i)(j)) = "Gastos Medicos (LUC)" _
                                            Or Trim(dt.Rows(i)(j)) = "Gastos M�dicos" _
                                            Or Trim(dt.Rows(i)(j)) = "Gastos Medicos" _
                                            Or Trim(dt.Rows(i)(j)) = "Gastos Medicos Ocupantes" Then
                                            banG = 1
                                        End If

                                    End If
                                End If
                            End If
                        End If
                        objcell.CssClass = "obligatorio_negro"
                        objrow.Controls.Add(objcell)
                    Next
                    tb_cob.Controls.Add(objrow)
                Next
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = ""
            objrow.Controls.Add(objcell)

            objcell = New TableCell
            objcell.Text = ""
            objrow.Controls.Add(objcell)

            objcell = New TableCell
            objcell.Text = "Prima Neta"
            objcell.CssClass = "obligatorio_negro"
            objrow.Controls.Add(objcell)

            objcell = New TableCell
            total = pn
            objcell.Text = Format(total, "$##,##0.00")
            'objcell.Text = pn
            objcell.CssClass = "obligatorio_negro"
            objrow.Controls.Add(objcell)

            tb_cob.Controls.Add(objrow)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regresar.Click
        Response.Redirect("WfrmImp.aspx?band=2")
    End Sub

    Private Sub cmd_imprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_imprimir.Click
        Dim cadenaEnvio As String
        Try
            Session("Bandera_Espera") = 1
            Dim llave As Integer = ccliente.inserta_contenedor_reportes(Session("bid"), lbl_poliza.Text, "P")
            Session("Llave") = llave

            If Session("aseguradora") = 1 Then
                Session("LlaveC") = ccliente.inserta_contenedor_reportes(Session("bid"), lbl_poliza.Text, "P")
            Else
                Session("LlaveC") = 0
            End If

            'cadenaEnvio = "8||||||||||||" & Session("cliente") & "||" & _
            '        Session("bid") & "|" & Session("idpoliza") & "|" & _
            '        Session("idcontrato") & "|" & llave & "|" & _
            '        Session("idgrdcontrato") & "|" & Session("NombreDistribuidor") & "|" & _
            '        Session("aseguradora") & "|" & Session("EstatusV")

            If Session("aseguradora") = 1 Then
                cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
                        Session("bid") & "|" & Session("cliente") & "|" & _
                        Session("idcontrato") & "|" & Session("idgrdcontrato") & "|" & _
                        Session("Llave") & "|" & Session("LlaveC") & "|0|0"
            Else
                If Session("programa") = 4 Or Session("programa") = 5 Or Session("programa") = 9 Then
                    'Parametros = "6 |36|181|1|5|8|170|0|4|1"
                    cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
                            Session("bid") & "|" & Session("cliente") & "|" & _
                            Session("idcontrato") & "|" & Session("idgrdcontrato") & "|" & _
                            Session("Llave") & "|0|" & Session("programa") & "|1"
                Else
                    cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
                            Session("bid") & "|" & Session("cliente") & "|" & _
                            Session("idcontrato") & "|" & Session("idgrdcontrato") & "|" & _
                            Session("Llave") & "|0|0|0"
                End If
            End If

            Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            _bsPolizas.Export(cadenaEnvio)
            Response.Redirect("EsperaReportes.html")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
