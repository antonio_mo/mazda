Imports Cn_Cotizador
Public Class WfrmUImprime
    Inherits System.Web.UI.UserControl

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents sd As Ontranet.WebControls.SaveDialog
    Protected WithEvents MsgBox As MsgBox.MsgBox
    Protected WithEvents Lnk_imprime As System.Web.UI.WebControls.HyperLink

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private cgenera As CnCotizador

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Dim dt As DataTable
        Dim i As Integer = 0
        cgenera = New CnCotizador(Session("DsnAcceso"))
        Try
            If Not Page.IsPostBack Or Session("PostBack") Then
                If Request.QueryString("url") Is Nothing Then
                    If Not Session("Bandera_Espera") Is Nothing Then
                        dt = cgenera.Consulta_contenedor(Session("Llave"), Session("LlaveC"))
                        If dt.Rows.Count > 0 Then
                            Lnk_imprime.Text = dt.Rows(i)("nombre_reporte")
                            Lnk_imprime.NavigateUrl = "wfrmImprime.aspx?url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                            Lnk_imprime.Target = "contenido"
                        End If
                    End If
                Else
                    Dim url As String = Request.QueryString("url")
                    Dim nombre As String = Request.QueryString("nomb")
                    url = "C:/Inetpub/wwwroot/CotizadorContado_QA/" & url
                    Session("nombrepdf") = nombre
                    Session("urlpdf") = url
                    sd.FileName = nombre
                    sd.File = url
                    sd.ShowDialogBox()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

End Class
