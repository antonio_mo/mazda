Imports Emisor_Conauto
Imports CN_Negocios

Partial Class WfrmClientesP
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                lbl_cotizacion.Visible = False
                rb_tipo.Visible = False
                Session("PostBack") = False
                ViewState("iva") = 0
                pnl_dir.Visible = True
                cmd_imprimir.Visible = False

                If Not Request.QueryString("url") Is Nothing Then
                    sd.FileName = IIf(Request.QueryString("nomb") Is Nothing, "", Request.QueryString("nomb"))
                    sd.File = "D:/Aplicaciones/CotizadorContado_Conauto/" & Request.QueryString("url")
                    sd.ShowDialogBox()
                Else
                    If Request.QueryString("cveporenganche") Is Nothing Then
                        Session("cveporenganche") = 0
                    Else
                        Session("cveporenganche") = Request.QueryString("cveporenganche")
                    End If

                    If Not Request.QueryString("cvevida") Is Nothing Then
                        'cprincipal.vida = Request.QueryString("cvevida")
                        txt_vida.Text = Request.QueryString("cvevida")
                    End If
                    If Not Request.QueryString("cvedesempleo") Is Nothing Then
                        'cprincipal.desempleo = Request.QueryString("cvedesempleo")
                        txt_desempleo.Text = Request.QueryString("cvedesempleo")
                    End If

                    'If Request.QueryString("cvecoti") Is Nothing Then
                    If Request.QueryString("cveclicoti") Is Nothing Then
                        If Session("programa") = 4 Or Session("programa") = 5 Or Session("programa") = 9 Or Session("programa") = 11 Then
                            Session("cotiza") = ccliente.maximo_cotiza
                        Else
                            Session("cotiza") = 0
                        End If
                    End If
                    Select Case Session("contribuyente")
                        Case 0
                            lbl_persona.Text = "F�sica"
                        Case 1
                            lbl_persona.Text = "Act. Empresarial"
                        Case 2
                            lbl_persona.Text = "Moral"
                    End Select
                    rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(Session("contribuyente")))
                    personalidad(rb_tipo)
                    cmd_agregar.Visible = True
                    cmd_modificar.Visible = False
                    pnl_clientes.Visible = False
                    txt_razon.CssClass = ""
                    cmd_imprimir.Visible = False
                    If Session("ClienteCotiza") = 1 Then
                        txt_clave.Text = Session("cliente")
                        carga_datos_cliente(txt_clave.Text)
                    End If
                    If Not Request.QueryString("cveclicoti") Is Nothing Then
                        recarga_clienteP(Request.QueryString("cveclicoti"))
                    End If
                    If Session("programa") = 4 Or Session("programa") = 5 Then 'nuevo
                        txt_cp.Text = Session("CP")
                        txt_cp.Enabled = False
                    End If
                End If
            End If
            'If Session("Cotizacion") = 0 Then
            '    cmd_imprimir.Visible = True
            'Else
            '    cmd_imprimir.Visible = False
            'End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub grdcatalogo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdcatalogo.SelectedIndexChanged
        txt_clave.Text = 0
        viewstate("NombreCliente") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(0).Text
        viewstate("TipoGrd") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(3).Text
        carga_clientep(0)
        pnl_clientes.Visible = False
        cmd_modificar.Visible = True
        cmd_agregar.Visible = False
        'If Session("Cotizacion") = 0 Then
        '    cmd_imprimir.Visible = True
        'Else
        '    cmd_imprimir.Visible = False
        'End If
    End Sub

    Public Sub carga_datos_cliente(ByVal intclave As Integer)
        carga_clientep(intclave)
        pnl_clientes.Visible = False
        cmd_modificar.Visible = False
        cmd_agregar.Visible = False
        cmd_imprimir.Visible = True
    End Sub

    Public Sub carga_clientep(ByVal intclave As Integer)
        Dim dt As DataTable = ccliente.DatosClienteP(Session("bid"), viewstate("NombreCliente"), viewstate("TipoGrd"), intclave)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("id_clientep") Then
                txt_clave.Text = dt.Rows(0)("id_clientep")
            Else
                txt_clave.Text = 0
            End If
            If Not dt.Rows(0).IsNull("nombreP") Then
                txt_nombre.Text = dt.Rows(0)("nombreP")
            Else
                txt_nombre.Text = ""
            End If
            If Not dt.Rows(0).IsNull("apaternoP") Then
                txt_apaterno.Text = dt.Rows(0)("apaternoP")
            Else
                txt_apaterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("amaternop") Then
                txt_amaterno.Text = dt.Rows(0)("amaternop")
            Else
                txt_amaterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("cp") Then
                txt_cp.Text = dt.Rows(0)("cp")
            Else
                txt_cp.Text = ""
            End If
            If Not dt.Rows(0).IsNull("estado") Then
                lbl_estado.Text = dt.Rows(0)("estado")
            Else
                lbl_estado.Text = ""
            End If
            If Not dt.Rows(0).IsNull("provincia") Then
                lbl_provincia.Text = dt.Rows(0)("provincia")
            Else
                lbl_provincia.Text = ""
            End If
            If Not dt.Rows(0).IsNull("tipopersonap") Then
                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("tipopersonap")))
                If dt.Rows(0)("tipopersonap") = 2 Then
                    habilita()
                    If Not dt.Rows(0).IsNull("razon_socialp") Then
                        txt_razon.Text = dt.Rows(0)("razon_socialp")
                    Else
                        txt_razon.Text = ""
                    End If
                Else
                    txt_razon.Enabled = False
                    txt_razon.CssClass = ""
                    txt_razon.CssClass = "negrita"
                    txt_nombre.Enabled = True
                    txt_apaterno.Enabled = True
                    txt_amaterno.Enabled = True
                    txt_nombre.CssClass = "combos_small"
                    txt_apaterno.CssClass = "combos_small"
                    txt_amaterno.CssClass = "combos_small"
                End If
            End If
            If Not dt.Rows(0).IsNull("telefonop") Then
                txt_tel.Text = dt.Rows(0)("telefonop")
            Else
                txt_tel.Text = ""
            End If
        End If
    End Sub


    'Public Sub inserta_contrato()
    '    Dim asfecha() As String
    '    Dim dFecha As String = ""
    '    Dim Dnacimiento As String = ""

    '    asFecha = Split(Session("FechaInicio"), "/")
    '    If Session("fecha") = 1 Then
    '        Dnacimiento = asFecha(1) & "/" & asFecha(0) & "/" & asFecha(2)
    '    Else
    '        Dnacimiento = asFecha(0) & "/" & asFecha(1) & "/" & asFecha(2)
    '    End If

    '    Dim max As Integer = Session("plazo") / 12
    '    Dim fechaf As String = DateAdd(DateInterval.Month, max, CDate(Session("FechaInicio")))
    '    asfecha = Split(fechaf, "/")
    '    If Session("fecha") = 1 Then
    '        dFecha = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
    '    Else
    '        dFecha = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
    '    End If

    '    Session("idcontrato") = ccliente.Inserta_contrato_coti(Session("cliente"), Session("bid"), _
    '            Session("plazo"), 0, Dnacimiento, Session("EstatusV"), _
    '            Session("contrato"), dFecha, Session("idgrdcontrato"))
    'End Sub

    'Public Sub inserta_poliza()
    '    Dim venta As String = "F"
    '    Dim arr1() As String = Session("ArrPoliza")
    '    Dim arr2() As String = Session("subsidios")
    '    Dim cadena As String
    '    Dim totalban As Double = 0

    '    Dim asFecha() As String
    '    Dim finicio As String = ""
    '    cadena = "Ninguno"
    '    finicio = Session("FechaInicio")
    '    Session("idpoliza") = ccliente.inserta_poliza_coti(Session("bid"), Session("polizainicio"), Session("tipopol"), _
    '    Session("seguro"), Session("plazo"), Session("paquete"), Session("opcsa"), _
    '    finicio, Session("NombreDistribuidor"), Session("moneda"), Session("vehiculo"), _
    '    arr1, "", Session("intuso"), Session("precioEscrito"), _
    '    "", Session("contrato"), _
    '    Session("resultado")(0), Session("resultado")(2), Session("resultado")(4), _
    '    Session("resultado")(1), Session("bUnidadfinanciada"), Session("zurich"), cadena, _
    '    Session("plazo"), arr2, Session("resultado")(5), _
    '    Session("precioEscrito"), Session("version"), Session("EstatusV"), _
    '    Session("zurich"), venta, Session("cliente"), Session("region"), Session("idgrdcontrato"), _
    '    Session("idcontrato"), Session("fecha"), Session("FechaInicio"), Session("usuario"), "")
    'End Sub

    Public Function valida_informacion() As Integer
        Dim bandera As Integer = 0
        Try
            If rb_tipo.SelectedValue <> 2 Then
                If txt_nombre.Text = "" Then
                    MsgBox.ShowMessage("El Nombre del Cliente no puede quedar en blanco")
                    bandera = 1
                End If
                If txt_apaterno.Text = "" Then
                    MsgBox.ShowMessage("El Apellido Paterno no puede quedar en blanco")
                    bandera = 1
                End If
            Else
                If txt_razon.Text = "" Then
                    MsgBox.ShowMessage("La razon social no puede quedar en blanco")
                    bandera = 1
                End If
            End If

            If txt_tel.Text = "" Then
                MsgBox.ShowMessage("El Telefono no puede quedar en blanco")
                bandera = 1
            Else
                If txt_tel.Text.Length < 8 Then
                    MsgBox.ShowMessage("El n�mero del Telefono debe de contener minimo 8 digitos")
                    bandera = 1
                Else
                    If txt_tel.Text.Length > 13 Then
                        MsgBox.ShowMessage("El n�mero del Telefono debe de contener como maximo 13 digitos")
                        bandera = 1
                    End If
                End If
            End If
            If txt_elaboro.Text = "" Then
                MsgBox.ShowMessage("El Nombre de quien Elabor� la Cotizaci�n no puede quedar en blanco")
                bandera = 1
            End If
            If txt_cp.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el codigo postal")
                bandera = 1
            End If
            If Not IsNumeric(txt_cp.Text) Then
                MsgBox.ShowMessage("el codigo postal debe de ser n�merico")
                bandera = 1
            End If

            Return bandera
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            If valor = 2 Then
                habilita()
            Else
                desabilita()
            End If
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub habilita()
        txt_razon.Enabled = True
        txt_razon.Text = ""
        txt_razon.CssClass = "combos_small"

        txt_nombre.Enabled = False
        txt_apaterno.Enabled = False
        txt_amaterno.Enabled = False
        txt_nombre.Text = ""
        txt_apaterno.Text = ""
        txt_amaterno.Text = ""
        txt_nombre.CssClass = "negrita"
        txt_apaterno.CssClass = "negrita"
        txt_amaterno.CssClass = "negrita"
    End Sub

    Public Sub desabilita()
        txt_razon.Enabled = False
        txt_razon.CssClass = ""
        txt_razon.CssClass = "negrita"

        txt_nombre.Enabled = True
        txt_apaterno.Enabled = True
        txt_amaterno.Enabled = True
        txt_nombre.Text = ""
        txt_apaterno.Text = ""
        txt_amaterno.Text = ""
        txt_nombre.CssClass = "combos_small"
        txt_apaterno.CssClass = "combos_small"
        txt_amaterno.CssClass = "combos_small"
    End Sub

    Private Sub rb_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tipo.SelectedIndexChanged
        personalidad(rb_tipo)
    End Sub

    Public Sub recarga_clienteP(ByVal intcliente As Integer)
        txt_clave.Text = intcliente
        carga_clientep(txt_clave.Text)
        pnl_clientes.Visible = False
        cmd_modificar.Visible = True
        cmd_agregar.Visible = False
    End Sub

    Public Sub determina_badenra_iva(ByVal strcp As String)
        viewstate("iva") = 0
        Dim i As Integer = 0
        Dim dt As DataTable = ccliente.busqueda_iva(cvalida.QuitaCaracteres(txt_cp.Text))
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                If Not dt.Rows(i).IsNull("iva") Then
                    If dt.Rows(i)("iva") <> 0.16 Then
                        viewstate("iva") = 1
                        Exit For
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub grdcatalogo_fondo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdcatalogo_fondo.SelectedIndexChanged

    End Sub

    Private Sub cmd_buscar_cp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar_cp.Click
        Dim i As Integer = 0
        If txt_cp.Text = "" Then
            MsgBox.ShowMessage("Favor de escribir el codigo postal")
            Exit Sub
        End If
        If Not IsNumeric(txt_cp.Text) Then
            MsgBox.ShowMessage("el codigo postal debe de ser n�merico")
            Exit Sub
        End If
        Try
            Dim dt As DataTable = ccliente.busqueda_iva(cvalida.QuitaCaracteres(txt_cp.Text))
            If dt.Rows.Count > 0 Then
                pnl_dir.Visible = True
                If dt.Rows(0).IsNull("nombre_estado") Then
                    lbl_estado.Text = ""
                Else
                    lbl_estado.Text = dt.Rows(0)("nombre_estado")
                End If
                If dt.Rows(0).IsNull("nombre_provincia") Then
                    lbl_provincia.Text = ""
                Else
                    lbl_provincia.Text = dt.Rows(0)("nombre_provincia")
                End If
                For i = 0 To dt.Rows.Count - 1
                    If Not dt.Rows(i).IsNull("iva") Then
                        If dt.Rows(i)("iva") <> 0.16 Then
                            viewstate("iva") = 1
                            Exit For
                        End If
                    End If
                Next
            Else
                'pnl_dir.Visible = False
                MsgBox.ShowMessage("No existe informaci�n que mostrar")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Private Sub cmd_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cliente.Click
        Try
            If pnl_clientes.Visible = False Then
                pnl_clientes.Visible = True
                cmd_imprimir.Visible = False
                Dim dt As DataTable = ccliente.Consulta_cliente_prospecto(Session("bid"), txt_cliente.Text)
                If dt.Rows.Count > 0 Then
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                    Exit Sub
                End If
            Else
                pnl_clientes.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_imprimir.Click
        Dim StrSubsidio As String = ""
        Dim StrPoliza As String = ""
        Dim StrResult As String = ""
        Dim i As Integer
        Try
            If Session("ClienteCotiza") = 0 Then

                If valida_informacion() = 0 Then
                    'IIf(Session("programa") = 7, 1, 0)
                    ccliente.modifica_clientep(cvalida.QuitaCaracteres(txt_apaterno.Text, True), _
                    cvalida.QuitaCaracteres(txt_amaterno.Text, True), _
                        cvalida.QuitaCaracteres(txt_nombre.Text, True), _
                            rb_tipo.SelectedValue, _
                            cvalida.QuitaCaracteres(txt_razon.Text, True), _
                            cvalida.QuitaCaracteres(txt_tel.Text, True), _
                            cvalida.QuitaCaracteres(txt_elaboro.Text, True), "1", _
                            txt_clave.Text, Session("bid"), _
                            cvalida.QuitaCaracteres(txt_cp.Text, True), _
                            cvalida.QuitaCaracteres(lbl_estado.Text, True), _
                            cvalida.QuitaCaracteres(lbl_provincia.Text, True))
                    Session("cliente") = txt_clave.Text


                    For i = 0 To UBound(Session("subsidios"))
                        If i = 0 Then
                            StrSubsidio = Session("subsidios")(i)
                        Else
                            StrSubsidio = StrSubsidio & "|" & Session("subsidios")(i)
                        End If
                    Next
                    For i = 0 To UBound(Session("ArrPoliza"))
                        If i = 0 Then
                            StrPoliza = Session("ArrPoliza")(i)
                        Else
                            StrPoliza = StrPoliza & "|" & Session("ArrPoliza")(i)
                        End If
                    Next
                    For i = 0 To UBound(Session("resultado"))
                        If i = 0 Then
                            StrResult = Session("resultado")(i)
                        Else
                            StrResult = StrResult & "|" & Session("resultado")(i)
                        End If
                    Next

                    Session("Cotizacion") = ccliente.CreaCotizacion(Session("version"), Session("bid"), _
                    Session("programa"), Session("MarcaC"), _
                    Session("vehiculo"), Session("EstatusV"), Session("modelo"), Session("anio"), _
                    Session("FechaInicio"), _
                    Session("precioEscrito"), Session("contribuyente"), Session("Promocion"), Session("plazo"), _
                    Session("plazoFinanciamiento"), Session("folio"), Session("tipopol"), Session("seguro"), _
                    Session("ForPagVida"), Session("subramo"), Session("idgrdcontrato"), Session("cliente"), _
                    Session("paquete"), Session("FechaInicio"), Session("intuso"), Session("Enganche"), _
                    txt_vida.Text, txt_desempleo.Text, Session("CadenaCob"), Session("PaqSelCon"), _
                    Session("Calculos"), StrSubsidio, StrPoliza, _
                    StrResult, Session("MultArrPolisa"), Session("iva"), _
                    Session("contrato"))


                    'Session("cotizacionP") = ccliente.Inserta_cotizacion_p(Session("idgrdcontrato"), _
                    'txt_clave.Text, Session("bid"), Session("vehiculo"), _
                    'Session("plazo"), Session("precioEscrito"), Session("tipopol"), _
                    'cvalida.QuitaCaracteres(txt_elaboro.Text, True))

                    Imprime_cotizacion()

                    'inserta_contrato()
                    'inserta_poliza()

                End If
            Else
                If txt_elaboro.Text = "" Then
                    MsgBox.ShowMessage("El Nombre de quien Elabor� la Cotizaci�n no puede quedar en blanco")
                    Exit Sub
                End If
            End If

            'If Session("Cotizacion") = 0 Then
            '    cmd_imprimir.Visible = True
            'Else
            '    cmd_imprimir.Visible = False
            'End If

            cmd_imprimir.Visible = True

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regresar.Click
        Response.Redirect("WfrmCotiza1.aspx?valcost1=" & Session("precioEscrito") & "")
    End Sub

    Private Sub cmd_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar.Click
        Dim dt As DataTable = ccliente.Consulta_cliente_prospecto(Session("bid"), txt_cliente.Text)
        Try
            If dt.Rows.Count > 0 Then
                grdcatalogo_fondo.DataSource = dt
                grdcatalogo_fondo.DataBind()
                grdcatalogo.DataSource = dt
                grdcatalogo.DataBind()
                grdcatalogo_fondo.Visible = True
                grdcatalogo.Visible = True
            Else
                grdcatalogo_fondo.Visible = False
                grdcatalogo.Visible = False
                MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_agregar.Click
        Try
            cmd_buscar_cp_Click(sender, e)
            If valida_informacion() = 0 Then
                txt_clave.Text = ccliente.Inserta_Clientep(Session("bid"), _
                cvalida.QuitaCaracteres(txt_apaterno.Text, True), _
                cvalida.QuitaCaracteres(txt_amaterno.Text, True), _
                cvalida.QuitaCaracteres(txt_nombre.Text, True), _
                rb_tipo.SelectedValue, _
                cvalida.QuitaCaracteres(txt_razon.Text, True), _
                cvalida.QuitaCaracteres(txt_tel.Text, True), _
                cvalida.QuitaCaracteres(txt_elaboro.Text, True), _
                "1", cvalida.QuitaCaracteres(txt_cp.Text, True), _
                cvalida.QuitaCaracteres(lbl_estado.Text, True), _
                cvalida.QuitaCaracteres(lbl_provincia.Text, True))

                MsgBox.ShowMessage("Los datos del cliente se registraron corectamente")
                cmd_agregar.Visible = False
                cmd_modificar.Visible = True
                cmd_imprimir.Visible = True

                'If Session("Cotizacion") = 0 Then
                '    MsgBox.ShowMessage("Para que la C O T I Z A C I � N se registre/n correctamente favor de precionar el boton de/n Generar Cotizaci�n, gracias")
                '    cmd_imprimir.Visible = True
                'Else
                '    cmd_imprimir.Visible = False
                'End If

                MsgBox.ShowMessage("Para que la C O T I Z A C I � N se registre\n correctamente favor de presionar el bot�n de\n Generar Cotizaci�n, gracias")
                cmd_imprimir.Visible = True

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_modificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_modificar.Click
        Try
            cmd_buscar_cp_Click(sender, e)
            If valida_informacion() = 0 Then
                ccliente.modifica_clientep(cvalida.QuitaCaracteres(txt_apaterno.Text, True), _
                cvalida.QuitaCaracteres(txt_amaterno.Text, True), _
                    cvalida.QuitaCaracteres(txt_nombre.Text, True), _
                    rb_tipo.SelectedValue, _
                    cvalida.QuitaCaracteres(txt_razon.Text, True), _
                    cvalida.QuitaCaracteres(txt_tel.Text, True), _
                    cvalida.QuitaCaracteres(txt_elaboro.Text, True), "1", _
                    txt_clave.Text, Session("bid"), _
                    cvalida.QuitaCaracteres(txt_cp.Text, True), _
                    cvalida.QuitaCaracteres(lbl_estado.Text, True), _
                    cvalida.QuitaCaracteres(lbl_provincia.Text, True))
                cmd_imprimir.Visible = True

                'If Session("Cotizacion") = 0 Then
                '    MsgBox.ShowMessage("Para que la C O T I Z A C I � N  se registre\n correctamente favor de precionar el boton de\n Generar Cotizaci�n, gracias")
                '    cmd_imprimir.Visible = True
                'Else
                '    cmd_imprimir.Visible = False
                'End If
                MsgBox.ShowMessage("Para que la C O T I Z A C I � N se registre\n correctamente favor de presionar el bot�n de\n Generar Cotizaci�n, gracias")
                cmd_imprimir.Visible = True
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function Imprime_cotizacion()
        Dim cadenaEnvio As String = ""
        Dim strpoliza As String = ""
        Dim llave As Integer
        Session("LlaveC") = 0
        Dim version As String = ccliente.carga_version(Session("version"), IIf(Session("aseguradora") Is Nothing, 0, Session("aseguradora")))
        Try

            Session("Bandera_Espera") = 1
            If strpoliza = "" Then
                llave = ccliente.inserta_contenedor_reportes(Session("bid"), "", "C")
            Else
                llave = ccliente.inserta_contenedor_reportes(Session("bid"), strpoliza, "C")
            End If
            Session("Llave") = llave

            lbl_cotizacion.Text = "No. Cotizaci�n : " & Session("cotizacion")
            lbl_cotizacion.Visible = True

            ''''cadenaEnvio = "1|1|" & Session("cotizacionP") & "|" & _
            ''''Session("bid") & "|" & Session("cliente") & "|" & Session("idgrdcontrato") & "|" & _
            ''''Session("vehiculo") & "|" & Session("version") & "|" & Session("EstatusV") & "|" & _
            ''''llave & "|" & Session("programa") & "|" & Session("anio") & "|" & Session("subramo") & "|" & Session("PaqSelCon") & "|0|" & Session("precioEscrito")

            cadenaEnvio = "11|1|" & Session("cotizacion") & "|" & _
            Session("bid") & "|" & Session("cliente") & "|" & Session("idgrdcontrato") & "|" & _
            Session("vehiculo") & "|" & Session("version") & "|" & Session("EstatusV") & "|" & _
            llave & "|" & Session("programa") & "|" & Session("anio") & "|" & Session("subramo") & "|" & _
            Session("PaqSelCon") & "|0|" & Session("precioEscrito")

            Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            _bsPolizas.Export(cadenaEnvio)
            'DllReporte.carga(cadenaEnvio, "E")
            'System.Threading.Thread.Sleep(6000)
            carga_impresion()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub carga_impresion()
        Dim dt As DataTable
        Dim tbrow As TableRow
        Dim tbcell As TableCell
        Dim hp As HyperLink
        Try
            dt = ccliente.Consulta_contenedor(Session("Llave"), Session("LlaveC"))
            If dt.Rows.Count > 0 Then
                tbcell = New TableCell
                tbrow = New TableRow
                hp = New HyperLink

                hp.Text = "Cotizaci�n"
                hp.NavigateUrl = "WfrmClientesP.aspx?url=" & dt.Rows(0)("Url_reporte") & " &nomb=" & dt.Rows(0)("nombre_reporte") & ""
                hp.Target = "contenido"
                tbcell.Controls.Add(hp)
                tbcell.Width = Unit.Percentage(100)
                tbrow.Controls.Add(tbcell)
                tb_imp.Controls.Add(tbrow)
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class

