Imports CN_Negocios

Partial Class WfrmImp
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
        If Session("nivel") = 0 Then
            'evi 17/12/2014 se agre fecha libre 20 a�os
            pc_reporte.From.Increment = -7300 ' -360
            pc_reporte.To.Increment = 7300 '360
        Else
            pc_reporte.From.Increment = 0
            pc_reporte.To.Increment = 4
        End If
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private Cvalida As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Session("Bandera_Espera") = 0

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then


                If Session("programa") = 31 Then
                    lblERepuve.Visible = False
                    txt_renave.Visible = False
                End If

                Session("Llave") = Nothing
                If Request.QueryString("url") Is Nothing Then
                    lbl_bid.Text = Session("bidConauto")
                    'evi 16/12/2013 se agrega la opcion de bid

                    If Session("programa") = 29 Then
                        lbl_integrante.Visible = True
                        lbl_grupo.Visible = True
                        txt_integrante.Visible = True
                        txt_grupo.Visible = True
                        lbl_bid.Visible = True
                        txt_nocontrato.Visible = False
                        lbl_contrato.Text = "Bid :"
                    Else
                        lbl_bid.Visible = False
                        lbl_integrante.Visible = False
                        lbl_grupo.Visible = False
                        txt_integrante.Visible = False
                        txt_grupo.Visible = False
                        txt_nocontrato.Visible = True
                        lbl_contrato.Text = "*No. contrato :"
                        txtContactoCob.Visible = True
                        'txtContactoCob.Text = Session("nombre")
                    End If

                    If (Session("programa") = 2) Then
                        lbl_contrato.Visible = False
                        txt_nocontrato.Visible = False
                    End If

                    If Session("programa") = 1 Or Session("programa") = 2 Then
                        ''Dim lblfechaIni As String = Session("FechaInicio")
                        'lblfecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                        'txt_fecha.Text = ""
                        'pc_reporte.Visible = False
                        'txt_fecha.Visible = False
                        'lblfecha.Visible = True
                        txt_fecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                        pc_reporte.Enabled = True
                        txt_fecha.Enabled = True
                        lblfecha.Text = ""
                        lblfecha.Visible = False
                    Else
                        txt_fecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                        pc_reporte.Enabled = True
                        txt_fecha.Enabled = True
                        lblfecha.Text = ""
                        lblfecha.Visible = False
                    End If

                    If ccliente.determina_banderas_X_programa(Session("programa"), Session("aseguradora"), 14) = 1 Then
                        lbl_factura.Visible = True
                        txt_factura.Visible = True
                    Else
                        lbl_factura.Visible = False
                        txt_factura.Visible = False
                    End If
                    If Not Request.QueryString("cvevida") Is Nothing Then
                        txt_vida.Text = Request.QueryString("cvevida")
                    End If
                    If Not Request.QueryString("cvedesempleo") Is Nothing Then
                        txt_desempleo.Text = Request.QueryString("cvedesempleo")
                    End If
                    If Not Request.QueryString("cveben") Is Nothing Then
                        txt_bene.Text = Request.QueryString("cveben")
                    End If
                    If Not Request.QueryString("band") Is Nothing Then
                        If Request.QueryString("band") = 2 Then
                            crea_tabla(Session("Llave"))
                            pnl_auto.Visible = False
                        Else
                            pnl_auto.Visible = True
                            SetFocus(txt_serie)
                        End If
                    Else
                        limpia()
                        pnl_auto.Visible = True
                        SetFocus(txt_serie)
                    End If
                Else
                    Dim url As String = Request.QueryString("url")
                    Dim nombre As String = Request.QueryString("nomb")
                    'url = "C:/Inetpub/wwwroot/CotizadorContado_Conauto/" & url
                    url = System.Configuration.ConfigurationManager.AppSettings("Raiz").ToString() & url
                    sd.FileName = nombre
                    sd.File = url
                    sd.ShowDialogBox()
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        txt_fecha.Text = ""
        txt_serie.Text = ""
        txt_motor.Text = ""
        txt_renave.Text = ""
        txt_placa.Text = ""
        txt_renave.Text = ""
        txt_contrato.Text = 0
        txt_factura.Text = ""

        'Validando grupo, integrante y no. serie
        If Session("programa") = 29 Then
            txt_grupo.Text = ""
            txt_integrante.Text = ""
            Session("ValidaSerie") = ""
            If ccliente.ValidandoInfInicial(Session("IdCotiUnica")) = 1 Then
                If Not ccliente.StrGrupo = "" Then
                    txt_grupo.Text = ccliente.StrGrupo
                    txt_grupo.Enabled = False
                End If
                If Not ccliente.StrIntegrante = "" Then
                    txt_integrante.Text = ccliente.StrIntegrante
                    txt_integrante.Enabled = False
                End If
                If Not ccliente.StrSerie = "" Then
                    Session("ValidaSerie") = ccliente.StrSerie
                    'txt_serie.Text = ccliente.StrSerie
                    'txt_serie.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Public Sub inserta_poliza(ByVal StrCveFscar As String)
        Dim venta As String = "F"
        Dim arr1() As String = Session("ArrPoliza")
        Dim arr2() As String = Session("subsidios")
        Dim cadena As String
        Dim totalban As Double = 0
        Dim Arr(0, 1) As String
        Dim contador As Integer = 0
        Dim IdAseguradora1 As Integer = 0

        Dim asFecha() As String
        Dim finicio As String = ""
        cadena = "Ninguno"

        venta = "F"

        Dim arrx() As String = Session("ArrPoliza1")
        Dim arrz() As String = Session("ArrPoliza")

        finicio = Session("FechaInicio")

        'Session("precioEscrito"), Session("version"), Session("EstatusV"), _
        If (Session("programa") = 1 Or Session("programa") = 2) And Session("Exclusion") = 0 Then

            IdAseguradora1 = Session("aseguradora1")

            ReDim Arr(1, 1)
            Dim arr11() As String = Session("ArrPoliza1")
            Dim arr22() As String = Session("subsidios1")
            Dim xs As String = Session("TipoProducto")

            'Promocion 12 meses
            Session("idpoliza") = ccliente.inserta_poliza(Session("bid"), 0, Session("tipopol"), _
            Session("seguro"), Session("plazo1"), _
            finicio, Session("NombreDistribuidor"), Session("moneda"), Session("vehiculo"), _
            arr11, Cvalida.valida_cadena(txt_serie.Text, 1, "|"), Session("intuso"), Session("precioEscrito"), _
            Cvalida.valida_cadena(txt_motor.Text, 1, "|"), Session("contrato"), _
            Session("resultado1")(0), Session("resultado1")(2), Session("resultado1")(4), _
            Session("resultado1")(1), Session("bUnidadfinanciada"), "", cadena, _
            Session("plazo1"), arr22, Session("resultado1")(5), _
            Session("precioEscrito"), Session("version"), Session("DatosGenerales1")(8), _
            venta, Session("cliente1"), Session("region"), Session("idgrdcontrato"), _
            Session("idcontrato1"), Session("fecha"), Session("FechaInicio"), Session("usuario"), _
            Cvalida.valida_cadena(txt_renave.Text, 1, "|"), Session("aseguradora1"), Session("anio"), _
            Session("subramo1"), Session("modelo"), Session("paquete1"), Session("programa"), _
            Session("fincon"), IIf(txt_vida.Text = "", 0, txt_vida.Text), _
            IIf(txt_desempleo.Text = "", 0, txt_desempleo.Text), _
            Cvalida.valida_cadena(txt_factura.Text, 1, "|"), Session("subsidios1")(4), _
            IIf(Session("multianual") = 1, Session("Financiamiento"), ""), Session("Fraccion"), _
            StrCveFscar, _
            IIf((Session("programa") = 1 Or Session("programa") = 2), Session("IdCotiUnica"), Session("cotizacion")), _
            0, Session("ArrPoliza1")(13), Session("ArrPoliza1")(14), _
            Session("plazofinanciamiento1"), Session("plazoRecargo1"), _
            Session("DatosGenerales1")(24), Session("DatosGenerales1")(25), _
            Parametros_poliza_zurich(Session("cliente1"), Session("aseguradora1"), Session("idcontrato1"), Session("idgrdcontrato")), _
            Session("Tipocarga"), Session("TipoProducto"), contador, Session("IdCotizaCobertura"))
            'Session("Tipocarga"), Session("Exclusion"), contador)

            finicio = DateAdd(DateInterval.Month, 12, Session("FechaInicio"))
            Arr(0, 0) = Session("idpoliza")
            Arr(0, 1) = Session("aseguradora1")
            contador = contador + 1
        End If

        'Cuando sea poliza cruzada y la poliza 2 sea Axa se valida que el tipoproducto sea 8 para que tome
        'el serial DW4
        If contador <> 0 Then
            If IdAseguradora1 = "62" And Session("aseguradora") = "63" And Session("aseguradora") = "65" Then
                Session("TipoProducto") = 8
            End If
        End If

        'Session("precioEscrito"), Session("version"), Session("EstatusV"), _
        'aseguradora seleccionada
        Session("idpoliza") = ccliente.inserta_poliza(Session("bid"), 0, Session("tipopol"), _
        Session("seguro"), Session("plazo"), _
        finicio, Session("NombreDistribuidor"), Session("moneda"), Session("vehiculo"), _
        arr1, Cvalida.valida_cadena(txt_serie.Text, 1, "|"), Session("intuso"), Session("precioEscrito"), _
        Cvalida.valida_cadena(txt_motor.Text, 1, "|"), Session("contrato"), _
        Session("resultado")(0), Session("resultado")(2), Session("resultado")(4), _
        Session("resultado")(1), Session("bUnidadfinanciada"), "", cadena, _
        Session("plazo"), arr2, Session("resultado")(5), _
        Session("precioEscrito"), Session("version"), Session("DatosGenerales")(8), _
        venta, Session("cliente"), Session("region"), Session("idgrdcontrato"), _
        Session("idcontrato"), Session("fecha"), finicio, Session("usuario"), _
        Cvalida.valida_cadena(txt_renave.Text, 1, "|"), Session("aseguradora"), Session("anio"), _
        Session("subramo"), Session("modelo"), Session("paquete"), Session("programa"), _
        Session("fincon"), IIf(txt_vida.Text = "", 0, txt_vida.Text), _
        IIf(txt_desempleo.Text = "", 0, txt_desempleo.Text), _
        Cvalida.valida_cadena(txt_factura.Text, 1, "|"), Session("subsidios")(4), _
        IIf(Session("multianual") = 1, Session("Financiamiento"), ""), Session("Fraccion"), _
        StrCveFscar, _
        IIf(Session("programa") = 29, Session("IdCotiUnica"), Session("cotizacion")), _
        0, Session("ArrPoliza")(13), Session("ArrPoliza")(14), _
        Session("plazofinanciamiento"), Session("plazoRecargo"), _
        Session("DatosGenerales")(24), Session("DatosGenerales")(25), _
        Parametros_poliza_zurich(Session("cliente1"), Session("aseguradora"), Session("idcontrato"), Session("idgrdcontrato")), _
        Session("Tipocarga"), Session("TipoProducto"), contador, Session("IdCotizaCobertura"))
        'Session("Tipocarga"), Session("Exclusion"), contador)


        If (Session("programa") = 1 Or Session("programa") = 2) And Session("Exclusion") = 0 Then
            Arr(1, 0) = Session("idpoliza")
            Arr(1, 1) = Session("aseguradora")
        Else
            Arr(0, 0) = Session("idpoliza")
            Arr(0, 1) = Session("aseguradora")
        End If
        Session("Arrpoliza") = Arr

        If Session("idpoliza") = 0 Then
            MsgBox.ShowMessage("Favor de consultar con su administrador, la p�liza no puede ser emitida")
            pnl_auto.Visible = True
            cmd_continuar.Visible = False
            Exit Sub
        End If

    End Sub

    Public Function valida_auto() As Integer
        Dim bandera As Integer = 0
        Try
            'If Not Session("programa") = 29 Then
            '    If txt_fecha.Text = "" Then
            '        MsgBox.ShowMessage("La fecha de siniestro no puede quedar en Blanco, Verifiquelo")
            '        bandera = 1
            '    Else
            '        Session("FechaInicio") = txt_fecha.Text
            '    End If
            'Else
            '    Session("FechaInicio") = lblfecha.Text
            'End If
            If txt_fecha.Text = "" Then
                MsgBox.ShowMessage("La fecha de siniestro no puede quedar en Blanco, Verifiquelo")
                bandera = 1
            Else
                Session("FechaInicio") = txt_fecha.Text
            End If
            If Session("programa") = 1 Then
                If txt_nocontrato.Text = "" Then
                    MsgBox.ShowMessage("El n�mero de contrato no puede quedar en blanco, Verifiquelo")
                    bandera = 1
                    'Else
                    '    Session("contrato") = txt_nocontrato.Text
                Else

                    If Len(txt_nocontrato.Text) > 15 Then
                        MsgBox.ShowMessage("El n�mero de contrato debe de ser menor de 15 d�gitos, Verifiquelo")
                        bandera = 1
                    End If
                End If
            ElseIf Session("programa") <> 2 Then
                If txt_grupo.Text = "" Then
                    MsgBox.ShowMessage("El n�mero de grupo no puede quedar en blanco, Verifiquelo")
                    bandera = 1
                Else
                    If txt_integrante.Text = "" Then
                        MsgBox.ShowMessage("El n�mero de integrante no puede quedar en blanco, Verifiquelo")
                        bandera = 1
                        'Else
                        '    Session("contrato") = txt_grupo.Text.Trim & txt_integrante.Text.Trim
                    End If
                End If

                If Not IsNumeric(txt_grupo.Text) Then
                    MsgBox.ShowMessage("El n�mero de grupo debe ser solo d�gitos, Verifiquelo")
                    bandera = 1
                End If

                If Not IsNumeric(txt_integrante.Text) Then
                    MsgBox.ShowMessage("El n�mero de integrante debe ser solo d�gitos, Verifiquelo")
                    bandera = 1
                End If

            End If

            If txt_serie.Text = "" Then
                MsgBox.ShowMessage("El n�mero de serie no puede quedar en blanco, Verifiquelo")
                bandera = 1
            ElseIf txt_serie.Text.Length < 17 Then
                MsgBox.ShowMessage("El n�mero de Serie debe de ser igual a 17 caracteres, Verifiquelo")
                bandera = 1
            End If

            If txt_motor.Text = "" Then
                MsgBox.ShowMessage("El n�mero de motor no puede quedar en blanco, Verifiquelo")
                bandera = 1
            End If

            If Not txt_renave.Text = "" Then
                If txt_renave.Text.Length < 8 Then
                    MsgBox.ShowMessage("El n�mero de carateres para Repuve son de 8, Verifiquelo")
                    bandera = 1
                End If
            End If

            If ccliente.determina_banderas_X_programa(Session("programa"), Session("aseguradora"), 14) = 1 Then
                If txt_factura.Text = "" Then
                    MsgBox.ShowMessage("El n�mero de factura no puede quedar en blanco, Verifiquelo")
                    bandera = 1
                End If
            End If

            If Not txtCorreoContacto.Text = "" Then
                If Not Cvalida.ValidarCorreo(txtCorreoContacto.Text) Then
                    MsgBox.ShowMessage("El correo no es correcto, Verifiquelo")
                    bandera = 1
                End If
            End If

            Return bandera
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Function Parametros_poliza_zurich(ByVal Idcliente As Integer, ByVal idaseguradora As Integer, _
    ByVal IdContrato As Integer, ByVal idgrdcontrato As Integer) As String
        Dim cadenaEnvio As String
        Try
            'cadenaEnvio = Session("aseguradora") & "|txtIdpolizaD|" & _
            'Session("bid") & "|" & Idcliente & "|" & _
            'Session("idcontrato") & "|" & Session("idgrdcontrato") & _
            '"|txtLlave|0|" & Session("programa") & "|0"

            cadenaEnvio = idaseguradora & "|txtIdpolizaD|" & _
            Session("bid") & "|" & Idcliente & "|" & _
            IdContrato & "|" & idgrdcontrato & _
            "|txtLlave|0|" & Session("programa") & "|0"

            Return cadenaEnvio
            'Dim strpoliza As String = ""

            '    Dim dt As DataTable = ccliente.carga_reporte_poliza(Session("idpoliza"), Session("bid"), Session("cliente"), Session("idcontrato"), Session("aseguradora"))
            '    If dt.Rows.Count > 0 Then
            '        'If dt.Rows(0).IsNull("poliza_multianual") Then
            '        If dt.Rows(0).IsNull("poliza") Then
            '            strpoliza = ""
            '        Else
            '            strpoliza = dt.Rows(0)("poliza")
            '        End If
            '    End If

            '    Session("Bandera_Espera") = 1
            '    Dim llave As Integer = ccliente.inserta_contenedor_reportes(Session("bid"), strpoliza, "P")
            '    Session("Llave") = llave

            '    Session("LlaveC") = 0


            '    cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
            '    Session("bid") & "|" & Session("cliente") & "|" & _
            '    Session("idcontrato") & "|" & Session("idgrdcontrato") & "|" & _
            '    Session("Llave") & "|0|" & Session("programa") & "|0"

            '    ccliente.carga_cadena_impresion(Session("idpoliza"), strpoliza, cadenaEnvio, 0)

            '    CreaPDF.carga(cadenaEnvio, "P")
            '    System.Threading.Thread.Sleep(3500)

            'Session("bid") & "|" & Session("cliente") & "|" & _

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    'Public Sub crea_tabla(ByVal intllave As Integer)
    Public Sub crea_tabla(ByVal intllave As String)
        Dim hp As HyperLink
        Dim i, j As Integer
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arr() As String = intllave.Split("*")
        Dim arrPol(,) As String
        arrPol = Session("Arrpoliza")
        tb_imprime.Controls.Clear()

        For j = 0 To UBound(arr)
            'Dim dt As DataTable = ccliente.carga_tabla_impresion(intllave)
            Dim dt As DataTable = ccliente.carga_tabla_impresion(arr(j))
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    tbrow = New TableRow
                    tbcell = New TableCell
                    hp = New HyperLink
                    If dt.Rows(i)("tipo") = "P" Then
                        If Not dt.Rows(i).IsNull("nombre_reporte") Then
                            hp.Text = dt.Rows(i)("nombre_reporte")
                        Else
                            hp.Text = ""
                        End If
                        If Not dt.Rows(i).IsNull("url_reporte") Then
                            'hp.NavigateUrl = "WfrmImp.aspx?cvellave=" & intllave & "&url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                            hp.NavigateUrl = "WfrmImp.aspx?cvellave=" & arr(j) & "&url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                        Else
                            hp.NavigateUrl = ""
                        End If
                        hp.Width = Unit.Percentage(100)
                        hp.ForeColor = New System.Drawing.Color().DarkBlue
                        tbcell.Controls.Add(hp)
                        tbcell.CssClass = "negrita"
                        tbrow.Controls.Add(tbcell)
                        tb_imprime.Controls.Add(tbrow)

                        tbrow = New TableRow
                        tbcell = New TableCell
                        hp = New HyperLink
                        hp.Text = "Condiciones Generales"
                        hp.NavigateUrl = "WfrmImp.aspx?cvellave=" & arr(j) & "&url=" & CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS
                        Select Case arrPol(j, 1)
                            Case 1, 4
                                hp.NavigateUrl += "CondicionesQualitas.pdf"
                            Case 2, 5
                                hp.NavigateUrl += "CondicionesChubb.pdf"
                            Case 3, 6
                                hp.NavigateUrl += "CondicionesHDI.pdf"
                        End Select
                        hp.NavigateUrl += " &nomb=CondicionesGenerales.pdf"
                        hp.Width = Unit.Percentage(100)
                        hp.ForeColor = New System.Drawing.Color().DarkBlue
                        tbcell.Controls.Add(hp)
                        tbcell.CssClass = "negrita"
                        tbrow.Controls.Add(tbcell)
                        tb_imprime.Controls.Add(tbrow)
                    Else
                        If Not dt.Rows(i).IsNull("nombre_reporte") Then
                            hp.Text = dt.Rows(i)("nombre_reporte")
                        Else
                            hp.Text = ""
                        End If
                        If Not dt.Rows(i).IsNull("url_reporte") Then
                            'hp.NavigateUrl = "WfrmImp.aspx?cvellave=" & intllave & "&url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                            hp.NavigateUrl = "WfrmImp.aspx?cvellave=" & arr(j) & "&url=" & dt.Rows(i)("Url_reporte") & " &nomb=" & dt.Rows(i)("nombre_reporte") & ""
                        Else
                            hp.NavigateUrl = ""
                        End If
                        hp.Width = Unit.Percentage(100)
                        hp.ForeColor = New System.Drawing.Color().DarkBlue
                        tbcell.Controls.Add(hp)
                        tbcell.CssClass = "negrita"
                        tbrow.Controls.Add(tbcell)
                        tb_imprime.Controls.Add(tbrow)
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub Registra_Poliza()
        Dim strcveFscar As String = ""
        Try
            If Session("idpoliza") = 0 Then
                inserta_poliza(strcveFscar)
                If Session("idpoliza") = 0 Then
                    MsgBox.ShowMessage("Favor de consultar con su administrador, la p�liza no puede ser emitida")
                    pnl_auto.Visible = True
                    cmd_continuar.Visible = False
                    Exit Sub
                End If

                ''EHG - SI HA LLEGADO AQUI, HABILITAR CAPA DE ESPERA Y ENVIA LLAMADA A FUNCION DE GENRACION
                Page.RegisterStartupScript("activaEspera", "<script language='JavaScript'>muestraCapa();</script>")
            Else
                MsgBox.ShowMessage("Los datos ya fueron emitidos por lo cual no pueden volver a ser emitidos")
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_continuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_continuar.Click
        Dim Dcontrato As String
        Dim dFecha As String
        Dim asFecha() As String
        Try

            txt_serie.Text = Cvalida.valida_cadena(txt_serie.Text, 1)
            txt_motor.Text = Cvalida.valida_cadena(txt_motor.Text, 1)
            txt_renave.Text = Cvalida.valida_cadena(txt_renave.Text, 1)
            If Session("programa") = 29 Then
                txt_grupo.Text = Cvalida.valida_cadena(txt_grupo.Text, 1)
                txt_integrante.Text = Cvalida.valida_cadena(txt_integrante.Text, 1)
            Else
                txt_nocontrato.Text = Cvalida.valida_cadena(txt_nocontrato.Text, 1)
            End If

            If valida_auto() = 0 Then

                'If Session("programa") = 32 Or Session("programa") = 30 Then
                '    txt_nocontrato.Text = Cvalida.valida_cadena(txt_nocontrato.Text, 1)
                'Else
                '    txt_nocontrato.Text = lbl_bid.Text & Cvalida.valida_cadena(txt_nocontrato.Text, 1)
                'End If
                If Session("programa") = 29 Then
                    txt_nocontrato.Text = txt_grupo.Text & txt_integrante.Text
                    Session("contrato") = lbl_bid.Text & txt_nocontrato.Text
                Else
                    txt_nocontrato.Text = txt_nocontrato.Text
                    If Session("programa") = 1 Or Session("programa") = 2 Then
                        Session("contrato") = txt_nocontrato.Text
                    Else
                        Session("contrato") = lbl_bid.Text & txt_nocontrato.Text
                    End If
                End If

                'evi 17/12/2014 se quita esta validacion
                'If Session("programa") = 29 Or Session("programa") = 32 Then
                '    If Not txt_serie.Text = Session("ValidaSerie") Then
                '        If Not Session("nivel") = 0 Then
                '            MsgBox.ShowMessage("El n�mero de serie no es el mismo que se registro en la cotizaci�n favor de verificarlo")
                '            Exit Sub
                '        End If
                '    End If
                'End If

                'la validacion se comentarios a peticion de ulises
                'solo para poder realizar pruebas 22/07/2011
                'hay que activarlo para produccion 
                'Por indicaciones de Erick Avelar del dia 02/02/2014 
                'que muestre el mensaje si hay alguna que este vigente y que no este cancelada
                Dim dtvs As DataTable = ccliente.valida_noserie(Cvalida.valida_cadena(Trim(txt_serie.Text), 1))
                If dtvs.Rows.Count > 0 Then
                    If Not dtvs.Rows(0).IsNull("bandera") Then
                        If dtvs.Rows(0)("bandera") = 0 Then
                            MsgBox.ShowMessage("No se puede emitir la p�liza ya que el n�mero de serie ya existe, consulte a su administrador")
                            Exit Sub
                        End If
                    End If
                End If

                ''EVI 07/01/2014 verificar esta validacion ya que no deberia de existir
                ''Se agrega validacion por bid solo conauto panel
                'If Session("programa") = 29 Then
                '    If ccliente.validaBidGrupoIntSerie(Session("contrato"), Cvalida.valida_cadena(Trim(txt_serie.Text), 1)) > 0 Then
                '        MsgBox.ShowMessage("El n�mero de Grupo - Integrante - Serie ya existe por lo cual no se puede emitir la p�liza, consulte a su administrador")
                '        Exit Sub
                '    End If
                'End If

                'Dcontrato = Session("FechaInicio")
                asFecha = Split(Session("FechaInicio"), "/")
                If Session("fecha") = 1 Then
                    Dcontrato = asFecha(1) & "/" & asFecha(0) & "/" & asFecha(2)
                Else
                    Dcontrato = asFecha(0) & "/" & asFecha(1) & "/" & asFecha(2)
                End If

                Dim max As Integer = Session("plazo") / 12
                Dim fechaf As String = DateAdd(DateInterval.Month, max, CDate(Session("FechaInicio")))
                'dFecha = fechaf
                asFecha = Split(fechaf, "/")
                If Session("fecha") = 1 Then
                    dFecha = asFecha(1) & "/" & asFecha(0) & "/" & asFecha(2)
                Else
                    dFecha = asFecha(0) & "/" & asFecha(1) & "/" & asFecha(2)
                End If

                txt_contrato.Text = ccliente.Inserta_contrato(Session("cliente"), Session("bid"), _
                Session("plazo"), 0, Dcontrato, Session("EstatusV"), _
                Session("contrato"), dFecha, Session("aseguradora"), _
                Cvalida.valida_cadena(txt_bene.Text, 1), Session("Enganche"), Cvalida.valida_cadena(txtContactoCob.Text, 1))
                Session("idcontrato") = txt_contrato.Text

                If (Session("programa") = 1 Or Session("programa") = 2) Then
                    txt_contrato.Text = ccliente.Inserta_contrato(Session("cliente1"), Session("bid"), _
                    Session("plazo1"), 0, Dcontrato, Session("EstatusV"), _
                    Session("contrato"), dFecha, Session("aseguradora"), _
                    Cvalida.valida_cadena(txt_bene.Text, 1), Session("Enganche"), Cvalida.valida_cadena(txtContactoCob.Text, 1) & " " & Cvalida.valida_cadena(txtCorreoContacto.Text, 1) & " " & Cvalida.valida_cadena(txtTelContacto.Text, 1))
                    Session("idcontrato1") = txt_contrato.Text
                End If

                pnl_auto.Visible = False
                lbl_titulo.Text = "Impresi�n de Documentos"
                Registra_Poliza()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdCreaTabla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreaTabla.Click
        crea_tabla(Session("llave"))
    End Sub

    Private Sub cmdCreaError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreaError.Click
        pnl_auto.Visible = False
        MsgBox.ShowMessage("Se genero un error en el proceso de la generaci�n de la p�liza,\n favor de consultar con su administrador")
    End Sub
    
End Class
