<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmRecotiza.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmRecotiza" %>
<%@ Register TagPrefix="uc2" TagName="WfrmUCobertura" Src="WfrmUCobertura.ascx" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Register TagPrefix="uc1" TagName="WfrmUCobertura" Src="WfrmUCobertura.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmRecotiza</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="../JavaScript/Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="../Css/submodal.css">
		<script language="javascript" type="text/javascript" src="../JavaScript/submodalsource.js"></script>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		 function Cuota(NumArr)
		{
			var x ="WfrmCantidadCuota.aspx?NumArr="+NumArr+""
			initPopUp();
			showPopWin(x, 550, 200, '<font color="#FFFFFF" face=arial><b>Detalle Cuotas y Primas</b></font>' , null);
	    }
	     function Msg()
		{
			var x ="WfrmMsg.aspx"
			initPopUp();
			showPopWin(x, 400, 100, '<font color="#FFFFFF" face=arial><b>Calculo monto a Financiar</b></font>' , null);
	    }
		function PonDiagonal()
		{
			var stCadena=document.Form1.txt_fecha.value;
			if(stCadena.length == 2)
			{
				document.Form1.Form1.value=document.Form1.txt_fecha.value+"/"
			}
			if(stCadena.length == 5)
			{
				document.Form1.txt_fecha.value=document.Form1.txt_fecha.value+"/"
			}	
		}
		
		function ckb_vida(cuenta, strpaquete)
		{
			switch(cuenta)
			{
				case "12":
					var origen = "grd_cobertura__ctl2_Vida_" + strpaquete
					break
				case "13":
					var origen = "grd_cobertura__ctl3_Vida_" + strpaquete
					break
				case "14":
					var origen = "grd_cobertura__ctl4_Vida_" + strpaquete
					break
				case "15":
					var origen = "grd_cobertura__ctl5_Vida_" + strpaquete
					break
				case "16":
					var origen = "grd_cobertura__ctl6_Vida_" + strpaquete
					break
			}
			var elementos = document.getElementById(origen)
			//alert(" Elemento: " + elementos.value + "\n Seleccionado: " + elementos.checked);
			if(elementos.checked==true)
			{
				//Palomeado
				valor =1;
			}
			else
			{
				//no Palomeado
				valor =0;
			}
			var vida = document.getElementById("txt_vida").value
			//alert(creaCadena(vida,strpaquete,valor))
			document.getElementById("txt_vida").value = creaCadena(vida,strpaquete,valor)
		}
		
		function ckb_desempleo(cuenta, strpaquete)
		{
			switch(cuenta)
			{
				case "12":
					var origen = "grd_cobertura__ctl2_Desempleo_" + strpaquete
					break
				case "13":
					var origen = "grd_cobertura__ctl3_Desempleo_" + strpaquete
					break
				case "14":
					var origen = "grd_cobertura__ctl4_Desempleo_" + strpaquete
					break
				case "15":
					var origen = "grd_cobertura__ctl5_Desempleo_" + strpaquete
					break
				case "16":
					var origen = "grd_cobertura__ctl6_Desempleo_" + strpaquete
					break
			}
			var elementos = document.getElementById(origen)
			//alert(" Elemento: " + elementos.value + "\n Seleccionado: " + elementos.checked);
			if(elementos.checked==true)
			{
				//Palomeado
				valor =1;
			}
			else
			{
				//no Palomeado
				valor =0;
			}
			
			var desempleo = document.getElementById("txt_desempleo").value
			//alert(creaCadena(desempleo,strpaquete,valor))
			document.getElementById("txt_desempleo").value = creaCadena(desempleo,strpaquete,valor)
			
			var valcost = document.getElementById("txt_precio").value;
			var cvevida1 = document.getElementById("txt_vida").value;
			var cvedesempleo1 = document.getElementById("txt_desempleo").value;
			var cveanganche1 = document.getElementById("lbl_enganche").value;
			var path = "WfrmCotizaFscar2.aspx?valcost1="+valcost+"&cvevida="+cvevida1+"&cvedesempleo="+cvedesempleo1+"&cveenganche="+cveanganche1+""
			//alert(path)
			window.location.href=path;
		}
		
		function creaCadena(nombreVD,strpaquete,valor)
		{
			var cadena='';
			var arr1 = nombreVD.split("*");	
			for (i=0;  i<arr1.length; i++)
			{
				var arr2 = arr1[i].split("|");	
				if (arr2[0] == strpaquete)
				{
					if (cadena=='')
					{
						cadena = strpaquete +'|'+ valor
					}
					else
					{
						cadena = cadena +'*'+ strpaquete +'|'+ valor
					}
				}
				else
				{
					if (cadena=='')
					{
						cadena = arr2[0] +'|'+ arr2[1]
					}
					else
					{
						cadena = cadena +'*'+ arr2[0] +'|'+ arr2[1]
					}
				}
			}
			return cadena
		}
		
		function SoloUnValor(nombreVD,strpaquete)
		{
			var cadena='';
			var arr1 = nombreVD.split("*");	
			for (i=0;  i<arr1.length; i++)
			{
				var arr2 = arr1[i].split("|");	
				if (arr2[0] == strpaquete)
				{
					return arr2[1]
				}
			}
		}
				
		function redireccion(intaseguradora, intregion, intpaquete, intplazo, nombrepaquete,financiamiento, subramoI, banvida, contribuyente, intplazoFinanciado)
		{
			if ((document.getElementById("txt_contrato").value)=='')
			{
				alert("El numero de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_contrato").focus();
				return 0
			}
			//alert('si')
			var cvevida1 = document.getElementById("txt_vida").value;
			var valcost1 = document.getElementById("txt_precio").value;
			var strfecha1 = document.getElementById("txt_fecha").value;
			var cveenganche1 = 0;
			var cvevidaU = 0; //SoloUnValor(document.getElementById("txt_vida").value,nombrepaquete)
			var cvedesempleo1 = 0; //document.getElementById("txt_desempleo").value;
			var cvedesempleoU = 0; //SoloUnValor(document.getElementById("txt_desempleo").value,nombrepaquete)
			var path = "WfrmRecotiza.aspx?cveAseg="+intaseguradora+"&cvepaquete="+intpaquete+"&cveregion="+intregion+"&cveplazo="+intplazo+"&valcost="+valcost1+"&strfecha="+strfecha1+"&cveenganche="+cveenganche1+"&cvevida="+cvevida1+"&cvedesempleo="+cvedesempleo1+"&cvevidaU="+cvevidaU+"&cvedesempleoU="+cvedesempleoU+"&cvefinancia="+financiamiento+"&subramo="+subramoI+"&cveplazoFinancia="+intplazoFinanciado+"&contrato="+cvecontrato1+""
			//alert(path)
			window.location.href=path;
			
		}
		String.prototype.trim = function() 
		{
			return this.replace(',','');
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="Obligatorio_negro" align="right" width="15%">Marca :</TD>
					<TD width="25%">
						<asp:label id="txt_marca" runat="server" Width="100%" CssClass="small"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right" width="15%">Modelo :</TD>
					<TD width="25%">
						<asp:label id="txt_modelo" runat="server" Width="100%" CssClass="small"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" align="right">Tipo :</TD>
					<TD>
						<asp:label id="txt_tipo" runat="server" Width="100%" CssClass="small"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right">Descripción :</TD>
					<TD>
						<asp:textbox id="txt_descripcion" runat="server" Width="100%" CssClass="small" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" align="right">Precio :</TD>
					<TD>
						<asp:label id="txt_precio" runat="server" Width="100%" CssClass="small"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right">
						<asp:label id="lbl_clasificacion" runat="server" Width="100%">Clasificación :</asp:label></TD>
					<TD>
						<asp:label id="txt_clasificacion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Forma del seguro :</TD>
					<TD>
						<asp:label id="lbl_seguro" runat="server" CssClass="small" Width="100%"></asp:label></TD>
					<TD class="Obligatorio" align="right">Contribuyente :</TD>
					<TD>
						<asp:label id="lbl_persona" runat="server" Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Plazo del arrendamiento :</TD>
					<TD>
						<asp:label id="lbl_plazof" runat="server" Width="100%"></asp:label></TD>
					<TD class="Obligatorio" align="right">Inicio de contrato :</TD>
					<TD>
						<asp:label id="lbl_fechai" runat="server" Width="100%" CssClass="small"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Obligatorio" align="right">Plazo del seguro :
					</TD>
					<TD>
						<asp:label id="lbl_plazo" runat="server" Width="100%"></asp:label></TD>
					<TD class="Obligatorio_negro" align="right">No. contrato</TD>
					<TD>
						<asp:textbox id="txt_contrato" runat="server" Width="40%" CssClass="combos_small" MaxLength="10"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro">
						<asp:dropdownlist id="cbo_plazof" runat="server" CssClass="combos_small" AutoPostBack="True" Visible="False"></asp:dropdownlist></TD>
					<TD>
						<asp:dropdownlist id="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True" Visible="False"></asp:dropdownlist>
						<asp:textbox id="txt_fecha" onkeyup="PonDiagonal()" tabIndex="10" runat="server" Width="0%" CssClass="combos_small"
							MaxLength="10" AutoPostBack="True"></asp:textbox></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD>
						<asp:radiobuttonlist id="rb_seguro" runat="server" CssClass="combos_small" AutoPostBack="True" RepeatDirection="Horizontal"
							Visible="False">
							<asp:ListItem Value="0">Financiado</asp:ListItem>
							<asp:ListItem Value="1">Contado</asp:ListItem>
						</asp:radiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="obligatorio" width="25%" colSpan="4"></TD>
							</TR>
							<TR>
								<TD width="25%"></TD>
								<TD width="25%"></TD>
								<TD class="Obligatorio_negro" align="right" width="25%"></TD>
								<TD width="25%">
									<asp:textbox id="txt_grdramo" runat="server" Width="16px" CssClass="combos_small" Height="16px"
										Visible="False"></asp:textbox>
									<asp:textbox id="txt_grdcobertura" runat="server" Width="16px" CssClass="combos_small" Height="16px"
										Visible="False"></asp:textbox>
									<asp:textbox id="txt_banseguro" runat="server" Width="16px" CssClass="combos_small" Height="16px"
										Visible="False">2</asp:textbox>
									<asp:textbox id="txt_gdescripcion" runat="server" Width="16px" CssClass="combos_small" Height="16px"
										Visible="False"></asp:textbox>
									<asp:textbox id="txt_sumcob" runat="server" Width="16px" CssClass="combos_small" Height="16px"
										Visible="False"></asp:textbox>
									<asp:textbox id="txt_precioF" runat="server" Width="0px" CssClass="combos_small" Height="0px"></asp:textbox>
									<asp:textbox id="txt_ban" runat="server" Width="16px" CssClass="combos_small" Height="16px" Visible="False"></asp:textbox>
									<asp:label id="txt_subtipo" runat="server" CssClass="small"></asp:label></TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="4">Para poder comprar la póliza es necesario que 
									seleccione la impresora debajo de la aseguradora deseada *</TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="4">
									<asp:datagrid id="grd_cobertura" runat="server" Width="100%" CssClass="datagrid" AllowSorting="True"
										CellPadding="1" AutoGenerateColumns="False">
										<FooterStyle Wrap="False" CssClass="Footer_azul"></FooterStyle>
										<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
										<EditItemStyle Wrap="False"></EditItemStyle>
										<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
										<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
										<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
												HeaderText="Seleccionar" CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;" HeaderText="Eliminar"
												CommandName="Delete">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:ButtonColumn>
											<asp:BoundColumn Visible="False" DataField="cobertura"></asp:BoundColumn>
											<asp:BoundColumn DataField="descripcion_cob" ReadOnly="True" HeaderText="Cobertura">
												<ItemStyle Wrap="False" Width="13%"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n">
												<ItemStyle Wrap="False" Width="15%"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label1 runat="server" Width="100%" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grddesc runat="server" Width="100%" CssClass="combos_small" TextMode="MultiLine" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Suma Aseg.">
												<ItemStyle Wrap="False" HorizontalAlign="Right" Width="10%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label2 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdsuma runat="server" Width="100%" CssClass="combos_small" MaxLength="11" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="No. Fact.">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label3 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdfact runat="server" Width="100%" CssClass="combos_small" MaxLength="15" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="cap" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="SUMAASEG_COB"></asp:BoundColumn>
										</Columns>
										<PagerStyle Wrap="False"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<uc1:wfrmucobertura id="WfrmUCobertura1" runat="server"></uc1:wfrmucobertura></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD>
						<asp:button id="cmd_regrear" runat="server" CssClass="boton" Text="Regresar"></asp:button></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" height="10"></TD>
					<TD height="10"></TD>
					<TD class="Obligatorio_negro" height="10"></TD>
					<TD height="10"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD colSpan="2"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table8" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" align="center" width="2%">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3">Datos del Cotizador &nbsp; </T></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 103; LEFT: 352px; POSITION: absolute; TOP: 928px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
