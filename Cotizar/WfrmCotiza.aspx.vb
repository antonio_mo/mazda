Imports CN_Negocios

Partial Class WfrmCotiza
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalida As New CnValidacion

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Session("ClienteCotiza") = 0
        Session("idpoliza") = 0
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                If Not Request.QueryString("limpia") Is Nothing Then
                    Session("cotizacion") = 0
                End If

                Session("fincon") = "F"
                Session("ClienteCotiza") = 0
                Session("cotiza") = 0
                carga_marca()
                limpia()
                If Not Request.QueryString("valcost") Is Nothing Then
                    ViewState("catalogo") = Session("catalogo")
                    If Not Session("cotizacion") = 0 Then
                        txt_precio.Text = Session("precioEscrito")
                    Else
                        txt_precio.Text = Request.QueryString("valcost")
                    End If
                    cbo_marca.SelectedIndex = cbo_marca.Items.IndexOf(cbo_marca.Items.FindByValue(Session("MarcaC")))
                    cbo_carga_marca(cbo_marca.SelectedValue)
                    cbo_carga_modelo(cbo_marca.SelectedValue, Session("modelo"))
                    cbo_modelo.SelectedIndex = cbo_modelo.Items.IndexOf(cbo_modelo.Items.FindByValue(Session("modelo")))
                    cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(Session("anio")))
                    cbo_carga_descripcion()
                    cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(Session("vehiculo")))
                    datos_descripcion(cbo_descripcion.SelectedValue)
                    If Not Session("cotizacion") = 0 Then
                        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByValue(Session("intuso")))
                    Else
                        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText(Session("uso")))
                    End If
                End If
                SetFocus(cbo_marca)
            End If
            Session("Llave") = Nothing
            Session("LlaveC") = Nothing
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        txt_precio.Text = ""
    End Sub

    Public Sub carga_marca()
        Dim valor As String
        valor = estatus_auto()
        Dim dt As DataTable = ccliente.carga_marca(Session("intmarcabid"), Session("programa"), valor)
        Try
            If dt.Rows.Count > 0 Then
                cbo_marca.DataSource = dt
                cbo_marca.DataTextField = "marca"
                cbo_marca.DataValueField = "id_marca"
                cbo_marca.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_marca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_marca.SelectedIndexChanged
        Try
            If cbo_marca.SelectedValue = 0 Then
                MsgBox.ShowMessage("Seleccione la Marca")
                Exit Sub
            End If
            cbo_carga_marca(cbo_marca.SelectedValue)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cbo_carga_marca(ByVal intmcarca As Integer)
        Dim valor As String
        valor = estatus_auto()
        carga_modelo(intmcarca, valor)
    End Sub

    Public Sub carga_modelo(ByVal intmarca As Integer, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_modelo(intmarca, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_modelo.DataSource = dt
                cbo_modelo.DataTextField = "modelo_vehiculo"
                cbo_modelo.DataValueField = "modelo_vehiculo"
                cbo_modelo.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub cbo_modelo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_modelo.SelectedIndexChanged
        Try
            If cbo_modelo.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione el Tipo")
                Exit Sub
            End If
            limpia_campos()
            If Session("BanderaMacro") = 0 Then
                cbo_carga_modelo(cbo_marca.SelectedValue, cbo_modelo.SelectedValue)
            Else
                cbo_carga_modelo(cbo_marca.SelectedValue, cbo_modelo.SelectedValue)
                cbo_anio.SelectedIndex = 1
                cbo_carga_descripcion()
                cbo_descripcion.SelectedIndex = 1
                datos_descripcion(cbo_descripcion.SelectedValue)
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Particular"))
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia_campos()
        cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(-1))
        cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(-1))
        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByValue(-1))
        txt_precio.Text = ""
    End Sub


    Public Sub cbo_carga_modelo(ByVal intmarca As Integer, ByVal intmodelo As String)
        Dim valor As String
        valor = estatus_auto()
        carga_anio(intmarca, intmodelo, valor)
    End Sub

    Public Sub carga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_anio(intmarca, strmodelo, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_anio.DataSource = dt
                cbo_anio.DataTextField = "anio_vehiculo"
                cbo_anio.DataValueField = "anio_vehiculo"
                cbo_anio.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_anio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_anio.SelectedIndexChanged
        Try
            If cbo_anio.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione el a�o del vehiculo")
                Exit Sub
            End If
            cbo_carga_descripcion()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cbo_carga_descripcion()
        Dim valor As String
        valor = estatus_auto()
        carga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, valor)
    End Sub

    Public Sub carga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_descripcion(intmarca, strmodelo, stranio, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_descripcion.DataSource = dt
                cbo_descripcion.DataTextField = "descripcion_vehiculo"
                cbo_descripcion.DataValueField = "id_vehiculo"
                cbo_descripcion.DataBind()
                If dt.Rows.Count > 1 Then
                    cbo_descripcion.SelectedIndex = 1
                    datos_descripcion(cbo_descripcion.SelectedValue)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_descripcion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_descripcion.SelectedIndexChanged
        Try
            If cbo_descripcion.SelectedValue = 0 Then
                MsgBox.ShowMessage("Seleccione la descripci�n del vehiculo")
                Exit Sub
            End If
            datos_descripcion(cbo_descripcion.SelectedValue)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub datos_descripcion(ByVal intdescripcion As Integer)
        Dim valor As String
        Try
            Dim dth As DataTable = ccliente.carga_subramo_homologado(intdescripcion, estatus_auto(), Session("programa"), cbo_marca.SelectedValue, Session("intmarcabid"))
            If dth.Rows.Count > 0 Then
                viewstate("subramo") = ccliente.subramo
                viewstate("catalogo") = ccliente.Catalogo
            Else
                viewstate("subramo") = 0
                viewstate("catalogo") = 0
            End If


            valor = estatus_auto()

            Dim dt As DataTable = ccliente.carga_uso(Session("programa"), cbo_descripcion.SelectedValue, valor, viewstate("subramo"), Session.SessionID)
            If dt.Rows.Count > 0 Then
                cbo_uso.DataSource = dt
                cbo_uso.DataTextField = "uso"
                cbo_uso.DataValueField = "id_uso"
                cbo_uso.DataBind()
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Particular"))
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function estatus_auto() As String
        Return "N"
    End Function

    Private Sub txt_precio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_precio.TextChanged
        Dim total As Double
        Try
            If txt_precio.Text = "" Then
                txt_precio.Text = ""
            Else
                total = cvalida.QuitaCaracteres(txt_precio.Text)
                txt_precio.Text = Format(total, "##,##0.00")
            End If
            SetFocus(cmd_continuar)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub cmd_continuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_continuar.Click
        Dim i As Integer = 0
        Dim bandera As Integer = 0
        Try
            If cbo_marca.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar la Marca")
                Exit Sub
            End If
            If cbo_modelo.SelectedValue = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Vehiculo")
                Exit Sub
            End If
            If cbo_anio.SelectedValue = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el A�o del Vehiculo ")
                Exit Sub
            End If
            If cbo_descripcion.SelectedValue = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la Descripci�n del Vehiculo")
                Exit Sub
            End If
            If cbo_uso.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Uso del Vehiculo")
                Exit Sub
            End If
            If txt_precio.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el monto del Vehiculo")
                Exit Sub
            End If
            If txt_precio.Text <= 0 Then
                MsgBox.ShowMessage("El monto del Vehiculo debe de ser mayo a 0 ")
                Exit Sub
            End If
            For i = 0 To rb_tipo.Items.Count - 1
                If rb_tipo.Items(i).Selected = True Then
                    bandera = 1
                    Exit For
                End If
            Next
            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Contribuyente")
                Exit Sub
            End If

            Session("vehiculo") = cbo_descripcion.SelectedValue
            Session("vehiculoDesc") = cbo_descripcion.SelectedItem.Text
            Session("subramo") = viewstate("subramo")
            Session("anio") = cbo_anio.SelectedItem.Text
            Session("modelo") = cbo_modelo.SelectedValue
            Session("uso") = cbo_uso.SelectedItem.Text

            Dim dt As DataTable = ccliente.Carga_definicion_uso(Session("programa"), viewstate("subramo"), cvalida.QuitaCaracteres(cbo_uso.SelectedItem.Text, True), Session("version"))
            If dt.Rows.Count > 0 Then
                Session("intuso") = dt.Rows(0)("id_uso")
            Else
                Session("intuso") = cbo_uso.SelectedValue
            End If

            Session("catalogo") = IIf(viewstate("catalogo") = "", "0", viewstate("catalogo"))
            Session("EstatusV") = estatus_auto()
            Session("MarcaC") = cbo_marca.SelectedValue
            Session("MarcaDesc") = cbo_marca.SelectedItem.Text
            Session("contribuyente") = personalidad(rb_tipo)
            Session("iva") = 0.16
            Session("precioEscrito") = txt_precio.Text

            Response.Redirect("WfrmCotiza1.aspx?valcost=" & cvalida.QuitaCaracteres(txt_precio.Text) & "")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_uso_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_uso.SelectedIndexChanged

    End Sub

    Private Sub rb_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tipo.SelectedIndexChanged

    End Sub
End Class
