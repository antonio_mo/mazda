<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmReCotizaPromo.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmReCotizaPromo"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmReCotizaPromo</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
        <link rel="stylesheet" type="text/css" href="..\Css\submodal.css">
        <script language="javascript" type="text/javascript" src="..\JavaScript\submodalsource.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
		<script language="JScript">
		function redireccion(intaseguradora, intplazo, intregion, intpaquete, subramo, IntRecargo, IntNumPago, Pago, SubPago)
		{
			var uno = document.getElementById("txt_precio").value;
			var cvecontrato1 = 0
			var tipoCarga = 0
			dbprecio=0
			var path = "WfrmReCotizaPromo.aspx?cveAseg="+intaseguradora+"&valcost="+uno+"&cveplazo="+intplazo+"&cveregion="+intregion+"&cvepaquete="+intpaquete+"&subramo="+subramo+"&contrato="+cvecontrato1+"&Recargo="+IntRecargo+"&NumPago="+IntNumPago+"&Pago1="+Pago+"&Pago2="+SubPago+"&TipoCarga="+tipoCarga+""
			//alert(path)
			window.location.href=path;
		}
		String.prototype.trim = function() 
		{
			return this.replace(',','');
		}
		function Cobertura(intaseguradora, intpaquete, strModelo, strCp, strSumaAsegurada) {

		    var x = "WfrmDetalleCobertura.aspx?cveAseg=" + intaseguradora + "&cvepaquete=" + intpaquete + "&Modelo=" + strModelo + "&CodigoPostal=" + strCp + "&SumaAsegurada=" + strSumaAsegurada
		    initPopUp();
		    showPopWin(x, 800, 550, '<font color="#FFFFFF" face=arial><b>Detalle de la cobertura</b></font>', null);
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 103; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT style="Z-INDEX: 0" size="+0">&nbsp;&nbsp;Busque 
							la cotizaci�n</FONT></TD>
				</TR>
			</TABLE>
			<HR style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 102; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD>
						<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" width="100%">
							<TR>
								<TD class="obligatorio" align="right">&nbsp;
									<asp:requiredfieldvalidator style="Z-INDEX: 0" id="RequiredFieldValidator1" runat="server" ErrorMessage="No. Cotizaci�n"
										ControlToValidate="txt_cotiza">*</asp:requiredfieldvalidator>&nbsp;No.&nbsp;Cotizaci�n 
									:</TD>
								<TD><asp:textbox style="Z-INDEX: 0" id="txt_cotiza" onkeypress="javascript:onlyDigits(event,'decOK');"
										runat="server" MaxLength="8" CssClass="Texto_Cantidad" Width="100%"></asp:textbox></TD>
								<TD align="center"><asp:button style="Z-INDEX: 0" id="cmd_cotizar" runat="server" CssClass="boton" Text="Buscar"></asp:button></TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="3" align="center"><asp:validationsummary style="Z-INDEX: 0" id="Validationsummary2" runat="server" Font-Size="12px" Font-Names="Tahoma"
										HeaderText="Los siguientes campos son obligatorios"></asp:validationsummary></TD>
							</TR>
							<TR>
								<TD width="30%"></TD>
								<TD width="30%"></TD>
								<TD width="40%"></TD>
							</TR>
						</TABLE>
					</TD>
					<TD vAlign="top">
						<TABLE style="Z-INDEX: 0" id="Table5" border="0" cellSpacing="1" cellPadding="1" width="100%">
							<TR>
								<TD class="obligatorio" align="left"><asp:table style="Z-INDEX: 0" id="tb_impConfirma" runat="server" CssClass="negrita" Width="100%"></asp:table></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><asp:panel id="pnl_cotiza" runat="server" Width="100%" Visible="False">
							<TABLE style="Z-INDEX: 0" id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="obligatorio" align="right">Automovil :</TD>
									<TD>
										<asp:label id="lbl_tipo" runat="server" Width="100%" CssClass="negrita">Nuevo</asp:label></TD>
									<TD class="obligatorio" align="right"></TD>
									<TD class="negrita" align="right">&nbsp;
									</TD>
									<TD>
										<asp:table style="Z-INDEX: 0" id="tb_imp" runat="server" Width="100%" CssClass="negrita"></asp:table></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Marca :</TD>
									<TD>
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_marca" runat="server" Width="100%" CssClass="combos_small"
											AutoPostBack="True"></asp:dropdownlist></TD>
									<TD class="obligatorio" align="right"></TD>
									<TD></TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right"></TD>
									<TD></TD>
									<TD class="obligatorio" align="right"></TD>
									<TD></TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right"></TD>
									<TD class="obligatorio" colSpan="2">
										<asp:hyperlink style="Z-INDEX: 0" id="Hyperlink2" runat="server" CssClass="normal">Auto Entregado</asp:hyperlink></TD>
									<TD></TD>
									<TD vAlign="top" rowSpan="6">
										<asp:table style="Z-INDEX: 0" id="tb_seguro" runat="server" Width="100%" GridLines="Both" CellSpacing="0"
											CellPadding="0"></asp:table></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Tipo :</TD>
									<TD>
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_modelo" runat="server" Width="100%" CssClass="combos_small"
											AutoPostBack="True"></asp:dropdownlist></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Modelo :</TD>
									<TD>
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_anio" runat="server" Width="100%" CssClass="combos_small"
											AutoPostBack="True"></asp:dropdownlist></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Descripci�n :
									</TD>
									<TD colSpan="2">
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_descripcion" runat="server" Width="100%" CssClass="combos_small"
											AutoPostBack="True"></asp:dropdownlist></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">
										<asp:label style="Z-INDEX: 0" id="lbl_uso" runat="server">Uso :</asp:label></TD>
									<TD>
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_uso" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								                     <TR>
									<TD class="obligatorio" align="right">Precio de la unidad :</TD>
									<TD colSpan="2">
										<asp:textbox style="Z-INDEX: 0" id="txt_precio" onkeypress="javascript:onlyDigits(event,'decOK');"
											runat="server" Width="50%" CssClass="Texto_Cantidad" MaxLength="8"></asp:textbox></TD>
									<TD></TD>
								</TR>           
                                <TR>
									<TD class="obligatorio" align="right">C�digo Postal :</TD>
									<TD colspan="2">
                                    <asp:textbox style="Z-INDEX: 0" id="txtCodigoPostal" Enabled="false"
											runat="server" Width="50%" CssClass="Texto_Cantidad" MaxLength="10"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right"></TD>
									<TD colSpan="2"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Tipo contribuyente :</TD>
									<TD colSpan="2">
										<asp:radiobuttonlist id="rb_tipo" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="3"
											RepeatDirection="Horizontal">
											<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
											<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
											<asp:ListItem Value="2">Moral</asp:ListItem>
										</asp:radiobuttonlist></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right"></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Plazos contratado :</TD>
									<TD class="negrita">
										<asp:dropdownlist style="Z-INDEX: 0" id="cbo_plazo" runat="server" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist>
										<asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="negrita">Meses</asp:label></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">Fecha de&nbsp;entrega&nbsp;:</TD>
									<TD class="negrita">
										<asp:label style="Z-INDEX: 0" id="lbl_fecha" runat="server" Width="100%" CssClass="negrita"></asp:label></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colSpan="5">
										<asp:table id="tb_aseguradora" runat="server" Width="100%" GridLines="Both" CellSpacing="0"
											CellPadding="0"></asp:table></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD>
										<asp:radiobuttonlist id="rb_seguro" runat="server" Width="100%" CssClass="combos_small" Visible="False"
											AutoPostBack="True" RepeatDirection="Horizontal">
											<asp:ListItem Value="0">Financiado</asp:ListItem>
											<asp:ListItem Value="1">Contado</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="negrita"></TD>
									<TD align="right"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD width="15%"></TD>
									<TD width="25%"></TD>
									<TD width="20%"></TD>
									<TD width="15%"></TD>
									<TD width="25%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="40%"></TD>
					<TD width="60%"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox style="Z-INDEX: 104; POSITION: absolute; TOP: 952px; LEFT: 616px" id="MsgBox" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
