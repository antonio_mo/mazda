﻿Imports CN_Negocios
Imports System.Threading.Tasks
Imports System.IO
Public Class WfrmCotizaPromoM
    Inherits System.Web.UI.Page

#Region " Código generado por el Diseñador de Web Forms "

    'El Diseñador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Diseñador de Web Forms necesita la siguiente declaración del marcador de posición.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Diseñador de Web Forms requiere esta llamada de método
        'No la modifique con el editor de código.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo
    Private PaqSel As String = ""
    Private CdFraccion As CP_FRACCIONES.clsEmision

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Introducir aquí el código de usuario para inicializar la página
        Session("ClienteCotiza") = 0
        Session("idpoliza") = 0
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                carga_plazo()
                Session("IsPostBack") = False

                If Not Request.QueryString("unico") Is Nothing Then
                    Session("IdCotiUnica") = Nothing
                    Session("idpoliza") = Nothing
                    Session("IdCotizaCobertura") = Nothing
                End If

                If Not Request.QueryString("click_menu") Is Nothing Then
                    Session("click_menu") = True
                End If

                Dim axy As Long = Session("IdCotiUnica")

                Session("FechaIni") = IIf(ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0", DateTime.Parse(Date.Now).ToString("dd/MM/yyyy"), DateTime.Parse(Date.Now).ToString("MM/dd/yyyy"))
                txt_fecha.Text = DateTime.Parse(Date.Now).ToString("dd/MM/yyyy")
                Session("fincon") = "F"
                Session("ClienteCotiza") = 0
                Session("cotiza") = 0
                'forma pago daños
                Dim banSeg As Integer
                If Session("seguro") = 0 Then
                    banSeg = 1
                Else
                    banSeg = 0
                End If
                rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))
                Seg_financiado(rb_seguro)

                If Not Request.QueryString("cveAseg") Is Nothing Then
                    'nuevo
                    tipo_poliza(Request.QueryString("cveplazo"))

                    Session("FechaIni") = IIf(ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0", DateTime.Parse(Request.QueryString("strfecha")).ToString("dd/MM/yyyy"), DateTime.Parse(Request.QueryString("strfecha")).ToString("MM/dd/yyyy"))

                    'Session("FechaIni") = Request.QueryString("strfecha")

                    'HM 02/01/2014 se agrega hay que validarlo
                    Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")
                    Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                           Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                           Session("EstatusV"), _
                           0, Session("InterestRate"), Session("FechaIni"), _
                           Session("tiposeguro"), Request.QueryString("cveplazo"), _
                           Request.QueryString("valcost"), _
                           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                           0, Session("vehiculo"), Request.QueryString("cveAseg"), _
                           Request.QueryString("cvepaquete"), Session("programa"), _
                           Session("fincon"), , , Session("CP"), Session("MarcaC"), Session("intmarcabid"), _
                           Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
                           Request.QueryString("TipoCarga"), Session("Exclusion"), Session("IdCotizaCobertura"))
                    'fin cambio micha

                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results
                    Session("MultArrPolisa") = ccalculo.MultArrPolisa

                    Session("SubRamo") = Request.QueryString("subramo")

                    Session("DatosGenerales") = Nothing
                    Dim arr(25) As String
                    arr(0) = Session("moneda")
                    arr(1) = Request.QueryString("subramo")
                    arr(2) = Session("anio")
                    arr(3) = Session("modelo")
                    arr(4) = 1
                    'arr(5) = Seg_financiado(rb_seguro)
                    arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                    arr(6) = Session("uso")
                    arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arr(8) = Session("EstatusV")
                    arr(9) = 0
                    arr(10) = Session("InterestRate")
                    arr(11) = Session("FechaIni")
                    arr(12) = Session("tiposeguro")
                    arr(13) = Request.QueryString("cveplazo")
                    'arr(14) = txt_precio.Text
                    arr(14) = Request.QueryString("valcost")
                    arr(15) = Session("idgrdcontrato")
                    arr(16) = Session("seguro")
                    arr(17) = Session("bid")
                    arr(18) = 0
                    arr(19) = Session("vehiculo")
                    arr(20) = Request.QueryString("cveAseg")
                    arr(21) = Request.QueryString("cvepaquete")
                    arr(22) = Session("programa")
                    arr(23) = Session("fincon")
                    arr(24) = Request.QueryString("Pago1")
                    arr(25) = Request.QueryString("Pago2")
                    Session("DatosGenerales") = arr

                    'manolito
                    Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                    Session("plazoRecargo") = Request.QueryString("NumPago")

                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("FechaInicio") = Session("FechaIni")
                    Session("contrato") = Request.QueryString("cvecontrato")
                    Session("precioEscrito") = Request.QueryString("valcost")
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Session("Tipocarga") = Request.QueryString("TipoCarga")
                    Session("EstatusV") = "N"

                    Dim xs() As String = Session("ArrPoliza")

                    calculoaseguradora12meses(Request.QueryString("valcost"), Request.QueryString("TipoCarga"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), Request.QueryString("cveregion"), Request.QueryString("subramo"))

                    Dim xs1() As String = Session("ArrPoliza")

                    If Session("IdCotiUnica") Is Nothing Then

                        Dim uno As String = Session("vehiculoC")
                        Dim xx As String = Session("idcotizapanel12")
                        Dim s As String = Session("Cuota")
                        Dim s1 As String = Session("MesesTranscurridos")
                        Dim s2 As String = Session("MesesSeguroGratis")
                        Dim s3 As String = Session("idcotizapanel12C")
                        Dim s4 As String = Session("Exclusion")
                        Dim s5 As String = Session("idcotizapanel")
                        Dim s6 As String = Session("idgrdcontrato")
                        Dim s7 As String = Session("bid")
                        Dim s8 As String = Session("vehiculo")
                        Dim s9 As String = Session("modelo")
                        Dim s10 As String = Session("anio")
                        Dim s11 As String = Request.QueryString("valcost")
                        Dim s12 As String = Request.QueryString("cveplazo")
                        Dim s13 As String = Session("MarcaC")
                        Dim s14 As String = Session("programa")
                        Dim s15 As String = Session("contribuyente")
                        Dim s16 As String = Session("EstatusV")
                        Dim s17 As String = Session("FechaInicio")
                        Dim s18 As String = Session("modeloc")
                        Dim s21 As String = Session("vehiculoC")

                        If Session("Exclusion") = "1" Then
                            Session("MesesSeguroGratis") = 0
                        End If


                        Session("IdCotiUnica") = ccalculo.inserta_cotizacion_unica( _
                        Session("idcotizapanel12"), _
                        Session("idcotizapanel"), Session("idgrdcontrato"), _
                        Session("bid"), Session("vehiculo"), Session("modelo"), _
                        Session("anio"), CDbl(Request.QueryString("valcost")), _
                        CInt(Request.QueryString("cveplazo")), _
                        IIf(Session("Exclusion") = 0, CInt(Request.QueryString("cveplazo")), 0), _
                        Session("MarcaC"), Session("programa"), _
                        Session("Exclusion"), Session("contribuyente"), Session("EstatusV"), Session("FechaInicio"), _
                        Session("modeloc"), Session("anioc"), CDbl(Session("MontoCarta")), Session("Diferencia"), _
                        Session("Cuota"), Session("MesesTranscurridos"), _
                        Session("MesesSeguroGratis"), _
                        Session("vehiculoC"), Session("idcotizapanel12C"), Session("CP"))

                    End If

                    Response.Redirect("WfrmCliente.aspx")

                End If
            Else
                Session("IsPostBack") = True
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>" & ex.Message & "</alert>", True)
        End Try
    End Sub


    Public Sub calculoaseguradora12meses(ByVal valcost As Double, ByVal TipoCarga As Integer, _
    ByVal strcatalogo As String, ByVal IdRegion As Integer, ByVal subramo As String)

        Dim dt As DataTable = ccalculo.Promocion12meses(Session("idcotizapanel12"))
        If dt.Rows.Count > 0 Then
            Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                1, "A", _
                Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                Session("EstatusV"), _
                0, Session("InterestRate"), Session("FechaIni"), _
                Session("tiposeguro"), 12, _
                valcost, _
                Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                dt.Rows(0)("idpaquete"), Session("programa"), _
                Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                dt.Rows(0)("id_tipoRecargo"), 1, TipoCarga, 1, , , 0, 1, 0)
            'fin cambio micha

            Session("subsidios1") = ccalculo.subsidios12
            Session("ArrPoliza1") = ccalculo.ArrPolisa12
            Session("resultado1") = ccalculo.Results12
            Session("MultArrPolisa1") = ccalculo.MultArrPolisa12

            Dim cc() As String = ccalculo.ArrPolisa12

            Session("DatosGenerales1") = Nothing
            Dim arr(25) As String
            arr(0) = Session("moneda")
            arr(1) = subramo
            arr(2) = Session("anio")
            arr(3) = Session("modelo")
            arr(4) = 1
            'arr(5) = Seg_financiado(rb_seguro)
            arr(5) = Seg_financiado_Seleccion(Session("seguro"))
            arr(6) = Session("uso")
            arr(7) = strcatalogo
            arr(8) = Session("EstatusV")
            arr(9) = 0
            arr(10) = Session("InterestRate")
            arr(11) = Session("FechaIni")
            arr(12) = Session("tiposeguro")
            arr(13) = 12
            arr(14) = txt_precio.Text
            arr(15) = Session("idgrdcontrato")
            arr(16) = Session("seguro")
            arr(17) = Session("bid")
            arr(18) = 0
            arr(19) = Session("vehiculo")
            arr(20) = dt.Rows(0)("Id_aseguradora")
            arr(21) = dt.Rows(0)("idpaquete")
            arr(22) = Session("programa")
            arr(23) = Session("fincon")
            arr(24) = dt.Rows(0)("primatotal")
            arr(25) = dt.Rows(0)("primaconsecutiva")
            Session("DatosGenerales1") = arr

            'manolito
            Session("plazo1") = 12
            Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
            Session("plazoRecargo1") = 1
            Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
            Session("paquete1") = dt.Rows(0)("idpaquete")
            Session("subramo1") = subramo
        End If
        dt.Clear()

    End Sub

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Public Function estatus_auto() As String
        Return "N"
    End Function

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>" & ex.Message & "</alert>", True)
        End Try
    End Function

    Public Sub validaSeguro12()
        Try
            Ajuste_plazo_calculo(12, 1)
            'HM 02/01/2014 se agrega hay que validarlo
            'calculo_general(12, 0)            
            calculo_general(12, 0, Session("Exclusion"))
            crearPrima12meses()
            'HM 02/01/2014 se agrega hay que validarlo
            If Session("Exclusion") = 0 Then
                cbo_plazo.SelectedIndex = 0
            Else
                cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'HM 02/01/2014 se agrega hay que validarlo
    'Public Function calculo_general(ByVal NumPlazo As Integer, ByVal IntSubCob As Double) As String
    Public Function calculo_general(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal BanderaPromocion As Integer) As String
        Dim arr() As String = Nothing
        Dim Arr1() As String = Nothing
        Dim i As Integer = 0
        Dim strcadenaC As String = ""
        Dim strcadena As String = ""
        Dim iPrograma As Integer = 0
        Dim arrCadenaResp As String() = Nothing

        Try
            iPrograma = Session("programa")
            
            strcadena = ccalculo.calculoSP(iPrograma, Session("EstatusV"), Session("vehiculo"), Session("intmarcabid"), Session("MarcaC"), 1, txtCodigoPostal.Text, Session("bid"), Session("idgrdcontrato"), Session("anio"), _
                            Session("uso"), Session("moneda"), txt_precio.Text, Session("plazoFinanciamiento"), Session("plazoRecargo"), 2, NumPlazo, Session("IdCotizaCobertura"))

            arrCadenaResp = strcadena.Split("$")
            strcadena = arrCadenaResp(0)
            Session("CadenaCob") = arrCadenaResp(0)
            Session("idcotizapanel12") = arrCadenaResp(1)

            If strcadena = "" Then
                MsgBox.ShowMessage("No existe información del auto a cotizar, favor de consultar al administrador")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>No existe información del auto a cotizar, favor de consultar al administrador</alert>", True)
                Return ""
            Else
                Session("CadenaCob") = strcadena
                Session("idcotizapanel12") = ccalculo.Inst_Cotizacion_panel_12meses(strcadena, "E")
            End If
            Return strcadena

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>" & ex.Message & "</alert>", True)
        End Try
    End Function

    Public Sub Ajuste_plazo_calculo(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, Optional ByVal BanActualiza As Integer = 0)
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                'HM 05062014
                If intplazoIni = 0 Then
                    dbajuste = 0
                Else
                    dbajuste = intplazoIni / intplazoIni
                End If
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select

        ComplementoAjuste(IntCboTipo, intPlazofinal, IntPagos)

        If BanActualiza = 1 Then
            ccalculo.actualiza_numplazo_cob(intPlazofinal, Session("idgrdcontrato"), Session("bid"))
        End If
    End Sub

    Public Sub ComplementoAjuste(ByVal IntCboTipo As Integer, ByVal intPlazofinal As Integer, ByVal IntPagos As Integer)
        Session("plazoSeleccion") = intPlazofinal
        Session("plazo") = intPlazofinal
        Session("plazoFinanciamiento") = IntCboTipo
        Session("plazoRecargo") = IntPagos

        tipo_poliza(intPlazofinal)
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                Session("tipopol") = "M"
            Else
                Session("tipopol") = "A"
            End If
        Else
            Session("tipopol") = "A"
        End If
    End Sub

    Public Sub carga_plazo()
        'HM 31/10/2013
        Dim arrP As New ArrayList
        Dim maxP As Integer = 60
        Dim i As Integer = 0
        If Session("Exclusion") = 0 Then maxP = 48
        arrP.Add("-- Seleccione --")
        arrP.Add(12)
        If (Session("Programa") = 1) Then
            arrP.Add(24)
            arrP.Add(36)
            arrP.Add(48)
            arrP.Add(60)
        End If

        cbo_plazo.DataSource = arrP
        cbo_plazo.DataBind()
    End Sub

    Public Sub crearPrima12meses()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arr() As String
        Dim i, j As Integer
        Dim BanMax As Double = 0
        Dim dbMonto As Double = 0
        Dim strcadena As String = ""
        Dim TipoAuto As String = ""

        Try

            TipoAuto = "C"
            tb_seguroC.Controls.Clear()
            If Not Session("CadenaCobCont") = "" Then
                arr = Split(Session("CadenaCobCont"), "|")
                arr.Sort(arr)
                BanMax = VerificaMaximno(arr)
                encabenzado12meses(TipoAuto)
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arr12meses() As String = arr(i).Split("*")
                        ccliente.DatosAseguradora(arr12meses(0))
                        tbrow = New TableRow

                        dbMonto = CDbl(arr12meses(2))
                        If BanMax = dbMonto Then
                            Session("SACont") = dbMonto
                            'aseguradora mas barata
                            Session("12meses") = arr12meses(0)
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                            ccalculo.Upd_Cotizacion_panel_12meses(Session("idcotizapanel12C"), arr12meses(0), "C")
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If

                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = ccliente.DescrAseg
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        tb_seguroC.Controls.Add(tbrow)
                    End If
                Next
            End If

            If Session("Exclusion") = 0 Then
                tb_seguroC.Visible = True
            Else
                tb_seguroC.Visible = False
            End If

            'HMH 26052014 Auto Entregado
            strcadena = ""
            TipoAuto = "E"
            tb_seguro.Controls.Clear()
            If Not Session("CadenaCob") = "" Then
                arr = Split(Session("CadenaCob"), "|")
                arr.Sort(arr)
                BanMax = VerificaMaximno(arr)
                encabenzado12meses(TipoAuto)
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arr12meses() As String = arr(i).Split("*")
                        ccliente.DatosAseguradora(arr12meses(0))
                        tbrow = New TableRow

                        dbMonto = CDbl(arr12meses(2))
                        If BanMax = dbMonto Then
                            Session("SAEnt") = dbMonto
                            'aseguradora mas barata
                            Session("12meses") = arr12meses(0)
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                            ccalculo.Upd_Cotizacion_panel_12meses(Session("idcotizapanel12"), arr12meses(0), "E")
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If

                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = ccliente.DescrAseg
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        tb_seguro.Controls.Add(tbrow)
                    End If
                Next
            End If

            If Session("Exclusion") = 0 Then
                tb_seguro.Visible = True
            Else
                tb_seguro.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub encabenzado12meses(ByVal TipoAuto As String)
        Dim tbcell As TableCell
        Dim tbrow As New TableRow

        tbcell = New TableCell
        tbcell.Text = "Seguro gratis un año"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(100)
        'HM 13/02/2014 solo administradores
        If Session("nivel") = 0 Then
            tbcell.ColumnSpan = 3
        Else
            tbcell.ColumnSpan = 2
        End If
        tbrow.Controls.Add(tbcell)
        If TipoAuto = "C" Then
            tb_seguroC.Controls.Add(tbrow)
        Else
            tb_seguro.Controls.Add(tbrow)
        End If

    End Sub

    Public Function agrega_celdas(ByVal strcadena As String, ByVal strclase As String, ByVal intalinea As Integer, _
    ByVal tipoFormato As String, ByVal tamano As Double, Optional ByVal Comentario As String = "") As TableCell
        'intalinea = 
        '1 = izquierda
        '2 = centro
        '3 = derecha
        Dim tbcell As New TableCell
        Dim hp As HyperLink
        Dim Himage As HtmlImage

        Select Case tipoFormato
            Case "G"
                tbcell.Text = strcadena
            Case "I"
                Himage = New HtmlImage
                Himage.Src = strcadena
                tbcell.Controls.Add(Himage)
            Case "H"
                'hp = New HyperLink
                'hp.Text = Comentario
                'hp.NavigateUrl = strcadena
                'tbcell.Controls.Add(hp)
        End Select
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.CssClass = strclase
        tbcell.HorizontalAlign = intalinea
        tbcell.Width = Unit.Percentage(tamano)
        Return tbcell
    End Function

    Public Function VerificaMaximno(ByVal arr As Object) As Double
        Dim i As Integer = 0
        Dim ValorMaximo As Double = 0
        Dim ValorMaximoAux As Double = 0

        For i = 0 To arr.Length - 1
            Dim arrvalida() As String = arr(i).Split("*")
            If arrvalida(2) <> "" Then
                ValorMaximoAux = CDbl(arrvalida(2))
            End If
            If ValorMaximoAux < ValorMaximo Then
                ValorMaximo = ValorMaximoAux
            ElseIf i = 0 Then
                ValorMaximo = ValorMaximoAux
            End If
        Next
        Return ValorMaximo
    End Function

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Private Sub cbo_plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_plazo.SelectedIndexChanged
        Dim i As Integer = 0
        Dim plazo As Integer

        If cbo_plazo.SelectedValue <> "-- Seleccione --" Then
            Session("IdCotiUnica") = Nothing
            Session("plazoSeleccion") = Session("plazoRecargo")
            crearPrima12meses()

            plazo = cbo_plazo.SelectedValue

        End If

    End Sub

    'Public Function calculo_general_aseguradora(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal Tiporecargo As Integer) As String
    Public Sub calculo_general_aseguradora()
        Dim arr() As String
        Dim Arr1() As String
        Dim i, j, k, x As Integer
        Dim strcadena As String = ""
        Dim strCadenaCal As String = ""
        Dim FechaInicio As String = ""
        Dim FechaTermina As String = ""
        Dim ds As DataSet

        Dim NumPlazo As Integer
        Dim IntSubCob As Integer = 0
        Dim Tiporecargo As Integer = 0
        Dim IdRecargo As Integer = 0
        Dim strInserta As String = ""
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0

        Dim ArrAseg(4, 5) As String

        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arrCadena As String()

        Try
            Dim iPrograma As Integer = Session("programa")

            Dim dt As DataTable = ccliente.Carga_tipoPago(iPrograma)
            If dt.Rows.Count > 1 Then
                NumPlazo = cbo_plazo.SelectedValue

                IntSubCob = 0
                For k = 1 To dt.Rows.Count - 1
                    Ajuste_plazo_calculo(cbo_plazo.SelectedValue, dt.Rows(k)("id_tiporecargo"))

                    IdRecargo = dt.Rows(k)("id_tiporecargo")

                    strcadena = ccalculo.calculoSP(iPrograma, IIf(Session("Exclusion") = 0, "U", "N"), Session("vehiculo"), Session("intmarcabid"), Session("MarcaC"), Session("Exclusion"), _
                                                   txtCodigoPostal.Text, Session("bid"), Session("idgrdcontrato"), Session("anio"), Session("uso"), Session("moneda"), txt_precio.Text, _
                                                   Session("plazoFinanciamiento"), Session("plazoRecargo"), 2, cbo_plazo.SelectedValue, Session("IdCotizaCobertura"))

                    arrCadena = strcadena.Split("$")
                    strcadena = arrCadena(0)
                    strCadenaCal = arrCadena(1)

                    If strcadena = "" Then
                        MsgBox.ShowMessage("No existe información del auto a cotizar, favor de consultar al administrador")
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>No existe información del auto a cotizar, favor de consultar al administrador</alert>", True)
                        Exit Sub
                    Else
                        If iPrograma <> 1 And iPrograma <> 2 Then
                            strCadenaCal = ccalculo.StrSalidaPanle
                        End If

                        Select Case IdRecargo
                            Case 2 'Anual
                                Tiporecargo = 1
                            Case 3 'Semestral
                                Tiporecargo = 4
                            Case 4 'Trimestral
                                Tiporecargo = 3
                            Case 5 'Mensual
                                Tiporecargo = 2
                            Case Else
                                'validando contado
                                'Tiporecargo = IdRecargo
                                Tiporecargo = 0
                        End Select

                        Dim arrFinicio() As String
                        arrFinicio = Split(Session("FechaIni"), "/")
                        FechaInicio = Session("FechaIni")

                        'pruebas locales se descomento todo el if
                        'If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0" Then
                        '    FechaInicio = arrFinicio(0) & "/" & arrFinicio(1) & "/" & arrFinicio(2)
                        '    'FechaInicio = Format(CDate(FechaInicio), "dd/MM/yyyy")
                        'Else
                        '    If (ConfigurationManager.AppSettings("BanderaFechaServidor").ToString() = "1") Then
                        '        FechaInicio = Format(CDate(FechaInicio), "MM/dd/yyyy") 'servidor 
                        '    Else
                        '        FechaInicio = arrFinicio(1) & "/" & arrFinicio(0) & "/" & arrFinicio(2) 'Maquina
                        '    End If
                        'End If
                        If (ConfigurationManager.AppSettings("BanderaFechaServidor").ToString() = "1") Then
                            FechaInicio = CDate(FechaInicio)
                        Else
                            FechaInicio = arrFinicio(1) & "/" & arrFinicio(0) & "/" & arrFinicio(2) 'Maquina
                        End If



                        'FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), cbo_plazo.SelectedValue)
                        FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), Session("plazo")) 'Maquina
                        'If (ConfigurationManager.AppSettings("BanderaFechaServidor").ToString() = "1") Then
                        '    FechaTermina = Format(CDate(FechaTermina), "MM/dd/yyyy") 'servidor 
                        'End If
                        FechaTermina = CDate(FechaTermina)

                        Dim Results() As String
                        Dim Results1() As String

                        arr = strcadena.Split("|")
                        Results = strCadenaCal.Split("|")

                        For i = 0 To arr.Length - 1
                            Arr1 = arr(i).Split("*")
                            Results1 = Results(i).Split("*")


                            If Arr1(0) = Results1(0) Then

                                If Tiporecargo > 0 Then

                                    ds = Nothing
                                    CdFraccion = New CP_FRACCIONES.clsEmision
                                    Select Case CInt(Arr1(0))
                                        Case 1, 4
                                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                        Case 2, 5
                                            ds = CdFraccion.FraccionesCHUBB_MZD(FechaInicio, _
                                                        FechaTermina, _
                                                        Results1(1), Results1(2), _
                                                        Results1(3), Results1(4), _
                                                        Results1(5), Tiporecargo, Results1(6), _
                                                        (Results1(7) * 100))
                                        Case 3, 6
                                            ds = CdFraccion.FraccionesHDI(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                    End Select

                                    If ds.Tables(0).Rows.Count > 0 Then
                                        For x = 0 To ds.Tables(0).Rows.Count - 1
                                            Select Case x
                                                Case 0
                                                    Dbtotales1 = Math.Round(ds.Tables(0).Rows(x)("prima_total"), 2)

                                                Case 1
                                                    Dbtotales2 = Math.Round(ds.Tables(0).Rows(x)("prima_total"), 2)
                                                    Exit For
                                            End Select
                                        Next
                                    End If

                                Else
                                    Dbtotales1 = Results1(5)
                                    Dbtotales2 = 0
                                End If

                                'HM 30/10/2013 se modifico para agregar los datos del panel
                                'HM 02/01/2014 se agrega hay que validarlo
                                If strInserta = "" Then
                                    strInserta = Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                Else
                                    strInserta = strInserta & "|" & Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                End If
                            End If

                        Next
                        End If
                Next

                If Not strInserta = "" Then
                    Session("idcotizapanel") = ccalculo.Inst_Cotizacion_panel(strInserta, Session("IdCotizaCobertura"))
                    Session("CP") = txtCodigoPostal.Text

                    recargadatos(Session("vehiculo"), Session("modelo"), Session("anio"), cbo_plazo.SelectedValue, cbo_plazo.SelectedValue, Session("v_m_c"), personalidad(rb_tipo), estatus_auto)
                    hp_coti.Visible = True
                    hp_coti1.Visible = True
                End If
            End If
            dt.Clear()

            dt = ccalculo.CargaCtoizaPanelAseguradora(Session("idcotizapanel"))
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    ccliente.DatosAseguradora(dt.Rows(i)("id_aseguradora"))
                    Dim descPaquete As String = String.Empty
                    Dim dtV As DataTable = ccalculo.CargaCtoizaPanel(Session("idcotizapanel"), dt.Rows(i)("id_aseguradora"))
                    If dtV.Rows.Count > 0 Then
                        For j = 0 To dtV.Rows.Count - 1
                            If Not dtV.Rows(j).IsNull("Id_TipoRecargo") Then
                                ArrAseg(j, 0) = dtV.Rows(j)("Id_TipoRecargo")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaTotal") Then
                                ArrAseg(j, 1) = dtV.Rows(j)("PrimaTotal")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaConsecutiva") Then
                                ArrAseg(j, 2) = dtV.Rows(j)("PrimaConsecutiva")
                            End If
                            If Not dtV.Rows(j).IsNull("subramo") Then
                                ArrAseg(j, 3) = dtV.Rows(j)("subramo")
                            End If
                            If Not dtV.Rows(j).IsNull("IdPaquete") Then
                                ArrAseg(j, 4) = dtV.Rows(j)("IdPaquete")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaConsecutiva2") Then
                                ArrAseg(j, 5) = dtV.Rows(j)("PrimaConsecutiva2")
                            End If
                        Next
                    End If

                    tbrow = New TableRow
                    tbcell = New TableCell
                    tbcell.ColumnSpan = IIf(iPrograma = 1, 5, 3)
                    tbcell.Controls.Add(encabenzadoPaquetes(dt.Rows(i)("id_aseguradora"), iPrograma, ArrAseg))
                    tbcell.Controls.Add(encabenzadoAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, iPrograma))
                    tbcell.Controls.Add(DetalleAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, iPrograma))
                    tbcell.Controls.Add(EmisionAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, iPrograma))
                    tbcell.Controls.Add(encabenzadoAseguradora_vacio(iPrograma))
                    tbrow.Controls.Add(tbcell)
                    tb_aseguradora.Controls.Add(tbrow)
                Next
            End If
            dt.Clear()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>" & ex.Message & "</alert>", True)
        End Try
    End Sub

    Public Function encabenzadoAseguradora_vacio(ByVal iPrograma As Integer) As Table
        Dim tb As New Table
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.ColumnSpan = IIf(iPrograma = 1, 4, 2)
        tbcell.Text = "&nbsp;"
        tbrow.Controls.Add(tbcell)
        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function encabenzadoPaquetes(ByVal IdAseguradora As Integer, ByVal iPrograma As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(IIf(iPrograma = 1, 25, 45))
        tbrow.Controls.Add(tbcell)

        Dim dt As DataTable = ccalculo.CargaPaquete(IdAseguradora)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_paquete")
                lbl.Width = Unit.Percentage(100)
                tbcell.Controls.Add(lbl)

                hp = New HyperLink
                hp.Text = "<br />" & "Detalle cobertura "
                hp.Attributes.Add("onclick", "Cobertura(" & IdAseguradora & "," & dt.Rows(i)("id_paquete") & ",'" & Session("modelo") & "','" & Session("CP") & "'," & txt_precio.Text & ")")
                hp.Style.Add("cursor", "pointer")
                tbcell.Controls.Add(hp)

                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "Interiro_tabla_centro"
                tbcell.Width = Unit.Percentage(IIf(iPrograma = 1, 30, 27))
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function

    Public Function encabenzadoAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object, ByVal iPrograma As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = ccliente.DescrAseg
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        Dim dt As DataTable = ccalculo.CargaTipoRecargo(IdAseguradora, iPrograma)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                If dtRegion.Rows.Count > 0 Then
                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                        IdRegion = dtRegion.Rows(0)("id_region")
                    End If
                End If
                dtRegion.Clear()

                NumPagos = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 0)
                'HM 08/01/2014 se agrega el plazo del producto
                NumPlazo = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 1)

                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_periodo")
                lbl.Width = Unit.Percentage(100)
                tbcell.Controls.Add(lbl)

                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "Interiro_tabla_centro"
                tbcell.Width = Unit.Percentage(15)
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function

    Public Function DetalleAseguradora(ByVal IdASeguradora As Integer, ByVal Arr As Object, ByVal iPrograma As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim strcadena As String = ""
        Dim Himage As HtmlImage


        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.RowSpan = IIf(iPrograma = 2, 4, 6)
        Himage = New HtmlImage
        Himage.Src = ccliente.UrlImagAseg
        tbcell.Controls.Add(Himage)
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "negrita"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        For i = 0 To 5
            If (Not (i = 2 And iPrograma = 2) And Not (i = 3 And iPrograma = 2)) Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        strcadena = "Meses de seguro"
                    Case 1
                        strcadena = "No. De pagos"
                        tbrow = New TableRow
                    Case 2
                        strcadena = "Primer Recibo"
                        tbrow = New TableRow
                    Case 3
                        strcadena = "Recibo Subsecuente"
                        tbrow = New TableRow
                    Case 4
                        strcadena = "Prima total"
                        tbrow = New TableRow
                    Case 5
                        strcadena = "Pago Mensual Aprox"
                        tbrow = New TableRow
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 1, "G", 15, ""))

                For x As Integer = 0 To IIf(iPrograma = 1, 3, 1)
                    tbcell = New TableCell
                    Select Case i
                        Case 0
                            strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 1)
                        Case 1
                            strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 0)
                        Case 2
                            If (CDbl(Arr(x, 2)) = CDbl(Arr(x, 1))) Then
                                strcadena = "NA"
                            Else
                                If (x Mod 2) <> 0 And x <> 0 Then
                                    strcadena = Format(CDbl(Arr(x, 2)), "$ #,###,##0.00")
                                Else
                                    strcadena = String.Empty
                                End If
                            End If
                        Case 3
                            If (CDbl(Arr(x, 2)) = CDbl(Arr(x, 1))) Then
                                strcadena = "NA"
                            Else
                                If (x Mod 2) <> 0 And x <> 0 Then
                                    strcadena = Format(CDbl(Arr(x, 5)), "$ #,###,##0.00")
                                Else
                                    strcadena = String.Empty
                                End If
                            End If
                        Case 4
                            strcadena = Format(CDbl(Arr(x, 1)), "$ #,###,##0.00")
                        Case 5
                            strcadena = Format(CDbl(Arr(x, 1) / total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 1)), "$ #,###,##0.00")
                    End Select
                    tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
                Next

                tb.Controls.Add(tbrow)
            End If
        Next

        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    'HM 08/01/2014 se agrega para pagos y plazo
    'Bantipo = 0 pagos
    'Bantipo = 1 plazo
    Public Function total_Pagos(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, ByVal Bantipo As Integer) As Integer
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select
        If Bantipo = 0 Then
            Return IntPagos
        Else
            Return intPlazofinal
        End If
    End Function

    Public Function TipoPrograma(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>" & ex.Message & "</alert>", True)
        End Try
    End Function

    Private Sub recargadatos(ByVal idaonh As Integer, ByVal strmodelo As String, ByVal stranio As Integer, ByVal idplazo As Integer, ByVal plazorestante As Integer, ByVal idmarca As Integer, ByVal strpersona As Integer, ByVal strestatus As String)
        hp_coti.ToolTip = "Seleccione para continuar con el registro"
        hp_coti.Attributes.Add("onclick", "Cliente(" & idaonh & ",'" & strmodelo & "'," & stranio & "," & idplazo & "," & plazorestante & "," & idmarca & "," & strpersona & ",'" & strestatus & "')")
        hp_coti.Style.Add("cursor", "pointer")
        hp_coti1.ToolTip = "Seleccione para continuar con el registro"
        hp_coti1.Attributes.Add("onclick", "Cliente(" & idaonh & ",'" & strmodelo & "'," & stranio & "," & idplazo & "," & plazorestante & "," & idmarca & "," & strpersona & ",'" & strestatus & "')")
        hp_coti1.Style.Add("cursor", "pointer")
    End Sub

    Private Sub rb_promocion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_promocion.SelectedIndexChanged

        cbo_plazo.Items.Clear()
    End Sub

    Private Sub clearSession()

        Session("v_m_c") = Nothing '("v_m_c")
        Session("t_m_c") = Nothing '("t_m_c")
        Session("v_t_p") = Nothing '("v_t_p")
        Session("t_t_p") = Nothing '("t_t_p")
        Session("v_t_p_c") = Nothing '("v_p_c")
        Session("t_t_p_c") = Nothing '("t_p_c")
        Session("v_m_l") = Nothing '("v_m_l")
        Session("t_m_l") = Nothing '("t_m_l")
        Session("v_m_l_c") = Nothing '("v_m_l_c")
        Session("t_m_l_c") = Nothing '("t_m_l_c")
        Session("v_d_s") = Nothing '("v_d_s")
        Session("t_d_s") = Nothing '("t_d_s")
        Session("v_d_s_c") = Nothing '("v_d_s_c")
        Session("t_d_s_c") = Nothing '("t_d_s_c")
        Session("v_u_s") = 1 '("v_u_s")
        Session("t_u_s") = "Particular" '("t_u_s")
        Session("v_u_s_c") = 1 '("v_u_s_c")
        Session("t_u_s_c") = "Particular" '("t_u_s_c")

    End Sub

    Private Async Sub cmd_CalPagos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_CalPagos.Click
        Await Task.Run(Sub()
                           CalculaPagos()
                       End Sub)
    End Sub

    Public Sub CalculaPagos()
        Try
            Dim i As Integer = 0
            Dim bandera As Integer = 0
            hp_coti.Visible = False
            hp_coti1.Visible = False

            If Session("v_m_c") = "" Or Session("v_m_c") = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la marca")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>Favor de seleccionar la marca</alert>", True)
                Exit Sub
            End If

            Session("vehiculoC") = Session("v_d_s_c")
            Session("vehiculoDescC") = Session("t_d_s_c")
            Session("subramoC") = Session("subramo")
            Session("anioC") = Session("t_m_l_c")
            Session("modeloc") = Session("v_t_p_c")
            Session("usoC") = Session("t_u_s_c")


            For i = 0 To rb_tipo.Items.Count - 1
                If rb_tipo.Items(i).Selected = True Then
                    bandera = 1
                    Exit For
                End If
            Next

            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Contribuyente")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>Favor de seleccionar el Tipo de Contribuyente</alert>", True)
                Exit Sub
            End If

            If txtCodigoPostal.Text = String.Empty Then
                MsgBox.ShowMessage("Favor de ingresar un código postal para cotizar")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>Favor de ingresar un código postal para cotizar</alert>", True)
                Exit Sub
            End If

            Session("vehiculo") = Session("v_d_s")
            Session("vehiculoDesc") = Session("t_d_s")
            Session("subramo") = Session("subramo")
            Session("anio") = Session("t_m_l")
            Session("modelo") = Session("v_t_p")
            Session("uso") = Session("t_u_s")
            Session("CodPostal") = txtCodigoPostal.Text

            Session("vehiculo") = Session("vehiculoC")
            Session("vehiculoDesc") = Session("vehiculoDescC")
            Session("subramo") = Session("subramoC")
            Session("anio") = Session("anioC")
            Session("modelo") = Session("modeloc").ToString().Trim()
            Session("uso") = Session("usoC")

            Dim dt As DataTable = ccliente.Carga_definicion_uso(Session("programa"), Session("subramo"), cvalida.QuitaCaracteres(Session("t_u_s"), True), Session("version"))
            If dt.Rows.Count > 0 Then
                Session("intuso") = dt.Rows(0)("id_uso")
            Else
                Session("intuso") = Session("t_u_s")
            End If
            dt.Clear()

            'HM 02/01/2014 se agrega hay que validarlo
            If TipoPrograma(rb_promocion) = 0 Then
                'Si cuenta con promocion
                Session("Exclusion") = 0
                Session("TipoProducto") = 0
            Else
                'No cuenta con promocion 
                Session("Exclusion") = 1
                Session("TipoProducto") = 1

            End If

            Session("catalogo") = IIf(Session("catalogo") = "", "0", Session("catalogo"))
            Session("EstatusV") = estatus_auto()
            Session("MarcaC") = Session("v_m_c")
            Session("MarcaDesc") = Session("t_m_c")
            Session("contribuyente") = personalidad(rb_tipo)
            Session("iva") = 0.16

            'HMH 26052014
            Session("precioEscrito") = txt_precio.Text

            Session("idgrdcontrato") = ccliente.carga_contrato(Session("bid"))

            If Session("Exclusion") = 0 Then
                validaSeguro12()
            End If

            'Se muestra la diferencia entre el monto carta credito vs precio de la unidad
            Session("Diferencia") = Format(CDbl(Session("SAEnt")) - CDbl(Session("SACont")), "##0.00")

            'HM 27/06/2013 se agrega la opción de anual
            If Session("Exclusion") = 1 Then
                cbo_plazo_SelectedIndexChanged(cbo_plazo, EventArgs.Empty)
            End If

            MsgBox.ShowMessage("Recuerda que debes imprimir y firmar tu cotización")
            EnviaMensaje("Recuerda que debes imprimir y firmar tu cotización")

            If cbo_plazo.Items.Count = 0 Then
                MsgBox.ShowMessage("No hay información para cotizar")
                EnviaMensaje("No hay información para cotizar")
                Exit Sub
            End If

            Seg_financiado(rb_seguro)
            calculo_general_aseguradora()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            EnviaMensaje(ex.Message)
        End Try
    End Sub

    Private Function CalPagos() As Action
        Throw New NotImplementedException
    End Function

    Public Function EmisionAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object, ByVal iPrograma As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = ""
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "&nbsp;" 'ccliente.DescrAseg
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        Dim dt As DataTable = ccalculo.CargaTipoRecargo(IdAseguradora, iPrograma)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                If dtRegion.Rows.Count > 0 Then
                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                        IdRegion = dtRegion.Rows(0)("id_region")
                    End If
                End If
                dtRegion.Clear()

                NumPagos = total_Pagos(cbo_plazo.Text, dt.Rows(i)("id_tiporecargo"), 0)
                'HM 08/01/2014 se agrega el plazo del producto
                NumPlazo = total_Pagos(cbo_plazo.Text, dt.Rows(i)("id_tiporecargo"), 1)

                If (Session("nivel") = 0 Or Session("nivel") = 1 Or Session("nivel") = 4) Then

                    hp = New HyperLink
                    hp.ToolTip = "Seleccione la opción que desea emitir"
                    hp.Text = "Emitir póliza "
                    If (Not Arr(i, 2) Is Nothing) Then
                        hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.Text & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        hp.Style.Add("cursor", "pointer")
                        hp.Width = Unit.Percentage(100)
                        tbcell.Controls.Add(hp)
                    End If
                End If
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "AzulSub"
                tbcell.Width = Unit.Percentage(15)
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function

    Protected Sub txtCodigoPostal_TextChanged(sender As Object, e As EventArgs) Handles txtCodigoPostal.TextChanged
        If (txtCodigoPostal.Text <> String.Empty) Then
            Dim _bsCotizacion As New CC.Negocio.Cotizar.Cotizacion
            Dim objEstado As CC.Modelo.Entities.ABC_ESTADO = _bsCotizacion.GetStateByZipCode(txtCodigoPostal.Text)
            If (Not objEstado Is Nothing) Then
                lblCodigoPostal.Text = objEstado.Descripcion_Estado
            Else
                txtCodigoPostal.Text = String.Empty
                MsgBox.ShowMessage("El código postal no existe, favor de verificarlo")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ServerControlScript", "<alert>El código postal no existe, favor de verificarlo</alert>", True)
            End If
        Else
            txtCodigoPostal.Text = String.Empty
        End If
    End Sub

    Private Sub EnviaMensaje(ByVal strMensaje As String)
        'Page.ClientScript.RegisterStartupScript(Me.GetType(), "ServerControlScript", "alert('" & strMensaje.Replace("\r\n", "\\n").Replace("'", "") & "'); ", True)
    End Sub
End Class