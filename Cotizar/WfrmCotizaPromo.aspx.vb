Imports CN_Negocios
Imports System.Threading.Tasks
Imports System.IO

Partial Class WfrmCotizaPromo
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo
    Private PaqSel As String = ""
    Private CdFraccion As CP_FRACCIONES.clsEmision

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Session("ClienteCotiza") = 0
        Session("idpoliza") = 0

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                Session("IsPostBack") = False

                'HM 06/02/2014 si la clave sale del menu izq es valcio
                If Not Request.QueryString("unico") Is Nothing Then
                    Session("IdCotiUnica") = Nothing
                    'evi 15/12/2014
                    Session("idpoliza") = Nothing
                End If

                'unico
                If Not Request.QueryString("click_menu") Is Nothing Then
                    Session("click_menu") = True
                End If

                Dim axy As Long = Session("IdCotiUnica")

                Session("FechaIni") = IIf(ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0", DateTime.Parse(Date.Now).ToString("dd/MM/yyyy"), DateTime.Parse(Date.Now).ToString("MM/dd/yyyy"))
                txt_fecha.Text = DateTime.Parse(Date.Now).ToString("dd/MM/yyyy")
                Session("fincon") = "F"
                Session("ClienteCotiza") = 0
                Session("cotiza") = 0
                'carga_marca()
                'forma pago da�os
                Dim banSeg As Integer
                If Session("seguro") = 0 Then
                    banSeg = 1
                Else
                    banSeg = 0
                End If
                rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))
                Seg_financiado(rb_seguro)

                If Not Request.QueryString("cveAseg") Is Nothing Then
                    'nuevo
                    tipo_poliza(Request.QueryString("cveplazo"))

                    Session("FechaIni") = IIf(ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0", DateTime.Parse(Request.QueryString("strfecha")).ToString("dd/MM/yyyy"), DateTime.Parse(Request.QueryString("strfecha")).ToString("MM/dd/yyyy"))

                    'Session("FechaIni") = Request.QueryString("strfecha")

                    'HM 02/01/2014 se agrega hay que validarlo
                    Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")
                    Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                           Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                           Session("EstatusV"), _
                           0, Session("InterestRate"), Session("FechaIni"), _
                           Session("tiposeguro"), Request.QueryString("cveplazo"), _
                           Request.QueryString("valcost"), _
                           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                           0, Session("vehiculo"), Request.QueryString("cveAseg"), _
                           Request.QueryString("cvepaquete"), Session("programa"), _
                           Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                           Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
                           Request.QueryString("TipoCarga"), Session("Exclusion"))
                    'fin cambio micha

                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results
                    Session("MultArrPolisa") = ccalculo.MultArrPolisa

                    Session("SubRamo") = Request.QueryString("subramo")

                    Session("DatosGenerales") = Nothing
                    Dim arr(25) As String
                    arr(0) = Session("moneda")
                    arr(1) = Request.QueryString("subramo")
                    arr(2) = Session("anio")
                    arr(3) = Session("modelo")
                    arr(4) = 1
                    'arr(5) = Seg_financiado(rb_seguro)
                    arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                    arr(6) = Session("uso")
                    arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arr(8) = Session("EstatusV")
                    arr(9) = 0
                    arr(10) = Session("InterestRate")
                    arr(11) = Session("FechaIni")
                    arr(12) = Session("tiposeguro")
                    arr(13) = Request.QueryString("cveplazo")
                    'arr(14) = txt_precio.Text
                    arr(14) = Request.QueryString("valcost")
                    arr(15) = Session("idgrdcontrato")
                    arr(16) = Session("seguro")
                    arr(17) = Session("bid")
                    arr(18) = 0
                    arr(19) = Session("vehiculo")
                    arr(20) = Request.QueryString("cveAseg")
                    arr(21) = Request.QueryString("cvepaquete")
                    arr(22) = Session("programa")
                    arr(23) = Session("fincon")
                    arr(24) = Request.QueryString("Pago1")
                    arr(25) = Request.QueryString("Pago2")
                    Session("DatosGenerales") = arr

                    'manolito

                    Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                    Session("plazoRecargo") = Request.QueryString("NumPago")

                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("FechaInicio") = Session("FechaIni")
                    Session("contrato") = Request.QueryString("cvecontrato")
                    Session("precioEscrito") = Request.QueryString("valcost")
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Session("Tipocarga") = Request.QueryString("TipoCarga")

                    ''HM 02/01/2014 se agrega hay que validarlo
                    Session("EstatusV") = "N"

                    Dim xs() As String = Session("ArrPoliza")

                    calculoaseguradora12meses(Request.QueryString("valcost"), Request.QueryString("TipoCarga"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), Request.QueryString("cveregion"), Request.QueryString("subramo"))

                    Dim xs1() As String = Session("ArrPoliza")

                    If Session("IdCotiUnica") Is Nothing Then

                        Dim uno As String = Session("vehiculoC")
                        Dim xx As String = Session("idcotizapanel12")
                        Dim s As String = Session("Cuota")
                        Dim s1 As String = Session("MesesTranscurridos")
                        Dim s2 As String = Session("MesesSeguroGratis")
                        Dim s3 As String = Session("idcotizapanel12C")
                        Dim s4 As String = Session("Exclusion")
                        Dim s5 As String = Session("idcotizapanel")
                        Dim s6 As String = Session("idgrdcontrato")
                        Dim s7 As String = Session("bid")
                        Dim s8 As String = Session("vehiculo")
                        Dim s9 As String = Session("modelo")
                        Dim s10 As String = Session("anio")
                        Dim s11 As String = Request.QueryString("valcost")
                        Dim s12 As String = Request.QueryString("cveplazo")
                        Dim s13 As String = Session("MarcaC")
                        Dim s14 As String = Session("programa")
                        Dim s15 As String = Session("contribuyente")
                        Dim s16 As String = Session("EstatusV")
                        Dim s17 As String = Session("FechaInicio")
                        Dim s18 As String = Session("modeloc")
                        Dim s21 As String = Session("vehiculoC")

                        If Session("Exclusion") = "1" Then
                            Session("MesesSeguroGratis") = 0
                        End If


                        Session("IdCotiUnica") = ccalculo.inserta_cotizacion_unica( _
                            Session("idcotizapanel12"), _
                            Session("idcotizapanel"), Session("idgrdcontrato"), _
                            Session("bid"), Session("vehiculo"), Session("modelo"), _
                            Session("anio"), CDbl(Request.QueryString("valcost")), _
                            CInt(Request.QueryString("cveplazo")), _
                            IIf(Session("Exclusion") = 0, CInt(Request.QueryString("cveplazo")), 0), _
                            Session("MarcaC"), Session("programa"), _
                            Session("Exclusion"), Session("contribuyente"), Session("EstatusV"), Session("FechaInicio"), _
                            Session("modeloc"), Session("anioc"), CDbl(Session("MontoCarta")), Session("Diferencia"), _
                            Session("Cuota"), Session("MesesTranscurridos"), _
                            Session("MesesSeguroGratis"), _
                            Session("vehiculoC"), Session("idcotizapanel12C"), Session("CP"))

                    End If

                    Response.Redirect("WfrmCliente.aspx")

                End If
            Else
                Session("IsPostBack") = True
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Public Sub calculoaseguradora12meses(ByVal valcost As Double, ByVal TipoCarga As Integer, _
    ByVal strcatalogo As String, ByVal IdRegion As Integer, ByVal subramo As String)

        Dim dt As DataTable = ccalculo.Promocion12meses(Session("idcotizapanel12"))
        If dt.Rows.Count > 0 Then
            Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                1, "A", _
                Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                Session("EstatusV"), _
                0, Session("InterestRate"), Session("FechaIni"), _
                Session("tiposeguro"), 12, _
                valcost, _
                Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                dt.Rows(0)("idpaquete"), Session("programa"), _
                Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                dt.Rows(0)("id_tipoRecargo"), 1, TipoCarga, , , , 0, 1, 0)
            'fin cambio micha

            Session("subsidios1") = ccalculo.subsidios12
            Session("ArrPoliza1") = ccalculo.ArrPolisa12
            Session("resultado1") = ccalculo.Results12
            Session("MultArrPolisa1") = ccalculo.MultArrPolisa12

            Dim cc() As String = ccalculo.ArrPolisa12

            Session("DatosGenerales1") = Nothing
            Dim arr(25) As String
            arr(0) = Session("moneda")
            arr(1) = subramo
            arr(2) = Session("anio")
            arr(3) = Session("modelo")
            arr(4) = 1
            'arr(5) = Seg_financiado(rb_seguro)
            arr(5) = Seg_financiado_Seleccion(Session("seguro"))
            arr(6) = Session("uso")
            arr(7) = strcatalogo
            arr(8) = Session("EstatusV")
            arr(9) = 0
            arr(10) = Session("InterestRate")
            arr(11) = Session("FechaIni")
            arr(12) = Session("tiposeguro")
            arr(13) = 12
            arr(14) = txt_precio.Text
            arr(15) = Session("idgrdcontrato")
            arr(16) = Session("seguro")
            arr(17) = Session("bid")
            arr(18) = 0
            arr(19) = Session("vehiculo")
            arr(20) = dt.Rows(0)("Id_aseguradora")
            arr(21) = dt.Rows(0)("idpaquete")
            arr(22) = Session("programa")
            arr(23) = Session("fincon")
            arr(24) = dt.Rows(0)("primatotal")
            arr(25) = dt.Rows(0)("primaconsecutiva")
            Session("DatosGenerales1") = arr

            'manolito
            Session("plazo1") = 12
            Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
            Session("plazoRecargo1") = 1
            Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
            Session("paquete1") = dt.Rows(0)("idpaquete")
            Session("subramo1") = subramo
        End If
        dt.Clear()

    End Sub

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Public Function estatus_auto() As String
        Return "N"
    End Function

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Async Sub cmd_cotizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cotizar.Click

        If Not IsNumeric(txt_montocarta.Text) Then
            MsgBox.ShowMessage("El monto es incorrecto")
            Exit Sub
        End If

        If Not IsNumeric(txt_precio.Text) Then
            MsgBox.ShowMessage("El precio es incorrecto")
            Exit Sub
        End If

        Await Task.Run(Sub()
                           CalculosCotizar(sender, e)
                       End Sub)
    End Sub

    Public Sub CalculosCotizar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer = 0
        Dim bandera As Integer = 0
        'lbl_cotizacion.Visible = False
        hp_coti.Visible = False
        hp_coti1.Visible = False

        Try

            If Session("v_m_c") = "" Or Session("v_m_c") = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la Marca")
                Exit Sub
            End If

            'Auto Contratado            
            If Session("t_t_p") = "" Or Session("t_t_p") = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Vehiculo Contratado")
                Exit Sub
            End If

            If Session("v_m_l") = "" Or Session("v_m_l") = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el A�o del Vehiculo Contratado")
                Exit Sub
            End If

            If Session("v_d_s") = "" Or Session("v_d_s") = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la Descripci�n del Vehiculo Contratado")
                Exit Sub
            End If

            If Session("t_u_s") = "" Then
                MsgBox.ShowMessage("Favor de seleccionar el Uso del Vehiculo Contratado")
                Exit Sub
            ElseIf Session("v_u_s") = 0 Then

            End If


            If txt_montocarta.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el monto carta cr�dito")
                Exit Sub
            ElseIf txt_montocarta.Text <= 0 Then
                MsgBox.ShowMessage("El monto carta cr�dito debe de ser mayo a 0")
                Exit Sub
            End If

            Session("vehiculoC") = Session("v_d_s_c")
            Session("vehiculoDescC") = Session("t_d_s_c")
            Session("subramoC") = Session("subramo")
            Session("anioC") = Session("t_m_l_c")
            Session("modeloc") = Session("v_t_p_c")
            Session("usoC") = Session("t_u_s_c")


            'Auto Entregado
            If Session("t_t_p_c") = "" Or Session("t_t_p_c") = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Vehiculo Entregado")
                Exit Sub
            End If

            If Session("v_m_l_c") = "" Or Session("v_m_l_c") = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el A�o del Vehiculo Entregado")
                Exit Sub
            End If

            If Session("v_d_s_c") = "" Or Session("v_d_s_c") = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la Descripci�n del Vehiculo Entregado")
                Exit Sub
            End If

            If Session("t_u_s_c") = "" Or Session("v_u_s_c") = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar el Uso del Vehiculo Entregado")
                Exit Sub
            End If

            If txt_precio.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el monto del Vehiculo Entregado")
                Exit Sub
            End If

            If txt_precio.Text <= 0 Then
                MsgBox.ShowMessage("El monto del Vehiculo debe de ser mayo a 0 ")
                Exit Sub
            End If

            For i = 0 To rb_tipo.Items.Count - 1
                If rb_tipo.Items(i).Selected = True Then
                    bandera = 1
                    Exit For
                End If
            Next
            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Contribuyente")
                Exit Sub
            End If

            Session("vehiculo") = Session("v_d_s")
            Session("vehiculoDesc") = Session("t_d_s")
            Session("subramo") = Session("subramo")
            Session("anio") = Session("t_m_l")
            Session("modelo") = Session("v_t_p")
            Session("uso") = Session("t_u_s")

            Dim dt As DataTable = ccliente.Carga_definicion_uso(Session("programa"), Session("subramo"), cvalida.QuitaCaracteres(Session("t_u_s"), True), Session("version"))
            If dt.Rows.Count > 0 Then
                Session("intuso") = dt.Rows(0)("id_uso")
            Else
                Session("intuso") = Session("t_u_s")
            End If
            dt.Clear()

            'HM 02/01/2014 se agrega hay que validarlo
            If TipoPrograma(rb_promocion) = 0 Then
                'Si cuenta con promocion
                Session("Exclusion") = 0
                Session("TipoProducto") = 0
                cbo_mesesseggra.Enabled = True

            Else
                'No cuenta con promocion 
                Session("Exclusion") = 1
                Session("TipoProducto") = 1
                cbo_mesesseggra.Enabled = False

            End If

            Session("catalogo") = IIf(Session("catalogo") = "", "0", Session("catalogo"))
            Session("EstatusV") = estatus_auto()
            Session("MarcaC") = Session("v_m_c")
            Session("MarcaDesc") = Session("t_m_c")
            Session("contribuyente") = personalidad(rb_tipo)
            Session("iva") = 0.16

            Session("MontoCarta") = txt_montocarta.Text
            Session("precioEscrito") = txt_precio.Text
            carga_plazo()
            Session("idgrdcontrato") = ccliente.carga_contrato(Session("bid"))

            validaSeguro12()

            If Session("programa") <> 29 Then
                dt = New DataTable()
                dt = ccliente.inserta_coberturas( _
                         Session("CadenaCob"), Session("idgrdcontrato"), Session("bid"), Session("programa"), _
                         Session("vehiculo"), Session("EstatusV"), Session("anio"))
                If dt.Rows.Count > 0 Then
                End If
                dt.Clear()
            End If

            'Se muestra la diferencia entre el monto carta credito vs precio de la unidad (Solo es informativo)
            'HM 30062014
            txt_Diferencia.Text = Format(CDbl(Session("SAEnt")) - CDbl(Session("SACont")), "##0.00")
            Session("Diferencia") = txt_Diferencia.Text
            If CDbl(Session("Diferencia")) > 600 Then
                If Session("Exclusion") = 0 Then
                    lbl_diferencia.Visible = True
                    txt_Diferencia.Visible = True
                Else
                    lbl_diferencia.Visible = False
                    txt_Diferencia.Visible = False
                End If
            Else
                lbl_diferencia.Visible = False
                txt_Diferencia.Visible = False
            End If

            'HM 27/06/2013 se agrega la opci�n de anual
            If Session("Exclusion") = 1 Then
                cbo_plazo_SelectedIndexChanged(sender, e)
            End If

            cbo_cuotas.Items.Clear()
            cbo_mesestrans.Items.Clear()
            cbo_mesesseggra.Items.Clear()
            txt_plazosrest.Text = ""

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message & " " & ex.StackTrace.ToString() & " " & Date.Now.ToString())
        End Try
    End Sub
    Public Sub validaSeguro12()
        Try
            Ajuste_plazo_calculo(12, 1)
            'HM 02/01/2014 se agrega hay que validarlo
            'calculo_general(12, 0)            
            calculo_general(12, 0, Session("Exclusion"))
            crearPrima12meses()
            'HM 02/01/2014 se agrega hay que validarlo
            If Session("Exclusion") = 0 Then
                cbo_plazo.SelectedIndex = 0
            Else
                cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'HM 02/01/2014 se agrega hay que validarlo
    'Public Function calculo_general(ByVal NumPlazo As Integer, ByVal IntSubCob As Double) As String
    Public Function calculo_general(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal BanderaPromocion As Integer) As String
        Dim arr() As String = Nothing
        Dim Arr1() As String = Nothing
        Dim i As Integer = 0
        Dim strcadenaC As String = ""
        Dim strcadena As String = ""
        Dim iPrograma As Integer = 0
        Dim arrCadenaResp As String() = Nothing

        Try
            iPrograma = Session("programa")
            'HMH 26052014
            If iPrograma <> 29 Then
                strcadenaC = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                            Session("anioC"), Session("modeloc"), _
                            1, Session("tipopol"), _
                            Session("usoC"), Session("EstatusV"), _
                            0, Session("InterestRate"), Session("FechaIni"), _
                            Session("tiposeguro"), NumPlazo, txt_montocarta.Text, _
                            Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                            IntSubCob, Session("vehiculoC"), Session("programa"), _
                            Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                            Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                            Session("Tipocarga"), , BanderaPromocion)
            Else
                strcadenaC = ccalculo.calculoSP(iPrograma, Session("EstatusV"), Session("vehiculoC"), Session("intmarcabid"), Session("MarcaC"), IIf(BanderaPromocion = 0, 0, 1), "", Session("bid"), Session("idgrdcontrato"), Session("anioC"), _
                                                Session("usoC"), Session("moneda"), txt_montocarta.Text, Session("plazoFinanciamiento"), Session("plazoRecargo"), 1, NumPlazo, Session("IdCotizaCobertura"))

                arrCadenaResp = strcadenaC.Split("$")
                strcadenaC = arrCadenaResp(0)
                Session("CadenaCobCont") = arrCadenaResp(0)
                Session("idcotizapanel12C") = arrCadenaResp(1)
            End If

            If strcadenaC = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto contratado a cotizar, favor de consultar al administrador")
                Return ""
            ElseIf iPrograma <> 29 Then
                Session("CadenaCobCont") = strcadenaC
                Session("idcotizapanel12C") = ccalculo.Inst_Cotizacion_panel_12meses(strcadenaC, "C")
            End If

            If iPrograma <> 29 Then
                strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                            Session("anio"), Session("modelo"), _
                            1, Session("tipopol"), _
                            Session("uso"), Session("EstatusV"), _
                            0, Session("InterestRate"), Session("FechaIni"), _
                            Session("tiposeguro"), NumPlazo, txt_precio.Text, _
                            Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                            IntSubCob, Session("vehiculo"), Session("programa"), _
                           Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                           Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                           Session("Tipocarga"), , BanderaPromocion)
            Else
                strcadena = ccalculo.calculoSP(iPrograma, Session("EstatusV"), Session("vehiculo"), Session("intmarcabid"), Session("MarcaC"), 0, "", Session("bid"), Session("idgrdcontrato"), Session("anio"), _
                                Session("uso"), Session("moneda"), txt_precio.Text, Session("plazoFinanciamiento"), Session("plazoRecargo"), 1, NumPlazo, Session("IdCotizaCobertura"))

                arrCadenaResp = strcadena.Split("$")
                strcadena = arrCadenaResp(0)
                Session("CadenaCob") = arrCadenaResp(0)
                Session("idcotizapanel12") = arrCadenaResp(1)
            End If


            If strcadena = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto entregado a cotizar, favor de consultar al administrador")
                Return ""
            ElseIf iPrograma <> 29 Then
                Session("CadenaCob") = strcadena
                'arr = strcadena.Split("|")
                'For i = 0 To arr.Length - 1
                '    Arr1 = arr(i).Split("*")
                '    If i = 0 Then
                '        PaqSel = Arr1(1)
                '    Else
                '        PaqSel = PaqSel & "," & Arr1(1)
                '    End If
                'Next
                'Session("PaqSelCon") = PaqSel

                'Session("Calculos") = ccalculo.CalculoArr
                'Session("subsidios") = ccalculo.subsidios
                'Session("ArrPoliza") = ccalculo.ArrPolisa
                'Session("resultado") = ccalculo.Results
                'Session("MultArrPolisa") = ccalculo.MultArrPolisa

                Session("idcotizapanel12") = ccalculo.Inst_Cotizacion_panel_12meses(strcadena, "E")

            End If
            Return strcadena

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub Ajuste_plazo_calculo(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, Optional ByVal BanActualiza As Integer = 0)
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                'HM 05062014
                If intplazoIni = 0 Then
                    dbajuste = 0
                Else
                    dbajuste = intplazoIni / intplazoIni
                End If
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select

        ComplementoAjuste(IntCboTipo, intPlazofinal, IntPagos)

        If BanActualiza = 1 Then
            ccalculo.actualiza_numplazo_cob(intPlazofinal, Session("idgrdcontrato"), Session("bid"))
        End If
    End Sub

    Public Sub ComplementoAjuste(ByVal IntCboTipo As Integer, ByVal intPlazofinal As Integer, ByVal IntPagos As Integer)
        lbl_plazo.Text = intPlazofinal
        Session("plazoFinanciamiento") = IntCboTipo
        Session("plazoRecargo") = IntPagos
        'Session("plazoSeleccion") = cbo_plazo.SelectedValue
        Session("plazoSeleccion") = txt_plazosrest.Text
        Session("plazo") = lbl_plazo.Text
        tipo_poliza(lbl_plazo.Text)
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                'Session("seguro") = 2
                Session("tipopol") = "M"
                'Session("bUnidadfinanciada") = True
            Else
                'Session("seguro") = 0
                Session("tipopol") = "A"
                'Session("bUnidadfinanciada") = False
            End If
        Else
            Session("tipopol") = "A"
            'Session("seguro") = 0
            'Session("bUnidadfinanciada") = False
        End If
    End Sub

    Public Sub carga_plazo()
        'HM 31/10/2013
        Dim arrP As New ArrayList
        Dim maxP As Integer = 60
        Dim i As Integer = 0
        If Session("Exclusion") = 0 Then maxP = 48
        arrP.Add("-- Seleccione --")
        arrP.Add(48)
        arrP.Add(60)

        'For i = 1 To maxP
        '    arrP.Add(i)
        'Next
        cbo_plazo.DataSource = arrP
        cbo_plazo.DataBind()
        'Session("opcion") = ""
        'Dim dt As DataTable = ccalculo.carga_plazo(Session("programa"), Session("opcion"))
        'If dt.Rows.Count > 0 Then
        '    cbo_plazo.DataSource = dt
        '    cbo_plazo.DataTextField = "plazo"
        '    cbo_plazo.DataValueField = "id_plazo"
        '    cbo_plazo.DataBind()
        'End If
    End Sub

    Public Sub crearPrima12meses()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arr() As String
        Dim i, j As Integer
        Dim BanMax As Double = 0
        Dim dbMonto As Double = 0
        Dim strcadena As String = ""
        Dim TipoAuto As String = ""

        Try

            TipoAuto = "C"
            tb_seguroC.Controls.Clear()
            If Not Session("CadenaCobCont") = "" Then
                arr = Split(Session("CadenaCobCont"), "|")
                arr.Sort(arr)
                BanMax = VerificaMaximno(arr)
                encabenzado12meses(TipoAuto)
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arr12meses() As String = arr(i).Split("*")
                        ccliente.DatosAseguradora(arr12meses(0))
                        tbrow = New TableRow

                        dbMonto = CDbl(arr12meses(2))
                        If BanMax = dbMonto Then
                            Session("SACont") = dbMonto
                            'aseguradora mas barata
                            Session("12meses") = arr12meses(0)
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                            ccalculo.Upd_Cotizacion_panel_12meses(Session("idcotizapanel12C"), arr12meses(0), "C")
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If

                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = ccliente.DescrAseg
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        tb_seguroC.Controls.Add(tbrow)
                    End If
                Next
            End If

            If Session("Exclusion") = 0 Then
                tb_seguroC.Visible = True
            Else
                tb_seguroC.Visible = False
            End If

            'HMH 26052014 Auto Entregado
            strcadena = ""
            TipoAuto = "E"
            tb_seguro.Controls.Clear()
            If Not Session("CadenaCob") = "" Then
                arr = Split(Session("CadenaCob"), "|")
                arr.Sort(arr)
                BanMax = VerificaMaximno(arr)
                encabenzado12meses(TipoAuto)
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arr12meses() As String = arr(i).Split("*")
                        ccliente.DatosAseguradora(arr12meses(0))
                        tbrow = New TableRow

                        dbMonto = CDbl(arr12meses(2))
                        If BanMax = dbMonto Then
                            Session("SAEnt") = dbMonto
                            'aseguradora mas barata
                            Session("12meses") = arr12meses(0)
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                            ccalculo.Upd_Cotizacion_panel_12meses(Session("idcotizapanel12"), arr12meses(0), "E")
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If

                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = ccliente.DescrAseg
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        tb_seguro.Controls.Add(tbrow)
                    End If
                Next
            End If

            If Session("Exclusion") = 0 Then
                tb_seguro.Visible = True
            Else
                tb_seguro.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub encabenzado12meses(ByVal TipoAuto As String)
        Dim tbcell As TableCell
        Dim tbrow As New TableRow

        tbcell = New TableCell
        tbcell.Text = "Seguro gratis un a�o"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(100)
        'HM 13/02/2014 solo administradores
        If Session("nivel") = 0 Then
            tbcell.ColumnSpan = 3
        Else
            tbcell.ColumnSpan = 2
        End If
        tbrow.Controls.Add(tbcell)
        If TipoAuto = "C" Then
            tb_seguroC.Controls.Add(tbrow)
        Else
            tb_seguro.Controls.Add(tbrow)
        End If

    End Sub

    Public Function agrega_celdas(ByVal strcadena As String, ByVal strclase As String, ByVal intalinea As Integer, _
    ByVal tipoFormato As String, ByVal tamano As Double, Optional ByVal Comentario As String = "") As TableCell
        'intalinea = 
        '1 = izquierda
        '2 = centro
        '3 = derecha
        Dim tbcell As New TableCell
        Dim hp As HyperLink
        Dim Himage As HtmlImage

        Select Case tipoFormato
            Case "G"
                tbcell.Text = strcadena
            Case "I"
                Himage = New HtmlImage
                Himage.Src = strcadena
                tbcell.Controls.Add(Himage)
            Case "H"
                'hp = New HyperLink
                'hp.Text = Comentario
                'hp.NavigateUrl = strcadena
                'tbcell.Controls.Add(hp)
        End Select
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.CssClass = strclase
        tbcell.HorizontalAlign = intalinea
        tbcell.Width = Unit.Percentage(tamano)
        Return tbcell
    End Function

    Public Function VerificaMaximno(ByVal arr As Object) As Double
        Dim i As Integer = 0
        Dim ValorMaximo As Double = 0
        Dim ValorMaximoAux As Double = 0

        For i = 0 To arr.Length - 1
            Dim arrvalida() As String = arr(i).Split("*")
            If arrvalida(2) <> "" Then
                ValorMaximoAux = CDbl(arrvalida(2))
            End If
            If ValorMaximoAux < ValorMaximo Then
                ValorMaximo = ValorMaximoAux
            ElseIf i = 0 Then
                ValorMaximo = ValorMaximoAux
            End If
        Next
        Return ValorMaximo
    End Function

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Private Sub cbo_plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_plazo.SelectedIndexChanged
        Dim i As Integer = 0
        Dim plazo As Integer

        'HM 05062014
        If cbo_plazo.SelectedValue <> "-- Seleccione --" Then
            'MsgBox.ShowMessage("Seleccione el plazo de seguro")
            'Exit Sub
            'HM 06/02/2014 se valida por que cualquier cambio debe de eliminar el numero de cotizacion
            Session("IdCotiUnica") = Nothing
            'Session("plazoSeleccion") = cbo_plazo.SelectedValue
            Session("plazoSeleccion") = txt_plazosrest.Text

            'Seg_financiado(rb_seguro)
            'Ajuste_plazo_calculo(12, 1)
            ''HM 02/01/2014 se agrega hay que validarlo
            ''calculo_general(12, 0)

            'calculo_general(12, 0, Session("Exclusion"))
            crearPrima12meses()

            'calculo_general_aseguradora()

            'HM 02/01/2014 se agrega hay que validarlo
            'If Session("Exclusion") = 0 Then
            '    'lbl_plazo.Text = cbo_plazo.SelectedValue + 12
            '    lbl_plazo.Text = txt_plazosrest.Text + 12
            'Else
            '    'lbl_plazo.Text = cbo_plazo.SelectedValue
            '    lbl_plazo.Text = txt_plazosrest.Text
            'End If

            'Dim dt As DataTable = ccliente.Carga_tipoPago()
            'If dt.Rows.Count > 1 Then
            '    Session("CadenaCob") = ""
            '    For i = 1 To dt.Rows.Count - 1
            '        Ajuste_plazo_calculo(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"))
            '        calculo_general_aseguradora(cbo_plazo.SelectedValue, 0, dt.Rows(i)("id_tiporecargo"))
            '    Next
            'End If

            'HMH 21052014
            'Carga el control de cuotas anticipadas de acuerdo a la informacion seleccionada en plazos contratados.
            plazo = cbo_plazo.SelectedValue

            cbo_cuotas.Items.Clear()
            cbo_mesestrans.Items.Clear()
            cbo_mesesseggra.Items.Clear()
            txt_plazosrest.Text = ""
            For i = 0 To plazo
                If i = 0 Then
                    cbo_cuotas.Items.Add("--Seleccionar--")
                End If
                cbo_cuotas.Items.Add(i)
            Next
        End If

    End Sub

    'Public Function calculo_general_aseguradora(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal Tiporecargo As Integer) As String
    Public Sub calculo_general_aseguradora()
        Dim arr() As String
        Dim Arr1() As String
        Dim i, j, k, x As Integer
        Dim strcadena As String = ""
        Dim strCadenaCal As String = ""
        Dim FechaInicio As String = ""
        Dim FechaTermina As String = ""
        Dim ds As DataSet

        Dim NumPlazo As Integer
        Dim IntSubCob As Integer = 0
        Dim Tiporecargo As Integer = 0
        Dim IdRecargo As Integer = 0
        Dim strInserta As String = ""
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0

        Dim ArrAseg(4, 5) As String

        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arrCadena As String()

        Try

            Dim dt As DataTable = ccliente.Carga_tipoPago(Session("programa"))
            If dt.Rows.Count > 1 Then
                'HMH 26052014
                'NumPlazo = cbo_plazo.SelectedValue
                NumPlazo = txt_plazosrest.Text

                IntSubCob = 0
                For k = 1 To dt.Rows.Count - 1

                    'Ajuste_plazo_calculo(cbo_plazo.SelectedValue, dt.Rows(k)("id_tiporecargo"))
                    Ajuste_plazo_calculo(txt_plazosrest.Text, dt.Rows(k)("id_tiporecargo"))
                    IdRecargo = dt.Rows(k)("id_tiporecargo")

                    'HM 02/01/2014 se agrega hay que validarlo
                    'Session("uso"), Session("EstatusV"), _
                    'HM 08/01/2014 se modifica NumPlazo por lbl_plazo.Text
                    'NumPlazo
                    'Session("tiposeguro"), NumPlazo, txt_precio.Text, _
                    Dim iPrograma As Integer = Session("programa")


                    If iPrograma <> 29 Then
                        strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                                    Session("anio"), Session("modelo"), _
                                    1, Session("tipopol"), _
                                    Session("uso"), IIf(Session("Exclusion") = 0, "U", "N"), _
                                    0, Session("InterestRate"), Session("FechaIni"), _
                                    Session("tiposeguro"), lbl_plazo.Text, txt_precio.Text, _
                                    Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                                    IntSubCob, Session("vehiculo"), Session("programa"), _
                                Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                                Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                                Session("Tipocarga"), , Session("Exclusion"))
                    Else
                        strcadena = ccalculo.calculoSP(iPrograma, IIf(Session("Exclusion") = 0, "U", "N"), Session("vehiculo"), Session("intmarcabid"), Session("MarcaC"), Session("Exclusion"), "", Session("bid"), _
                                                       Session("idgrdcontrato"), Session("anio"), Session("uso"), Session("moneda"), txt_precio.Text, Session("plazoFinanciamiento"), Session("plazoRecargo"), 2, lbl_plazo.Text, Session("IdCotizaCobertura"))
                        arrCadena = strcadena.Split("$")
                        strcadena = arrCadena(0)
                        strCadenaCal = arrCadena(1)
                    End If


                    'strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                    '           Session("anio"), Session("modelo"), _
                    '           1, Session("tipopol"), _
                    '           Session("uso"), IIf(Session("Exclusion") = 0, "U", "N"), _
                    '           0, Session("InterestRate"), Session("FechaIni"), _
                    '           Session("tiposeguro"), lbl_plazo.Text, txt_precio.Text, _
                    '           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                    '           IntSubCob, Session("vehiculo"), Session("programa"), _
                    '       Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                    '       Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                    '       Session("Tipocarga"), , Session("Exclusion"))


                    If strcadena = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    Else
                        If iPrograma <> 29 Then
                            strCadenaCal = ccalculo.StrSalidaPanle
                        End If

                        Select Case IdRecargo
                            Case 2 'Anual
                                Tiporecargo = 1
                            Case 3 'Semestral
                                Tiporecargo = 4
                            Case 4 'Trimestral
                                Tiporecargo = 3
                            Case 5 'Mensual
                                Tiporecargo = 2
                            Case Else
                                'validando contado
                                'Tiporecargo = IdRecargo
                                Tiporecargo = 0
                        End Select

                        Dim arrFinicio() As String
                        arrFinicio = Split(Session("FechaIni"), "/")
                        FechaInicio = Session("FechaIni")

                        'Para pruebas locales se descomento todo el if
                        'If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0" Then
                        '    FechaInicio = arrFinicio(0) & "/" & arrFinicio(1) & "/" & arrFinicio(2)
                        '    'FechaInicio = Format(CDate(FechaInicio), "dd/MM/yyyy")
                        'Else
                        '    FechaInicio = arrFinicio(1) & "/" & arrFinicio(0) & "/" & arrFinicio(2)
                        '    'FechaInicio = Format(CDate(FechaInicio), "MM/dd/yyyy")
                        'End If

                        'FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), cbo_plazo.SelectedValue)
                        FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), lbl_plazo.Text)
                        If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0" Then
                            FechaTermina = Format(CDate(FechaTermina), "dd/MM/yyyy")
                        Else
                            'FechaTermina = Format(CDate(FechaTermina), "dd/MM/yyyy")
                            FechaTermina = Format(CDate(FechaTermina), "MM/dd/yyyy")
                        End If

                        Dim Results() As String
                        Dim Results1() As String

                        arr = strcadena.Split("|")
                        Results = strCadenaCal.Split("|")

                        For i = 0 To arr.Length - 1
                            Arr1 = arr(i).Split("*")
                            Results1 = Results(i).Split("*")

                            If Arr1(0) = Results1(0) Then

                                If Tiporecargo > 0 Then

                                    ds = Nothing
                                    CdFraccion = New CP_FRACCIONES.clsEmision
                                    Select Case CInt(Arr1(0))
                                        'Case 25
                                        '    ds = CdFraccion.FraccionesZurich(FechaInicio, _
                                        '        FechaTermina, _
                                        '        Results1(1), Results1(2), _
                                        '        Results1(3), Results1(4), _
                                        '        Results1(5), Tiporecargo, Results1(6), _
                                        '        (Results1(7) * 100))
                                        Case 59
                                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                        Case 60
                                            ds = CdFraccion.FraccionesGNP_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                        Case 61
                                            ds = CdFraccion.FraccionesAXA_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                    End Select

                                    If ds.Tables(0).Rows.Count > 0 Then
                                        For x = 0 To ds.Tables(0).Rows.Count - 1
                                            Select Case x
                                                Case 0
                                                    Dbtotales1 = ds.Tables(0).Rows(x)("prima_total")
                                                Case 1
                                                    Dbtotales2 = ds.Tables(0).Rows(x)("prima_total")
                                                    Exit For
                                            End Select
                                        Next
                                    End If

                                Else
                                    Dbtotales1 = Results1(5)
                                    Dbtotales2 = 0
                                End If

                                'HM 30/10/2013 se modifico para agregar los datos del panel
                                'HM 02/01/2014 se agrega hay que validarlo
                                If strInserta = "" Then
                                    strInserta = Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                Else
                                    strInserta = strInserta & "|" & Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                End If
                            End If

                        Next
                    End If
                Next

                If Not strInserta = "" Then
                    'HM 02/01/2014 se agrega hay que validarlo
                    Session("idcotizapanel") = ccalculo.Inst_Cotizacion_panel(strInserta, Nothing)
                    recargadatos(Session("v_d_s"), Session("v_t_p"), Session("v_m_l"), txt_plazosrest.Text, cbo_plazo.SelectedValue, Session("v_m_c"), personalidad(rb_tipo), estatus_auto)
                    'lbl_cotizacion.Visible = True
                    hp_coti.Visible = True
                    hp_coti1.Visible = True
                End If
            End If
            dt.Clear()

            dt = ccalculo.CargaCtoizaPanelAseguradora(Session("idcotizapanel"))
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    ccliente.DatosAseguradora(dt.Rows(i)("id_aseguradora"))

                    Dim dtV As DataTable = ccalculo.CargaCtoizaPanel(Session("idcotizapanel"), dt.Rows(i)("id_aseguradora"))
                    If dtV.Rows.Count > 0 Then
                        For j = 0 To dtV.Rows.Count - 1
                            If Not dtV.Rows(i).IsNull("Id_TipoRecargo") Then
                                ArrAseg(j, 0) = dtV.Rows(j)("Id_TipoRecargo")
                            End If
                            If Not dtV.Rows(i).IsNull("PrimaTotal") Then
                                ArrAseg(j, 1) = dtV.Rows(j)("PrimaTotal")
                            End If
                            If Not dtV.Rows(i).IsNull("PrimaConsecutiva") Then
                                ArrAseg(j, 2) = dtV.Rows(j)("PrimaConsecutiva")
                            End If
                            If Not dtV.Rows(i).IsNull("subramo") Then
                                ArrAseg(j, 3) = dtV.Rows(j)("subramo")
                            End If
                            If Not dtV.Rows(i).IsNull("IdPaquete") Then
                                ArrAseg(j, 4) = dtV.Rows(j)("IdPaquete")
                            End If
                            If Not dtV.Rows(i).IsNull("PrimaConsecutiva2") Then
                                ArrAseg(j, 5) = dtV.Rows(j)("PrimaConsecutiva2")
                            End If
                        Next
                    End If

                    tbrow = New TableRow
                    tbcell = New TableCell
                    tbcell.ColumnSpan = 7
                    tbcell.Controls.Add(encabenzadoAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg))
                    tbcell.Controls.Add(DetalleAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg))
                    tbcell.Controls.Add(encabenzadoAseguradora_vacio())
                    tbrow.Controls.Add(tbcell)
                    tb_aseguradora.Controls.Add(tbrow)
                Next
            End If
            dt.Clear()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function encabenzadoAseguradora_vacio() As Table
        Dim tb As New Table
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.ColumnSpan = 5
        tbcell.Text = "&nbsp;"
        tbrow.Controls.Add(tbcell)
        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function encabenzadoAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = ""
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = ccliente.DescrAseg
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        Dim dt As DataTable = ccalculo.CargaTipoRecargo()
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                If dtRegion.Rows.Count > 0 Then
                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                        IdRegion = dtRegion.Rows(0)("id_region")
                    End If
                End If
                dtRegion.Clear()

                'NumPagos = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 0)
                NumPagos = total_Pagos(txt_plazosrest.Text, dt.Rows(i)("id_tiporecargo"), 0)
                'HM 08/01/2014 se agrega el plazo del producto
                'NumPlazo = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 1)
                NumPlazo = total_Pagos(txt_plazosrest.Text, dt.Rows(i)("id_tiporecargo"), 1)

                'If (Session("nivel") = 0 Or Session("nivel") = 1) And Session("Exclusion") = 1 Then
                If (Session("nivel") = 0 Or Session("nivel") = 1 Or Session("nivel") = 4) Then
                    'HM Se quito esta validaci�n para que puedan continuar la emisi�n sin la autorizaci�n del administrador
                    'If Session("Exclusion") = 1 Then
                    hp = New HyperLink
                    hp.ToolTip = "Seleccione la opci�n que desea emitir"
                    hp.ImageUrl = "..\Imagenes\Menu\printer.png"
                    'HM 08/01/2014 se agrega el plazo del producto
                    'hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.SelectedValue & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                    hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & NumPlazo & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                    hp.Style.Add("cursor", "pointer")
                    tbcell.Controls.Add(hp)
                    'Else
                    'If CDbl(Session("Diferencia")) <= 600 Then
                    '    If Session("nivel") = 0 Then
                    '        hp = New HyperLink
                    '        hp.ToolTip = "Seleccione la opci�n que desea emitir"
                    '        hp.ImageUrl = "..\Imagenes\Menu\printer.png"
                    '        'HM 08/01/2014 se agrega el plazo del producto
                    '        'hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.SelectedValue & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                    '        hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & NumPlazo & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                    '        hp.Style.Add("cursor", "pointer")
                    '        tbcell.Controls.Add(hp)
                    '    End If
                    'End If
                End If

                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_periodo")
                tbcell.Controls.Add(lbl)
                'tbcell.Text = "&nbsp;" & dt.Rows(i)("descripcion_periodo")
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "Interiro_tabla_centro"
                tbcell.Width = Unit.Percentage(15)
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function

    Public Function DetalleAseguradora(ByVal IdASeguradora As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim strcadena As String = ""
        Dim Himage As HtmlImage


        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.RowSpan = 4
        'tbcell.Text = "&nbsp;"
        Himage = New HtmlImage
        Himage.Src = ccliente.UrlImagAseg
        tbcell.Controls.Add(Himage)
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "negrita"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        For i = 0 To 3
            tbcell = New TableCell
            Select Case i
                Case 0
                    strcadena = "Meses de seguro"
                Case 1
                    strcadena = "No. De pagos"
                    tbrow = New TableRow
                Case 2
                    strcadena = "Prima parcial"
                    tbrow = New TableRow
                Case 3
                    strcadena = "Prima total"
                    tbrow = New TableRow
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 1, "G", 15, ""))

            tbcell = New TableCell
            Select Case i
                Case 0
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(0, 0), 1)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(0, 0), 1)
                Case 1
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(0, 0), 0)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(0, 0), 0)
                Case 2
                    strcadena = Format(CDbl(Arr(0, 2)), "$ #,###,##0.00")

                Case 3
                    strcadena = Format(CDbl(Arr(0, 1)), "$ #,###,##0.00")

            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))

            tbcell = New TableCell
            Select Case i
                Case 0
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(1, 0), 1)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(1, 0), 1)
                Case 1
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(1, 0), 0)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(1, 0), 0)
                Case 2
                    strcadena = Format(CDbl(Arr(1, 2)), "$ #,###,##0.00")
                Case 3
                    strcadena = Format(CDbl(Arr(1, 1)), "$ #,###,##0.00")
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))

            tbcell = New TableCell
            Select Case i
                Case 0
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(2, 0), 1)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(2, 0), 1)
                Case 1
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(2, 0), 0)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(2, 0), 0)
                Case 2
                    strcadena = Format(CDbl(Arr(2, 2)), "$ #,###,##0.00")
                Case 3
                    strcadena = Format(CDbl(Arr(2, 1)), "$ #,###,##0.00")
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))

            tbcell = New TableCell
            Select Case i
                Case 0
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(3, 0), 1)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(3, 0), 1)
                Case 1
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(3, 0), 0)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(3, 0), 0)
                Case 2
                    strcadena = Format(CDbl(Arr(3, 2)), "$ #,###,##0.00")
                Case 3
                    strcadena = Format(CDbl(Arr(3, 1)), "$ #,###,##0.00")
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))

            tbcell = New TableCell
            Select Case i
                Case 0
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(4, 0), 1)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(4, 0), 1)
                Case 1
                    'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(4, 0), 0)
                    strcadena = total_Pagos(txt_plazosrest.Text, Arr(4, 0), 0)
                Case 2
                    strcadena = Format(CDbl(Arr(4, 2)), "$ #,###,##0.00")
                Case 3
                    strcadena = Format(CDbl(Arr(4, 1)), "$ #,###,##0.00")
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))

            tb.Controls.Add(tbrow)
        Next

        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    'HM 08/01/2014 se agrega para pagos y plazo
    'Bantipo = 0 pagos
    'Bantipo = 1 plazo
    Public Function total_Pagos(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, ByVal Bantipo As Integer) As Integer
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select
        If Bantipo = 0 Then
            Return IntPagos
        Else
            Return intPlazofinal
        End If
    End Function

    Public Function TipoPrograma(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub recargadatos(ByVal idaonh As Integer, ByVal strmodelo As String, ByVal stranio As Integer, ByVal idplazo As Integer, ByVal plazorestante As Integer, ByVal idmarca As Integer, ByVal strpersona As Integer, ByVal strestatus As String)
        hp_coti.ToolTip = "Seleccione para continuar con el registro"
        hp_coti.Attributes.Add("onclick", "Cliente(" & idaonh & ",'" & strmodelo & "'," & stranio & "," & idplazo & "," & plazorestante & "," & idmarca & "," & strpersona & ",'" & strestatus & "')")
        hp_coti.Style.Add("cursor", "pointer")
        hp_coti1.ToolTip = "Seleccione para continuar con el registro"
        hp_coti1.Attributes.Add("onclick", "Cliente(" & idaonh & ",'" & strmodelo & "'," & stranio & "," & idplazo & "," & plazorestante & "," & idmarca & "," & strpersona & ",'" & strestatus & "')")
        hp_coti1.Style.Add("cursor", "pointer")
    End Sub

    Private Sub rb_promocion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_promocion.SelectedIndexChanged
        'cmd_cotizar_Click(sender, e)
        'carga_plazo()

        cbo_plazo.Items.Clear()
        cbo_cuotas.Items.Clear()
        cbo_mesestrans.Items.Clear()
        cbo_mesesseggra.Items.Clear()

        If TipoPrograma(rb_promocion) = 0 Then
            lblSeguroGratis.Visible = True
            cbo_mesesseggra.Visible = True
            lbl_MesSG.Visible = True
        Else
            lblSeguroGratis.Visible = False
            cbo_mesesseggra.Visible = False
            lbl_MesSG.Visible = False
        End If
    End Sub

    Private Sub clearSession()

        Session("v_m_c") = Nothing '("v_m_c")
        Session("t_m_c") = Nothing '("t_m_c")
        Session("v_t_p") = Nothing '("v_t_p")
        Session("t_t_p") = Nothing '("t_t_p")
        Session("v_t_p_c") = Nothing '("v_p_c")
        Session("t_t_p_c") = Nothing '("t_p_c")
        Session("v_m_l") = Nothing '("v_m_l")
        Session("t_m_l") = Nothing '("t_m_l")
        Session("v_m_l_c") = Nothing '("v_m_l_c")
        Session("t_m_l_c") = Nothing '("t_m_l_c")
        Session("v_d_s") = Nothing '("v_d_s")
        Session("t_d_s") = Nothing '("t_d_s")
        Session("v_d_s_c") = Nothing '("v_d_s_c")
        Session("t_d_s_c") = Nothing '("t_d_s_c")
        Session("v_u_s") = 1 '("v_u_s")
        Session("t_u_s") = "Particular" '("t_u_s")
        Session("v_u_s_c") = 1 '("v_u_s_c")
        Session("t_u_s_c") = "Particular" '("t_u_s_c")

    End Sub

    'Private Sub cbo_anioC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_anioC.SelectedIndexChanged
    '    Try
    '        Session("TipoAuto") = "C"
    '        If cbo_anioC.SelectedItem.Text = "-- Seleccione --" Then
    '            MsgBox.ShowMessage("Seleccione el a�o del vehiculo contratado")
    '            Exit Sub
    '        End If
    '        cbo_carga_descripcion(Session("TipoAuto"))
    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '    End Try
    'End Sub

    'Private Sub cbo_descripcionC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_descripcionC.SelectedIndexChanged
    '    Try
    '        If cbo_descripcionC.SelectedValue = 0 Then
    '            MsgBox.ShowMessage("Seleccione la descripci�n del vehiculo")
    '            Exit Sub
    '        End If
    '        datos_descripcion(cbo_descripcionC.SelectedValue)
    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '    End Try
    'End Sub

    Private Sub cbo_cuotas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_cuotas.SelectedIndexChanged
        Try
            'HMH 21052014
            'Carga el control de meses transcurridos de acuerdo a la informacion seleccionada en cuotas anticipadas.
            Dim cuotas As Integer
            Dim i As Integer

            'calculo_general(12, 0, Session("Exclusion"))

            'HMH 20150421
            'crearPrima12meses()

            cuotas = CInt(cbo_plazo.SelectedValue) - CInt(cbo_cuotas.SelectedValue)
            cbo_mesestrans.Items.Clear()
            cbo_mesesseggra.Items.Clear()
            txt_plazosrest.Text = ""
            For i = 0 To cuotas
                If i = 0 Then
                    cbo_mesestrans.Items.Add("--Seleccionar--")
                End If
                cbo_mesestrans.Items.Add(i)
            Next

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_mesestrans_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_mesestrans.SelectedIndexChanged
        Try
            'HMH 21052014
            'Carga el control de meses seguro gratis de acuerdo a la informacion seleccionada en meses transcurridos.
            Dim meses As Integer
            Dim i As Integer

            'calculo_general(12, 0, Session("Exclusion"))

            crearPrima12meses()

            meses = 12
            txt_plazosrest.Text = ""

            If Session("Exclusion") = 0 Then
                cbo_mesesseggra.Items.Clear()
                cbo_mesesseggra.Items.Add("--Seleccionar--")
                cbo_mesesseggra.Items.Add(meses)
                cbo_mesesseggra.SelectedIndex = cbo_mesesseggra.Items.IndexOf(cbo_mesesseggra.Items.FindByValue(12))

                txt_plazosrest.Text = CInt(cbo_plazo.SelectedValue) - CInt(cbo_cuotas.SelectedValue) - CInt(cbo_mesestrans.SelectedValue) - CInt(cbo_mesesseggra.SelectedValue)
                If CInt(txt_plazosrest.Text) <= 0 Then
                    txt_plazosrest.Text = "0"
                End If
            Else
                'Multianual
                txt_plazosrest.Text = CInt(cbo_plazo.SelectedValue) - CInt(cbo_cuotas.SelectedValue) - CInt(cbo_mesestrans.SelectedValue)
                If txt_plazosrest.Text <= 0 Then
                    'cmd_CalPagos.Enabled = False
                    txt_plazosrest.Text = "0"
                Else
                    'If CDbl(Session("Diferencia")) > 600 Then
                    '    cmd_CalPagos.Enabled = False
                    'Else
                    cmd_CalPagos.Enabled = True
                    'End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_mesesseggra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_mesesseggra.SelectedIndexChanged
        Try
            Dim meses As Integer = 0

            'calculo_general(12, 0, Session("Exclusion"))

            crearPrima12meses()

            txt_plazosrest.Text = CInt(cbo_plazo.SelectedValue) - CInt(cbo_cuotas.SelectedValue) - CInt(cbo_mesestrans.SelectedValue) - CInt(cbo_mesesseggra.SelectedValue)

            If CInt(txt_plazosrest.Text) <= 0 Then
                txt_plazosrest.Text = "0"
                'cmd_CalPagos.Enabled = False
            Else
                'If CDbl(Session("Diferencia")) > 600 Then
                '    cmd_CalPagos.Enabled = False
                'Else
                cmd_CalPagos.Enabled = True
                'End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Async Sub cmd_CalPagos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_CalPagos.Click
        Await Task.Run(Sub()
                           CalculaPagos()
                       End Sub)
    End Sub

    Public Sub CalculaPagos()
        Try
            'HM 30062014
            MsgBox.ShowMessage("Recuerda que debes imprimir y firmar tu cotizaci�n")

            If cbo_plazo.Items.Count = 0 Then
                MsgBox.ShowMessage("No hay informaci�n para cotizar")
                Exit Sub
            End If

            If txt_plazosrest.Text = "" Then
                MsgBox.ShowMessage("No hay informaci�n para cotizar")
                Exit Sub
            End If

            Session("Cuota") = cbo_cuotas.SelectedValue
            Session("MesesTranscurridos") = cbo_mesestrans.SelectedValue
            Session("MesesSeguroGratis") = cbo_mesesseggra.SelectedValue

            Seg_financiado(rb_seguro)
            Ajuste_plazo_calculo(12, 1)
            'HM 02/01/2014 se agrega hay que validarlo
            'calculo_general(12, 0)
            calculo_general(12, 0, Session("Exclusion"))
            crearPrima12meses()

            If Session("Exclusion") = 0 Then
                'lbl_plazo.Text = cbo_plazo.SelectedValue + 12
                'lbl_plazo.Text = txt_plazosrest.Text + 12
                lbl_plazo.Text = txt_plazosrest.Text
            Else
                'lbl_plazo.Text = cbo_plazo.SelectedValue
                lbl_plazo.Text = txt_plazosrest.Text
            End If

            calculo_general_aseguradora()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
    '<System.Web.Services.WebMethod()> _
    'Public Function cmdCalPagos() As String
    '    Return CalculaPagos()
    'End Function

    'Public Shared Function CalculaPagos() As String
    '    Try
    '        'If cbo_plazo.Items.Count = 0 Then
    '        'MsgBox.ShowMessage("No hay informaci�n para cotizar")
    '        'Return "No hay informaci�n para cotizar"
    '        'End If

    '        'If txt_plazosrest.Text = "" Then
    '        '    'MsgBox.ShowMessage("No hay informaci�n para cotizar")
    '        '    Return "No hay informaci�n para cotizar"
    '        'End If

    '        'REVISAR PARA QUE SE USAN'
    '        'Session("Cuota") = cbo_cuotas.SelectedValue
    '        'Session("MesesTranscurridos") = cbo_mesestrans.SelectedValue
    '        'Session("MesesSeguroGratis") = cbo_mesesseggra.SelectedValue

    '        Seg_financiado(rb_seguro)
    '        Ajuste_plazo_calculo(12, 1)
    '        'HM 02/01/2014 se agrega hay que validarlo
    '        'calculo_general(12, 0)
    '        calculo_general(12, 0, Session("Exclusion"))
    '        crearPrima12meses()

    '        If Session("Exclusion") = 0 Then
    '            'lbl_plazo.Text = cbo_plazo.SelectedValue + 12
    '            'lbl_plazo.Text = txt_plazosrest.Text + 12
    '            lbl_plazo.Text = txt_plazosrest.Text
    '        Else
    '            'lbl_plazo.Text = cbo_plazo.SelectedValue
    '            lbl_plazo.Text = txt_plazosrest.Text
    '        End If

    '        calculo_general_aseguradora()
    '        Return "OK"
    '    Catch ex As Exception
    '        Return ex.Message.ToString()
    '    End Try
    'End Function

    Private Function CalPagos() As Action
        Throw New NotImplementedException
    End Function

    Protected Sub txt_montocarta_TextChanged(sender As Object, e As EventArgs) Handles txt_montocarta.TextChanged

    End Sub
End Class
