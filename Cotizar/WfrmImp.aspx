<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmImp.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmImp" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
		<style type="text/css">#wait { POSITION: fixed; WIDTH: 100%; DISPLAY: none; HEIGHT: 100%; TOP: 0px; LEFT: 0px }
	#mask { Z-INDEX: 9000; POSITION: absolute; BACKGROUND-COLOR: #fff; DISPLAY: none; TOP: 0px; LEFT: 0px }
	#boxes .window { Z-INDEX: 9999; POSITION: absolute; PADDING-BOTTOM: 20px; PADDING-LEFT: 20px; WIDTH: 440px; PADDING-RIGHT: 20px; DISPLAY: none; HEIGHT: 200px; PADDING-TOP: 20px }
	#boxes #dialog { BACKGROUND-COLOR: #fff; WIDTH: 375px; HEIGHT: 203px; align: center }
		</style>
		<script src="../js/jquery.js"></script>
		<script language="javascript">

			function muestraCapa(){
			//select all the a tag with name equal to modal
			        var muestra = function () {
			        //Get the A tag
			        var id = '#dialog'; //$(this).attr('href');

			        //Get the screen height and width
			        var maskHeight = $(document).height();
			        var maskWidth = $(window).width();

			        //Set height and width to mask to fill up the whole screen
			        $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

			        //transition effect    
			        $('#mask').fadeIn(1000);
			        $('#mask').fadeTo("slow", 0.9);
			        //$('#mask').fadeTo("slow", 0.8); 
			        //$('#mask').fadeTo("slow",0); 

			        //Get the window height and width
			        var winH = $(window).height();
			        var winW = $(window).width();

			        //Set the popup window to center
			        $(id).css('top', winH / 2 - $(id).height() / 2);
			        $(id).css('left', winW / 2 - $(id).width() / 2);

			        //transition effect
			        $(id).fadeIn(2000);

			        //Hacer llamada ajax; no necesita parametros, todos los datos estan en Session
			        $.ajax({
			            url: 'WfrmImpPDF.aspx',
			            success: function (xhr) {
			                var mensajeSalida = xhr;

			                ocultaCapa();

			                //alert(mensajeSalida);
			                
			                //Asignar valores de retorno
			                //document.write(mensajeSalida)	//Se puede manipular el mensaje si es un error				
			                if (mensajeSalida == 'Error') {
			                    //EHG -  Se hace postback a un boton y ejecutar el dibujado del cuadro
			                    //__doPostBack('<%= cmdCreaError.ClientID %>','');
			                    __doPostBack('cmdCreaError', '');
			                    
			                }
			                else {
			                    //EHG -  Se hace postback a un boton y ejecutar el dibujado del cuadro
			                    //__doPostBack('<%= cmdCreaTabla.ClientID %>','');
			                    __doPostBack('cmdCreaTabla', '');
			                }
			            }
			        });
			    }
			
			muestra();
			
			}
			
			function ocultaCapa(){
		        $('.window').hide();
		        $('#mask').hide();
			}
		    
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<div id="mask"></div>
		<div id="boxes">
			<div class="window" id="dialog">
				<p align="center"><Img height="50" src="..\Imagenes\imagen_espera\spinner.gif" width="50"></p>
			</div>
		</div>
		<form id="Form1" method="post" runat="server">
			<HR style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 104; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;
							<asp:label id="lbl_titulo" runat="server" Width="100%"> Datos del Veh�culo</asp:label></FONT></TD>
				</TR>
			</TABLE>
			<TABLE style="Z-INDEX: 102; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD colSpan="2"><asp:panel id="pnl_auto" runat="server" Width="100%">
							<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="obligatorio" width="30%" align="right">*Fecha de inicio de 
										Vigencia&nbsp;:</TD>
									<TD width="35%">
										<asp:textbox id="txt_fecha" runat="server" Width="60%" AutoPostBack="True" MaxLength="10" CssClass="combos_small"
											ReadOnly="True"></asp:textbox>&nbsp;&nbsp;
										<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" AutoPostBack="False" From-Today="True"
											ShowErrorMessage="False" Control="txt_fecha" Separator="/" BorderStyle="Solid" InvalidDateMessage="D�a Inv�lido"
											Shadow="True" ShowWeekend="True" Move="True" Fade="0.5" RequiredDateMessage="La Fecha es Requerida"
											Culture="es-MX Espa�ol (M�xico)" Buttons="[<][m][y]  [>]" TextMessage="La fecha es Incorrecta"
											BackColor="Yellow" BorderWidth="1px" BorderColor="Black" To-Today="True" Height="8px" To-Increment="4"></rjs:popcalendar>
										<asp:Label id="lblfecha" runat="server" Width="100%" CssClass="negrita"></asp:Label></TD>
									<TD width="35%"></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">
										<asp:Label style="Z-INDEX: 0" id="lbl_contrato" runat="server" Width="100%">*No. contrato :</asp:Label></TD>
									<TD>
										<asp:Label id="lbl_bid" runat="server" CssClass="combos_small"></asp:Label>
										<asp:textbox id="txt_nocontrato" tabIndex="1" runat="server" Width="60%" MaxLength="15" CssClass="combos_small"
                                            onkeypress="javascript:onlyDigitsLetters(event);"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">
										<asp:Label style="Z-INDEX: 0" id="lbl_grupo" runat="server" Width="100%">*Grupo :</asp:Label></TD>
									<TD>
										<asp:textbox style="Z-INDEX: 0" id="txt_grupo" tabIndex="2" onkeypress="javascript:onlyDigits(event,'decOK');"
											runat="server" Width="60%" MaxLength="4" CssClass="combos_small"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">
										<asp:Label style="Z-INDEX: 0" id="lbl_integrante" runat="server" Width="100%">*Integrante :</asp:Label></TD>
									<TD>
										<asp:textbox style="Z-INDEX: 0" id="txt_integrante" tabIndex="3" onkeypress="javascript:onlyDigits(event,'decOK');"
											runat="server" Width="60%" MaxLength="3" CssClass="combos_small"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">*N�mero de Serie :</TD>
									<TD>
										<asp:TextBox id="txt_serie" tabIndex="4" onkeypress="javascript:onlyDigitsLetters(event);" runat="server" Width="100%"
											MaxLength="17" CssClass="combos_small"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio" align="right">*Motor :</TD>
									<TD>
										<asp:TextBox id="txt_motor" tabIndex="5" onkeypress="javascript:onlyAllLettersSpaceDigits(event);" runat="server" Width="100%"
											MaxLength="30" CssClass="combos_small"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">
                                        <asp:Label ID="lblERepuve" runat="server" Text="Repuve :"></asp:Label>
                                    </TD>
									<TD>
										<asp:TextBox id="txt_renave" tabIndex="6" onkeypress="javascript:onlyDigitsLetters(event);" runat="server"
											Width="100%" MaxLength="8" CssClass="combos_small"></asp:TextBox></TD>
                                     <TD></TD>
								</TR>
                                <TR>
									<TD>&nbsp;</TD>
									<TD class="obligatorio">&nbsp;</TD>
								</TR>
                                <TR>									
									<TD class="obligatorio" colspan="2">Especificar datos del contacto de la financiera para efectos de cobro.(nombre, correo y tel�fono)</TD>
								</TR>
                                <TR>
									<TD class="negrita" align="right">
                                        <asp:Label ID="lblContacto" runat="server" Text="Nombre de Contacto:"></asp:Label>
                                    </TD>
									<TD>
										<asp:TextBox id="txtContactoCob" tabIndex="6" runat="server" onkeypress="javascript:onlyAllLettersSpaceDigits(event);"
											Width="100%" MaxLength="150" CssClass="combos_small"></asp:TextBox></TD>
								</TR>       
                                <TR>
									<TD class="negrita" align="right">
                                        <asp:Label ID="Label1" runat="server" Text="Correo de Contacto:"></asp:Label>
                                    </TD>
									<TD>
										<asp:TextBox id="txtCorreoContacto" tabIndex="6" onkeypress="javascript:CaracterNoValido(event);" runat="server"
											Width="100%" MaxLength="150" CssClass="combos_small"></asp:TextBox></TD>
								</TR>       
                                <TR>
									<TD class="negrita" align="right">
                                        <asp:Label ID="Label2" runat="server" Text="Tel�fono de Contacto:"></asp:Label>
                                    </TD>
									<TD>
										<asp:TextBox id="txtTelContacto" tabIndex="6" onkeypress="javascript:onlyDigits(event,'decOK');" runat="server"
											Width="100%" MaxLength="150" CssClass="combos_small"></asp:TextBox></TD>
								</TR>                                
								<TR>
									<TD class="obligatorio" align="right">
										<asp:Label id="lbl_factura" runat="server" Width="100%">*N�mero de Factura :</asp:Label></TD>
									<TD>
										<asp:TextBox id="txt_factura" tabIndex="7" onkeypress="convierteMayusculas();" runat="server"
											Width="100%" MaxLength="10" CssClass="combos_small"></asp:TextBox></TD>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD class="obligatorio_negro"></TD>
									<TD>
										<asp:TextBox id="txt_placa" tabIndex="8" onkeypress="convierteMayusculas();" runat="server" Width="100%"
											CssClass="combos_small" Visible="False"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="obligatorio"></TD>
									<TD align="center">
										<asp:button id="cmd_continuar" runat="server" CssClass="boton" Text="Emitir"></asp:button></TD>
									<TD>
										<asp:TextBox id="txt_contrato" runat="server" Width="16px" CssClass="combos_small" Height="16px"
											Visible="False"></asp:TextBox>
										<asp:TextBox id="txt_bene" runat="server" Width="16px" CssClass="combos_small" Height="16px"
											Visible="False"></asp:TextBox>
										<asp:TextBox id="txt_vida" runat="server" Width="16px" CssClass="combos_small" Height="16px"
											Visible="False"></asp:TextBox>
										<asp:TextBox id="txt_desempleo" runat="server" Width="16px" CssClass="combos_small" Height="16px"
											Visible="False"></asp:TextBox></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD vAlign="top" colSpan="2"></TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2"><asp:table id="tb_imprime" runat="server" Width="100%" CssClass="combos_small"></asp:table></TD>
					<TD vAlign="top" colSpan="2"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD><ONTRANET:SAVEDIALOG id="sd" runat="server"></ONTRANET:SAVEDIALOG></TD>
					<TD><asp:linkbutton id="cmdCreaTabla" runat="server"></asp:linkbutton></TD>
					<TD><asp:linkbutton id="cmdCreaError" runat="server"></asp:linkbutton></TD>
				</TR>
			</TABLE>
			<cc1:msgbox style="Z-INDEX: 103; POSITION: absolute; TOP: 560px; LEFT: 408px" id="MsgBox" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
