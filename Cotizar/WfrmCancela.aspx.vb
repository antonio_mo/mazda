Imports CN_Negocios

Partial Class WfrmCancela
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cprincipal As New CnPrincipal
    Private cvalidar As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina        
        Try
            Dim ccliente As New CnPrincipal

            If Session("usuario") = "" Or Session("usuario") = Nothing Or Not ccliente.PermisosMenu(Session("IdNivel"), 4) Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                carga_grd()
                carga_combos()
                If Session("Programa") = 32 Then
                    chkLCriterio.Visible = False
                    chkLCriterioM.Visible = True
                Else
                    chkLCriterio.Visible = True
                    chkLCriterioM.Visible = False
                End If
                'Carga_OpcionesChkBox(Session("Programa"))
                limpia_campos()
                lbl_poliza.Visible = False
                l1.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia_campos()
        txt_poliza.Text = ""
        txt_serie.Text = ""
        lbl_poliza.Text = ""
    End Sub

    Public Sub carga_grd()
        Dim dt As DataTable = ccliente.carga_cancelacion(Session("bid"), Session("programa"), Session("nivel"))
        If dt.Rows.Count > 0 Then
            grd_cancelacion.DataSource = dt
            grd_cancelacion.DataBind()
        End If
    End Sub

    Public Sub carga_combos()

        'Distribuidores
        Dim dtd As DataTable = ccliente.determina_bid_consultas(Session("usuario"), Session("programa"), Session("intmarcabid"))
        If dtd.Rows.Count > 0 Then
            cbo_deeler.DataSource = dtd
            cbo_deeler.DataTextField = "nombre"
            cbo_deeler.DataValueField = "id_bid"
            cbo_deeler.DataBind()
            pnl_deeler.Visible = True
        Else
            pnl_deeler.Visible = False
        End If

        Dim dt As DataTable = ccliente.carga_abccancelacion
        If dt.Rows.Count > 0 Then
            cbo_motivo.DataSource = dt
            cbo_motivo.DataTextField = "descripcion_cancelacion"
            cbo_motivo.DataValueField = "id_cancelacion"
            cbo_motivo.DataBind()
        End If
    End Sub

    Private Sub Carga_OpcionesChkBox(Programa As Integer)
        'Dim i As Integer
        'Dim NoElementos As Integer
        'Dim arrOpciones(6) As String

        ''CONAUTO
        ''1.poliza = 7
        ''2.marca = 4
        ''3.modelo = 5
        ''4.serie = 1
        ''5.asegurado = 6
        ''6.aseguradora = 8

        ''MAZDA
        ''1.poliza = 7
        ''2.aseguradora = 8
        ''3.serie = 1
        ''4.asegurado = 6

        'Try
        '    chkLCriterio.Items.Clear()
        '    If Programa = 32 Then
        '        NoElementos = 4
        '        For i = 0 To NoElementos
        '            If i = 0 Then
        '                arrOpciones(i) = "poliza,7"
        '            ElseIf i = 1 Then
        '                arrOpciones(i) = "aseguradora,8"
        '            ElseIf i = 2 Then
        '                arrOpciones(i) = "serie,1"
        '            ElseIf i = 3 Then
        '                arrOpciones(i) = "asegurado,6"
        '            End If
        '        Next
        '    Else
        '        NoElementos = 6
        '        For i = 0 To NoElementos
        '            If i = 0 Then
        '                arrOpciones(i) = "poliza,7"
        '            ElseIf i = 1 Then
        '                arrOpciones(i) = "marca,4"
        '            ElseIf i = 2 Then
        '                arrOpciones(i) = "modelo,5"
        '            ElseIf i = 3 Then
        '                arrOpciones(i) = "serie,1"
        '            ElseIf i = 3 Then
        '                arrOpciones(i) = "asegurado,6"
        '            ElseIf i = 3 Then
        '                arrOpciones(i) = "aseguradora,8"
        '            End If
        '        Next
        '    End If

        '    For i = 0 To NoElementos - 1
        '        Dim arrValor() As String
        '        arrValor = Split(arrOpciones(i), ",")
        '        chkLCriterio.Items.Add(New ListItem(chkLCriterio.Item(arrValor(0))

        '        chkLCriterio.DataTextField = arrValor(0)
        '        chkLCriterio.DataValueField = arrValor(1)
        '    Next

        'Catch ex As Exception
        '    MsgBox.ShowMessage(ex.Message)
        'End Try
    End Sub

    Private Sub grd_cancelacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cancelacion.SelectedIndexChanged
        txt_poliza.Text = grd_cancelacion.Items(grd_cancelacion.SelectedIndex).Cells(1).Text
        txt_serie.Text = grd_cancelacion.Items(grd_cancelacion.SelectedIndex).Cells(2).Text
        lbl_poliza.Text = grd_cancelacion.Items(grd_cancelacion.SelectedIndex).Cells(1).Text
        l1.Visible = True
        lbl_poliza.Visible = True
    End Sub

    Private Sub grd_cancelacion_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cancelacion.ItemCommand
        txt_poliza.Text = e.Item.Cells(1).Text
        txt_serie.Text = e.Item.Cells(2).Text
        lbl_poliza.Text = e.Item.Cells(1).Text
        l1.Visible = True
        lbl_poliza.Visible = True
    End Sub

    Private Sub cmd_agregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_agregar.Click
        Dim item_grd As DataGridItem
        Dim strsalida As String = ""
        Dim strPoliza As String = ""
        Dim i As Integer = 0

        Try
            If cbo_motivo.SelectedValue = 0 Then
                MsgBox.ShowMessage("Seleccione el motivo de la cancelaci�n")
                Exit Sub
            End If

            viewstate("StrCancela") = ""
            For i = 0 To grd_cancelacion.Items.Count - 1
                item_grd = grd_cancelacion.Items(i)
                Dim ckb As CheckBox = item_grd.FindControl("cbk_grd")
                If ckb.Checked = True Then
                    If viewstate("StrCancela") = "" Then
                        viewstate("StrCancela") = item_grd.Cells(1).Text
                        strPoliza = "P�liza: " & item_grd.Cells(1).Text
                    Else
                        viewstate("StrCancela") = viewstate("StrCancela") & "|" & item_grd.Cells(1).Text
                        'salto de linea
                        strPoliza = strPoliza & "\n" & "P�liza: " & item_grd.Cells(1).Text
                    End If
                End If
            Next i

            If viewstate("StrCancela") = "" Then
                MsgBox.ShowMessage("Favor de seleccionar la(s) p�liza(s) que desea cancelar")
                Exit Sub
            End If

            strPoliza = "Esta seguro de que desea cancelar las siguiente P�lizas :\n" & strPoliza
            MsgBox.ShowConfirmation(strPoliza, "Cancela", True, False)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub MsgBox_YesChoosed(ByVal sender As Object, ByVal Key As String) Handles MsgBox.YesChoosed
        Try
            Dim arrPolizas() As String
            Dim Polizas As String = ""
            Dim PolizasCanceladas As String = ""
            Dim PolizasNoCanceladas As String = ""
            Dim NombreContacto As String = ""
            Dim TelefonoContacto As String = ""
            Dim CorreoContacto As String = ""

            If Key = "Cancela" Then
                ''If ccliente.valida_dias_cancelacion(txt_poliza.Text) <= 5 Or Session("nivel") = 0 Then
                ''evi 06/09/2011 cancelacion usuario

                Polizas = ccliente.inserta_cancelpol(ViewState("StrCancela"), Session("bid"), _
                txt_serie.Text, cbo_motivo.SelectedValue, Session("nivel"), Session("usuario"), Session("programa"))

                arrPolizas = Split(Polizas, "|")
                PolizasCanceladas = arrPolizas(0)
                PolizasNoCanceladas = arrPolizas(1)

                'MsgBox.ShowMessage("Los datos de la cancelaci�n se registraron correctamente")
                ViewState("StrCancela") = ""
                carga_grd()
                carga_combos()
                limpia_campos()

                'Dim dt As DataTable = ccliente.obtiene_datos_administrador(Session("programa"))
                'If dt.Rows.Count > 0 Then
                '    If Not dt.Rows(0).IsNull("Nombre") Then
                '        NombreContacto = dt.Rows(0).Item("Nombre")
                '    End If
                '    If Not dt.Rows(0).IsNull("email") Then
                '        CorreoContacto = dt.Rows(0).Item("email")
                '    End If
                '    If Not dt.Rows(0).IsNull("Telefono") Then
                '        TelefonoContacto = dt.Rows(0).Item("Telefono")
                '    End If
                'End If

                'If PolizasCanceladas <> "" And PolizasNoCanceladas = "" Then
                '    MsgBox.ShowMessage("La(s) p�liza(s) ya fueron canceladas")
                'ElseIf PolizasCanceladas = "" And PolizasNoCanceladas <> "" Then
                '    MsgBox.ShowMessage("Para realizar la(s) cancelacion(es) de est�(s) p�liza(s) : " & PolizasNoCanceladas & " comuniquese con " & NombreContacto & " al Tel. " & TelefonoContacto & " � por correo electr�nico: " & CorreoContacto & "")
                'ElseIf PolizasCanceladas <> "" And PolizasNoCanceladas <> "" Then
                '    MsgBox.ShowMessage("La(s) siguiente(s) p�liza(s) ya fueron canceladas: " & PolizasCanceladas & ", para realizar la(s) cancelacion(es) de esta(s) p�liza(s) : " & PolizasNoCanceladas & " comuniquese con " & NombreContacto & " al Tel. " & TelefonoContacto & " � por correo electr�nico: " & CorreoContacto & "")
                'End If

                If PolizasCanceladas <> "" And PolizasNoCanceladas = "" Then
                    MsgBox.ShowMessage("La(s) p�liza(s) ya fueron canceladas")
                ElseIf PolizasCanceladas = "" And PolizasNoCanceladas <> "" Then
                    MsgBox.ShowMessage("Para realizar la(s) cancelacion(es) de esta(s) p�liza(s) : " & Polizas & " comuniquese con FelipeRomero al Tel. 5228.7000 ext. 7020 � por e-mail  felipe.romero@aon.com")
                ElseIf PolizasCanceladas <> "" And PolizasNoCanceladas <> "" Then
                    MsgBox.ShowMessage("La(s) siguiente(s) p�liza(s) ya fueron canceladas: " & PolizasCanceladas & ", para realizar la(s) cancelacion(es) de esta(s) p�liza(s) : " & PolizasNoCanceladas & " comuniquese con FelipeRomero al Tel. 5228.7000 ext. 7020 � por e-mail  felipe.romero@aon.com")
                End If

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        carga_grd()
        carga_combos()
        limpia_campos()
        lbl_poliza.Visible = False
        l1.Visible = False
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try

            StrCriterios = ""

            If Session("Programa") = 32 Then
                For i = 0 To chkLCriterioM.Items.Count - 1
                    If chkLCriterioM.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterioM.Items(i).Value) & "|"
                    End If
                Next
            Else
                For i = 0 To chkLCriterio.Items.Count - 1
                    If chkLCriterio.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                    End If
                Next
            End If
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If

            If pnl_deeler.Visible = False Then
                intbid = Session("bid")
            Else
                If cbo_deeler.SelectedValue = 0 Then
                    intbid = Session("bid")
                    'intbid = 0
                Else
                    intbid = cbo_deeler.SelectedValue
                    'HMH 05092014
                    Session("bid") = intbid
                End If
            End If

            dt = ccliente.carga_cancelacion(intbid, Session("programa"), Session("nivel"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                grd_cancelacion.DataSource = dt
                grd_cancelacion.DataBind()
            Else
                MsgBox.ShowMessage("No hay registros con esos criterios de b�squeda")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
