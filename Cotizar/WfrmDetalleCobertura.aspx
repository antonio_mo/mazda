﻿<%@ Page Async="true" Language="vb" AutoEventWireup="false" CodeBehind="WfrmDetalleCobertura.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmDetalleCobertura" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
        <title>Detalle de coberturas</title>
        <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="..\Css\submodal.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="..\JavaScript\submodalsource.js" type="text/javascript"></script>
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
    </head>
    <body MS_POSITIONING="GridLayout">
        <form id="form1" method="post" runat="server">
            <TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table1" border="2"
				    cellSpacing="1" cellPadding="1" width="100%">
                <TR>
					<td class="Interiro_tabla_centro" width="50%">Cobertura</td>
                    <td class="Interiro_tabla_centro">Suma Asegurada</td>
                    <td class="Interiro_tabla_centro" width="20%">Deducible</td>
				</TR>
                <TR>
                    <TD colSpan="3">
                        <asp:Table ID="tb_cobertura" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                            GridLines="Both">
                        </asp:Table>
                        <cc1:MsgBox Style="z-index: 0" ID="MsgBox1" runat="server"></cc1:MsgBox>
                    </TD>
				</TR>
            </TABLE>
            
            <cc1:MsgBox style="Z-INDEX: 102; POSITION: absolute; TOP: 416px; LEFT: 360px" id="MsgBox" runat="server"></cc1:MsgBox>
        </form>
    </body>
</html>
