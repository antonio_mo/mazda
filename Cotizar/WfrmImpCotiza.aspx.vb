Imports System.Threading
Imports CN_Negocios
Imports CP_ReportePDF
Imports System.Web.Mail
Imports Cotizador_Mazda_Retail.WsCorreo

'Ambiente PROD
'http://192.168.66.233/MailServiceAON/MailSMTP.asmx

'Ambiente QA
'http://192.168.166.192/MailServiceAON/MailSMTP.asmx 

'Ambiente DEV
'http://192.168.166.193/MailServiceAON/MailSMTP.asmx 

Partial Class WfrmImpCotiza
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnPrincipal
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo
    'Private Correo As New EnvioCorreo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try

            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("banCreacion") Is Nothing Then
                    'Dim url As String = "D:/Aplicaciones/CotizadorContado_Conauto/Aplicaciones/Reporte/Generados/" & Request.QueryString("cvemonbre")
                    'sd.FileName = "Cotizacion " & Session("IdCtotizacion")
                    'sd.File = url
                    'sd.ShowDialogBox()

                    Dim url As String = ConfigurationSettings.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA) & _
                        CC.General.Diccionario.Carpetas.GENERADOCOTIZACION & Request.QueryString("cvemonbre")
                    If System.IO.File.Exists(url) Then
                        Dim nombre As String = "Cotizacion " & Session("IdCtotizacion") & ".pdf"
                        Response.Buffer = False
                        Response.Clear()
                        Response.ClearContent()
                        Response.ClearHeaders()
                        Response.ContentType = "application/pdf"
                        Response.AddHeader("Content-Disposition", "attachment; filename=""" & nombre & "")
                        Response.TransmitFile(url)
                        Response.End()
                    Else
                        MsgBox.ShowMessage("Favor de consultar a su administrador,\n la car�tula de la p�liza no se gener�")
                    End If

                Else
                    Dim idaonh As Integer = 0
                    Dim strmodelo As String = ""
                    Dim stranio As Integer = 0
                    Dim dbSA As Double = 0
                    Dim idplazo As Integer = 0
                    Dim plazorestante As Integer = 0
                    Dim idmarca As Integer = 0
                    Dim strpersona As Integer = 0
                    Dim strestatus As String = ""
                    Dim StrFecIni As String = ""
                    limpia()
                    If Not Request.QueryString("idaonh") Is Nothing Then
                        idaonh = Request.QueryString("idaonh")
                    End If
                    If Not Request.QueryString("strmodelo") Is Nothing Then
                        strmodelo = Request.QueryString("strmodelo")
                    End If
                    If Not Request.QueryString("stranio") Is Nothing Then
                        stranio = Request.QueryString("stranio")
                    End If
                    If Not Request.QueryString("dbSA") Is Nothing Then
                        dbSA = Request.QueryString("dbSA")
                    End If
                    If Not Request.QueryString("idplazo") Is Nothing Then
                        idplazo = Request.QueryString("idplazo")
                    End If
                    If Not Request.QueryString("plazorestante") Is Nothing Then
                        plazorestante = Request.QueryString("plazorestante")
                    End If
                    If Not Request.QueryString("strmarca") Is Nothing Then
                        idmarca = Request.QueryString("strmarca")
                    End If
                    If Not Request.QueryString("strpersona") Is Nothing Then
                        strpersona = Request.QueryString("strpersona")
                    End If
                    If Not Request.QueryString("strestatus") Is Nothing Then
                        strestatus = Request.QueryString("strestatus")
                    End If
                    If Not Request.QueryString("strfecha") Is Nothing Then
                        StrFecIni = Request.QueryString("strfecha")
                    End If

                    If Session("Exclusion") = "1" Then
                        Session("MesesSeguroGratis") = 0
                    End If

                    'HM  20/02/2014 se agrega fecha de inicio vigencia
                    Session("IdCotiUnica") = ccalculo.inserta_cotizacion_unica(Session("idcotizapanel12"), _
                            Session("idcotizapanel"), Session("idgrdcontrato"), _
                            Session("bid"), idaonh, strmodelo, stranio, dbSA, _
                            idplazo, plazorestante, idmarca, Session("programa"), _
                            Session("Exclusion"), strpersona, strestatus, StrFecIni, _
                            Session("modeloc"), Session("anioc"), CDbl(Session("MontoCarta")), CDbl(Session("Diferencia")), _
                            Session("Cuota"), Session("MesesTranscurridos"), Session("MesesSeguroGratis"), _
                            Session("vehiculoC"), Session("idcotizapanel12C"), Session("CP"))

                    lbl_cotizacion.Text = "No. cotizaci�n :  " & Session("IdCotiUnica")
                    ConsultaInfocliente()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub limpia()
        txt_cliente.Text = ""
        'txt_grupo.Text = ""
        txt_serie.Text = ""
        txt_observaciones.Text = ""
    End Sub
    Private Sub ConsultaInfocliente()
        Try
            Dim dt As DataTable = ccliente.CargaDatosClienteCotiza(Session("IdCotiUnica"))
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("DatosCliente") Then
                    txt_cliente.Text = dt.Rows(0)("DatosCliente")
                End If
                'If Not dt.Rows(0).IsNull("GrupoIntegrante") Then
                '    txt_grupo.Text = dt.Rows(0)("GrupoIntegrante")
                'End If
                If Not dt.Rows(0).IsNull("NoSerie") Then
                    txt_serie.Text = dt.Rows(0)("NoSerie")
                End If
                If Not dt.Rows(0).IsNull("Observaciones") Then
                    txt_observaciones.Text = dt.Rows(0)("Observaciones")
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage("No existe informaci�n del cliente")
        End Try
    End Sub

    Public Sub carga_impresion()
        Dim tbrow As TableRow
        Dim tbcell As TableCell
        Dim hp As HyperLink
        Dim hp1 As HyperLink
        Try
            Dim dt As DataTable = ccliente.UrlCotizacion(Session("IdCotiUnica"))
            If dt.Rows.Count > 0 Then
                tbcell = New TableCell
                tbrow = New TableRow
                hp = New HyperLink
                hp1 = New HyperLink

                hp.Text = "Imprimir Cotizaci�n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                hp.NavigateUrl = "WfrmImpCotiza.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                hp.Target = "contenido"
                tbcell.Controls.Add(hp)

                hp1.Text = "Imprimir Cotizaci�n"
                hp1.ImageUrl = "..\imagenes\general\acrobat.png"
                hp1.NavigateUrl = "WfrmImpCotiza.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                hp1.Target = "contenido"
                tbcell.Controls.Add(hp1)

                tbcell.Width = Unit.Percentage(100)
                tbrow.Controls.Add(tbcell)
                tb_imp.Controls.Add(tbrow)
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_guardar.Click
        txt_cliente.Text = cvalida.QuitaCaracteres(txt_cliente.Text, True)
        'txt_grupo.Text = cvalida.QuitaCaracteres(txt_grupo.Text, True)
        txt_serie.Text = cvalida.QuitaCaracteres(txt_serie.Text, True)
        txt_observaciones.Text = Session("nombre") & " : " & cvalida.QuitaCaracteres(Replace(txt_observaciones.Text, Session("nombre") & " : ", ""), True)
        'If txt_grupo.Text.Length <> 7 Then
        '    MsgBox.ShowMessage("El campo Grupo - Integrante es obligatorio a 7 d�gitos")
        '    Exit Sub
        'End If
        ccliente.ActualizaCotizacionCliente(txt_cliente.Text, txt_serie.Text, txt_observaciones.Text, Session("IdCotiUnica"))
        Dim iIdCotiza As Integer = Session("IdCotiUnica")
        Dim _reportePDF As New CC.Negocio.Reporte.N_Cotizacion()
        _reportePDF.GeneraPromoMazda(iIdCotiza)

        carga_impresion()
        'solo en mi pc local
        'Enviacorreo()

    End Sub

    Private Sub Enviacorreo()
        Dim Msubject As String = ""
        Dim Mbody As String = ""
        Dim Mailpara As String = Session("CorreoAdministrador")
        Dim Mailcc As String = ""
        Dim Mailbcc As String = ""
        Dim strFrom As String = "Emisor de P�lizas Conauto mx.emisorconauto@aon.com"
        Dim objMail = New WsCorreo.Service1SoapClient()

        Try

            Msubject = "Alerta de registro de cotizacion"

            Mbody = " Se le notifica que se dio de alta una cotizaci�n de Conauto con los siguientes datos, "
            Mbody = Mbody & " <br> la cual esta pendiente de su confirmaci�n : "
            Mbody = Mbody & " <br> "
            Mbody = Mbody & " <br> Agencia : <b>" & Session("NombreDistribuidor") & "</b>"
            Mbody = Mbody & " <br> "
            Mbody = Mbody & " <br> No. Cotizaci�n : <b>" & Format(Session("IdCotiUnica"), "00000") & "</b>"
            Mbody = Mbody & " <br>"
            Mbody = Mbody & " <br> Favor de no responder a este correo, si tiene dudas o comentarios "
            Mbody = Mbody & " <br> correspondientes a los datos de su usuario, favor de consultar "
            Mbody = Mbody & " <br> a su administrador"
            Mbody = Mbody & " <br> Contacto Email : mailto:" & Session("CorreoAdministrador")
            Mbody = Mbody & " <br> Visita: www.aon.com"
            Mbody = Mbody & " <br> Aon..."
            Mbody = Mbody & " <br> La seguridad de un gran respaldo"
            Mbody = Mbody & " <br>"

            objMail.MailAYB(strFrom, Mailpara, Msubject, Mbody, True, Nothing, "o9its725", "emisorconauto", Mailbcc, Mailcc, "AYB")
            'InvocaEmail("aon2302", strFrom, Mailpara, "", "", Msubject, Mbody, "", 5)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Protected Sub txt_grupo_TextChanged(sender As Object, e As EventArgs) Handles txt_grupo.TextChanged

    'End Sub
End Class
