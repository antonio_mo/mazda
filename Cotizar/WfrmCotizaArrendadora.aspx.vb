﻿Imports CN_Negocios

Public Class WfrmCotizaArrendadora
    Inherits System.Web.UI.Page

#Region " Código generado por el Diseñador de Web Forms "

    'El Diseñador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Diseñador de Web Forms necesita la siguiente declaración del marcador de posición.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Diseñador de Web Forms requiere esta llamada de método
        'No la modifique con el editor de código.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalida As New CnValidacion
    Private ccalculo As New CnCalculo
    Private PaqSel As String = ""
    Private CdFraccion As CP_FRACCIONES.clsEmision

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aquí el código de usuario para inicializar la página
        Session("ClienteCotiza") = 0
        Session("idpoliza") = 0
        Dim NumeroArr As New ArrayList
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim tipo As Integer = 0

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                Session("FechaIni") = IIf(ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0", DateTime.Parse(Date.Now).ToString("dd/MM/yyyy"), DateTime.Parse(Date.Now).ToString("MM/dd/yyyy"))
                'If Session("banderafecha") = "0" Then
                'Session("FechaIni") = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                'Else
                ' Session("FechaIni") = acompleta_Cero(Date.Now.Month) & "/" & acompleta_Cero(Date.Now.Day) & "/" & Date.Now.Year
                ' End If

                Session("fincon") = "F"
                Session("ClienteCotiza") = 0
                Session("cotiza") = 0
                carga_marca()
                'forma pago daños
                Dim banSeg As Integer
                If Session("seguro") = 0 Then
                    banSeg = 1
                Else
                    banSeg = 0
                End If
                rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))

                'Verifica si es distribuidor no despliega los planes anuales
                If Session("Nivel") = 0 Then
                    'NumeroArr.Add("Seguro Gratis 24 y 36")
                    'NumeroArr.Add("Seguro Gratis 48 y 60")
                    'NumeroArr.Add("Plazo 24 y 36")
                    'NumeroArr.Add("Plazo 48 y 60")
                    'NumeroArr.Add("Plan anual 24 y 36")
                    'NumeroArr.Add("Plan anual 48 y 60")
                    NumeroArr.Add("Multianual")
                Else
                    'Distribuidor
                    'NumeroArr.Add("Seguro Gratis 24 y 36")
                    'NumeroArr.Add("Seguro Gratis 48 y 60")
                    'NumeroArr.Add("Plazo 24 y 36")
                    'NumeroArr.Add("Plazo 48 y 60")
                    NumeroArr.Add("Multianual")
                End If
                rb_promocion.DataSource = NumeroArr
                rb_promocion.DataBind()


                For i = 0 To rb_promocion.Items.Count - 1
                    If rb_promocion.Items(i).Text = "Multianual" Then
                        rb_promocion.Items(i).Selected = True
                        'tipo = NumeroArr(rb_promocion.SelectedIndex)
                        Exit For
                    End If
                Next

                Seg_financiado(rb_seguro)

                If Not Request.QueryString("cveAseg") Is Nothing Then
                    Dim BanRecalculo12meses As Integer = 0
                    Dim BanRecalculoMultianual As Integer = 0

                    'nuevo
                    tipo_poliza(Request.QueryString("cveplazo"))

                    Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")

                    If Session("bandOpcion") = 0 Then 'Seguro Gratis
                        Dim IdAseg12 As Integer = Request.QueryString("Aseg12meses")
                        Dim IdAsegMulti As Integer = Request.QueryString("cveAseg")

                        If IdAseg12 = IdAsegMulti Then 'ASeguradoras iguales
                            'Qualitas o Axa igual no hace recalculo
                            BanRecalculo12meses = 0
                            BanRecalculoMultianual = 0
                            Recalculoaseguradora12meses(0, 0)
                        Else
                            If IdAseg12 = 62 Then ' Qualitas 12 y Axa resto
                                'Qualitas 12 sigue igual
                                'Axa multianual hace recalculo
                                BanRecalculo12meses = 0
                                BanRecalculoMultianual = 1
                                'RecalculaValor_ASeguradora(1) 'aseguradora AXA multianual (1 = nuevos paquetes >=13, 0 = paquetes iniciales <=12)
                            Else
                                'Axa 12 y Qualitas resto , RecalculaValor() aseguradora AXA 12, RecalculaValor() aseguradora Qualitas multianual
                                'Axa 12 meses recalcula
                                BanRecalculo12meses = 1
                                'Recalculoaseguradora12meses(1)
                                'Qualitas multianual hace recalculo
                                BanRecalculoMultianual = 1
                                'RecalculaValor_ASeguradora(1) 'aseguradora AXA multianual (1 = nuevos paquetes >=13, 0 = paquetes iniciales <=12)
                            End If
                        End If
                    End If

                    If BanRecalculoMultianual = 0 Then
                        Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                               Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
                               1, _
                               Session("tipopol"), _
                               Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                               Session("EstatusV"), _
                               0, Session("InterestRate"), Session("FechaIni"), _
                               Session("tiposeguro"), Request.QueryString("cveplazo"), _
                               Request.QueryString("valcost"), _
                               Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                               0, Session("vehiculo"), Request.QueryString("cveAseg"), _
                               Request.QueryString("cvepaquete"), Session("programa"), _
                               Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                               Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
                               Request.QueryString("TipoCarga"), 0, Session("TipoProducto"), _
                               Request.QueryString("Aseg12meses"))
                        'fin cambio micha

                        Session("subsidios") = ccalculo.subsidios
                        Session("ArrPoliza") = ccalculo.ArrPolisa
                        Session("resultado") = ccalculo.Results
                        Session("MultArrPolisa") = ccalculo.MultArrPolisa

                        Session("DatosGenerales") = ""
                        Dim arr(25) As String
                        arr(0) = Session("moneda")
                        arr(1) = Request.QueryString("subramo")
                        arr(2) = Session("anio")
                        arr(3) = Session("modelo")
                        arr(4) = 1
                        'arr(5) = Seg_financiado(rb_seguro)
                        arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                        arr(6) = Session("uso")
                        arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                        arr(8) = Session("EstatusV")
                        arr(9) = 0
                        arr(10) = Session("InterestRate")
                        arr(11) = Session("FechaIni")
                        arr(12) = Session("tiposeguro")
                        arr(13) = Request.QueryString("cveplazo")
                        arr(14) = txt_precio.Text
                        arr(15) = Session("idgrdcontrato")
                        arr(16) = Session("seguro")
                        arr(17) = Session("bid")
                        arr(18) = 0
                        arr(19) = Session("vehiculo")
                        arr(20) = Request.QueryString("cveAseg")
                        arr(21) = Request.QueryString("cvepaquete")
                        arr(22) = Session("programa")
                        arr(23) = Session("fincon")
                        arr(24) = Request.QueryString("Pago1")
                        arr(25) = Request.QueryString("Pago2")
                        Session("DatosGenerales") = arr

                        'manolito

                        Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                        Session("plazoRecargo") = Request.QueryString("NumPago")

                        Session("region") = Request.QueryString("cveregion")
                        Session("aseguradora") = Request.QueryString("cveAseg")
                        Session("plazo") = Request.QueryString("cveplazo")
                        Session("FechaInicio") = Session("FechaIni")
                        Session("contrato") = Request.QueryString("cvecontrato")
                        Session("precioEscrito") = Request.QueryString("valcost")
                        Session("paquete") = Request.QueryString("cvepaquete")
                        Session("Tipocarga") = Request.QueryString("TipoCarga")

                        Session("EstatusV") = "N"
                    Else
                        RecalculaValor_ASeguradora(1)
                    End If

                    If BanRecalculo12meses = 0 Then
                        calculoaseguradora12meses(Request.QueryString("valcost"), Request.QueryString("TipoCarga"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), Request.QueryString("cveregion"), Request.QueryString("subramo"))
                    Else
                        Recalculoaseguradora12meses(1)
                    End If

                    Dim xs1() As String = Session("ArrPoliza")
                    Dim xs() As String = Session("ArrPoliza1")

                    Response.Redirect("WfrmCliente.aspx")

                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Public Sub calculoaseguradora12meses(ByVal valcost As Double, ByVal TipoCarga As Integer, _
    ByVal strcatalogo As String, ByVal IdRegion As Integer, ByVal subramo As String)

        Try

            Dim dt As DataTable = ccalculo.Promocion12meses(Session("idcotizapanel12"))
            If dt.Rows.Count > 0 Then

                Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                    dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                    1, "A", _
                    Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                    Session("EstatusV"), _
                    0, Session("InterestRate"), Session("FechaIni"), _
                    Session("tiposeguro"), 12, _
                    valcost, _
                    Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                    0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                    dt.Rows(0)("idpaquete"), Session("programa"), _
                    Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                    dt.Rows(0)("id_tipoRecargo"), 1, TipoCarga, _
                    IIf(Session("Exclusion") = 0, 1, 0), , , , 1)
                'fin cambio micha

                Session("subsidios1") = ccalculo.subsidios12
                Session("ArrPoliza1") = ccalculo.ArrPolisa12
                Session("resultado1") = ccalculo.Results12
                Session("MultArrPolisa1") = ccalculo.MultArrPolisa12

                Session("DatosGenerales1") = ""
                Dim arr(25) As String
                arr(0) = Session("moneda")
                arr(1) = subramo
                arr(2) = Session("anio")
                arr(3) = Session("modelo")
                arr(4) = 1
                'arr(5) = Seg_financiado(rb_seguro)
                arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                arr(6) = Session("uso")
                arr(7) = strcatalogo
                arr(8) = Session("EstatusV")
                arr(9) = 0
                arr(10) = Session("InterestRate")
                arr(11) = Session("FechaIni")
                arr(12) = Session("tiposeguro")
                arr(13) = 12
                arr(14) = txt_precio.Text
                arr(15) = Session("idgrdcontrato")
                arr(16) = Session("seguro")
                arr(17) = Session("bid")
                arr(18) = 0
                arr(19) = Session("vehiculo")
                arr(20) = dt.Rows(0)("Id_aseguradora")
                arr(21) = dt.Rows(0)("idpaquete")
                arr(22) = Session("programa")
                arr(23) = Session("fincon")
                arr(24) = dt.Rows(0)("primatotal")
                arr(25) = dt.Rows(0)("primaconsecutiva")
                Session("DatosGenerales1") = arr

                'manolito
                Session("plazo1") = 12
                Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
                Session("plazoRecargo1") = 1
                Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
                Session("paquete1") = dt.Rows(0)("idpaquete")
                Session("subramo1") = subramo

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Public Function estatus_auto() As String
        Return "N"
    End Function

    Public Sub carga_marca()
        Dim valor As String
        valor = estatus_auto()
        Dim dt As DataTable = ccliente.carga_marca(Session("intmarcabid"), Session("programa"), valor)
        Try
            If dt.Rows.Count > 0 Then
                cbo_marca.DataSource = dt
                cbo_marca.DataTextField = "marca"
                cbo_marca.DataValueField = "id_marca"
                cbo_marca.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cbo_carga_marca(ByVal intmcarca As Integer)
        Dim valor As String
        valor = estatus_auto()
        carga_modelo(intmcarca, valor)
    End Sub

    Public Sub carga_modelo(ByVal intmarca As Integer, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_modelo(intmarca, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_modelo.DataSource = dt
                cbo_modelo.DataTextField = "modelo_vehiculo"
                cbo_modelo.DataValueField = "modelo_vehiculo"
                cbo_modelo.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cbo_carga_modelo(ByVal intmarca As Integer, ByVal intmodelo As String)
        Dim valor As String
        valor = estatus_auto()
        carga_anio(intmarca, intmodelo, valor)
    End Sub

    Public Sub carga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_anio(intmarca, strmodelo, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_anio.DataSource = dt
                cbo_anio.DataTextField = "anio_vehiculo"
                cbo_anio.DataValueField = "anio_vehiculo"
                cbo_anio.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub cbo_carga_descripcion()
        Dim valor As String
        valor = estatus_auto()
        carga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, valor)
    End Sub

    Public Sub carga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_descripcion(intmarca, strmodelo, stranio, strtipoa, Session("programa"), Session("intmarcabid"))
        Try
            If dt.Rows.Count > 0 Then
                cbo_descripcion.DataSource = dt
                cbo_descripcion.DataTextField = "descripcion_vehiculo"
                cbo_descripcion.DataValueField = "id_vehiculo"
                cbo_descripcion.DataBind()
                If dt.Rows.Count > 1 Then
                    cbo_descripcion.SelectedIndex = 1
                    datos_descripcion(cbo_descripcion.SelectedValue)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub datos_descripcion(ByVal intdescripcion As Integer)
        Dim valor As String
        Try
            Dim dth As DataTable = ccliente.carga_subramo_homologado(intdescripcion, estatus_auto(), Session("programa"), cbo_marca.SelectedValue, Session("intmarcabid"))
            If dth.Rows.Count > 0 Then
                viewstate("subramo") = ccliente.subramo
                viewstate("catalogo") = ccliente.Catalogo
            Else
                viewstate("subramo") = 0
                viewstate("catalogo") = 0
            End If


            valor = estatus_auto()

            Dim dt As DataTable = ccliente.carga_uso(Session("programa"), cbo_descripcion.SelectedValue, valor, viewstate("subramo"), Session.SessionID)
            If dt.Rows.Count > 0 Then
                cbo_uso.DataSource = dt
                cbo_uso.DataTextField = "uso"
                cbo_uso.DataValueField = "id_uso"
                cbo_uso.DataBind()
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Particular"))
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub cbo_marca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_marca.SelectedIndexChanged
        If cbo_marca.SelectedValue = 0 Then
            MsgBox.ShowMessage("La marca no puede quedar en blanco")
        End If
        cbo_carga_marca(cbo_marca.SelectedValue)
    End Sub

    Private Sub cbo_modelo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_modelo.SelectedIndexChanged
        Try
            If cbo_modelo.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione el Tipo")
                Exit Sub
            End If
            'limpia_campos()
            If Session("BanderaMacro") = 0 Then
                cbo_carga_modelo(cbo_marca.SelectedValue, cbo_modelo.SelectedValue)
            Else
                cbo_carga_modelo(cbo_marca.SelectedValue, cbo_modelo.SelectedValue)
                cbo_anio.SelectedIndex = 1
                cbo_carga_descripcion()
                cbo_descripcion.SelectedIndex = 1
                datos_descripcion(cbo_descripcion.SelectedValue)
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Particular"))
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_anio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_anio.SelectedIndexChanged
        Try
            If cbo_anio.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione el año del vehiculo")
                Exit Sub
            End If
            cbo_carga_descripcion()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_descripcion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_descripcion.SelectedIndexChanged
        Try
            If cbo_descripcion.SelectedValue = 0 Then
                MsgBox.ShowMessage("Seleccione la descripción del vehiculo")
                Exit Sub
            End If
            datos_descripcion(cbo_descripcion.SelectedValue)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_cotizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cotizar.Click
        Dim i As Integer = 0
        Dim bandera As Integer = 0
        Try
            Session("TipoProducto") = "0"

            If cbo_marca.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar la Marca")
                Exit Sub
            End If
            If cbo_modelo.SelectedValue = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Vehiculo")
                Exit Sub
            End If
            If cbo_anio.SelectedValue = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el Año del Vehiculo ")
                Exit Sub
            End If
            If cbo_descripcion.SelectedValue = "0" Then
                MsgBox.ShowMessage("Favor de seleccionar la Descripción del Vehiculo")
                Exit Sub
            End If
            If cbo_uso.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Uso del Vehiculo")
                Exit Sub
            End If
            If txt_precio.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el monto del Vehiculo")
                Exit Sub
            End If
            If txt_precio.Text <= 0 Then
                MsgBox.ShowMessage("El monto del Vehiculo debe de ser mayo a 0 ")
                Exit Sub
            End If
            For i = 0 To rb_tipo.Items.Count - 1
                If rb_tipo.Items(i).Selected = True Then
                    bandera = 1
                    Exit For
                End If
            Next
            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el Tipo de Contribuyente")
                Exit Sub
            End If

            Session("vehiculo") = cbo_descripcion.SelectedValue
            Session("vehiculoDesc") = cbo_descripcion.SelectedItem.Text
            Session("subramo") = viewstate("subramo")
            Session("anio") = cbo_anio.SelectedItem.Text
            Session("modelo") = cbo_modelo.SelectedValue
            Session("uso") = cbo_uso.SelectedItem.Text

            Dim dt As DataTable = ccliente.Carga_definicion_uso(Session("programa"), viewstate("subramo"), cvalida.QuitaCaracteres(cbo_uso.SelectedItem.Text, True), Session("version"))
            If dt.Rows.Count > 0 Then
                Session("intuso") = dt.Rows(0)("id_uso")
            Else
                Session("intuso") = cbo_uso.SelectedValue
            End If

            Session("catalogo") = IIf(viewstate("catalogo") = "", "0", viewstate("catalogo"))
            Session("EstatusV") = estatus_auto()
            Session("MarcaC") = cbo_marca.SelectedValue
            Session("MarcaDesc") = cbo_marca.SelectedItem.Text
            Session("contribuyente") = personalidad(rb_tipo)
            Session("iva") = 0.16
            Session("precioEscrito") = txt_precio.Text


            Session("Exclusion") = TipoPrograma(rb_promocion)
            Session("opcion") = ""
            Session("bandOpcion") = ""
            Session("TipoSeguro") = 0

            If Session("Exclusion") = 0 Then
                Session("opcion") = "12,24"
                Session("bandOpcion") = 0
                Session("TipoSeguro") = 0
            ElseIf Session("Exclusion") = 1 Then
                Session("opcion") = "36,48"
                Session("Exclusion") = 0
                Session("bandOpcion") = 0
                Session("TipoSeguro") = 1
            ElseIf Session("Exclusion") = 2 Then
                Session("opcion") = "24,36"
                Session("Exclusion") = 1
                Session("bandOpcion") = 1
                Session("TipoSeguro") = 2
            ElseIf Session("Exclusion") = 3 Then
                Session("opcion") = "48,60"
                Session("Exclusion") = 1
                Session("bandOpcion") = 1
                Session("TipoSeguro") = 3
            ElseIf Session("Exclusion") = 4 Then
                'Session("opcion") = "24,36"
                Session("opcion") = "12"
                Session("Exclusion") = 1
                Session("bandOpcion") = 2
                Session("TipoSeguro") = 4
            ElseIf Session("Exclusion") = 5 Then
                'Session("opcion") = "48,60"
                Session("opcion") = "12,24,36,48,60"
                Session("Exclusion") = 1
                Session("bandOpcion") = 2
                Session("TipoSeguro") = 5
            End If

            carga_plazo()

            Session("idgrdcontrato") = ccliente.carga_contrato(Session("bid"))

            '''validando promociones
            ''If ccliente.ValidaExclusion(Session("programa"), Session("modelo"), Session("anio"), Session("EstatusV")).Rows.Count > 0 Then
            ''    Session("Exclusion") = 1
            ''Else
            ''    Session("Exclusion") = 0
            ''End If
            'Session("Exclusion") = TipoPrograma(rb_promocion)

            validaSeguro12()
            If Not Session("CadenaCob") Is Nothing Then
                dt = Nothing
                dt = ccliente.inserta_coberturas( _
                         Session("CadenaCob"), Session("idgrdcontrato"), Session("bid"), Session("programa"), _
                         Session("vehiculo"), Session("EstatusV"), Session("anio"))
                If dt.Rows.Count > 0 Then

                End If
            End If


            'evi 27/06/2013 se agrega la opcion de anual
            cbo_plazo.Enabled = True
            If Session("Exclusion") = 6 Then
                cbo_plazo_SelectedIndexChanged(sender, e)
                If Session("Exclusion") = 6 Then
                    cbo_plazo.Enabled = False
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function TipoPrograma(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    If rb.Items(i).Text = "Seguro Gratis 24 y 36" Then
                        valor = 0
                    ElseIf rb.Items(i).Text = "Seguro Gratis 48 y 60" Then
                        valor = 1
                    ElseIf rb.Items(i).Text = "Plazo 24 y 36" Then
                        valor = 2
                    ElseIf rb.Items(i).Text = "Plazo 48 y 60" Then
                        valor = 3
                    ElseIf rb.Items(i).Text = "Plan anual 24 y 36" Then
                        valor = 4
                    ElseIf rb.Items(i).Text = "Plan anual 48 y 60" Then
                        valor = 5
                    ElseIf rb.Items(i).Text = "Multianual" Then
                        valor = 5
                    End If
                    'valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub validaSeguro12()
        Ajuste_plazo_calculo(12, 1)
        calculo_general(12, 0, IIf(Session("Exclusion") = 0, 1, 0))
        crearPrima12meses()
        'If Session("Exclusion") = 0 Then
        '    cbo_plazo.SelectedIndex = 0
        'Else
        '    cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
        'End If
    End Sub

    Public Function calculo_general(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal BanderaPromocion As Integer) As String
        Dim arr() As String
        Dim Arr1() As String
        Dim i As Integer = 0
        Dim strcadena As String = ""
        Try
            strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                        Session("anio"), _
                        Session("modelo"), _
                        1, Session("tipopol"), _
                        Session("uso"), Session("EstatusV"), _
                        0, Session("InterestRate"), Session("FechaIni"), _
                        Session("tiposeguro"), NumPlazo, txt_precio.Text, _
                        Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                        IntSubCob, Session("vehiculo"), Session("programa"), _
                       Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                       Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                       Session("Tipocarga"), Session("TipoProducto"), BanderaPromocion)
            'Session("Tipocarga"), Session("Exclusion"), BanderaPromocion)



            If strcadena = "" Then
                MsgBox.ShowMessage("No existe información del auto a cotizar, favor de consultar al administrador")
                Exit Function
            Else
                Session("CadenaCob") = strcadena
                'arr = strcadena.Split("|")
                'For i = 0 To arr.Length - 1
                '    Arr1 = arr(i).Split("*")
                '    If i = 0 Then
                '        PaqSel = Arr1(1)
                '    Else
                '        PaqSel = PaqSel & "," & Arr1(1)
                '    End If
                'Next
                'Session("PaqSelCon") = PaqSel

                'Session("Calculos") = ccalculo.CalculoArr
                'Session("subsidios") = ccalculo.subsidios
                'Session("ArrPoliza") = ccalculo.ArrPolisa
                'Session("resultado") = ccalculo.Results
                'Session("MultArrPolisa") = ccalculo.MultArrPolisa

                Session("idcotizapanel12") = ccalculo.Inst_Cotizacion_panel_12meses(strcadena, "0")

            End If
            Return strcadena
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub Ajuste_plazo_calculo(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, Optional ByVal BanActualiza As Integer = 0)
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select

        ComplementoAjuste(IntCboTipo, intPlazofinal, IntPagos)

        If BanActualiza = 1 Then
            ccalculo.actualiza_numplazo_cob(intPlazofinal, Session("idgrdcontrato"), Session("bid"))
        End If
    End Sub

    Public Sub ComplementoAjuste(ByVal IntCboTipo As Integer, ByVal intPlazofinal As Integer, ByVal IntPagos As Integer)
        lbl_plazo.Text = intPlazofinal
        Session("plazoFinanciamiento") = IntCboTipo
        Session("plazoRecargo") = IntPagos
        Session("plazoSeleccion") = cbo_plazo.SelectedValue
        Session("plazo") = lbl_plazo.Text
        tipo_poliza(lbl_plazo.Text)
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                'Session("seguro") = 2
                Session("tipopol") = "M"
                'Session("bUnidadfinanciada") = True
            Else
                'Session("seguro") = 0
                Session("tipopol") = "A"
                'Session("bUnidadfinanciada") = False
            End If
        Else
            Session("tipopol") = "A"
            'Session("seguro") = 0
            'Session("bUnidadfinanciada") = False
        End If
    End Sub

    Public Sub carga_plazo()
        Dim dt As DataTable = ccalculo.carga_plazo(Session("programa"), Session("opcion"))
        If dt.Rows.Count > 0 Then
            cbo_plazo.DataSource = dt
            cbo_plazo.DataTextField = "plazo"
            cbo_plazo.DataValueField = "id_plazo"
            cbo_plazo.DataBind()
        End If
    End Sub

    Public Sub crearPrima12meses()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim arr() As String
        Dim i, j As Integer
        Dim BanMax As Double = 0
        Dim dbMonto As Double = 0
        Dim strcadena As String = ""

        tb_seguro.Controls.Clear()
        Try
            If Not Session("CadenaCob") = "" Then
                arr = Split(Session("CadenaCob"), "|")
                arr.Sort(arr)
                BanMax = VerificaMaximno(arr)
                encabenzado12meses()
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arr12meses() As String = arr(i).Split("*")
                        ccliente.DatosAseguradora(arr12meses(0))
                        tbrow = New TableRow

                        dbMonto = CDbl(arr12meses(2))
                        If BanMax = dbMonto Then
                            'aseguradora mas barata
                            Session("12meses") = arr12meses(0)
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                            ccalculo.Upd_Cotizacion_panel_12meses(Session("idcotizapanel12"), arr12meses(0), "E")
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If

                        strcadena = ccliente.DescrAseg
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        If Session("Nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        tb_seguro.Controls.Add(tbrow)
                    End If
                Next
            End If

            If Session("Exclusion") = 0 Then
                tb_seguro.Visible = True
            Else
                tb_seguro.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub encabenzado12meses()
        Dim tbcell As TableCell
        Dim tbrow As New TableRow

        tbcell = New TableCell
        tbcell.Text = "Seguro gratis un año"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(100)
        tbcell.ColumnSpan = 3
        tbrow.Controls.Add(tbcell)
        tb_seguro.Controls.Add(tbrow)
    End Sub

    Public Function agrega_celdas(ByVal strcadena As String, ByVal strclase As String, ByVal intalinea As Integer, _
    ByVal tipoFormato As String, ByVal tamano As Double, Optional ByVal Comentario As String = "") As TableCell
        'intalinea = 
        '1 = izquierda
        '2 = centro
        '3 = derecha
        Dim tbcell As New TableCell
        Dim hp As HyperLink
        Dim Himage As HtmlImage

        Select Case tipoFormato
            Case "G"
                tbcell.Text = strcadena
            Case "I"
                Himage = New HtmlImage
                Himage.Src = strcadena
                tbcell.Controls.Add(Himage)
            Case "H"
                'hp = New HyperLink
                'hp.Text = Comentario
                'hp.NavigateUrl = strcadena
                'tbcell.Controls.Add(hp)
        End Select
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.CssClass = strclase
        tbcell.HorizontalAlign = intalinea
        tbcell.Width = Unit.Percentage(tamano)
        Return tbcell
    End Function

    Public Function VerificaMaximno(ByVal arr As Object) As Double
        Dim i As Integer = 0
        Dim ValorMaximo As Double = 0
        Dim ValorMaximoAux As Double = 0

        For i = 0 To arr.Length - 1
            Dim arrvalida() As String = arr(i).Split("*")
            If arrvalida(2) <> "" Then
                ValorMaximoAux = CDbl(arrvalida(2))
            End If
            If ValorMaximoAux < ValorMaximo Then
                ValorMaximo = ValorMaximoAux
            ElseIf i = 0 Then
                ValorMaximo = ValorMaximoAux
            End If
        Next
        Return ValorMaximo
    End Function

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Private Sub cbo_plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_plazo.SelectedIndexChanged
        Dim i As Integer = 0
        If cbo_plazo.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione el Plazo de seguro")
            Exit Sub
        End If

        Session("TipoProducto") = ""
        If Session("bandOpcion") = 0 Then
            If cbo_plazo.SelectedValue = 12 Then
                Session("TipoProducto") = "0"
            ElseIf cbo_plazo.SelectedValue = 24 Then
                Session("TipoProducto") = "1"
            ElseIf cbo_plazo.SelectedValue = 36 Then
                Session("TipoProducto") = "2"
            ElseIf cbo_plazo.SelectedValue = 48 Then
                Session("TipoProducto") = "3"
            End If
        ElseIf Session("bandOpcion") = 1 Then
            If cbo_plazo.SelectedValue = 24 Or cbo_plazo.SelectedValue = 36 Then
                Session("TipoProducto") = "4"
            ElseIf cbo_plazo.SelectedValue = 48 Or cbo_plazo.SelectedValue = 60 Then
                Session("TipoProducto") = "5"
            End If
        ElseIf Session("bandOpcion") = 2 Then
            If Session("TipoSeguro") = 4 Then
                Session("TipoProducto") = "6"
            Else
                Session("TipoProducto") = "7"
            End If
        End If

        If Session("TipoProducto") = "6" Or Session("TipoProducto") = "7" Then
            Session("plazoSeleccion") = "12"
        Else
            Session("plazoSeleccion") = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
        End If


        Seg_financiado(rb_seguro)

        Ajuste_plazo_calculo(12, 1)
        calculo_general(12, 0, IIf(Session("Exclusion") = 0, 1, 0))
        crearPrima12meses()

        calculo_general_aseguradora()

        'lbl_plazo.Text = cbo_plazo.SelectedValue + 12
        lbl_plazo.Text = cbo_plazo.SelectedValue


        '''Dim dt As DataTable = ccliente.Carga_tipoPago()
        '''If dt.Rows.Count > 1 Then
        '''    Session("CadenaCob") = ""
        '''    For i = 1 To dt.Rows.Count - 1
        '''        Ajuste_plazo_calculo(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"))
        '''        calculo_general_aseguradora(cbo_plazo.SelectedValue, 0, dt.Rows(i)("id_tiporecargo"))
        '''    Next
        '''End If
    End Sub

    'Public Function calculo_general_aseguradora(ByVal NumPlazo As Integer, ByVal IntSubCob As Double, ByVal Tiporecargo As Integer) As String
    Public Sub calculo_general_aseguradora()
        Dim arr() As String
        Dim Arr1() As String
        Dim i, j, k, x As Integer
        Dim strcadena As String = ""
        Dim strCadenaCal As String = ""
        Dim FechaTermina As String = ""
        Dim ds As DataSet

        Dim NumPlazo As Integer
        Dim IntSubCob As Integer = 0
        Dim Tiporecargo As Integer = 0
        Dim IdRecargo As Integer = 0
        Dim strInserta As String = ""
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0

        Dim ArrAseg(4, 5) As String

        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Try

            Dim dt As DataTable = ccliente.Carga_tipoPago(Session("programa"))
            If dt.Rows.Count > 1 Then
                'NumPlazo = cbo_plazo.SelectedValue
                'NumPlazo = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                NumPlazo = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)

                IntSubCob = 0
                For k = 1 To dt.Rows.Count - 1

                    'Ajuste_plazo_calculo(cbo_plazo.SelectedValue, dt.Rows(k)("id_tiporecargo"))
                    'Ajuste_plazo_calculo(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), dt.Rows(k)("id_tiporecargo"))
                    Ajuste_plazo_calculo(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), dt.Rows(k)("id_tiporecargo"))
                    IdRecargo = dt.Rows(k)("id_tiporecargo")

                    'Session("uso"), Session("EstatusV"), _
                    strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                                Session("anio"), Session("modelo"), _
                                1, Session("tipopol"), _
                                Session("uso"), IIf(Session("Exclusion") = 0, "U", "N"), _
                                0, Session("InterestRate"), Session("FechaIni"), _
                                Session("tiposeguro"), NumPlazo, txt_precio.Text, _
                                Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                                IntSubCob, Session("vehiculo"), Session("programa"), _
                            Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                            Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                            Session("Tipocarga"), Session("TipoProducto"), 0, Session("12meses"))
                    'Session("Tipocarga"), Session("Exclusion"), 0)

                    If strcadena = "" Then
                        MsgBox.ShowMessage("No existe información del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    Else

                        strCadenaCal = ccalculo.StrSalidaPanle

                        Select Case IdRecargo
                            Case 2 'Anual
                                Tiporecargo = 1
                            Case 3 'Semestral
                                Tiporecargo = 4
                            Case 4 'Trimestral
                                Tiporecargo = 3
                            Case 5 'Mensual
                                Tiporecargo = 2
                            Case Else
                                Tiporecargo = IdRecargo
                        End Select

                        'FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), cbo_plazo.SelectedValue)
                        'FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue))
                        If IdRecargo = 1 Then
                            FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), IIf(Session("Exclusion") = 0, 12, 12))
                        Else
                            FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue))
                        End If

                        Dim FechaInicio As String
                        Dim arrFinicio() As String
                        arrFinicio = Split(Session("FechaIni"), "/")
                        If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "0" Then
                            FechaInicio = arrFinicio(0) & "/" & arrFinicio(1) & "/" & arrFinicio(2)
                        Else
                            FechaInicio = arrFinicio(1) & "/" & arrFinicio(0) & "/" & arrFinicio(2)
                        End If

                        Dim Results() As String
                        Dim Results1() As String

                        arr = strcadena.Split("|")
                        Results = strCadenaCal.Split("|")

                        For i = 0 To arr.Length - 1
                            Arr1 = arr(i).Split("*")
                            Results1 = Results(i).Split("*")

                            If Arr1(0) = Results1(0) Then
                                ds = Nothing
                                CdFraccion = New CP_FRACCIONES.clsEmision
                                Try

                                    Select Case CInt(Arr1(0))
                                        Case 64
                                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                        Case 65
                                            ds = CdFraccion.FraccionesAXA_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                    End Select

                                Catch ex As Exception


                                    Dim arrfi() As String
                                    arrfi = Split(Session("FechaIni"), "/")
                                    If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "1" Then
                                        FechaTermina = arrfi(0) & "/" & arrfi(1) & "/" & arrfi(2)
                                    Else
                                        FechaTermina = arrfi(1) & "/" & arrfi(0) & "/" & arrfi(2)
                                    End If

                                    Select Case CInt(Arr1(0))
                                        Case 64
                                            ds = CdFraccion.FraccionesQualitas_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                        Case 65
                                            ds = CdFraccion.FraccionesAXA_Conauto(FechaInicio, _
                                                FechaTermina, _
                                                Results1(1), Results1(2), _
                                                Results1(3), Results1(4), _
                                                Results1(5), Tiporecargo, Results1(6), _
                                                (Results1(7) * 100))
                                    End Select
                                End Try

                                If ds.Tables(0).Rows.Count > 0 Then
                                    For x = 0 To ds.Tables(0).Rows.Count - 1
                                        Select Case x
                                            Case 0
                                                Dbtotales1 = ds.Tables(0).Rows(x)("prima_total")
                                            Case 1
                                                Dbtotales2 = ds.Tables(0).Rows(x)("prima_total")
                                                Exit For
                                        End Select
                                    Next
                                End If

                                If strInserta = "" Then
                                    strInserta = Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                Else
                                    strInserta = strInserta & "|" & Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                                End If
                            End If
                        Next
                    End If
                Next
                If Not strInserta = "" Then
                    Session("idcotizapanel") = ccalculo.Inst_Cotizacion_panel(strInserta, Nothing)
                End If
            End If

            dt = ccalculo.CargaCtoizaPanelAseguradora(Session("idcotizapanel"))
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    ccliente.DatosAseguradora(dt.Rows(i)("id_aseguradora"))

                    Dim dtV As DataTable = ccalculo.CargaCtoizaPanel(Session("idcotizapanel"), dt.Rows(i)("id_aseguradora"))
                    If dtV.Rows.Count > 0 Then
                        For j = 0 To dtV.Rows.Count - 1
                            If Not dtV.Rows(j).IsNull("Id_TipoRecargo") Then
                                ArrAseg(j, 0) = dtV.Rows(j)("Id_TipoRecargo")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaTotal") Then
                                ArrAseg(j, 1) = dtV.Rows(j)("PrimaTotal")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaConsecutiva") Then
                                ArrAseg(j, 2) = dtV.Rows(j)("PrimaConsecutiva")
                            End If
                            If Not dtV.Rows(j).IsNull("subramo") Then
                                ArrAseg(j, 3) = dtV.Rows(j)("subramo")
                            End If
                            If Not dtV.Rows(j).IsNull("IdPaquete") Then
                                ArrAseg(j, 4) = dtV.Rows(j)("IdPaquete")
                            End If
                            If Not dtV.Rows(j).IsNull("PrimaConsecutiva2") Then
                                ArrAseg(j, 5) = dtV.Rows(j)("PrimaConsecutiva2")
                            End If
                        Next
                    End If

                    tbrow = New TableRow
                    tbcell = New TableCell
                    tbcell.ColumnSpan = 7
                    tbcell.Controls.Add(encabenzadoAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg))
                    tbcell.Controls.Add(DetalleAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg))
                    tbcell.Controls.Add(encabenzadoAseguradora_vacio())
                    tbrow.Controls.Add(tbcell)
                    tb_aseguradora.Controls.Add(tbrow)
                Next
            End If


        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function encabenzadoAseguradora_vacio() As Table
        Dim tb As New Table
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.ColumnSpan = 7
        tbcell.Text = "&nbsp;"
        tbrow.Controls.Add(tbcell)
        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function encabenzadoAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim cuentaTotal As Integer = 0
        Try
            tbrow = New TableRow
            tbcell = New TableCell
            tbcell.Text = "&nbsp;"
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            'tbcell.CssClass = ""
            tbcell.Width = Unit.Percentage(10)
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            tbcell.Text = ccliente.DescrAseg
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "Interiro_tabla_centro"
            tbcell.Width = Unit.Percentage(15)
            tbrow.Controls.Add(tbcell)

            'Dim dt As DataTable = ccalculo.CargaTipoRecargo()
            Dim dt As DataTable = ccalculo.CargaTipoRecargo_AutoOpcion(Session("idcotizapanel"), IdAseguradora)
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    tbcell = New TableCell

                    Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                    If dtRegion.Rows.Count > 0 Then
                        If Not dtRegion.Rows(0).IsNull("id_region") Then
                            IdRegion = dtRegion.Rows(0)("id_region")
                        End If
                    End If

                    'NumPagos = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"))
                    'NumPagos = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), dt.Rows(i)("id_tiporecargo"))
                    NumPagos = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), dt.Rows(i)("id_tiporecargo"))

                    If (Session("nivel") = 0 Or Session("nivel") = 1 Or Session("nivel") = 4) And Session("bandOpcion") = 0 Then
                        hp = New HyperLink
                        hp.ToolTip = "recalcular prima"
                        hp.ImageUrl = "..\Imagenes\Menu\sum.png"
                        ''''hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.SelectedValue & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        ''''hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue) & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        'hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue) & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & "," & Session("12meses") & ")")
                        hp.Attributes.Add("onclick", "ValidaPrima(" & Session("12meses") & "," & IdAseguradora & "," & IdAseguradora & "," & IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue) & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & "," & Session("12meses") & ")")
                        hp.Style.Add("cursor", "pointer")
                        tbcell.Controls.Add(hp)
                    End If

                    lbl = New Label
                    lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_periodo")
                    tbcell.Controls.Add(lbl)
                    'tbcell.Text = "&nbsp;" & dt.Rows(i)("descripcion_periodo")
                    tbcell.VerticalAlign = VerticalAlign.Middle
                    tbcell.HorizontalAlign = HorizontalAlign.Center
                    tbcell.CssClass = "Interiro_tabla_centro"
                    tbcell.Width = Unit.Percentage(15)
                    tbrow.Controls.Add(tbcell)
                    cuentaTotal = cuentaTotal + 1

                    If (Session("nivel") = 0 Or Session("nivel") = 1 Or Session("nivel") = 4) Then
                        lbl = New Label
                        lbl.Text = "&nbsp;&nbsp;"
                        tbcell.Controls.Add(lbl)
                        hp = New HyperLink
                        hp.ToolTip = "Seleccione la opción que desea emitir"
                        hp.ImageUrl = "..\Imagenes\Menu\printer.png"
                        ''''hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.SelectedValue & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        ''''hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue) & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue) & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & "," & Session("12meses") & ")")
                        hp.Style.Add("cursor", "pointer")
                        tbcell.Controls.Add(hp)
                    End If
                Next
            End If

            If cuentaTotal < 5 Then
                For i = cuentaTotal To 4
                    tbcell = New TableCell
                    tbcell.Text = "&nbsp;"
                    tbcell.VerticalAlign = VerticalAlign.Middle
                    tbcell.HorizontalAlign = HorizontalAlign.Center
                    tbcell.CssClass = "negrita"
                    tbcell.Width = Unit.Percentage(15)
                    tbrow.Controls.Add(tbcell)
                Next
            End If

            tb.Controls.Add(tbrow)
            tb.Width = Unit.Percentage(100)
            Return tb

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Function DetalleAseguradora(ByVal IdASeguradora As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim strcadena As String = ""
        Dim Himage As HtmlImage

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.RowSpan = 4
        'tbcell.Text = "&nbsp;"
        Himage = New HtmlImage
        Himage.Src = ccliente.UrlImagAseg
        tbcell.Controls.Add(Himage)
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "negrita"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        For i = 0 To 3


            tbcell = New TableCell
            Select Case i
                Case 0
                    strcadena = "Meses de seguro"
                Case 1
                    strcadena = "No. De pagos"
                    tbrow = New TableRow
                Case 2
                    strcadena = "Prima parcial"
                    tbrow = New TableRow
                Case 3
                    strcadena = "Prima total"
                    tbrow = New TableRow
            End Select
            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 1, "G", 15, ""))

            If Not Arr(0, 0) Is Nothing Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        'strcadena = cbo_plazo.SelectedValue
                        'strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                        strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
                    Case 1
                        'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(0, 0))
                        'strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), Arr(0, 0))
                        strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), Arr(0, 0))
                    Case 2
                        strcadena = Format(CDbl(Arr(0, 2)), "$ #,###,##0.00")
                    Case 3
                        strcadena = Format(CDbl(Arr(0, 1)), "$ #,###,##0.00")
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
            Else
                tbrow.Controls.Add(agrega_celdas("", "negrita", 3, "G", 15, ""))
            End If

            If Not Arr(1, 0) Is Nothing Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        'strcadena = cbo_plazo.SelectedValue
                        'strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                        strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
                    Case 1
                        'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(1, 0))
                        'strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), Arr(1, 0))
                        strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), Arr(1, 0))
                    Case 2
                        strcadena = Format(CDbl(Arr(1, 2)), "$ #,###,##0.00")
                    Case 3
                        strcadena = Format(CDbl(Arr(1, 1)), "$ #,###,##0.00")
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
            Else
                tbrow.Controls.Add(agrega_celdas("", "negrita", 3, "G", 15, ""))
            End If

            If Not Arr(2, 0) Is Nothing Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        'strcadena = cbo_plazo.SelectedValue
                        'strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                        strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
                    Case 1
                        'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(2, 0))
                        'strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), Arr(2, 0))
                        strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), Arr(2, 0))
                    Case 2
                        strcadena = Format(CDbl(Arr(2, 2)), "$ #,###,##0.00")
                    Case 3
                        strcadena = Format(CDbl(Arr(2, 1)), "$ #,###,##0.00")
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
            Else
                tbrow.Controls.Add(agrega_celdas("", "negrita", 3, "G", 15, ""))
            End If

            If Not Arr(3, 0) Is Nothing Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        'strcadena = cbo_plazo.SelectedValue
                        'strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                        strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
                    Case 1
                        'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(3, 0))
                        'strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), Arr(3, 0))
                        strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), Arr(3, 0))
                    Case 2
                        strcadena = Format(CDbl(Arr(3, 2)), "$ #,###,##0.00")
                    Case 3
                        strcadena = Format(CDbl(Arr(3, 1)), "$ #,###,##0.00")
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
            Else
                tbrow.Controls.Add(agrega_celdas("", "negrita", 3, "G", 15, ""))
            End If

            If Not Arr(4, 0) Is Nothing Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        'strcadena = cbo_plazo.SelectedValue
                        'strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue)
                        strcadena = IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue)
                    Case 1
                        'strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(4, 0))
                        'strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue - 12, cbo_plazo.SelectedValue), Arr(4, 0))
                        strcadena = total_Pagos(IIf(Session("Exclusion") = 0, cbo_plazo.SelectedValue, cbo_plazo.SelectedValue), Arr(4, 0))
                    Case 2
                        strcadena = Format(CDbl(Arr(4, 2)), "$ #,###,##0.00")
                    Case 3
                        strcadena = Format(CDbl(Arr(4, 1)), "$ #,###,##0.00")
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
            Else
                tbrow.Controls.Add(agrega_celdas("", "negrita", 3, "G", 15, ""))
            End If

            tb.Controls.Add(tbrow)
        Next

        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function total_Pagos(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer) As Integer
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select
        Return IntPagos
    End Function

#Region " Calculo de validacion para las ventas cruzadas de seguro gratis"

    Private Sub RecalculaValor_ASeguradora(ByVal TipoPrograma As Integer)
        tipo_poliza(Request.QueryString("cveplazo"))

        Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")
        Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
               Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
               1, _
               Session("tipopol"), _
               Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
               Session("EstatusV"), _
               0, Session("InterestRate"), Session("FechaIni"), _
               Session("tiposeguro"), Request.QueryString("cveplazo"), _
               Request.QueryString("valcost"), _
               Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
               0, Session("vehiculo"), Request.QueryString("cveAseg"), _
               Request.QueryString("cvepaquete"), Session("programa"), _
               Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
               Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
               Request.QueryString("TipoCarga"), 0, Session("TipoProducto"), _
               Request.QueryString("Aseg12meses"), TipoPrograma)
        'fin cambio micha

        Session("subsidios") = ccalculo.subsidios
        Session("ArrPoliza") = ccalculo.ArrPolisa
        Session("resultado") = ccalculo.Results
        Session("MultArrPolisa") = ccalculo.MultArrPolisa

        Session("DatosGenerales") = ""
        Dim arr(25) As String
        arr(0) = Session("moneda")
        arr(1) = Request.QueryString("subramo")
        arr(2) = Session("anio")
        arr(3) = Session("modelo")
        arr(4) = 1
        'arr(5) = Seg_financiado(rb_seguro)
        arr(5) = Seg_financiado_Seleccion(Session("seguro"))
        arr(6) = Session("uso")
        arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
        arr(8) = Session("EstatusV")
        arr(9) = 0
        arr(10) = Session("InterestRate")
        arr(11) = Session("FechaIni")
        arr(12) = Session("tiposeguro")
        arr(13) = Request.QueryString("cveplazo")
        arr(14) = Request.QueryString("valcost")
        arr(15) = Session("idgrdcontrato")
        arr(16) = Session("seguro")
        arr(17) = Session("bid")
        arr(18) = 0
        arr(19) = Session("vehiculo")
        arr(20) = Request.QueryString("cveAseg")
        arr(21) = Request.QueryString("cvepaquete")
        arr(22) = Session("programa")
        arr(23) = Session("fincon")
        'arr(24) = Request.QueryString("Pago1")
        'arr(25) = Request.QueryString("Pago2")
        arr(24) = Math.Round(Session("ArrPoliza")(0) / Request.QueryString("NumPago"), 2)
        If CDbl(Request.QueryString("Pago2")) > 0 Then
            arr(25) = arr(24)
        Else
            arr(25) = Request.QueryString("Pago2")
        End If
        Session("DatosGenerales") = arr

        'manolito

        Session("plazoFinanciamiento") = Request.QueryString("Recargo")
        Session("plazoRecargo") = Request.QueryString("NumPago")

        Session("region") = Request.QueryString("cveregion")
        Session("aseguradora") = Request.QueryString("cveAseg")
        Session("plazo") = Request.QueryString("cveplazo")
        Session("FechaInicio") = Session("FechaIni")
        Session("contrato") = Request.QueryString("cvecontrato")
        Session("precioEscrito") = Request.QueryString("valcost")
        Session("paquete") = Request.QueryString("cvepaquete")
        Session("Tipocarga") = Request.QueryString("TipoCarga")

        Recalculo_general_aseguradora(Request.QueryString("Recargo"), Request.QueryString("cveplazo"))

        Session("DatosGenerales")(24) = Math.Round(Session("ArrPoliza")(0) / Request.QueryString("NumPago"), 2)
        If CDbl(Request.QueryString("Pago2")) > 0 Then
            Session("DatosGenerales")(25) = Session("DatosGenerales")(24)
        Else
            Session("DatosGenerales")(25) = Request.QueryString("Pago2")
        End If

    End Sub

    Public Sub Recalculoaseguradora12meses(ByVal TipoPrograma As Integer, Optional ByVal Recalcula As Integer = 1)
        Dim Aseguradora12 As Integer = 0
        Dim ValorIdPaquete As Integer = 0
        Dim i As Integer = 0

        Try
            Dim dt As DataTable = ccalculo.Promocion12meses(Session("idcotizapanel12"))
            If dt.Rows.Count > 0 Then

                If Recalcula = 0 Then
                    Dim arrbusqueda() As String = Split(Session("CadenaCob"), "|", )
                    If Not arrbusqueda Is Nothing Then
                        For i = 0 To arrbusqueda.Length - 1
                            Dim arrValor() As String = arrbusqueda(i).Split("*")
                            If arrValor(0) = dt.Rows(0)("Id_Aseguradora") Then
                                ValorIdPaquete = CInt(arrValor(1))
                                Exit For
                            End If
                        Next
                    End If
                End If

                Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                    dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                    1, "A", _
                    Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                    Session("EstatusV"), _
                    0, Session("InterestRate"), Session("FechaIni"), _
                    Session("tiposeguro"), 12, _
                    Request.QueryString("valcost"), _
                    Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                    0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                    IIf(Recalcula = 1, dt.Rows(0)("idpaquete"), ValorIdPaquete), Session("programa"), _
                    Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                    dt.Rows(0)("id_tipoRecargo"), 1, Request.QueryString("TipoCarga"), _
                    IIf(Session("Exclusion") = 0, 1, 0), , , TipoPrograma, , IIf(Recalcula = 1, 0, 1))
                'fin cambio micha

                Session("subsidios1") = ccalculo.subsidios
                Session("ArrPoliza1") = ccalculo.ArrPolisa
                Session("resultado1") = ccalculo.Results
                Session("MultArrPolisa1") = ccalculo.MultArrPolisa

                Session("DatosGenerales1") = ""
                Dim arr(25) As String
                arr(0) = Session("moneda")
                arr(1) = dt.Rows(0)("subramo")
                arr(2) = Session("anio")
                arr(3) = Session("modelo")
                arr(4) = 1
                'arr(5) = Seg_financiado(rb_seguro)
                arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                arr(6) = Session("uso")
                arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                arr(8) = Session("EstatusV")
                arr(9) = 0
                arr(10) = Session("InterestRate")
                arr(11) = Session("FechaIni")
                arr(12) = Session("tiposeguro")
                arr(13) = 12
                arr(14) = Request.QueryString("valcost") 'txt_precio.Text
                arr(15) = Session("idgrdcontrato")
                arr(16) = Session("seguro")
                arr(17) = Session("bid")
                arr(18) = 0
                arr(19) = Session("vehiculo")
                arr(20) = dt.Rows(0)("Id_aseguradora")
                arr(21) = dt.Rows(0)("idpaquete")
                arr(22) = Session("programa")
                arr(23) = Session("fincon")
                arr(24) = dt.Rows(0)("primatotal")
                arr(25) = dt.Rows(0)("primaconsecutiva")
                Session("DatosGenerales1") = arr

                'manolito
                Session("plazo1") = 12
                Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
                Session("plazoRecargo1") = 1
                Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
                Session("paquete1") = dt.Rows(0)("idpaquete")
                Session("subramo1") = dt.Rows(0)("subramo")

                Dim uno As String = ccalculo.StrCadenaAseg
                Dim dt12 As DataTable = ccalculo.Update_Cotizacion_panel_12meses(Session("idcotizapanel12"), ccalculo.StrCadenaAseg, "E")
                If dt12.Rows.Count > 0 Then
                    If Not dt12.Rows(0).IsNull("IdPaquete") Then
                        Session("DatosGenerales1")(21) = dt12.Rows(0)("IdPaquete")
                    End If

                    If Not dt12.Rows(0).IsNull("primatotal") Then
                        Session("DatosGenerales1")(24) = dt12.Rows(0)("primatotal")
                    End If

                    If Not dt12.Rows(0).IsNull("primaconsecutiva") Then
                        Session("DatosGenerales1")(25) = dt12.Rows(0)("primaconsecutiva")
                    End If

                    If Not dt12.Rows(0).IsNull("IdPaquete") Then
                        Session("paquete1") = dt12.Rows(0)("IdPaquete")
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub Recalculo_general_aseguradora(ByVal IdRecargo As Integer, ByVal cboplazo As Integer)
        Dim arr() As String
        Dim Arr1() As String
        Dim i, x As Integer
        Dim strcadena As String = ""
        Dim strCadenaCal As String = ""
        Dim FechaTermina As String = ""
        Dim ds As DataSet

        Dim Tiporecargo As Integer = 0
        Dim strInserta As String = ""
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0

        Dim Aseguradora As Integer = 0


        Try

            strcadena = ccalculo.StrCadenaAseg
            strCadenaCal = ccalculo.StrSalidaPanleAseg

            Select Case IdRecargo
                Case 2 'Anual
                    Tiporecargo = 1
                Case 3 'Semestral
                    Tiporecargo = 4
                Case 4 'Trimestral
                    Tiporecargo = 3
                Case 5 'Mensual
                    Tiporecargo = 2
                Case Else
                    Tiporecargo = IdRecargo
            End Select

            FechaTermina = ccliente.FechaFinalFracciones(Session("FechaIni"), IIf(Session("Exclusion") = 0, cboplazo, cboplazo))

            Dim Results() As String
            Dim Results1() As String

            arr = strcadena.Split("|")
            Results = strCadenaCal.Split("|")

            For i = 0 To arr.Length - 1
                Arr1 = arr(i).Split("*")
                Results1 = Results(i).Split("*")

                If Arr1(0) = Results1(0) Then
                    ds = Nothing
                    CdFraccion = New CP_FRACCIONES.clsEmision
                    Select Case CInt(Arr1(0))
                        Case 64
                            ds = CdFraccion.FraccionesQualitas_Conauto(Session("FechaIni"), _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                        Case 65
                            ds = CdFraccion.FraccionesAXA_Conauto(Session("FechaIni"), _
                                FechaTermina, _
                                Results1(1), Results1(2), _
                                Results1(3), Results1(4), _
                                Results1(5), Tiporecargo, Results1(6), _
                                (Results1(7) * 100))
                    End Select

                    If ds.Tables(0).Rows.Count > 0 Then
                        For x = 0 To ds.Tables(0).Rows.Count - 1
                            Select Case x
                                Case 0
                                    Dbtotales1 = ds.Tables(0).Rows(x)("prima_total")
                                Case 1
                                    Dbtotales2 = ds.Tables(0).Rows(x)("prima_total")
                                    Exit For
                            End Select
                        Next
                    End If

                    If strInserta = "" Then
                        strInserta = Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                    Else
                        strInserta = strInserta & "|" & Arr1(0) & "*" & IdRecargo & "*" & Arr1(2) & "*" & Dbtotales1 & "*" & Results1(8) & "*" & Results1(9) & "*" & Dbtotales2 & "*" & Results1(10)
                    End If
                End If
            Next

            If Not strInserta = "" Then
                Dim dtP As DataTable = ccalculo.Act_Cotizacion_panel(Session("idcotizapanel"), strInserta)
                If dtP.Rows.Count > 0 Then
                    If Not dtP.Rows(0).IsNull("IdPaquete") Then
                        Session("DatosGenerales")(21) = dtP.Rows(0)("IdPaquete")
                    End If

                    If Not dtP.Rows(0).IsNull("PrimaConsecutiva") Then
                        Session("DatosGenerales")(24) = dtP.Rows(0)("PrimaConsecutiva")
                    End If

                    If Not dtP.Rows(0).IsNull("PrimaConsecutiva2") Then
                        Session("DatosGenerales")(25) = dtP.Rows(0)("PrimaConsecutiva2")
                    End If

                    If Not dtP.Rows(0).IsNull("IdPaquete") Then
                        Session("paquete") = dtP.Rows(0)("IdPaquete")
                    End If

                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

#End Region


    Protected Sub cbo_uso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbo_uso.SelectedIndexChanged

    End Sub
End Class

