<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmMsg.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmMsg"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmMsg</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			&nbsp;
			<cc1:MsgBox style="Z-INDEX: 104; POSITION: absolute; TOP: 384px; LEFT: 296px" id="MsgBox" runat="server"></cc1:MsgBox>
			<TABLE style="Z-INDEX: 105; POSITION: absolute; TOP: 8px; LEFT: 0px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD style="Z-INDEX: 0"></TD>
					<TD class="Interiro_tabla" align="right">Seguro Gratis 12 meses :</TD>
					<TD class="Interiro_tabla" vAlign="middle" align="center">
						<asp:Image id="Img12" runat="server"></asp:Image></TD>
					<TD class="Interiro_tabla">
						<asp:label style="Z-INDEX: 0" id="lbl_12meses" runat="server" Width="100%"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD class="Interiro_tabla" align="right">Resto del Financiamiento :</TD>
					<TD class="Interiro_tabla" vAlign="middle" align="center">
						<asp:Image style="Z-INDEX: 0" id="ImgResto" runat="server"></asp:Image></TD>
					<TD class="Interiro_tabla">
						<asp:Label style="Z-INDEX: 0" id="lbl_resto1" runat="server" Width="100%"></asp:Label>
						<asp:Label style="Z-INDEX: 0" id="lbl_resto2" runat="server" Width="100%"></asp:Label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 28px"></TD>
					<TD style="HEIGHT: 28px"></TD>
					<TD style="HEIGHT: 30px"></TD>
					<TD style="HEIGHT: 28px"></TD>
					<TD style="HEIGHT: 28px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD align="center"><BUTTON class="boton" onclick="window.parent.hidePopWin(true)">Cerrar</BUTTON></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="5%"></TD>
					<TD width="35%"></TD>
					<TD width="15%"></TD>
					<TD width="40%"></TD>
					<TD width="5%"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
