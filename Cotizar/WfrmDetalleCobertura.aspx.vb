﻿Imports CN_Negocios
Imports System.Threading.Tasks
Imports System.IO

Public Class WfrmDetalleCobertura
    Inherits System.Web.UI.Page

    Private ccalculo As New CnCalculo
    Dim i, j, rowNum, cellNum As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load, MyBase.Load

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not IsPostBack Then
                tb_cobertura.Rows.Clear()
                Dim tbcell As New TableCell
                Dim tbrow As New TableRow
                Dim dt As DataTable = ccalculo.CargaCobertura(Request.QueryString("cveAseg"), Request.QueryString("cvepaquete"))
                If dt.Rows.Count > 0 Then
                    For rowNum = 0 To dt.Rows.Count - 1
                        Dim tempRow As New TableRow

                        Dim tempCell As New TableCell
                        Dim tempCells As New TableCell
                        Dim tempCelld As New TableCell
                        tempCell.Text = String.Format("{0}", dt.Rows(rowNum)("cobertura"))

                        If dt.Rows(rowNum)("descripcion_suma").ToString().Contains("Valor Factura") Then
                            tempCells.Text = Format(CDbl(Request.QueryString("SumaAsegurada")), "$ #,###,##0")
                        Else
                            If IsNumeric(dt.Rows(rowNum)("descripcion_suma")) Then
                                tempCells.Text = Format(CDbl(dt.Rows(rowNum)("descripcion_suma")), "$ #,###,##0")
                            Else
                                tempCells.Text = dt.Rows(rowNum)("descripcion_suma")
                            End If

                        End If

                        If (dt.Rows(rowNum)("cobertura").ToUpper().Contains("ROBO TOTAL") And
                            New CC.Negocio.Reporte.Coberturas().ValidaQualitasRiesgo(Request.QueryString("CodigoPostal"), Request.QueryString("Modelo"))) Then
                            tempCelld.Text = "20%"
                        Else
                            tempCelld.Text = IIf(dt.Rows(rowNum)("deducible") = Nothing Or dt.Rows(rowNum)("deducible") = String.Empty, "No Aplica", dt.Rows(rowNum)("deducible"))
                        End If

                        tempCell.Width = Unit.Percentage(50)
                        tempCelld.Width = Unit.Percentage(20)
                        tempCell.CssClass = "negrita"
                        tempCells.CssClass = "negrita"
                        tempCelld.CssClass = "negrita"
                        tempCells.HorizontalAlign = HorizontalAlign.Center
                        tempCelld.HorizontalAlign = HorizontalAlign.Center

                        tempRow.Cells.Add(tempCell)
                        tempRow.Cells.Add(tempCells)
                        tempRow.Cells.Add(tempCelld)

                        tb_cobertura.Rows.Add(tempRow)
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

End Class