'Seguro Financiado :
'se quita solo se deja el valor de no (2)
'txt_banseguro = 2
'esto es igual al pago de contado
'solo tenemos este valor para las validaciones y calculos
'
Imports CN_Negocios
Imports CP_FRACCIONES

Partial Class WfrmCotiza1
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private ccalculo As New CnCalculo
    Private cprincipal As New CnPrincipal
    Private arrTotal(0) As String
    Private arrPol(0) As String
    Private arrNombrePol(0) As String
    Private PaqSel As String = ""
    Private CdFraccion As New CP_FRACCIONES.clsEmision

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Dim total As Double = 0
        Dim strCadenaS As String = ""
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                txt_fecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                If Not Request.QueryString("valcost") Is Nothing Then
                    txt_precio.Text = Request.QueryString("valcost")
                    txt_precioF.Text = txt_precio.Text
                End If



                Select Case Session("contribuyente")
                    Case 0
                        lbl_persona.Text = "F�sica"
                        cmd_cotizar.Visible = False
                    Case 1
                        lbl_persona.Text = "Act. Empresarial"
                        cmd_cotizar.Visible = False
                    Case 2
                        lbl_persona.Text = "Moral"
                        cmd_cotizar.Visible = False
                End Select

                txt_sumcob.Text = 0
                limpia()
                carga_datos_vehiculo()

                If Request.QueryString("cveAseg") Is Nothing Then
                    If Not Request.QueryString("valcost1") Is Nothing Then
                        txt_precio.Text = Request.QueryString("valcost1")
                        txt_precioF.Text = Request.QueryString("valcost1")

                        'forma pago da�os
                        Dim banSeg As Integer
                        If Session("seguro") = 0 Then
                            banSeg = 1
                        Else
                            banSeg = 0
                        End If
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))
                        Seg_financiado(rb_seguro)

                        carga_plazo()
                        carga_plazo_TipoPago()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(CInt(Session("plazoSeleccion"))))
                        lbl_plazo.Text = Session("plazo")
                        cbo_tipo.SelectedIndex = cbo_tipo.Items.IndexOf(cbo_tipo.Items.FindByValue(CInt(Session("plazoFinanciamiento"))))
                        carga_plazo_Recargo(cbo_tipo.SelectedValue)
                        cbo_recargo.SelectedIndex = cbo_recargo.Items.IndexOf(cbo_recargo.Items.FindByValue(CInt(Session("plazoRecargo"))))
                    Else
                        tipo_poliza(12)
                        Session("idgrdcontrato") = ccliente.carga_contrato(Session("bid"))
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(0))
                        Seg_financiado(rb_seguro)

                        carga_plazo()
                        carga_plazo_TipoPago()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
                        lbl_plazo.Text = 12
                        cbo_tipo.SelectedIndex = cbo_tipo.Items.IndexOf(cbo_tipo.Items.FindByValue(1))
                        carga_plazo_Recargo(1)
                        cbo_recargo.SelectedIndex = cbo_recargo.Items.IndexOf(cbo_recargo.Items.FindByValue(1))
                        Session("plazo") = 12
                        Session("plazoSeleccion") = 12
                        Session("plazoFinanciamiento") = 1
                        Session("plazoRecargo") = 1
                    End If

                    strCadenaS = calculo_general()
                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    End If
                Else
                    'nuevo
                    'inicio cambio micha
                    If Not IsDate(Request.QueryString("strfecha")) Then
                        MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
                        'forma pago da�os
                        Dim banSeg As Integer
                        If Session("seguro") = 0 Then
                            banSeg = 1
                        Else
                            banSeg = 0
                        End If
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(banSeg))
                        Seg_financiado(rb_seguro)

                        carga_plazo()
                        carga_plazo_TipoPago()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(CInt(Session("plazoSeleccion"))))
                        lbl_plazo.Text = Request.QueryString("cveplazo")
                        cbo_tipo.SelectedIndex = cbo_tipo.Items.IndexOf(cbo_tipo.Items.FindByValue(CInt(Request.QueryString("Recargo"))))
                        carga_plazo_Recargo(Request.QueryString("NumPago"))
                        cbo_recargo.SelectedIndex = cbo_recargo.Items.IndexOf(cbo_recargo.Items.FindByValue(CInt(Request.QueryString("NumPago"))))
                        carga_grd()
                        Session("plazo") = Request.QueryString("cveplazo")
                        Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                        Session("plazoRecargo") = Request.QueryString("NumPago")
                        Exit Sub
                    End If

                    tipo_poliza(Request.QueryString("cveplazo"))

                    Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                           Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                           Session("EstatusV"), _
                           0, Session("InterestRate"), Request.QueryString("strfecha"), _
                           Session("tiposeguro"), Request.QueryString("cveplazo"), txt_precio.Text, _
                           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                           txt_sumcob.Text, Session("vehiculo"), Request.QueryString("cveAseg"), _
                           Request.QueryString("cvepaquete"), Session("programa"), _
                           Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                           Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
                           Session("Tipocarga"))
                    'fin cambio micha

                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results
                    Session("MultArrPolisa") = ccalculo.MultArrPolisa

                    Session("DatosGenerales") = ""
                    Dim arr(25) As String
                    arr(0) = Session("moneda")
                    arr(1) = Request.QueryString("subramo")
                    arr(2) = Session("anio")
                    arr(3) = Session("modelo")
                    arr(4) = 1
                    'arr(5) = Seg_financiado(rb_seguro)
                    arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                    arr(6) = Session("uso")
                    arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arr(8) = Session("EstatusV")
                    arr(9) = 0
                    arr(10) = Session("InterestRate")
                    arr(11) = Request.QueryString("strfecha")
                    arr(12) = Session("tiposeguro")
                    arr(13) = Request.QueryString("cveplazo")
                    arr(14) = txt_precio.Text
                    arr(15) = Session("idgrdcontrato")
                    arr(16) = Session("seguro")
                    arr(17) = Session("bid")
                    arr(18) = txt_sumcob.Text
                    arr(19) = Session("vehiculo")
                    arr(20) = Request.QueryString("cveAseg")
                    arr(21) = Request.QueryString("cvepaquete")
                    arr(22) = Session("programa")
                    arr(23) = Session("fincon")
                    arr(24) = Request.QueryString("Pago1")
                    arr(25) = Request.QueryString("Pago2")
                    Session("DatosGenerales") = arr

                    'manolito

                    Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                    Session("plazoRecargo") = Request.QueryString("NumPago")

                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("FechaInicio") = Request.QueryString("strfecha")
                    Session("contrato") = Request.QueryString("cvecontrato")
                    Session("precioEscrito") = txt_precio.Text
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Session("Tipocarga") = Request.QueryString("TipoCarga")
                    Response.Redirect("WfrmCliente.aspx")
                End If

                If Request.QueryString("valcost1") Is Nothing Then
                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        cmd_cotizar.Visible = False
                        Exit Sub
                    Else
                        cmd_cotizar.Visible = False
                    End If
                    Dim dt As DataTable = ccliente.inserta_coberturas( _
                        strCadenaS, Session("idgrdcontrato"), Session("bid"), Session("programa"), _
                        Session("vehiculo"), Session("EstatusV"), Session("anio"))
                    If dt.Rows.Count > 0 Then
                        grd_cobertura.DataSource = dt
                        grd_cobertura.DataBind()
                        grd_cobertura.Visible = True
                        Carga_Datos_Dinamicos()
                    Else
                        grd_cobertura.Visible = False
                    End If
                Else
                    carga_grd()
                End If
            End If

            'paneles contado
            If Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13 Then
                txt_subtipo.Visible = False
                lbl_clasificacion.Visible = True
                txt_clasificacion.Visible = True

                lbl_clasificacion.Text = "Uso"
                txt_clasificacion.Text = Session("uso")

            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Public Sub desabilita()
        txt_marca.Enabled = False
        txt_tipo.Enabled = False
        txt_subtipo.Enabled = False
        txt_modelo.Enabled = False
        txt_descripcion.Enabled = False
        txt_clasificacion.Enabled = False
    End Sub

    Public Sub limpia()
        txt_marca.Text = ""
        txt_tipo.Text = ""
        txt_subtipo.Text = ""
        txt_modelo.Text = ""
        txt_descripcion.Text = ""
        txt_clasificacion.Text = ""

        txt_grdcobertura.Text = ""
        txt_gdescripcion.Text = ""
        txt_ban.Text = 0
    End Sub

    Public Sub carga_datos_vehiculo()
        Try
            txt_marca.Text = Session("MarcaDesc")
            txt_modelo.Text = Session("anio")
            txt_tipo.Text = Session("modelo")
            txt_descripcion.Text = Session("vehiculoDesc")
            txt_subtipo.Text = Session("catalogo")
            'txt_clasificacion.Text = IIf( = 1, "Autom�vil", "Cami�n")
            Dim dt As DataTable = ccliente.carga_tipocarga(Session("programa"), Session("modelo"))
            If dt.Rows.Count > 1 Then
                cbo_carga.DataSource = dt
                cbo_carga.DataValueField = "id_tipocarga"
                cbo_carga.DataTextField = "descripcion_tipocarga"
                cbo_carga.DataBind()
                cbo_carga.SelectedIndex = cbo_carga.Items.IndexOf(cbo_carga.Items.FindByValue(1))
                Session("Tipocarga") = cbo_carga.SelectedValue
                cbo_carga.Visible = True
                lbl_carga.Visible = True
            Else
                Session("Tipocarga") = 0
                cbo_carga.Visible = False
                lbl_carga.Visible = False
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                'Session("seguro") = 2
                Session("tipopol") = "M"
                'Session("bUnidadfinanciada") = True
            Else
                'Session("seguro") = 0
                Session("tipopol") = "A"
                'Session("bUnidadfinanciada") = False
            End If
        Else
            Session("tipopol") = "A"
            'Session("seguro") = 0
            'Session("bUnidadfinanciada") = False
        End If
    End Sub


    Private Sub grd_cobertura_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.EditCommand
        Dim total As Double = 0
        Dim factV As Double = 0
        Dim strCadenaS As String = ""
        Try
            If txt_grdcobertura.Text <> 0 Then
                'If txt_gdescripcion.Text = "Auto Sustituto" Or txt_gdescripcion.Text = "Ext. Res. Civil" Then

                'tipo_poliza(cbo_plazo.SelectedValue)
                tipo_poliza(lbl_plazo.Text)
                Seg_financiado(rb_seguro)

                If ccliente.valida_grid_edit(Session("ArrPolValida"), txt_ban.Text) = 1 Then

                    'cbo_plazo.SelectedValue, Seg_financiado(rb_seguro), _
                    ccliente.determina_facvigencia_aseguradora(Session("moneda"), _
                        lbl_plazo.Text, Seg_financiado(rb_seguro), _
                        Session("idgrdcontrato"), Session("bid"), "", "", 0, _
                        txt_grdcobertura.Text, _
                        lbl_plazo.Text, e.Item.Cells(13).Text)
                    'cbo_plazo.SelectedValue, e.Item.Cells(13).Text)

                    grd_cobertura.EditItemIndex = -1

                    txt_sumcob.Text = 0

                    strCadenaS = calculo_general()
                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    End If
                    'nuevo bueno
                    'strCadenaS = ccalculo.calculo(Session("moneda"), _
                    '       Session("anio"), Session("modelo"), _
                    '       1, _
                    '       Seg_financiado(txt_banseguro.Text), _
                    '       Session("uso"), Session("EstatusV"), _
                    '       0, Session("InterestRate"), txt_fecha.Text, _
                    '       Session("tiposeguro"), cbo_plazo.SelectedValue, txt_precio.Text, _
                    '       txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                    '       txt_sumcob.Text, Session("vehiculo"), Session("programa"), Session("fincon"))

                    carga_grd()
                Else
                    grd_cobertura.EditItemIndex = e.Item.ItemIndex
                    carga_grd()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cobertura_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.UpdateCommand
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        Dim descripcion As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grddesc")
        Dim suma As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdsuma")
        Dim factura As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdfact")

        If suma.Text = "" Then
            MsgBox.ShowMessage("Escriba el Monto de la Suma Asegurada")
            Exit Sub
        End If

        If suma.Text > 0 Then
            If descripcion.Text = "" Then
                MsgBox.ShowMessage("Escriba la Descripci�n de la Cobertura")
                Exit Sub
            Else
                If descripcion.Text.Length >= 75 Then
                    MsgBox.ShowMessage("La descripci�n debe de ser menor a 75 caracteres")
                    Exit Sub
                End If
            End If
            If factura.Text = "" Then
                MsgBox.ShowMessage("Escriba el N�mero de Factura")
                Exit Sub
            End If
        Else
            If suma.Text < 0 Then
                MsgBox.ShowMessage("La cantidad del Monto no debe de ser Menor a Cero")
                Exit Sub
            End If
        End If
        Try
            'tipo_poliza(cbo_plazo.SelectedValue)
            tipo_poliza(lbl_plazo.Text)
            Seg_financiado(rb_seguro)

            ccliente.actualiza_cobertura(suma.Text, descripcion.Text, factura.Text, _
            txt_grdcobertura.Text, Session("idgrdcontrato"), Session("bid"), _
            lbl_plazo.Text)
            'cbo_plazo.SelectedValue)

            grd_cobertura.EditItemIndex = -1

            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If
            'nuevo bueno
            'strCadenaS = ccalculo.calculo(Session("moneda"), _
            '        Session("anio"), Session("modelo"), _
            '        1, _
            '        Seg_financiado(txt_banseguro.Text), _
            '        Session("uso"), Session("EstatusV"), _
            '        0, Session("InterestRate"), txt_fecha.Text, _
            '        Session("tiposeguro"), cbo_plazo.SelectedValue, txt_precio.Text, _
            '        txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
            '        txt_sumcob.Text, Session("vehiculo"), Session("programa"), Session("fincon"))

            carga_grd()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_grd()
        Dim dt As DataTable = ccliente.carga_cobertura_bid(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), Session("programa"), Session("vehiculo"))
        If dt.Rows.Count > 0 Then
            grd_cobertura.DataSource = dt
            grd_cobertura.DataBind()
            grd_cobertura.Visible = True
            Carga_Datos_Dinamicos()
        Else
            grd_cobertura.Visible = False
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub cbo_plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_plazo.SelectedIndexChanged
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        If cbo_plazo.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione el Plazo de seguro")
            Exit Sub
        End If
        Try
            Session("plazoSeleccion") = cbo_plazo.SelectedValue
            'Session("plazo") = cbo_plazo.SelectedValue
            'tipo_poliza(cbo_plazo.SelectedValue)
            Seg_financiado(rb_seguro)

            'ccalculo.actualiza_numplazo_cob(cbo_plazo.SelectedValue, Session("idgrdcontrato"), _
            '    Session("bid"))


            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub



    Private Sub grd_cobertura_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.ItemCommand
        If Not e.Item.Cells(2).Text = "" Then txt_grdcobertura.Text = e.Item.Cells(2).Text
        If Not e.Item.Cells(3).Text = "" Then txt_gdescripcion.Text = e.Item.Cells(3).Text
        If Not e.Item.Cells(12).Text = "" Then txt_ban.Text = e.Item.Cells(12).Text
    End Sub

    Private Sub grd_cobertura_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.CancelCommand
        grd_cobertura.EditItemIndex = -1
        carga_grd()
        txt_grdcobertura.Text = 0
    End Sub

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Private Sub grd_cobertura_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cobertura.SelectedIndexChanged

    End Sub

    Private Sub grd_cobertura_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.DeleteCommand
        Dim total As Double = 0
        Dim descripcion As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grddesc")
        Dim suma As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdsuma")
        Dim factura As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdfact")
        'Dim intsubramo As Integer = e.Item.Cells(14).Text
        Dim strCadenaS As String = ""
        Try
            If grd_cobertura.EditItemIndex <> -1 Then
                carga_grd()
                Exit Sub
            End If

            'tipo_poliza(cbo_plazo.SelectedValue)
            tipo_poliza(lbl_plazo.Text)
            Seg_financiado(rb_seguro)

            ccliente.actualiza_cobertura_VALOR_cero("", "", _
                txt_grdcobertura.Text, Session("idgrdcontrato"), Session("bid"), _
                lbl_plazo.Text, e.Item.Cells(13).Text)
            'cbo_plazo.SelectedValue, e.Item.Cells(13).Text)

            grd_cobertura.EditItemIndex = -1

            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cobertura_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_cobertura.ItemDataBound
        Dim dt As DataTable
        Dim intregion As Integer = 0
        Dim Dbvalor As Double = 0
        Dim Dbtotales1 As Double = 0
        Dim Dbtotales2 As Double = 0
        If e.Item.ItemType = ListItemType.Header Then
            Dim cuenta As Integer = 7
            Dim i As Integer = 0
            Dim hp As HyperLink
            Dim lbl As Label
            dt = ccliente.carga_cobertura_bid_aseguradora(Session("idgrdcontrato"), Session("bid"))
            If dt.Rows.Count > 0 Then
                ReDim arrTotal(dt.Rows.Count - 1)
                ReDim arrPol(dt.Rows.Count - 1)
                ReDim arrNombrePol(dt.Rows.Count - 1)
                For i = 0 To dt.Rows.Count - 1
                    e.Item.Cells(cuenta).Attributes.Add("align", "center")

                    If dt.Rows(i).IsNull("url_imagen") Then
                        lbl = New Label
                        lbl.Text = dt.Rows(i)("descripcion_aseguradora")
                        e.Item.Cells(cuenta).Controls.Add(lbl)
                    Else
                        hp = New HyperLink
                        hp.Text = dt.Rows(i)("descripcion_aseguradora")
                        hp.ImageUrl = dt.Rows(i)("url_imagen")
                        e.Item.Cells(cuenta).Controls.Add(hp)
                    End If

                    ''''e.Item.Cells(cuenta).Text = dt.Rows(i)("descripcion_aseguradora")

                    arrPol(i) = dt.Rows(i)("id_aseguradora")
                    arrNombrePol(i) = dt.Rows(i)("descripcion_aseguradora")
                    grd_cobertura.Columns(cuenta).Visible = True
                    cuenta = cuenta + 1
                Next
            End If
        End If

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim recorrepoliza As Integer = 0
            Dim validarecorrido As Integer = 0
            Dim hp As HyperLink
            Dim hpImagen As HyperLink
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim x As Integer = 0
            Dim cuenta As Integer = 7
            Dim formaPago As Integer = 0
            Dim FechaTermina As String = ""
            If IsNumeric(e.Item.Cells(2).Text) Then
                If e.Item.Cells(2).Text = 0 Then 'coberturas
                    e.Item.Cells(0).Text = ""
                    e.Item.Cells(1).Text = ""
                    'e.Item.Cells(14).Text antes era el subramo
                    dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), e.Item.Cells(3).Text, 0, PaqSel)
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            If arrPol(recorrepoliza) = dt.Rows(i)("id_aseguradora") Then

                                If dt.Rows(i)("total_cob") > 0 Then
                                    validarecorrido = 0

                                    Dim DbRvalor As Double = 0
                                    arrTotal(i) = dt.Rows(i)("total_cob")
                                    Dbvalor = dt.Rows(i)("total_cob")
                                    'esto solo se aplica cuando compramo para mostrar la suma general
                                    'Dim dtR As DataTable = ccliente.restale_carga_cobertura_total_cob_aseguradora_ramo(txt_grdcontrato.Text, Session("bid"), e.Item.Cells(2).Text, dt.Rows(i)("id_aseguradora"), dt.Rows(i)("id_paquete"))
                                    'If dtR.Rows.Count > 0 Then
                                    '    For j = 0 To dtR.Rows.Count - 1
                                    '        If dtR.Rows.Count > 0 Then
                                    '            DbRvalor = dtR.Rows(j)("total_cob")
                                    '        End If
                                    '        Dbvalor = Dbvalor - (DbRvalor * 1.16)
                                    '    Next
                                    'End If

                                    Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(dt.Rows(i)("subramo"), dt.Rows(i)("id_aseguradora"), Session("bid"))
                                    If dtRegion.Rows.Count > 0 Then
                                        If Not dtRegion.Rows(0).IsNull("id_region") Then
                                            intregion = dtRegion.Rows(0)("id_region")
                                        End If
                                    End If

                                    'hp = New HyperLink
                                    'hp.ImageUrl = "..\Imagenes\Menu\arrow_right.gif"
                                    'hp.NavigateUrl = "WfrmCotiza1.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & ""
                                    'e.Item.Cells(cuenta).Controls.Add(hp)
                                    'e.Item.Cells(cuenta).VerticalAlign = VerticalAlign.Bottom

                                    hp = New HyperLink

                                    hp.Text = Dbvalor.ToString("c") & "&nbsp;&nbsp;"
                                    hp.CssClass = "negrita"
                                    hp.ForeColor = New System.Drawing.Color().Black
                                    hp.ToolTip = "Cobertura : " & e.Item.Cells(3).Text & " " & "Aseguradora : " & arrNombrePol(recorrepoliza)

                                    Dbtotales1 = 0
                                    Dbtotales2 = 0
                                    ''nuevo metodo ya que este calculava incorrectamente
                                    'Dbtotales1 = (((Session("resultado")(11) * (1 + Session("ArrPoliza")(14))) / cbo_recargo.SelectedValue) + Session("resultado")(2)) * (1 + Session("subsidios")(4))
                                    'lbl_primero.Text = Dbtotales1.ToString("c")

                                    'Dbtotales2 = (((Session("resultado")(11) * (1 + Session("ArrPoliza")(14))) / cbo_recargo.SelectedValue)) * (1 + Session("subsidios")(4))
                                    'If cbo_recargo.SelectedItem.Value <= 1 Then
                                    '    Dbtotales2 = 0
                                    'End If
                                    'lbl_segundo.Text = Dbtotales2.ToString("c")


                                    If cbo_tipo.SelectedValue > 1 Then
                                        'diferente de contado
                                        Select Case cbo_tipo.SelectedValue
                                            Case 2 'Anual
                                                formaPago = 1
                                            Case 3 'Semestral
                                                formaPago = 4
                                            Case 4 'Trimestral
                                                formaPago = 3
                                            Case 5 'Mensual
                                                formaPago = 2
                                        End Select

                                        'DateAdd(DateInterval.Month, CInt(cbo_plazo.SelectedValue), CDate(txt_fecha.Text)), _
                                        FechaTermina = ccliente.FechaFinalFracciones(txt_fecha.Text, cbo_plazo.SelectedValue)
                                        Dim ds As DataSet

                                        Select Case Session("programa")
                                            Case 25
                                                ds = CdFraccion.FraccionesZurich(txt_fecha.Text, _
                                                    FechaTermina, _
                                                    Session("resultado")(0), Session("ArrPoliza")(13), _
                                                    Session("resultado")(2), Session("resultado")(4), _
                                                    Session("resultado")(1), formaPago, Session("subsidios")(4), _
                                                    (Session("ArrPoliza")(14) * 100))
                                            Case 26
                                                ds = CdFraccion.FraccionesQualitas_Conauto(txt_fecha.Text, _
                                                    FechaTermina, _
                                                    Session("resultado")(0), Session("ArrPoliza")(13), _
                                                    Session("resultado")(2), Session("resultado")(4), _
                                                    Session("resultado")(1), formaPago, Session("subsidios")(4), _
                                                    (Session("ArrPoliza")(14) * 100))
                                            Case 27
                                                ds = CdFraccion.FraccionesGNP_Conauto(txt_fecha.Text, _
                                                    FechaTermina, _
                                                    Session("resultado")(0), Session("ArrPoliza")(13), _
                                                    Session("resultado")(2), Session("resultado")(4), _
                                                    Session("resultado")(1), formaPago, Session("subsidios")(4), _
                                                    (Session("ArrPoliza")(14) * 100))
                                            Case 28
                                                ds = CdFraccion.FraccionesAXA_Conauto(txt_fecha.Text, _
                                                    FechaTermina, _
                                                    Session("resultado")(0), Session("ArrPoliza")(13), _
                                                    Session("resultado")(2), Session("resultado")(4), _
                                                    Session("resultado")(1), formaPago, Session("subsidios")(4), _
                                                    (Session("ArrPoliza")(14) * 100))
                                        End Select

                                        If ds.Tables(0).Rows.Count > 0 Then
                                            For x = 0 To ds.Tables(0).Rows.Count - 1
                                                Select Case x
                                                    Case 0
                                                        Dbtotales1 = ds.Tables(0).Rows(x)("prima_total")
                                                    Case 1
                                                        Dbtotales2 = ds.Tables(0).Rows(x)("prima_total")
                                                        Exit For
                                                End Select
                                            Next
                                        End If
                                    Else
                                        'contado
                                        Dbtotales1 = Session("resultado")(1)
                                        Dbtotales2 = 0
                                    End If
                                    lbl_primero.Text = Dbtotales1.ToString("c")
                                    lbl_segundo.Text = Dbtotales2.ToString("c")


                                    If (Session("nivel") = 0 Or Session("nivel") = 1) Then
                                        hp.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & lbl_plazo.Text & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & dt.Rows(i)("subramo") & "," & cbo_tipo.SelectedValue & "," & cbo_recargo.SelectedValue & "," & Dbtotales1 & "," & Dbtotales2 & "," & Session("Tipocarga") & ")")
                                        'hp.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & cbo_plazo.SelectedValue & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & dt.Rows(i)("subramo") & "," & cbo_tipo.SelectedValue & "," & cbo_recargo.SelectedValue & ")")
                                        hp.Style.Add("cursor", "pointer")
                                        '''hp.NavigateUrl = "WfrmCotiza1.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & " &cveregion=" & intregion & " &cvepaquete=" & dt.Rows(i)("id_paquete") & "&subramo=" & dt.Rows(i)("subramo") & ""


                                        hpImagen = New HyperLink
                                        hpImagen.Text = Dbvalor.ToString("c")
                                        hpImagen.CssClass = "negrita"
                                        hpImagen.ForeColor = New System.Drawing.Color().Black
                                        hpImagen.ToolTip = "Cobertura : " & e.Item.Cells(3).Text & " " & "Aseguradora : " & arrNombrePol(recorrepoliza)
                                        hpImagen.ImageUrl = "..\Imagenes\Menu\printer.png"
                                        hpImagen.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & lbl_plazo.Text & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & dt.Rows(i)("subramo") & "," & cbo_tipo.SelectedValue & "," & cbo_recargo.SelectedValue & "," & Dbtotales1 & "," & Dbtotales2 & "," & Session("Tipocarga") & ")")
                                        'hpImagen.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & cbo_plazo.SelectedValue & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & dt.Rows(i)("subramo") & "," & cbo_tipo.SelectedValue & "," & cbo_recargo.SelectedValue & ")")
                                        ''''hpImagen.NavigateUrl = "WfrmCotiza1.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & " &cveregion=" & intregion & " &cvepaquete=" & dt.Rows(i)("id_paquete") & "&subramo=" & dt.Rows(i)("subramo") & ""
                                        hpImagen.Style.Add("cursor", "pointer")

                                        e.Item.Cells(cuenta).Controls.Add(hp)
                                        e.Item.Cells(cuenta).Controls.Add(hpImagen)
                                    Else
                                        e.Item.Cells(cuenta).Controls.Add(hp)
                                    End If

                                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                    'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                    'e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                                Else
                                    e.Item.Cells(cuenta).Text = "-"
                                    e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                                End If
                            Else
                                validarecorrido = 1
                                e.Item.Cells(cuenta).Text = "-"
                                e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                            End If

                            recorrepoliza = recorrepoliza + 1
                            cuenta = cuenta + 1

                            If validarecorrido = 1 Then
                                i = i - 1
                                validarecorrido = 0
                            End If
                        Next
                    End If
                Else
                    'paquetes especiales
                    For i = 0 To UBound(Session("ArrPolValida"))
                        dt = ccliente.carga_cobertura_total_cob_aseguradora_ramo(Session("idgrdcontrato"), _
                               Session("bid"), e.Item.Cells(2).Text, Session("ArrPolValida")(i))
                        Dbvalor = 0
                        If dt.Rows.Count > 0 Then
                            If dt.Rows(0)("total_cob") > 0 _
                                And e.Item.Cells(12).Text = dt.Rows(0)("cap") _
                                And e.Item.Cells(13).Text = dt.Rows(0)("sumaaseg_cob") Then
                                Dbvalor = dt.Rows(0)("total_cob")
                                If Session("banderafecha") = 1 Then
                                    Dbvalor = Dbvalor * 1.16
                                Else
                                    Dbvalor = Dbvalor * 1.16
                                End If
                            End If
                        End If
                        e.Item.Cells(cuenta).CssClass = "negrita"
                        e.Item.Cells(cuenta).Attributes.Add("align", "center")
                        If Dbvalor <= 0 Then
                            e.Item.Cells(cuenta).Text = ""
                        Else
                            e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                        End If
                        cuenta = cuenta + 1
                    Next
                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            If arrTotal.Length > 0 Then
                e.Item.Cells(6).Text = "Total"
                e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Right
                Dim i As Integer = 0
                Dim cuenta As Integer = 7
                For i = 0 To arrTotal.Length - 1
                    Dbvalor = 0
                    Dbvalor = arrTotal(i)
                    e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                    cuenta = cuenta + 1
                Next
            End If
        End If
        Session("ArrPolValida") = arrPol
    End Sub

    Public Function calculo_general() As String
        Dim arr() As String
        Dim Arr1() As String
        Dim i As Integer = 0
        Dim strcadena As String = ""
        Try
            'Session("tiposeguro"), cbo_plazo.SelectedValue, txt_precio.Text, _
            Ajuste_plazo_calculo()
            strcadena = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                        Session("anio"), Session("modelo"), _
                        1, Session("tipopol"), _
                        Session("uso"), Session("EstatusV"), _
                        0, Session("InterestRate"), txt_fecha.Text, _
                        Session("tiposeguro"), lbl_plazo.Text, txt_precio.Text, _
                        Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                        txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                       Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                       Session("plazoFinanciamiento"), Session("plazoRecargo"), _
                       Session("Tipocarga"))

            If strcadena = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                cmd_cotizar.Visible = False
                Exit Function
            Else
                cmd_cotizar.Visible = False
                Session("CadenaCob") = strcadena
                arr = strcadena.Split("|")
                For i = 0 To arr.Length - 1
                    Arr1 = arr(i).Split("*")
                    If i = 0 Then
                        PaqSel = Arr1(1)
                    Else
                        PaqSel = PaqSel & "," & Arr1(1)
                    End If
                Next
                Session("PaqSelCon") = PaqSel

                Session("Calculos") = ccalculo.CalculoArr
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
                Session("MultArrPolisa") = ccalculo.MultArrPolisa

            End If
            Return strcadena
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    'Format(CDbl(e.Item.Cells(2).Text), "##,##0.00")
    Private Sub cmd_regrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regrear.Click
        Try
            If Session("programa") = 4 Then
                Response.Redirect("..\fscar\WfrmCotizaFscar1.aspx?valcost=" & txt_precio.Text & "")
            Else
                Response.Redirect("WfrmCotiza.aspx?valcost=" & txt_precio.Text & "")
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_cotizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cotizar.Click
        Try
            If Not IsDate(txt_fecha.Text) Then
                MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
                Exit Sub
            End If

            Session("plazo") = lbl_plazo.Text
            Session("plazoSeleccion") = cbo_plazo.SelectedValue
            Session("FechaInicio") = txt_fecha.Text
            Session("precioEscrito") = txt_precio.Text
            Session("paquete") = 0


            Dim arr(19) As String
            arr(0) = Session("moneda")
            arr(1) = Session("anio")
            arr(2) = Session("modelo")
            arr(3) = 1
            arr(4) = Seg_financiado(rb_seguro)
            arr(5) = Session("uso")
            arr(6) = Session("EstatusV")
            arr(7) = 0
            arr(8) = Session("InterestRate")
            arr(9) = txt_fecha.Text
            arr(10) = Session("tiposeguro")
            arr(11) = lbl_plazo.Text
            'arr(11) = cbo_plazo.SelectedValue
            arr(12) = txt_precio.Text
            arr(13) = Session("idgrdcontrato")
            arr(14) = Session("seguro")
            arr(15) = Session("bid")
            arr(16) = txt_sumcob.Text
            arr(17) = Session("vehiculo")
            arr(18) = Session("programa")
            arr(19) = Session("fincon")
            Session("DatosGenerales") = arr

            Response.Redirect("WfrmClientesP.aspx")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Public Sub carga_plazo_financiamiento()
    '    Dim i As Integer = 0
    '    Dim arr As New ArrayList
    '    Dim max As Integer = 60
    '    arr.Add("-- Seleccione --")
    '    For i = 6 To max
    '        arr.Add(i)
    '    Next
    '    cbo_plazof.DataSource = arr
    '    cbo_plazof.DataBind()
    '    cbo_plazof.SelectedIndex = cbo_plazof.Items.IndexOf(cbo_plazof.Items.FindByValue(12))
    'End Sub

    'Public Sub carga_plazo()
    '    Dim i As Integer = 0
    '    Dim arr As New ArrayList
    '    Dim max As Integer = 60
    '    arr.Add("-- Seleccione --")
    '    For i = 6 To max
    '        arr.Add(i)
    '    Next
    '    cbo_plazo.DataSource = arr
    '    cbo_plazo.DataBind()
    '    cbo_plazo.SelectedIndex = cbo_plazof.Items.IndexOf(cbo_plazof.Items.FindByValue(12))
    'End Sub
    Public Sub carga_plazo()
        Session("opcion") = ""
        Dim dt As DataTable = ccalculo.carga_plazo(Session("programa"), Session("opcion"))
        If dt.Rows.Count > 0 Then
            cbo_plazo.DataSource = dt
            cbo_plazo.DataTextField = "plazo"
            cbo_plazo.DataValueField = "id_plazo"
            cbo_plazo.DataBind()
        End If
        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
    End Sub

    Public Sub carga_plazo_TipoPago()
        Dim dt As DataTable = ccliente.Carga_tipoPago(Session("programa"))
        If dt.Rows.Count > 0 Then
            cbo_tipo.DataSource = dt
            cbo_tipo.DataTextField = "descripcion_periodo"
            cbo_tipo.DataValueField = "id_tiporecargo"
            cbo_tipo.DataBind()
        End If
        cbo_tipo.SelectedIndex = cbo_tipo.Items.IndexOf(cbo_tipo.Items.FindByValue(12))
    End Sub

    Public Sub carga_plazo_Recargo(ByVal IdTipo As Integer)
        Dim dt As DataTable = ccliente.Carga_FactorTipoPago(IdTipo)
        If dt.Rows.Count > 0 Then
            cbo_recargo.DataSource = dt
            cbo_recargo.DataTextField = "num_pago"
            cbo_recargo.DataValueField = "cve"
            cbo_recargo.DataBind()
        End If
    End Sub

    Private Sub rb_seguro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_seguro.SelectedIndexChanged
        Dim strCadenaS As String = ""
        Dim i As Integer = 0
        Try
            Seg_financiado(rb_seguro)
            tipo_poliza(lbl_plazo.Text)
            'tipo_poliza(cbo_plazo.SelectedValue)

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_tipo.SelectedIndexChanged
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        If cbo_tipo.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione la forma de pago")
            Exit Sub
        End If
        Try
            Session("plazofinanciamiento") = cbo_tipo.SelectedValue
            carga_plazo_Recargo(cbo_tipo.SelectedValue)
            cbo_recargo.SelectedIndex = cbo_recargo.Items.IndexOf(cbo_recargo.Items.FindByValue(1))
            Session("plazoRecargo") = 1
            'tipo_poliza(cbo_plazo.SelectedValue)
            Seg_financiado(rb_seguro)

            'ccalculo.actualiza_numplazo_cob(cbo_plazo.SelectedValue, Session("idgrdcontrato"), Session("bid"))

            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_recargo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_recargo.SelectedIndexChanged
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        If cbo_plazo.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione el n�mero de pagos")
            Exit Sub
        End If
        Try
            Session("plazoRecargo") = cbo_recargo.SelectedValue
            tipo_poliza(lbl_plazo.Text)
            'tipo_poliza(cbo_plazo.SelectedValue)
            Seg_financiado(rb_seguro)

            ccalculo.actualiza_numplazo_cob(lbl_plazo.Text, Session("idgrdcontrato"), Session("bid"))
            'ccalculo.actualiza_numplazo_cob(cbo_plazo.SelectedValue, Session("idgrdcontrato"), Session("bid"))

            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub Ajuste_plazo_calculo()
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select

        'cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(intPlazofinal))
        lbl_plazo.Text = intPlazofinal
        cbo_recargo.SelectedIndex = cbo_recargo.Items.IndexOf(cbo_recargo.Items.FindByValue(IntPagos))
        Session("plazoFinanciamiento") = cbo_tipo.SelectedValue
        Session("plazoRecargo") = cbo_recargo.SelectedValue
        Session("plazoSeleccion") = cbo_plazo.SelectedValue
        Session("plazo") = lbl_plazo.Text

        tipo_poliza(lbl_plazo.Text)
        ccalculo.actualiza_numplazo_cob(lbl_plazo.Text, Session("idgrdcontrato"), Session("bid"))
    End Sub

    Private Sub cbo_carga_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_carga.SelectedIndexChanged
        Dim strCadenaS As String = ""
        If cbo_carga.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione el tipo de carga")
            Exit Sub
        End If
        Try
            Session("Tipocarga") = cbo_carga.SelectedValue
            Seg_financiado(rb_seguro)

            txt_sumcob.Text = 0
            strCadenaS = calculo_general()
            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            carga_grd()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

#Region "carga tabla dinamica"
    Public Sub Carga_Datos_Dinamicos()
        Dim dth As DataTable
        viewstate("estatus") = Session("EstatusV")
        viewstate("catalogo") = ""

        dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))
        If dth.Rows.Count > 0 Then
            If dth.Rows.Count > 1 Then
                carga_datos_cob_unico_homologada()
            Else
                carga_datos_cob_unico()
            End If
        End If
    End Sub

    Public Sub carga_datos_cob_unico_homologada()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 5
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim celdas As Integer
        Dim nombrePaqueteAux As String = ""
        Dim nombrePaquete As String = ""
        Dim cuenta As Integer = 0
        'Dim strpaquete As String = ""
        Dim totalpaquetes As Integer = 0
        Dim hp As HyperLink
        Dim hpI As HyperLink
        Dim lbl As Label

        tb.BorderWidth = Unit.Pixel(1)
        tb.GridLines = GridLines.Both

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))

            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    If Session("programa") = 9 Then
                        objcell.Text = "COBERTURAS MULTIANUAL"
                    Else
                        objcell.Text = "COBERTURAS DE CONTADO"
                        'objcell.Text = "COBERTURAS"
                    End If
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                    'objcell.Text = "COBERTURAS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                celdas = ccliente.cuenta_paquete_aseguradora_subramo_inicio(Session("bid"), Session("idgrdcontrato"))

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(celdas * 18.75)
                    objcell.ColumnSpan = celdas
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                For i = contador To totalceldas - 1 - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(25)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                    cuenta = cuenta + i
                Next
                tb.Controls.Add(objrow)
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = "COBERTURA"
            objcell.CssClass = "Combos_small_celda"
            objcell.VerticalAlign = VerticalAlign.Middle
            objcell.HorizontalAlign = HorizontalAlign.Center
            objrow.Controls.Add(objcell)

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), _
                    Session("programa"), Session("vehiculo"), Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                Dim ArrNomPaquete(dt.Rows.Count - 1, 1) As String
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        nombrePaqueteAux = ""
                    Else
                        nombrePaqueteAux = dt.Rows(j)("descripcion_paquete")
                        ArrNomPaquete(j, 0) = nombrePaqueteAux
                        ArrNomPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    End If
                    If nombrePaquete <> nombrePaqueteAux Then
                        objcell = New TableCell
                        objcell.Text = nombrePaqueteAux
                        objcell.CssClass = "Combos_small_celda"
                        'objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)
                        nombrePaquete = nombrePaqueteAux
                        totalpaquetes = totalpaquetes + 1
                    End If
                Next
                'For i = 0 To totalceldas - dt.Rows.Count - celdas
                For i = 0 To cuenta
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)

                nombrePaqueteAux = ""
                If dt.Rows.Count > 0 Then
                    Dim dtec As DataTable
                    'strpaquete
                    'dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                    dtec = ccliente.encabezado_coberturas_inicio_homologada(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"))
                    If dtec.Rows.Count > 0 Then
                        Dim arrLimresp(dtec.Rows.Count - 1) As String
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            hp = New HyperLink
                            hpI = New HyperLink
                            lbl = New Label
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                                'objcell.Text = ""
                                hp.Text = ""
                                hpI.Text = ""
                            Else
                                If Session("programa") = 9 Then
                                    strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    objcell.Text = strdescrcobertura
                                Else
                                    strdescrcobertura = dtec.Rows(i)("cobertura")
                                    'objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    hp.Text = dtec.Rows(i)("cobertura") & "&nbsp;&nbsp;&nbsp;"
                                    If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales") Then
                                        lbl.Text = "&nbsp;&nbsp;&nbsp;" & "5%"
                                    End If
                                    If strdescrcobertura = "Robo Total" Then
                                        lbl.Text = "&nbsp;&nbsp;&nbsp;" & "10%"
                                    End If
                                    If Not dtec.Rows(i).IsNull("toltip") Then
                                        hpI.ImageUrl = "..\Imagenes\Menu\add.png"
                                        hp.ToolTip = dtec.Rows(i)("toltip")
                                        hpI.ToolTip = dtec.Rows(i)("toltip")
                                        hp.Style.Add("cursor", "pointer")
                                        hpI.Style.Add("cursor", "pointer")
                                    End If
                                End If
                                If arrLimresp.LastIndexOf(arrLimresp, strdescrcobertura) < 0 Then
                                    arrLimresp(i) = strdescrcobertura
                                Else
                                    GoTo salta
                                End If
                            End If
                            objcell.Controls.Add(hp)
                            objcell.Controls.Add(hpI)
                            If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                objcell.Controls.Add(lbl)
                            End If
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)


                            Dim dtc As DataTable
                            For j = 0 To (ArrNomPaquete.Length / 2) - 1
                                dtc = ccliente.carga_coberturas_inicio_homologada(viewstate("estatus"), _
                                    Session("version"), ArrNomPaquete(j, 1), _
                                    Session("bid"), Session("cotizacionP"), _
                                    arraseg(k), strdescrcobertura, ArrNomPaquete(j, 0), _
                                    Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), _
                                    Session("vehiculo"))
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim deduc As Double = 0
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                If (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") _
                                                    And viewstate("estatus") = "N" Then
                                                    Select Case ccliente.banfactura
                                                        Case 0
                                                            dbdedu = dbdedu * 100
                                                            'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                        Case 1
                                                            dbvalor = Session("precioEscrito")
                                                            objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                        Case 2
                                                            'paneles contado
                                                            If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                                'otrs
                                                                dbdedu = dbdedu * 100
                                                                If ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                                    deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                                            dt.Rows(j)("id_paquete"), _
                                                                            Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                                            1, Session("anio"), _
                                                                            Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                                    'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%, " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo") & ", " & Format(deduc, "$##,##0.#0") & " Deducible"
                                                                Else
                                                                    'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                                End If
                                                            Else
                                                                'contado Paneles
                                                                If Session("EstatusV") = "N" Then
                                                                    dbvalor = Session("precioEscrito")
                                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                                Else
                                                                    objcell.Text = "Valor Comercial"
                                                                End If
                                                            End If
                                                    End Select
                                                Else
                                                    'If Not (Session("programa") = 7 Or Session("programa") = 8 Or Session("programa") = 12 Or Session("programa") = 13) Then
                                                    If Not (strdescrcobertura = "Da�os Materiales" Or strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Robo Total") Then
                                                        If dbdedu < 1 Then
                                                            dbdedu = dbdedu * 100
                                                            'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                            objcell.Text = Math.Round(dbdedu, 2) & "%"
                                                        Else
                                                            'dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                            objcell.Text = Math.Round(dbdedu, 2)
                                                        End If
                                                    Else
                                                        If Session("EstatusV") = "U" Then
                                                            objcell.Text = "Valor Comercial"
                                                        Else
                                                            objcell.Text = "-"
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.#0")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                            Next
                            tb.Controls.Add(objrow)
salta:
                        Next
                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        'objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                        objrow.Controls.Add(objcell)
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_cob_unico()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 5
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim celdas As Integer
        Dim deduc As Double = 0
        Dim nombrePaqueteAux As String = ""
        Dim nombrePaquete As String = ""
        Dim cuenta As Integer = 0
        'Dim strpaquete As String = ""
        Dim totalpaquetes As Integer = 0

        tb.BorderWidth = Unit.Pixel(1)
        tb.GridLines = GridLines.Both

        tb.Controls.Clear()
        Try
            dth = ccliente.Carga_aseguradora_cobertura_inicio(Session("bid"), Session("idgrdcontrato"), Session("version"), viewstate("estatus"))

            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If viewstate("estatus") = "N" Then
                    If Session("programa") = 9 Then
                        objcell.Text = "COBERTURAS MULTIANUAL"
                    Else
                        objcell.Text = "COBERTURAS DE CONTADO"
                    End If
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                celdas = ccliente.cuenta_paquete_aseguradora_subramo_inicio(Session("bid"), Session("idgrdcontrato"))

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(celdas * 18.75)
                    objcell.ColumnSpan = celdas
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                For i = contador To totalceldas - 1 - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(25)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                    cuenta = cuenta + i
                Next
                tb.Controls.Add(objrow)
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = "COBERTURA"
            objcell.CssClass = "Combos_small_celda"
            objcell.VerticalAlign = VerticalAlign.Middle
            objcell.HorizontalAlign = HorizontalAlign.Center
            objrow.Controls.Add(objcell)

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo_inicio(Session("subramo"), viewstate("estatus"), Session("paquete"), _
                    Session("programa"), Session("vehiculo"), Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                Dim ArrNomPaquete(dt.Rows.Count - 1, 1) As String
                For j = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        nombrePaqueteAux = ""
                    Else
                        nombrePaqueteAux = dt.Rows(j)("descripcion_paquete")
                        ArrNomPaquete(j, 0) = nombrePaqueteAux
                        ArrNomPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    End If
                    'If strpaquete = "" Then
                    '    strpaquete = dt.Rows(j)("id_paquete")
                    '    ArrPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    'Else
                    '    strpaquete = strpaquete & "," & dt.Rows(j)("id_paquete")
                    '    ArrPaquete(j, 1) = dt.Rows(j)("id_paquete")
                    'End If
                    If nombrePaquete <> nombrePaqueteAux Then
                        objcell = New TableCell
                        objcell.Text = nombrePaqueteAux
                        objcell.CssClass = "Combos_small_celda"
                        'objcell.ColumnSpan = dth.Rows.Count
                        objcell.VerticalAlign = VerticalAlign.Middle
                        objcell.HorizontalAlign = HorizontalAlign.Center
                        objrow.Controls.Add(objcell)
                        nombrePaquete = nombrePaqueteAux
                        totalpaquetes = totalpaquetes + 1
                    End If
                Next
                'For i = 0 To totalceldas - dt.Rows.Count - celdas
                For i = 0 To cuenta
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)

                nombrePaqueteAux = ""
                If dt.Rows.Count > 0 Then
                    Dim dtec As DataTable
                    'strpaquete
                    dtec = ccliente.encabezado_coberturas_inicio(viewstate("estatus"), 0, Session("version"), Session("bid"), Session("idgrdcontrato"), Session("PaqSelCon"), Session("Tipocarga"))
                    If dtec.Rows.Count > 0 Then
                        Dim arrLimresp(dtec.Rows.Count - 1) As String
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                                objcell.Text = ""
                            Else
                                If Session("programa") = 9 Then
                                    strdescrcobertura = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                    objcell.Text = strdescrcobertura
                                Else
                                    strdescrcobertura = dtec.Rows(i)("cobertura")
                                    objcell.Text = remplaza_cadena(dtec.Rows(i)("cobertura"))
                                End If
                                If arrLimresp.LastIndexOf(arrLimresp, strdescrcobertura) < 0 Then
                                    arrLimresp(i) = strdescrcobertura
                                Else
                                    GoTo salta
                                End If
                            End If
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)

                            'Dim arrPaquete() As String = Split(Session("PaqSelCon"), ",")

                            Dim dtc As DataTable
                            'For j = 0 To IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), totalpaquetes, dt.Rows.Count) - 1

                            'For j = 0 To arrPaquete.Length - 1
                            For j = 0 To (ArrNomPaquete.Length / 2) - 1

                                '''''For j = 0 To totalpaquetes - 1
                                'If Not ArrPaquete(j, 1) Is Nothing Then
                                'Dim dr As DataRow() = dt.Select("id_paquete ='" & ArrPaquete(j, 1) & "'")
                                'If nombrePaqueteAux <> dr(0)("descripcion_paquete") Then
                                'If Session("Programa") = 9 Then
                                'If totalpaquetes > 1 Or Session("Programa") = 9 Then
                                '    nombrePaqueteAux = dr(0)("descripcion_paquete")
                                'End If

                                'dt.Rows(i)("id_paquete")
                                'dt.Rows(i)("descripcion_paquete")

                                'dtc = ccliente.carga_coberturas_inicio(viewstate("estatus"), _
                                'Session("version"), _
                                'IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), strpaquete, dr(0)("id_paquete")), _
                                'Session("bid"), Session("cotizacionP"), _
                                'arraseg(k), strdescrcobertura, dr(0)("descripcion_paquete"), _
                                'Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                dtc = ccliente.carga_coberturas_inicio(viewstate("estatus"), _
                               Session("version"), ArrNomPaquete(j, 1), _
                               Session("bid"), Session("cotizacionP"), _
                               arraseg(k), strdescrcobertura, ArrNomPaquete(j, 0), _
                               Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("idgrdcontrato"))
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                dbdedu = dbdedu * 100
                                                If Session("intmarcabid") = 1 Then
                                                    If (strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Da�os Materiales") And _
                                                        ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                        'deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                        'IIf((dt.Rows.Count > 1 And dt.Rows.Count < 3), strpaquete, dr(0)("id_paquete")), _
                                                        '    Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                        '    1, Session("anio"), _
                                                        '    Session("vehiculo"), Session("resultado")(5))
                                                        deduc = ccliente.determinando_deducpl(arraseg(k), _
                                                            dt.Rows(j)("id_paquete"), _
                                                            Session("subramo"), Session("programa"), viewstate("estatus"), _
                                                            1, Session("anio"), _
                                                            Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                        If Session("tipopol") = "M" Or Session("tipopol") = "F" Then
                                                            Dim strdescripcion As String
                                                            Select Case dtc.Rows(0)("descripcion_tipo")
                                                                Case "Auto Nuevo"
                                                                    strdescripcion = "6"
                                                                Case "Valor Comercial"
                                                                    strdescripcion = "7"
                                                                Case "Valor Factura"
                                                                    strdescripcion = "7"
                                                            End Select
                                                            If dbdedu > 0 Then
                                                                objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%, " & strdescripcion & "% Deducible"
                                                            Else
                                                                objcell.Text = dtc.Rows(0)("descripcion_tipo") & " 0%, " & strdescripcion & "% Deducible"
                                                            End If
                                                        Else
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & " Deducible " & dbdedu & "% PT, " & Format(deduc, "##,##0") & "% PP" ' Deducible
                                                        End If
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                    End If
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & "%"
                                                End If
                                            Else
                                                If Not (strdescrcobertura = "Equipo Especial" Or strdescrcobertura = "Adaptaciones y Conversiones") Then
                                                    If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                        dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                        objcell.Text = Format(dbvalor, "$##,##0.#0")
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                    End If
                                                Else
                                                    objcell.Text = dtc.Rows(0)("deducible")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.#0")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                                'Else
                                'nombrePaqueteAux = dr(0)("descripcion_paquete")
                                'End If
                                'End If
                            Next
                            tb.Controls.Add(objrow)
salta:
                        Next
                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        'objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                        objrow.Controls.Add(objcell)
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function remplaza_cadena(ByVal strcadena As String) As String
        If strcadena = "Gastos Medicos (LUC)" _
        Or strcadena = "Gastos M�dicos (LUC)" _
        Or strcadena = "Gastos Medicos" Then
            'Or strcadena = "Gastos Medicos Ocupantes" Then
            strcadena = "Gastos M�dicos"
        End If
        Return strcadena.Trim
    End Function
#End Region
End Class
