Imports System.Threading
Imports CN_Negocios
Imports Emisor_Conauto
Partial Class WfrmImpPDF
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador

    'Private Delegate Sub EjecutaReporte(ByVal strpoliza As String)
    'Private Delegate Sub EjecutaReporteParametro(ByVal strpoliza As String)

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            'Response.Write(Crea_pdfs())
            Response.Write(Crea_pdf())

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            Response.Flush()
            Response.End()
        End Try

    End Sub

    Public Function Crea_pdf() As String
        Dim strsalida As String = "Exito"
        Dim strpoliza As String = ""
        Dim StrcadenaD As String = ""
        Dim StrCadenaV As String = ""
        Dim Intbid As Integer = 0
        Dim dtPoliza As DataTable
        Dim arr(,) As String
        Dim i As Integer = 0
        Dim strllave As String = ""
        If Session("programa") = 29 Or Session("programa") = 30 Then
            ReDim arr(1, 1)
        Else
            ReDim arr(0, 1)
        End If
        arr = Session("Arrpoliza")
        'Dim strStream As New System.IO.StreamWriter("D:\Aplicaciones\CotizadorContado_Conauto\LogPoliza.txt")

        Try

            For i = 0 To UBound(arr)
                'arr(0, 0) = Session("idpoliza")
                'arr(0, 1) = Session("aseguradora")
                dtPoliza = Nothing
                'dtPoliza = ccliente.carga_reporte_poliza(Session("idpoliza"), Session("bid"), Session("cliente"), Session("idcontrato"), Session("aseguradora"))
                dtPoliza = ccliente.carga_reporte_poliza(arr(i, 0), Session("bid"), Session("cliente"), Session("idcontrato"), arr(i, 1))
                If dtPoliza.Rows.Count > 0 Then
                    If dtPoliza.Rows(0).IsNull("poliza") Then
                        strpoliza = ""
                    Else
                        strpoliza = dtPoliza.Rows(0)("poliza")
                    End If
                    If dtPoliza.Rows(0).IsNull("id_bid") Then
                        Intbid = ""
                    Else
                        Intbid = dtPoliza.Rows(0)("id_bid")
                    End If
                    If dtPoliza.Rows(0).IsNull("cadena_impresionD") Then
                        StrcadenaD = ""
                    Else
                        StrcadenaD = dtPoliza.Rows(0)("cadena_impresionD")
                    End If
                    If dtPoliza.Rows(0).IsNull("cadena_impresionv") Then
                        StrCadenaV = ""
                    Else
                        StrCadenaV = dtPoliza.Rows(0)("cadena_impresionv")
                    End If
                End If

                If Not StrcadenaD = "" Then
                    'CreaPDF.carga(StrcadenaD, "P")
                    'System.Threading.Thread.Sleep(5500)

                    'Dim opp As New System.Diagnostics.Process
                    'opp.StartInfo.FileName = "C:\Inetpub\wwwroot\CotizadorContado_Conauto\Exe\PdFrom\Poliza_Conauto.exe"
                    'opp.StartInfo.FileName = "D:\Aplicaciones\CotizadorContado_Conauto\Exe\PdFrom\Poliza_Conauto.exe"
                    'opp.StartInfo.FileName = ConfigurationManager.AppSettings("RutaExe")
                    'opp.StartInfo.Arguments = StrcadenaD
                    'opp.Start()
                    Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
                    strllave = _bsPolizas.Export(StrcadenaD).ToString()
                    'System.Threading.Thread.Sleep(3500)


                    'strStream.Write("Llave " & strllave)
                    'strStream.Write("Cadena " & StrcadenaD)
                    'strStream.Write("Poliza " & strpoliza)
                    'strStream.Write("Bid " & Intbid)

                    'strllave = ccliente.carga_Contenedor_reporte(strpoliza, Intbid)

                    'strStream.Write("strllave " & strllave)

                    If i = 0 Then
                        Session("llave") = strllave
                        'strStream.Write("Llave1 " & strllave)
                    Else
                        Session("llave") = Session("llave") & "*" & strllave
                        'strStream.Write("Llave2 " & strllave)
                    End If
                    'If Session("llave") = 0 Then
                    If strllave = "" Then
                        strsalida = "Error"
                        'strStream.Write("Llave3 " & strllave)
                    End If
                Else
                    strsalida = "Error"
                    'strStream.Write("Llave4 " & strllave)
                End If
            Next

        Catch ex As Exception
            strsalida = "Error"
        Finally
            'strStream.Close()
        End Try
        Return strsalida
    End Function

End Class
