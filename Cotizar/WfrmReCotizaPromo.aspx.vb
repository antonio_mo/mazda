Imports CN_Negocios


Public Class WfrmReCotizaPromo
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private cprincipal As New CnPrincipal
    Private cvalida As New CnValidacion
    Private ccliente As New CnCotizador
    Private Crecalculo As New CnCalculo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                'HM 06/02/2014 si la clave sale del menu izq es valcio
                If Not Request.QueryString("unico") Is Nothing Then
                    'evi 15/12/2014
                    Session("idpoliza") = Nothing
                End If
                txt_cotiza.Text = ""
                pnl_cotiza.Visible = False
                Session("fincon") = "F"
                If Not Request.QueryString("banCreacion") Is Nothing Then
                    Dim url As String = ""
                    'url = "D:/Aplicaciones/CotizadorContado_Conauto/Aplicaciones/Reporte/Generados/" & Request.QueryString("cvemonbre")
                    url = ConfigurationSettings.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA) & CC.General.Diccionario.Carpetas.GENERADOCOTIZACION & Request.QueryString("cvemonbre")
                    If System.IO.File.Exists(url) Then
                        'sd.FileName = "Cotizacion " & Session("IdCtotizacion")
                        'sd.File = url
                        'sd.ShowDialogBox()

                        Dim nombre As String = "Cotizacion " & Session("IdCtotizacion") & ".pdf"
                        Response.Buffer = False
                        Response.Clear()
                        Response.ClearContent()
                        Response.ClearHeaders()
                        Response.ContentType = "application/pdf"
                        Response.AddHeader("Content-Disposition", "attachment; filename=""" & nombre & "")
                        Response.TransmitFile(url)
                        Response.End()


                    Else
                        Dim _reportePDF As New CC.Negocio.Reporte.N_Cotizacion()
                        _reportePDF.GeneraPromoMazda(Session("IdCotiUnica"))
                        Dim dtImp As DataTable = cprincipal.UrlCotizacion(Session("IdCotiUnica"))
                        If dtImp.Rows.Count > 0 Then
                            If Not dtImp.Rows(0).IsNull("urlcotizacion") Then
                                'url = "D:/Aplicaciones/CotizadorContado_Conauto/Aplicaciones/Reporte/Generados/" & dtImp.Rows(0)("urlcotizacion")
                                url = ConfigurationSettings.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA) & CC.General.Diccionario.Carpetas.GENERADOCOTIZACION & _
                                    dtImp.Rows(0)("urlcotizacion")
                                If System.IO.File.Exists(url) Then
                                    'sd.FileName = "Cotizacion " & Session("IdCtotizacion")
                                    'sd.File = url
                                    'sd.ShowDialogBox()

                                    Dim nombre As String = "Cotizacion " & Session("IdCtotizacion")
                                    Response.Buffer = False
                                    Response.Clear()
                                    Response.ClearContent()
                                    Response.ClearHeaders()
                                    Response.ContentType = "application/pdf"
                                    Response.AddHeader("Content-Disposition", "attachment; filename=""" & nombre & "")
                                    Response.TransmitFile(url)
                                    Response.End()

                                Else
                                    MsgBox.ShowMessage("La cotizacion no se puede regenerar")
                                End If
                            Else
                                MsgBox.ShowMessage("La cotizacion no se puede regenerar")
                            End If
                        Else
                            MsgBox.ShowMessage("La cotizacion no se puede regenerar")
                        End If
                    End If
                End If
                If Not Request.QueryString("cveAseg") Is Nothing Then
                    'nuevo
                    tipo_poliza(Request.QueryString("cveplazo"))
                    Session("EstatusV") = IIf(Session("Exclusion") = 0, "U", "N")
                    Session("primaNeta") = Crecalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                           Request.QueryString("subramo"), Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                           Session("EstatusV"), _
                           0, Session("InterestRate"), Session("FechaIni"), _
                           Session("tiposeguro"), Request.QueryString("cveplazo"), _
                           Request.QueryString("valcost"), _
                           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                           0, Session("vehiculo"), Request.QueryString("cveAseg"), _
                           Request.QueryString("cvepaquete"), Session("programa"), _
                           Session("fincon"), , , Session("CP"), Session("MarcaC"), Session("intmarcabid"), _
                           Request.QueryString("Recargo"), Request.QueryString("NumPago"), _
                           Request.QueryString("TipoCarga"), Session("Exclusion"))
                    'fin cambio micha

                    Session("subsidios") = Crecalculo.subsidios
                    Session("ArrPoliza") = Crecalculo.ArrPolisa
                    Session("resultado") = Crecalculo.Results
                    Session("MultArrPolisa") = Crecalculo.MultArrPolisa

                    Session("DatosGenerales") = ""
                    Dim arr(25) As String
                    arr(0) = Session("moneda")
                    arr(1) = Request.QueryString("subramo")
                    arr(2) = Session("anio")
                    arr(3) = Session("modelo")
                    arr(4) = 1
                    'arr(5) = Seg_financiado(rb_seguro)
                    arr(5) = Seg_financiado_Seleccion(Session("seguro"))
                    arr(6) = Session("uso")
                    arr(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arr(8) = Session("EstatusV")
                    arr(9) = 0
                    arr(10) = Session("InterestRate")
                    arr(11) = Session("FechaIni")
                    arr(12) = Session("tiposeguro")
                    arr(13) = Request.QueryString("cveplazo")
                    arr(14) = txt_precio.Text
                    arr(15) = Session("idgrdcontrato")
                    arr(16) = Session("seguro")
                    arr(17) = Session("bid")
                    arr(18) = 0
                    arr(19) = Session("vehiculo")
                    arr(20) = Request.QueryString("cveAseg")
                    arr(21) = Request.QueryString("cvepaquete")
                    arr(22) = Session("programa")
                    arr(23) = Session("fincon")
                    arr(24) = Request.QueryString("Pago1")
                    arr(25) = Request.QueryString("Pago2")
                    Session("DatosGenerales") = arr
                    Session("CodPostal") = Session("CP")

                    'manolito

                    Session("plazoFinanciamiento") = Request.QueryString("Recargo")
                    Session("plazoRecargo") = Request.QueryString("NumPago")

                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("FechaInicio") = Session("FechaIni")
                    Session("contrato") = Request.QueryString("cvecontrato")
                    Session("precioEscrito") = Request.QueryString("valcost")
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Session("Tipocarga") = Request.QueryString("TipoCarga")

                    'HM 31/10/2013 modificado
                    Session("EstatusV") = "N"
                    calculoaseguradora12meses(Request.QueryString("valcost"), Request.QueryString("TipoCarga"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), Request.QueryString("cveregion"), Request.QueryString("subramo"))

                    Response.Redirect("WfrmCliente.aspx")

                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub calculoaseguradora12meses(ByVal valcost As Double, ByVal TipoCarga As Integer, _
    ByVal strcatalogo As String, ByVal IdRegion As Integer, ByVal subramo As String)

        Dim dt As DataTable = ccliente.Recarga12MesesAseg(Session("IdCotiUnica"))
        If dt.Rows.Count > 0 Then
            Session("primaNeta") = Crecalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                dt.Rows(0)("subramo"), Session("anio"), Session("modelo"), _
                1, "A", _
                Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), _
                Session("EstatusV"), _
                0, Session("InterestRate"), Session("FechaIni"), _
                Session("tiposeguro"), 12, _
                valcost, _
                Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                0, Session("vehiculo"), dt.Rows(0)("Id_Aseguradora"), _
                dt.Rows(0)("idpaquete"), Session("programa"), _
                Session("fincon"), , , , Session("MarcaC"), Session("intmarcabid"), _
                dt.Rows(0)("id_tipoRecargo"), 1, TipoCarga, , , , 0, 1, 0)
            'fin cambio micha



            Session("subsidios1") = Crecalculo.subsidios12
            Session("ArrPoliza1") = Crecalculo.ArrPolisa12
            Session("resultado1") = Crecalculo.Results12
            Session("MultArrPolisa1") = Crecalculo.MultArrPolisa12

            Session("DatosGenerales1") = ""
            Dim arr(25) As String
            arr(0) = Session("moneda")
            arr(1) = subramo
            arr(2) = Session("anio")
            arr(3) = Session("modelo")
            arr(4) = 1
            'arr(5) = Seg_financiado(rb_seguro)
            arr(5) = Seg_financiado_Seleccion(Session("seguro"))
            arr(6) = Session("uso")
            arr(7) = strcatalogo
            arr(8) = Session("EstatusV")
            arr(9) = 0
            arr(10) = Session("InterestRate")
            arr(11) = Session("FechaIni")
            arr(12) = Session("tiposeguro")
            arr(13) = 12
            arr(14) = txt_precio.Text
            arr(15) = Session("idgrdcontrato")
            arr(16) = Session("seguro")
            arr(17) = Session("bid")
            arr(18) = 0
            arr(19) = Session("vehiculo")
            arr(20) = dt.Rows(0)("Id_aseguradora")
            arr(21) = dt.Rows(0)("idpaquete")
            arr(22) = Session("programa")
            arr(23) = Session("fincon")
            arr(24) = dt.Rows(0)("primatotal")
            arr(25) = dt.Rows(0)("primaconsecutiva")
            Session("DatosGenerales1") = arr

            'manolito
            Session("plazo1") = 12
            Session("plazoFinanciamiento1") = dt.Rows(0)("id_tipoRecargo")
            Session("plazoRecargo1") = 1
            Session("aseguradora1") = dt.Rows(0)("Id_aseguradora")
            Session("paquete1") = dt.Rows(0)("idpaquete")
            Session("subramo1") = subramo

        End If
    End Sub

    Public Function Seg_financiado_Seleccion(ByVal intvalor As Integer) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        Else
            If Not intvalor = 0 Then
                tipo = 2
                Session("seguro") = 2
                'Session("tipopol") = "M"
                Session("bUnidadfinanciada") = True
            Else
                tipo = 0
                Session("seguro") = 0
                'Session("tipopol") = "A"
                Session("bUnidadfinanciada") = False
            End If
        End If
        Return tipo
    End Function

    Private Sub cmd_cotizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cotizar.Click
        Dim dt As New DataTable
        Dim dtR As New DataTable
        'Auto Contratado
        Dim IntSubMarcaC As Integer = 0
        Dim strModeloC As String = ""
        Dim IntAnioC As Integer = 0
        Dim IntAonhC As Integer = 0
        'Auto Entregado
        Dim IntSubMarca As Integer = 0
        Dim strModelo As String = ""
        Dim IntAnio As Integer = 0
        Dim IntAonh As Integer = 0
        Dim IdPaquete As Integer = 0
        Dim Idconfirmacion As Integer = 0
        Dim Cuotas As Integer = 0
        Dim MesesTrans As Integer = 0
        Dim MesesRest As Integer = 0

        Try
            If cmd_cotizar.Text = "Buscar" Then
                If Not IsNumeric(txt_cotiza.Text) Then
                    MsgBox.ShowMessage("El n�mero de cotizaci�n debe de ser n�merico")
                    Exit Sub
                End If
                dt = cprincipal.ValidaCotizacion(txt_cotiza.Text, Session("bid"), Session("programa"))
                If dt.Rows.Count > 0 Then
                    'If dt.Rows(0)("confirmacion") = 0 Then
                    If dt.Rows(0)("Emision") = 0 Then
                        'If Session("programa") = 29 And dt.Rows(0)("meses_segurogratis") <> 0 Then
                        Session("IdCotiUnica") = txt_cotiza.Text
                        cmd_cotizar.Text = "Nueva busqueda"
                        pnl_cotiza.Visible = True
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(0))
                        Seg_financiado(rb_seguro)
                        If Not dt.Rows(0).IsNull("Confirmacion") Then
                            Idconfirmacion = dt.Rows(0)("Confirmacion")
                        End If
                        If Not dt.Rows(0).IsNull("Id_Cotizacion12meses") Then
                            Session("idcotizapanel12") = dt.Rows(0)("Id_Cotizacion12meses")
                        End If
                        If Not dt.Rows(0).IsNull("estatus_auto") Then
                            Session("EstatusV") = dt.Rows(0)("estatus_auto")
                        End If
                        If Not dt.Rows(0).IsNull("id_marca") Then
                            IntSubMarca = dt.Rows(0)("id_marca")
                        End If
                        If Not dt.Rows(0).IsNull("Modelo") Then
                            strModelo = dt.Rows(0)("Modelo")
                        End If
                        If Not dt.Rows(0).IsNull("anio") Then
                            IntAnio = dt.Rows(0)("anio")
                        End If
                        If Not dt.Rows(0).IsNull("Id_Aonh") Then
                            IntAonh = dt.Rows(0)("Id_Aonh")
                        End If
                        If Not dt.Rows(0).IsNull("suma_asegurada") Then
                            txt_precio.Text = dt.Rows(0)("suma_asegurada")
                        End If
                        'Auto Contratado
                        If Not dt.Rows(0).IsNull("Modelo_AutoCont") Then
                            strModeloC = dt.Rows(0)("Modelo_AutoCont")
                        End If
                        If Not dt.Rows(0).IsNull("anio_AutoCont") Then
                            IntAnioC = dt.Rows(0)("anio_AutoCont")
                        End If
                        If Not dt.Rows(0).IsNull("Id_AonhAutoCont") Then
                            IntAonhC = dt.Rows(0)("Id_AonhAutoCont")
                        End If
                        If Not dt.Rows(0).IsNull("TipoPersona") Then
                            Session("contribuyente") = dt.Rows(0)("TipoPersona")
                            rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(Session("contribuyente")))
                        End If
                        If Not dt.Rows(0).IsNull("plazo") Then
                            Session("plazoSeleccion") = dt.Rows(0)("plazo")
                        End If
                        If Not dt.Rows(0).IsNull("Restoplazo") Then
                            Session("PlazoTotal") = dt.Rows(0)("plazo")
                        End If
                        If Not dt.Rows(0).IsNull("num_contrato") Then
                            Session("idgrdcontrato") = dt.Rows(0)("num_contrato")
                        End If
                        If Not dt.Rows(0).IsNull("Exclusion") Then
                            Session("Exclusion") = dt.Rows(0)("Exclusion")
                        End If
                        If Not dt.Rows(0).IsNull("CodigoPostal") Then
                            Session("CP") = dt.Rows(0)("CodigoPostal")
                            txtCodigoPostal.Text = Session("CP")
                        End If
                        'se agrega para validar si es o no seguro gratis
                        If Not dt.Rows(0).IsNull("meses_segurogratis") Then
                            If dt.Rows(0)("meses_segurogratis") > 0 Then
                                'Si cuenta con promocion
                                Session("TipoProducto") = 0
                            Else
                                'No cuenta con promocion
                                Session("TipoProducto") = 1
                            End If
                        End If
                        If Not dt.Rows(0).IsNull("Fecha_Entrega") Then
                            Session("FechaIni") = dt.Rows(0)("Fecha_Entrega")
                        Else
                            Session("FechaIni") = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                        End If
                        lbl_fecha.Text = Session("FechaIni")
                        dtR = cprincipal.SubRamoPanel(txt_cotiza.Text)
                        If dtR.Rows.Count > 0 Then
                            If Not dtR.Rows(0).IsNull("subramo") Then
                                ViewState("subramo") = dtR.Rows(0)("subramo")
                            Else
                                ViewState("subramo") = 0
                            End If
                            If Not dtR.Rows(0).IsNull("idpaquete") Then
                                IdPaquete = dtR.Rows(0)("idpaquete")
                            End If
                        End If

                        If Not dt.Rows(0).IsNull("Cuotas_Anticipadas") Then
                            Session("cuotas") = dt.Rows(0)("Cuotas_Anticipadas")
                        End If
                        If Not dt.Rows(0).IsNull("Meses_Transcurridos") Then
                            Session("mesesTrans") = dt.Rows(0)("Meses_Transcurridos")
                        End If


                        If Not dt.Rows(0).IsNull("Meses_SeguroGratis") Then
                            Session("mesesseggratis") = dt.Rows(0)("Meses_SeguroGratis")
                        End If


                        recarga_marca(Session("EstatusV"), IntSubMarca)
                        
                        'Auto Contratado
                        Recarga_modelo(Session("EstatusV"), strModeloC, "C")
                        Recarga_anio(Session("EstatusV"), IntAnioC, "C")
                        recarga_descripcion(Session("EstatusV"), IntAonhC, "C")

                        carga_plazo()
                        Recarga_12Meses(Idconfirmacion)
                        deshabilita()
                        carga_impresion()
                        'Else
                        '    pnl_cotiza.Visible = False
                        '    MsgBox.ShowMessage("La cotizaci�n no necesita confirmaci�n, por lo cual no se podra consultar, gracias")
                        '    Exit Sub
                        'End If
                    Else
                        carga_impresionConfirmacion(txt_cotiza.Text)
                        pnl_cotiza.Visible = False
                        MsgBox.ShowMessage("La cotizaci�n ya fue emitida")
                        Exit Sub
                    End If
                Else
                    pnl_cotiza.Visible = False
                    MsgBox.ShowMessage("El No. de cotizaci�n no existe")
                    Exit Sub
                End If
            Else
                cmd_cotizar.Text = "Buscar"
                pnl_cotiza.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub deshabilita()
        cbo_marca.Enabled = False

        'Auto Entregado
        cbo_modelo.Enabled = False
        cbo_anio.Enabled = False
        cbo_descripcion.Enabled = False
        cbo_uso.Enabled = False
        rb_tipo.Enabled = False
        txt_precio.Enabled = False
        cbo_plazo.Enabled = False
    End Sub

    Private Sub recarga_marca(ByVal StrEstatusAuto As String, ByVal IdSubMarca As Integer)
        Try
            Dim dt As DataTable = ccliente.recarga_marca(Session("intmarcabid"), Session("programa"), StrEstatusAuto, IdSubMarca)
            If dt.Rows.Count > 0 Then
                cbo_marca.DataSource = dt
                cbo_marca.DataTextField = "marca"
                cbo_marca.DataValueField = "id_marca"
                cbo_marca.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Recarga_modelo(ByVal StrEstatusAuto As String, ByVal strmodelo As String, ByVal strTipo As String)
        Dim dt As DataTable = ccliente.Recarga_modelo(cbo_marca.SelectedValue, StrEstatusAuto, Session("programa"), Session("intmarcabid"), strmodelo)
        Try
            If dt.Rows.Count > 0 Then
                cbo_modelo.DataSource = dt
                cbo_modelo.DataTextField = "modelo_vehiculo"
                cbo_modelo.DataValueField = "modelo_vehiculo"
                cbo_modelo.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Recarga_anio(ByVal StrEstatusAuto As String, ByVal IntAnio As Integer, ByVal strTipo As String)
        Dim dt As DataTable = ccliente.Recarga_anio(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, StrEstatusAuto, Session("programa"), Session("intmarcabid"), IntAnio)
        Try
            If dt.Rows.Count > 0 Then
                    cbo_anio.DataSource = dt
                    cbo_anio.DataTextField = "anio_vehiculo"
                    cbo_anio.DataValueField = "anio_vehiculo"
                    cbo_anio.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub recarga_descripcion(ByVal StrEstatusAuto As String, ByVal IntAonh As Integer, ByVal strTipo As String)
        'Dim dt As DataTable = ccliente.Recarga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, StrEstatusAuto, Session("programa"), Session("intmarcabid"), IntAonh)
        Dim dt As DataTable

        Try
            dt = ccliente.Recarga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, StrEstatusAuto, Session("programa"), Session("intmarcabid"), IntAonh)

            If dt.Rows.Count > 0 Then
                cbo_descripcion.DataSource = dt
                cbo_descripcion.DataTextField = "descripcion_vehiculo"
                cbo_descripcion.DataValueField = "id_vehiculo"
                cbo_descripcion.DataBind()

                datos_descripcion(StrEstatusAuto, IntAonh, strTipo)
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub datos_descripcion(ByVal StrEstatusAuto As String, ByVal IntAonh As Integer, ByVal strTipo As String)
        Dim valor As String
        Try
            Dim dth As DataTable = ccliente.Recarga_Catalogo(StrEstatusAuto, Session("programa"), cbo_marca.SelectedValue, Session("intmarcabid"), IntAonh)
            If dth.Rows.Count > 0 Then
                viewstate("catalogo") = ccliente.Catalogo
            Else
                viewstate("catalogo") = 0
            End If

            Dim dt As DataTable = ccliente.carga_uso(Session("programa"), IntAonh, StrEstatusAuto, viewstate("subramo"), Session.SessionID)
            If dt.Rows.Count > 0 Then
                cbo_uso.DataSource = dt
                cbo_uso.DataTextField = "uso"
                cbo_uso.DataValueField = "id_uso"
                cbo_uso.DataBind()
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Particular"))
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_plazo()
        'HM 31/10/2013
        Dim arrP As New ArrayList
        Dim maxP As Integer = 60
        Dim i As Integer = 0
        If Session("PlazoTotal") = 60 Then
            If Session("Exclusion") = 0 Then maxP = 60
        Else
            If Session("Exclusion") = 0 Then maxP = 48
        End If
        arrP.Add("-- Seleccione --")
        For i = 1 To maxP
            arrP.Add(i)
        Next
        cbo_plazo.DataSource = arrP
        cbo_plazo.DataBind()
        'cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(Session("plazoSeleccion")))
        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(Session("PlazoTotal")))
    End Sub

    Private Sub Recarga_12Meses(ByVal Idconfirmacion As Integer)
        Dim i As Integer = 0
        Dim bandera As Integer = 0
        Try
            Session("vehiculo") = cbo_descripcion.SelectedValue
            Session("vehiculoDesc") = cbo_descripcion.SelectedItem.Text
            Session("subramo") = viewstate("subramo")
            Session("anio") = cbo_anio.SelectedItem.Text
            Session("modelo") = cbo_modelo.SelectedValue
            Session("uso") = cbo_uso.SelectedItem.Text

            Dim dt As DataTable = ccliente.Carga_definicion_uso(Session("programa"), viewstate("subramo"), cvalida.QuitaCaracteres(cbo_uso.SelectedItem.Text, True), Session("version"))
            If dt.Rows.Count > 0 Then
                Session("intuso") = dt.Rows(0)("id_uso")
            Else
                Session("intuso") = cbo_uso.SelectedValue
            End If
            Session("catalogo") = IIf(viewstate("catalogo") = "", "0", viewstate("catalogo"))
            Session("MarcaC") = cbo_marca.SelectedValue
            Session("MarcaDesc") = cbo_marca.SelectedItem.Text
            Session("iva") = 0.16
            Session("precioEscrito") = txt_precio.Text
            validaSeguro12()
            Recargacbo_plazo(Idconfirmacion)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub validaSeguro12()
        Ajuste_plazo_calculo(12, 1)
        crearPrima12meses()
    End Sub

    Public Sub Ajuste_plazo_calculo(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, Optional ByVal BanActualiza As Integer = 0)
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select

        ComplementoAjuste(IntCboTipo, intPlazofinal, IntPagos)
    End Sub

    Public Sub ComplementoAjuste(ByVal IntCboTipo As Integer, ByVal intPlazofinal As Integer, ByVal IntPagos As Integer)
        Session("plazoFinanciamiento") = IntCboTipo
        Session("plazoRecargo") = IntPagos
        Session("plazoSeleccion") = cbo_plazo.SelectedValue
        Session("plazo") = cbo_plazo.SelectedValue
        tipo_poliza(cbo_plazo.SelectedValue)
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        valor = IIf(valor <= 12, 1, 0)
        If Session("multianual") = 1 Or valor = 0 Then
            If valor = 0 Then
                Session("tipopol") = "M"
            Else
                Session("tipopol") = "A"
            End If
        Else
            Session("tipopol") = "A"
        End If
    End Sub

    Public Sub crearPrima12meses()
        Dim dt12 As New DataTable
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim i, j As Integer
        Dim dbMonto As Double = 0
        Dim strcadena As String = ""

        tb_seguro.Controls.Clear()
        Try
            'Contratado
            dt12 = ccliente.recargaDatos12meses(txt_cotiza.Text, "C")
            If dt12.Rows.Count > 0 Then
                encabenzado12mesesC()
                For i = 0 To dt12.Rows.Count - 1
                    If Not dt12.Rows(i).IsNull("Id_Aseguradora") Then
                        tbrow = New TableRow
                        dbMonto = CDbl(dt12.Rows(i)("PT"))
                        If dt12.Rows(i)("bandera") = 1 Then
                            'aseguradora mas barata
                            Session("12meses") = dt12.Rows(i)("Id_Aseguradora")
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If
                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = dt12.Rows(i)("descripcion_aseguradora")
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                    End If
                Next
            End If

            'Entregado
            dt12 = ccliente.recargaDatos12meses(txt_cotiza.Text, "E")
            If dt12.Rows.Count > 0 Then
                encabenzado12meses()
                For i = 0 To dt12.Rows.Count - 1
                    If Not dt12.Rows(i).IsNull("Id_Aseguradora") Then
                        tbrow = New TableRow
                        dbMonto = CDbl(dt12.Rows(i)("PT"))
                        If dt12.Rows(i)("bandera") = 1 Then
                            'aseguradora mas barata
                            Session("12meses") = dt12.Rows(i)("Id_Aseguradora")
                            strcadena = "..\Imagenes\general\This.gif"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "I", 10, ""))
                        Else
                            strcadena = "&nbsp;"
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 10, ""))
                        End If
                        'HM 13/02/2014 solo administradores
                        If Session("nivel") = 0 Then
                            strcadena = Format(dbMonto, "$ #,###,##0.00")
                            tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 40, ""))
                        End If

                        strcadena = dt12.Rows(i)("descripcion_aseguradora")
                        tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 2, "G", 40, ""))

                        tb_seguro.Controls.Add(tbrow)
                    End If
                Next
            End If
            'HM 31/10/2013 validamos promocion Si/No
            If Session("Exclusion") = 0 Then
                tb_seguro.Visible = True
            Else
                tb_seguro.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub encabenzado12mesesC()
        Dim tbcell As TableCell
        Dim tbrow As New TableRow

        tbcell = New TableCell
        tbcell.Text = "Seguro gratis un a�o"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(100)
        'HM 13/02/2014 solo administradores
        If Session("nivel") = 0 Then
            tbcell.ColumnSpan = 3
        Else
            tbcell.ColumnSpan = 2
        End If
        tbrow.Controls.Add(tbcell)
    End Sub

    Public Sub encabenzado12meses()
        Dim tbcell As TableCell
        Dim tbrow As New TableRow

        tbcell = New TableCell
        tbcell.Text = "Seguro gratis un a�o"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(100)
        'HM 13/02/2014 solo administradores
        If Session("nivel") = 0 Then
            tbcell.ColumnSpan = 3
        Else
            tbcell.ColumnSpan = 2
        End If
        tbrow.Controls.Add(tbcell)
        tb_seguro.Controls.Add(tbrow)
    End Sub

    Public Function agrega_celdas(ByVal strcadena As String, ByVal strclase As String, ByVal intalinea As Integer, _
    ByVal tipoFormato As String, ByVal tamano As Double, Optional ByVal Comentario As String = "") As TableCell
        'intalinea = 
        '1 = izquierda
        '2 = centro
        '3 = derecha
        Dim tbcell As New TableCell
        Dim hp As HyperLink
        Dim Himage As HtmlImage

        Select Case tipoFormato
            Case "G"
                tbcell.Text = strcadena
            Case "I"
                Himage = New HtmlImage
                Himage.Src = strcadena
                tbcell.Controls.Add(Himage)
            Case "H"
                'hp = New HyperLink
                'hp.Text = Comentario
                'hp.NavigateUrl = strcadena
                'tbcell.Controls.Add(hp)
        End Select
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.CssClass = strclase
        tbcell.HorizontalAlign = intalinea
        tbcell.Width = Unit.Percentage(tamano)
        Return tbcell
    End Function

    Private Sub Recargacbo_plazo(ByVal Idconfirmacion As Integer)
        Dim i As Integer = 0
        Session("plazoSeleccion") = cbo_plazo.SelectedValue
        Seg_financiado(rb_seguro)

        Ajuste_plazo_calculo(12, 1)
        crearPrima12meses()

        calculo_general_aseguradora(Idconfirmacion)
    End Sub

    Public Sub calculo_general_aseguradora(ByVal Idconfirmacion As Integer)
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim ArrAseg(4, 7) As String
        Dim dtV As New DataTable
        Dim dt As New DataTable
        Dim i, j As Integer
        Try

            dt = ccliente.ReCargaCotizaPanelAseguradora(txt_cotiza.Text)
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    dtV = ccliente.ReCargaCotizaPanel(txt_cotiza.Text, dt.Rows(i)("id_aseguradora"))
                    If dtV.Rows.Count > 0 Then
                        For j = 0 To dtV.Rows.Count - 1
                            If Not dtV.Rows(j).IsNull("Id_TipoRecargo") Then
                                ArrAseg(j, 0) = dtV.Rows(j)("Id_TipoRecargo")
                            End If
                            If Not dtV.Rows(j).IsNull("PT") Then
                                ArrAseg(j, 1) = dtV.Rows(j)("PT")
                            End If
                            If Not dtV.Rows(j).IsNull("PTC") Then
                                ArrAseg(j, 2) = dtV.Rows(j)("PTC")
                            End If
                            If Not dtV.Rows(j).IsNull("subramo") Then
                                ArrAseg(j, 3) = dtV.Rows(j)("subramo")
                            End If
                            If Not dtV.Rows(j).IsNull("IdPaquete") Then
                                ArrAseg(j, 4) = dtV.Rows(j)("IdPaquete")
                            End If
                            If Not dtV.Rows(j).IsNull("PTC2") Then
                                ArrAseg(j, 5) = dtV.Rows(j)("PTC2")
                            End If
                            If Not dtV.Rows(j).IsNull("url_imagenpeque") Then
                                ArrAseg(j, 6) = dtV.Rows(j)("url_imagenpeque")
                            End If
                            If Not dtV.Rows(j).IsNull("descripcion_aseguradora") Then
                                ArrAseg(j, 7) = dtV.Rows(j)("descripcion_aseguradora")
                            End If
                        Next j
                    End If

                    tbrow = New TableRow
                    tbcell = New TableCell
                    tbcell.ColumnSpan = IIf(Session("programa") = 1, 5, 3)
                    tbcell.Controls.Add(encabenzadoPaquetes(dt.Rows(i)("id_aseguradora"), Session("programa"), ArrAseg))
                    tbcell.Controls.Add(encabenzadoAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, 0, Idconfirmacion))
                    tbcell.Controls.Add(DetalleAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, 0))
                    tbcell.Controls.Add(EmisionAseguradora(dt.Rows(i)("id_aseguradora"), ArrAseg, Session("programa")))
                    tbcell.Controls.Add(encabenzadoAseguradora_vacio())
                    
                    tbrow.Controls.Add(tbcell)
                    tb_aseguradora.Controls.Add(tbrow)
                Next i
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function encabenzadoPaquetes(ByVal IdAseguradora As Integer, ByVal iPrograma As Integer, ByVal Arr As Object) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(IIf(iPrograma = 1, 25, 45))
        tbrow.Controls.Add(tbcell)
        Dim ccalculo As New CnCalculo

        Dim dt As DataTable = ccalculo.CargaPaquete(IdAseguradora)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_paquete")
                lbl.Width = Unit.Percentage(100)
                tbcell.Controls.Add(lbl)

                hp = New HyperLink
                hp.Text = "Detalle cobertura "
                hp.Attributes.Add("onclick", "Cobertura(" & IdAseguradora & "," & dt.Rows(i)("id_paquete") & ",'" & Session("modelo") & "','" & Session("CP") & "'," & txt_precio.Text & ")")
                hp.Style.Add("cursor", "pointer")
                tbcell.Controls.Add(hp)

                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "Interiro_tabla_centro"
                tbcell.Width = Unit.Percentage(IIf(iPrograma = 1, 30, 27))
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function

    Public Function encabenzadoAseguradora_vacio() As Table
        Dim tb As New Table
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.ColumnSpan = IIf(Session("programa") = 1, 4, 2)
        tbcell.Text = "&nbsp;"
        tbrow.Controls.Add(tbcell)
        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function encabenzadoAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object, ByVal k As Integer, ByVal Idconfirmacion As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = ""
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = Arr(k, 7)
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        Dim dt As DataTable = Crecalculo.CargaTipoRecargo(IdAseguradora, Session("programa"))
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                If dtRegion.Rows.Count > 0 Then
                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                        IdRegion = dtRegion.Rows(0)("id_region")
                    End If
                End If

                'NumPagos = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 0)
                NumPagos = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 0)
                'HM 08/01/2014 se agrega el plazo del producto
                'NumPlazo = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 1)
                NumPlazo = total_Pagos(cbo_plazo.SelectedValue, dt.Rows(i)("id_tiporecargo"), 1)

                
                hp = New HyperLink
                hp.ToolTip = "Seleccione la opci�n que desea emitir"
                hp.ImageUrl = "..\Imagenes\Menu\printer.png"
                hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & NumPlazo & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                hp.Style.Add("cursor", "pointer")
                'tbcell.Controls.Add(hp)

                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;" & dt.Rows(i)("descripcion_periodo")
                tbcell.Controls.Add(lbl)
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "Interiro_tabla_centro"
                tbcell.Width = Unit.Percentage(15)
                tbrow.Controls.Add(tbcell)
            Next
        End If

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    Public Function DetalleAseguradora(ByVal IdASeguradora As Integer, ByVal Arr As Object, ByVal k As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim strcadena As String = ""
        Dim Himage As HtmlImage
        Dim iPrograma As Integer = Session("programa")

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.RowSpan = IIf(iPrograma = 2, 4, 6)
        'tbcell.Text = "&nbsp;"
        Himage = New HtmlImage
        Himage.Src = Arr(k, 6)
        tbcell.Controls.Add(Himage)
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.CssClass = "negrita"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        For i = 0 To 5
            If (Not (i = 2 And iPrograma = 2) And Not (i = 3 And iPrograma = 2)) Then
                tbcell = New TableCell
                Select Case i
                    Case 0
                        strcadena = "Meses de seguro"
                    Case 1
                        strcadena = "No. De pagos"
                        tbrow = New TableRow
                    Case 2
                        strcadena = "Primer Recibo"
                        tbrow = New TableRow
                    Case 3
                        strcadena = "Recibo Subsecuente"
                        tbrow = New TableRow
                    Case 4
                        strcadena = "Prima total"
                        tbrow = New TableRow
                    Case 5
                        strcadena = "Pago Mensual Aprox"
                        tbrow = New TableRow
                End Select
                tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 1, "G", 15, ""))

                For x As Integer = 0 To IIf(iPrograma = 1, 3, 1)
                    tbcell = New TableCell
                    Select Case i
                        Case 0
                            strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 1)
                        Case 1
                            strcadena = total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 0)
                        Case 2
                            If (CDbl(Arr(x, 2)) = CDbl(Arr(x, 1))) Then
                                strcadena = "NA"
                            Else
                                If (x Mod 2) <> 0 And x <> 0 Then
                                    strcadena = Format(CDbl(Arr(x, 2)), "$ #,###,##0.00")
                                Else
                                    strcadena = String.Empty
                                End If
                            End If
                        Case 3
                            If (CDbl(Arr(x, 2)) = CDbl(Arr(x, 1))) Then
                                strcadena = "NA"
                            Else
                                If (x Mod 2) <> 0 And x <> 0 Then
                                    strcadena = Format(CDbl(Arr(x, 5)), "$ #,###,##0.00")
                                Else
                                    strcadena = String.Empty
                                End If
                            End If
                        Case 4
                            strcadena = Format(CDbl(Arr(x, 1)), "$ #,###,##0.00")
                        Case 5
                            strcadena = Format(CDbl(Arr(x, 1) / total_Pagos(cbo_plazo.SelectedValue, Arr(x, 0), 1)), "$ #,###,##0.00")
                    End Select
                    tbrow.Controls.Add(agrega_celdas(strcadena, "negrita", 3, "G", 15, ""))
                Next

                tb.Controls.Add(tbrow)
            End If
        Next

        tb.Width = Unit.Percentage(100)
        Return tb
    End Function

    'HM 08/01/2014 se agrega para pagos y plazo
    'Bantipo = 0 pagos
    'Bantipo = 1 plazo
    Public Function total_Pagos(ByVal IntCboPlazo As Integer, ByVal IntCboTipo As Integer, ByVal Bantipo As Integer) As Integer
        Dim dbajuste As Double = 0
        Dim intplazoIni As Integer = IntCboPlazo ' cbo_plazo.SelectedValue
        Dim intPlazofinal As Integer
        Dim IntPagos As Integer
        Select Case IntCboTipo 'cbo_tipo.SelectedValue
            Case 1 'contado
                dbajuste = intplazoIni / intplazoIni
                intPlazofinal = Int(dbajuste) * intplazoIni
                IntPagos = 1
            Case 2 'anual
                dbajuste = intplazoIni / 12
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 12), ((Int(dbajuste) + 1) * 12))
                IntPagos = intPlazofinal / 12
            Case 3 'semestral
                dbajuste = intplazoIni / 6
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 6), ((Int(dbajuste) + 1) * 6))
                IntPagos = intPlazofinal / 6
            Case 4 'trimestral
                dbajuste = intplazoIni / 3
                intPlazofinal = IIf(Int(dbajuste) - dbajuste = 0, (Int(dbajuste) * 3), ((Int(dbajuste) + 1) * 3))
                IntPagos = intPlazofinal / 3
            Case 5 'mensual
                dbajuste = intplazoIni / 1
                intPlazofinal = Int(dbajuste) * 1
                IntPagos = intPlazofinal / 1
        End Select
        If Bantipo = 0 Then
            Return IntPagos
        Else
            Return intPlazofinal
        End If
    End Function

    Public Function Seg_financiado(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        End If
        Return tipo
    End Function

    Public Sub carga_impresion()
        Dim tbrow As TableRow
        Dim tbcell As TableCell
        Dim hp As HyperLink
        Dim hp1 As HyperLink
        Try
            Dim dt As DataTable = cprincipal.UrlCotizacion(Session("IdCotiUnica"))
            If dt.Rows.Count > 0 Then
                tbcell = New TableCell
                tbrow = New TableRow
                hp = New HyperLink
                hp1 = New HyperLink

                hp.Text = "ReImprimir Cotizaci�n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                hp.NavigateUrl = "WfrmReCotizaPromo.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                hp.Target = "contenido"
                tbcell.Controls.Add(hp)

                hp1.Text = "ReImprimir Cotizaci�n"
                hp1.ImageUrl = "..\imagenes\general\acrobat.png"
                hp1.NavigateUrl = "WfrmReCotizaPromo.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                hp1.Target = "contenido"
                tbcell.Controls.Add(hp1)
                tbcell.Width = Unit.Percentage(100)
                tbrow.Controls.Add(tbcell)
                tb_imp.Controls.Add(tbrow)
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_impresionConfirmacion(ByVal IdCotizacion As Integer)
        Dim tbrow As TableRow
        Dim tbcell As TableCell
        Dim hp As HyperLink
        Dim hp1 As HyperLink
        Try
            Dim dt As DataTable = cprincipal.UrlCotizacion(IdCotizacion)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("urlcotizacion") Then
                    tbcell = New TableCell
                    tbrow = New TableRow
                    hp = New HyperLink
                    hp1 = New HyperLink

                    hp.Text = "ReImprimir Cotizaci�n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    hp.NavigateUrl = "WfrmReCotizaPromo.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                    hp.Target = "contenido"
                    tbcell.Controls.Add(hp)

                    hp1.Text = "ReImprimir Cotizaci�n"
                    hp1.ImageUrl = "..\imagenes\general\acrobat.png"
                    hp1.NavigateUrl = "WfrmReCotizaPromo.aspx?banCreacion=1&cvemonbre=" & dt.Rows(0)("urlcotizacion") & ""
                    hp1.Target = "contenido"
                    tbcell.Controls.Add(hp1)
                    tbcell.Width = Unit.Percentage(100)
                    tbrow.Controls.Add(tbcell)
                    tb_impConfirma.Controls.Add(tbrow)
                Else
                    regeneracotizacion(IdCotizacion)
                End If
            Else
                regeneracotizacion(IdCotizacion)
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub regeneracotizacion(ByVal IdCotizacion As Integer)
        Dim _reportePDF As New CC.Negocio.Reporte.N_Cotizacion()
        _reportePDF.GeneraPromoMazda(IdCotizacion)
        carga_impresionConfirmacion(IdCotizacion)
    End Sub

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Function EmisionAseguradora(ByVal IdAseguradora As Integer, ByVal Arr As Object, ByVal iPrograma As Integer) As Table
        Dim tb As New Table
        Dim i As Integer = 0
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim hp As HyperLink
        Dim lbl As Label
        Dim IdRegion As Integer = 0
        Dim NumPagos As Integer = 0
        Dim NumPlazo As Integer = 0

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "&nbsp;"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = ""
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "&nbsp;" 'ccliente.DescrAseg
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        'tbcell.CssClass = "Interiro_tabla_centro"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        Dim ccalculo As New CnCalculo
        Dim dt As DataTable = ccalculo.CargaTipoRecargo(IdAseguradora, iPrograma)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                tbcell = New TableCell

                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(Arr(i, 3), IdAseguradora, Session("bid"))
                If dtRegion.Rows.Count > 0 Then
                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                        IdRegion = dtRegion.Rows(0)("id_region")
                    End If
                End If
                dtRegion.Clear()

                NumPagos = total_Pagos(cbo_plazo.Text, dt.Rows(i)("id_tiporecargo"), 0)
                'HM 08/01/2014 se agrega el plazo del producto
                NumPlazo = total_Pagos(cbo_plazo.Text, dt.Rows(i)("id_tiporecargo"), 1)

                If (Session("nivel") = 0 Or Session("nivel") = 1 Or Session("nivel") = 4) Then

                    hp = New HyperLink
                    hp.ToolTip = "Seleccione la opci�n que desea emitir"
                    hp.Text = "Emitir p�liza "
                    If (Not Arr(i, 2) Is Nothing) Then
                        hp.Attributes.Add("onclick", "redireccion(" & IdAseguradora & "," & cbo_plazo.Text & "," & IdRegion & "," & Arr(i, 4) & "," & Arr(i, 3) & "," & dt.Rows(i)("id_tiporecargo") & "," & NumPagos & "," & Arr(i, 2) & "," & Arr(i, 5) & ")")
                        hp.Style.Add("cursor", "pointer")
                        hp.Width = Unit.Percentage(100)
                        tbcell.Controls.Add(hp)
                    End If
                End If
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "AzulSub"
                tbcell.Width = Unit.Percentage(15)
                tbrow.Controls.Add(tbcell)
            Next
        End If
        dt.Clear()

        tb.Controls.Add(tbrow)
        tb.Width = Unit.Percentage(100)
        Return tb

    End Function
End Class
