<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wfrmImprime.aspx.vb" Inherits="Cotizador_Mazda_Retail.wfrmImprime" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="2%" height="20" class="negrita_marco_color" vAlign="middle" align="center">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="90%" colSpan="2"><FONT size="+0">&nbsp;Impresión de 
							Documentos</FONT></TD>
					<TD width="8%" class="Interiro_tabla" align="right">
						<asp:LinkButton id="lk_regresa" runat="server" CssClass="negrita">Regresar</asp:LinkButton></TD>
				</TR>
			</TABLE>
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 102; LEFT: 312px; POSITION: absolute; TOP: 448px" runat="server"></cc1:MsgBox>
			<ontranet:SaveDialog id="sd" runat="server"></ontranet:SaveDialog>
			<TABLE id="Table1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="3">
						<asp:HyperLink id="Lnk_imprime" runat="server" Width="100%" CssClass="negrita"></asp:HyperLink></TD>
				</TR>
				<TR>
					<TD colSpan="3" height="10"></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<asp:HyperLink id="Lnk_imprime1" runat="server" CssClass="negrita" Width="100%" Visible="False"></asp:HyperLink></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<asp:HyperLink id="Lnk_imprime2" runat="server" CssClass="negrita" Width="100%" Visible="False"></asp:HyperLink></TD>
				</TR>
				<TR>
					<TD colSpan="3"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
