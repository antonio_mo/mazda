﻿Imports System
Imports System.IO
Imports System.Xml.Serialization
Imports System.Web
Imports CN_Negocios
Imports System.Web.Script.Serialization
Imports System.Linq

Public Class cotizaPromo

    Implements System.Web.IHttpHandler, System.Web.SessionState.IReadOnlySessionState


    Public Shared valor As String = "N"
    Public Shared ccliente As New CnCotizador

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/plain"

        Dim method As String = context.Request.Params("p")

        Dim method_desc As String = "["

        If (context.Session("click_menu") = True) Then

            clearSession(context)
            context.Session("click_menu") = False

        End If

        setValuesSession(context, method)

        method_desc &= "{""sel"" : """ & context.Session("v_m_c") & """,""m_c"":"
        method_desc &= GetMarca(context)

        If context.Session("v_m_c") <> Nothing Or context.Session("v_m_c") <> "" And context.Session("t_m_c") <> Nothing Or context.Session("t_m_c") <> "" Then

            'If context.Session("v_t_p_c") <> Nothing Or context.Session("v_t_p_c") <> "" And context.Session("t_t_p_c") <> Nothing Or context.Session("t_t_p_c") <> "" Then
            method_desc &= "},{""sel"" : """ & context.Session("v_t_p_c") & """,""t_p_c"":"
            method_desc &= getModelo(context, context.Session("v_m_c"))
            'End If

            If (context.Session("v_t_p_c") <> Nothing Or context.Session("v_t_p_c") <> "") Or (context.Session("t_t_p_c") <> Nothing Or context.Session("t_t_p_c") <> "") Then

                method_desc &= "},{""sel"" : """ & context.Session("v_m_l_c") & """,""m_l_c"":"
                method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p_c"))

                If context.Session("v_m_l_c") <> Nothing Or context.Session("v_m_l_c") <> "" And context.Session("t_m_l_c") <> Nothing Or context.Session("t_m_l_c") <> "" Then

                    'If context.Session("v_d_s_c") <> Nothing Or context.Session("v_d_s_c") <> "" And context.Session("t_d_s_c") <> Nothing Or context.Session("t_d_s_c") <> "" Then
                    method_desc &= "},{""sel"" : """ & context.Session("v_d_s_c") & """,""d_s_c"":"
                    method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p_c"), context.Session("v_m_l_c"))
                    'End If

                    'If context.Session("v_u_s_c") <> Nothing Or context.Session("v_u_s_c") <> "" And context.Session("t_u_s_c") <> Nothing Or context.Session("t_u_s_c") <> "" Then
                    method_desc &= "},{""sel"" : """ & context.Session("v_u_s_c") & """,""u_s_c"":"
                    method_desc &= getUso(context, context.Session("v_d_s_c"))
                    'End If

                Else
                    context.Session("v_d_s_c") = Nothing
                    method_desc &= "},{""sel"" : """",""d_s_c"":{}"
                    method_desc &= "},{""sel"" : """",""u_s_c"":{}"
                End If
                'Else
                '    method_desc &= "},{""sel"" : """",""t_p_c"":"
                '    method_desc &= getModelo(context, context.Session("v_m_c"))
            End If

            'If context.Session("v_t_p") <> Nothing Or context.Session("v_t_p") <> "" And context.Session("t_t_p") <> Nothing Or context.Session("t_t_p") <> "" Then
            method_desc &= "},{""sel"" : """ & context.Session("v_t_p") & """,""t_p"":"
            method_desc &= getModelo(context, context.Session("v_m_c"))
            'End If

            If (context.Session("v_t_p") <> Nothing Or context.Session("v_t_p") <> "") Or (context.Session("t_t_p") <> Nothing Or context.Session("t_t_p") <> "") Then

                method_desc &= "},{""sel"" : """ & context.Session("v_m_l") & """,""m_l"":"
                method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p"))

                If context.Session("v_m_l") <> Nothing Or context.Session("v_m_l") <> "" And context.Session("t_m_l") <> Nothing Or context.Session("t_m_l") <> "" Then

                    'If context.Session("v_d_s") <> Nothing Or context.Session("v_d_s") <> "" And context.Session("t_d_s") <> Nothing Or context.Session("t_d_s") <> "" Then
                    method_desc &= "},{""sel"" : """ & context.Session("v_d_s") & """,""d_s"":"
                    method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p"), context.Session("v_m_l"))
                    'End If

                    'If context.Session("v_u_s") <> Nothing Or context.Session("v_u_s") <> "" And context.Session("t_u_s") <> Nothing Or context.Session("t_u_s") <> "" Then
                    method_desc &= "},{""sel"" : """ & context.Session("v_u_s") & """,""u_s"":"
                    method_desc &= getUso(context, context.Session("v_d_s"))
                    'End If

                Else
                    context.Session("v_d_s") = Nothing
                    method_desc &= "},{""sel"" : """",""d_s"":{}"
                    method_desc &= "},{""sel"" : """",""u_s"":{}"
                End If
                'Else
                '    method_desc &= "},{""sel"" : """",""m_l"":"
                '    method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p"))
            End If

        End If










        'Select Case method

        '    Case "m_c"
        '        method_desc &= "{""sel"" : """",""m_c"":"
        '        method_desc &= GetMarca(context)
        '    Case "t_p"
        '        method_desc &= "{""tipo"" : ""t_p"",""data"":"
        '        method_desc &= getModelo(context, context.Session("v_m_c"))
        '    Case "t_p_c"
        '        method_desc &= "{""tipo"" : ""t_p_c"",""data"":"
        '        method_desc &= getModelo(context, context.Session("v_m_c"))
        '    Case "m_l"
        '        method_desc &= "{""tipo"" : ""m_l"",""data"":"
        '        method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p"))
        '    Case "m_l_c"
        '        method_desc &= "{""tipo"" : ""m_l_c"",""data"":"
        '        method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p_c"))
        '    Case "d_s"
        '        method_desc &= "{""tipo"" : ""d_s"",""data"":"
        '        method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p"), context.Session("v_m_l"))
        '    Case "d_s_c"
        '        method_desc &= "{""tipo"" : ""d_s_c"",""data"":"
        '        method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p_c"), context.Session("v_m_l_c"))
        '    Case "u_s"
        '        method_desc &= "{""tipo"" : ""u_s"",""data"":"
        '        method_desc &= getUso(context, context.Session("v_d_s"))

        '        Dim dth As DataTable = ccliente.carga_subramo_homologado(context.Session("v_d_s"), valor, context.Session("programa"), context.Session("v_m_c"), context.Session("intmarcabid"))
        '        If dth.Rows.Count > 0 Then
        '            context.Session("subramo") = ccliente.subramo
        '            context.Session("catalogo") = ccliente.Catalogo
        '        Else
        '            context.Session("subramo") = 0
        '            context.Session("catalogo") = 0
        '        End If

        '    Case "u_s_c"
        '        method_desc &= "{""tipo"" : ""u_s_c"",""data"":"
        '        method_desc &= getUso(context, context.Session("v_d_s_c"))

        '    Case "f_l"

        '        If context.Session("v_m_c") <> Nothing Or context.Session("v_m_c") <> "" And context.Session("t_m_c") <> Nothing Or context.Session("t_m_c") <> "" Then
        '            method_desc &= "{""sel"" : """ & context.Session("v_m_c") & """,""m_c"":"
        '            method_desc &= GetMarca(context)
        '        Else
        '            method_desc &= "{""sel"" : """",""m_c"":"
        '            method_desc &= GetMarca(context)
        '        End If

        '        If context.Session("v_t_p") <> Nothing Or context.Session("v_t_p") <> "" And context.Session("t_t_p") <> Nothing Or context.Session("t_t_p") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_t_p") & """,""t_p"":"
        '            method_desc &= getModelo(context, context.Session("v_m_c"))
        '        End If

        '        If context.Session("v_t_p_c") <> Nothing Or context.Session("v_t_p_c") <> "" And context.Session("t_t_p_c") <> Nothing Or context.Session("t_t_p_c") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_t_p_c") & """,""t_p_c"":"
        '            method_desc &= getModelo(context, context.Session("v_m_c"))
        '        End If

        '        If context.Session("v_m_l") <> Nothing Or context.Session("v_m_l") <> "" And context.Session("t_m_l") <> Nothing Or context.Session("t_m_l") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_m_l") & """,""m_l"":"
        '            method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p"))
        '        End If

        '        If context.Session("v_m_l_c") <> Nothing Or context.Session("v_m_l_c") <> "" And context.Session("t_m_l_c") <> Nothing Or context.Session("t_m_l_c") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_m_l_c") & """,""m_l_c"":"
        '            method_desc &= getAnio(context, context.Session("v_m_c"), context.Session("v_t_p_c"))
        '        End If

        '        If context.Session("v_d_s") <> Nothing Or context.Session("v_d_s") <> "" And context.Session("t_d_s") <> Nothing Or context.Session("t_d_s") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_d_s") & """,""d_s"":"
        '            method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p"), context.Session("v_m_l"))
        '        End If

        '        If context.Session("v_d_s_c") <> Nothing Or context.Session("v_d_s_c") <> "" And context.Session("t_d_s_c") <> Nothing Or context.Session("t_d_s_c") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_d_s_c") & """,""d_s_c"":"
        '            method_desc &= getDescripcion(context, context.Session("v_m_c"), context.Session("v_t_p_c"), context.Session("v_m_l_c"))
        '        End If

        '        If context.Session("v_u_s") <> Nothing Or context.Session("v_u_s") <> "" And context.Session("t_u_s") <> Nothing Or context.Session("t_u_s") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_u_s") & """,""u_s"":"
        '            method_desc &= getUso(context, context.Session("v_d_s"))
        '        End If

        '        If context.Session("v_u_s_c") <> Nothing Or context.Session("v_u_s_c") <> "" And context.Session("t_u_s_c") <> Nothing Or context.Session("t_u_s_c") <> "" Then
        '            method_desc &= "},{""sel"" : """ & context.Session("v_u_s_c") & """,""u_s_c"":"
        '            method_desc &= getUso(context, context.Session("v_d_s_c"))
        '        End If

        '    Case "c_c"
        '        method_desc = "{""msg"" : ""Operación generada satisfactoriamente""}"
        '    Case "c_p"
        '        method_desc = "{""msg"" : ""Operación generada satisfactoriamente""}"
        '    Case Else
        '        method_desc = "The parameters provided are incorrect"
        'End Select

        If (method_desc.StartsWith("[{") And (method_desc.EndsWith("}]") Or method_desc.EndsWith("{}"))) Then
            method_desc &= "}]"
        End If

        context.Response.Write(method_desc)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub clearSession(ByVal context As HttpContext)

        context.Session("v_m_c") = "" '("v_m_c")
        context.Session("t_m_c") = "" '("t_m_c")
        context.Session("v_t_p") = "" '("v_t_p")
        context.Session("t_t_p") = "" '("t_t_p")
        context.Session("v_t_p_c") = "" '("v_p_c")
        context.Session("t_t_p_c") = "" '("t_p_c")
        context.Session("v_m_l") = "" '("v_m_l")
        context.Session("t_m_l") = "" '("t_m_l")
        context.Session("v_m_l_c") = "" '("v_m_l_c")
        context.Session("t_m_l_c") = "" '("t_m_l_c")
        context.Session("v_d_s") = "" '("v_d_s")
        context.Session("t_d_s") = "" '("t_d_s")
        context.Session("v_d_s_c") = "" '("v_d_s_c")
        context.Session("t_d_s_c") = "" '("t_d_s_c")
        context.Session("v_u_s") = 1 '("v_u_s")
        context.Session("t_u_s") = "Particular" '("t_u_s")
        context.Session("v_u_s_c") = 1 '("v_u_s_c")
        context.Session("t_u_s_c") = "Particular" '("t_u_s_c")

    End Sub

    Public Sub setValuesSession(ByVal context As HttpContext, ByVal method As String)


        Dim passed_values() As String = context.Request.Params.ToString().Split("&")

        If (context.Session("IsPostBack") = False) Then

            If (context.Request.Params("v_m_c") <> "" Or context.Request.Params("t_m_c") <> "") Then

                context.Session("v_m_c") = context.Request.Params("v_m_c")
                context.Session("t_m_c") = context.Request.Params("t_m_c")

            End If



            If (context.Request.Params("v_t_p") <> "" And context.Request.Params("t_t_p") <> "") Then
                context.Session("v_t_p") = context.Request.Params("v_t_p")
                context.Session("t_t_p") = context.Request.Params("t_t_p")
            End If

            If (context.Request.Params("v_m_l") <> "" And context.Request.Params("t_m_l") <> "") Then
                context.Session("v_m_l") = context.Request.Params("v_m_l")
                context.Session("t_m_l") = context.Request.Params("t_m_l")
            End If

            If (context.Request.Params("v_d_s") <> "" And context.Request.Params("t_d_s") <> "") Then
                context.Session("v_d_s") = context.Request.Params("v_d_s")
                context.Session("t_d_s") = context.Request.Params("t_d_s")

                Dim dth As DataTable = ccliente.carga_subramo_homologado(context.Session("v_d_s"), valor, context.Session("programa"), context.Session("v_m_c"), context.Session("intmarcabid"))
                If dth.Rows.Count > 0 Then
                    context.Session("subramo") = ccliente.subramo
                    context.Session("catalogo") = ccliente.Catalogo
                Else
                    context.Session("subramo") = 0
                    context.Session("catalogo") = 0
                End If

            End If

            If (context.Request.Params("v_u_s") <> "" And context.Request.Params("t_u_s") <> "") Then

                context.Session("v_u_s") = context.Request.Params("v_u_s")
                context.Session("t_u_s") = context.Request.Params("t_u_s")

            End If



            If (context.Request.Params("v_t_p_c") <> "" And context.Request.Params("t_t_p_c") <> "") Then
                context.Session("v_t_p_c") = context.Request.Params("v_t_p_c")
                context.Session("t_t_p_c") = context.Request.Params("t_t_p_c")
            End If

            If (context.Request.Params("v_m_l_c") <> "" And context.Request.Params("t_m_l_c") <> "") Then
                context.Session("v_m_l_c") = context.Request.Params("v_m_l_c")
                context.Session("t_m_l_c") = context.Request.Params("t_m_l_c")
            End If

            If (context.Request.Params("v_d_s_c") <> "" And context.Request.Params("t_d_s_c") <> "") Then
                context.Session("v_d_s_c") = context.Request.Params("v_d_s_c")
                context.Session("t_d_s_c") = context.Request.Params("t_d_s_c")


                Dim dth = ccliente.carga_subramo_homologado(context.Session("v_d_s_c"), valor, context.Session("programa"), context.Session("v_m_c"), context.Session("intmarcabid"))
                If dth.Rows.Count > 0 Then
                    context.Session("subramo") = ccliente.subramo
                    context.Session("catalogo") = ccliente.Catalogo
                Else
                    context.Session("subramo") = 0
                    context.Session("catalogo") = 0
                End If
            End If

            If (context.Request.Params("v_u_s_c") <> "" And context.Request.Params("t_u_s_c") <> "") Then
                context.Session("v_u_s_c") = context.Request.Params("v_u_s_c")
                context.Session("t_u_s_c") = context.Request.Params("t_u_s_c")

            End If

        Else
            context.Session("IsPostBack") = False

        End If
    End Sub

    'Public Sub setValuesSession(ByVal cx As HttpContext, ByVal method As String)


    '    If method <> "f_l" Then

    '        Dim passed_values() As String = cx.Request.Params.ToString().Split("&")

    '        If cx.Request.Params("v_m_c") <> Nothing Or cx.Request.Params("v_m_c") <> "" Then
    '            cx.Session("v_m_c") = cx.Request.Params("v_m_c")
    '        End If
    '        If cx.Request.Params("t_m_c") <> Nothing Or cx.Request.Params("t_m_c") <> "" Then
    '            cx.Session("t_m_c") = cx.Request.Params("t_m_c")
    '        End If

    '        If cx.Request.Params("v_t_p") <> Nothing Or cx.Request.Params("v_t_p") <> "" Then
    '            cx.Session("v_t_p") = cx.Request.Params("v_t_p")
    '        End If
    '        If cx.Request.Params("t_t_p") <> Nothing Or cx.Request.Params("t_t_p") <> "" Then
    '            cx.Session("t_t_p") = cx.Request.Params("t_t_p")
    '        End If

    '        If cx.Request.Params("v_t_p_c") <> Nothing Or cx.Request.Params("v_t_p_c") <> "" Then
    '            cx.Session("v_t_p_c") = cx.Request.Params("v_t_p_c")
    '        End If
    '        If cx.Request.Params("t_t_p_c") <> Nothing Or cx.Request.Params("t_t_p_c") <> "" Then
    '            cx.Session("t_t_p_c") = cx.Request.Params("t_t_p_c")
    '        End If

    '        If cx.Request.Params("v_m_l") <> Nothing Or cx.Request.Params("v_m_l") <> "" Then
    '            cx.Session("v_m_l") = cx.Request.Params("v_m_l")
    '        End If
    '        If cx.Request.Params("t_m_l") <> Nothing Or cx.Request.Params("t_m_l") <> "" Then
    '            cx.Session("t_m_l") = cx.Request.Params("t_m_l")
    '        End If

    '        If cx.Request.Params("v_m_l_c") <> Nothing Or cx.Request.Params("v_m_l_c") <> "" Then
    '            cx.Session("v_m_l_c") = cx.Request.Params("v_m_l_c")
    '        End If
    '        If cx.Request.Params("t_m_l_c") <> Nothing Or cx.Request.Params("t_m_l_c") <> "" Then
    '            cx.Session("t_m_l_c") = cx.Request.Params("t_m_l_c")
    '        End If

    '        If cx.Request.Params("v_d_s") <> Nothing Or cx.Request.Params("v_d_s") <> "" Then
    '            cx.Session("v_d_s") = cx.Request.Params("v_d_s")
    '        End If
    '        If cx.Request.Params("t_d_s") <> Nothing Or cx.Request.Params("t_d_s") <> "" Then
    '            cx.Session("t_d_s") = cx.Request.Params("t_d_s")
    '        End If

    '        If cx.Request.Params("v_d_s_c") <> Nothing Or cx.Request.Params("v_d_s_c") <> "" Then
    '            cx.Session("v_d_s_c") = cx.Request.Params("v_d_s_c")
    '        End If
    '        If cx.Request.Params("t_d_s_c") <> Nothing Or cx.Request.Params("t_d_s_c") <> "" Then
    '            cx.Session("t_d_s_c") = cx.Request.Params("t_d_s_c")
    '        End If

    '        If cx.Request.Params("v_u_s") <> Nothing Or cx.Request.Params("v_u_s") <> "" Then
    '            cx.Session("v_u_s") = cx.Request.Params("v_u_s")
    '        End If
    '        If cx.Request.Params("t_u_s") <> Nothing Or cx.Request.Params("t_u_s") <> "" Then
    '            cx.Session("t_u_s") = cx.Request.Params("t_u_s")
    '        End If

    '        If cx.Request.Params("v_u_s_c") <> Nothing Or cx.Request.Params("v_u_s_c") <> "" Then
    '            cx.Session("v_u_s_c") = cx.Request.Params("v_u_s_c")
    '        End If
    '        If cx.Request.Params("t_u_s_c") <> Nothing Or cx.Request.Params("t_u_s_c") <> "" Then
    '            cx.Session("t_u_s_c") = cx.Request.Params("t_u_s_c")
    '        End If

    '    End If

    'End Sub

    Private Shared Function GetMarca(ByVal context As HttpContext) As String

        Try
            Dim dt As DataTable = ccliente.carga_marca(context.Session("intmarcabid"), context.Session("programa"), valor)
            If dt.Rows.Count > 0 Then
                dt.Columns(0).ColumnName = "optionValue"
                dt.Columns(1).ColumnName = "optionDisplay"
                dt.AcceptChanges()
                Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            Else
                Return "{}"
            End If
        Catch ex As Exception
            Return "{}"
        End Try

    End Function

    Private Shared Function getModelo(ByVal context As HttpContext, ByVal intmarca As Integer) As String
        Try
            Dim dt As DataTable = ccliente.carga_modelo(intmarca, valor, context.Session("programa"), context.Session("intmarcabid"))
            If dt.Rows.Count > 0 Then
                dt.Columns(0).ColumnName = "optionValue"
                dt.Columns(1).ColumnName = "optionDisplay"
                dt.AcceptChanges()
                Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            Else
                Return "{}"
            End If
        Catch ex As Exception
            Return "{}"
        End Try
    End Function

    Private Shared Function getAnio(ByVal context As HttpContext, ByVal intmarca As Integer, ByVal strmodelo As String) As String
        Try
            Dim dt As DataTable = ccliente.carga_anio(intmarca, strmodelo, valor, context.Session("programa"), context.Session("intmarcabid"))
            If dt.Rows.Count > 0 Then
                dt.Columns(0).ColumnName = "optionValue"
                dt.Columns(1).ColumnName = "optionDisplay"
                dt.AcceptChanges()
                Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            Else
                Return "{}"
            End If
        Catch ex As Exception
            Return "{}"
        End Try
    End Function

    Private Shared Function getDescripcion(ByVal context As HttpContext, ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String) As String
        Try
            Dim dt As DataTable = ccliente.carga_descripcion(intmarca, strmodelo, stranio, valor, context.Session("programa"), context.Session("intmarcabid"))
            If dt.Rows.Count > 0 Then
                dt.Columns(0).ColumnName = "optionValue"
                dt.Columns(1).ColumnName = "optionDisplay"
                dt.AcceptChanges()
                Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            Else
                Return "{}"
            End If
        Catch ex As Exception
            Return "{}"
        End Try
    End Function

    Private Shared Function getUso(ByVal context As HttpContext, ByVal cboDesc As String) As String ', ByVal idTipo As Integer
        Try
            'If (idTipo = 0) Then
            '    Return "[]"
            'End If
            Dim dt As DataTable = ccliente.carga_uso(context.Session("programa"), cboDesc, valor, context.Session("subramo"), context.Session.SessionID)
            If dt.Rows.Count > 0 Then
                dt.Columns(0).ColumnName = "optionValue"
                dt.Columns(1).ColumnName = "optionDisplay"
                dt.AcceptChanges()
                Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            Else
                Return "{}"
            End If
        Catch ex As Exception
            Return "{}"
        End Try
    End Function

End Class