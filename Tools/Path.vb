﻿Imports System.Configuration

Namespace CotizadorContado_Conauto

    Public Class Path

        Public Shared Function GetValidUrl(url As String) As String
            Dim SSL As String = ConfigurationManager.AppSettings("SSL")
            url = url.Replace("http", SSL)
            Return IIf(Not url.EndsWith("/"), url & "/", url)
        End Function

    End Class

End Namespace
