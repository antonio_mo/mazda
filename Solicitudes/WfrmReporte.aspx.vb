Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Windows
Imports Daimler_Solicitudes.WsDaimlerSolicitud
Imports CN_Negocios
'Imports Microsoft.Office.Interop.Excel
Partial Class WfrmReporte
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private wsconsulta As New WsDaimlerSolicitud.WsConexionSoapClient

    Private cprincipal As New CnPrincipal
    Private cvalidacion As New CnValidacion

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    'Introducir aqu� el c�digo de usuario para inicializar la p�gina
    '    Dim parametros() As String
    '    Dim Archivo As String

    '    If Not Page.IsPostBack Then
    '        If Not Request.QueryString("rep") Is Nothing Then
    '            parametros = Split(Request.QueryString("rep"), "|")
    '            Archivo = generarReporteExcel(CInt(IIf(parametros(0) = "", 0, parametros(0))), parametros(1), parametros(2), parametros(3), parametros(4), parametros(5), parametros(6))
    '        End If
    '    End If


    'End Sub

    'Private Sub MatarProceso(ByVal MainWindowHandle As IntPtr, ByVal Caption As String)
    '    SetLastError(0)
    '    ' for Excel versions <10, this won't be set yet
    '    If IntPtr.Equals(MainWindowHandle, IntPtr.Zero) Then
    '        MainWindowHandle = FindWindow(Nothing, Caption)
    '    End If
    '    If IntPtr.Equals(MainWindowHandle, IntPtr.Zero) Then
    '        Exit Sub ' at this point, presume the window has been closed.
    '    End If
    '    Dim iRes, iProcID As Integer
    '    iRes = GetWindowThreadProcessId(MainWindowHandle, iProcID)
    '    If iProcID = 0 Then ' can�t get Process ID
    '        If EndTask(MainWindowHandle) <> 0 Then Exit Sub ' success
    '        Throw New ApplicationException("Failed to close.")
    '    End If
    '    Dim proc As System.Diagnostics.Process
    '    proc = System.Diagnostics.Process.GetProcessById(iProcID)
    '    proc.CloseMainWindow()
    '    proc.Refresh()
    '    If proc.HasExited Then Exit Sub
    '    proc.Kill()
    'End Sub

    'Declare Function EndTask Lib "user32.dll" (ByVal hWnd As IntPtr) As Integer
    'Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" _
    '       (ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    'Declare Function GetWindowThreadProcessId Lib "user32.dll" _
    '       (ByVal hWnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer
    'Declare Function SetLastError Lib "kernel32.dll" (ByVal dwErrCode As Integer) As IntPtr

    'Private Sub liberar_ObjetoCOM(ByRef objeto As Object)
    '    Try
    '        While System.Runtime.InteropServices.Marshal.ReleaseComObject(objeto) > 0
    '        End While
    '        objeto = Nothing
    '    Catch ex As Exception
    '        objeto = Nothing
    '    Finally
    '        GC.Collect()
    '        GC.WaitForPendingFinalizers()
    '    End Try
    'End Sub

    'Private Function generarReporteExcel(ByVal IdSolicitud As Integer, ByVal TipoSolicitud As String, ByVal Poliza As String, _
    'ByVal Serie As String, ByVal Contrato As String, ByVal NombreCliente As String, ByVal Aseguradora As String) As String
    '    Dim i, j As Integer
    '    Dim s As String = String.Empty
    '    Dim nombreArchivo As String = String.Empty
    '    Dim rutaArchivo, rutaAcceso As String

    '    Dim window As New Microsoft.Office.Interop.Excel.Application
    '    Dim libro As Workbook

    '    Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
    '    ''Para evitar error "Old format or invalid type library."
    '    System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")


    '    window.Visible = False
    '    window.DisplayAlerts = False
    '    window.Interactive = False
    '    window.Caption = System.Guid.NewGuid.ToString.ToUpper()
    '    s = window.Caption

    '    Try
    '        ''Rutas de disco y web            
    '        rutaArchivo = cprincipal.rutaArchivo
    '        rutaAcceso = cprincipal.rutaReporte

    '        Dim parametro As New DatosEntrada

    '        parametro.Usuario = "WSDAIMLER"
    '        parametro.Password = "SOLICITUDES"
    '        parametro.TipoPerfil = Session("Perfil")
    '        'parametro.IdSolOracle = 0            '0 = solo para insercion, otro valor > 0 es actualizaci�n
    '        parametro.Solicitud = 4

    '        IdSolicitud = IIf(IdSolicitud < 0, 0, IdSolicitud)
    '        parametro.IdSolOracle = IIf(IdSolicitud = "", 0, IdSolicitud)
    '        parametro.TipoSolicitud = TipoSolicitud
    '        parametro.NoPoliza = cvalidacion.QuitaCaracteres(Poliza)
    '        parametro.Serie = cvalidacion.QuitaCaracteres(Serie)
    '        parametro.Contrato = cvalidacion.QuitaCaracteres(Contrato)
    '        parametro.Cliente = cvalidacion.QuitaCaracteres(NombreCliente)
    '        'parametro.User = UCase(Session("nombre"))
    '        parametro.Aseguradora = cvalidacion.QuitaCaracteres(Aseguradora)

    '        Dim dt As DataTable = wsconsulta.WsTraerInformacion(parametro).Tables(0)
    '        If dt Is Nothing Then
    '            MsgBox.ShowMessage("No se tiene informaci�n")
    '            Exit Function
    '        Else
    '            If dt.rows.Count > 0 Then
    '                nombreArchivo = "Solicitud_" & Session("IdSolicitud") & "_" & DateTime.Now.Ticks.ToString()

    '                libro = window.Workbooks.Add()
    '                ''Poner Envcabezado
    '                libro.Worksheets(1).Cells(1, 1).Value = "Reporte de Solicitudes"
    '                libro.Worksheets(1).Cells(1, 1).Resize(, 23).Merge()
    '                libro.Worksheets(1).Cells(1, 1).Font.Bold = True
    '                libro.Worksheets(1).Cells(1, 1).HorizontalAlignment = -4108

    '                libro.Worksheets(1).Cells(2, 1).Value = "Id_Solicitud"
    '                libro.Worksheets(1).Cells(2, 2).Value = "Tipo Solicitud"
    '                libro.Worksheets(1).Cells(2, 3).Value = "P�liza"
    '                libro.Worksheets(1).Cells(2, 4).Value = "Serie"
    '                libro.Worksheets(1).Cells(2, 5).Value = "Contrato"
    '                libro.Worksheets(1).Cells(2, 6).Value = "Nombre del Cliente"
    '                libro.Worksheets(1).Cells(2, 7).Value = "Aseguradora"
    '                libro.Worksheets(1).Cells(2, 8).Value = "Fecha Solicitado"
    '                libro.Worksheets(1).Cells(2, 9).Value = "Status"
    '                libro.Worksheets(1).Cells(2, 10).Value = "Fecha Status"
    '                libro.Worksheets(1).Cells(2, 11).Value = "Quien Solicita"
    '                libro.Worksheets(1).Cells(2, 12).Value = "Nivel Servicio a Status"
    '                libro.Worksheets(1).Cells(2, 13).Value = "Fecha Cerrado"
    '                libro.Worksheets(1).Cells(2, 14).Value = "Nivel Servicio Pendiente"
    '                libro.Worksheets(1).Cells(2, 15).Value = "Nivel Servicio Tr�mite"
    '                libro.Worksheets(1).Cells(2, 16).Value = "Nivel Servicio Cierre"

    '                ''Coloca valores de las solicitudes
    '                For j = 0 To dt.Rows.Count - 1
    '                    'libro.Worksheets(1).Cells(j + 3, i + 1).NumberFormat = "@"
    '                    'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), DateTime).ToString("dd/MM/yyyy")
    '                    'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), Decimal).ToString("$ #,##0.00")
    '                    'libro.Worksheets(1).Cells(j + 3, i + 1).Value = dr(i) ''& " dias"
    '                    'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), CComun.EstatusSolicitud).ToString()

    '                    libro.Worksheets(1).Cells(j + 3, 1).Value = ""
    '                    libro.Worksheets(1).Cells(j + 3, 2).Value = ""
    '                    libro.Worksheets(1).Cells(j + 3, 3).Value = ""
    '                    libro.Worksheets(1).Cells(j + 3, 4).Value = ""

    '                Next

    '                ''Formatear celdas
    '                libro.Worksheets(1).Columns.AutoFit()
    '                libro.Worksheets(1).UsedRange().Borders.LineStyle = 1
    '                libro.Worksheets(1).UsedRange().Borders.Weight = 2
    '                libro.Worksheets(1).UsedRange().Borders.Color = RGB(0, 0, 0)
    '                'libro.Worksheets(1).PageSetup.PrintArea = "A1:X" & (j + 3)
    '                libro.Worksheets(1).PageSetup.Orientation = 2
    '                ''Dim hoja As Excel.Worksheet = libro.Worksheets(1)

    '            End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '        Throw ex
    '    Finally
    '        ''Termina proceso de libro de excel
    '        If (Not libro Is Nothing) Then
    '            Dim ws As Worksheet
    '            For Each ws In libro.Worksheets
    '                System.Runtime.InteropServices.Marshal.ReleaseComObject(ws)
    '                ws = Nothing
    '            Next
    '            libro.Close(True, rutaArchivo & nombreArchivo & ".xls")
    '            Me.liberar_ObjetoCOM(libro)
    '        End If
    '        If (Not window Is Nothing) Then
    '            window.Workbooks.Close()
    '            window.Quit()
    '            Me.liberar_ObjetoCOM(window)
    '        End If
    '        ''Mata proceso de excel en caso de quedar activo
    '        Dim iHandle As IntPtr = IntPtr.Zero
    '        Me.MatarProceso(iHandle, s)
    '    End Try
    '    Return nombreArchivo
    'End Function
End Class
