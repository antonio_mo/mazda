Imports Daimler_Solicitudes.WsDaimlerSolicitud
Imports CN_Negocios
Imports System.Threading
Imports Cotizador_Mazda_Retail.WsDaimlerSolicitud

Partial Class WfrmConsulta
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cmbRegresar As System.Web.UI.WebControls.Button
    Protected WithEvents panRequisitos As System.Web.UI.WebControls.Panel

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private wsconsulta As New WsDaimlerSolicitud.WsConexionSoapClient

    Private cprincipal As New S_CnPrincipal
    Private cvalidacion As New S_CnValidacion
    Private IdSolicitud As Integer
    Private TipoSolicitud As String
    Private Poliza As String
    Private Serie As String
    Private Contrato1 As String
    Private Contrato2 As String
    Private NombreCliente As String
    Private Aseguradora As String
    Private Usuario As String
    Private Status As String
    Private iduser As String
    Dim paramIsUser As String 'valor del parametro de la sesion

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Introducir aqu� el c�digo de usuario para inicializar la p�gina
            Dim Nombre As String
            Dim Liga As String

            'Session.Timeout = 180   este session timeout se quita porque se establecio desde la pagina de login 



            'lblTitulo.Text = "SOLICITUDES DE MOVIMIENTOS DE FINANCIAMIENTO AUTOMOTRIZ"
            'lblTitulo2.Text = "SOLICITUDES DE MOVIMIENTOS DE FINANCIAMIENTO AUTOMOTRIZ"

            'lblUsuarioI.Text = "Usuario: " & Session("nombre")
            'lblUsuarioI.CssClass = "NegritaMediana"
            'lblNivel.Text = "Perfil: " & Session("nombre_nivel")
            'lblNivel.CssClass = "NegritaMediana"

            lblSubtitulo.Text = "CONSULTA"
            lblSubTituloM.Text = "CONSULTA"

            lblUsuarioII.Text = "Usuario: " & Session("nombre")
            lblUsuarioII.CssClass = "NegritaMediana"
            lblNivel2.Text = "Perfil: " & Session("nombre_nivel")
            lblNivel2.CssClass = "NegritaMediana"
            If Session("Idusuario") = 1 Then
                btnUsers.Visible = True
            End If

            Controles()
            CargarMenu()

            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            'parametro.Funcionalidad = Session("FuncionalidadC")
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.Solicitud = 6
            parametro.IdSolOracle = 0   'default
            parametro.Id_Bid = Session("Bid")

            If Not Page.IsPostBack Then

                'If Session("BidAon") Is Nothing Then
                Dim _bsBid As New CC.Negocio.Administracion.BID()
                Session("BidAon") = _bsBid.GetBid(Session("Bid")).Bid_Aon
                'End If

                ''Seccion agregada para controlar una solo si la sesion sigue activa
                Dim userId As String
                userId = Session("Idusuario")
                iduser = IIf(userId Is Nothing, Request.QueryString("var"), userId)
                If Me.nomLogin.Value = String.Empty Then
                    Me.nomLogin.Value = IIf(iduser Is Nothing, "0", iduser) ' ****Fin de session
                End If
                paramIsUser = "var=" & Me.nomLogin.Value
                ValidaSesion()
                'fin de seccion

                cmbFuncionalidad.Items.Clear()
                cmbFuncionalidad.Items.Add("SOLICITUDES")
                cmbFuncionalidad.Enabled = False

                cmdExcel.Visible = False
                Session("IdArchivoSolicitud") = Nothing

                'Obtiene la informaci�n para el tipo de solicitud
                Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If ds Is Nothing Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    Dim dt As DataTable = ds.Tables(0)
                    If dt.Rows.Count > 0 Then
                        cmbTipoSolicitud.Items.Clear()
                        cmbTipoSolicitud.DataSource = dt
                        cmbTipoSolicitud.DataTextField = "TIPOSOLICITUD"
                        cmbTipoSolicitud.DataValueField = "TIPOSOLICITUD"
                        cmbTipoSolicitud.DataBind()
                    End If
                End If

                'Estatus
                parametro.Solicitud = 8
                Dim dsE As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If dsE Is Nothing Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    Dim dt As DataTable = dsE.Tables(0)
                    If dt.Rows.Count > 0 Then
                        cmbEstatus.DataSource = dt
                        cmbEstatus.DataTextField = "STATUS"
                        cmbEstatus.DataValueField = "STATUS"
                        cmbEstatus.DataBind()
                    End If
                End If

                hpReporte.Visible = False
                pnlConsulta.Visible = True
                pnlModificacion.Visible = False

                parametro = New WsDaimlerSolicitud.DatosEntrada

                parametro.Usuario = "WSDAIMLER"
                parametro.Password = "SOLICITUDES"
                parametro.TipoPerfil = Session("nombre_nivel")
                parametro.Solicitud = 11
                parametro.IdSolOracle = 0   'default


                Session("ConsultaCliente") = False
                Dim dsAseguradora As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If dsAseguradora Is Nothing Then
                    MsgBox.ShowMessage("No se tienen aseguradoras")
                    Exit Sub
                Else
                    Dim dtAseguradora As DataTable = dsAseguradora.Tables(0)

                    If dtAseguradora.Rows.Count > 0 Then
                        ddlAseguradora.Items.Clear()
                        ddlAseguradora.DataSource = dtAseguradora
                        ddlAseguradora.DataTextField = "abreviatura"
                        ddlAseguradora.DataValueField = "value"
                        ddlAseguradora.DataBind()

                        ddlAseguradoraM.Items.Clear()
                        ddlAseguradoraM.DataSource = dtAseguradora
                        ddlAseguradoraM.DataTextField = "abreviatura"
                        ddlAseguradoraM.DataValueField = "value"
                        ddlAseguradoraM.DataBind()

                        For Each dr As DataRow In dtAseguradora.Rows
                            dr("abreviatura") = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dr("abreviatura").ToString().ToLower())
                        Next

                        dtAseguradora.Rows(0).Delete()
                        cblAseguradoras.Items.Clear()
                        cblAseguradoras.DataSource = dtAseguradora
                        cblAseguradoras.DataTextField = "abreviatura"
                        cblAseguradoras.DataValueField = "value"
                        cblAseguradoras.DataBind()
                    End If
                End If

                'CargarControles()

                If Not Request.QueryString("ctr") Is Nothing Then
                    If Request.QueryString("ctr") = 1 Then
                        Session("FuncionalidadC") = "SOLICITUDES"
                        Response.Redirect("WfrmSolicitud.aspx?ini=1" & paramIsUser)
                    ElseIf Request.QueryString("ctr") = 2 Then
                        Session("FuncionalidadC") = "SOLICITUDES"
                        Response.Redirect("WfrmSolicitud.aspx?ini=1" & paramIsUser)
                    ElseIf Request.QueryString("ctr") = 3 Then
                        Session("FuncionalidadC") = "SOLICITUDES"
                        Response.Redirect("WfrmConsulta.aspx" & paramIsUser)
                    ElseIf Request.QueryString("ctr") = 4 Then
                        '    Response.Redirect("../Administrador/WfrmUsuario.aspx")
                        'Else
                        Session("FuncionalidadC") = Nothing
                        cprincipal.TerminaSesion(Integer.Parse(Me.nomLogin.Value))
                        Session.Clear()
                        Response.Redirect("../WfrmLogin.aspx")
                    End If
                End If

                If Not Request.QueryString("band") Is Nothing Then
                    'Estatus
                    parametro.Solicitud = 8
                    dsE = wsconsulta.WsTraerInformacion(parametro)
                    If dsE Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dt As DataTable = dsE.Tables(0)
                        If dt.Rows.Count > 0 Then
                            cmbEstatus.DataSource = dt
                            cmbEstatus.DataTextField = "STATUS"
                            cmbEstatus.DataValueField = "STATUS"
                            cmbEstatus.DataBind()
                        End If
                    End If

                    CargarInformacionBusqueda()
                End If

                lblSubTituloM.Text = Session("FuncionalidadC")
            Else
                ValidaSesion()
            End If

            If Not Request.QueryString("Ctr1") Is Nothing Then
                Nombre = Request.QueryString("Ctr2")
                If Not Request.QueryString("Ctr1") Is Nothing Then
                    Liga = Request.QueryString("Ctr1")
                End If
                sd.FileName = Nombre
                sd.File = Liga
                sd.ShowDialogBox()
            End If

            If Not Request.QueryString("rep") Is Nothing Then
                Nombre = Request.QueryString("rep")
                'Liga = cprincipal.rutaReporte & Request.QueryString("rep")
                If (Not System.IO.Directory.Exists(System.Configuration.ConfigurationManager.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA).ToString() +
                        CC.General.Diccionario.Carpetas.REPORTES)) Then
                    System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA).ToString() +
                    CC.General.Diccionario.Carpetas.REPORTES)
                End If

                Liga = System.Configuration.ConfigurationManager.AppSettings(CC.General.Diccionario.AppSettings.RUTA_MAZDA).ToString() & _
                    CC.General.Diccionario.Carpetas.REPORTES & Request.QueryString("rep")
                sd.FileName = Nombre
                sd.File = Liga
                sd.ShowDialogBox()
            End If

            If Not Request.QueryString("eli1") Is Nothing Then
                cprincipal.Eliminar_Archivos(Request.QueryString("eli1"), Request.QueryString("eli2"))
                pnlConsulta.Visible = False
                pnlModificacion.Visible = True
                ActualizaInformacion(Session("IdSolicitud"))
                MuestraArchivo(Session("IdSolicitud"))
            End If

            'If Session("usuario") = "" Then
            '    MsgBox.ShowMessage("Consulte con su administrador la informaci�n del usuario")
            '    Exit Sub
            'ElseIf Session("usuario") Is Nothing Then
            '    MsgBox.ShowMessage("Consulte con su administrador la informaci�n del usuario")
            '    Exit Sub
            'End If

        Catch ex As Exception
            Dim dasdas As String = ex.Message
        End Try
    End Sub

    Private Sub CargarMenu()
        Try
            Dim i As Integer
            Dim row As New TableRow
            ' agregar al menu seccion admnistraci�n de usuarios
            If Session("Acceso") = "3" Or Session("Acceso") = "4" Then
                'For i = 1 To 3
                For i = 1 To 5
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim cell As New TableCell

                    If i = 1 Then
                        hp.Text = "Consulta"
                        hp.NavigateUrl = "WfrmConsulta.aspx?ctr=3" & paramIsUser
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                    ElseIf i = 2 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)
                        'ElseIf i = 3 Then
                        '    hp.Text = "Salir"
                        '    hp.NavigateUrl = "WfrmConsulta.aspx?ctr=4"
                        '    hp.CssClass = "NegritaMediana"
                        '    cell.Controls.Add(hp)
                    ElseIf i = 3 Then
                        If Session("nombre_nivel") = "ADMINISTRADOR" Then
                            'Seccion para ADMINISTRACION de usuarios se retoma hata que se acepte o haya ticket MTG-2016
                            'hp.Text = "Usuarios"
                            'hp.NavigateUrl = "../Administrador/WFrmUsers.aspx"
                            'hp.CssClass = "NegritaMediana"
                            'cell.Controls.Add(hp)
                        End If
                    ElseIf i = 4 Then
                        If Session("nombre_nivel") = "ADMINISTRADOR" Then
                            'lbl.Text = "|"
                            'lbl.CssClass = "NegritaMediana"
                            'cell.Controls.Add(lbl)
                        End If
                    ElseIf i = 5 Then
                        'hp.Text = "Salir"
                        'hp.NavigateUrl = "WfrmConsulta.aspx?ctr=4" & paramIsUser
                        'hp.CssClass = "NegritaMediana"
                        'cell.Controls.Add(hp)
                    End If
                    row.Controls.Add(cell)
                    tblTitulo.Controls.Add(row)
                Next
            Else
                'For i = 1 To 7
                For i = 1 To 9
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim cell As New TableCell

                    If i = 1 Then
                        If Session("nombre_nivel") <> "AGENCIA" Then
                            hp.Text = "Solicitudes"
                            hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=1" & paramIsUser
                            hp.CssClass = "NegritaMediana"
                            cell.Controls.Add(hp)
                            row.Controls.Add(cell)
                        End If

                        'ElseIf i = 2 Then
                        '    If Session("nombre_nivel") <> "AONDAIMLER" Then
                        '        If Session("nombre_nivel") <> "AGENCIA" Then
                        '            If Session("Quejas") <> "0" Then
                        '                lbl.Text = "|"
                        '                lbl.CssClass = "NegritaMediana"
                        '                cell.Controls.Add(lbl)
                        '                row.Controls.Add(cell)
                        '            End If
                        '        End If
                        '    End If

                        'ElseIf i = 3 Then
                        'If Session("nombre_nivel") <> "AONDAIMLER" Then
                        '    If Session("Quejas") <> "0" Then
                        '        hp.Text = "S&C"
                        '        hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=2" & paramIsUser
                        '        hp.CssClass = "NegritaMediana"
                        '        cell.Controls.Add(hp)
                        '        row.Controls.Add(cell)
                        '    End If
                        'End If

                    ElseIf i = 4 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)
                        row.Controls.Add(cell)
                    ElseIf i = 5 Then
                        hp.Text = "Consulta"
                        hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=3" & paramIsUser
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                        row.Controls.Add(cell)
                    ElseIf i = 6 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)
                        row.Controls.Add(cell)
                        'ElseIf i = 7 Then
                        '    hp.Text = "Salir"
                        '    hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=4"
                        '    hp.CssClass = "NegritaMediana"
                        '    cell.Controls.Add(hp)
                        '    row.Controls.Add(cell)
                    ElseIf i = 7 Then
                        If Session("nombre_nivel") = "ADMINISTRADOR" Then
                            'hp.Text = "Usuarios"
                            'hp.NavigateUrl = "../Administrador/WFrmUsers.aspx"
                            'hp.CssClass = "NegritaMediana"
                            'cell.Controls.Add(hp)
                            'row.Controls.Add(cell)
                        End If
                    ElseIf i = 8 Then
                        If Session("nombre_nivel") = "ADMINISTRADOR" Then
                            'lbl.Text = "|"
                            'lbl.CssClass = "NegritaMediana"
                            'cell.Controls.Add(lbl)
                            'row.Controls.Add(cell)
                        End If
                    ElseIf i = 9 Then
                        'hp.Text = "Salir"
                        'hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=4" & paramIsUser
                        'hp.CssClass = "NegritaMediana"
                        'cell.Controls.Add(hp)
                        'row.Controls.Add(cell)
                    End If

                    tblTitulo.Controls.Add(row)
                Next
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Controles()
        lblTitulo.CssClass = "Titulo"
        lblTitulo2.CssClass = "Titulo"
        lblSubtitulo.CssClass = "SubTitulo"
        lblFuncionalidad.CssClass = "NegritaMediana"
        lblPoliza.CssClass = "NegritaMediana"
        lblSerie.CssClass = "NegritaMediana"
        lblContrato.CssClass = "NegritaMediana"
        lblNombreCliente.CssClass = "NegritaMediana"
        lblAseguradora.CssClass = "NegritaMediana"
        lblUsuario.CssClass = "NegritaMediana"
        lblArchivos.CssClass = "NegritaMediana"
        lblIdSolicitud.CssClass = "NegritaMediana"
        lblTipoSolicitud.CssClass = "NegritaMediana"
        lblEstatus.CssClass = "NegritaMediana"
        lblFechaInicial.CssClass = "NegritaMediana"
        lblFechaFinal.CssClass = "NegritaMediana"

        txtIdSolicitud.CssClass = "Caja_Texto"
        cmbFuncionalidad.CssClass = "Caja_Texto"
        cmbTipoSolicitud.CssClass = "Caja_Texto"
        txtPoliza.CssClass = "Caja_Texto"
        txtSerie.CssClass = "Caja_Texto"
        txtContrato1.CssClass = "Caja_Texto"
        txtContrato2.CssClass = "Caja_Texto"
        txtNombreCliente.CssClass = "Caja_Texto"
        ddlAseguradora.CssClass = "Caja_Texto"
        txtUsuario.CssClass = "Caja_Texto"
        cmbEstatus.CssClass = "Caja_Texto"
        txtFechaI.CssClass = "Caja_Texto"
        txtFechaF.CssClass = "Caja_Texto"

        'Modificacion
        lblSubTituloM.CssClass = "SubTitulo"
        lblIdSolicitudM.CssClass = "NegritaMediana"
        lblTipoSolicitudM.CssClass = "NegritaMediana"
        lblDetalleM.CssClass = "NegritaMediana"
        lblPolizaM.CssClass = "NegritaMediana"
        lblSerieM.CssClass = "NegritaMediana"
        lblContratoM.CssClass = "NegritaMediana"
        lblClienteM.CssClass = "NegritaMediana"
        lblAseguradoraM.CssClass = "NegritaMediana"
        lblStatusM.CssClass = "NegritaMediana"
        lblFechaStatusM.CssClass = "NegritaMediana"
        lblSolicitaM.CssClass = "NegritaMediana"
        lblFechaSolicitadoM.CssClass = "NegritaMediana"
        lblNivelServicioM.CssClass = "NegritaMediana"
        lblFechaCerradoM.CssClass = "NegritaMediana"
        lblNivelServicioPendienteM.CssClass = "NegritaMediana"
        lblNivelServicioTramiteM.CssClass = "NegritaMediana"
        lblNivelServicioCierreM.CssClass = "NegritaMediana"
        lblObservaciones.CssClass = "NegritaMediana"
        lblArchivos.CssClass = "NegritaMediana"
        lblSolicitanteM.CssClass = "NegritaMediana"
        lblFechaPendienteM.CssClass = "NegritaMediana"
        lblFechaCanceladoM.CssClass = "NegritaMediana"
        lblFechaTramiteM.CssClass = "NegritaMediana"
        lblSLAM.CssClass = "NegritaMediana"
        lblConformeSLAM.CssClass = "NegritaMediana"
        lblNuevaPolizaM.CssClass = "NegritaMediana"

        txtIdSolicitudM.CssClass = "Caja_Texto"
        cmbTipoSolicitudM.CssClass = "Caja_Texto"
        txtDetalleM.CssClass = "Caja_Texto"
        txtPolizaM.CssClass = "Caja_Texto"
        txtSerieM.CssClass = "Caja_Texto"
        txtContratoM.CssClass = "Caja_Texto"
        txtClienteM.CssClass = "Caja_Texto"
        ddlAseguradoraM.CssClass = "Caja_Texto"
        cmbStatusM.CssClass = "Caja_Texto"
        txtFechaEstatusM.CssClass = "Caja_Texto"
        txtSolicitanteM.CssClass = "Caja_Texto"
        txtSolicitaM.CssClass = "Caja_Texto"
        txtFechaSolicitadoM.CssClass = "Caja_Texto"
        txtNivelServicioM.CssClass = "Caja_Texto"
        txtFechaCerradoM.CssClass = "Caja_Texto"
        txtNivelServicioPendienteM.CssClass = "Caja_Texto"
        txtNivelServicioTramiteM.CssClass = "Caja_Texto"
        txtNivelServicioCierreM.CssClass = "Caja_Texto"
        txtFechaPendienteM.CssClass = "Caja_Texto"
        txtFechaCanceladoM.CssClass = "Caja_Texto"
        txtFechaTramiteM.CssClass = "Caja_Texto"
        txtObservacionesL.CssClass = "Caja_Texto"
        txtObservaciones.CssClass = "Caja_Texto"
        txtSLAM.CssClass = "Caja_Texto"
        txtConformeSLAM.CssClass = "Caja_Texto"
        txtNuevaPolizaM.CssClass = "Caja_Texto"

        If Session("nombre_nivel") <> "Administrador" Then
            txtIdSolicitudM.ReadOnly = True
            txtDetalleM.ReadOnly = True
            txtPolizaM.ReadOnly = True
            txtSerieM.ReadOnly = True
            txtContratoM.ReadOnly = True
            txtClienteM.ReadOnly = True
            txtFechaEstatusM.ReadOnly = True
            txtSolicitaM.ReadOnly = True
            txtFechaSolicitadoM.ReadOnly = True
            txtNivelServicioM.ReadOnly = True
            txtFechaCerradoM.ReadOnly = True
            txtNivelServicioPendienteM.ReadOnly = True
            txtNivelServicioTramiteM.ReadOnly = True
            txtNivelServicioCierreM.ReadOnly = True
            txtFechaPendienteM.ReadOnly = True
            txtFechaCanceladoM.ReadOnly = True
            txtFechaTramiteM.ReadOnly = True
            txtSLAM.ReadOnly = True
            txtConformeSLAM.ReadOnly = True
            txtObservacionesL.ReadOnly = True
            cmdGuardar.Enabled = False
        End If

        If Session("FuncionalidadC") = "SOLICITUDES" Then
            lblPoliza.Visible = True
            lblSerie.Visible = True
            lblContrato.Visible = True
            lblNombreCliente.Visible = True
            lblAseguradora.Visible = True
            lblUsuario.Visible = True
            txtPoliza.Visible = True
            txtSerie.Visible = True
            txtContrato1.Visible = True
            txtNombreCliente.Visible = True
            ddlAseguradora.Visible = True
            txtUsuario.Visible = True

        ElseIf Session("FuncionalidadC") = "QUEJAS" Then
            lblPoliza.Visible = False
            lblSerie.Visible = False
            lblContrato.Visible = False
            lblNombreCliente.Visible = False
            lblAseguradora.Visible = False
            lblUsuario.Visible = False
            txtPoliza.Visible = False
            txtSerie.Visible = False
            txtContrato1.Visible = False
            txtNombreCliente.Visible = False
            ddlAseguradora.Visible = False
            txtUsuario.Visible = False
        End If
    End Sub

    Private Sub CargarControles()
        Try

            Dim parametro As New DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.Solicitud = 6
            parametro.IdSolOracle = 0   'default
            parametro.Funcionalidad = Session("Funcionalidad")
            'Anexar linea si falta de Funcionalidad
            'Obtiene la informaci�n para el tipo de solicitud
            Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
            If ds Is Nothing Then
                MsgBox.ShowMessage("No se tiene informaci�n")
                Exit Sub
            Else
                Dim dt As DataTable = ds.Tables(0)
                If dt.Rows.Count > 0 Then
                    cmbTipoSolicitud.DataSource = dt
                    cmbTipoSolicitud.DataTextField = "TIPOSOLICITUD"
                    cmbTipoSolicitud.DataValueField = "TIPOSOLICITUD"
                    cmbTipoSolicitud.DataBind()
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Try
            Dim IdSolicitud As Long
            Session("IdSolicitudB") = ""
            Session("TipoSolicitudB") = ""
            Session("PolizaB") = ""
            Session("SerieB") = ""
            Session("Contrato1B") = ""
            Session("Contrato2B") = ""
            Session("AseguradoraB") = ""
            Session("NombreClienteB") = ""
            Session("UsuarioB") = ""
            Session("StatusB") = ""
            Session("FechaInicioB") = ""
            Session("FechaFinB") = ""

            hpReporte.Visible = False

            If cmbFuncionalidad.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione la funcionalidad")
                Exit Sub
            Else
                If cmbFuncionalidad.SelectedItem.Text = "SUGERENCIAS" Then
                    Session("FuncionalidadC") = "QUEJAS"
                Else
                    Session("FuncionalidadC") = cmbFuncionalidad.SelectedItem.Text
                End If
            End If

            If txtIdSolicitud.Text <> "" Then
                IdSolicitud = IIf(CStr(txtIdSolicitud.Text) = "", 0, txtIdSolicitud.Text)
                IdSolicitud = IIf(IdSolicitud < 0, 0, IdSolicitud)
                Session("IdSolicitudB") = IdSolicitud
            Else
                Session("IdSolicitudB") = ""
            End If

            If cmbTipoSolicitud.SelectedItem.Text <> "-- Seleccione --" Then
                Session("TipoSolicitudB") = cmbTipoSolicitud.SelectedItem.Text
            End If

            Session("PolizaB") = txtPoliza.Text
            Session("SerieB") = txtSerie.Text
            Session("Contrato1B") = txtContrato1.Text
            Session("Contrato2B") = txtContrato2.Text
            Session("AseguradoraB") = IIf(ddlAseguradora.SelectedItem.Value = "0", "", ddlAseguradora.SelectedValue)
            Session("NombreClienteB") = txtNombreCliente.Text
            Session("UsuarioB") = txtUsuario.Text
            Session("FechaInicioB") = txtFechaI.Text
            Session("FechaFinB") = txtFechaF.Text

            'If Poliza = "" And Serie = "" And Contrato1 = "" And Contrato2 = "" And Aseguradora = "" And Usuario = "" Then
            '    MsgBox.ShowMessage("Ingrese por lo menos una opci�n")
            '    Exit Sub
            'End If

            'If Poliza = "" And Serie = "" And Contrato1 = "" And Aseguradora = "" And Usuario = "" Then
            '    MsgBox.ShowMessage("Ingrese por lo menos una opci�n")
            '    Exit Sub
            'End If

            If cmbEstatus.SelectedItem.Text <> "-- Seleccione --" Then
                Session("StatusB") = cmbEstatus.SelectedItem.Text
            End If

            If txtFechaI.Text <> "" Then
                If txtFechaF.Text = "" Then
                    MsgBox.ShowMessage("Ingrese la fecha final")
                    Exit Sub
                End If
            End If

            If txtFechaF.Text <> "" Then
                If txtFechaI.Text = "" Then
                    MsgBox.ShowMessage("Ingrese la fecha inicial")
                    Exit Sub
                End If
            End If

            Session("FechaInicioB") = txtFechaI.Text
            Session("FechaFinB") = txtFechaF.Text

            pnlConsulta.Visible = True
            pnlModificacion.Visible = False

            CargarGrid(IdSolicitud, Session("TipoSolicitudB"), Session("PolizaB"), Session("SerieB"), _
            Session("Contrato1B"), Session("AseguradoraB"), Session("Usuario"), Session("StatusB"), _
            Session("FechaInicioB"), Session("FechaFinB"), Session("NombreClienteB"), Session("BidAon"), Session("Bid"), Session("ConsultaCliente"))

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub CargarGrid(ByVal IdSolicitud As Long, ByVal TipoSolicitud As String, ByVal Poliza As String, _
    ByVal Serie As String, ByVal Contrato1 As String, _
    ByVal Aseguradora As String, ByVal Usuario As String, ByVal Status As String, _
    ByVal FechaInicial As String, ByVal FechaFinal As String, ByVal NombreCliente As String, _
    ByVal Bid As String, ByVal Id_Bid As Integer, ByVal filtroCliente As Boolean)

        Try
            Dim i As Long
            Dim datSolicitud As Integer
            Dim datTipoSolicitud As String
            Dim datPoliza As String
            Dim datSerie As String
            Dim datContrato As String
            Dim datCliente As String
            Dim datAseguradora As String
            Dim datFECSOLICITADO As String
            Dim datStatus As String
            Dim datFechaStatus As String
            Dim datUSUARIOSOLICITANTE As String
            Dim datNSSTATUS As String
            Dim datFECCERRADO As String
            Dim datNSPENDIENTE As String
            Dim datNSTRAMITE As String
            Dim datNSCERRADO As String
            Dim datFECPENDIENTE As String
            Dim datFECTRAMITE As String
            Dim datFECCANCELADO As String
            Dim datDetalle As String
            Dim datSLA As String
            Dim datConformeSLA As String
            Dim datColor As String
            Dim datDistribuidor As String
            Dim datNuevaPoliza As String
            Dim datBandera As String

            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            'parametro.IdSolOracle = 0            '0 = solo para insercion, otro valor > 0 es actualizaci�n
            parametro.Solicitud = 4
            parametro.User = IIf(Session("nombre_nivel") <> "Administrador", Usuario, Nothing)
            parametro.Funcionalidad = Session("FuncionalidadC")
            parametro.IdSolOracle = IdSolicitud
            parametro.TipoSolicitud = IIf(CStr(cmbTipoSolicitud.SelectedItem.Text) = "-- Seleccione --", "", cmbTipoSolicitud.SelectedItem.Text)
            parametro.NoPoliza = cvalidacion.QuitaCaracteres(Poliza)
            parametro.Serie = cvalidacion.QuitaCaracteres(Serie)
            parametro.Contrato = cvalidacion.QuitaCaracteres(Contrato1)
            parametro.Cliente = cvalidacion.QuitaCaracteres(NombreCliente)
            parametro.Aseguradora = cvalidacion.QuitaCaracteres(Aseguradora)
            parametro.TipoEstatus = Status
            parametro.FechaInicioConsulta = FechaInicial
            parametro.FechaFinConsulta = FechaFinal
            parametro.CadenaBinAon = Bid
            parametro.FiltroCliente = filtroCliente
            parametro.Id_Bid = Id_Bid

            'parametro.TipoSolicitante = Session("usuario")
            'parametro.User = UCase(Session("usuario"))

            If Session("TipoUsuario") = 1 Then
                parametro.TipoPerfil = Session("nombre_nivel")
                'parametro.TipoSolicitante = Session("nombre_nivel")
            ElseIf Session("TipoUsuario") = 2 Then
                parametro.User = Session("usuario")
            End If
            'parametro.UserSol = UCase(Session("nombre"))

            Dim dt As DataTable = wsconsulta.WsTraerInformacion(parametro).Tables(0)
            If dt Is Nothing Then
                MsgBox.ShowMessage("No se tiene informaci�n")
                grdGeneral.Visible = False
                cmdExcel.Visible = False
                Exit Sub
            Else
                If dt.Rows.Count = 0 Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    grdGeneral.Visible = False
                    cmdExcel.Visible = False
                    Exit Sub
                Else
                    cmdExcel.Visible = True

                    grdGeneral.Controls.Clear()
                    grdGeneral.Visible = True
                    grdGeneral.DataSource = dt
                    grdGeneral.DataBind()

                    'Guarda la informacion en BD SQL
                    'Borra la informaci�n del usuario activo tomando el IdSession de la tabla temporal
                    cprincipal.Eliminar_TablaTemporal(Session("IdSession"))

                    For i = 0 To dt.Rows.Count - 1
                        datSolicitud = dt.Rows(i).Item("IdSolicitud")
                        If dt.Rows(i).IsNull("TipoSolicitud") Then
                            datTipoSolicitud = ""
                        Else
                            datTipoSolicitud = dt.Rows(i).Item("TipoSolicitud")
                        End If
                        If dt.Rows(i).IsNull("Poliza") Then
                            datPoliza = ""
                        Else
                            datPoliza = dt.Rows(i).Item("Poliza")
                        End If
                        If dt.Rows(i).IsNull("Serie") Then
                            datSerie = ""
                        Else
                            datSerie = dt.Rows(i).Item("Serie")
                        End If
                        If dt.Rows(i).IsNull("Contrato") Then
                            datContrato = ""
                        Else
                            datContrato = dt.Rows(i).Item("Contrato")
                        End If
                        If dt.Rows(i).IsNull("Cliente") Then
                            datCliente = ""
                        Else
                            datCliente = dt.Rows(i).Item("Cliente")
                        End If
                        If dt.Rows(i).IsNull("Aseguradora") Then
                            datAseguradora = ""
                        Else
                            datAseguradora = dt.Rows(i).Item("Aseguradora")
                        End If
                        If dt.Rows(i).IsNull("FECSOLICITADO") Then
                            datFECSOLICITADO = ""
                        Else
                            datFECSOLICITADO = dt.Rows(i).Item("FECSOLICITADO")
                        End If
                        If dt.Rows(i).IsNull("Status") Then
                            datStatus = ""
                        Else
                            datStatus = dt.Rows(i).Item("Status")
                        End If
                        If dt.Rows(i).IsNull("FechaStatus") Then
                            datFechaStatus = ""
                        Else
                            datFechaStatus = dt.Rows(i).Item("FechaStatus")
                        End If
                        If dt.Rows(i).IsNull("USUARIOSOLICITANTE") Then
                            datUSUARIOSOLICITANTE = ""
                        Else
                            datUSUARIOSOLICITANTE = dt.Rows(i).Item("USUARIOSOLICITANTE")
                        End If
                        If dt.Rows(i).IsNull("NSSTATUS") Then
                            datNSSTATUS = ""
                        Else
                            datNSSTATUS = dt.Rows(i).Item("NSSTATUS")
                        End If
                        If dt.Rows(i).IsNull("FECCERRADO") Then
                            datFECCERRADO = ""
                        Else
                            datFECCERRADO = dt.Rows(i).Item("FECCERRADO")
                        End If
                        If dt.Rows(i).IsNull("NSPENDIENTE") Then
                            datNSPENDIENTE = ""
                        Else
                            datNSPENDIENTE = dt.Rows(i).Item("NSPENDIENTE")
                        End If
                        If dt.Rows(i).IsNull("NSTRAMITE") Then
                            datNSTRAMITE = ""
                        Else
                            datNSTRAMITE = dt.Rows(i).Item("NSTRAMITE")
                        End If
                        If dt.Rows(i).IsNull("NSCERRADO") Then
                            datNSCERRADO = ""
                        Else
                            datNSCERRADO = dt.Rows(i).Item("NSCERRADO")
                        End If
                        If dt.Rows(i).IsNull("FECPENDIENTE") Then
                            datFECPENDIENTE = ""
                        Else
                            datFECPENDIENTE = dt.Rows(i).Item("FECPENDIENTE")
                        End If
                        If dt.Rows(i).IsNull("FECTRAMITE") Then
                            datFECTRAMITE = ""
                        Else
                            datFECTRAMITE = dt.Rows(i).Item("FECTRAMITE")
                        End If
                        If dt.Rows(i).IsNull("FECCANCELADO") Then
                            datFECCANCELADO = ""
                        Else
                            datFECCANCELADO = dt.Rows(i).Item("FECCANCELADO")
                        End If
                        If dt.Rows(i).IsNull("Detalle") Then
                            datDetalle = ""
                        Else
                            datDetalle = dt.Rows(i).Item("Detalle")
                        End If
                        If dt.Rows(i).IsNull("SLA") Then
                            datSLA = ""
                        Else
                            datSLA = dt.Rows(i).Item("SLA")
                        End If
                        If dt.Rows(i).IsNull("ConformeSLA") Then
                            datConformeSLA = ""
                        Else
                            datConformeSLA = dt.Rows(i).Item("ConformeSLA")
                        End If
                        If dt.Rows(i).IsNull("Color") Then
                            datColor = ""
                        Else
                            datColor = dt.Rows(i).Item("Color")
                        End If
                        If dt.Rows(i).IsNull("BidAon") Then
                            datDistribuidor = ""
                        Else
                            datDistribuidor = dt.Rows(i).Item("BidAon")
                        End If
                        If dt.Rows(i).IsNull("PolizaNueva") Then
                            datNuevaPoliza = ""
                        Else
                            datNuevaPoliza = dt.Rows(i).Item("PolizaNueva")
                        End If
                        If dt.Rows(i).IsNull("VIP") Then
                            datBandera = ""
                        Else
                            datBandera = dt.Rows(i).Item("VIP")
                        End If


                        'IdSolicitud = CInt(IIf(txtIdSolicitud.Text = "", 0, txtIdSolicitud.Text))
                        'Inserta la informacion
                        cprincipal.Inserta_InformacionTemporal(datSolicitud, datTipoSolicitud, datPoliza, datSerie, _
                        datContrato, datCliente, datAseguradora, datFECSOLICITADO, datStatus, datFechaStatus, _
                        datUSUARIOSOLICITANTE, datNSSTATUS, datFECCERRADO, datNSPENDIENTE, datNSTRAMITE, datNSCERRADO, _
                        datFECPENDIENTE, datFECTRAMITE, datFECCANCELADO, datDetalle, datSLA, datConformeSLA, datColor, _
                        Session("IdSession"), datDistribuidor, datNuevaPoliza, datBandera)
                    Next

                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdGeneral_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdGeneral.ItemCommand
        Try
            If e.CommandName = "Edit" Then


            ElseIf e.CommandName = "Update" Then

            ElseIf e.CommandName = "Select" Then
                pnlConsulta.Visible = False
                pnlModificacion.Visible = True

                Dim parametro As New WsDaimlerSolicitud.DatosEntrada

                parametro.Usuario = "WSDAIMLER"
                parametro.Password = "SOLICITUDES"
                parametro.TipoPerfil = Session("nombre_nivel")
                parametro.Funcionalidad = Session("FuncionalidadC")
                parametro.Solicitud = 6
                parametro.IdSolOracle = 0   'default

                'Id Solicitud
                If e.Item.Cells(0).Text() = "&nbsp;" Then
                    txtIdSolicitudM.Text = ""
                Else
                    txtIdSolicitudM.Text = e.Item.Cells(0).Text()
                    Session("IdSolicitud") = e.Item.Cells(0).Text()
                End If

                'Tipo solicitud
                If e.Item.Cells(1).Text() = "&nbsp;" Then
                    'cmbTipoSolicitud.SelectedItem.Text = 0
                Else
                    'Obtiene la informaci�n para el tipo de solicitud
                    Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
                    If ds Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dt As DataTable = ds.Tables(0)
                        If dt.Rows.Count > 0 Then
                            cmbTipoSolicitudM.DataSource = dt
                            cmbTipoSolicitudM.DataTextField = "TIPOSOLICITUD"
                            cmbTipoSolicitudM.DataValueField = "TIPOSOLICITUD"
                            cmbTipoSolicitudM.DataBind()
                        End If
                        cmbTipoSolicitudM.SelectedIndex = cmbTipoSolicitudM.Items.IndexOf(cmbTipoSolicitudM.Items.FindByValue(e.Item.Cells(1).Text))
                    End If
                End If

                If Session("FuncionalidadC") = "SOLICITUDES" Then
                    'GDDD
                    ValidaOpcPoliza()
                    lblSerieM.Visible = True
                    txtSerieM.Visible = True
                    lblContratoM.Visible = True
                    txtContratoM.Visible = True
                    lblAseguradoraM.Visible = True
                    ddlAseguradoraM.Visible = True
                    lblNivelServicioPendienteM.Visible = True
                    txtNivelServicioPendienteM.Visible = True
                    lblFechaPendienteM.Visible = True
                    txtFechaPendienteM.Visible = True
                    lblFechaCanceladoM.Visible = True
                    txtFechaCanceladoM.Visible = True
                Else
                    lblPolizaM.Visible = False
                    txtPolizaM.Visible = False
                    lblSerieM.Visible = False
                    txtSerieM.Visible = False
                    lblContratoM.Visible = False
                    txtContratoM.Visible = False
                    lblAseguradoraM.Visible = False
                    ddlAseguradoraM.Visible = False
                    lblNivelServicioPendienteM.Visible = False
                    txtNivelServicioPendienteM.Visible = False
                    lblFechaPendienteM.Visible = False
                    txtFechaPendienteM.Visible = False
                    lblFechaCanceladoM.Visible = False
                    txtFechaCanceladoM.Visible = False
                End If

                'P�liza
                If e.Item.Cells(2).Text() = "&nbsp;" Then
                    txtPolizaM.Text = ""
                Else
                    txtPolizaM.Text = e.Item.Cells(2).Text()
                End If

                'Serie
                If e.Item.Cells(3).Text() = "&nbsp;" Then
                    txtSerieM.Text = ""
                Else
                    txtSerieM.Text = e.Item.Cells(3).Text()
                End If

                'Contrato
                If e.Item.Cells(4).Text() = "&nbsp;" Then
                    txtContratoM.Text = ""
                Else
                    txtContratoM.Text = e.Item.Cells(4).Text()
                End If

                'Nombre del Cliente
                If e.Item.Cells(5).Text() = "&nbsp;" Then
                    txtClienteM.Text = ""
                Else
                    txtClienteM.Text = e.Item.Cells(5).Text()
                End If

                ''Aseguradora
                If e.Item.Cells(6).Text() = "&nbsp;" Then
                    ddlAseguradoraM.SelectedValue = 0
                Else
                    hdnAseguradora.Value = String.Empty
                    ddlAseguradoraM.Visible = False
                    cblAseguradoras.Visible = False
                    If e.Item.Cells(1).Text().IndexOf("COTIZACION") = -1 Then
                        BuscaAseguradora(e)
                    Else
                        If Session("ConsultaCliente") = True Then
                            BuscaAseguradora(e)
                        Else
                            For x As Integer = 0 To cblAseguradoras.Items.Count - 1
                                If Not e.Item.Cells(6).Text().ToUpper().IndexOf(cblAseguradoras.Items(x).Value) = -1 Then
                                    cblAseguradoras.Items(x).Selected = True
                                Else
                                    cblAseguradoras.Items(x).Selected = False
                                End If
                            Next
                            cblAseguradoras.Visible = True
                        End If
                    End If
                End If

                'Fecha Solicitado
                If e.Item.Cells(7).Text() = "&nbsp;" Then
                    txtFechaSolicitadoM.Text = ""
                Else
                    txtFechaSolicitadoM.Text = e.Item.Cells(7).Text()
                End If

                'Status
                If e.Item.Cells(8).Text() = "&nbsp;" Then
                    'cmbStatus.SelectedItem.Text = ""
                Else
                    'Estatus
                    parametro.Funcionalidad = ""
                    parametro.Solicitud = 8
                    Dim dsE As DataSet = wsconsulta.WsTraerInformacion(parametro)
                    If dsE Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dt As DataTable = dsE.Tables(0)
                        If dt.Rows.Count > 0 Then
                            cmbStatusM.DataSource = dt
                            cmbStatusM.DataTextField = "STATUS"
                            cmbStatusM.DataValueField = "STATUS"
                            cmbStatusM.DataBind()
                        End If
                        cmbStatusM.SelectedIndex = cmbStatusM.Items.IndexOf(cmbStatusM.Items.FindByValue(e.Item.Cells(8).Text()))
                        If e.Item.Cells(8).Text = "CANCELADO" Or e.Item.Cells(8).Text = "CONCLUIDO" Then
                            cmdGuardar.Enabled = False
                        End If
                    End If
                End If

                'Fecha Estatus
                If e.Item.Cells(9).Text() = "&nbsp;" Then
                    txtFechaEstatusM.Text = ""
                Else
                    txtFechaEstatusM.Text = e.Item.Cells(9).Text()
                End If

                'Quien Solicita
                If e.Item.Cells(10).Text() = "&nbsp;" Then
                    txtSolicitaM.Text = ""
                Else
                    txtSolicitaM.Text = e.Item.Cells(10).Text()
                End If

                'Nivel Servicio Status
                If e.Item.Cells(11).Text() = "&nbsp;" Then
                    txtNivelServicioM.Text = ""
                Else
                    txtNivelServicioM.Text = e.Item.Cells(11).Text()
                End If

                'Fecha Cerrado
                If e.Item.Cells(12).Text() = "&nbsp;" Then
                    txtFechaCerradoM.Text = ""
                Else
                    txtFechaCerradoM.Text = e.Item.Cells(12).Text()
                End If

                'Nivel Servicio Pendiente
                If e.Item.Cells(13).Text() = "&nbsp;" Then
                    txtNivelServicioPendienteM.Text = ""
                Else
                    txtNivelServicioPendienteM.Text = e.Item.Cells(13).Text()
                End If

                'Nivel Servicio Tr�mite
                If e.Item.Cells(14).Text() = "&nbsp;" Then
                    txtNivelServicioTramiteM.Text = ""
                Else
                    txtNivelServicioTramiteM.Text = e.Item.Cells(14).Text()
                End If

                'Nivel Servicio Cierre
                If e.Item.Cells(15).Text() = "&nbsp;" Then
                    txtNivelServicioCierreM.Text = ""
                Else
                    txtNivelServicioCierreM.Text = e.Item.Cells(15).Text()
                End If

                'Solicitante
                If e.Item.Cells(16).Text() = "&nbsp;" Then
                    txtSolicitanteM.Text = ""
                Else
                    txtSolicitanteM.Text = e.Item.Cells(16).Text()
                End If

                'Agencia
                If e.Item.Cells(17).Text() = "&nbsp;" Then
                    Session("Agencia") = ""
                Else
                    Session("Agencia") = e.Item.Cells(17).Text()
                End If

                'Bid Aon
                If e.Item.Cells(18).Text() = "&nbsp;" Then
                    Session("BidAon") = ""
                Else
                    Session("BidAon") = e.Item.Cells(18).Text()
                End If

                'Fecha Pendiente
                If e.Item.Cells(19).Text() = "&nbsp;" Then
                    txtFechaPendienteM.Text = ""
                Else
                    txtFechaPendienteM.Text = e.Item.Cells(19).Text()
                End If

                'Fecha Tramite
                If e.Item.Cells(20).Text() = "&nbsp;" Then
                    txtFechaTramiteM.Text = ""
                Else
                    txtFechaTramiteM.Text = e.Item.Cells(20).Text()
                End If

                'Fecha Cancelado
                If e.Item.Cells(21).Text() = "&nbsp;" Then
                    txtFechaCanceladoM.Text = ""
                Else
                    txtFechaCanceladoM.Text = e.Item.Cells(21).Text()
                End If

                'Detalle
                If e.Item.Cells(22).Text() = "&nbsp;" Then
                    txtDetalleM.Text = ""
                Else
                    txtDetalleM.Text = e.Item.Cells(22).Text()
                End If

                'SLA
                If e.Item.Cells(23).Text() = "&nbsp;" Then
                    txtSLAM.Text = ""
                Else
                    txtSLAM.Text = e.Item.Cells(23).Text()
                End If

                'Conforme SLA
                If e.Item.Cells(24).Text() = "&nbsp;" Then
                    txtConformeSLAM.Text = ""
                Else
                    txtConformeSLAM.Text = e.Item.Cells(24).Text()
                End If

                'Observaciones
                parametro.Solicitud = 3
                parametro.IdSolOracle = txtIdSolicitudM.Text
                Dim dsO As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If dsO Is Nothing Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    Dim dt As DataTable = dsO.Tables(0)
                    If dt.Rows.Count > 0 Then
                        If dt.Rows(0).IsNull("Observaciones") Then
                            txtObservacionesL.Text = ""
                        Else
                            txtObservacionesL.Text = dt.Rows(0).Item("Observaciones")
                        End If
                        'Dim Observaciones As String = dt.Rows(0)(0)                    
                    End If
                End If

                MuestraArchivo(txtIdSolicitudM.Text)
            End If


        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub BuscaAseguradora(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim existe As Boolean
        For x As Integer = 0 To ddlAseguradoraM.Items.Count - 1
            If Not e.Item.Cells(6).Text().ToUpper().IndexOf(ddlAseguradoraM.Items(x).Value) = -1 Then
                ddlAseguradoraM.SelectedValue = ddlAseguradoraM.Items(x).Value
                existe = True
                Exit For
            End If
        Next

        If Not existe Then
            ddlAseguradoraM.SelectedIndex = 0
        End If

        hdnAseguradora.Value = e.Item.Cells(6).Text()
        ddlAseguradoraM.Visible = True
    End Sub

    Private Sub cmdRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRegresar.Click
        pnlConsulta.Visible = True
        pnlModificacion.Visible = False

        If Session("IdSolicitudB") = "0" Then
            txtIdSolicitud.Text = ""
        Else
            txtIdSolicitud.Text = Session("IdSolicitudB")
        End If
        CargarInformacionBusqueda()

        'cmbTipoSolicitudM.SelectedIndex = cmbTipoSolicitudM.Items.IndexOf(cmbTipoSolicitudM.Items.FindByValue(Session("TipoSolicitudB")))
        'txtPoliza.Text = Session("PolizaB")
        'txtSerie.Text = Session("SerieB")
        'txtContrato1.Text = Session("Contrato1B")
        'txtAseguradora.Text = Session("AseguradoraB")
        'txtUsuario.Text = Session("UsuarioB")
        'txtNombreCliente.Text = Session("NombreClienteB")
        'txtFechaI.Text = Session("FechaInicioB")
        'txtFechaF.Text = Session("FechaFinB")
        'cmbTipoSolicitudM.SelectedIndex = cmbTipoSolicitudM.Items.IndexOf(cmbTipoSolicitudM.Items.FindByValue(Session("StatusB")))

        'CargarGrid(Session("IdSolicitudB"), Session("TipoSolicitudB"), Session("PolizaB"), Session("SerieB"), _
        'Session("Contrato1B"), Session("Contrato2B"), Session("AseguradoraB"), Session("Usuario"), Session("StatusB"), _
        'Session("FechaInicioB"), Session("FechaFinB"), Session("NombreClienteB"), Session("Bid"))
    End Sub

    Private Sub MuestraArchivo(ByVal IdSolicitud As Long)
        Try
            'Obtiene el IdArchivoSolicitud en base al IdSolicitud
            'Dim dtS As DataTable = cprincipal.Obtiene_IdArchivoSolicitud(IdSolicitud)
            'If dtS.Rows.Count > 0 Then
            '    Session("IdArchivoSolicitud") = dtS.Rows(0)("Id_ArchivoSolicitud")
            'End If

            tblArchivo.Controls.Clear()
            'Muestra el archivo en la tabla tblArchivo
            If IdSolicitud <> "0" Then
                Dim dt As DataTable = cprincipal.Obtiene_Archivos(0, Session("IdSolicitud"))
                If dt.Rows.Count > 0 Then
                    'Encabezado
                    Dim j As Integer
                    Dim i As Integer

                    For i = 0 To dt.Rows.Count - 1
                        If i = 0 Then
                            Dim rowT As New TableRow
                            For j = 1 To 3
                                Dim lblT As New Label
                                Dim cellT As New TableCell

                                If j = 1 Then
                                    lblT.Text = "Archivo"
                                    cellT.Controls.Add(lblT)
                                ElseIf j = 2 Then
                                    lblT.Text = "Abrir"
                                    cellT.Controls.Add(lblT)
                                Else
                                    lblT.Text = "&nbsp;"
                                    cellT.Controls.Add(lblT)
                                End If
                                cellT.CssClass = "negritagris"
                                rowT.Controls.Add(cellT)
                            Next
                            tblArchivo.Controls.Add(rowT)
                        End If

                        Dim row As New TableRow
                        For j = 1 To 3
                            Dim lbl As New Label
                            Dim cell As New TableCell
                            Dim hp As New HyperLink

                            If j = 1 Then
                                lbl.Text = dt.Rows(i).Item("nombre_Archivo")
                                cell.Controls.Add(lbl)
                            ElseIf j = 2 Then
                                hp.Text = "Ver"
                                hp.NavigateUrl = "WfrmConsulta.aspx?ctr1=" & dt.Rows(i).Item("Url_Archivo") & " &ctr2=" & dt.Rows(i).Item("Nombre_Archivo") & paramIsUser
                                cell.Controls.Add(hp)
                                'Else
                                '    hp.Text = "Eliminar"
                                '    hp.NavigateUrl = "WfrmConsulta.aspx?eli1=" & dt.Rows(i).Item("Id_ArchivoSolicitud") & " &eli2=" & dt.Rows(i).Item("Id_Archivo") & paramIsUser
                                '    cell.Controls.Add(hp)
                            End If
                            cell.CssClass = "normalgris"

                            row.Controls.Add(cell)
                        Next
                        row.CssClass = "normal"
                        tblArchivo.Controls.Add(row)
                    Next
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Try
            Dim cadena() As String

            pnlConsulta.Visible = False
            pnlModificacion.Visible = True

            If cmbTipoSolicitudM.SelectedItem.Text = "" Then
                MsgBox.ShowMessage("Seleccione el tipo de solicitud")
                Exit Sub
            End If

            'If cvalidacion.QuitaCaracteres(txtClienteM.Text) = "" Then
            '    MsgBox.ShowMessage("Ingrese el nombre del cliente")
            '    Exit Sub
            'End If

            Dim parametroAseguradora As String = String.Empty
            If Session("FuncionalidadC") = "SOLICITUDES" Then
                'If cvalidacion.QuitaCaracteres(txtAseguradoraM.Text) = "" Then
                '    MsgBox.ShowMessage("Ingrese la aseguradora")
                '    Exit Sub
                'End If


                If cmbTipoSolicitudM.SelectedItem.Text.IndexOf("COTIZACION") = -1 Then
                    If Not ddlAseguradoraM.SelectedItem.Value = "0" Then
                        parametroAseguradora = ddlAseguradoraM.SelectedItem.Value
                    End If
                Else
                    If Session("ConsultaCliente") = True Then
                        If Not ddlAseguradoraM.SelectedItem.Value = "0" Then
                            parametroAseguradora = ddlAseguradoraM.SelectedItem.Value
                        End If
                    Else
                        For x As Integer = 0 To cblAseguradoras.Items.Count - 1
                            If cblAseguradoras.Items(x).Selected Then
                                parametroAseguradora += cblAseguradoras.Items(x).Value & "/"
                            End If
                        Next
                        If Not parametroAseguradora = String.Empty Then
                            parametroAseguradora = parametroAseguradora.Substring(0, parametroAseguradora.Length - 1)
                        End If
                    End If
                End If
                parametroAseguradora = IIf(parametroAseguradora = String.Empty, hdnAseguradora.Value, parametroAseguradora.ToUpper())
                If parametroAseguradora = String.Empty Then
                    MsgBox.ShowMessage("Ingrese la aseguradora")
                    Exit Sub
                End If

                If cmbTipoSolicitudM.SelectedItem.Text <> "COTIZACIONES" Then
                    'If cvalidacion.QuitaCaracteres(txtSerieM.Text) = "" Then
                    '    MsgBox.ShowMessage("Ingrese el n�mero de serie")
                    '    Exit Sub
                    'End If

                    If Len(cvalidacion.QuitaCaracteres(txtSerieM.Text)) <> 17 Then
                        MsgBox.ShowMessage("El n�mero de serie es incorrecto, debe de ser de 17 car�cteres")
                        Exit Sub
                    End If
                End If
            End If

            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Solicitud = 5
            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.Funcionalidad = Session("FuncionalidadC")
            parametro.IdSolOracle = txtIdSolicitudM.Text
            parametro.TipoSolicitud = cmbTipoSolicitudM.SelectedItem.Text
            parametro.TipoEstatus = cmbStatusM.SelectedItem.Text
            parametro.TipoSolicitante = cvalidacion.QuitaCaracteres(txtSolicitanteM.Text)
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.User = Session("usuario")
            parametro.UserSol = cvalidacion.QuitaCaracteres(txtSolicitaM.Text)
            parametro.Cliente = cvalidacion.QuitaCaracteres(txtClienteM.Text)
            parametro.Aseguradora = parametroAseguradora
            parametro.NoPoliza = cvalidacion.QuitaCaracteres(txtPolizaM.Text)
            parametro.Serie = cvalidacion.QuitaCaracteres(txtSerieM.Text)
            parametro.Contrato = cvalidacion.QuitaCaracteres(txtContratoM.Text)
            parametro.FiltroCliente = Session("ConsultaCliente")

            If cvalidacion.QuitaCaracteres(Trim(txtObservaciones.Text)) = "" Then
                parametro.Observaciones = txtObservacionesL.Text
            Else
                parametro.Observaciones = txtObservacionesL.Text & vbCrLf & Now() & " " & Session("nombre") & " " & cvalidacion.QuitaCaracteres(txtObservaciones.Text)
            End If

            parametro.IdRamo = "DAN"
            parametro.LineaNegocio = "MZD"
            parametro.IdAgencia = Session("Agencia")
            parametro.BidAon = Session("BidAon")
            parametro.StrDetalle = cvalidacion.QuitaCaracteres(txtDetalleM.Text)

            If Trim(txtIdSolicitudM.Text) = "" Then
                MsgBox.ShowMessage("Consulte con su administrador el Id de la Solicitud")
                Exit Sub
            ElseIf Trim(txtIdSolicitudM.Text) = "0" Then
                MsgBox.ShowMessage("Consulte con su administrador el Id de la Solicitud")
                Exit Sub
            End If

            Dim ds As String = wsconsulta.WsTraerStore(parametro)
            cadena = ds.Split("|")
            If cadena(1) = "" Then
                Dim _comWSCorreo As New CC.Comunicacion.General.Correos
                Dim objCorreo As New CC.Modelo.Solicitudes.Correo
                Dim _bsUsuario As New CC.Negocio.Administracion.Usuarios
                Dim _comWSSolicitudes As New CC.Comunicacion.WCFSolicitudes.WCFSolicitudesClient

                With objCorreo
                    .IdSolicitud = cadena(0)
                    .Para = _bsUsuario.GetData(Session("usuario")).email
                    .CC = _comWSSolicitudes.ObtieneCorreoSolicitud(cmbTipoSolicitud.SelectedItem.Text)
                    .Detalle = parametro.StrDetalle
                    .Observaciones = parametro.Observaciones
                End With
                _comWSCorreo.EnviaCorreo(CC.General.Diccionario.Correos.Tipos.ActualizaSolicitud, objCorreo)

                MsgBox.ShowMessage("La informaci�n se guardo correctamente")
                ActualizaInformacion(txtIdSolicitudM.Text)
                'LimpiarControles()
            Else
                MsgBox.ShowMessage(cadena(1))
            End If

            MuestraArchivo(txtIdSolicitudM.Text)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExcel.Click
        Try
            'Dim resultados As String = String.Empty
            'Dim archivoReporte As String = Me.generarReporteExcel()
            'IdReporte & "|" & Session("idusuario") & "|" & Session("FuncionalidadC") & "|" & Session("IdSession")

            Dim solicitudesClass As New CC.Negocio.Consultas.Solicitudes
            Dim nombreReporte As String = solicitudesClass.Exportar(Session("FuncionalidadC"), Session("IdSession"))

            Context.Response.ClearContent()
            Context.Response.ClearHeaders()
            Context.Response.Buffer = True
            Context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Context.Response.AddHeader("content-disposition", "attachment;filename=Excel_Consulta_" + Session("FuncionalidadC") + ".xlsx")
            Context.Response.TransmitFile(nombreReporte)
            Context.Response.Flush()
            Context.Response.End()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub MatarProceso(ByVal MainWindowHandle As IntPtr, ByVal Caption As String)
        SetLastError(0)
        ' for Excel versions <10, this won't be set yet
        If IntPtr.Equals(MainWindowHandle, IntPtr.Zero) Then
            MainWindowHandle = FindWindow(Nothing, Caption)
        End If
        If IntPtr.Equals(MainWindowHandle, IntPtr.Zero) Then
            Exit Sub ' at this point, presume the window has been closed.
        End If
        Dim iRes, iProcID As Integer
        iRes = GetWindowThreadProcessId(MainWindowHandle, iProcID)
        If iProcID = 0 Then ' can�t get Process ID
            If EndTask(MainWindowHandle) <> 0 Then Exit Sub ' success
            Throw New ApplicationException("Failed to close.")
        End If
        Dim proc As System.Diagnostics.Process
        proc = System.Diagnostics.Process.GetProcessById(iProcID)
        proc.CloseMainWindow()
        proc.Refresh()
        If proc.HasExited Then Exit Sub
        proc.Kill()
    End Sub

    Declare Function EndTask Lib "user32.dll" (ByVal hWnd As IntPtr) As Integer
    Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" _
           (ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    Declare Function GetWindowThreadProcessId Lib "user32.dll" _
           (ByVal hWnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer
    Declare Function SetLastError Lib "kernel32.dll" (ByVal dwErrCode As Integer) As IntPtr

    Private Sub liberar_ObjetoCOM(ByRef objeto As Object)
        Try
            While System.Runtime.InteropServices.Marshal.ReleaseComObject(objeto) > 0
            End While
            objeto = Nothing
        Catch ex As Exception
            objeto = Nothing
        Finally
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try
    End Sub

    Private Sub cmbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click

        Try
            Dim postedFile As HttpPostedFile
            Dim NombreArchivo As String
            Dim FuenteArchivo As String

            If (Request.Files.Count > 0) Then
                postedFile = Request.Files("Archivo")
            Else
                MsgBox.ShowMessage("Seleccione el archivo")
                MuestraArchivo(Session("IdSolicitud"))
                Exit Sub
            End If

            If Archivo.Visible = True Then
                txtArchivo.Text = System.IO.Path.GetFileName(Archivo.PostedFile.FileName)
                NombreArchivo = txtArchivo.Text
                'FuenteArchivo = System.IO.Path.GetFullPath(Archivo.PostedFile.FileName)

                If txtArchivo.Text <> "" Then
                    'txtUbicacion.Text = "C:/Inetpub/wwwroot/Daimler_Solicitudes/Archivos/" & txtArchivo.Text
                    txtUbicacion.Text = System.Configuration.ConfigurationSettings.AppSettings("Raiz").ToString() & _
                        "/Archivos/" & txtArchivo.Text
                    txtArchivo.Text = "../Archivos/" & txtArchivo.Text

                    Dim strFileSave As String = txtUbicacion.Text
                    Dim iConteo As String = 0

                    While (System.IO.File.Exists(txtUbicacion.Text))
                        iConteo = iConteo + 1
                        txtUbicacion.Text = txtUbicacion.Text.Substring(0, txtUbicacion.Text.LastIndexOf(".") - IIf(iConteo > 1, 1, 0)) & iConteo.ToString() & txtUbicacion.Text.Substring(txtUbicacion.Text.LastIndexOf("."))
                    End While

                    postedFile.SaveAs(txtUbicacion.Text)

                    'Guarda el archivo en la BD
                    cprincipal.Guardar_Archivos(0, NombreArchivo, txtUbicacion.Text, Session("idusuario"), Session("IdSolicitud"))

                    MuestraArchivo(Session("IdSolicitud"))
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub ActualizaInformacion(ByVal IdSolicitud As Long)
        Try
            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            'parametro.IdSolOracle = 0            '0 = solo para insercion, otro valor > 0 es actualizaci�n
            parametro.Solicitud = 4
            parametro.Funcionalidad = Session("FuncionalidadC")

            parametro.IdSolOracle = IdSolicitud
            parametro.TipoSolicitud = IIf(CStr(cmbTipoSolicitud.SelectedItem.Text) = "-- Seleccione --", "", cmbTipoSolicitud.SelectedItem.Text)
            parametro.NoPoliza = cvalidacion.QuitaCaracteres(txtPoliza.Text)
            parametro.Serie = cvalidacion.QuitaCaracteres(txtSerie.Text)
            parametro.Contrato = cvalidacion.QuitaCaracteres(txtContrato1.Text)
            parametro.Cliente = cvalidacion.QuitaCaracteres(txtNombreCliente.Text)
            parametro.Aseguradora = IIf(ddlAseguradora.SelectedItem.Value = "0", "", ddlAseguradora.SelectedValue)
            parametro.TipoEstatus = Status
            parametro.FiltroCliente = Session("ConsultaCliente")
            'parametro.TipoSolicitante = Session("usuario")
            'parametro.User = UCase(Session("usuario"))
            If Session("TipoUsuario") = 1 Then
                parametro.TipoPerfil = Session("nombre_nivel")
            ElseIf Session("TipoUsuario") = 2 Then
                parametro.User = Session("usuario")
            End If

            Dim dt As DataTable = wsconsulta.WsTraerInformacion(parametro).Tables(0)
            If dt Is Nothing Then
                MsgBox.ShowMessage("No se tiene informaci�n")
                Exit Sub
            Else
                If dt.Rows.Count = 0 Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    If dt.Rows(0).IsNull("IdSolicitud") Then
                        txtIdSolicitudM.Text = ""
                    Else
                        txtIdSolicitudM.Text = dt.Rows(0).Item("IdSolicitud")
                        Session("IdSolicitud") = dt.Rows(0).Item("IdSolicitud")
                    End If

                    parametro.Usuario = "WSDAIMLER"
                    parametro.Password = "SOLICITUDES"
                    parametro.Funcionalidad = Session("FuncionalidadC")
                    parametro.TipoPerfil = Session("nombre_nivel")
                    parametro.Solicitud = 6
                    parametro.IdSolOracle = 0   'default

                    'Obtiene la informaci�n para el tipo de solicitud
                    Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
                    If ds Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dtTS As DataTable = ds.Tables(0)
                        If dtTS.Rows.Count > 0 Then
                            cmbTipoSolicitudM.DataSource = dtTS
                            cmbTipoSolicitudM.DataTextField = "TIPOSOLICITUD"
                            cmbTipoSolicitudM.DataValueField = "TIPOSOLICITUD"
                            cmbTipoSolicitudM.DataBind()
                        End If
                        cmbTipoSolicitudM.SelectedIndex = cmbTipoSolicitudM.Items.IndexOf(cmbTipoSolicitudM.Items.FindByValue(dt.Rows(0).Item("TipoSolicitud")))
                    End If

                    If dt.Rows(0).IsNull("Poliza") Then
                        txtPolizaM.Text = ""
                    Else
                        txtPolizaM.Text = dt.Rows(0).Item("Poliza")
                    End If
                    If dt.Rows(0).IsNull("Serie") Then
                        txtSerieM.Text = ""
                    Else
                        txtSerieM.Text = dt.Rows(0).Item("Serie")
                    End If
                    If dt.Rows(0).IsNull("Contrato") Then
                        txtContratoM.Text = ""
                    Else
                        txtContratoM.Text = dt.Rows(0).Item("Contrato")
                    End If
                    If dt.Rows(0).IsNull("Cliente") Then
                        txtClienteM.Text = ""
                    Else
                        txtClienteM.Text = dt.Rows(0).Item("Cliente")
                    End If
                    'If dt.Rows(0).IsNull("Aseguradora") Then
                    '    ddlAseguradoraM.SelectedValue = 0
                    'Else
                    '    dsdadaddlAseguradoraM.SelectedValue = dt.Rows(0).Item("Aseguradora")
                    'End If
                    If dt.Rows(0).IsNull("FECSOLICITADO") Then
                        txtFechaSolicitadoM.Text = ""
                    Else
                        txtFechaSolicitadoM.Text = dt.Rows(0).Item("FECSOLICITADO")
                    End If

                    'Estatus
                    parametro.Solicitud = 2
                    Dim dsE As DataSet = wsconsulta.WsTraerInformacion(parametro)
                    If dsE Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dtE As DataTable = dsE.Tables(0)
                        If dtE.Rows.Count > 0 Then
                            cmbStatusM.DataSource = dtE
                            cmbStatusM.DataTextField = "STATUS"
                            cmbStatusM.DataValueField = "STATUS"
                            cmbStatusM.DataBind()
                        End If
                        cmbStatusM.SelectedIndex = cmbStatusM.Items.IndexOf(cmbStatusM.Items.FindByValue(dt.Rows(0).Item("Status")))
                    End If

                    If dt.Rows(0).IsNull("FechaStatus") Then
                        txtFechaEstatusM.Text = ""
                    Else
                        txtFechaEstatusM.Text = dt.Rows(0).Item("FechaStatus")
                    End If
                    If dt.Rows(0).IsNull("USUARIOSOLICITANTE") Then
                        txtSolicitaM.Text = ""
                    Else
                        txtSolicitaM.Text = dt.Rows(0).Item("USUARIOSOLICITANTE")
                    End If
                    If dt.Rows(0).IsNull("NSSTATUS") Then
                        txtNivelServicioM.Text = ""
                    Else
                        txtNivelServicioM.Text = dt.Rows(0).Item("NSSTATUS")
                    End If
                    If dt.Rows(0).IsNull("FECCERRADO") Then
                        txtFechaCerradoM.Text = ""
                    Else
                        txtFechaCerradoM.Text = dt.Rows(0).Item("FECCERRADO")
                    End If
                    If dt.Rows(0).IsNull("NSPENDIENTE") Then
                        txtNivelServicioPendienteM.Text = ""
                    Else
                        txtNivelServicioPendienteM.Text = dt.Rows(0).Item("NSPENDIENTE")
                    End If
                    If dt.Rows(0).IsNull("NSTRAMITE") Then
                        txtNivelServicioTramiteM.Text = ""
                    Else
                        txtNivelServicioTramiteM.Text = dt.Rows(0).Item("NSTRAMITE")
                    End If
                    If dt.Rows(0).IsNull("NSCERRADO") Then
                        txtNivelServicioCierreM.Text = ""
                    Else
                        txtNivelServicioCierreM.Text = dt.Rows(0).Item("NSCERRADO")
                    End If
                    If dt.Rows(0).IsNull("FECPENDIENTE") Then
                        txtFechaPendienteM.Text = ""
                    Else
                        txtFechaPendienteM.Text = dt.Rows(0).Item("FECPENDIENTE")
                    End If
                    If dt.Rows(0).IsNull("FECTRAMITE") Then
                        txtFechaTramiteM.Text = ""
                    Else
                        txtFechaTramiteM.Text = dt.Rows(0).Item("FECTRAMITE")
                    End If
                    If dt.Rows(0).IsNull("FECCANCELADO") Then
                        txtFechaCanceladoM.Text = ""
                    Else
                        txtFechaCanceladoM.Text = dt.Rows(0).Item("FECCANCELADO")
                    End If

                    If dt.Rows(0).IsNull("PolizaNueva") Then
                        txtNuevaPolizaM.Text = ""
                    Else
                        txtNuevaPolizaM.Text = dt.Rows(0).Item("PolizaNueva")
                    End If

                    'Observaciones
                    parametro.Solicitud = 3
                    parametro.IdSolOracle = txtIdSolicitudM.Text
                    Dim dsO As DataSet = wsconsulta.WsTraerInformacion(parametro)
                    If dsO Is Nothing Then
                        MsgBox.ShowMessage("No se tiene informaci�n")
                        Exit Sub
                    Else
                        Dim dtO As DataTable = dsO.Tables(0)
                        If dtO.Rows.Count > 0 Then
                            If dtO.Rows(0).IsNull("Observaciones") Then
                                txtObservacionesL.Text = ""
                            Else
                                txtObservacionesL.Text = dtO.Rows(0).Item("Observaciones")
                            End If
                        End If
                    End If

                    MuestraArchivo(txtIdSolicitudM.Text)

                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdGeneral_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdGeneral.ItemDataBound
        Try
            Dim Color As String
            Dim i As Integer
            Dim dgItem As DataGridItem
            Dim Columna As String
            Dim Estatus As String
            Dim chk As New CheckBox
            Dim IdSolicitud As Integer
            Dim cadena As String
            Dim Bandera As String
            Dim NuevaPoliza As String

            If e.Item.ItemType = ListItemType.EditItem Then
                'si funciona
                'Nueva Poliza
                CType(e.Item.FindControl("txtnuevapoliza"), TextBox).Visible = IIf(UCase(e.Item.Cells(8).Text) = "CERRADO", True, False)
                'CType(e.Item.FindControl("txtnuevapoliza"), TextBox).Visible = IIf(UCase(e.Item.Cells(1).Text) = "COTIZACIONES", True, False)
                'CType(e.Item.FindControl("txtnuevapoliza"), TextBox).Visible = IIf(UCase(e.Item.Cells(1).Text) = "COTIZACIONES DAIMLER", True, False)
                CType(e.Item.FindControl("txtnuevapoliza"), TextBox).Text = Replace(Trim(e.Item.Cells(31).Text), "&nbsp;", "")

                'Bandera
                CType(e.Item.FindControl("chkvip"), CheckBox).Checked = IIf(UCase(e.Item.Cells(30).Text) = "S", 1, 0)
                If UCase(e.Item.Cells(8).Text) = "CERRADO" Or UCase(e.Item.Cells(8).Text) = "CANCELADO" Or UCase(e.Item.Cells(8).Text) = "CERRADO" Then
                    CType(e.Item.FindControl("chkvip"), CheckBox).Visible = False
                Else
                    CType(e.Item.FindControl("chkvip"), CheckBox).Visible = True
                End If

            End If

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.SelectedItem Then
                Dim lblRegion As Label = CType(e.Item.FindControl("lblNuevaPoliza"), Label)

                'NSPendiente
                'Columna = e.Item.Cells(13).Text
                'Fecha pendiente
                'Columna = e.Item.Cells(19).Text
                'Fecha Cancelado
                'Columna = e.Item.Cells(21).Text
                If Session("FuncionalidadC") = "SOLICITUDES" Then
                    Me.grdGeneral.Columns(2).Visible = True
                    Me.grdGeneral.Columns(3).Visible = True
                    Me.grdGeneral.Columns(4).Visible = True
                    Me.grdGeneral.Columns(6).Visible = True
                    divGrdGeneral.Style.Add("margin-left", "1%")
                ElseIf Session("FuncionalidadC") = "QUEJAS" Then
                    Me.grdGeneral.Columns(2).Visible = False
                    Me.grdGeneral.Columns(3).Visible = False
                    Me.grdGeneral.Columns(4).Visible = False
                    Me.grdGeneral.Columns(6).Visible = False
                    divGrdGeneral.Style.Add("margin-left", "10%")
                End If

                If Session("Acceso") <> "0" Then
                    Me.grdGeneral.Columns(28).Visible = False
                End If

                IdSolicitud = e.Item.Cells(0).Text
                Estatus = e.Item.Cells(8).Text
                cadena = Session("Funcionalidad") & "|" & IdSolicitud & "|" & Session("Usuario") & "|" & Estatus

                Color = e.Item.Cells(25).Text
                Select Case Color
                    Case "AMARILLO"
                        dgItem = e.Item
                        'dgItem.Attributes.CssStyle.Add("background-color", "#FF8000")
                        'dgItem.Attributes.CssStyle.Add("color", "#FFFFFF")
                        dgItem.Attributes.CssStyle.Add("color", "#FF8000")
                        dgItem.ForeColor = New System.Drawing.Color().FromArgb(255, 128, 0)
                        dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                        dgItem.Attributes.CssStyle.Add("font-weight", "bold")

                    Case "VERDE"
                        dgItem = e.Item
                        'dgItem.Attributes.CssStyle.Add("background-color", "#CCFFCC")
                        'dgItem.Attributes.CssStyle.Add("color", "#000000")
                        dgItem.Attributes.CssStyle.Add("color", "#008080")
                        dgItem.ForeColor = New System.Drawing.Color().FromArgb(0, 128, 128)
                        dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                        dgItem.Attributes.CssStyle.Add("font-weight", "bold")

                    Case "ROJO"
                        dgItem = e.Item
                        'dgItem.Attributes.CssStyle.Add("background-color", "#FF0000")
                        'dgItem.Attributes.CssStyle.Add("color", "#FFFFFF")
                        dgItem.Attributes.CssStyle.Add("color", "#FF0000")
                        dgItem.ForeColor = New System.Drawing.Color().FromArgb(255, 0, 0)
                        dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                        dgItem.Attributes.CssStyle.Add("font-weight", "bold")

                    Case "VIOLETA"
                        dgItem = e.Item
                        'dgItem.Attributes.CssStyle.Add("background-color", "#3E1573")
                        'dgItem.Attributes.CssStyle.Add("color", "#FFFFFF")
                        dgItem.Attributes.CssStyle.Add("color", "#3E1573")
                        dgItem.ForeColor = New System.Drawing.Color().FromArgb(62, 21, 115)
                        dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                        dgItem.Attributes.CssStyle.Add("font-weight", "bold")
                End Select

                'VIP
                Bandera = e.Item.Cells(30).Text
                Dim hp As HyperLink
                Dim sw As New System.IO.StringWriter
                Dim htw As New System.Web.UI.HtmlTextWriter(sw)
                hp = New HyperLink
                If Bandera = "S" Then
                    hp.ImageUrl = "..\Imagenes\general\flg-m-6.gif"
                Else
                    hp.ImageUrl = "..\Imagenes\general\vacio.gif"
                End If
                e.Item.Cells(27).Controls.Add(hp)

            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try

    End Sub

    Private Sub cmbFuncionalidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFuncionalidad.SelectedIndexChanged
        Try
            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            grdGeneral.Controls.Clear()
            grdGeneral.Visible = False
            cmdExcel.Visible = False
            hpReporte.Visible = False


            If cmbFuncionalidad.SelectedItem.Text = "-- Seleccione --" Then
                cmbTipoSolicitud.Controls.Clear()
                cmbEstatus.Controls.Clear()
            Else
                If cmbFuncionalidad.SelectedItem.Text = "SUGERENCIAS" Then
                    Session("FuncionalidadC") = "QUEJAS"
                Else
                    Session("FuncionalidadC") = cmbFuncionalidad.SelectedItem.Text
                End If

                If cmbFuncionalidad.SelectedItem.Text = "SOLICITUDES" Then
                    lblPoliza.Visible = True
                    txtPoliza.Visible = True
                    lblSerie.Visible = True
                    txtSerie.Visible = True
                    lblContrato.Visible = True
                    txtContrato1.Visible = True
                    lblAseguradora.Visible = True
                    ddlAseguradora.Visible = True
                    lblUsuario.Visible = True
                    txtUsuario.Visible = True
                Else
                    lblPoliza.Visible = False
                    txtPoliza.Visible = False
                    lblSerie.Visible = False
                    txtSerie.Visible = False
                    lblContrato.Visible = False
                    txtContrato1.Visible = False
                    lblAseguradora.Visible = False
                    ddlAseguradora.Visible = False
                    lblUsuario.Visible = False
                    txtUsuario.Visible = False
                End If

                parametro.Usuario = "WSDAIMLER"
                parametro.Password = "SOLICITUDES"
                parametro.Funcionalidad = IIf(cmbFuncionalidad.SelectedItem.Text = "SUGERENCIAS", "QUEJAS", cmbFuncionalidad.SelectedItem.Text)
                parametro.TipoPerfil = Session("nombre_nivel")
                parametro.Solicitud = 6
                parametro.IdSolOracle = 0   'default

                divRequisitos.Style.Add("display", "none")
                divRequisitos.InnerHtml = String.Empty
                'Obtiene la informaci�n para el tipo de solicitud
                Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If ds Is Nothing Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    Dim dt As DataTable = ds.Tables(0)
                    If dt.Rows.Count > 0 Then
                        cmbTipoSolicitud.DataSource = dt
                        cmbTipoSolicitud.DataTextField = "TIPOSOLICITUD"
                        cmbTipoSolicitud.DataValueField = "TIPOSOLICITUD"
                        cmbTipoSolicitud.DataBind()
                    End If
                End If

                'Estatus
                parametro.Solicitud = 8
                Dim dsE As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If dsE Is Nothing Then
                    MsgBox.ShowMessage("No se tiene informaci�n")
                    Exit Sub
                Else
                    Dim dt As DataTable = dsE.Tables(0)
                    If dt.Rows.Count > 0 Then
                        cmbEstatus.DataSource = dt
                        cmbEstatus.DataTextField = "STATUS"
                        cmbEstatus.DataValueField = "STATUS"
                        cmbEstatus.DataBind()
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub LimpiarControles()
        txtDetalleM.Text = ""
        txtDetalleM.Text = ""
        txtPolizaM.Text = ""
        txtSerieM.Text = ""
        txtContratoM.Text = ""
        txtClienteM.Text = ""
        'txtAseguradoraM.Text = ""
        ddlAseguradoraM.SelectedIndex = 0
        txtFechaEstatusM.Text = ""
        txtSolicitanteM.Text = ""
        txtSolicitaM.Text = ""
        txtFechaSolicitadoM.Text = ""
        txtNivelServicioM.Text = ""
        txtFechaCerradoM.Text = ""
        txtNivelServicioPendienteM.Text = ""
        txtNivelServicioTramiteM.Text = ""
        txtNivelServicioCierreM.Text = ""
        txtFechaPendienteM.Text = ""
        txtFechaTramiteM.Text = ""
        txtFechaCanceladoM.Text = ""
        txtSLAM.Text = ""
        txtConformeSLAM.Text = ""
        txtObservacionesL.Text = ""
        txtObservaciones.Text = ""
        txtArchivo.Text = ""
        txtUbicacion.Text = ""
        txtNuevaPolizaM.Text = ""
    End Sub

    Private Sub CargarInformacionBusqueda()
        Try
            Dim IdSolicitud As Long = 0

            If Session("FuncionalidadC") = "QUEJAS" Then
                cmbFuncionalidad.SelectedIndex = cmbFuncionalidad.Items.IndexOf(cmbFuncionalidad.Items.FindByValue("SUGERENCIAS"))
            Else
                cmbFuncionalidad.SelectedIndex = cmbFuncionalidad.Items.IndexOf(cmbFuncionalidad.Items.FindByValue(Session("Funcionalidad")))
            End If

            txtIdSolicitud.Text = Session("IdSolicitudB")
            cmbTipoSolicitud.SelectedIndex = cmbTipoSolicitud.Items.IndexOf(cmbTipoSolicitud.Items.FindByValue(Session("TipoSolicitudB")))
            txtPoliza.Text = Session("PolizaB")
            txtSerie.Text = Session("SerieB")
            txtContrato1.Text = Session("Contrato1B")
            txtUsuario.Text = Session("UsuarioB")
            txtNombreCliente.Text = Session("NombreClienteB")
            txtFechaI.Text = Session("FechaInicioB")
            txtFechaF.Text = Session("FechaFinB")
            cmbEstatus.SelectedIndex = cmbEstatus.Items.IndexOf(cmbEstatus.Items.FindByValue(Session("StatusB")))

            If txtIdSolicitud.Text <> "" Then
                IdSolicitud = txtIdSolicitud.Text
            End If

            CargarGrid(IdSolicitud, Session("TipoSolicitudB"), Session("PolizaB"), Session("SerieB"), _
            Session("Contrato1B"), Session("AseguradoraB"), Session("Usuario"), Session("StatusB"), _
            Session("FechaInicioB"), Session("FechaFinB"), Session("NombreClienteB"), Session("BidAon"), Session("Bid"), Session("ConsultaCliente"))

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdGeneral_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdGeneral.EditCommand
        Try
            Dim Color As String
            Dim i As Integer
            Dim dgItem As DataGridItem
            Dim Estatus As String
            Dim vip As String

            grdGeneral.EditItemIndex = e.Item.ItemIndex

            Color = e.Item.Cells(25).Text
            Select Case Color
                Case "AMARILLO"
                    dgItem = e.Item
                    dgItem.Attributes.CssStyle.Add("color", "#FF8000")
                    dgItem.ForeColor = New System.Drawing.Color().FromArgb(255, 128, 0)
                    dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                    dgItem.Attributes.CssStyle.Add("font-weight", "bold")
                Case "VERDE"
                    dgItem = e.Item
                    dgItem.Attributes.CssStyle.Add("color", "#008080")
                    dgItem.ForeColor = New System.Drawing.Color().FromArgb(0, 128, 128)
                    dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                    dgItem.Attributes.CssStyle.Add("font-weight", "bold")
                Case "ROJO"
                    dgItem = e.Item
                    dgItem.Attributes.CssStyle.Add("color", "#FF0000")
                    dgItem.ForeColor = New System.Drawing.Color().FromArgb(255, 0, 0)
                    dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                    dgItem.Attributes.CssStyle.Add("font-weight", "bold")
                Case "VIOLETA"
                    dgItem = e.Item
                    dgItem.Attributes.CssStyle.Add("color", "#3E1573")
                    dgItem.ForeColor = New System.Drawing.Color().FromArgb(62, 21, 115)
                    dgItem.Attributes.CssStyle.Add("font-size", "7pt")
                    dgItem.Attributes.CssStyle.Add("font-weight", "bold")
            End Select

            Estatus = e.Item.Cells(8).Text

            CargarInformacionBusqueda()
            'unica_cadena(Session("unica_cadena"), 2)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdGeneral_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdGeneral.CancelCommand
        grdGeneral.EditItemIndex = -1
        CargarInformacionBusqueda()

    End Sub

    Private Sub grdGeneral_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdGeneral.UpdateCommand
        Dim txtNuevaPoliza As TextBox = grdGeneral.Items(e.Item.ItemIndex).Cells(26).FindControl("txtNuevaPoliza")
        Dim chkVip As CheckBox = grdGeneral.Items(e.Item.ItemIndex).Cells(28).FindControl("chkVip")
        Dim IdSolicitud As Integer
        Dim Estatus As String
        Dim VIP As String
        Dim cadena() As String
        Dim NuevaPoliza As String
        Dim parametro As New WsDaimlerSolicitud.DatosEntrada
        Dim Perfil As String
        Dim actualPoliza As String
        Dim actualVIP As String

        IdSolicitud = grdGeneral.Items(e.Item.ItemIndex).Cells(0).Text
        Perfil = grdGeneral.Items(e.Item.ItemIndex).Cells(2).Text
        Estatus = grdGeneral.Items(e.Item.ItemIndex).Cells(8).Text
        NuevaPoliza = txtNuevaPoliza.Text
        actualVIP = grdGeneral.Items(e.Item.ItemIndex).Cells(30).Text
        actualPoliza = grdGeneral.Items(e.Item.ItemIndex).Cells(31).Text

        'Actualiza la p�liza
        If Estatus = "CERRADO" Then
            If NuevaPoliza <> "" Then
                If NuevaPoliza <> actualPoliza Then
                    'Actualiza la nueva p�liza
                    parametro.Solicitud = 9
                    parametro.Usuario = "WSDAIMLER"
                    parametro.Password = "SOLICITUDES"
                    parametro.IdSolOracle = IdSolicitud
                    parametro.TipoEstatus = Estatus
                    parametro.User = Session("usuario")
                    parametro.PolizaNueva = NuevaPoliza
                    parametro.Funcionalidad = Session("FuncionalidadC")
                    'parametro.IdOperacion = "0"
                    'parametro.UserSol = cvalidacion.QuitaCaracteres(txtSolicitaM.Text)

                    Dim dsP As String = wsconsulta.WsTraerStore(parametro)
                    cadena = dsP.Split("|")
                    If cadena(1) = "" Then
                        cprincipal.Actualiza_NuevaPoliza(IdSolicitud, NuevaPoliza)
                        'MsgBox.ShowMessage("La informaci�n se guardo correctamente")
                    Else
                        MsgBox.ShowMessage(cadena(1))
                    End If
                End If
            End If
            'Else
            '    If txtNuevaPoliza.Text <> "" Then
            '        MsgBox.ShowMessage("Las nuevas p�lizas solo aplican para solicitudes cerradas")
            '        txtNuevaPoliza.Text = ""
            '        Exit Sub
            '    End If
        End If

        'Actualiza la bandera
        If chkVip.Visible = True Then
            If chkVip.Checked = True Then
                VIP = "S"
            Else
                VIP = "N"
            End If

            If actualVIP <> VIP Then
                parametro.Solicitud = 10
                parametro.Usuario = "WSDAIMLER"
                parametro.Password = "SOLICITUDES"
                parametro.Funcionalidad = Session("FuncionalidadC")
                parametro.IdSolOracle = IdSolicitud
                parametro.TipoEstatus = Estatus
                parametro.User = Session("usuario")
                parametro.Bandera = VIP
                'parametro.UserSol = cvalidacion.QuitaCaracteres(txtSolicitaM.Text)

                Dim dsB As String = wsconsulta.WsTraerStore(parametro)
                cadena = dsB.Split("|")
                If cadena(1) = "" Then
                    cprincipal.Actualiza_Bandera(IdSolicitud, VIP)
                    'MsgBox.ShowMessage("La informaci�n se guardo correctamente")
                Else
                    MsgBox.ShowMessage(cadena(1))
                End If
            End If
        End If

        grdGeneral.EditItemIndex = -1
        CargarInformacionBusqueda()
    End Sub

    Private Function generarReporteExcel() As String
        'Dim i, j As Integer
        'Dim s As String = String.Empty
        'Dim nombreArchivo As String = String.Empty
        'Dim rutaArchivo, rutaAcceso As String

        'Dim window As ApplicationClass = New ApplicationClass
        'Dim libro As Workbook

        'Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        '''Para evitar error "Old format or invalid type library."
        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")


        'window.Visible = False
        'window.DisplayAlerts = False
        'window.Interactive = False
        'window.Caption = System.Guid.NewGuid.ToString.ToUpper()
        's = window.Caption

        'Try
        '    Dim idUser As Integer
        '    Dim IdSolicitud As Integer

        '    ''Rutas de disco y web            
        '    rutaArchivo = cprincipal.rutaArchivo
        '    rutaAcceso = cprincipal.rutaReporte

        '    Dim parametro As New DatosEntrada

        '    parametro.Usuario = "WSDAIMLER"
        '    parametro.Password = "SOLICITUDES"
        '    parametro.TipoPerfil = Session("nombre_nivel")
        '    'parametro.IdSolOracle = 0            '0 = solo para insercion, otro valor > 0 es actualizaci�n
        '    parametro.Solicitud = 4

        '    IdSolicitud = IIf(txtIdSolicitud.Text < 0, 0, txtIdSolicitud.Text)
        '    parametro.IdSolOracle = IIf(IdSolicitud = "", 0, IdSolicitud)
        '    parametro.TipoSolicitud = cmbTipoSolicitud.SelectedItem.Text
        '    parametro.NoPoliza = cvalidacion.QuitaCaracteres(txtPoliza.Text)
        '    parametro.Serie = cvalidacion.QuitaCaracteres(txtSerie.Text)
        '    parametro.Contrato = cvalidacion.QuitaCaracteres(txtContrato1.Text)
        '    parametro.Cliente = cvalidacion.QuitaCaracteres(txtNombreCliente.Text)
        '    parametro.User = UCase(Session("nombre"))
        '    parametro.Aseguradora = cvalidacion.QuitaCaracteres(txtAseguradora.Text)

        '    Dim dt As DataTable = wsconsulta.WsTraerInformacion(parametro).Tables(0)
        '    If dt Is Nothing Then
        '        MsgBox.ShowMessage("No se tiene informaci�n")
        '        grdGeneral.Visible = False
        '        Exit Function
        '    Else
        '        If dt.rows.Count > 0 Then
        '            nombreArchivo = "Solicitud_" & Session("IdSolicitud") & "_" & DateTime.Now.Ticks.ToString()

        '            libro = window.Workbooks.Add()
        '            ''Poner Envcabezado
        '            libro.Worksheets(1).Cells(1, 1).Value = "Reporte de Solicitudes"
        '            libro.Worksheets(1).Cells(1, 1).Resize(, 23).Merge()
        '            libro.Worksheets(1).Cells(1, 1).Font.Bold = True
        '            libro.Worksheets(1).Cells(1, 1).HorizontalAlignment = -4108

        '            libro.Worksheets(1).Cells(2, 1).Value = "Id_Solicitud"
        '            libro.Worksheets(1).Cells(2, 2).Value = "Tipo Solicitud"
        '            libro.Worksheets(1).Cells(2, 3).Value = "P�liza"
        '            libro.Worksheets(1).Cells(2, 4).Value = "Serie"
        '            libro.Worksheets(1).Cells(2, 5).Value = "Contrato"
        '            libro.Worksheets(1).Cells(2, 6).Value = "Nombre del Cliente"
        '            libro.Worksheets(1).Cells(2, 7).Value = "Aseguradora"
        '            libro.Worksheets(1).Cells(2, 8).Value = "Fecha Solicitado"
        '            libro.Worksheets(1).Cells(2, 9).Value = "Status"
        '            libro.Worksheets(1).Cells(2, 10).Value = "Fecha Status"
        '            libro.Worksheets(1).Cells(2, 11).Value = "Quien Solicita"
        '            libro.Worksheets(1).Cells(2, 12).Value = "Nivel Servicio a Status"
        '            libro.Worksheets(1).Cells(2, 13).Value = "Fecha Cerrado"
        '            libro.Worksheets(1).Cells(2, 14).Value = "Nivel Servicio Pendiente"
        '            libro.Worksheets(1).Cells(2, 15).Value = "Nivel Servicio Tr�mite"
        '            libro.Worksheets(1).Cells(2, 16).Value = "Nivel Servicio Cierre"

        '            ''Coloca valores de las solicitudes
        '            For j = 0 To dt.Rows.Count - 1
        '                'libro.Worksheets(1).Cells(j + 3, i + 1).NumberFormat = "@"
        '                'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), DateTime).ToString("dd/MM/yyyy")
        '                'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), Decimal).ToString("$ #,##0.00")
        '                'libro.Worksheets(1).Cells(j + 3, i + 1).Value = dr(i) ''& " dias"
        '                'libro.Worksheets(1).Cells(j + 3, i + 1).Value = CType(dr(i), CComun.EstatusSolicitud).ToString()

        '                libro.Worksheets(1).Cells(j + 3, 1).Value = ""
        '                libro.Worksheets(1).Cells(j + 3, 2).Value = ""
        '                libro.Worksheets(1).Cells(j + 3, 3).Value = ""
        '                libro.Worksheets(1).Cells(j + 3, 4).Value = ""

        '            Next

        '            ''Formatear celdas
        '            libro.Worksheets(1).Columns.AutoFit()
        '            libro.Worksheets(1).UsedRange().Borders.LineStyle = 1
        '            libro.Worksheets(1).UsedRange().Borders.Weight = 2
        '            libro.Worksheets(1).UsedRange().Borders.Color = RGB(0, 0, 0)
        '            'libro.Worksheets(1).PageSetup.PrintArea = "A1:X" & (j + 3)
        '            libro.Worksheets(1).PageSetup.Orientation = 2
        '            ''Dim hoja As Excel.Worksheet = libro.Worksheets(1)

        '        End If
        '    End If

        'Catch ex As Exception
        '    MsgBox.ShowMessage(ex.Message)
        '    Throw ex
        'Finally
        '    ''Termina proceso de libro de excel
        '    If (Not libro Is Nothing) Then
        '        Dim ws As Worksheet
        '        For Each ws In libro.Worksheets
        '            System.Runtime.InteropServices.Marshal.ReleaseComObject(ws)
        '            ws = Nothing
        '        Next
        '        libro.Close(True, rutaArchivo & nombreArchivo & ".xls")
        '        Me.liberar_ObjetoCOM(libro)
        '    End If
        '    If (Not window Is Nothing) Then
        '        window.Workbooks.Close()
        '        window.Quit()
        '        Me.liberar_ObjetoCOM(window)
        '    End If
        '    ''Mata proceso de excel en caso de quedar activo
        '    Dim iHandle As IntPtr = IntPtr.Zero
        '    Me.MatarProceso(iHandle, s)
        'End Try
        'Return nombreArchivo
    End Function

    Private Sub grdGeneral_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdGeneral.SelectedIndexChanged

    End Sub

    Private Sub cmbTipoSolicitud_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoSolicitud.SelectedIndexChanged
        If Not cmbTipoSolicitud.SelectedIndex = 0 Then
            Dim negSolic As New CN_Negocios.S_CnSolicitudes
            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.Solicitud = 12
            parametro.IdSolOracle = 0 'default
            parametro.TipoSolicitud = cmbTipoSolicitud.SelectedValue
            parametro.Funcionalidad = cmbFuncionalidad.SelectedValue

            Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim html As String = negSolic.ObtenerRequisitos(ds.Tables(0), parametro.TipoSolicitud)
                    divRequisitos.Style.Add("display", "block")
                    divRequisitos.InnerHtml = html
                Else
                    divRequisitos.Style.Add("display", "none")
                End If
            Else
                divRequisitos.Style.Add("display", "none")
            End If
        Else
            divRequisitos.Style.Add("display", "none")
        End If
    End Sub

    Private Sub cmbTipoSolicitudM_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoSolicitudM.SelectedIndexChanged
        If Not Session("nombre_nivel") = "CONSULTA" Then
            If cmbTipoSolicitudM.SelectedValue.IndexOf("COTIZACION") = -1 Then
                ddlAseguradoraM.Visible = True
                cblAseguradoras.Visible = False
            Else
                cblAseguradoras.Visible = True
                ddlAseguradoraM.Visible = False
            End If
            ValidaOpcPoliza()
        End If
    End Sub

    Private Sub cmdBuscar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles cmdBuscar.Command

    End Sub
    Private Sub btnUsers_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsers.Click
        Response.Redirect("../Administrador/WfrmUsuario.aspx?var=" & cprincipal.IdUsuario)
    End Sub
    Private Sub ValidaSesion()
        If Session("id_usuario") Is Nothing Then 'If Session("Usuario") = 0 Then
            Dim Pagina As String
            Pagina = "../Principal/SessionEnd.aspx?var=FS" & nomLogin.Value
            Dim userId As String
            userId = Session("id_usuario")
            iduser = IIf(userId Is Nothing, Request.QueryString("var"), userId)
            cprincipal.TerminaSesion(Integer.Parse(iduser))

            Dim sJScript1 As String = "<script language=""Javascript"">" & _
                                                " window.open('" & Pagina & "','_top')" & _
                                                "</script>"
            Response.Write(sJScript1)

        End If
    End Sub

    Private Sub ValidaOpcPoliza()
        If cmbTipoSolicitudM.SelectedItem.Text = "Cotizaci�n" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "Emisi�n" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "Inclusi�n de unidad en cat�logo-modelos anteriores" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "Inclusi�n de unidad en cat�logo-tipos y subtipos nuevos" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "Reporte de errores en cotizador-emisor AON" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "Sugerencias para mejoras del programa" Then
            Session("VerPoliza") = False
        ElseIf cmbTipoSolicitudM.SelectedItem.Text = "ENDOSOS" Then
            Session("VerPoliza") = False
        Else
            Session("VerPoliza") = True
        End If

        lblPolizaM.Visible = Session("VerPoliza")
        txtPolizaM.Visible = Session("VerPoliza")
    End Sub
End Class
