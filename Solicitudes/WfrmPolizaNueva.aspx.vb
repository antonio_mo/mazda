Imports Daimler_Solicitudes.WsDaimlerSolicitud
Partial Class WfrmPolizaNueva
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private wsconsulta As New WsDaimlerSolicitud.WsConexionSoapClient

    Private Funcionalidad As String
    Private IdSolicitud As Integer
    Private Usuario As String
    Private Estatus As String
    Private Cadena As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        lblPoliza.CssClass = "NegritaMediana"
        txtPoliza.CssClass = "Caja_Texto"

        If Not Page.IsPostBack Then
            If Not Request.QueryString("cad") Is Nothing Then
                Dim Valores() As String
                Valores = Split(Request.QueryString("cad"), "|")

                Funcionalidad = Valores(0)
                IdSolicitud = Valores(1)
                Usuario = Valores(2)
                Estatus = Valores(3)

            End If
        End If
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Try
            Dim cadena() As String
            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Solicitud = 5
            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.Funcionalidad = Funcionalidad
            parametro.IdSolOracle = IdSolicitud
            parametro.User = Usuario
            parametro.TipoEstatus = Estatus


            If Trim(IdSolicitud) = "" Then
                MsgBox.ShowMessage("No se puede guardar la p�liza nueva, consulte con su administrador")
                Exit Sub
            ElseIf Trim(IdSolicitud) = "0" Then
                MsgBox.ShowMessage("No se puede guardar la p�liza nueva, consulte con su administrador")
                Exit Sub
            End If

            Dim ds As String = wsconsulta.WsTraerStore(parametro)
            cadena = ds.Split("|")
            If cadena(1) = "" Then
                MsgBox.ShowMessage("La informaci�n se guardo correctamente")
                Dim sJScript As String = "<script language=""Javascript"">" & _
                                                    " window.open('WfrmConsulta.aspx?band=1','_parent'); " & _
                                                    " window.top.hidePopWin(true); " & _
                                                    " parent.location.reload();" & _
                                                   "</script>"
                Response.Write(sJScript)
            Else
                MsgBox.ShowMessage(cadena(1))
            End If


        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Try
            Dim sJScript As String = "<script language=""Javascript"">" & _
                                    " window.open('WfrmConsulta.aspx?band=1','_parent'); " & _
                                    " window.top.hidePopWin(true); " & _
                                    " parent.location.reload();" & _
                                   "</script>"
            Response.Write(sJScript)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
