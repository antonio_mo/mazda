<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmPolizaNueva.aspx.vb" Inherits="Daimler_Solicitudes.WfrmPolizaNueva"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmPolizaNueva</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="../Javascript/Jscripts.js"></script>
        <%--<script language="javascript"  type="text/javascript">
            window.onunload = function (event) {
                if (((window.event.clientX || event.clientX) < 0) || ((window.event.clientY || event.clientY) < 0)) {
                    window.open('../Usuario/SessionEnd.aspx?var=FXD'+<%= nomLogin.Value%>, 'popup', 'width=400,height=70')
                }

            }
        </script>--%>
		<LINK rel="stylesheet" type="text/css" href="../Css/Styles.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout" leftMargin="5" rightMargin="5">
		<form id="Form1" method="post" runat="server">
			<asp:Panel style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Panel1" runat="server"
				Width="100%" Height="88px">
				<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="100%">
					<TR>
						<TD height="30" width="25%"></TD>
						<TD height="30" width="50%"></TD>
						<TD height="30" width="25%"></TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:Label style="Z-INDEX: 0" id="lblPoliza" runat="server">P�liza :</asp:Label></TD>
						<TD>
							<asp:TextBox style="Z-INDEX: 0" id="txtPoliza" tabIndex="1" onkeypress="convierteMayusculas();"
								runat="server" Width="100%" MaxLength="25"></asp:TextBox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD height="30"></TD>
						<TD height="30"></TD>
						<TD height="30"></TD>
					</TR>
					<TR>
						<TD>
							</TD>
						<TD align="center">
							<asp:button style="Z-INDEX: 0" id="cmdGuardar" tabIndex="2" runat="server" Text="Guardar" CssClass="boton"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button style="Z-INDEX: 0" id="cmdSalir" tabIndex="2" runat="server" Text="Salir" CssClass="boton"></asp:button></TD>
						<TD></TD>
					</TR>
                    <cc1:msgbox id="MsgBox" style="POSITION: absolute; LEFT: 16px; Z-INDEX: 102; TOP: 2064px" runat="server"></cc1:msgbox>
				</TABLE>
			</asp:Panel>
		</form>
	</body>
</HTML>
