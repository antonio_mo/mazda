Imports Daimler_Solicitudes.WsDaimlerSolicitud
Imports CN_Negocios
Partial Class WfrmSolicitud
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private wsconsulta As New WsDaimlerSolicitud.WsConexionSoapClient

    Private cprincipal As New S_CnPrincipal
    Private Scprincipal As New CnPrincipal
    Private cvalidacion As New S_CnValidacion
    Private iduser As String
    Dim paramIsUser As String 'valor del parametro de la sesion

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Dim Nombre As String
        Dim Liga As String

        'If Session("IdSession") Is Nothing Then
        '    Dim sJScript As String = "<script language=""Javascript"">" & _
        '            "window.open('../wfrmlogooff.aspx?bantiempo=1','_parent')"     & _
        '            "</script>"
        '    Response.Write(sJScript)
        '    Exit Sub
        'End If

        'lblTitulo.Text = "SOLICITUDES DE MOVIMIENTOS DE FINANCIAMIENTO AUTOMOTRIZ"

        'lblUsuarioI.Text = "Usuario: " & Session("usuario")
        'lblUsuarioI.CssClass = "NegritaMediana"
        'lblNivel.Text = "Perfil: " & Session("nombre_nivel")
        'lblNivel.CssClass = "NegritaMediana"

        lblSubtitulo.Text = "SOLICITUDES"
        regresar.Visible = False

        If Session("nombre_nivel") = "AGENCIA" Then
            Session("Funcionalidad") = "QUEJAS"
        End If

        CargarMenu()



        If Not Page.IsPostBack Then
            Session("VerPoliza") = True
            Controles()
            ''Seccion agregada para controlar una solo si la sesion sigue activa
            'Dim userId As String
            'userId = Session("id_usuario")
            'idUser = IIf(userId Is Nothing, Request.QueryString("var"), userId)
            'If Me.nomLogin.Value = String.Empty Then
            '    Me.nomLogin.Value = IIf(iduser Is Nothing, "0", iduser) ' ****Fin de session
            'End If
            'paramIsUser = "var=" & Me.nomLogin.Value
            'ValidaSesion()
            'fin de seccion
            LimpiarControles()

            If Not Request.QueryString("ini") Is Nothing Then
                tblArchivo.Controls.Clear()
                Session("IdArchivoSolicitud") = Nothing
            End If

            If Session("IdArchivoSolicitud") Is Nothing Then
                'Obtiene el IdArchivoSolicitud para la carga de los archivos
                Dim dt As DataTable = cprincipal.Obtiene_IdArchivoSolicitud(0)
                If dt.Rows.Count > 0 Then
                    Session("IdArchivoSolicitud") = dt.Rows.Item(0)("Id_ArchivoSolicitud")
                End If
            End If

            If Not Request.QueryString("ctr") Is Nothing Then
                If Request.QueryString("ctr") = 1 Then
                    Session("Funcionalidad") = "SOLICITUDES"
                    Response.Redirect("WfrmSolicitud.aspx?ini=1" & paramIsUser)
                ElseIf Request.QueryString("ctr") = 2 Then
                    Session("Funcionalidad") = "QUEJAS"
                    Response.Redirect("WfrmSolicitud.aspx?ini=1" & paramIsUser)
                ElseIf Request.QueryString("ctr") = 3 Then
                    Session("Funcionalidad") = "SOLICITUDES"
                    Response.Redirect("WfrmConsulta.aspx?" & paramIsUser)
                ElseIf Request.QueryString("ctr") = 4 Then
                    '    Response.Redirect("../Administrador/WfrmUsuario.aspx")
                    'Else
                    'antes cierra sesiones y cambia estatus 
                    cprincipal.TerminaSesion(Integer.Parse(Me.nomLogin.Value))
                    Session.Clear()
                    Response.Redirect("../WfrmLogin.aspx")
                End If
            End If

            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.Solicitud = 11
            parametro.IdSolOracle = 0   'default

            Dim dsAseguradora As DataSet = wsconsulta.WsTraerInformacion(parametro)
            If dsAseguradora Is Nothing Then
                MsgBox.ShowMessage("No se tienen aseguradoras")
                Exit Sub
            Else
                Dim dtAseguradora As DataTable = dsAseguradora.Tables(0)
                If dtAseguradora.Rows.Count > 0 Then
                    ddlAseguradora.Items.Clear()
                    ddlAseguradora.Items.Add(New ListItem("-- Seleccione --", "0"))
                    ddlAseguradora.DataSource = dtAseguradora
                    ddlAseguradora.DataTextField = "abreviatura"
                    ddlAseguradora.DataValueField = "value"
                    ddlAseguradora.DataBind()
                    ddlAseguradora.Visible = False

                    For Each dr As DataRow In dtAseguradora.Rows
                        dr("abreviatura") = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dr("abreviatura").ToString().ToLower())
                    Next

                    dtAseguradora.Rows(0).Delete()
                    cblAseguradoras.DataSource = dtAseguradora
                    cblAseguradoras.DataTextField = "abreviatura"
                    cblAseguradoras.DataValueField = "value"
                    cblAseguradoras.DataBind()
                    cblAseguradoras.Visible = False

                    If cmbTipoSolicitud.SelectedItem Is Nothing Then
                        ddlAseguradora.Visible = True
                    Else
                        If cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 Then
                            ddlAseguradora.Visible = True
                        Else
                            cblAseguradoras.Visible = True
                        End If
                    End If
                End If
            End If

            If Session("Funcionalidad") = "QUEJAS" Then
                lblSubtitulo.Text = "SUGERENCIAS Y COMENTARIOS"
                lblSerieOb.Visible = False
                lblClienteOb.Visible = False
                ddlAseguradora.Visible = False
                lblAseguradoraObj.Visible = False
            Else
                lblSubtitulo.Text = Session("Funcionalidad")
            End If

            CargarControles()
        Else
            Controles()
            ValidaSesion()

        End If

        If Not Request.QueryString("Ctr1") Is Nothing Then
            Nombre = Request.QueryString("Ctr2")
            If Not Request.QueryString("Ctr1") Is Nothing Then
                Liga = Request.QueryString("Ctr1")
            End If
            sd.FileName = Nombre
            sd.File = Liga
            sd.ShowDialogBox()
        End If

        If Not Request.QueryString("eli1") Is Nothing Then
            cprincipal.Eliminar_Archivos(Request.QueryString("eli1"), Request.QueryString("eli2"))
            MuestraArchivo()
        End If

    End Sub

    Private Sub CargarMenu()
        Try
            Dim i As Integer
            Dim row As New TableRow

            For i = 1 To 5
                Dim lbl As New Label
                Dim hp As New HyperLink
                Dim cell As New TableCell

                If i = 1 Then
                    If Session("nombre_nivel") <> "AGENCIA" Then
                        hp.Text = "Solicitudes"
                        hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=1" & paramIsUser
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                        row.Controls.Add(cell)
                    End If

                    'ElseIf i = 2 Then
                    '    If Session("nombre_nivel") <> "AONDAIMLER" Then
                    '        If Session("nombre_nivel") <> "AGENCIA" Then
                    '            If Session("Quejas") <> "0" Then
                    '                lbl.Text = "|"
                    '                lbl.CssClass = "NegritaMediana"
                    '                cell.Controls.Add(lbl)
                    '                row.Controls.Add(cell)
                    '            End If
                    '        End If
                    '    End If

                    'ElseIf i = 3 Then
                    '    If Session("nombre_nivel") <> "AONDAIMLER" Then
                    '        If Session("Quejas") <> "0" Then
                    '            hp.Text = "S&C"
                    '            hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=2" & paramIsUser
                    '            hp.CssClass = "NegritaMediana"
                    '            cell.Controls.Add(hp)
                    '            row.Controls.Add(cell)
                    '        End If
                    '    End If

                ElseIf i = 4 Then
                    lbl.Text = "|"
                    lbl.CssClass = "NegritaMediana"
                    cell.Controls.Add(lbl)
                    row.Controls.Add(cell)
                ElseIf i = 5 Then
                    If Session("nombre_nivel") <> "AGENCIA" Then
                        hp.Text = "Consulta"
                        hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=3" & paramIsUser
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                        row.Controls.Add(cell)
                    End If
                End If

                'ElseIf i = 6 Then
                '    If Session("nombre_nivel") <> "AGENCIA" Then
                '        lbl.Text = "|"
                '        lbl.CssClass = "NegritaMediana"
                '        cell.Controls.Add(lbl)
                '        row.Controls.Add(cell)
                '    End If

                'ElseIf i = 7 Then
                '    hp.Text = "Salir"
                '    hp.NavigateUrl = "WfrmSolicitud.aspx?ctr=4" & paramIsUser
                '    hp.CssClass = "NegritaMediana"
                '    cell.Controls.Add(hp)
                '    row.Controls.Add(cell)
                'End If
                tblTitulo.Controls.Add(row)
            Next

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Controles()
        lblTitulo.CssClass = "Titulo"
        lblSubtitulo.CssClass = "SubTitulo"
        lblTipoSolicitud.CssClass = "NegritaMediana"
        lblDetalle.CssClass = "NegritaMediana"
        lblIdSolicitud.CssClass = "NegritaMediana"
        lblSolicita.CssClass = "NegritaMediana"
        lblStatus.CssClass = "NegritaMediana"
        lblFechaStatus.CssClass = "NegritaMediana"

        lblOtrosMovimientos.CssClass = "NegritaGrande"
        lblPoliza.CssClass = "NegritaMediana"
        lblSerie.CssClass = "NegritaMediana"
        lblContrato.CssClass = "NegritaMediana"
        lblNombreCliente.CssClass = "NegritaMediana"
        lblAseguradora.CssClass = "NegritaMediana"
        lblObservaciones.CssClass = "NegritaMediana"
        lblArchivos.CssClass = "NegritaMediana"

        lblFechaSolicitado.CssClass = "NegritaMediana"
        lblFechaPendiente.CssClass = "NegritaMediana"
        lblFechaTramite.CssClass = "NegritaMediana"
        lblFechaCerrado.CssClass = "NegritaMediana"
        lblFechaCancelado.CssClass = "NegritaMediana"
        lblFechaAlta.CssClass = "NegritaMediana"
        lblUsuario.CssClass = "NegritaMediana"
        lblStatus.CssClass = "NegritaMediana"

        cmbTipoSolicitud.CssClass = "Caja_Texto"
        txtIdSolicitud.CssClass = "Caja_Texto"
        txtDetalle.CssClass = "Caja_Texto"
        txtSolicita.CssClass = "Caja_Texto"
        cmbEstatus.CssClass = "Caja_Texto"
        txtFechaEstatus.CssClass = "Caja_Texto"
        txtNombreCliente.CssClass = "Caja_Texto"
        ddlAseguradora.CssClass = "Caja_Texto"
        txtObservaciones.CssClass = "Caja_Texto"
        txtPoliza.CssClass = "Caja_Texto"
        txtSerie.CssClass = "Caja_Texto"
        txtContrato.CssClass = "Caja_Texto"

        txtFechaSolicitado.CssClass = "Caja_Texto"
        txtFechaPendiente.CssClass = "Caja_Texto"
        txtFechaTramite.CssClass = "Caja_Texto"
        txtFechaCerrado.CssClass = "Caja_Texto"
        txtFechaCancelado.CssClass = "Caja_Texto"
        txtFechaAlta.CssClass = "Caja_Texto"
        txtUsuario.CssClass = "Caja_Texto"

        If Session("Funcionalidad") = "SOLICITUDES" Then
            lblFechaStatus.Visible = True
            lblOtrosMovimientos.Visible = True
            lblPoliza.Visible = Session("VerPoliza")
            lblSerie.Visible = True
            lblContrato.Visible = True
            lblNombreCliente.Visible = True
            lblAseguradora.Visible = True
            lblObservaciones.Visible = True
            txtFechaEstatus.Visible = True
            txtNombreCliente.Visible = True

            If cmbTipoSolicitud.SelectedItem Is Nothing Then
                ddlAseguradora.Visible = True
                cblAseguradoras.Visible = False
            Else
                If cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 Then
                    ddlAseguradora.Visible = True
                    cblAseguradoras.Visible = False
                Else
                    cblAseguradoras.Visible = True
                    ddlAseguradora.Visible = False
                End If
            End If
            txtObservaciones.Visible = True
            txtPoliza.Visible = Session("VerPoliza")
            txtSerie.Visible = True
            txtContrato.Visible = True
            lblSerieOb.Visible = True
            lblClienteOb.Visible = True
            lblAseguradoraObj.Visible = True
        Else
            lblFechaStatus.Visible = False
            lblOtrosMovimientos.Visible = False
            lblPoliza.Visible = False
            lblSerie.Visible = False
            lblContrato.Visible = False
            lblAseguradora.Visible = False
            lblObservaciones.Visible = False
            txtFechaEstatus.Visible = False
            ddlAseguradora.Visible = False
            cblAseguradoras.Visible = False
            txtObservaciones.Visible = False
            txtPoliza.Visible = False
            txtSerie.Visible = False
            txtContrato.Visible = False
            lblSerieOb.Visible = False
            lblClienteOb.Visible = False
            lblAseguradoraObj.Visible = False
        End If

    End Sub

    Private Sub CargarControles()
        Try
            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.TipoPerfil = Session("nombre_nivel")
            parametro.Solicitud = 6
            parametro.IdSolOracle = 0   'default
            parametro.Funcionalidad = Session("Funcionalidad")

            'Obtiene la informaci�n para el tipo de solicitud
            Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
            If ds Is Nothing Then
                MsgBox.ShowMessage("No se tiene informaci�n")
                Exit Sub
            Else
                Dim dt As DataTable = ds.Tables(0)
                If dt.Rows.Count > 0 Then
                    cmbTipoSolicitud.DataSource = dt
                    cmbTipoSolicitud.DataTextField = "TIPOSOLICITUD"
                    cmbTipoSolicitud.DataValueField = "TIPOSOLICITUD"
                    cmbTipoSolicitud.DataBind()
                End If

                'If cmbTipoSolicitud.SelectedItem.Text = "COTIZACIONES" Then
                '    lblCotizaciones.Visible = True
                'Else
                '    lblCotizaciones.Visible = False
                'End If
            End If


            'Quien solicita 
            txtSolicita.Text = Session("nombre_nivel")

            'Estatus
            parametro.Solicitud = 2
            cmbEstatus.Enabled = False
            Dim dsE As DataSet = wsconsulta.WsTraerInformacion(parametro)
            If dsE Is Nothing Then
                MsgBox.ShowMessage("No se tiene informaci�n")
                Exit Sub
            Else
                Dim dtE As DataTable = dsE.Tables(0)
                If dtE.Rows.Count > 0 Then
                    cmbEstatus.DataSource = dtE
                    cmbEstatus.DataTextField = "STATUS"
                    cmbEstatus.DataValueField = "STATUS"
                    cmbEstatus.DataBind()
                End If
                cmbEstatus.SelectedIndex = cmbEstatus.Items.IndexOf(cmbEstatus.Items.FindByValue("SOLICITADO"))
            End If

            MuestraArchivo()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click
        Try
            Dim postedFile As HttpPostedFile
            If (Request.Files.Count > 0) Then
                postedFile = Request.Files("Archivo")
            Else
                MsgBox.ShowMessage("Seleccione el archivo")
                MuestraArchivo()
                Exit Sub
            End If

            Dim NombreArchivo As String
            Dim FuenteArchivo As String



            'If Archivo.PostedFile.ContentLength = 0 Then
            '    MsgBox.ShowMessage("Seleccione el archivo")
            '    MuestraArchivo()
            '    Exit Sub
            'End If

            If Archivo.Visible = True Then
                txtArchivo.Text = System.IO.Path.GetFileName(postedFile.FileName)
                NombreArchivo = txtArchivo.Text
                'FuenteArchivo = System.IO.Path.GetFullPath(Archivo.PostedFile.FileName)

                If txtArchivo.Text <> "" Then
                    'txtUbicacion.Text = "C:/Inetpub/wwwroot/Daimler_Solicitudes/Archivos/" & txtArchivo.Text
                    If Not System.IO.Directory.Exists(System.Configuration.ConfigurationSettings.AppSettings("Raiz").ToString() & "/Archivos/") Then
                        System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationSettings.AppSettings("Raiz").ToString() & "/Archivos/")
                    End If
                    txtUbicacion.Text = System.Configuration.ConfigurationSettings.AppSettings("Raiz").ToString() & _
                        "/Archivos/" & txtArchivo.Text
                    txtArchivo.Text = "../Archivos/" & txtArchivo.Text

                    'Guarda el archivo fisicamente
                    ' If Not postedFile Is Nothing And postedFile.ContentLength > 0 Then
                    'System.IO.File.Copy(FuenteArchivo, txtUbicacion.Text, True)
                    Dim strFileSave As String = txtUbicacion.Text
                    Dim iConteo As String = 0

                    While (System.IO.File.Exists(txtUbicacion.Text))
                        iConteo = iConteo + 1
                        txtUbicacion.Text = txtUbicacion.Text.Substring(0, txtUbicacion.Text.LastIndexOf(".") - IIf(iConteo > 1, 1, 0)) & iConteo.ToString() & txtUbicacion.Text.Substring(txtUbicacion.Text.LastIndexOf("."))
                    End While



                    postedFile.SaveAs(txtUbicacion.Text)
                    'End If

                    'Guarda el archivo en la BD
                    cprincipal.Guardar_Archivos(Session("IdArchivoSolicitud"), NombreArchivo, txtUbicacion.Text, Session("id_usuario"), 0)

                    MuestraArchivo()
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Private Sub ddlAseguradora_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAseguradora.SelectedIndexChanged
    '    Try
    '        MsgBox.ShowMessage("Llego")
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub cmbTipoSolicitud_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoSolicitud.SelectedIndexChanged
        Try
            Session("VerPoliza") = True

            If cmbTipoSolicitud.SelectedItem.Text = "Cotizaci�n" Then
                lblSerieOb.Visible = False
                lblClienteOb.Visible = False
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "Emisi�n" Then
                'lblSerieOb.Visible = False
                'lblClienteOb.Visible = False
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "Inclusi�n de unidad en cat�logo-modelos anteriores" Then
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "Inclusi�n de unidad en cat�logo-tipos y subtipos nuevos" Then
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "Reporte de errores en cotizador-emisor AON" Then
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "Sugerencias para mejoras del programa" Then
                lblPoliza.Visible = False
                lblPolizaOb.Visible = False
                txtPoliza.Visible = False
                Session("VerPoliza") = False
            ElseIf cmbTipoSolicitud.SelectedItem.Text = "ENDOSOS" Then
                lblSerieOb.Visible = False
                lblClienteOb.Visible = False
                Session("VerPoliza") = False
            Else
                txtPoliza.Visible = True
                lblSerieOb.Visible = True
                lblClienteOb.Visible = True
                Session("VerPoliza") = True
            End If

            lblPoliza.Visible = Session("VerPoliza")
            lblPolizaOb.Visible = Session("VerPoliza")

            If Not Session("Funcionalidad") = "QUEJAS" Then
                If cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 Then
                    ddlAseguradora.Visible = True
                    cblAseguradoras.Visible = False
                Else
                    ddlAseguradora.Visible = False
                    cblAseguradoras.Visible = True
                End If
            End If

            'If cmbTipoSolicitud.SelectedItem.Text = "--Seleccione--" Then
            '    MsgBox.ShowMessage("Seleccione el tipo de solicitud")
            '    Exit Sub
            'Else
            'End If

            If Not cmbTipoSolicitud.SelectedIndex = 0 Then
                Dim negSolic As New CN_Negocios.S_CnSolicitudes
                Dim parametro As New WsDaimlerSolicitud.DatosEntrada

                parametro.Usuario = "WSDAIMLER"
                parametro.Password = "SOLICITUDES"
                parametro.TipoPerfil = Session("nombre_nivel")
                parametro.Solicitud = 12
                parametro.IdSolOracle = 0 'default
                parametro.TipoSolicitud = cmbTipoSolicitud.SelectedValue
                parametro.Funcionalidad = Session("Funcionalidad")

                Dim ds As DataSet = wsconsulta.WsTraerInformacion(parametro)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim html As String = negSolic.ObtenerRequisitos(ds.Tables(0), parametro.TipoSolicitud)
                        divRequisitos.Style.Add("display", "block")
                        divRequisitos.InnerHtml = html
                    Else
                        divRequisitos.Style.Add("display", "none")
                    End If
                Else
                    divRequisitos.Style.Add("display", "none")
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub



    Private Sub MuestraArchivo()
        Try
            tblArchivo.Controls.Clear()
            'Muestra el archivo en la tabla tblArchivo
            If Session("IdArchivoSolicitud") <> "0" Then
                Dim dt As DataTable = cprincipal.Obtiene_Archivos(Session("IdArchivoSolicitud"), 0)
                If dt.Rows.Count > 0 Then
                    'Encabezado
                    Dim j As Integer
                    Dim i As Integer

                    For i = 0 To dt.Rows.Count - 1
                        If i = 0 Then
                            Dim rowT As New TableRow
                            For j = 1 To 3
                                Dim lblT As New Label
                                Dim cellT As New TableCell

                                If j = 1 Then
                                    lblT.Text = "Archivo"
                                    cellT.Controls.Add(lblT)
                                ElseIf j = 2 Then
                                    lblT.Text = "Abrir"
                                    cellT.Controls.Add(lblT)
                                Else
                                    lblT.Text = "&nbsp;"
                                    cellT.Controls.Add(lblT)
                                End If
                                cellT.CssClass = "negritagris"
                                rowT.Controls.Add(cellT)
                            Next
                            tblArchivo.Controls.Add(rowT)
                        End If

                        Dim row As New TableRow
                        For j = 1 To 3
                            Dim lbl As New Label
                            Dim cell As New TableCell
                            Dim hp As New HyperLink

                            If j = 1 Then
                                lbl.Text = dt.Rows(i).Item("nombre_Archivo")
                                cell.Controls.Add(lbl)
                            ElseIf j = 2 Then
                                hp.Text = "Ver"
                                hp.NavigateUrl = "WfrmSolicitud.aspx?ctr1=" & dt.Rows(i).Item("Url_Archivo") & " &ctr2=" & dt.Rows(i).Item("Nombre_Archivo") & paramIsUser
                                cell.Controls.Add(hp)
                            Else
                                hp.Text = "Eliminar"
                                hp.NavigateUrl = "WfrmSolicitud.aspx?eli1=" & dt.Rows(i).Item("Id_ArchivoSolicitud") & " &eli2=" & dt.Rows(i).Item("Id_Archivo") & paramIsUser
                                cell.Controls.Add(hp)
                            End If
                            cell.CssClass = "normalgris"

                            row.Controls.Add(cell)
                        Next
                        row.CssClass = "normal"
                        tblArchivo.Controls.Add(row)
                    Next
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles regresar.Click
        LimpiarControles()
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Try
            regresar.Visible = True
            'regresar.Attributes.Add("onclick", "javascript:LimpiarControles();")
            ValidaSesion()
            Dim cadena() As String

            If cmbTipoSolicitud.SelectedItem.Text = "-- Seleccione --" Then
                MsgBox.ShowMessage("Seleccione el tipo de solicitud")
                Exit Sub
            End If

            If cmbEstatus.SelectedItem.Text = "" Then
                MsgBox.ShowMessage("Seleccione el estatus")
                Exit Sub
            End If

            If (ddlAseguradora.SelectedValue = "0" And cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 And ddlAseguradora.Visible) Or _
                (cblAseguradoras.SelectedIndex = -1 And Not cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 And cblAseguradoras.Visible) Then
                MsgBox.ShowMessage("Seleccione una aseguradora")
                Exit Sub
            End If

            If Session("Funcionalidad") = "SOLICITUDES" Then
                'If cvalidacion.QuitaCaracteres(txtAseguradora.Text) = "" Then
                '    MsgBox.ShowMessage("Ingrese la aseguradora")
                '    Exit Sub
                'End If

                If cmbTipoSolicitud.SelectedItem.Text = "COTIZACIONES" Then
                    lblSerieOb.Visible = False
                    lblClienteOb.Visible = False

                ElseIf cmbTipoSolicitud.SelectedItem.Text = "ACLARACION DOCUMENTACION ART.140" Then
                    lblSerieOb.Visible = False
                    lblClienteOb.Visible = False

                ElseIf cmbTipoSolicitud.SelectedItem.Text = "COTIZACIONES DAIMLER" Then
                    lblSerieOb.Visible = False
                    lblClienteOb.Visible = False

                Else
                    lblSerieOb.Visible = True
                    lblClienteOb.Visible = True

                    If cvalidacion.QuitaCaracteres(txtSerie.Text) = "" Then
                        MsgBox.ShowMessage("Ingrese el n�mero de serie")
                        Exit Sub
                    End If

                    If Len(cvalidacion.QuitaCaracteres(txtSerie.Text)) <> 17 Then
                        MsgBox.ShowMessage("El n�mero de serie es incorrecto, debe de ser de 17 car�cteres")
                        Exit Sub
                    End If

                    If cvalidacion.QuitaCaracteres(txtNombreCliente.Text) = "" Then
                        MsgBox.ShowMessage("Ingrese el nombre del cliente")
                        Exit Sub
                    End If
                End If
            End If


            Dim parametro As New WsDaimlerSolicitud.DatosEntrada

            parametro.Solicitud = 5
            parametro.Usuario = "WSDAIMLER"
            parametro.Password = "SOLICITUDES"
            parametro.Funcionalidad = Session("Funcionalidad")
            parametro.IdSolOracle = 0   'default
            parametro.TipoSolicitud = cmbTipoSolicitud.SelectedItem.Text
            parametro.TipoEstatus = cmbEstatus.SelectedItem.Text
            parametro.TipoSolicitante = cvalidacion.QuitaCaracteres(txtSolicita.Text)
            parametro.UserSol = UCase(Session("nombre"))
            parametro.IdAgencia = Session("bid")
            parametro.BidAon = Session("bid")
            parametro.Cliente = cvalidacion.QuitaCaracteres(txtNombreCliente.Text)

            If cmbTipoSolicitud.SelectedItem.Text.IndexOf("COTIZACION") = -1 Then
                parametro.Aseguradora = ddlAseguradora.SelectedValue 'cvalidacion.QuitaCaracteres(txtAseguradora.Text)    
            Else
                parametro.Aseguradora = String.Empty
                For x As Integer = 0 To cblAseguradoras.Items.Count - 1
                    If cblAseguradoras.Items(x).Selected Then
                        parametro.Aseguradora += cblAseguradoras.Items(x).Value.ToString() & "/"
                    End If
                Next
                parametro.Aseguradora = parametro.Aseguradora.Substring(0, parametro.Aseguradora.Length - 1)
            End If

            parametro.NoPoliza = cvalidacion.QuitaCaracteres(txtPoliza.Text)
            parametro.IdRamo = "DAN"
            parametro.LineaNegocio = "MZD"
            parametro.IniVig = ""
            parametro.FinVig = ""
            parametro.Serie = cvalidacion.QuitaCaracteres(txtSerie.Text)
            parametro.Contrato = cvalidacion.QuitaCaracteres(txtContrato.Text)
            parametro.NoEndoso = ""
            parametro.PolRenovacion = ""
            parametro.PrimaNeta = 0
            parametro.PrimaTotal = 0
            parametro.Archivo = ""
            parametro.Observaciones = Now() & " " & Session("usuario") & " " & cvalidacion.QuitaCaracteres(txtObservaciones.Text)
            parametro.Email = ""
            parametro.User = Session("usuario")
            parametro.Email = Session("EmailUsuario")
            parametro.StrDetalle = cvalidacion.QuitaCaracteres(txtDetalle.Text)
            parametro.TipoPerfil = Session("nombre_nivel")

            If Session("usuario") = "" Then
                MsgBox.ShowMessage("Consulte con su administrador la informaci�n del usuario")
                Exit Sub
            ElseIf Session("usuario") Is Nothing Then
                MsgBox.ShowMessage("Consulte con su administrador la informaci�n del usuario")
                Exit Sub
            End If

            Dim ds As String = wsconsulta.WsTraerStore(parametro)
            cadena = ds.Split("|")
            txtIdSolicitud.Text = cadena(0)
            txtFechaEstatus.Text = cadena(2)

            If txtIdSolicitud.Text <> "" Then
                cprincipal.Actualiza_Archivo(Session("IdArchivoSolicitud"), txtIdSolicitud.Text)
            End If

            If cadena(1) = "" Then
                Dim _comWSCorreo As New CC.Comunicacion.General.Correos
                Dim objCorreo As New CC.Modelo.Solicitudes.Correo
                Dim _bsUsuario As New CC.Negocio.Administracion.Usuarios
                Dim _comWSSolicitudes As New CC.Comunicacion.WCFSolicitudes.WCFSolicitudesClient

                With objCorreo
                    .IdSolicitud = cadena(0)
                    .Para = _bsUsuario.GetData(Session("usuario")).email
                    .CC = _comWSSolicitudes.ObtieneCorreoSolicitud(cmbTipoSolicitud.SelectedItem.Text)
                    .Detalle = parametro.StrDetalle
                    .Observaciones = parametro.Observaciones
                End With
                _comWSCorreo.EnviaCorreo(CC.General.Diccionario.Correos.Tipos.ConfirmaSolicitud, objCorreo)
                MsgBox.ShowMessage("La informaci�n se guardo correctamente.\n\nID de solicitud: " + cadena(0))
                cmdGuardar.Enabled = False
            Else
                MsgBox.ShowMessage(cadena(1))
            End If

            MuestraArchivo()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try

    End Sub

    Private Sub LimpiarControles()
        cmbTipoSolicitud.SelectedIndex = 0
        txtIdSolicitud.Text = ""
        txtDetalle.Text = ""
        txtSolicita.Text = ""
        txtFechaEstatus.Text = ""
        txtNombreCliente.Text = ""
        'txtAseguradora.Text = ""
        ddlAseguradora.SelectedIndex = 0
        txtObservaciones.Text = ""
        txtPoliza.Text = ""
        txtSerie.Text = ""
        txtContrato.Text = ""
        txtFechaSolicitado.Text = ""
        txtFechaPendiente.Text = ""
        txtFechaTramite.Text = ""
        txtFechaCerrado.Text = ""
        txtFechaCancelado.Text = ""
        txtFechaAlta.Text = ""
        txtUsuario.Text = ""

        Session("IdArchivoSolicitud") = Nothing
        MuestraArchivo()
    End Sub
    Private Sub ValidaSesion()
        If Session("id_usuario") Is Nothing Then 'If Session("Usuario") = 0 Then
            Dim Pagina As String
            Pagina = "../Principal/WfrmLogin.aspx?var=FS" & nomLogin.Value
            Dim userId As String
            userId = Session("id_usuario")
            'Session("id_usuario") = userId
            iduser = IIf(userId Is Nothing, Request.QueryString("var"), userId)

            cprincipal.TerminaSesion(Integer.Parse(iduser))
            Dim sJScript1 As String = "<script language=""Javascript"">" & _
                                            " window.open('" & Pagina & "','_top')" & _
                                            "</script>"
            Response.Write(sJScript1)
        End If
    End Sub

    Protected Sub txtPoliza_TextChanged(sender As Object, e As EventArgs) Handles txtPoliza.TextChanged
        If (txtPoliza.Text <> String.Empty) Then
            txtSerie.Text = String.Empty
            txtContrato.Text = String.Empty
            txtNombreCliente.Text = String.Empty
            ddlAseguradora.SelectedIndex = 0

            Dim objPoliza As CC.Modelo.Solicitudes.Poliza = New CC.Negocio.Consultas.Solicitudes().GetPol(txtPoliza.Text, Session("bid"))
            If (Not objPoliza Is Nothing) Then
                txtSerie.Text = objPoliza.NumeroSerie
                txtContrato.Text = objPoliza.Contrato
                txtNombreCliente.Text = objPoliza.NombreCliente + IIf(objPoliza.APaternoCliente Is Nothing, String.Empty, " " + objPoliza.APaternoCliente) + IIf(objPoliza.AMaternoCliente Is Nothing, String.Empty, " " + objPoliza.AMaternoCliente)
                ddlAseguradora.SelectedValue = ddlAseguradora.Items.FindByText(objPoliza.Aseguradora).Value

                Dim sol As New CC.Negocio.Consultas.Solicitudes()
                If (Not sol.PolizaDerivada(txtPoliza.Text)) Then
                    MsgBox.ShowMessage("La P�liza " + txtPoliza.Text + " no se encuentra en Sistema Administrador")
                    txtPoliza.Text = String.Empty
                    txtSerie.Text = String.Empty
                    txtContrato.Text = String.Empty
                    txtNombreCliente.Text = String.Empty
                    ddlAseguradora.SelectedIndex = 0
                Else
                    Dim _comSolicitud As New CC.Comunicacion.Solicitudes.Poliza()
                    If (Not _comSolicitud.ValidaPoliza(txtPoliza.Text)) Then
                        txtPoliza.Text = String.Empty
                        MsgBox.ShowMessage("L� p�liza no es valida")
                    End If
                End If
            Else
                MsgBox.ShowMessage("L� p�liza no existe en el emisor")
            End If
        End If
    End Sub

    Protected Sub cmdLimpCamp_Click(sender As Object, e As EventArgs) Handles cmdLimpCamp.Click
        LimpiarControles()
    End Sub
End Class
