﻿Public Class Solicitudes
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Session("IdUser") Is Nothing Then 'If Session("Usuario") = 0 Then
                Dim Pagina As String
                Pagina = "../Usuario/SessionEnd.aspx?var=" '& idUser
                'If nomLogin.Value <> "0" Or nomLogin.Value <> String.Empty Then
                '    'admon.TerminaSesion(Integer.Parse(idUser)) 'Fin de session   Revisar si aqui si va
                'End If
                Dim sJScript1 As String = "<script language=""Javascript"">" & _
                                                " window.open('" & Pagina & "','_top')" & _
                                                "</script>"
                Response.Write(sJScript1)

            End If
        End If
        Try
            lblTitulo.Text = "SOLICITUDES DE MOVIMIENTOS DE FINANCIAMIENTO AUTOMOTRIZ"
            lblUsuarioI.Text = "Usuario: " & Session("nombre")
            lblUsuarioI.CssClass = "NegritaMediana"
            lblNivel.Text = "Perfil: " & Session("Perfil")
            lblNivel.CssClass = "NegritaMediana"

            lblSubtitulo.Text = "CONSULTA"
            Dim i As Integer
            Dim row As New TableRow
            ' agregar al menu seccion admnistración de usuarios
            If Session("Acceso") = "3" Or Session("Acceso") = "4" Then
                For i = 1 To 5
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim cell As New TableCell

                    If i = 1 Then
                        hp.Text = "Consulta"
                        hp.NavigateUrl = "../Principal/WfrmConsulta.aspx?ctr=3"
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                    ElseIf i = 2 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)

                    ElseIf i = 3 Then
                        If Session("Perfil") = "ADMINISTRADOR" Then
                            hp.Text = "Usuarios"
                            hp.NavigateUrl = "WFrmUsers.aspx"
                            hp.CssClass = "NegritaMediana"
                            cell.Controls.Add(hp)
                        End If
                    ElseIf i = 4 Then
                        If Session("Perfil") = "ADMINISTRADOR" Then
                            lbl.Text = "|"
                            lbl.CssClass = "NegritaMediana"
                            cell.Controls.Add(lbl)
                        End If
                    ElseIf i = 5 Then
                        hp.Text = "Salir"
                        hp.NavigateUrl = "../Principal/WfrmConsulta.aspx?ctr=4"
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                    End If
                    row.Controls.Add(cell)
                    tblTitulo.Controls.Add(row)
                Next
            Else
                For i = 1 To 9
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim cell As New TableCell

                    If i = 1 Then
                        If Session("Perfil") <> "AGENCIA" Then
                            hp.Text = "Solicitudes"
                            hp.NavigateUrl = "../Principal/WfrmSolicitud.aspx?ctr=1"
                            hp.CssClass = "NegritaMediana"
                            cell.Controls.Add(hp)
                            row.Controls.Add(cell)
                        End If

                    ElseIf i = 2 Then
                        If Session("Perfil") <> "AONDAIMLER" Then
                            If Session("Perfil") <> "AGENCIA" Then
                                If Session("Quejas") <> "0" Then
                                    lbl.Text = "|"
                                    lbl.CssClass = "NegritaMediana"
                                    cell.Controls.Add(lbl)
                                    row.Controls.Add(cell)
                                End If
                            End If
                        End If

                    ElseIf i = 3 Then
                        If Session("Perfil") <> "AONDAIMLER" Then
                            If Session("Quejas") <> "0" Then
                                hp.Text = "S&C"
                                hp.NavigateUrl = "../Principal/WfrmSolicitud.aspx?ctr=2"
                                hp.CssClass = "NegritaMediana"
                                cell.Controls.Add(hp)
                                row.Controls.Add(cell)
                            End If
                        End If

                    ElseIf i = 4 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)
                        row.Controls.Add(cell)
                    ElseIf i = 5 Then
                        hp.Text = "Consulta"
                        hp.NavigateUrl = "../Principal/WfrmSolicitud.aspx?ctr=3"
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                        row.Controls.Add(cell)
                    ElseIf i = 6 Then
                        lbl.Text = "|"
                        lbl.CssClass = "NegritaMediana"
                        cell.Controls.Add(lbl)
                        row.Controls.Add(cell)

                    ElseIf i = 7 Then
                        If Session("Perfil") = "ADMINISTRADOR" Then
                            hp.Text = "Usuarios"
                            hp.NavigateUrl = "WFrmUsers.aspx"
                            hp.CssClass = "NegritaMediana"
                            cell.Controls.Add(hp)
                            row.Controls.Add(cell)
                        End If
                    ElseIf i = 8 Then
                        If Session("Perfil") = "ADMINISTRADOR" Then
                            lbl.Text = "|"
                            lbl.CssClass = "NegritaMediana"
                            cell.Controls.Add(lbl)
                            row.Controls.Add(cell)
                        End If
                    ElseIf i = 9 Then
                        hp.Text = "Salir"
                        hp.NavigateUrl = "../Principal/WfrmSolicitud.aspx?ctr=4"
                        hp.CssClass = "NegritaMediana"
                        cell.Controls.Add(hp)
                        row.Controls.Add(cell)
                    End If

                    tblTitulo.Controls.Add(row)
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class