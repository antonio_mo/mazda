<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmConsultaGeneral.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmConsultaGenera" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table2" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD colSpan="4">
						<asp:panel id="pnl_deeler" runat="server" Width="100%">
							<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="obligatorio" width="25%">Seleccione Distribuidor :</TD>
									<TD width="50%">
										<asp:DropDownList id="cbo_deeler" runat="server" Width="100%" CssClass="combos_small"></asp:DropDownList></TD>
									<TD width="25%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
<%--				<TR>
					<TD class="Header_Gris" width="25%">Busqueda</TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="15%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" CssClass="combos_small" Width="100%"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="80%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" CssClass="combos_small" Width="488px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="3">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="4">Marca</asp:ListItem>
										<asp:ListItem Value="5">Modelo</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
									</asp:checkboxlist>
									<asp:checkboxlist id="chkLCriterioM" runat="server" CssClass="combos_small" Width="164px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="2">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>--%>
				<TR>
					<TD></TD>
					<TD>
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Text="Consultar"></asp:button></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
							<TD colSpan="5">
								<DIV id="divGrdGeneral" style="MARGIN-LEFT: 5%" runat="server">
									<asp:datagrid id="grdGeneral" runat="server" Height="120px" Width="900px" BorderWidth="1px" BorderStyle="Solid"
										ForeColor="Transparent" BackColor="White" Font-Size="9pt" Font-Names="Tahoma,Arial,Helvetica,sans-serif"
										BorderColor="#7BB8D9" CellPadding="3" AutoGenerateColumns="False" Font-Name="Tahoma,Arial,Helvetica,sans-serif">
										<FooterStyle ForeColor="Black"></FooterStyle>
										<SelectedItemStyle Font-Size="7pt" ForeColor="Black"></SelectedItemStyle>
										<EditItemStyle ForeColor="Black"></EditItemStyle>
										<AlternatingItemStyle Font-Size="7pt" HorizontalAlign="Center" ForeColor="Black" VerticalAlign="Middle"
											BackColor="#DDE4CB"></AlternatingItemStyle>
										<ItemStyle Font-Size="7pt" HorizontalAlign="Center" ForeColor="Black" VerticalAlign="Middle"
											BackColor="Transparent"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="Gray"></HeaderStyle>
										<Columns>
											<%--0--%>
											<asp:BoundColumn DataField="IdSolicitud" ReadOnly="True" HeaderText="Id_Solicitud">
												<HeaderStyle HorizontalAlign="Center" Width="10pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--1--%>
											<asp:BoundColumn DataField="TipoSolicitud" ReadOnly="True" HeaderText="Tipo&#160;Solicitud&#160;&#160;&#160;&#160;&#160;">
												<HeaderStyle HorizontalAlign="Center" Width="200pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--2--%>
											<asp:BoundColumn DataField="Poliza" ReadOnly="True" HeaderText="P&#243;liza">
												<HeaderStyle HorizontalAlign="Center" Width="20pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--3--%>
											<asp:BoundColumn DataField="Serie" ReadOnly="True" HeaderText="Serie">
												<HeaderStyle HorizontalAlign="Center" Width="30pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--4--%>
											<asp:BoundColumn DataField="Contrato" ReadOnly="True" HeaderText="Contrato">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--5--%>
											<asp:BoundColumn DataField="Cliente" ReadOnly="True" HeaderText="Nombre&#160;del&#160;Cliente&#160;&#160;&#160;&#160;&#160;&#160;">
												<HeaderStyle Width="200pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--6--%>
											<asp:BoundColumn DataField="Aseguradora" ReadOnly="True" HeaderText="Aseguradora">
												<HeaderStyle Width="40px"></HeaderStyle>
											</asp:BoundColumn>
											<%--7--%>
											<asp:BoundColumn DataField="FECSOLICITADO" ReadOnly="True" HeaderText="Fecha_Solicitado" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="120px"></HeaderStyle>
											</asp:BoundColumn>
											<%--8--%>
											<asp:BoundColumn DataField="Status" ReadOnly="True" HeaderText="Status">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--9--%>
											<asp:BoundColumn DataField="FechaStatus" ReadOnly="True" HeaderText="Fecha_Status" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--10--%>
											<asp:BoundColumn DataField="USUARIOSOLICITANTE" ReadOnly="True" HeaderText="Quien_Solicita">
												<HeaderStyle Width="30px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--11--%>
											<asp:BoundColumn Visible="False" DataField="NSSTATUS" ReadOnly="True" HeaderText="Nivel Servicio a Status">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--12--%>
											<asp:BoundColumn DataField="FECCERRADO" ReadOnly="True" HeaderText="Fecha_Cerrado" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--13--%>
											<asp:BoundColumn Visible="False" DataField="NSPENDIENTE" ReadOnly="True" HeaderText="Nivel Servicio Pendiente">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--14--%>
											<asp:BoundColumn Visible="False" DataField="NSTRAMITE" ReadOnly="True" HeaderText="Nivel Servicio Tr&#225;mite">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--15--%>
											<asp:BoundColumn Visible="False" DataField="NSCERRADO" ReadOnly="True" HeaderText="Nivel Servicio Cierre">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--16--%>
											<asp:BoundColumn Visible="False" DataField="Solicitante" ReadOnly="True" HeaderText="Solicitante"></asp:BoundColumn>
											<%--17--%>
											<asp:BoundColumn Visible="False" DataField="Agencia" ReadOnly="True" HeaderText="Agencia"></asp:BoundColumn>
											<%--18--%>
											<asp:BoundColumn DataField="BidAon" ReadOnly="True" HeaderText="BidAon"></asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--19--%>
											<asp:BoundColumn Visible="False" DataField="FecPendiente" ReadOnly="True" HeaderText="Fecha_Pendiente"
												DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--20--%>
											<asp:BoundColumn Visible="False" DataField="FecTramite" ReadOnly="True" HeaderText="Fecha_Tr&#225;mite"
												DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--21--%>
											<asp:BoundColumn Visible="False" DataField="FecCancelado" ReadOnly="True" HeaderText="Fecha_Cancelado">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--22--%>
											<asp:BoundColumn Visible="False" DataField="Detalle" ReadOnly="True" HeaderText="Detalle"></asp:BoundColumn>
											<%--23--%>
											<asp:BoundColumn DataField="SLA" ReadOnly="True" HeaderText="SLA"></asp:BoundColumn>
											<%--24--%>
											<asp:BoundColumn DataField="ConformeSLA" ReadOnly="True" HeaderText="ConformeSLA"></asp:BoundColumn>
											<%--25--%>
											<asp:BoundColumn Visible="False" DataField="Color" ReadOnly="True" HeaderText="Color"></asp:BoundColumn>
											<%--26--%>
											<asp:TemplateColumn HeaderText="Poliza Nueva">
												<ItemTemplate>
													<asp:Label id=lblNuevaPoliza runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PolizaNueva") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id="txtNuevaPoliza" runat="server" MaxLength="25"></asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<%--27--%>
											<asp:TemplateColumn HeaderText="VIP">
												<ItemTemplate>
													<asp:HyperLink style="Z-INDEX: 0" id="hpVip1" runat="server"></asp:HyperLink>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:CheckBox style="Z-INDEX: 0" id="chkvip" runat="server"></asp:CheckBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<%--28--%>
<%--											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Actualizar" HeaderText="Asignar" CancelText="Cancelar"
												EditText="Editar"></asp:EditCommandColumn>--%>
											<%--29--%>
<%--											<asp:ButtonColumn Text="&lt;img src='..\Imagenes\grid01.gif' border='0'&gt;" HeaderText="Modif." CommandName="Select">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:ButtonColumn>--%>
											<%--30--%>
											<asp:BoundColumn Visible="False" DataField="VIP" ReadOnly="True"></asp:BoundColumn>
											<%--31--%>
											<asp:BoundColumn Visible="False" DataField="PolizaNueva" ReadOnly="True"></asp:BoundColumn>
										</Columns>
									</asp:datagrid></DIV>
							</TD>
						</TR>
			</TABLE>
			<TABLE id="Table4" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="2%" height="20" class="negrita_marco_color" vAlign="middle" align="center">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Consulta de Solicitudes</FONT></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 103; LEFT: 352px; POSITION: absolute; TOP: 776px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
