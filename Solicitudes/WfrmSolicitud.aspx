<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmSolicitud.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmSolicitud" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>Solicitudes de Movimientos Mercedes Benz Insurance</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="../JavaScript/JScripts.js"></script>
        <script language="javascript"  type="text/javascript">
            window.onunload = function (event) {
                if (((window.event.clientX || event.clientX) < 0) || ((window.event.clientY || event.clientY) < 0)) {
                    window.open('../Principal/wfrmlogooff.aspx?var=FXD' +<%= nomLogin.Value%> 'popup', 'width=400,height=70')
                    }
            }
            function Reload() {
                window.opener.document.location.reload()
            }
        </script>
		<LINK href="../Css/Requisitos.css" type="text/css" rel="stylesheet">
		<LINK rel="stylesheet" type="text/css" href="../Css/Styles.css">
        <style type="text/css">
            .style19
            {
                height: 22px;
            }
        </style>
  </HEAD>
	<body leftMargin="5" rightMargin="5" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			&nbsp;
			<asp:panel style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 0px" id="pnlGeneral" runat="server"
				Width="100%" Height="496px">
<asp:panel id=pnlPanel1 runat="server" Width="100%">
<TABLE id=Table1 cellSpacing=1 cellPadding=1 width="100%" border=0>
  <TR>
    <TD width="20%">
<%--<asp:image id=Aon runat="server" CssClass="aon-logo" ImageUrl="../Imagenes/aon-logo" AlternateText="Aons" BorderWidth="0"></asp:image>--%></TD>
    <TD width="2%"></TD>
    <TD align=center width="50%"></TD>
    <TD align=right width="20%" colSpan=2><asp:HiddenField ID="nomLogin" runat="server" EnableViewState="true" Visible="false" />      </TD></TR>
  <TR>
    <TD width="20%" class="style19"></TD>
    <TD width="2%" class="style19"></TD>
    <TD align=center width="50%" class="style19">
<asp:Label id=lblTitulo runat="server"></asp:Label></TD>
    <TD width="20%" class="style19"></TD>
    <TD width="10%" class="style19"></TD></TR>
  <TR>
    <TD width="20%"></TD>
    <TD width="2%"></TD>
    <TD align=right width="50%" colSpan=3>
<asp:Label id=lblUsuarioI runat="server"></asp:Label></TD></TR>
  <TR>
    <TD width="20%"></TD>
    <TD width="2%"></TD>
    <TD align=right width="50%" colSpan=3>
<asp:Label id=lblNivel runat="server"></asp:Label></TD></TR>
  <TR>
    <TD width="20%"></TD>
    <TD width="2%"></TD>
    <TD align=right width="50%" colSpan=3>
<asp:Table id=tblTitulo runat="server"></asp:Table></TD></TR>
  <TR>
    <TD width="20%" height=40></TD>
    <TD width="2%" height=40></TD>
    <TD style="HEIGHT: 35px" vAlign=top align=center width="50%" class="obligatorio,normal">
<asp:Label id=lblSubtitulo runat="server"></asp:Label></TD>
    <TD width="20%" height=40></TD>
    <TD width="10%" height=40></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblTipoSolicitud runat="server">Tipo solicitud :</asp:Label></TD>
    <TD>*</TD>
    <TD>
<asp:DropDownList id=cmbTipoSolicitud tabIndex=1 runat="server" Width="80%" AutoPostBack="True"></asp:DropDownList></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD vAlign=top align=right class="obligatorio">
<asp:Label id=lblDetalle runat="server">Descripci�n tipo de solicitud :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtDetalle tabIndex=2 runat="server" Width="100%" MaxLength="250" TextMode="MultiLine"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblIdSolicitud runat="server">Id Solicitud :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtIdSolicitud tabIndex=3 runat="server" Width="50%" ReadOnly="True"></asp:TextBox></TD>
    <TD width="80%" rowSpan=12>
      <TABLE>
        <TR>
          <TD width="10%"></TD>
          <TD>
            <DIV class=Requisitos id=divRequisitos style="DISPLAY: none" 
            runat="server"></DIV></TD></TR></TABLE></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblSolicita runat="server">Quien Solicita :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtSolicita tabIndex=4 runat="server" Width="60%" MaxLength="20" ReadOnly="True"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>  
    <TD align=right class="obligatorio">
<asp:Label id=lblStatus runat="server">Status :</asp:Label></TD>
    <TD>
<asp:Label id=lblStatusOb runat="server">*</asp:Label></TD>
    <TD>
<asp:DropDownList id=cmbEstatus  sel="1" tabIndex=6 runat="server"></asp:DropDownList></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaStatus runat="server" Visible="False">Fecha Status :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaEstatus tabIndex=7 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD>
</TD>
    <TD></TD></TR>
  <TR>
    <TD align=right></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD align=center class="obligatorio,normal"><asp:Label id=lblOtrosMovimientos runat="server">Otros Datos</asp:Label></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblPoliza runat="server">P�liza :</asp:Label></TD>
    <TD>
        <asp:Label id=lblPolizaOb runat="server">*</asp:Label></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtPoliza tabIndex=8 runat="server" Width="50%" MaxLength="25" AutoPostBack="True"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD 
    align=right class="obligatorio">
<asp:Label id=lblSerie runat="server">Serie :</asp:Label></TD>
    <TD>
<asp:Label id=lblSerieOb runat="server">*</asp:Label></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtSerie tabIndex=9 runat="server" Width="50%" MaxLength="17"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblContrato runat="server">Contrato :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtContrato tabIndex=10 runat="server" Width="50%" MaxLength="20"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblNombreCliente runat="server">Nombre Cliente :</asp:Label></TD>
    <TD>
<asp:Label id=lblClienteOb runat="server">*</asp:Label></TD>
    <TD>
<asp:TextBox onkeypress=convierteMayusculas(event); id=txtNombreCliente tabIndex=11 runat="server" Width="80%" MaxLength="150"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblAseguradora runat="server">Aseguradora :</asp:Label></TD>
    <TD>
<asp:Label id=lblAseguradoraObj runat="server">*</asp:Label></TD>
    <TD>
      <P>
<asp:DropDownList id=ddlAseguradora tabIndex=12 runat="server" Width="80%" Visible="False" AutoPostBack="True"></asp:DropDownList></P>
      <P>
<asp:CheckBoxList id=cblAseguradoras runat="server" RepeatLayout="Table" RepeatDirection="Vertical" VISIBLE="False" CssClass="CblStyle" RepeatColumns="5">
</asp:CheckBoxList></P></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD vAlign=top align=right class="obligatorio">
<asp:Label id=lblObservaciones runat="server">Observaciones :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox onkeypress="convierteMayusculas(event);" id=txtObservaciones tabIndex=13 runat="server" Height="64px" Width="100%" MaxLength="250" TextMode="MultiLine"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD vAlign=top align=right class="obligatorio">
        <asp:Label id=lblArchivos runat="server">Archivos :</asp:Label>
    </TD>      
      <td />
    <TD>
                
        <input class="Caja_Texto" id="Archivo" style="WIDTH: 70%; HEIGHT: 22px" tabIndex="14" type="file" runat="server"/> 
        <asp:button id=cmbAgregar tabIndex=24 runat="server" CssClass="boton" Text="Agregar"></asp:button>
                
    </TD>
    <TD></TD>
    <TD>
        <asp:TextBox id=txtArchivo tabIndex=7 runat="server" Width="20px" Visible="False"></asp:TextBox>
        <asp:TextBox id=txtUbicacion tabIndex=7 runat="server" Width="20px" Visible="False"></asp:TextBox></TD>
  </TR>
    <tr>
        <td></td>
        <td></td>
        <td><asp:Label id=Label1 runat="server">Puede adjuntar archivos en formato PDF, GIF, JPG, DOC, XLS o PPT.</asp:Label></td>
    </tr>
  <TR>
    <TD align=right></TD>
    <TD></TD>
    <TD>
<asp:Table id=tblArchivo runat="server" Width="100%"></asp:Table></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right></TD>
    <TD>*</TD>
    <TD class=NegritaMediana>Campos obligatorios</TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right>
<ontranet:SaveDialog id=sd runat="server"></ontranet:SaveDialog></TD>
    <TD></TD>
    <TD>
        <asp:button id="cmdGuardar" tabIndex="15" runat="server" CssClass="boton" Text="Guardar"></asp:button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:button id="cmdLimpCamp" tabIndex="15" runat="server" CssClass="boton" Text="Limpiar Campos"></asp:button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:button id=regresar tabIndex=16 runat="server" CssClass="boton" Text="Nueva Solicitud" OnClientClick="LimpiarControles();"></asp:button>
    </TD>
    <TD></TD>
    <TD></TD></TR></TABLE></asp:panel>
<asp:panel id=pnlPanel3 runat="server" Width="100%">
<TABLE id=Table6 cellSpacing=1 cellPadding=1 width="100%" border=0>
  <TR>
    <TD align=right width="20%" class="obligatorio">
<asp:Label id=lblFechaSolicitado runat="server" Visible="False">Fecha solicitado :</asp:Label></TD>
    <TD width="2%"></TD>
    <TD width="50%">
<asp:TextBox id=txtFechaSolicitado tabIndex=17 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD width="20%"></TD>
    <TD width="10%"></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaPendiente runat="server" Visible="False">Fecha pendiente :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaPendiente tabIndex=18 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaTramite runat="server" Visible="False">Fecha en tr�mite :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaTramite tabIndex=19 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaCerrado runat="server" Visible="False">Fecha cerrado :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaCerrado tabIndex=20 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaCancelado runat="server" Visible="False">Fecha cancelado : </asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaCancelado tabIndex=21 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblFechaAlta runat="server" Visible="False">Fecha alta :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtFechaAlta tabIndex=22 runat="server" Width="40%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD align=right class="obligatorio">
<asp:Label id=lblUsuario runat="server" Visible="False">Usuario :</asp:Label></TD>
    <TD></TD>
    <TD>
<asp:TextBox id=txtUsuario tabIndex=23 runat="server" Width="50%" Visible="False" Enabled="False"></asp:TextBox></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD class=NegritaMediana></TD>
    <TD></TD>
    <TD></TD></TR>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD>
    <TD></TD></TR></TABLE></asp:panel>
			</asp:panel>
            <cc1:msgbox id="MsgBox" style="POSITION: absolute; LEFT: 16px; Z-INDEX: 102; TOP: 2064px" runat="server"></cc1:msgbox>
            </form>
	</body>
</HTML>
