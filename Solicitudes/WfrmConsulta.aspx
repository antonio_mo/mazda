<%@ OutputCache Location="None" VaryByParam="None" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmConsulta.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmConsulta" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Solicitudes de Movimientos Mercedes Benz Insurance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="../Javascript/Jscripts.js"></script>
		<LINK href="../css/submodal.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="../Javascript/submodalsource.js"></script>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<LINK href="../Css/Requisitos.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			//document.all ("divScroll").style.pixelLeft=divScroll.scrollLeft * -1;			
		}
		
		function PonDiagonal()		
		{
			
			var stCadena=document.Consulta.txtFechaI.value;							
			if (IsNumeric(stCadena))
			{
				if(stCadena.length == 2)
				{
					document.Consulta.txtFechaI.value=document.Consulta.txtFechaI.value+"/"
				}
				if(stCadena.length == 5)
				{
					document.Consulta.txtFechaI.value=document.Consulta.txtFechaI.value+"/"
				}	
			}
			else
			{
				document.Consulta.txtFechaI.value='';
			}			
		}
		function PonDiagonal1()
		{
			var stCadena=document.Consulta.txtFechaF.value;
			if (IsNumeric(stCadena))
			{
				if(stCadena.length == 2)
				{
					document.Consulta.txtFechaF.value=document.Consulta.txtFechaF.value+"/"
				}
				if(stCadena.length == 5)
				{
					document.Consulta.txtFechaF.value=document.Consulta.txtFechaF.value+"/"
				}	
			}
			else
			{
				document.Consulta.txtFechaF.value='';
			}
		}
		function IsNumeric(sText)
		{
			var ValidChars = "0123456789/";
			var IsNumber=true;
			var Char;
			
			for (i = 0; i < sText.length && IsNumber == true; i++) 
			{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
				{
					IsNumber = false;
				}
			}
			return IsNumber;
		}
		
		function CapturaPoliza(Cadena)
		{									
			var x="WfrmPolizaNueva.aspx?cad="+Cadena+""								
			var y="WfrmConsulta.aspx?band=1"
			initPopUp(y);
			showPopWin(x, 350, 150, '<font color="#FFFFFF" face=arial><b>Captura de P�liza Nueva</b></font>', null);
			return false;							
		}	
		window.onunload = function (event) {
		    if (((window.event.clientX || event.clientX) < 0) || ((window.event.clientY || event.clientY) < 0)) {
		        //window.open('../Principal/SessionEnd.aspx?var=FXD'+<%= nomLogin.Value%>, 'popup', 'width=400,height=70')
                }

		}
		
		</script>
	</HEAD>
	<body leftMargin="5" rightMargin="5" MS_POSITIONING="GridLayout">
		<form id="Consulta" method="post" runat="server">
			&nbsp;
			<asp:panel id="pnlGeneral" style="POSITION: absolute; LEFT: 0px; Z-INDEX: 101; TOP: 0px" runat="server"
				Width="100%" Height="136px">
				<asp:panel id="pnlConsulta" runat="server" Width="100%">
					<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD width="20%">
								<%--<asp:image id="Aon" runat="server" CssClass="aon-logo" BorderWidth="0" AlternateText="Aons"
									ImageUrl="../Imagenes/aon-logo.png"></asp:image>--%></TD>
							<TD width="2%"></TD>
							<TD width="50%" align="center"></TD>
							<TD width="20%" colSpan="2" align="right">
                                <asp:HiddenField ID="nomLogin" runat="server" EnableViewState="true" Visible="false" />
							</TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" align="center">
								<asp:Label id="lblTitulo" runat="server"></asp:Label></TD>
							<TD width="20%"></TD>
							<TD width="10%"><asp:Button ID="btnUsers" runat="server" Text="Usuarios" Visible="false" style="height: 26px" /></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Label id="lblUsuarioI" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Label id="lblNivel" runat="server"></asp:Label></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Table id="tblTitulo" runat="server"></asp:Table></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD style="HEIGHT: 35px" vAlign="top" width="50%" align="center">
								<asp:Label id="lblSubtitulo" runat="server"></asp:Label></TD>
							<TD width="20%"></TD>
							<TD width="10%"></TD>
						</TR>
						<TR>
							<TD width="20%" align="right">
								<asp:Label id="lblFuncionalidad" runat="server">Funcionalidad :</asp:Label></TD>
							<TD width="2%"></TD>
							<TD width="50%" align="left">
								<asp:DropDownList tabIndex="1" id="cmbFuncionalidad" runat="server" AutoPostBack="True"></asp:DropDownList></TD>
							<TD rowSpan="12" width="45%">
								<DIV id="divRequisitos" class="Requisitos" style="DISPLAY: none" runat="server"></DIV>
							</TD>
							<TD width="10%"></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblIdSolicitud" runat="server">Id Solicitud :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="2" id="txtIdSolicitud" runat="server"
									Width="30%" MaxLength="25"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblTipoSolicitud" runat="server">Tipo de Solicitud :</asp:Label></TD>
							<TD></TD>
							<TD align="left">
								<asp:DropDownList tabIndex="3" id="cmbTipoSolicitud" runat="server" Width="80%" AutoPostBack="True"></asp:DropDownList></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblPoliza" runat="server">P�liza :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="4" onkeypress="convierteMayusculas();" id="txtPoliza" runat="server" Width="50%"
									MaxLength="25"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblSerie" runat="server">Serie :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="5" onkeypress="convierteMayusculas();" id="txtSerie" runat="server" Width="50%"
									MaxLength="20"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblContrato" runat="server">Contrato :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="6" onkeypress="convierteMayusculas();" id="txtContrato1" runat="server"
									Width="20%" MaxLength="20"></asp:TextBox>&nbsp;&nbsp;
								<asp:Label id="lblEntre" runat="server" Visible="False">entre</asp:Label>&nbsp;&nbsp;
								<asp:TextBox tabIndex="7" onkeypress="convierteMayusculas();" id="txtContrato2" runat="server"
									Width="20%" Visible="False"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNombreCliente" runat="server">Nombre Cliente :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="8" onkeypress="convierteMayusculas();" id="txtNombreCliente" runat="server"
									Width="60%" MaxLength="150"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblAseguradora" runat="server">Aseguradora :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:DropDownList tabIndex="9" id="ddlAseguradora" runat="server" Width="60%"></asp:DropDownList></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblUsuario" runat="server">Usuario :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="10" onkeypress="convierteMayusculas();" id="txtUsuario" runat="server"
									Width="60%" MaxLength="50"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblEstatus" runat="server">Estatus :</asp:Label></TD>
							<TD></TD>
							<TD align="left">
								<asp:DropDownList tabIndex="11" id="cmbEstatus" runat="server"></asp:DropDownList></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaInicial" runat="server">Fecha Inicial :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="12" onkeyup="PonDiagonal()" id="txtFechaI" runat="server" Width="20%"
									MaxLength="10"></asp:TextBox>
								<rjs:PopCalendar id="calFechaInicial" runat="server" BorderWidth="1px" BorderStyle="Solid" Buttons="[<][m][y]  [>]"
									Culture="es-MX Espa�ol (M�xico)" TextMessage="La fecha es incorrecta" RequiredDateMessage="La fecha es requerida"
									ShowErrorMessage="False" InvalidDateMessage="D�a Inv�lido" Control="txtFechaI" ShowWeekend="True"
									Shadow="True" Separator="/" Fade="0.5"></rjs:PopCalendar></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaFinal" runat="server">Fecha Final :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="13" onkeyup="PonDiagonal1()" id="txtFechaF" runat="server" Width="20%"
									MaxLength="10"></asp:TextBox>
								<rjs:PopCalendar id="calFechaFinal" runat="server" BorderWidth="1px" BorderStyle="Solid" Buttons="[<][m][y]  [>]"
									Culture="es-MX Espa�ol (M�xico)" TextMessage="La fecha es incorrecta" RequiredDateMessage="La fecha es requerida"
									ShowErrorMessage="False" InvalidDateMessage="D�a Inv�lido" Control="txtFechaF" ShowWeekend="True"
									Shadow="True" Separator="/" Fade="0.5" From-Control="txtFechaI"></rjs:PopCalendar></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
							<TD>
								<asp:button tabIndex="8" id="cmdBuscar" runat="server" CssClass="boton" Text="Filtrar"></asp:button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:button tabIndex="8" id="cmdExcel" runat="server" CssClass="boton" Visible="False" Text="Exportar a Excel"></asp:button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:HyperLink id="hpReporte" runat="server"></asp:HyperLink>
							</TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD align="left">
                                </TD>
							<TD align="right">
								</TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD align="right"></TD>
							<TD align="right"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD colSpan="5">
								<DIV id="divGrdGeneral" style="MARGIN-LEFT: 10%" runat="server">
									<asp:datagrid id="grdGeneral" runat="server" Height="120px" Width="900px" BorderWidth="1px" BorderStyle="Solid"
										ForeColor="Transparent" BackColor="White" Font-Size="9pt" Font-Names="Tahoma,Arial,Helvetica,sans-serif"
										BorderColor="#7BB8D9" CellPadding="3" AutoGenerateColumns="False" Font-Name="Tahoma,Arial,Helvetica,sans-serif">
										<FooterStyle ForeColor="Black"></FooterStyle>
										<SelectedItemStyle Font-Size="7pt" ForeColor="Black"></SelectedItemStyle>
										<EditItemStyle ForeColor="Black"></EditItemStyle>
										<AlternatingItemStyle Font-Size="7pt" HorizontalAlign="Center" ForeColor="Black" VerticalAlign="Middle"
											BackColor="#DDE4CB"></AlternatingItemStyle>
										<ItemStyle Font-Size="7pt" HorizontalAlign="Center" ForeColor="Black" VerticalAlign="Middle"
											BackColor="Transparent"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" BackColor="Gray"></HeaderStyle>
										<Columns>
											<%--0--%>
											<asp:BoundColumn DataField="IdSolicitud" ReadOnly="True" HeaderText="Id_Solicitud">
												<HeaderStyle HorizontalAlign="Center" Width="10pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--1--%>
											<asp:BoundColumn DataField="TipoSolicitud" ReadOnly="True" HeaderText="Tipo&#160;Solicitud&#160;&#160;&#160;&#160;&#160;">
												<HeaderStyle HorizontalAlign="Center" Width="200pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--2--%>
											<asp:BoundColumn DataField="Poliza" ReadOnly="True" HeaderText="P&#243;liza">
												<HeaderStyle HorizontalAlign="Center" Width="20pt"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--3--%>
											<asp:BoundColumn DataField="Serie" ReadOnly="True" HeaderText="Serie">
												<HeaderStyle HorizontalAlign="Center" Width="30pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--4--%>
											<asp:BoundColumn DataField="Contrato" ReadOnly="True" HeaderText="Contrato">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<%--5--%>
											<asp:BoundColumn DataField="Cliente" ReadOnly="True" HeaderText="Nombre&#160;del&#160;Cliente&#160;&#160;&#160;&#160;&#160;&#160;">
												<HeaderStyle Width="200pt"></HeaderStyle>
											</asp:BoundColumn>
											<%--6--%>
											<asp:BoundColumn DataField="Aseguradora" ReadOnly="True" HeaderText="Aseguradora">
												<HeaderStyle Width="40px"></HeaderStyle>
											</asp:BoundColumn>
											<%--7--%>
											<asp:BoundColumn DataField="FECSOLICITADO" ReadOnly="True" HeaderText="Fecha_Solicitado" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="120px"></HeaderStyle>
											</asp:BoundColumn>
											<%--8--%>
											<asp:BoundColumn DataField="Status" ReadOnly="True" HeaderText="Status">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--9--%>
											<asp:BoundColumn DataField="FechaStatus" ReadOnly="True" HeaderText="Fecha_Status" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--10--%>
											<asp:BoundColumn DataField="USUARIOSOLICITANTE" ReadOnly="True" HeaderText="Quien_Solicita">
												<HeaderStyle Width="30px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--11--%>
											<asp:BoundColumn Visible="False" DataField="NSSTATUS" ReadOnly="True" HeaderText="Nivel Servicio a Status">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--12--%>
											<asp:BoundColumn DataField="FECCERRADO" ReadOnly="True" HeaderText="Fecha_Cerrado" DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--13--%>
											<asp:BoundColumn Visible="False" DataField="NSPENDIENTE" ReadOnly="True" HeaderText="Nivel Servicio Pendiente">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--14--%>
											<asp:BoundColumn Visible="False" DataField="NSTRAMITE" ReadOnly="True" HeaderText="Nivel Servicio Tr&#225;mite">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--15--%>
											<asp:BoundColumn Visible="False" DataField="NSCERRADO" ReadOnly="True" HeaderText="Nivel Servicio Cierre">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:BoundColumn>
											<%--16--%>
											<asp:BoundColumn Visible="False" DataField="Solicitante" ReadOnly="True" HeaderText="Solicitante"></asp:BoundColumn>
											<%--17--%>
											<asp:BoundColumn Visible="False" DataField="Agencia" ReadOnly="True" HeaderText="Agencia"></asp:BoundColumn>
											<%--18--%>
											<asp:BoundColumn DataField="BidAon" ReadOnly="True" HeaderText="BidAon"></asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--19--%>
											<asp:BoundColumn Visible="False" DataField="FecPendiente" ReadOnly="True" HeaderText="Fecha_Pendiente"
												DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--20--%>
											<asp:BoundColumn Visible="False" DataField="FecTramite" ReadOnly="True" HeaderText="Fecha_Tr&#225;mite"
												DataFormatString="{0:dd/MM/yy H:mm:ss}">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--Cambios Solicitud--%>
											<%--21--%>
											<asp:BoundColumn Visible="False" DataField="FecCancelado" ReadOnly="True" HeaderText="Fecha_Cancelado">
												<HeaderStyle Width="90px"></HeaderStyle>
											</asp:BoundColumn>
											<%--22--%>
											<asp:BoundColumn Visible="False" DataField="Detalle" ReadOnly="True" HeaderText="Detalle"></asp:BoundColumn>
											<%--23--%>
											<asp:BoundColumn DataField="SLA" ReadOnly="True" HeaderText="SLA"></asp:BoundColumn>
											<%--24--%>
											<asp:BoundColumn DataField="ConformeSLA" ReadOnly="True" HeaderText="ConformeSLA"></asp:BoundColumn>
											<%--25--%>
											<asp:BoundColumn Visible="False" DataField="Color" ReadOnly="True" HeaderText="Color"></asp:BoundColumn>
											<%--26--%>
											<asp:TemplateColumn HeaderText="Poliza Nueva">
												<ItemTemplate>
													<asp:Label id=lblNuevaPoliza runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PolizaNueva") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id="txtNuevaPoliza" runat="server" MaxLength="25"></asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<%--27--%>
											<asp:TemplateColumn HeaderText="VIP">
												<ItemTemplate>
													<asp:HyperLink style="Z-INDEX: 0" id="hpVip1" runat="server"></asp:HyperLink>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:CheckBox style="Z-INDEX: 0" id="chkvip" runat="server"></asp:CheckBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<%--28--%>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Actualizar" HeaderText="Asignar" CancelText="Cancelar"
												EditText="Editar"></asp:EditCommandColumn>
											<%--29--%>
											<asp:ButtonColumn Text="&lt;img src='..\Imagenes\grid01.gif' border='0'&gt;" HeaderText="Modif." CommandName="Select">
												<HeaderStyle Width="10px"></HeaderStyle>
											</asp:ButtonColumn>
											<%--30--%>
											<asp:BoundColumn Visible="False" DataField="VIP" ReadOnly="True"></asp:BoundColumn>
											<%--31--%>
											<asp:BoundColumn Visible="False" DataField="PolizaNueva" ReadOnly="True"></asp:BoundColumn>
										</Columns>
									</asp:datagrid></DIV>
							</TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
					</TABLE>
				</asp:panel>
				<asp:panel id="pnlModificacion" runat="server" Width="100%">
					<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" align="center">
								<asp:Label id="lblTitulo2" runat="server"></asp:Label></TD>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="8%"></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Label id="lblUsuarioII" runat="server"></asp:Label></TD>
							<TD width="50%" align="right"></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Label id="lblNivel2" runat="server"></asp:Label></TD>
							<TD width="50%" align="right"></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" colSpan="3" align="right">
								<asp:Table id="tblTitulo2" runat="server"></asp:Table></TD>
							<TD width="50%" align="right"></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%" align="center">
								<asp:Label id="lblSubTituloM" runat="server"></asp:Label></TD>
							<TD width="20%"></TD>
							<TD></TD>
							<TD width="10%"></TD>
						</TR>
						<TR>
							<TD width="20%"></TD>
							<TD width="2%"></TD>
							<TD width="50%"></TD>
							<TD width="20%"></TD>
							<TD></TD>
							<TD width="10%"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 19px" align="right">
								<asp:Label id="lblIdSolicitudM" runat="server">Id Solicitud :</asp:Label></TD>
							<TD style="HEIGHT: 19px"></TD>
							<TD style="HEIGHT: 19px">
								<asp:TextBox tabIndex="1" id="txtIdSolicitudM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD style="HEIGHT: 19px"></TD>
							<TD style="HEIGHT: 19px"></TD>
							<TD style="HEIGHT: 19px"></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblTipoSolicitudM" runat="server">Tipo solicitud :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:DropDownList id="cmbTipoSolicitudM" runat="server" Width="60%" AutoPostBack="True"></asp:DropDownList></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD vAlign="top" align="right">
								<asp:Label id="lblDetalleM" runat="server">Detalle :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="11" onkeypress="convierteMayusculas();" id="txtDetalleM" runat="server"
									Width="100%" MaxLength="250" TextMode="MultiLine"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblPolizaM" runat="server">P�liza :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="10" onkeypress="convierteMayusculas();" id="txtPolizaM" runat="server"
									Width="50%" MaxLength="25"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblSerieM" runat="server">Serie :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="11" onkeypress="convierteMayusculas();" id="txtSerieM" runat="server"
									Width="50%" MaxLength="17"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblContratoM" runat="server">Contrato :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="12" onkeypress="convierteMayusculas();" id="txtContratoM" runat="server"
									Width="50%" MaxLength="20"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblClienteM" runat="server">Nombre Cliente :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="13" onkeypress="convierteMayusculas();" id="txtClienteM" runat="server"
									Width="70%" MaxLength="150"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblAseguradoraM" runat="server">Aseguradora :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:DropDownList tabIndex="7" id="ddlAseguradoraM" runat="server" Width="70%"></asp:DropDownList><INPUT id="hdnAseguradora" type="hidden" runat="server" value="">
								<asp:CheckBoxList id="cblAseguradoras" runat="server" CssClass="CblStyle" RepeatLayout="Table" RepeatDirection="Vertical"
									RepeatColumns="5"></asp:CheckBoxList></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblStatusM" runat="server">Status :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:DropDownList tabIndex="4" id="cmbStatusM" runat="server"></asp:DropDownList></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaStatusM" runat="server">Fecha Status :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="5" id="txtFechaEstatusM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblSolicitanteM" runat="server">Solicitante :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="1" onkeypress="convierteMayusculas();" id="txtSolicitanteM" runat="server"
									Width="60%" MaxLength="20" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblSolicitaM" runat="server">Quien Solicita :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="1" onkeypress="convierteMayusculas();" id="txtSolicitaM" runat="server"
									Width="60%" MaxLength="20" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaSolicitadoM" runat="server">Fecha Solicitado :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="17" id="txtFechaSolicitadoM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNivelServicioM" runat="server">Nivel Servicio Estatus :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="17" id="txtNivelServicioM" runat="server" Width="10%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaCerradoM" runat="server">Fecha Cerrado :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtFechaCerradoM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNivelServicioPendienteM" runat="server">Nivel Servicio Pendiente :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="17" id="txtNivelServicioPendienteM" runat="server" Width="10%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNivelServicioTramiteM" runat="server">Nivel Servicio Tr�mite :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="17" id="txtNivelServicioTramiteM" runat="server" Width="10%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNivelServicioCierreM" runat="server">Nivel Servicio Cierre :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="17" id="txtNivelServicioCierreM" runat="server" Width="10%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaPendienteM" runat="server">Fecha Pendiente :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtFechaPendienteM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaTramiteM" runat="server">Fecha Tr�mite :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtFechaTramiteM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblFechaCanceladoM" runat="server">Fecha Cancelado :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtFechaCanceladoM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblSLAM" runat="server">SLA :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtSLAM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblConformeSLAM" runat="server">Conforme SLA :</asp:Label></TD>
							<TD></TD>
							<TD>
								<asp:TextBox tabIndex="20" id="txtConformeSLAM" runat="server" Width="30%" ReadOnly="True"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD vAlign="top" align="right">
								<asp:Label id="lblObservaciones" runat="server">Observaciones :</asp:Label></TD>
							<TD></TD>
							<TD class="NegritaMediana">
								<asp:TextBox tabIndex="8" id="txtObservacionesL" runat="server" Height="64px" Width="100%" ReadOnly="True"
									TextMode="MultiLine"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD class="NegritaMediana">
								<asp:TextBox tabIndex="8" onkeypress="convierteMayusculas();" id="txtObservaciones" runat="server"
									Height="64px" Width="100%" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblNuevaPolizaM" runat="server">Nueva P�liza :</asp:Label></TD>
							<TD></TD>
							<TD class="NegritaMediana">
								<asp:TextBox tabIndex="10" onkeypress="convierteMayusculas();" id="txtNuevaPolizaM" runat="server"
									Width="50%" MaxLength="25"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<asp:Label id="lblArchivos" runat="server">Archivos :</asp:Label></TD>
							<TD></TD>
							<TD><INPUT tabIndex="13" id="Archivo" class="Caja_Texto" style="HEIGHT: 22px; WIDTH: 70%" size="57"
									type="file" name="File1" runat="server">
								<asp:button tabIndex="24" id="cmbAgregar" runat="server" CssClass="boton" Text="Agregar"></asp:button></TD>
							<TD>
								<asp:TextBox tabIndex="7" id="txtArchivo" runat="server" Width="20px" Visible="False"></asp:TextBox>
								<asp:TextBox tabIndex="7" id="txtUbicacion" runat="server" Width="20px" Visible="False"></asp:TextBox></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD class="NegritaMediana">
								<asp:Table id="tblArchivo" runat="server" Width="100%"></asp:Table></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right">
								<ontranet:SaveDialog id="sd" runat="server"></ontranet:SaveDialog></TD>
							<TD></TD>
							<TD class="NegritaMediana"></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD align="right"></TD>
							<TD></TD>
							<TD>
								<asp:button tabIndex="24" id="cmdGuardar" runat="server" CssClass="boton" Text="Guardar"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button tabIndex="24" id="cmdRegresar" runat="server" CssClass="boton" Text="Regresar"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
					</TABLE>
				</asp:panel>
			</asp:panel><cc1:msgbox id="MsgBox" style="POSITION: absolute; LEFT: 16px; Z-INDEX: 102; TOP: 2064px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
