Imports CN_Negocios
Partial Class Wfrmframetop
    Inherits System.Web.UI.Page
    Private ccliente As New CnPrincipal

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Not Page.IsPostBack Then

                lbl_cargo.Text = Session("nombre_nivel")
                lbl_nombre.Text = Session("nombre")
                lbl_empleado.Text = Session("noempleado")

                If Not Session("IdMarca") Is Nothing Then
                    Dim dt As DataTable = ccliente.Cargar_InformacionMarca(Session("IdMarca"))
                    If dt.Rows.Count > 0 Then
                        If Session("IdMarca") = 1 Then
                            Dim dtP As DataTable = ccliente.Cargar_InformacionPrograma(Session("Programa"))
                            If dtP.Rows.Count > 0 Then
                                'imgLogo.ImageUrl = dtP.Rows(0).Item("Imagen_Url")
                            Else
                                'imgLogo.ImageUrl = "..\Imagenes\logos\vacio.gif"
                            End If
                        Else
                            'imgLogo.ImageUrl = dt.Rows(0).Item("Imagen_Marca")
                        End If
                    Else
                        'imgLogo.ImageUrl = "..\Imagenes\logos\vacio.gif"
                    End If
                End If

                If Not Session("Programa") Is Nothing Then
                    Dim dt As DataTable = ccliente.Cargar_InformacionPrograma(Session("Programa"))
                    If dt.Rows.Count > 0 Then
                        lblTitulo.Text = dt.Rows(0).Item("Titulo_Programa")
                    End If
                End If

                'Select Case Session("programa")
                '    Case 2
                '        lblTitulo.Text = "SEGURO AUTOMOTOR"
                '    Case 9
                '        lblTitulo.Text = ""
                '    Case Else
                '        lblTitulo.Text = "EMISOR DE P�LIZAS"
                'End Select


                'Select Case Session("programa")
                '    Case 3
                '        imgLogo.ImageUrl = "..\Imagenes\logos\logo_mazda.png"
                '    Case 7
                '        imgLogo.ImageUrl = "..\Imagenes\logos\Ford.png"
                '    Case 8
                '        imgLogo.ImageUrl = "..\Imagenes\logos\logo_mazda.png"
                '    Case 5, 9
                '        If Session("intmarcabid") = 1 Then
                '            imgLogo.ImageUrl = "..\Imagenes\logos\Ford_Insure.png"
                '        Else
                '            imgLogo.ImageUrl = "..\Imagenes\logos\Zurich.gif"
                '        End If
                '    Case 12
                '        imgLogo.ImageUrl = "..\Imagenes\logos\volvo.png"
                '    Case 13
                '        imgLogo.ImageUrl = "..\Imagenes\logos\Lincoln.png"
                '    Case 14
                '        imgLogo.ImageUrl = "..\Imagenes\logos\Jaguar.png"
                '    Case 15
                '        imgLogo.ImageUrl = "..\Imagenes\logos\LandRover.jpg"
                '    Case 19
                '        imgLogo.ImageUrl = "..\Imagenes\logos\unifin.jpg"
                '    Case 29
                '        imgLogo.ImageUrl = "..\Imagenes\Logos\Logo Conauto.JPG"
                '    Case 30
                '        imgLogo.ImageUrl = "..\Imagenes\logos\Logo Autopcion.jpg"
                '    Case 31
                '        imgLogo.ImageUrl = "..\Imagenes\logos\Logo Arrendadora.jpg"
                '    Case Else
                '        imgLogo.ImageUrl = "..\Imagenes\logos\vacio.gif"
                'End Select

                'If Session("programa") = 7 Then
                '    'Logo_Sistema.ImageUrl = "Imagenes\logos\PanelFord.png"
                '    Logo_Sistema.ImageUrl = "Imagenes\logos\vacio.gif"
                'Else
                '    Logo_Sistema.ImageUrl = "Imagenes\logos\vacio.gif"
                'End If
            End If

            'If Session("bancss") = 1 Then
            '    Logo_Aon.Visible = True
            'Else
            '    Logo_Aon.Visible = False
            'End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

End Class
