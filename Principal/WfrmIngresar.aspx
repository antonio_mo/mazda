﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WfrmIngresar.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmIngresar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">

.TituloPagAcceso
{
	color: black;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size:30px;
	text-align:center;
    vertical-align:middle;   
}

.EtiquetasLogin
	{
	/*color:MidnightBlue;*/
	color:#000000;
	font-family:Arial;
	font-size:10pt;
	font-weight:bold}
	
.Divisorlogin
{
	font-family:Arial;
	color: MidnightBlue;
	font-size:10pt;
	background-color:#336699;
	width:100%;
	height:5px
}

.Divisorlogin
	{font-family:Arial;
	color: MidnightBlue;
	font-size:10pt;
	background-color:#336699;
	width:100%;
	height:5px}
	
            #Table3 {
                width: 94%;
            }
        .small
{
	font-size: 8pt;
	color: black;
	font-family: Arial, Helvetica, sans-serif;
}

a:link {
	TEXT-DECORATION: none;
	color: #000000;
}

.negrita
{
	font-weight: bold;
	font-size: 8pt;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
}

.DatosInicio
{
	font-family:Arial, Helvetica, sans-serif;
	color:Black;
	font-weight:bold; 
	font-size: 8pt;
	background-color :#EFEFEF; 
	text-transform:uppercase;  
}

.boton
{
		font-family: Tahoma;
		font-size: 10px;
		font-weight: bold;		
		color: #FFFFFF;
		text-align:center; 				
		border: 1px solid #FFFFFF;
		background-image: url(../Imagenes/Botones/grouping_area_column_gris.png);
		vertical-align:middle ;
		padding-top: 4px;
		padding-bottom: 0px;
		padding-left: 10px;
		padding-right: 10px;
}

.style2
{
	font-family: Arial, Helvetica, sans-serif;
	color: red;
	font-size: 10pt;
	font-weight: bold;
}

.DivisorloginPrivacidad
{
	font-family:Arial;
	color:MidnightBlue;
	font-size:10pt;
	background-color:#336699;
	width:80%;
	height:2px
}

.AvisoPrivacidad
{ 
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px; 
	color:#000000;	
	text-decoration: none;
	text-align:justify;
}

        .auto-style2 {
            height: 62px;
            width: 381px;
        }
        .auto-style3 {
            height: 12px;
        }
        .auto-style4 {
            height: 11px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
		<TABLE style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table2" border="0"
			cellSpacing="0" cellPadding="0" width="100%" height="100%">
			<TR>
				<TD width="5%"></TD>
				<TD width="90%"></TD>
				<TD width="5%"></TD>
			</TR>
			<TR>
				<TD colSpan="3" align="center">					
					<DIV style="WIDTH: 800px" tabIndex="7">
                         <table border="0" style="WIDTH: 100%">
							<tr>
								<td align="left" class="auto-style4">
								</td>
                                <td class="auto-style4"></td>
								<td align="right" class="auto-style4" colspan="1">&nbsp;</td>
							</tr>
							<tr>
								<td vAlign="bottom" colSpan="3" align="center" class="auto-style3">
								</td>
							</tr>
                             <tr>
                                 <td align="center" colspan="3">
                                     <table border="0" align="center">
                                         <tr>
                                             <td align="center" class="auto-style2">
                                                 <!--<SCRIPT type="text/javascript">Flash_Portada('../Emisor_Polizas_Conauto.swf');</SCRIPT>-->
						                        <!--<img src ="..\Imagenes\logos\Logo.jpg">-->
							                        <HR color="#dcdcdc" SIZE="1">
							                        <TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1">
								                        <TR>
									                        <TD vAlign="top" width="45%">
										                        <TABLE id="Table4" tabIndex="10" border="0" cellSpacing="0" width="100%">
											                        <TR>
												                        <TD class="negrita" align="right"><FONT face="Arial"><B tabIndex="8">Código 
																	                        de Usuario:</B></FONT></TD>
												                        <TD align="left"><FONT face="Arial"><asp:textbox id="txtUsuario" tabIndex="1" onkeypress="" runat="server"
															                        Width="90px" MaxLength="10" CssClass="DatosInicio"></asp:textbox>&nbsp;
														                        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" ErrorMessage="Código de Usuario">*</asp:requiredfieldvalidator></FONT></TD>
											                        </TR>
											                        <TR>
												                        <TD class="negrita" align="right"><FONT face="Arial"><B tabIndex="9">Contraseña:</B></FONT></TD>
												                        <TD align="left"><FONT face="Arial"><asp:textbox id="txtPassword" tabIndex="2" onkeypress="" runat="server"
															                        Width="90px" MaxLength="15" CssClass="DatosInicio" TextMode="Password"></asp:textbox>&nbsp;
														                        <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="Contraseña">*</asp:requiredfieldvalidator></FONT></TD>
											                        </TR>
											                        <TR>
												                        <TD class="negrita" colSpan="2" align="center"><asp:button id="cmdAceptar" runat="server" CssClass="boton" Text="Ingresar"></asp:button></TD>
											                        </TR>
										                        </TABLE>
									                        </TD>
								                        </TR>
                                                        <TR>
                                                            <td align="center" class="EtiquetasLogin">&nbsp;</td>
                                                        </TR>
                                                        </TABLE>
							                        <HR tabIndex="11" color="gainsboro" SIZE="1">
							                        <P><FONT face="Arial"><asp:validationsummary id="Validationsummary2" runat="server" HeaderText="Los siguientes campos son obligatorios"
										                        Font-Names="Tahoma" Font-Size="12px"></asp:validationsummary><asp:label id="lbl_msg" runat="server" CssClass="style2" Visible="False"></asp:label></FONT></P>


                                             </td>
                                         </tr>
                                    </table>
                                 </td>                                 
                             </tr>
						</table>              
					</DIV>
				</TD>
			</TR>
			</TABLE>
	</form>
</body>
</html>
