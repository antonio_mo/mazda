<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmInicioadmin.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmInicioadmin" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
        <title>Cotizador</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">        
        <script Language="JavaScript">
            
            if (history.forward(1)) {
                history.replace(history.forward(1));
            }
        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 103; POSITION: absolute; TOP: 8px; LEFT: 8px" cellSpacing="0"
				cellPadding="0" width="100%" border="0" leftMargin="0" topMargin="0" rightMargin="0">
				<TR>
					<TD colSpan="6">
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0" leftMargin="0"
							topMargin="0" rightMargin="0">
							<TR>
								<TD vAlign="middle" align="left" width="15%">
									<IMG title="MAZDA RETAIL" border=0 alt="" src="..\Imagenes\logos\logo_mazda_CF.png">
								</TD>
								<TD vAlign="middle" width="25%"><asp:label id="lblTitulo" runat="server" Width="100%" CssClass="Nombredepto" BorderColor="Transparent"></asp:label></TD>
								<TD vAlign="middle" align="center" width="18%"><asp:label id="Label1" runat="server" Width="100%" CssClass="Nombredepto" BorderColor="Transparent"></asp:label></TD>
								<TD width="5%"></TD>
								<TD vAlign="middle" align="left" width="22%">
									<P><asp:label id="lbl_nombre" runat="server" Width="100%" CssClass="NombrePrincipal" BorderColor="Transparent"></asp:label><asp:label id="lbl_cargo" runat="server" Width="100%" CssClass="NombrePrincipal" BorderColor="Transparent"></asp:label></P>
								</TD>
								<TD vAlign="middle" align="center" width="15%"><asp:image id="Image1" runat="server" ImageUrl="..\Imagenes\logos\logo.jpg"></asp:image></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="6">
						<TABLE id="Table2" style="HEIGHT: 24px" height="24" cellSpacing="1" cellPadding="1" width="100%"
							border="0">
							<TR>
								<TD width="25%" background="..\Imagenes\Nuevo\img_fondomenu.gif" colSpan="3" height="10">.</TD>
							</TR>
							<TR>
								<TD width="35%" colSpan="3"><asp:label id="lbl_recomendacion1" runat="server" Width="100%" CssClass="Nombredepto_centro"
										BorderColor="Transparent">Seleccione el Distribuidor</asp:label></TD>
							</TR>
							<TR>
								<TD width="35%" colSpan="3">
									<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD class="obligatorio" width="20%" align="right"><asp:label id="lblMarca" runat="server" Width="100%" CssClass="Obligatorio" BorderColor="Transparent">Marca :</asp:label></TD>
											<TD width="50%"><asp:dropdownlist id="cbo_marca" runat="server" Width="30%" CssClass="combos_small" Enabled="false" AutoPostBack="True"></asp:dropdownlist></TD>
											<TD width="30%"></TD>
										</TR>
										<TR>
											<TD class="obligatorio" align="right">Distribuidor :</TD>
											<TD><asp:dropdownlist id="cbo_distribuidor" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD></TD>
											<TD align="center"><asp:imagebutton id="cmd_continuar" tabIndex="12" runat="server" ImageUrl="..\Imagenes\botones\continuar.gif"></asp:imagebutton></TD>
											<TD></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD width="15%"></TD>
					<TD width="35%"></TD>
					<TD width="18%"></TD>
					<TD width="5%"></TD>
					<TD width="22%"></TD>
					<TD width="5%"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 101; POSITION: absolute; TOP: 496px; LEFT: 272px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
