Imports CN_Negocios
Partial Class WfrmMenu
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                lbl_cargo.Text = Session("nombre_nivel")
                lbl_nombre.Text = Session("nombre")
                carga_marca()
                If Not Request.QueryString("cve") Is Nothing Then
                    recarga_infromacion_admin(Request.QueryString("cve"))
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub recarga_infromacion_admin(ByVal intprograma As Integer)
        Session("programa") = intprograma
        If Session("UsuarioRegistroBandera") > 0 Then
            If Session("contadoruser") > 1 Then
                Response.Redirect("WfrmInicioadmin.aspx")
            Else
                Response.Redirect("HPrincipal.htm")
            End If
        Else
            Response.Redirect("HPrincipal.htm")
        End If
    End Sub

    Public Sub carga_marca()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim hp As HyperLink
        Dim filas As Integer = 0
        Dim residuo As Integer = 0
        Dim columnas As Integer = 3
        Dim cuenta As Integer = 0
        tb_logo.Controls.Clear()

        Dim dt As DataTable = ccliente.pinta_menu_usuario(Session("usuario"))
        If dt.Rows.Count > 0 Then
            'columnas = dt.Rows.Count
            columnas = 2
            'filas = Math.DivRem(7, 2, 0)
            ' filas = Math.DivRem(columnas, 2, 0)
            residuo = dt.Rows.Count Mod 2
            'filas = filas + residuo
            filas = 1

            tbrow = New TableRow
            For i = 0 To columnas - 1
                ' For j = 0 To 1
                'If cuenta <= columnas - 1 Then
                hp = New HyperLink
                tbcell = New TableCell
                If dt.Rows(cuenta).IsNull("imagen_url") Then
                    hp.Text = dt.Rows(cuenta)("descripcion_programa")
                Else
                    If dt.Rows(cuenta)("imagen_url") = "" Then
                        hp.Text = dt.Rows(cuenta)("descripcion_programa")
                    Else
                        hp.ImageUrl = dt.Rows(cuenta)("imagen_url")
                    End If
                End If
                hp.ToolTip = dt.Rows(cuenta)("descripcion_programa")
                hp.NavigateUrl = "WfrmMenu.aspx?cve=" & dt.Rows(cuenta)("id_programa") & ""
                'Else
                '    hp = New HyperLink
                '    tbcell = New TableCell
                '    hp.ImageUrl = "..\Imagenes\logos\Vacio.gif"
                'End If
                cuenta = cuenta + 1
                hp.ForeColor = New System.Drawing.Color().Black
                tbcell.Controls.Add(hp)
                tbcell.Height = Unit.Percentage(100)
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.CssClass = "negrita"
                tbcell.Width = Unit.Percentage(50)
                tbrow.Cells.Add(tbcell)
                'Next

            Next
            tb_logo.Controls.Add(tbrow)
        End If
    End Sub

End Class
