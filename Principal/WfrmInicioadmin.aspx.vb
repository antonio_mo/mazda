Imports CN_Negocios
Partial Class WfrmInicioadmin
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then

                'Select Case Session("programa")
                '    Case 3 'mazda
                '        img_logo.ImageUrl = "..\Imagenes\logos\mazda.gif"
                '    Case 7 'ford panel
                '        img_logo.ImageUrl = "..\Imagenes\logos\Ford.png"
                '    Case 8 'ford panel mazda
                '        img_logo.ImageUrl = "..\Imagenes\logos\mazda.gif"
                '    Case 5, 9 'multianual
                '        img_logo.ImageUrl = "..\Imagenes\logos\Ford_Insure.png"
                '    Case 12
                '        img_logo.ImageUrl = "..\Imagenes\logos\Volvo.png"
                '    Case 13
                '        img_logo.ImageUrl = "..\Imagenes\logos\Lincoln.png"
                '    Case 14
                '        img_logo.ImageUrl = "..\Imagenes\logos\Jaguar.png"
                '    Case 15
                '        img_logo.ImageUrl = "..\Imagenes\logos\LandRover.jpg"
                '    Case 19
                '        img_logo.ImageUrl = "..\Imagenes\logos\Unifin.jpg"
                '    Case 20
                '        img_logo.ImageUrl = "..\Imagenes\logos\Unifin.jpg"
                '    Case 29
                '        img_logo.ImageUrl = "..\Imagenes\Logos\Logo Conauto.JPG"
                '    Case 30
                '        img_logo.ImageUrl = "..\Imagenes\logos\Logo Autopcion.jpg"
                '    Case 31
                '        img_logo.ImageUrl = "..\Imagenes\logos\Logo Arrendadora.jpg"
                '    Case Else
                '        img_logo.ImageUrl = "..\Imagenes\logos\vacio.gif"
                'End Select

                lbl_cargo.Text = Session("nombre_nivel")
                lbl_nombre.Text = Session("nombre")

                If Not Session("Programa") Is Nothing Then
                    Dim dt As DataTable = ccliente.Cargar_InformacionPrograma(Session("Programa"))
                    If dt.Rows.Count > 0 Then

                        lblTitulo.Text = dt.Rows(0).Item("Titulo_Programa")
                    Else

                    End If
                End If

                'If Not Request.QueryString("cve") Is Nothing Then
                '    recarga_Informacion_admin(Request.QueryString("cve"))
                'End If

                If Session("Programa") = 32 Then    'Mazda
                    lblMarca.Visible = False
                    cbo_marca.Visible = False
                    carga_distribuidor(Session("IdMarca"))
                Else
                    lblMarca.Visible = True
                    cbo_marca.Visible = True
                    carga_marca()
                End If

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_marca()
        Dim dt As DataTable = ccliente.carga_marca_admin(Session("usuario"), Session("programa"))
        If dt.Rows.Count > 0 Then
            cbo_marca.DataSource = dt
            cbo_marca.DataValueField = "id_marca"
            cbo_marca.DataTextField = "marca"
            cbo_marca.DataBind()
            cbo_marca.SelectedValue = 1
            cbo_marca_SelectedIndexChanged(cbo_marca, Nothing)
        End If
    End Sub

    Public Sub carga_distribuidor(IdMarca As Integer)
        Dim dt As DataTable = ccliente.carga_distribuidor_admin(Session("usuario"), IdMarca, Session("programa"))
        If dt.Rows.Count > 0 Then
            cbo_distribuidor.DataSource = dt
            cbo_distribuidor.DataTextField = "empresa"
            cbo_distribuidor.DataValueField = "id_bid"
            cbo_distribuidor.DataBind()
        End If
    End Sub

    Public Sub recarga_Informacion_admin(ByVal intbid As Integer)
        Try
            ccliente.Login_acceso_admin(Session("usuario"), intbid)
            'Ejecuto la instrucci�n hacia la base de datos.
            If ccliente.BanInicio = 1 Then
                Session("ClienteCotiza") = 0
                Session("usuario") = ccliente.usuario
                Session("nombre_nivel") = ccliente.NomNivel
                Session("nivel") = ccliente.niveles
                Session("IDNivel") = ccliente.nivel
                Session("bid") = intbid
                Session("bidConauto") = ccliente.BidConauto
                Session("NombreRegion") = ccliente.Region
                'Session("Region") = ccliente.idRegion
                Session("moneda") = 0
                Session("InterestRate") = "B"
                Session("tiposeguro") = 1
                'Session("polizainicio") = ccliente.poliza_inicio()
                'Session("version") = ccliente.carga_version
                Session("UsuarioRegistroBandera") = ccliente.UsurarioRegistro
                Session("bancss") = ccliente.banCss
                Session("version") = ccliente.carga_version(Session("programa"))
            End If

            If Session("UsuarioRegistroBandera") > 0 Then
                Response.Redirect("HPrincipal.htm")
            Else
                Response.Redirect("administrador\WfrmCambioPassword.aspx")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_marca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_marca.SelectedIndexChanged
        If cbo_marca.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione la marca del Distribuidor")
            Exit Sub
        End If
        carga_distribuidor(cbo_marca.SelectedValue)
    End Sub

    Private Sub cmd_continuar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_continuar.Click
        'HM
        If Session("Programa") <> 32 Then
            If cbo_marca.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar la Marca")
                Exit Sub
            End If
        End If

        If cbo_distribuidor.SelectedValue = 0 Then
            MsgBox.ShowMessage("Favor de seleccionar el Distribuidor")
            Exit Sub
        End If

        Session("NombreDistribuidor") = cbo_distribuidor.SelectedItem.Text
        'Session("nombre") = cbo_distribuidor.SelectedItem.Text
        If Session("Programa") = 32 Then    'Mazda
            Session("intmarcabid") = Session("IdMarca")
        Else
            Session("intmarcabid") = cbo_marca.SelectedValue
        End If

        recarga_Informacion_admin(cbo_distribuidor.SelectedValue)

    End Sub


    'Public Sub genera_numero()
    '    Dim intid As Integer = 0

    '    Dim dt As DataTable = ccliente.ordena_algoritmo
    '    If dt.Rows.Count > 0 Then
    '        intid = dt.Rows(0)(0)
    '        intid = intid + 1
    '    End If

    '    Dim dtValor As DataTable = ccliente.TraerMaxalgoritmo(intid)
    '    If dtValor.Rows.Count > 0 Then
    '        ccliente.id = dtValor.Rows(0)(0)
    '        ccliente.v = dtValor.Rows(0)(1)
    '        ccliente.w = dtValor.Rows(0)(2)
    '        ccliente.x = dtValor.Rows(0)(3)
    '        ccliente.y = dtValor.Rows(0)(4)
    '        ccliente.z = dtValor.Rows(0)(5)
    '    End If

    '    'Dim dt As DataTable = Cvalida.maximo_valor
    '    'Cvalida.id = dt.Rows(0).Item(0)
    '    'Cvalida.v = dt.Rows(0).Item(1)
    '    'Cvalida.w = dt.Rows(0).Item(2)
    '    'Cvalida.x = dt.Rows(0).Item(3)
    '    'Cvalida.y = dt.Rows(0).Item(4)
    '    'Cvalida.z = dt.Rows(0).Item(5)

    '    Dim valor As String = ccliente.Algoritmo()
    '    ccliente.actualiza_valor(ccliente.a, ccliente.b, ccliente.c, ccliente.d, ccliente.id, intid)
    '    MsgBox.ShowMessage(valor)

    'End Sub

End Class
