﻿Imports System.Web.Security
Imports CN_Negocios
Imports System.Security.Cryptography
Imports System.Text
Public Class WfrmIngresar
    Inherits System.Web.UI.Page
    Private ccliente As New CnPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            activa(False)
            If Not Request.QueryString("ban") Is Nothing Then
                lbl_msg.Text = "Por seguridad el usuario esta bloqueado, consulte a su Administrador"
                'MsgBox.ShowMessage("Por seguridad el usuario esta bloqueado, consulte a su Administrador")
                activa(True)
            Else
                SetFocus(txtUsuario)
            End If
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripción de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos dígitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()

    End Function

    Public Function Solo_Output()
        Response.Redirect("administrador/HPrincipal1.htm", True)
    End Function

    Public Sub activa(ByVal ban As Boolean)
        lbl_msg.Visible = ban
    End Sub

    Protected Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim sRptServer As String = ""
        Dim sPathImg As String = ""
        Dim sServer As String = ""
        Dim sDB As String = ""
        Dim sDSN As String = ""
        Dim sUSUARIO As String = ""
        Dim sPASSWORD As String = ""
        Dim usuario As String = ""
        Dim password As String = ""
        Dim ValorContador As Integer = 0
        Dim encripta As String = ""
        Dim banKey As String = ""

        Try
            If Trim(txtUsuario.Text) = "" Or Trim(txtPassword.Text) = "" Then
                lbl_msg.Text = "Los datos del usuario y/o contraseña son necesarios para poder continuar"
                'MsgBox.ShowMessage("Los datos del Distribuidor son necesarios para poder continuar")
                SetFocus(txtUsuario)
                activa(True)
                Exit Sub
            End If

            'Se arma la cadena de conexión con los valores obtenidos del web.config  y los introducidos por el usuario
            Session("fecha") = ConfigurationSettings.AppSettings("Bandera_Fecha")
            Session("BanMulti") = ConfigurationSettings.AppSettings("Bandera_Multianual")
            Session("CorreoAdministrador") = ConfigurationSettings.AppSettings("CorreoAdministrador")

            'se conecta para verificar que este abierta la conexión
            usuario = Trim(ccliente.valida_cadena(txtUsuario.Text))
            password = Trim(ccliente.valida_cadena(txtPassword.Text))
            encripta = ccliente.Encripta(password)
            'encripta = generarClaveSHA1(cvalida.valida_cadena(UCase(txtPassword.Text)))

            Session("banderafecha") = ccliente.fecha_servidor

            Dim dtV As DataTable = ccliente.valida_accesso(usuario, encripta, Session("IdMarca"))
            If dtV.Rows.Count > 0 Then
                'Obtiene los datos del usuario
                ccliente.Login_acceso(usuario, encripta)

                'Ejecuto la instrucción hacia la base de datos.
                'If ccliente.BanInicio = 1 Then
                Session("ClienteCotiza") = 0
                Session("usuario") = ccliente.usuario
                Session("id_usuario") = dtV.Rows(0)("id_usuario")
                Session("nombre") = ccliente.Empresa
                Session("nombre_nivel") = ccliente.NomNivel
                Session("nivel") = ccliente.niveles
                Session("IdNivel") = ccliente.nivel
                Session("bid") = ccliente.Bid
                Session("bidConauto") = ccliente.BidConauto
                Session("NombreRegion") = ccliente.Region
                'Session("Region") = ccliente.idRegion
                Session("moneda") = 0
                Session("InterestRate") = "B"
                Session("tiposeguro") = 1
                'Session("polizainicio") = ccliente.poliza_inicio()
                Session("UsuarioRegistroBandera") = ccliente.UsurarioRegistro
                Session("intmarcabid") = ccliente.intmarca
                Session("IdMarca") = ccliente.intmarca
                Session("Programa") = ccliente.programa

                'nos indica a que programa tiene permiso
                If Session("nombre") <> ccliente.Distribuidor_Bid Then
                    Session("NombreDistribuidor") = ccliente.Distribuidor_Bid
                Else
                    Session("NombreDistribuidor") = Nothing
                End If
                Session("bancss") = ccliente.banCss
                'inicializamos en blanco
                'Session("ImagenFondo") = "Imagenes\Nuevo\Logo.jpg"
                'Nos indica el numero de registro con el que cuenta el usuario
                Session("contadoruser") = ccliente.contador
                'If dtV.Rows(0)("programa") > 1 Then
                '    If Not ccliente.niveles = 3 Then
                '        Response.Redirect("wfrmmenu.aspx")
                '    Else
                '        Solo_Output()
                '    End If
                'Else
                '    Session("programa") = ccliente.programa
                '    Session("version") = ccliente.carga_version(Session("programa"))
                'End If
                Session("programa") = ccliente.programa
                Session("version") = ccliente.carga_version(Session("programa"))
                ' Autentifico al usuario y continuo con el proceso.
                FormsAuthentication.RedirectFromLoginPage(usuario, False)

                '20160615 MAVILA
                If Not ccliente.Valida_Password(encripta) Then
                    Session("mensaje") = "La contraseña no contiene los elementos mínimos de seguridad favor de modificarla"
                    Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                Else
                    If Not ccliente.Verifica_Vigencia_Password(Convert.ToInt32(dtV.Rows(0)("id_usuario").ToString)) Then
                        Session("mensaje") = "La contraseña ha expirado favor de modificarla"
                        Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                    End If
                End If
            Else
                'Mitigar la vulnerabilidad - Password attack
                Dim rand As New Random()
                System.Threading.Thread.Sleep(rand.Next(1, 20) * 1000)

                ValorContador = ccliente.registra_accesos_negados(usuario)
                Session("ContadorUsuario") = Session("ContadorUsuario") + ValorContador
                If Session("ContadorUsuario") >= 3 Then
                    ccliente.Update_Usuario_Bloqueado(usuario)
                    lbl_msg.Text = "Por seguridad el usuario esta bloqueado, consulte a su administrador"
                    'MsgBox.ShowMessage("Por seguridad el usuario esta bloqueado, consulte a su Administrador")
                Else
                    lbl_msg.Text = "El usuario no existe ó la contraseña no es válida"
                    'MsgBox.ShowMessage("El usuario no existe ó la contraseña no es válida")
                End If
                activa(True)
                SetFocus(txtUsuario)
                Exit Sub
            End If

            If Session("UsuarioRegistroBandera") <> 2 Then
                If password.Length = 10 Then
                    banKey = Mid(password, password.Length - 3, 4)
                Else
                    banKey = ""
                End If
                If Not banKey = "779N" Then
                    If Session("UsuarioRegistroBandera") > 0 Then
                        If dtV.Rows(0)("programa") > 1 Then
                            If dtV.Rows(0)("programa") = 32 Then
                                Response.Redirect("wfrmInicioAdmin.aspx")
                            Else
                                Response.Redirect("wfrmmenu.aspx")
                            End If
                        Else
                            If Not ccliente.niveles = 3 Then
                                If Session("contadoruser") > 1 Then
                                    Response.Redirect("WfrmInicioadmin.aspx")
                                Else
                                    Response.Redirect("HPrincipal.htm")
                                End If
                            Else
                                Solo_Output()
                            End If
                        End If
                    Else
                        Response.Redirect("HPrincipal.htm")
                        'Response.Redirect("administrador\WfrmCambioPassword.aspx")
                    End If
                Else
                    Response.Redirect("..\administrador\WfrmCambioPassword.aspx")
                End If
            Else
                Response.Redirect("frmLogin.aspx?ban=1")
            End If

        Catch ex As Exception
            lbl_msg.Text = "El usuario no existe ó la contraseña no es válida."
            'MsgBox.ShowMessage("El usuario no existe ó la contraseña no es válida.")
            activa(True)
            Exit Sub
        End Try
    End Sub
End Class