'comentarios 
'esta pendiente dependiendo del tipo de poliza para estas aseguradoras 
'abc_programa_aseguradora
'ban_tipopoliza debe de ir a 1 
'id_aseguradora = 9
'id_aseguradora = 10

'Ambiente DRP
'http://192.168.166.14/AonWebServiceEC/Users.asmx

'Ambiente PROD
'http://192.168.66.232/AonWebServiceEC/Users.asmx

'Ambiente QA
'http://192.168.166.192/AonWebServiceEC/Users.asmx 

'Ambiente DEV
'http://192.168.166.193/AonWebServiceEC/Users.asmx 

Imports System.Web.Security
Imports CN_Negocios
Imports System.Security.Cryptography
Imports System.Text

Partial Class frmLogin
    Inherits System.Web.UI.Page
    Private ccliente As New CnPrincipal

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imgFuncion1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion2 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion3 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion4 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion5 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion6 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgFuncion7 As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        ccliente = Nothing
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            'Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            '_bsPolizas.GetCHUBB(3, 57, 1, 0, 1, 1, 63, 0, 1, 0)
            '_bsPolizas.GetQTS(3, 48, 1, 0, 1, 1, 63, 0, 1, 0)

            'HM 062016
            Session("IdMarca") = 1

            If Not Session("IdMarca") Is Nothing Then
                Dim dt As DataTable = ccliente.Cargar_InformacionMarca(Session("IdMarca"))
                If dt.Rows.Count > 0 Then
                    imgLogo.ImageUrl = dt.Rows(0).Item("Imagen_Marca")
                    lblTitulo.Text = dt.Rows(0).Item("Descripcion_Marca")
                End If

                activa(False)
                If Not Request.QueryString("ban") Is Nothing Then
                    lbl_msg.Text = "Por seguridad el usuario esta bloqueado, consulte a su Administrador"
                    'MsgBox.ShowMessage("Por seguridad el usuario esta bloqueado, consulte a su Administrador")
                    activa(True)
                Else
                    SetFocus(txtUsuario)
                End If
            End If
        End If
    End Sub

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos d�gitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()

    End Function

    Public Function Solo_Output()
        Response.Redirect("administrador/HPrincipal1.htm", True)
    End Function

    Public Sub activa(ByVal ban As Boolean)
        lbl_msg.Visible = ban
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Dim sRptServer As String = ""
        Dim sPathImg As String = ""
        Dim sServer As String = ""
        Dim sDB As String = ""
        Dim sDSN As String = ""
        Dim sUSUARIO As String = ""
        Dim sPASSWORD As String = ""
        Dim usuario As String = ""
        Dim password As String = ""
        Dim ValorContador As Integer = 0
        Dim encripta As String = ""
        Dim banKey As String = ""

        'Dim contador_usuario As Integer = 0
        Session.Timeout = 120

        Try
            If Trim(txtUsuario.Text) = "" Or Trim(txtPassword.Text) = "" Then
                lbl_msg.Text = "Los datos del usuario y/o contrase�a son necesarios para poder continuar"
                'MsgBox.ShowMessage("Los datos del Distribuidor son necesarios para poder continuar")
                SetFocus(txtUsuario)
                activa(True)
                Exit Sub
            End If

            'Se arma la cadena de conexi�n con los valores obtenidos del web.config  y los introducidos por el usuario
            Session("fecha") = ConfigurationSettings.AppSettings("Bandera_Fecha")
            'sServer = ConfigurationSettings.AppSettings("Server")
            'sDB = ConfigurationSettings.AppSettings("DataBase")
            'sUSUARIO = ConfigurationSettings.AppSettings("usuario")
            'sPASSWORD = ConfigurationSettings.AppSettings("password")
            Session("BanMulti") = ConfigurationSettings.AppSettings("Bandera_Multianual")
            Session("CorreoAdministrador") = ConfigurationSettings.AppSettings("CorreoAdministrador")
            'sDSN = "server=" & sServer & ";" & "database=" & sDB & ";" & "uid=" & sUSUARIO & ";pwd=" & sPASSWORD & ""
            'Session("DsnAcceso") = sDSN
            'Session("RptSrvURL") = sRptServer

            'se conecta para verificar que este abierta la conexi�n
            'ccliente = New Cn_Principal.CnPrincipal(Session("DsnAcceso"))
            'ccliente.verificar_open(Session("DsnAcceso"))
            usuario = Trim(ccliente.valida_cadena(txtUsuario.Text))
            password = Trim(ccliente.valida_cadena(txtPassword.Text))

            encripta = generarClaveSHA1(ccliente.valida_cadena(UCase(password)))
            'encripta = ccliente.Encripta(password)

            Session("banderafecha") = ccliente.fecha_servidor

            'Dim dtV As DataTable = ccliente.valida_accesso(usuario, password)
            Dim dtV As DataTable = ccliente.valida_accesso(usuario, encripta, Session("IdMarca"))
            If dtV.Rows.Count > 0 Then
                'Obtiene los datos del usuario
                'ccliente.Login_acceso(usuario, password)
                ccliente.Login_acceso(usuario, encripta)

                'Ejecuto la instrucci�n hacia la base de datos.
                'If ccliente.BanInicio = 1 Then
                Session("ClienteCotiza") = 0
                Session("usuario") = ccliente.usuario
                Session("id_usuario") = dtV.Rows(0)("id_usuario")
                Session("nombre") = ccliente.Empresa
                Session("nombre_nivel") = ccliente.NomNivel
                Session("nivel") = ccliente.niveles
                Session("IdNivel") = ccliente.nivel
                Session("bid") = ccliente.Bid
                Session("bidConauto") = ccliente.BidConauto
                Session("NombreRegion") = ccliente.Region
                'Session("Region") = ccliente.idRegion
                Session("moneda") = 0
                Session("InterestRate") = "B"
                Session("tiposeguro") = 1
                'Session("polizainicio") = ccliente.poliza_inicio()
                Session("UsuarioRegistroBandera") = ccliente.UsurarioRegistro
                Session("intmarcabid") = ccliente.intmarca
                Session("IdMarca") = ccliente.intmarca
                Session("Programa") = ccliente.programa
                Session("EmailUsuario") = String.Empty
                Session("Funcionalidad") = "SOLICITUDES"
                Session("noempleado") = ccliente.noempleado

                'nos indica a que programa tiene permiso
                If Session("nombre") <> ccliente.Distribuidor_Bid Then
                    Session("NombreDistribuidor") = ccliente.Distribuidor_Bid
                Else
                    Session("NombreDistribuidor") = Nothing
                End If
                Session("bancss") = ccliente.banCss
                'inicializamos en blanco
                'Session("ImagenFondo") = "Imagenes\Nuevo\Logo.jpg"
                'Nos indica el numero de registro con el que cuenta el usuario
                Session("contadoruser") = ccliente.contador
                'If dtV.Rows(0)("programa") > 1 Then
                '    If Not ccliente.niveles = 3 Then
                '        Response.Redirect("wfrmmenu.aspx")
                '    Else
                '        Solo_Output()
                '    End If
                'Else
                '    Session("programa") = ccliente.programa
                '    Session("version") = ccliente.carga_version(Session("programa"))
                'End If
                Session("programa") = ccliente.programa
                Session("version") = ccliente.carga_version(Session("programa"))
                ' Autentifico al usuario y continuo con el proceso.
                FormsAuthentication.RedirectFromLoginPage(usuario, False)

                '20160615 MAVILA
                'If Not ccliente.Valida_Password(encripta) Then
                '    Session("mensaje") = "La contrase�a no contiene los elementos m�nimos de seguridad favor de modificarla"
                '    Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                'Else
                '    If Not ccliente.Verifica_Vigencia_Password(Convert.ToInt32(dtV.Rows(0)("id_usuario").ToString)) Then
                '        Session("mensaje") = "La contrase�a ha expirado favor de modificarla"
                '        Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                '    End If
                'End If
            Else
                'Mitigar la vulnerabilidad - Password attack
                Dim rand As New Random()
                System.Threading.Thread.Sleep(rand.Next(1, 20) * 1000)

                ValorContador = ccliente.registra_accesos_negados(usuario)
                Session("ContadorUsuario") = Session("ContadorUsuario") + ValorContador
                If Session("ContadorUsuario") >= 3 Then
                    ccliente.Update_Usuario_Bloqueado(usuario)
                    lbl_msg.Text = "Por seguridad el usuario esta bloqueado, consulte a su administrador"
                    'MsgBox.ShowMessage("Por seguridad el usuario esta bloqueado, consulte a su Administrador")
                Else
                    lbl_msg.Text = "El usuario no existe � la contrase�a no es v�lida"
                    'MsgBox.ShowMessage("El usuario no existe � la contrase�a no es v�lida")
                End If
                activa(True)
                SetFocus(txtUsuario)
                Exit Sub
            End If

            If Session("UsuarioRegistroBandera") <> 2 Then
                If password.Length = 10 Then
                    banKey = Mid(password, password.Length - 3, 4)
                Else
                    banKey = ""
                End If
                If Not banKey = "779N" And Session("UsuarioRegistroBandera") <> 0 Then
                    If Session("UsuarioRegistroBandera") > 0 Then
                        If dtV.Rows(0)("programa") > 1 Then
                            Response.Redirect("~/Principal/wfrmmenu.aspx")
                        Else
                            If Not ccliente.niveles = 3 Then
                                If Session("contadoruser") > 1 Then
                                    If dtV.Rows(0)("programa") = 32 Then
                                        Response.Redirect("~/Principal/HPrincipal.htm")
                                    Else
                                        Response.Redirect("~/Principal/WfrmInicioadmin.aspx")
                                    End If
                                Else
                                    Response.Redirect("~/Principal/HPrincipal.htm")
                                End If
                            Else
                                Solo_Output()
                            End If
                        End If
                    Else
                        Response.Redirect("~/Principal/HPrincipal.htm")
                        'Response.Redirect("administrador\WfrmCambioPassword.aspx")
                    End If
                Else
                    Response.Redirect("~/administrador/WfrmCambioPassword.aspx")
                End If
            Else
                Response.Redirect("~/Principal/frmLogin.aspx?ban=1")
            End If

        Catch ex As Exception
            lbl_msg.Text = "El usuario no existe � la contrase�a no es v�lida."
            'MsgBox.ShowMessage("El usuario no existe � la contrase�a no es v�lida.")
            activa(True)
            Exit Sub
        End Try
    End Sub
   
End Class