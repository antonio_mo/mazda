Imports CN_Negocios
Imports System.Web.Security
Imports System.Security.Cryptography
Imports System.Text

Partial Class wfrmlogooff
    Inherits System.Web.UI.Page
    Private ccliente As New CnPrincipal

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Logo_Corporativo As System.Web.UI.WebControls.Image

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        'If Not Session("IdMarca") Is Nothing Then
        Dim dt As DataTable = ccliente.Cargar_InformacionMarca(1)
        If dt.Rows.Count > 0 Then
            imgLogo.ImageUrl = dt.Rows(0).Item("Imagen_Marca")
        Else
            imgLogo.ImageUrl = "..\Imagenes\logos\vacio.gif"
        End If

        lbl_nota.Text = "Gracias por utilizar los servicios electr�nicos. <br> "
        lbl_nota.Text = lbl_nota.Text & " Presione el boton de Regresar para volver a la pantalla de entrada."
        cmd_regresar.Visible = True
        cmdContinuar.Visible = False
        cmdSalir.Visible = False

        'Else

        '    lbl_nota.Text = "Por seguridad la sesi�n que sosten�a con el servidor ha terminado, <br> "
        '    lbl_nota.Text = lbl_nota.Text & " para que pueda continuar usando el sistema deber� de <br>"
        '    lbl_nota.Text = lbl_nota.Text & " dar click en Continuar, en caso contrario dar click en Salir para cerrar la aplicaci�n."
        '    cmd_regresar.Visible = False
        '    cmdContinuar.Visible = True
        '    cmdSalir.Visible = True
        'End If

        If Not Page.IsPostBack Then
            If Session("UsuarioRegistroBandera") = 2 Then
                lbl_msg0.Text = "Por seguridad el usuario esta bloqueado, consulte a su Administrador"
            End If
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    'Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regresar2.Click
    '    Dim bandera As Integer = Session("programa")
    '    Dim IdMarca As Integer = Session("IdMarca")
    '    Select Case bandera
    '        Case 32
    '            If IdMarca = 5 Then
    '                Response.Redirect("frmLogin.aspx?pr=5", True)
    '            Else
    '                Response.Redirect("frmLogin.aspx", True)
    '            End If

    '        Case Else
    '            Response.Redirect("frmLogin.aspx", True)

    '    End Select
    '    eliminaCookies()
    'End Sub

    Public Sub eliminaCookies()
        Dim aCookie As HttpCookie
        Dim i As Integer
        Dim cookieName As String
        Dim limit As Integer = Request.Cookies.Count - 1
        For i = 0 To limit
            cookieName = Request.Cookies(i).Name
            aCookie = New HttpCookie(cookieName)
            aCookie.Expires = DateTime.Now.AddDays(-1)
            Response.Cookies.Add(aCookie)
        Next
        Response.Buffer = True
        Response.ExpiresAbsolute = Now.AddYears(-1)
        Response.Expires = 0
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", "private") 'stops proxy server cache
        Response.CacheControl = "no-cache"
        Session.Clear()
        Session.Abandon()
        Session.RemoveAll()
    End Sub

    Protected Sub cmd_regresar_Click1(sender As Object, e As EventArgs) Handles cmd_regresar.Click
        Dim bandera As Integer = Session("programa")
        Dim IdMarca As Integer = Session("IdMarca")
        Select Case bandera
            Case 32
                If IdMarca = 5 Then
                    Response.Redirect("frmLogin.aspx?pr=5", True)
                Else
                    Response.Redirect("frmLogin.aspx", True)
                End If

            Case Else
                Response.Redirect("frmLogin.aspx", True)

        End Select
        eliminaCookies()
    End Sub

    Protected Sub cmdContinuar_Click(sender As Object, e As EventArgs) Handles cmdContinuar.Click
        Try
            pnlAcceso.Visible = True

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub cmdSalir_Click(sender As Object, e As EventArgs) Handles cmdSalir.Click
        Try

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim sRptServer As String = ""
        Dim sPathImg As String = ""
        Dim sServer As String = ""
        Dim sDB As String = ""
        Dim sDSN As String = ""
        Dim sUSUARIO As String = ""
        Dim sPASSWORD As String = ""
        Dim usuario As String = ""
        Dim password As String = ""
        Dim ValorContador As Integer = 0
        Dim encripta As String = ""
        Dim banKey As String = ""

        'Dim contador_usuario As Integer = 0
        Session.Timeout = 120

        Try
            If Trim(txtUsuario.Text) = "" Or Trim(txtPassword.Text) = "" Then
                lbl_msg0.Text = "Los datos del usuario y/o contrase�a son necesarios para poder continuar"
                'MsgBox.ShowMessage("Los datos del Distribuidor son necesarios para poder continuar")
                SetFocus(txtUsuario)
                activa(True)
                Exit Sub
            End If

            'Se arma la cadena de conexi�n con los valores obtenidos del web.config  y los introducidos por el usuario
            Session("fecha") = ConfigurationSettings.AppSettings("Bandera_Fecha")
            'sServer = ConfigurationSettings.AppSettings("Server")
            'sDB = ConfigurationSettings.AppSettings("DataBase")
            'sUSUARIO = ConfigurationSettings.AppSettings("usuario")
            'sPASSWORD = ConfigurationSettings.AppSettings("password")
            Session("BanMulti") = ConfigurationSettings.AppSettings("Bandera_Multianual")
            Session("CorreoAdministrador") = ConfigurationSettings.AppSettings("CorreoAdministrador")
            'sDSN = "server=" & sServer & ";" & "database=" & sDB & ";" & "uid=" & sUSUARIO & ";pwd=" & sPASSWORD & ""
            'Session("DsnAcceso") = sDSN
            'Session("RptSrvURL") = sRptServer

            'se conecta para verificar que este abierta la conexi�n
            'ccliente = New Cn_Principal.CnPrincipal(Session("DsnAcceso"))
            'ccliente.verificar_open(Session("DsnAcceso"))
            usuario = Trim(ccliente.valida_cadena(txtUsuario.Text))
            password = Trim(ccliente.valida_cadena(txtPassword.Text))

            'encripta = generarClaveSHA1(ccliente.valida_cadena(UCase(password)))

            encripta = ccliente.Encripta(password)

            Session("banderafecha") = ccliente.fecha_servidor

            'Dim dtV As DataTable = ccliente.valida_accesso(usuario, password)
            Dim dtV As DataTable = ccliente.valida_accesso(usuario, encripta, Session("IdMarca"))
            If dtV.Rows.Count > 0 Then
                'Obtiene los datos del usuario
                'ccliente.Login_acceso(usuario, password)
                ccliente.Login_acceso(usuario, encripta)

                'Ejecuto la instrucci�n hacia la base de datos.
                'If ccliente.BanInicio = 1 Then
                Session("ClienteCotiza") = 0
                Session("usuario") = ccliente.usuario
                Session("id_usuario") = dtV.Rows(0)("id_usuario")
                Session("nombre") = ccliente.Empresa
                Session("nombre_nivel") = ccliente.NomNivel
                Session("nivel") = ccliente.niveles
                Session("IdNivel") = ccliente.nivel
                Session("bid") = ccliente.Bid
                Session("bidConauto") = ccliente.BidConauto
                Session("NombreRegion") = ccliente.Region
                'Session("Region") = ccliente.idRegion
                Session("moneda") = 0
                Session("InterestRate") = "B"
                Session("tiposeguro") = 1
                'Session("polizainicio") = ccliente.poliza_inicio()
                Session("UsuarioRegistroBandera") = ccliente.UsurarioRegistro
                Session("intmarcabid") = ccliente.intmarca
                Session("IdMarca") = ccliente.intmarca
                Session("programa") = ccliente.programa

                'nos indica a que programa tiene permiso

                If Session("nombre") <> ccliente.Distribuidor_Bid Then
                    Session("NombreDistribuidor") = ccliente.Distribuidor_Bid
                Else
                    Session("NombreDistribuidor") = Nothing
                End If
                Session("bancss") = ccliente.banCss
                'inicializamos en blanco
                'Session("ImagenFondo") = "Imagenes\Nuevo\Logo.jpg"
                'Nos indica el numero de registro con el que cuenta el usuario
                Session("contadoruser") = ccliente.contador
                'If dtV.Rows(0)("programa") > 1 Then
                '    If Not ccliente.niveles = 3 Then
                '        Response.Redirect("wfrmmenu.aspx")
                '    Else
                '        Solo_Output()
                '    End If
                'Else
                '    Session("programa") = ccliente.programa
                '    Session("version") = ccliente.carga_version(Session("programa"))
                'End If
                Session("programa") = ccliente.programa
                Session("version") = ccliente.carga_version(Session("programa"))
                'Autentifico al usuario y continuo con el proceso.
                FormsAuthentication.RedirectFromLoginPage(usuario, False)

                '20160615 MAVILA
                'If Not ccliente.Valida_Password(encripta) Then
                '    Session("mensaje") = "La contrase�a no contiene los elementos m�nimos de seguridad favor de modificarla"
                '    Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                'Else
                '    If Not ccliente.Verifica_Vigencia_Password(Convert.ToInt32(dtV.Rows(0)("id_usuario").ToString)) Then
                '        Session("mensaje") = "La contrase�a ha expirado favor de modificarla"
                '        Response.Redirect("~\administrador\WfrmCambioPassword.aspx")
                '    End If
                'End If
            Else
                'Mitigar la vulnerabilidad - Password attack
                Dim rand As New Random()
                System.Threading.Thread.Sleep(rand.Next(1, 20) * 1000)

                ValorContador = ccliente.registra_accesos_negados(usuario)
                Session("ContadorUsuario") = Session("ContadorUsuario") + ValorContador
                If Session("ContadorUsuario") >= 3 Then
                    ccliente.Update_Usuario_Bloqueado(usuario)
                    lbl_msg0.Text = "Por seguridad el usuario esta bloqueado, consulte a su administrador"
                    'MsgBox.ShowMessage("Por seguridad el usuario esta bloqueado, consulte a su Administrador")
                Else
                    lbl_msg0.Text = "El usuario no existe � la contrase�a no es v�lida"
                    'MsgBox.ShowMessage("El usuario no existe � la contrase�a no es v�lida")
                End If
                activa(True)
                SetFocus(txtUsuario)
                Exit Sub
            End If

            If Session("UsuarioRegistroBandera") <> 2 Then
                If password.Length = 10 Then
                    banKey = Mid(password, password.Length - 3, 4)
                Else
                    banKey = ""
                End If
                If Not banKey = "779N" Then
                    If Session("UsuarioRegistroBandera") > 0 Then
                        If dtV.Rows(0)("programa") > 1 Then
                            If dtV.Rows(0)("programa") = 32 Then
                                Response.Redirect("wfrmInicioAdmin.aspx")
                            Else
                                Response.Redirect("wfrmmenu.aspx")
                            End If
                        Else
                            If Not ccliente.niveles = 3 Then
                                If Session("contadoruser") > 1 Then
                                    Response.Redirect("WfrmInicioadmin.aspx")
                                Else
                                    Response.Redirect("HPrincipal.htm")
                                End If
                            Else
                                Solo_Output()
                            End If
                        End If
                    Else
                        Response.Redirect("HPrincipal.htm")
                        'Response.Redirect("administrador\WfrmCambioPassword.aspx")
                    End If
                Else
                    Response.Redirect("..\administrador\WfrmCambioPassword.aspx")
                End If
            Else
                Response.Redirect("frmLogin.aspx?ban=1")
            End If

        Catch ex As Exception
            lbl_msg0.Text = "El usuario no existe � la contrase�a no es v�lida."
            'MsgBox.ShowMessage("El usuario no existe � la contrase�a no es v�lida.")
            activa(True)
            Exit Sub
        End Try
    End Sub

    Public Function Solo_Output()
        Response.Redirect("administrador/HPrincipal1.htm", True)
    End Function

    Public Sub activa(ByVal ban As Boolean)
        lbl_msg.Visible = ban
    End Sub

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos d�gitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()
    End Function

End Class
