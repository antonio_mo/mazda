<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wfrmlogooff.aspx.vb" Inherits="Cotizador_Mazda_Retail.wfrmlogooff" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>Cotizador</title>
        <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
        <script Language="JavaScript">
            if (history.forward(1)) {
                history.replace(history.forward(1));
            }            
        </script>

        <script type="text/javascript">
            function cerrarVentana() {
                window.close();
                return false;
            }
        </script>
	    <style type="text/css">
            .auto-style1 {
                height: 59px;
            }
            .auto-style2 {
                font-weight: bold;
                font-size: 10pt;
                color: black;
                font-family: Arial, Helvetica, sans-serif;
                height: 59px;
            }
        </style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
            <TABLE id="Table2" style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 8px; bottom: 446px;" cellSpacing="0"
				cellPadding="0" width="100%" border="0" leftMargin="0" topMargin="0" rightMargin="0">
				<TR>
					<TD vAlign="middle" align="left" width="15%"><!--<asp:image id="Image1" runat="server" ImageUrl="Imagenes\Logo_catweb copia.gif"></asp:image>--><asp:image id="imgLogo" runat="server" ImageUrl=""
							ImageAlign="Baseline"></asp:image></TD>
					<TD width="28%"></TD>
					<TD style="HEIGHT: 68px" vAlign="middle" width="10%"></TD>
					<TD width="5%"></TD>
					<TD vAlign="middle" align="left" width="27%">
						<P align="right">
							<asp:image id="Logo_Aon" runat="server" ImageUrl="..\Imagenes\logos\logo.jpg" style="Z-INDEX: 0"></asp:image></P>
					</TD>
					<TD style="HEIGHT: 68px" vAlign="top" align="right" width="5%"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 100; POSITION: absolute; HEIGHT: 277px; TOP: 190px; LEFT: 8px" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD style="HEIGHT: 24px" width="25%" background="..\Imagenes\Nuevo\img_fondomenu.gif"
						colSpan="3"></TD>
				</TR>
				<TR>
					<TD width="25%"></TD>
					<TD width="50%" vAlign="bottom" align="center">
						<asp:Label id="lbl_msg" runat="server" CssClass="negrita_media" Width="100%"></asp:Label></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="auto-style1"></TD>
					<TD class="auto-style2"><p align="center">
							<asp:Label id="lbl_nota" runat="server" Width="100%" CssClass="negrita_media">Gracias por utilizar los servicios electr�nicos. <br> Presione Regresar para volver a la pantalla de entrada.</asp:Label></p>
					</TD>
					<TD class="auto-style1"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD vAlign="top" align="center">
						<asp:button id="cmd_regresar" runat="server" CssClass="boton" Text="Regresar" Visible="False"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD vAlign="top" align="center">
						&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD vAlign="top" align="center">
                        <asp:button id="cmdContinuar" runat="server" CssClass="boton" Text="Continuar" ></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:button id="cmdSalir" runat="server" CssClass="boton" Text="Salir" Width="87px" OnClientClick="return cerrarVentana();"></asp:button></TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD vAlign="top" align="center">
						&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD vAlign="top" align="center">
						<asp:Panel ID="pnlAcceso" runat="server" Height="115px" Visible="False">
                            <TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1">
                                <TR>
                                    <TD vAlign="top" width="45%">
                                        <TABLE id="Table4" tabIndex="10" border="0" cellSpacing="0" width="100%">
                                            <TR>
                                                <TD class="negrita" align="center" colspan="2">
                                                    <asp:Label ID="lbl_nota0" runat="server" CssClass="negrita_media" Width="100%">Favor de ingresar la informaci�n de acceso</asp:Label>
                                                </TD>
                                            </TR>
                                            <tr>
                                                <td align="right" class="negrita">&nbsp;</td>
                                                <td align="left">&nbsp;</td>
                                            </tr>
                                            <TR>
                                                <TD class="negrita" align="right"><FONT face="Arial"><B tabIndex="8">C�digo de Usuario:</B></FONT></TD>
                                                <TD align="left"><FONT face="Arial">
                                                    <asp:textbox id="txtUsuario" tabIndex="1" onkeypress="" runat="server"
															                        Width="90px" MaxLength="10" CssClass="DatosInicio"></asp:textbox>
                                                    &nbsp;
                                                    <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" ErrorMessage="C�digo de Usuario">*</asp:requiredfieldvalidator>
                                                    </FONT></TD>
                                            </TR>
                                            <TR>
                                                <TD class="negrita" align="right">
                                                    <font face="Arial"><b tabindex="9">Contrase�a:</b></font>
                                                </TD>
                                                <td align="left"><font face="Arial">
                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="DatosInicio" MaxLength="10" onkeypress="" tabIndex="2" TextMode="Password" Width="90px"></asp:TextBox>
                                                    &nbsp;
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="Contrase�a">*</asp:RequiredFieldValidator>
                                                    </font></td>
                                            </TR>
                                            <tr>
                                                <td align="center" class="negrita" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="negrita" colspan="2">
                                                    <asp:Button ID="cmdAceptar" runat="server" CssClass="boton" Text="Aceptar" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="negrita" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="negrita" colspan="2"><font face="Arial">
                                                    <asp:ValidationSummary ID="Validationsummary2" runat="server" Font-Names="Tahoma" Font-Size="12px" HeaderText="Los siguientes campos son obligatorios" />
                                                    </font></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="negrita" colspan="2"><font face="Arial">
                                                    <asp:Label ID="lbl_msg0" runat="server" CssClass="style2" Visible="False"></asp:Label>
                                                    </font></td>
                                            </tr>
                                        </TABLE>
                                    </TD>
                                </TR>
                                <TR>
                                    <td align="center" class="EtiquetasLogin">&nbsp;</td>
                                </TR>
                            </TABLE>
                        </asp:Panel>
                    </TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD vAlign="top" align="center">
						&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>			
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 101; POSITION: absolute; TOP: 400px; LEFT: 560px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
