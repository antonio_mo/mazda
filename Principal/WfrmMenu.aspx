<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmMenu.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>Cotizador</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
        <script Language="JavaScript">
            if (history.forward(1)) {
                history.replace(history.forward(1));
            }
        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" cellSpacing="0"
				cellPadding="0" width="100%" border="0" rightMargin="0" topMargin="0" leftMargin="0">
				<TR>
					<TD vAlign="middle" align="left" width="15%"><!--<asp:image id="Image1" runat="server" ImageUrl="Imagenes\Logo_catweb copia.gif"></asp:image>-->
						<asp:image style="Z-INDEX: 0" id="Logo_Sistema" runat="server" ImageUrl="..\Imagenes\logos\Vacio.gif"
							ImageAlign="Baseline"></asp:image></TD>
					<TD vAlign="middle" width="5%"></TD>
					<TD vAlign="middle" width="35%">
						<asp:label id="lbl_nombre" runat="server" Width="100%" CssClass="Nombredepto" BorderColor="Transparent"></asp:label></TD>
					<TD vAlign="middle" align="right" width="18%">
						<P>
							<asp:label id="lbl_cargo" runat="server" Width="100%" CssClass="Nombredepto" BorderColor="Transparent"
								style="Z-INDEX: 0"></asp:label></P>
					</TD>
					<TD width="5%"></TD>
					<TD vAlign="middle" align="left" width="22%">
						<P align="right">
							<asp:image style="Z-INDEX: 0" id="Logo_Aon" runat="server" ImageUrl="..\Imagenes\logos\logo.jpg"></asp:image></P>
					</TD>
					<TD vAlign="top" align="right" width="5%"></TD>
				</TR>
                
				<TR>
					<TD colSpan="6">
						<TABLE id="Table2" style="HEIGHT: 24px" height="24" cellSpacing="1" cellPadding="1" width="100%"
							border="0">
							<TR>
								<TD width="25%" background="..\Imagenes\Nuevo\img_fondomenu.gif" colSpan="3" height="10"
									align="right">.</TD>
							<TR>
								<TD width="35%" colSpan="3">
									<asp:label id="lbl_recomendacion1" runat="server" Width="100%" CssClass="Nombredepto_centro"
										BorderColor="Transparent">Seleccione el Programa</asp:label></TD>
							</TR>
							<TR>                                
								<TD width="35%" colSpan="3" align="center" valign="middle">
                                    <br /><br /><br /><br /><br />
									<asp:Table id="tb_logo" runat="server" Width="80%"></asp:Table>
								</TD>
							</TR>
						</TABLE>
						<P></P>
					</TD>
					<TD></TD>
				</TR>

			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 102; POSITION: absolute; TOP: 512px; LEFT: 408px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
