<%@ Page Language="vb" AutoEventWireup="false" Codebehind="frmLogin.aspx.vb" Inherits="Cotizador_Mazda_Retail.frmLogin" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>Cotizador</title>        
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" type="text/css" href="../Css/Styles.css">
		<script language="javascript" src="../JavaScript/JScripts.js"></script>
		<!--<link href="imagenes\Original.ico" type="image/x-icon" rel="shortcut icon">-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-32951061-5', 'auto');
            ga('send', 'pageview');

        </script>
        <script Language="JavaScript">
            if (history.forward(1)) {
                history.replace(history.forward(1));
            }
        </script>
	    <style type="text/css">
            #Table3 {
                width: 94%;
            }
        </style>
	</HEAD>
	<body>
		<cc1:msgbox style="Z-INDEX: 101; POSITION: absolute; TOP: 632px; LEFT: 40px" id="MsgBox" runat="server"></cc1:msgbox>
		<TABLE style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table2" border="0"
			cellSpacing="0" cellPadding="0" width="100%" height="100%">
			<TR>
				<TD width="5%"></TD>
				<TD width="90%"></TD>
				<TD width="5%"></TD>
			</TR>
			<TR>
				<TD colSpan="3" align="center">					
					<DIV style="WIDTH: 800px" tabIndex="7">
						<FORM id="Form1" method="post" name="login" action="frmlogin.aspx" target="_parent" runat="server">
                         <table border="0" style="WIDTH: 100%">
							<tr>
								<td align="left" colspan="2">&nbsp;<asp:Image ID="imgLogo" runat="server" />
								</td>
                                <td>&nbsp;</td>
								<td align="right" class="auto-style1" colspan="1"><IMG src="..\Imagenes\logos\logo.jpg">
								</td>
							</tr>
							<tr>
								<td height="70" vAlign="bottom" colSpan="4" align="center">
									<asp:Label ID="lblTitulo" runat="server" class="TituloPagAcceso"></asp:Label>
								</td>
							</tr>
                             <tr>
                                 <td align="center" colspan="4">
                                     <table border="0" align="center">
                                         <tr>
                                             <td align="center" class="auto-style2">
                                                 <!--<SCRIPT type="text/javascript">Flash_Portada('../Emisor_Polizas_Conauto.swf');</SCRIPT>-->
						                        <!--<img src ="..\Imagenes\logos\Logo.jpg">-->
							                        <FONT size="-1"><FONT class="EtiquetasLogin" tabIndex="10" face="Arial"><FONT face="Arial"><STRONG class="EtiquetasLogin" tabIndex="6">
											                        <HR class="divisorlogin" tabIndex="11" SIZE="8">
											                        Favor de indicar sus datos de Acceso al Sistema</STRONG></FONT><BR>
									                        Por su seguridad, la informaci�n proporcionada se mostrar� encriptada. </FONT>
							                        </FONT><FONT face="Arial">
								                        <BR>
							                        </FONT>
							                        <BR>
							                        <HR color="#dcdcdc" SIZE="1">
							                        <TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1">
								                        <TR>
									                        <TD vAlign="top" width="55%">
										                        <TABLE id="Table1" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications."
											                        border="0" cellSpacing="0" cellPadding="0">
											                        <TR>
												                        <TD vAlign="top" width="135" align="center">
													                        <SCRIPT src="https://seal.verisign.com/getseal?host_name=www.aonseguros.com.mx&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=es"></SCRIPT>
												                        </TD>
												                        <TD class="small" vAlign="middle" width="155">This site chose VeriSign <A href="http://www.verisign.es/products-services/security-services/ssl/ssl-information-center/"
														                        target="_blank">SSL</A> for secure e-commerce and confidential 
													                        communications.</TD>
											                        </TR>
											                        <TR>
												                        <TD vAlign="top" align="center"><A style="TEXT-ALIGN: center; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT: bold 7px verdana,sans-serif; COLOR: #000000; TEXT-DECORATION: none; PADDING-TOP: 0px"
														                        href="http://www.verisign.es/products-services/security-services/ssl/ssl-information-center/" target="_blank">Acerca 
														                        de los certificados SSL</A></TD>
												                        <TD>&nbsp;</TD>
											                        </TR>
										                        </TABLE>
									                        </TD>
									                        <TD vAlign="top" width="45%">
										                        <TABLE id="Table4" tabIndex="10" border="0" cellSpacing="0" width="100%">
											                        <TR>
												                        <TD class="negrita" align="right"><FONT face="Arial"><B tabIndex="8"><FONT face="Arial"><B tabIndex="8">C�digo 
																	                        de Usuario:</B></FONT></B></FONT></TD>
												                        <TD align="left"><FONT face="Arial"><asp:textbox id="txtUsuario" tabIndex="1" onkeypress="" runat="server"
															                        Width="90px" MaxLength="10" CssClass="DatosInicio"></asp:textbox>&nbsp;
														                        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" ErrorMessage="C�digo de Usuario">*</asp:requiredfieldvalidator></FONT></TD>
											                        </TR>
											                        <TR>
												                        <TD class="negrita" align="right"><FONT face="Arial"><B tabIndex="9"><FONT face="Arial"><B tabIndex="9"><FONT face="Arial"><B tabIndex="9"><FONT face="Arial"><B tabIndex="9"><FONT face="Arial"><B tabIndex="9">Contrase�a:</B></FONT></B></FONT></B></FONT></B></FONT></B></FONT></TD>
												                        <TD align="left"><FONT face="Arial"><asp:textbox id="txtPassword" tabIndex="2" onkeypress="" runat="server"
															                        Width="90px" MaxLength="20" CssClass="DatosInicio" TextMode="Password"></asp:textbox>&nbsp;
														                        <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="Contrase�a">*</asp:requiredfieldvalidator></FONT></TD>
											                        </TR>
											                        <TR>
												                        <TD class="negrita" colSpan="2" align="center"><asp:button id="cmdAceptar" runat="server" CssClass="boton" Text="Ingresar"></asp:button></TD>
											                        </TR>
										                        </TABLE>
									                        </TD>
								                        </TR>
                                                        <TR>
                                                            <td align="center" colspan="2" class="EtiquetasLogin">&nbsp;</td>
                                                        </TR>
                                                        <TR>
                                                            <td align="center" colspan="2" class="EtiquetasLogin">Solo ejecutar con Internet Explorer</td>
                                                        </TR>
                                                        <TR>
                                                            <td align="center" colspan="2" style="color:black;font-family:Arial, Helvetica, sans-serif;font-weight: bold;font-size:20px;text-align:center;vertical-align:middle;"></td>
                                                        </TR>
							                        </TABLE>
							                        <HR tabIndex="11" color="gainsboro" SIZE="1">
							                        <FONT face="Arial">
								                        <HR class="divisorlogin" tabIndex="11" SIZE="8">
							                        </FONT>
							                        <P><FONT face="Arial"><asp:validationsummary id="Validationsummary2" runat="server" HeaderText="Los siguientes campos son obligatorios"
										                        Font-Names="Tahoma" Font-Size="12px"></asp:validationsummary><asp:label id="lbl_msg" runat="server" CssClass="style2" Visible="False"></asp:label></FONT></P>


                                             </td>
                                         </tr>
                                    </table>
                                 </td>                                 
                             </tr>
						</table>              
						</FORM>
					</DIV>
				</TD>
			</TR>
			<TR>
				<TD vAlign="bottom"></TD>
				<TD vAlign="top">
					<HR style="WIDTH: 100%" class="DivisorloginPrivacidad" tabIndex="11" SIZE="2">
					<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="2" width="100%">
						<TR>
							<TD class="AvisoPrivacidad">Copyright 2012 � | <A href="http://www.aon.com/mexico/about-aon/aviso-de-privacidad-ARS.jsp" target="_blank">
									<FONT color="#2186bd"><B>Aviso de Privacidad</B></FONT></A> | T�rminos y 
								Condiciones | Aon M�xico es parte de Aon Corporation situada en Chicago 
								Illinois, E.U.A. Aon es el intermediario de seguros m�s grande del mundo con 
								presencia en 120 pa�ses, 500 oficinas y 60,000 asesores.<B>Somos asesores en 
									seguros, fianzas, beneficios para empleados, retiro, talento y compensaciones</B></TD>
						</TR>
					</TABLE>
				</TD>
				<TD vAlign="bottom" align="right"></TD>
			</TR>
			<TR>
				<TD vAlign="bottom">
					<asp:image id="Logo_Aon" runat="server" ImageUrl="..\Imagenes\logos\vacio.gif" Height="48px"></asp:image></TD>
				<TD vAlign="top" align="center">&nbsp;&nbsp;</TD>
				<TD vAlign="bottom" align="right">
					<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\logos\vacio.gif" Height="48px"></asp:image></TD>
			</TR>
		</TABLE>
	</body>
</HTML>
