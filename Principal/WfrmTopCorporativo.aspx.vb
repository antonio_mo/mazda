Partial Class WfrmTopCorporativo
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objcell As TableCell
        Dim objrow As TableRow

        If Session("usuario") = "" Or Session("usuario") = Nothing Then
            Response.Write("<script>window.open('wfrmlogooff.aspx','_parent');</script>")
        End If

        objrow = New TableRow
        objcell = New TableCell
        objcell.Text = Session("NombreDistribuidor")
        objcell.CssClass = "negrita_media"
        objcell.VerticalAlign = VerticalAlign.Middle
        objcell.HorizontalAlign = HorizontalAlign.Center
        objcell.Width = Unit.Percentage(100)
        objrow.Controls.Add(objcell)
        tb_region.Controls.Add(objrow)

    End Sub
End Class
