Imports CN_Negocios
Partial Class WfrmMenuPortada
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Image1 As System.Web.UI.WebControls.Image

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    'Variables que utilizaremos para capturar los datos de env�o al Web Service
    Private ccliente As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('wfrmlogooff.aspx','_parent');</script>")
            End If

            pnl_portada.Visible = True
            carga_portada()
            Session("LlaveC") = Nothing
            Session("Llave") = Nothing
            Session("cveFscar") = Nothing
            Session("ClienteCotiza") = 0
            Session("cotizacion") = 0
            Session("idpoliza") = 0
            Session("Exclusion") = -1
            Session("IdCotiUnica") = Nothing
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
    'H�ctor
    Public Sub carga_portada()
        Dim dt As DataTable
        Dim filas As Integer
        Dim fila As Integer
        Dim columnas As Integer
        Dim columna As Integer
        Dim contador As Integer = 0
        Dim bandera_carga As Integer = 0
        Dim tipo_url As Integer = 0
        Dim tipo As Char = "E"
        Dim Bandera As Integer = 0
        Dim i, j, k As Integer
        Dim Renglon As Integer

        'tbl_listado.GridLines = GridLines.Both
        'tbl_listado.BorderWidth = Unit.Pixel(1)

        tbl_listado.Rows.Clear()
        dt = ccliente.menu_portada(tipo, Session("contadoruser"), Session("programa"), Session("nivel"), Session("IdNivel"))
        filas = dt.Rows.Count
        columnas = 4
        Try
            If dt.Rows.Count > 0 Then
                For fila = 1 To filas
                    Dim tbrow As New TableRow
                    For columna = 1 To columnas
                        Dim lbl As New Label
                        Dim tbcell As New TableCell
                        Dim hp As New HyperLink

                        'Borde superior
                        If Bandera = 0 Then
                            For Renglon = 1 To 2
                                Dim tbrow1 As New TableRow
                                For i = 1 To 4
                                    Dim lbl1 As New Label
                                    Dim celda1 As New TableCell

                                    lbl1.Text = ""
                                    celda1.Controls.Add(lbl1)
                                    Select Case i
                                        Case 1
                                            If Renglon = 1 Then
                                                'celda1.BackColor = New System.Drawing.Color().Gray
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(3)
                                            Else
                                                'celda1.CssClass = "FondoMenu1"
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(5)
                                            End If
                                            celda1.Width = Unit.Percentage(5)

                                        Case 2
                                            If Renglon = 1 Then
                                                'celda1.BackColor = New System.Drawing.Color().Gray
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(3)
                                            Else
                                                celda1.CssClass = "FondoMenu2"
                                                celda1.Height = Unit.Point(5)
                                            End If
                                            celda1.Width = Unit.Percentage(10)

                                        Case 3
                                            If Renglon = 1 Then
                                                'celda1.BackColor = New System.Drawing.Color().Gray
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(3)
                                            Else
                                                celda1.CssClass = "FondoMenu2"
                                                celda1.Height = Unit.Point(5)
                                            End If
                                            celda1.Width = Unit.Percentage(80)

                                        Case 4
                                            If Renglon = 1 Then
                                                'celda1.BackColor = New System.Drawing.Color().Gray
                                                'celda1.CssClass = "FondoMenu3"
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(3)
                                            Else
                                                'celda1.CssClass = "FondoMenu3"
                                                celda1.CssClass = "negrita"
                                                celda1.Height = Unit.Point(5)
                                            End If
                                            celda1.Width = Unit.Percentage(5)
                                    End Select
                                    tbrow1.Controls.Add(celda1)
                                Next i
                                tbl_listado.Rows.Add(tbrow1)
                            Next
                            Bandera = 1
                        End If

                        'Men�
                        Select Case columna
                            Case 1
                                lbl.Text = ""
                                tbcell.Controls.Add(lbl)
                                tbcell.CssClass = "FondoMenu1"
                                tbcell.Width = Unit.Percentage(5)
                                tbcell.Height = Unit.Point(10)
                                tbcell.VerticalAlign = VerticalAlign.Middle
                                tbrow.Controls.Add(tbcell)
                            Case 2
                                lbl.Text = ""
                                tbcell.Controls.Add(lbl)
                                tbcell.CssClass = "FondoMenu2"
                                tbcell.Width = Unit.Percentage(10)
                                tbcell.Height = Unit.Point(10)
                                tbcell.HorizontalAlign = HorizontalAlign.Right
                                tbcell.VerticalAlign = VerticalAlign.Top
                                tbrow.Controls.Add(tbcell)
                            Case 3
                                If Not dt.Rows(fila - 1).IsNull("id_portada") Then
                                    bandera_carga = 1
                                    'Nombre del indice
                                    If Not dt.Rows(fila - 1).IsNull("nombre_indice") Then
                                        If Session("programa") = 9 _
                                            And dt.Rows(fila - 1)("nombre_indice") = "Cliente Prospecto" Then
                                            hp.Text = "Cotizaci�n Prospecto"
                                        Else
                                            hp.Text = dt.Rows(fila - 1)("nombre_indice")
                                        End If
                                    End If
                                    If Not dt.Rows(fila - 1).IsNull("url_indice") Then
                                        If dt.Rows(fila - 1)("nombre_indice") = "Cotizador" Then
                                            Select Case Session("programa")
                                                Case 1
                                                    hp.NavigateUrl = "..\cotizar\WfrmCotizaPromoM.aspx?unico=0&click_menu=1"
                                                Case 2
                                                    hp.NavigateUrl = "..\cotizar\WfrmCotizaPromoM.aspx?unico=0&click_menu=1"
                                                Case Else
                                                    hp.NavigateUrl = dt.Rows(fila - 1)("url_indice")
                                            End Select
                                            ''Session("programa") = 29
                                            ''hp.NavigateUrl = "..\cotizar\WfrmCotizaPromo.aspx"
                                            '''hp.NavigateUrl = dt.Rows(fila - 1)("url_indice")
                                        Else
                                            hp.NavigateUrl = dt.Rows(fila - 1)("url_indice")
                                        End If
                                        'blanco
                                        'hp.ForeColor = New System.Drawing.Color().White
                                        hp.Target = dt.Rows(fila - 1)("target")
                                    Else
                                        hp.NavigateUrl = ""
                                    End If
                                    If Session("programa") = 9 Then
                                        hp.ForeColor = New System.Drawing.Color().White
                                    End If
                                End If
                                tbcell.Controls.Add(hp)
                                tbcell.HorizontalAlign = HorizontalAlign.Left
                                tbcell.VerticalAlign = VerticalAlign.Middle
                                tbcell.CssClass = "FondoMenu2"
                                tbcell.Width = Unit.Percentage(80)
                                tbcell.Height = Unit.Point(10)
                                tbrow.Controls.Add(tbcell)
                            Case 4
                                lbl.Text = ""
                                tbcell.Controls.Add(lbl)
                                'tbcell.CssClass = "FondoMenu3"
                                tbcell.CssClass = "negrita"
                                tbcell.Width = Unit.Percentage(5)
                                tbcell.Height = Unit.Point(10)
                                tbrow.Controls.Add(tbcell)
                        End Select
                    Next columna
                    tbl_listado.Rows.Add(tbrow)

                    If bandera_carga = 1 Then
                        Dim tbrow2 As New TableRow
                        For columna = 1 To 4
                            Dim lbl As New Label
                            Dim celda As New TableCell
                            Dim img As New HtmlImage

                            Select Case columna
                                Case 1
                                    lbl.Text = ""
                                    celda.Controls.Add(lbl)
                                    celda.CssClass = "FondoMenu1"
                                    celda.Width = Unit.Percentage(5)
                                    celda.Height = Unit.Point(5)
                                    tbrow2.Controls.Add(celda)

                                Case 2
                                    lbl.Text = ""
                                    celda.Controls.Add(lbl)
                                    celda.CssClass = "FondoMenu2"
                                    celda.Width = Unit.Percentage(10)
                                    celda.Height = Unit.Point(5)
                                    tbrow2.Controls.Add(celda)

                                Case 3
                                    If fila < filas Then
                                        'img.Src = "Imagenes\logos\separador.jpg"
                                        img.Src = "..\Imagenes\logos\sepa2.jpg"
                                        img.Width = 120
                                        celda.Controls.Add(img)
                                    Else
                                        lbl.Text = ""
                                        celda.Controls.Add(lbl)
                                    End If
                                    celda.Width = Unit.Percentage(80)
                                    celda.Height = Unit.Point(5)
                                    celda.CssClass = "FondoMenu2"
                                    celda.VerticalAlign = VerticalAlign.Middle
                                    tbrow2.Controls.Add(celda)

                                Case 4
                                    lbl.Text = ""
                                    celda.Controls.Add(lbl)
                                    'celda.CssClass = "FondoMenu3"
                                    celda.CssClass = "negrita"
                                    celda.Width = Unit.Percentage(5)
                                    celda.Height = Unit.Point(5)
                                    tbrow2.Controls.Add(celda)
                            End Select
                        Next columna
                        tbl_listado.Rows.Add(tbrow2)
                        bandera_carga = 0
                        tipo_url = 0
                    End If
                Next fila

                'Borde inferior
                Dim tbrow3 As New TableRow
                For i = 1 To 4
                    Dim lbl1 As New Label
                    Dim celda1 As New TableCell

                    lbl1.Text = ""
                    celda1.Controls.Add(lbl1)
                    Select Case i
                        Case 1
                            'celda1.BackColor = New System.Drawing.Color().Gray
                            celda1.CssClass = "FondoMenu1"
                            celda1.Width = Unit.Percentage(5)

                        Case 2
                            'celda1.BackColor = New System.Drawing.Color().Gray
                            celda1.CssClass = "FondoMenu1"
                            celda1.Width = Unit.Percentage(10)

                        Case 3
                            'celda1.BackColor = New System.Drawing.Color().Gray
                            celda1.CssClass = "FondoMenu1"
                            celda1.Width = Unit.Percentage(80)

                        Case 4
                            'celda1.BackColor = New System.Drawing.Color().Gray
                            celda1.CssClass = "negrita"
                            celda1.Width = Unit.Percentage(5)
                    End Select
                    celda1.Height = Unit.Point(3)
                    tbrow3.Controls.Add(celda1)
                Next i
                tbl_listado.Rows.Add(tbrow3)

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

End Class
