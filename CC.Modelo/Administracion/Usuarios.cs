﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CC.Modelo.Administracion
{
    public class Usuarios
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Perfil { get; set; }
        public string Quejas { get; set; }
        public string Acceso { get; set; }
        public string Aseguradora { get; set; }
        public bool IsLocked { get; set; }
        public bool IsActive { get; set; }
    }

    public class UsuariosTabla
    {
        public int IdUser { get; set; }
        public string UserName { get; set; }
        public string Nombre { get; set; }
        public string Perfil { get; set; }
    }
}
