﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Modelo.Reporte.Polizas
{
    public class Coberturas
    {
        public string Cobertura { get;set; }
        public string Descripcion_Suma { get; set; }
        public string Deducible { get; set; }
        public decimal? PrimaCobertura { get; set; }
    }
}
