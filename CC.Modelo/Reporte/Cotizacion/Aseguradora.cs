﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Modelo.Reporte.Cotizacion
{
    public class Aseguradora
    {
        public short IdAseguradora { get; set; }
        public string DescripcionAseguradora { get; set; }
        public string UrlImage { get; set; }
        public int IdRecargo { get; set; }
        public string DescripcionRecargo { get; set; }
        public short IdPaquete { get; set; }
        public string DescripcionPaquete { get; set; }
        public int? orden { get; set; }
        public decimal? PrimerRecibo { get; set; }
        public decimal? ReciboSubsecuente { get; set; }
        public decimal? PrimaTotal { get; set; }
    }
}
