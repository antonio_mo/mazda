﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Modelo.Reporte.Cotizacion
{
    public class CotizacionUnica : Modelo.Entities.COTIZACION_UNICA
    {        
        public int Promocion { get; set; }
        public int Promocion12Meses { get; set; }
        public string DescripcionHomo { get; set; }
        public string DescripcionBID { get; set; }
    }
}
