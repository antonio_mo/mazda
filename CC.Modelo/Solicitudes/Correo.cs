﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Modelo.Solicitudes
{
    public class Correo
    {
        public string Para { get; set; }
        public string CC { get; set; }

        public string IdSolicitud { get; set; }
        public string Detalle { get; set; }
        public string Observaciones { get; set; }
    }
}
