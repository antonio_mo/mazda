﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Modelo.Solicitudes
{
    public class Poliza
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Aseguradora { get; set; }
        public int IdBid { get; set; }
        public string NumeroSerie { get; set; }
        public string Contrato { get; set; }
        public string NombreCliente { get; set; }
        public string APaternoCliente { get; set; }
        public string AMaternoCliente { get; set; }
    }
}
