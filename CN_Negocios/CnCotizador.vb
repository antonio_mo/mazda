Imports CD_Datos
Public Class CnCotizador
    Dim cnegocio As New CdCotizador
    Public Plaza As Integer = 0
    Public subramo As Integer = 0
    Public Anio As String = ""
    Public Plan As Integer = 0
    Public Catalogo As String = ""
    Public statusCotiza As String = ""
    Public banVida As Integer = 0
    Public bandes As Integer = 0
    Public banfactura As Integer = 0
    Public DescrAseg As String = ""
    Public UrlImagAseg As String = ""
    Public StrGrupo As String = ""
    Public StrIntegrante As String = ""
    Public StrSerie As String = ""
    Private StrContratoGNP As String = ""
    Private StrCaratulaGNP As String = ""

    Public Sub carga_cadena_impresion(ByVal Idpoliza As Long, ByVal strpoliza As String, ByVal strcadena As String, ByVal strtipo As Byte)
        Cnegocio.carga_cadena_impresion(Idpoliza, strpoliza, strcadena, strtipo)
    End Sub

    Public Function usuario_bid(ByVal intusuario As Integer) As DataTable
        Return cnegocio.usuario_bid(intusuario)
    End Function

    Public Function carga_region(ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_region(intbid)
    End Function

    Public Function carga_marca(ByVal intmarca As Integer, ByVal intprograma As Integer, ByVal strestatus As String) As DataTable
        Return cnegocio.carga_marca(intmarca, intprograma, strestatus)
    End Function

    Public Function carga_modelo(ByVal intmarca As Integer, ByVal strtipoa As String, ByVal intprograma As Integer, ByVal intmarcabase As Integer) As DataTable
        Return cnegocio.carga_modelo(intmarca, strtipoa, intprograma, intmarcabase)
    End Function

    Public Function carga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal strtipoa As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer) As DataTable
        Return cnegocio.carga_anio(intmarca, strmodelo, strtipoa, intprograma, intmarcaoriginal)
    End Function

    'Public Function carga_clave_zurich(ByVal strestatus As String, ByVal strmodelo As String, _
    'ByVal strcatalogo As String, ByVal strdescripcion As String, ByVal stranio As String, ByVal intmarca As Integer) As String
    '    Dim cadena As String = ""
    '    Dim dt As DataTable = cnegocio.carga_clave_zurich(strestatus, strmodelo, _
    '                strcatalogo, strdescripcion, stranio, intmarca)
    '    Try
    '        If dt.Rows.Count > 0 Then
    '            If Not dt.Rows(0).IsNull("INSCO_vehiculo") Then
    '                cadena = dt.Rows(0)("INSCO_vehiculo")
    '            End If
    '        End If
    '        Return cadena
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function carga_insco_homologado_vehiculo(ByVal intvehiculo As Integer, _
   ByVal stranio As String, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_insco_homologado_vehiculo(intvehiculo, stranio, intaseguradora)
    End Function

    Public Function carga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal strtipoa As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer) As DataTable
        Return cnegocio.carga_descripcion(intmarca, strmodelo, stranio, strtipoa, intprograma, intmarcaoriginal)
    End Function

    Public Function carga_subramo_homologado(ByVal intvehiculo As Integer, ByVal strestatus As String, ByVal intprograma As Integer, ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer) As DataTable
        Dim dt As DataTable = cnegocio.carga_subramo_homologado(intvehiculo, strestatus, intprograma, intmarca, intmarcaoriginal)
        Try
            If dt.Rows.Count > 0 And dt.Rows.Count <= 1 Then
                If dt.Rows(0).IsNull("subramo_vehiculo") Then
                    subramo = 0
                Else
                    subramo = dt.Rows(0)("subramo_vehiculo")
                End If
                If dt.Rows(0).IsNull("catalogo_vehiculo") Then
                    Catalogo = 0
                Else
                    Catalogo = dt.Rows(0)("catalogo_vehiculo")
                End If
            Else
                subramo = 0
                Catalogo = ""
            End If

            Return dt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_plaza(ByVal intvehiculo As Integer) As DataTable
        Dim dt As DataTable = cnegocio.carga_plaza(intvehiculo)
        Try
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("plaza_vehiculo") Then
                    Plaza = 0
                Else
                    Plaza = dt.Rows(0)("plaza_vehiculo")
                End If
                If dt.Rows(0).IsNull("subramo_vehiculo") Then
                    subramo = 0
                Else
                    subramo = dt.Rows(0)("subramo_vehiculo")
                End If
                If dt.Rows(0).IsNull("anio_vehiculo") Then
                    Anio = 0
                Else
                    Anio = dt.Rows(0)("anio_vehiculo")
                End If
                If dt.Rows(0).IsNull("plan_vehiculo") Then
                    Plan = 0
                Else
                    Plan = dt.Rows(0)("plan_vehiculo")
                End If
                If dt.Rows(0).IsNull("catalogo_vehiculo") Then
                    Catalogo = 0
                Else
                    Catalogo = dt.Rows(0)("catalogo_vehiculo")
                End If
            Else
                Plaza = 0
                subramo = 0
                Anio = 0
                Plan = 0
                Catalogo = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_uso(ByVal intprograma As Integer, ByVal intaon As Integer, ByVal strestatus As String, ByVal intsubramo As Integer, ByVal strsession As String) As DataTable
        Return cnegocio.carga_uso(intprograma, intaon, strestatus, intsubramo, strsession)
    End Function

    Public Function Carga_definicion_uso(ByVal intprograma As Integer, ByVal intsubramo As Integer, ByVal struso As String, ByVal intversion As Integer) As DataTable
        Return cnegocio.Carga_definicion_uso(intprograma, intsubramo, struso, intversion)
    End Function

    Public Function carga_vehiculo_general(ByVal intvehiculo As Integer, ByVal stranio As String, _
        ByVal intaseguradora As Integer, ByVal intprograma As Integer) As DataTable

        Dim strinsco As String = ""
        Dim strstatus As String = ""
        Dim dt As DataTable = carga_insco_homologado_vehiculo(intvehiculo, stranio, intaseguradora)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("insco") Then
                strinsco = dt.Rows(0)("insco")
            End If
            If Not dt.Rows(0).IsNull("estatus") Then
                strstatus = dt.Rows(0)("estatus")
            End If
            statusCotiza = strstatus
        End If
        Return cnegocio.carga_vehiculo_general(strinsco, intprograma, statusCotiza, intaseguradora, intvehiculo, stranio)
    End Function

    Public Function carga_vehiculo_homologado(ByVal intvehiculo As Integer, ByVal intprograma As Integer, _
    ByVal stranio As String, ByVal strestatus As String) As DataTable
        Return cnegocio.carga_vehiculo_homologado(intvehiculo, intprograma, stranio, strestatus)
    End Function

    Public Function descripcion_paquete(ByVal intpaquete As Integer) As String
        Dim cadena As String = ""
        Dim dt As DataTable = cnegocio.descripcion_paquete(intpaquete)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("descripcion_paquete") Then
                    cadena = dt.Rows(0)("descripcion_paquete")
                End If
            End If
            Return cadena
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_paquete(ByVal strsubramo As String) As DataTable
        Return cnegocio.carga_paquete(strsubramo)
    End Function

    Public Function carga_contrato(ByVal intbid As Integer) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.carga_contrato(intbid)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("numero") Then
                    valor = dt.Rows(0)("numero")
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_coberturas(ByVal strseguro As String, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intprograma As Integer, _
    ByVal intaonv As Integer, ByVal strestatus As String, ByVal stranio As Integer) As DataTable
        Try
            'cnegocio.inserta_coberturas(subramoA, strseguro, intcontrato, intbid, intprograma, intaonv, strestatus, stranio)
            cnegocio.inserta_coberturas(strseguro, intcontrato, intbid, intprograma, intaonv, strestatus, stranio)
            'Return cnegocio.carga_cobertura_bid(intcontrato, intbid, strestatus, subramoA)
            Return cnegocio.carga_cobertura_bid(intcontrato, intbid, strestatus, intprograma, intaonv)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_coberturas_Cotizacion(ByVal intcontrato As Integer, _
   ByVal intbid As Integer, ByVal intprograma As Integer, _
  ByVal intaonv As Integer, ByVal strestatus As String, ByVal intversion As Integer) As DataTable
        Try
            Return cnegocio.carga_cobertura_bid(intcontrato, intbid, strestatus, intprograma, intaonv)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_grid_edit(ByVal arr() As String, ByVal intcap As Integer) As Integer
        Return cnegocio.valida_grid_edit(arr, intcap)
    End Function

    Public Function carga_cobertura_bid_aseguradora(ByVal intcontrato As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid_aseguradora(intcontrato, intbid)
    End Function

    Public Function carga_cobertura_bid_aseguradora_total_cob(ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal strestatus As String, ByVal strdescripcionPaquete As String, _
    ByVal intaseguradora As Integer, ByVal strpaquete As String) As DataTable
        Return cnegocio.carga_cobertura_bid_aseguradora_total_cob(intcontrato, intbid, strestatus, strdescripcionPaquete, intaseguradora, strpaquete)
    End Function

    Public Function carga_coberturas_tmep() As DataTable
        Return cnegocio.carga_coberturas_tmep()
    End Function

    Public Function carga_coberturas_detalle_tmep(ByVal strestatus As String, ByVal intcobtemp As Integer, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_coberturas_detalle_tmep(strestatus, intcobtemp, intaseguradora)
    End Function

    Public Function carga_asegurador_coberturas_detalle_tmep(ByVal intcotiza As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer) As DataTable
        Return cnegocio.carga_asegurador_coberturas_detalle_tmep(intcotiza, intbid, intcontrato)
    End Function

    Public Function carga_asegurador_coberturas_contrato(ByVal intcontrato As Integer, ByVal intcliente As Integer, _
    ByVal intbid As Integer, ByVal intnumcontrato As Integer) As DataTable
        Return cnegocio.carga_asegurador_coberturas_contrato(intcontrato, intcliente, intbid, intnumcontrato)
    End Function

    Public Function carga_cobertura_bid_aseguradora_imp(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid_aseguradora_imp(intcontrato, intbid, intaseguradora)
    End Function

    Public Function carga_cobertura_total_cob_aseguradora_ramo(ByVal intcontrato As Integer, _
        ByVal intbid As Integer, ByVal intcobertura As Integer, ByVal idaseguradora As Integer) As DataTable
        Return cnegocio.carga_cobertura_total_cob_aseguradora_ramo(intcontrato, intbid, _
            intcobertura, idaseguradora)
    End Function

    Public Function restale_carga_cobertura_total_cob_aseguradora_ramo(ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intcobertura As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable
        Return cnegocio.restale_carga_cobertura_total_cob_aseguradora_ramo(intcontrato, intbid, intcobertura, intaseguradora, intpaquete)
    End Function

    Public Function carga_cobertura_bid(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strestatus As String, ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid(intcontrato, intbid, strestatus, intprograma, intaonv)
    End Function

    Public Function carga_cobertura_bid_sincob(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strestatus As String, _
    ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid_sincob(intcontrato, intbid, strestatus, intprograma, intaonv)
    End Function

    Public Function carga_cobertura_bid_aseguradora_total_cob_contrato(ByVal intcontrato As Integer, _
ByVal intbid As Integer, ByVal intcob As Integer, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid_aseguradora_total_cob_contrato(intcontrato, intbid, intcob, intaseguradora)
    End Function

    Public Sub actualiza_cobertura(ByVal dbsuma As Double, ByVal strdescripcion As String, _
    ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intnumplazo As Integer)
        cnegocio.actualiza_cobertura(dbsuma, strdescripcion, strfactura, intcobertura, _
                intcontrato, intbid, intnumplazo)
    End Sub

    Public Sub actualiza_cobertura_VALOR(ByVal strdescripcion As String, _
    ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intsubramo As Integer, ByVal intnumplazo As Integer, _
    ByVal strfacV As Double)

        cnegocio.actualiza_cobertura_VALOR(strdescripcion, strfactura, intcobertura, _
                        intcontrato, intbid, intsubramo, intnumplazo, strfacV, 0, "")
    End Sub


    Public Function determina_facvigencia_aseguradora(ByVal strmoneda As String, _
    ByVal strvigencia As String, ByVal strtipopol As String, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal strdescripcion As String, ByVal strfactura As String, _
    ByVal intcobertura As Integer, ByVal intsubramo As Integer, _
    ByVal intnumplazo As Integer, ByVal strsumcob As String) As DataTable
        Dim i As Integer = 0
        Dim anio As Integer = 0
        Dim mes As Integer = 0
        Dim dbvalor As Double = 0
        Dim cuenta As Integer = 1
        Dim intaseguradora As Integer = 0

        anio = Math.Round((strvigencia / 12)) * 12
        mes = strvigencia Mod 12

        If mes > 0 Then
            cuenta = cuenta + 1
        End If

        For i = 0 To cuenta - 1
            If i = 0 Then
                strvigencia = anio
            Else
                strvigencia = mes
            End If
            Dim dt As DataTable = cnegocio.determina_facvigencia_aseguradora(strmoneda, strvigencia, _
                        strtipopol, intcontrato, intbid)
            If dt.Rows.Count > 0 Then
                dbvalor = dbvalor + dt.Rows(0)("valor")
                intaseguradora = dt.Rows(0)("id_aseguradora")
            End If
        Next
        If dbvalor > 0 Then
            cnegocio.actualiza_cobertura_VALOR(strdescripcion, strfactura, _
                    intcobertura, intcontrato, intbid, intsubramo, _
                    intnumplazo, dbvalor, _
                    intaseguradora, strsumcob)
        End If
        'Dim dt As DataTable = cnegocio.determina_facvigencia_aseguradora(strmoneda, strvigencia, _
        '        strtipopol, intcontrato, intbid)
        'If dt.Rows.Count > 0 Then
        '    'For i = 0 To dt.Rows.Count - 1
        '    cnegocio.actualiza_cobertura_VALOR(strdescripcion, strfactura, _
        '        intcobertura, intcontrato, intbid, intsubramo, _
        '        intnumplazo, dt.Rows(i)("valor"), _
        '        dt.Rows(i)("id_aseguradora"), strsumcob)
        '    'Next
        'End If
    End Function


    Public Function determina_facvigencia(ByVal strmoneda As String, ByVal strvigencia As String, ByVal strtipopol As String) As Double
        Dim valor As Double
        Try
            Dim dt As DataTable = cnegocio.determina_facvigencia(strmoneda, strvigencia, strtipopol)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("valor") Then
                    valor = dt.Rows(0)("valor")
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_cobertura_VALOR_cero(ByVal strdescripcion As String, _
        ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
        ByVal intbid As Integer, ByVal intnumplazo As Integer, _
ByVal strsumcob As String)
        cnegocio.actualiza_cobertura_VALOR_cero(strdescripcion, strfactura, intcobertura, _
        intcontrato, intbid, intnumplazo, strsumcob)
    End Sub

    Public Function Consulta_cliente_prospecto(ByVal intbid As Integer, ByVal strnombre As String) As DataTable
        Return cnegocio.Consulta_cliente_prospecto(intbid, strnombre)
    End Function

    Public Function DatosClienteP(ByVal intbid As Integer, ByVal strnombre As String, ByVal strtipo As String, Optional ByVal intclientep As Integer = 0) As DataTable
        Return cnegocio.DatosClienteP(intbid, strnombre, strtipo, intclientep)
    End Function

    Public Function Inserta_Clientep(ByVal intbid As Integer, ByVal strpaterno As String, _
    ByVal strmaterno As String, ByVal strnombre As String, _
    ByVal strtipo As String, ByVal strrazon As String, _
    ByVal strtel As String, ByVal strelaboro As String, _
    ByVal strestatus As String, ByVal strcp As String, _
    ByVal strestado As String, ByVal strprovincia As String) As Integer
        Return cnegocio.Inserta_Clientep(intbid, strpaterno, strmaterno, strnombre, _
        strtipo, strrazon, strtel, _
        strelaboro, strestatus, strcp, strestado, strprovincia)
    End Function

    Public Sub modifica_clientep(ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strnombre As String, ByVal strtipo As String, _
    ByVal strrazon As String, ByVal strtel As String, _
    ByVal strelaboro As String, ByVal strestatus As String, _
    ByVal intclientep As Integer, ByVal intbid As Integer, _
    ByVal strcp As String, ByVal strestado As String, ByVal strprovincia As String)
        cnegocio.modifica_clientep(strpaterno, strmaterno, strnombre, _
            strtipo, strrazon, strtel, strelaboro, _
            strestatus, intclientep, intbid, strcp, _
            strestado, strprovincia)
    End Sub

    Public Function consulta_cliente(ByVal intbid As Integer, ByVal strnombre As String) As DataTable
        Return cnegocio.consulta_cliente(intbid, strnombre)
    End Function

    Public Function carga_inf_cliente(ByVal intbid As Integer, ByVal strnombre As String, ByVal strtipo As String) As DataTable
        Return cnegocio.carga_inf_cliente(intbid, strnombre, strtipo)
    End Function

    Public Function cargando_cp(ByVal scp As String) As DataTable
        Return cnegocio.cargando_cp(scp)
    End Function

    Public Function actualiza_cliente(ByVal strnombre As String, ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strfecha As String, ByVal strrfc As String, ByVal strlugar As String, ByVal strocupacion As String, _
    ByVal strfiscal As String, ByVal strrazon As String, ByVal strlegal As String, ByVal strcontacto As String, _
    ByVal strtel As String, ByVal strestatus As String, ByVal strempleado As String, ByVal stragencia As String, _
    ByVal strcp As String, ByVal strdir As String, ByVal strcol As String, ByVal strciudad As String, _
    ByVal strestado As String, ByVal intbid As Integer, ByVal intcliente As Integer, ByVal inttipocliente As Integer, _
    ByVal intban As Integer, _
    ByVal strbeneficiario As String, ByVal stremail As String) As Integer
        Return cnegocio.actualiza_cliente(strnombre, strpaterno, strmaterno, strfecha, strrfc, strlugar, _
        strocupacion, strfiscal, strrazon, strlegal, strcontacto, strtel, strestatus, strempleado, _
        stragencia, strcp, strdir, strcol, strciudad, strestado, intbid, intcliente, _
        inttipocliente, intban, strbeneficiario, stremail)
    End Function

    Public Function inserta_cliente(ByVal intbid As Integer, _
    ByVal strnombre As String, ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strfecha As String, ByVal strrfc As String, ByVal strlugar As String, ByVal strocupacion As String, _
    ByVal strfiscal As String, ByVal strrazon As String, ByVal strlegal As String, ByVal strcontacto As String, _
    ByVal strtel As String, ByVal strestatus As String, ByVal strempleado As String, ByVal stragencia As String, _
    ByVal strcp As String, ByVal strdir As String, ByVal strcol As String, ByVal strciudad As String, _
    ByVal strestado As String, ByVal intban As Integer, ByVal strbeneficiario As String, _
    ByVal stremail As String, ByVal strbanbeneficiario As String, _
    ByVal strinterior As String, ByVal strexterior As String) As Integer
        Return cnegocio.inserta_cliente(intbid, strnombre, strpaterno, strmaterno, strfecha, strrfc, strlugar, _
        strocupacion, strfiscal, strrazon, strlegal, strcontacto, strtel, strestatus, strempleado, _
        stragencia, strcp, strdir, strcol, strciudad, strestado, intban, _
        strbeneficiario, stremail, strbanbeneficiario, strinterior, strexterior)
    End Function

    Public Function carga_version(ByVal intversion As Integer, ByVal intaseguradora As Integer) As String
        Dim cadena As String = ""
        Dim dt As DataTable = cnegocio.carga_version(intversion, intaseguradora)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("vigencia") Then
                    cadena = dt.Rows(0)("vigencia")
                End If
            End If
            Return cadena
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_clienteP_impresion(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_clienteP_impresion(intcliente, intbid)
    End Function

    Public Function carga_cliente_impresion(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_cliente_impresion(intcliente, intbid)
    End Function

    Public Function Inserta_contrato(ByVal intcliente As Integer, ByVal intbid As Integer, _
    ByVal strplazo As String, ByVal strnumpago As String, ByVal Dcontrato As String, _
    ByVal strtipo As String, ByVal strnumcontrato As String, ByVal fechaf As String, _
    ByVal intaseguradora As Integer, ByVal strbene As String, _
    ByVal dbenganche As Double, ByVal strContactoCob As String) As Integer
        'Dim fechaf As String = ""
        'Dim max As Integer = strplazo / 12
        Try
            'fechaf = DateAdd(DateInterval.Month, max, CDate(strfechai))
            Return cnegocio.Inserta_contrato(intcliente, intbid, strplazo, strnumpago, _
            Dcontrato, fechaf, strtipo, strnumcontrato, intaseguradora, strbene, dbenganche, strContactoCob)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_contrato_coti(ByVal intcliente As Integer, ByVal intbid As Integer, _
   ByVal strplazo As String, ByVal strnumpago As String, ByVal strfechai As String, _
   ByVal strtipo As String, ByVal strnumcontrato As String, ByVal fechaf As String, _
   ByVal intnumconcob As Integer) As Integer
        'Dim fechaf As String = ""
        'Dim max As Integer = strplazo / 12
        Try
            'fechaf = DateAdd(DateInterval.Month, max, CDate(strfechai))
            Return cnegocio.Inserta_contrato_coti(intcliente, intbid, strplazo, _
                            strnumpago, strfechai, fechaf, strtipo, _
                            strnumcontrato, intnumconcob)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function max_poliza(ByVal intaseguradora As String, ByVal intsubramo As Integer, ByVal bantipopol As Integer, ByVal intbid As Integer, _
    ByVal banestatusparametro As Integer, ByVal strestatus As String, ByVal IdPrograma As Integer, ByVal tiporpoducto As Integer, ByVal Intcontador As Integer) As String
        'traemos la poliza mas alta que exista en poliza
        Dim valor As Long = 0
        Dim Salida As String = ""
        Dim dtPA As DataTable = Nothing
        Dim Aseguradora1 As Integer = 0
        Dim bandCruzada As Integer = 0
        Dim IdPoliza As Long = 0

        Try

            'evi 28/06/2013
            'HH 08/01/2014 se valida rangos de p�lizas
            'HM 06/03/2014 se agrega GNP por ajustar poliza
            'EVI 15/12/2014 programa Conauto 29 = (59,60,61)
            If (IdPrograma = 1 Or IdPrograma = 2) Then
                dtPA = cnegocio.PolizaAjustadas(intaseguradora, IdPrograma, tiporpoducto, Intcontador)
                If dtPA.Rows.Count > 0 Then
                    If Not dtPA.Rows(0).IsNull("maxid") Then
                        Salida = dtPA.Rows(0)("maxid")
                        If IdPrograma = 29 Then
                            If Not dtPA.Rows(0).IsNull("contrato") Then
                                StrContratoGNP = dtPA.Rows(0)("contrato")
                            End If
                            If Not dtPA.Rows(0).IsNull("caratula") Then
                                StrCaratulaGNP = dtPA.Rows(0)("caratula")
                            End If
                        End If
                        GoTo PolTexto
                    End If
                End If
                If Salida = "" Then
                    Salida = "0"
                    GoTo PolTexto
                End If
            Else
                Dim dt As DataTable = cnegocio.Max_poliza(intaseguradora, intsubramo, bantipopol, intbid, banestatusparametro, strestatus)
                If dt.Rows.Count > 0 Then
                    If Not dt.Rows(0).IsNull("maxid") Then
                        'IdPoliza = dt.Rows(0)("maxid")
                        valor = dt.Rows(0)("maxid")
                    End If
                End If

                'Dim dtP As DataTable = cnegocio.Poliza(IdPoliza)
                'If dtP.Rows.Count > 0 Then
                '    If Not dtP.Rows(0).IsNull("maxpoliza") Then
                '        valor = dtP.Rows(0)("maxpoliza")
                '    End If
                'End If
            End If

            If valor = 0 Then
                'si es 0 entonces sacamos el n�mero de parametros
                Dim dtA As DataTable = cnegocio.poliza_aseguradora(intaseguradora, intsubramo, bantipopol, intbid, banestatusparametro, strestatus)
                If dtA.Rows.Count > 0 Then
                    valor = dtA.Rows(0)("valor")
                End If
            Else
                'si trae datos de poliza validamos que este entre los rangos
                Dim dtv As DataTable = cnegocio.rango_poliza_parametro(intaseguradora, intsubramo, bantipopol, intbid, banestatusparametro, strestatus)
                If dtv.Rows.Count > 0 Then
                    If dtv.Rows(0)("polizainicio") <= valor And valor <= dtv.Rows(0)("polizafinal") Then
                        valor = valor
                    Else
                        valor = 0
                        'valor = dtv.Rows(0)("polizainicio")
                    End If
                Else
                    valor = 0
                End If
            End If
            Salida = valor
PolTexto:
            'Return valor
            Return Salida
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function max_poliza_Ace(ByVal intaseguradora As String, ByVal intbid As Integer, ByVal intbantipopol As Integer) As Long
        'traemos la poliza mas alta que exista en poliza
        Dim dt As DataTable = cnegocio.Max_poliza_ace(intbid, intaseguradora)
        Dim valor As Long = 0
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("maxid") Then
                valor = dt.Rows(0)("maxid")
            End If
        End If
        If valor = 0 Then
            'si es 0 entonces sacamos el numero de parametros
            Dim dtA As DataTable = cnegocio.poliza_aseguradora_Ace(intaseguradora, intbid, intbantipopol)
            If dtA.Rows.Count > 0 Then
                valor = dtA.Rows(0)("valor")
            End If
        Else
            'si trae datos de poliza validamos que este entre los rangos
            Dim dtv As DataTable = cnegocio.rango_poliza_parametro_ace(intaseguradora, intbid, intbantipopol)
            If dtv.Rows.Count > 0 Then
                If dtv.Rows(0)("PolizaIniAce") <= valor And valor <= dtv.Rows(0)("PolizaFinAce") Then
                    valor = valor
                Else
                    valor = 0
                End If
            Else
                valor = 0
            End If
        End If
        Return valor
    End Function

    Public Function max_poliza_coti(ByVal intbid As Integer, ByVal polizaInicio As String) As Long
        Dim dt As DataTable = cnegocio.Max_poliza_coti(intbid)
        Dim valor As Long = 0
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("maxid") Then
                valor = dt.Rows(0)("maxid")
            End If
        End If
        If valor = 0 Then
            valor = polizaInicio + 1
        End If
        Return valor
    End Function

    Public Function inserta_poliza(ByVal intbid As Integer, ByVal polizainicio As String, _
    ByVal strtipopol As String, ByVal intseguro As Integer, _
    ByVal NumPlazoPoliza As Integer, ByVal fechaini As String, _
    ByVal strdeeler As String, ByVal strmoneda As String, ByVal intaonH As Integer, _
    ByVal arrpoliza() As String, ByVal strserie As String, ByVal intuso As Integer, _
    ByVal strcobertura As String, ByVal strmotor As String, _
    ByVal strcontrato As String, ByVal strprimaneta As String, _
    ByVal strderpol As String, ByVal vlivaseguro As String, ByVal strprimatotal As String, _
    ByVal bunidadfinanciada As String, ByVal strzurich As String, ByVal strcolor As String, _
    ByVal strplazo As String, ByVal arrsub() As String, ByVal strprecio_ctrl As String, _
    ByVal strprecio As String, ByVal intversion As Integer, ByVal strestatus As String, _
    ByVal strventa As String, _
    ByVal intcliente As Integer, ByVal intregion As Integer, _
    ByVal intcontratopol As Integer, ByVal tcontrato As Integer, _
    ByVal intbanf As Integer, ByVal fechavalor As String, _
    ByVal intusuario As Integer, ByVal strrenave As String, _
    ByVal intaseguradora As Integer, ByVal stranio As String, _
    ByVal intsubramo As Integer, ByVal strmodelo As String, _
    ByVal intpaquete As Integer, ByVal intprograma As Integer, _
    ByVal strfincon As String, ByVal strbansegvida As Integer, _
    ByVal bansegdesempleo As Integer, ByVal strfactura As String, _
    ByVal dbiva As Double, ByVal TipoFinanciamiento As String, _
    Optional ByVal Strfracciones As String = "", _
    Optional ByVal strCveFscar As String = "", _
    Optional ByVal intcotizacion As Long = 0, _
    Optional ByVal plazoFinanciamiento As Integer = 0, _
    Optional ByVal MontoPagoFraccionado As Double = 0, _
    Optional ByVal PorMontoPagoFraccionado As Double = 0, _
    Optional ByVal inttiporecargo As Integer = 0, _
    Optional ByVal intnumrecargo As Integer = 0, _
    Optional ByVal PagoInicial As Double = 0, _
    Optional ByVal PagoSubsecuente As Double = 0, _
    Optional ByVal strcadenaDano As String = "", _
    Optional ByVal IdTipoCarga As Integer = 0, _
    Optional ByVal tiporpoducto As Integer = -1, _
    Optional ByVal Intcontador As Integer = 0, Optional ByVal IdCotizaCobertura As Integer = 0) As Integer

        Dim strinsco As String = ""
        Dim stropcsa As String = ""
        '----
        Dim contador_M As Integer
        Dim NumPolizaCredito As Integer
        'Dim poliza As Long = 0
        Dim poliza As String = "0"
        Dim paqueteA As Integer = 0
        Dim opcsa As String = ""
        Dim bantipopol As Integer = 0
        Dim bandeduc As Integer = 0
        Dim polizaAce As Long = 0
        Dim banAce As String = 0
        Dim BanEstatusParametro As Integer = 0
        Dim dbRecargo As Double = 0
        Dim BanRecargo As Integer = 0
        Dim BanFactor As Integer = 0

        If strprecio_ctrl = 0 Then strprecio_ctrl = strprecio

        Dim dtP As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
        If dtP.Rows.Count > 0 Then
            If Not dtP.Rows(0).IsNull("ban_tipopoliza") Then
                bantipopol = dtP.Rows(0)("ban_tipopoliza")
            End If
            If Not dtP.Rows(0).IsNull("ban_deduc") Then
                bandeduc = dtP.Rows(0)("ban_deduc")
            End If
            If Not dtP.Rows(0).IsNull("ban_ace") Then
                banAce = dtP.Rows(0)("ban_ace")
            End If
            If Not dtP.Rows(0).IsNull("ban_estatus_parametro") Then
                BanEstatusParametro = dtP.Rows(0)("ban_estatus_parametro")
            End If
            If Not dtP.Rows(0).IsNull("ban_recargo") Then
                BanRecargo = dtP.Rows(0)("ban_recargo")
            End If
            If Not dtP.Rows(0).IsNull("ban_factores") Then
                BanFactor = dtP.Rows(0)("ban_factores")
            End If
            If BanRecargo = 1 And strtipopol = "F" Then
                If Not dtP.Rows(0).IsNull("recargo") Then
                    dbRecargo = dtP.Rows(0)("recargo")
                    dbRecargo = dbRecargo / 100
                End If
            End If
        End If

        poliza = max_poliza(intaseguradora, intsubramo, bantipopol, intbid, BanEstatusParametro, strestatus, intprograma, tiporpoducto, Intcontador)
        If banAce = 1 Then
            polizaAce = max_poliza_Ace(intaseguradora, intbid, bantipopol)
        End If

        If poliza = "0" Then
            Return 0
        End If

        'If intprograma = 4 Then
        If banAce = 1 Then
            If polizaAce = 0 Then
                Return 0
            End If
        End If

        NumPolizaCredito = 1
        If strtipopol = "A" Or strtipopol = "F" Then
            contador_M = 0
        Else
            If intseguro = 0 Then
                contador_M = 0
            Else
                contador_M = 0
                'esto lo comentarie ya que si es financiada hace varias polizas en vez de ser solo una 
                'contador_M = 4
                ''si es multianual se aplica esta
                'If TipoFinanciamiento = "MO" Then
                '    'financiado MO 25 y 37
                '    NumPolizaCredito = Math.Round((NumPlazoPoliza / 12), 0)
                'Else
                '    'financiado WOR todos
                '    NumPolizaCredito = Math.Round(((NumPlazoPoliza / 12) + 0.416666666676), 0)
                'End If
                'contador_M = NumPolizaCredito - 1
            End If
        End If

        Dim dtI As DataTable = cnegocio.carga_insco_homologado_inserta(intaonH, stranio, intaseguradora, strestatus, intsubramo)
        If dtI.Rows.Count > 0 Then
            If Not dtI.Rows(0).IsNull("insco") Then
                strinsco = dtI.Rows(0)("insco")
            End If
        End If

        Dim dtPaquete As DataTable = cnegocio.paquete_aseguradora(intsubramo, intaseguradora, intpaquete)
        If dtP.Rows.Count > 0 Then
            'subramoA = dtp.Rows(0)("subramo")
            paqueteA = dtPaquete.Rows(0)("paquete")
            'intpaquete = dtp.Rows(0)("id_paquete")
            If dtPaquete.Rows(0).IsNull("opcsa_paquete") Then
                stropcsa = ""
            Else
                stropcsa = dtPaquete.Rows(0)("opcsa_paquete")
            End If
        End If

        'Dim dt As DataTable
        'If strestatus = "N" Then
        '    dt = cnegocio.tarifac(strmoneda, intsubramo, intpaquete, _
        '        stranio, strmodelo, intregion, 1, _
        '        strtipopol, intaseguradora, stropcsa)
        '    If dt.Rows.Count > 0 Then
        '        If Not dt.Rows(0).IsNull("opcsa_tarifac") Then
        '            stropcsa = dt.Rows(0)("opcsa_tarifac")
        '        End If
        '    End If
        'Else
        '    dt = cnegocio.tariprima(strmoneda, intsubramo, intpaquete, _
        '        stranio, strinsco, intregion, 1, _
        '        strtipopol, intaseguradora)
        '    If dt.Rows.Count > 0 Then
        '        If Not dt.Rows(0).IsNull("opcsa_tariprima") Then
        '            stropcsa = dt.Rows(0)("opcsa_tariprima")
        '        End If
        '    End If
        'End If


        Return cnegocio.inserta_poliza(intbid, strtipopol, intseguro, contador_M, fechaini, _
        NumPlazoPoliza, intpaquete, intsubramo, stropcsa, strdeeler, strmoneda, intaonH, _
        arrpoliza, strserie, intuso, strcobertura, strmotor, _
        strcontrato, strprimaneta, strderpol, vlivaseguro, strprimatotal, _
        bunidadfinanciada, strzurich, strcolor, strplazo, arrsub, strprecio_ctrl, strprecio, _
        intversion, strestatus, intpaquete, poliza, strventa, _
        intcliente, intregion, intcontratopol, tcontrato, intbanf, fechavalor, _
        intusuario, strrenave, intaseguradora, stranio, bandeduc, intprograma, _
        strfincon, polizaAce, strbansegvida, bansegdesempleo, paqueteA, _
        strfactura, dbiva, TipoFinanciamiento, Strfracciones, dbRecargo, _
        BanRecargo, BanFactor, strCveFscar, intcotizacion, plazoFinanciamiento, _
        MontoPagoFraccionado, PorMontoPagoFraccionado, _
        inttiporecargo, intnumrecargo, _
        PagoInicial, PagoSubsecuente, _
        strcadenaDano, IdTipoCarga, tiporpoducto, Intcontador, _
        StrContratoGNP, StrCaratulaGNP, IdCotizaCobertura)
        '"092" & polizaAce
    End Function

    Public Function inserta_poliza_recotizada(ByVal intprograma As Integer, ByVal intaseguradora As Integer, _
    ByVal intsubramo As Integer, ByVal intbid As Integer, ByVal strestatus As String, _
    ByVal strserie As String, ByVal strrenave As String, ByVal strmotor As String, ByVal strplaca As String, _
    ByVal strfactura As String, ByVal intcotizacion As Integer, ByVal intpaquete As Integer, _
    ByVal tcontrato As String, ByVal intcliente As Integer, ByVal strtipopol As String, _
    ByVal strcveFscar As String, ByVal intusuario As Integer) As Integer

        Dim poliza As Long = 0
        Dim bantipopol As Integer = 0
        Dim bandeduc As Integer = 0
        Dim polizaAce As Long = 0
        Dim banAce As String = 0
        Dim BanEstatusParametro As Integer = 0
        Dim dbRecargo As Double = 0
        Dim BanRecargo As Integer = 0
        Dim BanFactor As Integer = 0

        Dim dtP As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
        If dtP.Rows.Count > 0 Then
            If Not dtP.Rows(0).IsNull("ban_tipopoliza") Then
                bantipopol = dtP.Rows(0)("ban_tipopoliza")
            End If
            If Not dtP.Rows(0).IsNull("ban_deduc") Then
                bandeduc = dtP.Rows(0)("ban_deduc")
            End If
            If Not dtP.Rows(0).IsNull("ban_ace") Then
                banAce = dtP.Rows(0)("ban_ace")
            End If
            If Not dtP.Rows(0).IsNull("ban_estatus_parametro") Then
                BanEstatusParametro = dtP.Rows(0)("ban_estatus_parametro")
            End If
            If Not dtP.Rows(0).IsNull("ban_recargo") Then
                BanRecargo = dtP.Rows(0)("ban_recargo")
            End If
        End If

        poliza = max_poliza(intaseguradora, intsubramo, bantipopol, intbid, BanEstatusParametro, strestatus, 0, 0, 0)
        If banAce = 1 Then
            polizaAce = max_poliza_Ace(intaseguradora, intbid, bantipopol)
        End If

        If poliza = 0 Then
            Return 0
        End If

        'If intprograma = 4 Then
        If banAce = 1 Then
            If polizaAce = 0 Then
                Return 0
            End If
        End If

        Return cnegocio.inserta_poliza_recotizada(poliza, strserie, strrenave, strmotor, strplaca, _
        strfactura, intcotizacion, intpaquete, tcontrato, intcliente, intbid, strtipopol, _
        intprograma, polizaAce, strcveFscar, intusuario, intaseguradora)

    End Function

    Public Function Inserta_cotizacion_p(ByVal intnumcont As Integer, ByVal intcliente As Integer, _
    ByVal intbid As Integer, ByVal intvaonh As Integer, ByVal intplazo As Integer, _
    ByVal dbprecio As Double, ByVal strpol As String, ByVal strelaboro As String) As Integer
        Return cnegocio.Inserta_cotizacion_p(intnumcont, intcliente, intbid, intvaonh, intplazo, dbprecio, strpol, strelaboro)
    End Function

    'Public Function inserta_poliza_coti(ByVal intbid As Integer, ByVal polizainicio As String, _
    'ByVal strtipopol As String, ByVal intseguro As Integer, _
    'ByVal NumPlazoPoliza As Integer, ByVal intpaquete As Integer, ByVal opcsa As String, _
    'ByVal fechaini As String, ByVal strdeeler As String, ByVal strmoneda As String, ByVal intvehiculo As Integer, _
    'ByVal arrpoliza() As String, ByVal strserie As String, ByVal intuso As Integer, _
    'ByVal strcobertura As String, ByVal strmotor As String, _
    'ByVal strcontrato As String, ByVal strprimaneta As String, _
    'ByVal strderpol As String, ByVal vlivaseguro As String, ByVal strprimatotal As String, _
    'ByVal bunidadfinanciada As String, ByVal strzurich As String, ByVal strcolor As String, _
    'ByVal strplazo As String, ByVal arrsub() As String, ByVal strprecio_ctrl As String, _
    'ByVal strprecio As String, ByVal intversion As Integer, ByVal strestatus As String, _
    'ByVal strisco As String, ByVal strventa As String, _
    'ByVal intcliente As Integer, ByVal intregion As Integer, _
    'ByVal intcontratopol As Integer, ByVal tcontrato As Integer, _
    'ByVal intbanf As Integer, ByVal fechavalor As String, _
    'ByVal intusuario As Integer, ByVal strrenave As String, ByVal stranio As Integer) As Integer
    '    Dim contador_M As Integer
    '    Dim TipoFinanciamiento As String
    '    Dim NumPolizaCredito As Integer
    '    Dim poliza As Integer = 0
    '    Dim subramoA As Integer = 0
    '    Dim paqueteA As Integer = 0

    '    poliza = max_poliza_coti(intbid, polizainicio)

    '    If strtipopol = "A" Then
    '        contador_M = 0
    '    Else
    '        If intseguro = 0 Then
    '            contador_M = 0
    '            NumPolizaCredito = 1
    '        Else
    '            contador_M = 4
    '            'si es multianual se aplica esta
    '            If TipoFinanciamiento = "MO" Then
    '                'financiado MO 25 y 37
    '                NumPolizaCredito = Math.Round((NumPlazoPoliza / 12), 0)
    '            Else
    '                'financiado WOR todos
    '                NumPolizaCredito = Math.Round(((NumPlazoPoliza / 12) + 0.416666666676), 0)
    '            End If
    '            contador_M = NumPolizaCredito - 1
    '        End If
    '    End If


    '    'Dim dtp As DataTable = cnegocio.paquete(intpaquete)
    '    'If dtp.Rows.Count > 0 Then
    '    '    subramoA = dtp.Rows(0)("subramo")
    '    '    paqueteA = dtp.Rows(0)("paquete")
    '    'End If

    '    Return cnegocio.inserta_poliza_coti(intbid, strtipopol, intseguro, contador_M, fechaini, _
    '    NumPlazoPoliza, intpaquete, subramoA, opcsa, strdeeler, strmoneda, intvehiculo, _
    '    arrpoliza, strserie, intuso, strcobertura, strmotor, _
    '    strcontrato, strprimaneta, strderpol, vlivaseguro, strprimatotal, _
    '    bunidadfinanciada, strzurich, strcolor, strplazo, arrsub, strprecio_ctrl, strprecio, _
    '    intversion, strestatus, intpaquete, poliza, strisco, strventa, _
    '    intcliente, intregion, intcontratopol, tcontrato, intbanf, fechavalor, _
    '    intusuario, strrenave, stranio)
    'End Function

    Public Function imprime_abcreportes(ByVal intprograma As Integer, ByVal strfincon As String, _
    ByVal intpoliza As Integer) As DataTable
        Return cnegocio.imprime_abcreportes(intprograma, strfincon, intpoliza)
    End Function

    Public Function valida_vida_desempelo_impresion(ByVal intpoliza As Integer) As DataTable
        Return cnegocio.valida_vida_desempelo_impresion(intpoliza)
    End Function

    Public Function carga_reporte_poliza(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal intcliente As Integer, ByVal intcontrato As Integer, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_reporte_poliza(intpoliza, intbid, intcliente, intcontrato, intaseguradora)
    End Function

    Public Function carga_reporte_poliza_coti(ByVal intcotizacion As Integer, ByVal intbid As Integer, _
    ByVal intcliente As Integer, ByVal numcontrato As Integer, ByVal intprograma As Integer, _
    ByVal intanio As Integer, ByVal strfincon As String) As DataTable
        'ByVal strestatus As String, ByVal intanio As Integer
        Return cnegocio.carga_reporte_poliza_coti(intcotizacion, intbid, intcliente, _
            numcontrato, intprograma, intanio, strfincon)
        ', strestatus, intanio)
    End Function

    Public Function reporte_limpresp(ByVal intversion As Integer, ByVal intpaquete As Integer) As DataTable
        Return cnegocio.reporte_limpresp(intversion, intpaquete)
    End Function

    Public Function reporte_limpresp_poliza(ByVal intversion As Integer, _
    ByVal intpaquete As Integer, ByVal intbid As Integer, _
    ByVal intcontrato As Integer, ByVal intaseguradora As Integer, _
    ByVal strestatus As String) As DataTable
        Return cnegocio.reporte_limpresp_poliza(intversion, intpaquete, intbid, intcontrato, intaseguradora, strestatus)
    End Function

    Public Function carga_abccancelacion() As DataTable
        Return cnegocio.carga_abccancelacion
    End Function

    Public Function carga_cancelacion(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal intnivel As Integer, Optional ByVal StrCadena As String = "", Optional ByVal StrCriterios As String = "") As DataTable
        Return cnegocio.carga_cancelacion(intbid, intprograma, intnivel, StrCadena, StrCriterios)
    End Function

    Public Function BuscaPoliza(ByVal intbid As Integer, ByVal intprograma As Integer, Optional ByVal StrCadena As String = "", Optional ByVal StrCriterios As String = "") As DataTable
        Dim i As Integer = 0
        Dim dt As DataTable = cnegocio.BuscaPoliza(intbid, intprograma, StrCadena, StrCriterios)
        If intprograma = 9 Then
            Dim dtVD As DataTable = cnegocio.valida_vida_desempleo_consulta(cnegocio.StrSqlSalida)
            If dtVD.Rows.Count > 0 Then
                For i = 0 To dtVD.Rows.Count - 1
                    If dtVD.Rows(i)("bansegvida") Then
                        banVida = 1
                    End If
                    If dtVD.Rows(i)("bansegdesempleo") Then
                        bandes = 1
                    End If
                Next
            End If
        End If
        Return dt
    End Function

    Public Function BuscaPoliza_cancelada(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable
        Return cnegocio.BuscaPoliza_cancelada(intbid, intprograma, StrCadena, StrCriterios)
    End Function

    Public Function valida_dias_cancelacion(ByVal strpoliza As String) As Integer

        Try
            Dim valor As Integer = 0
            Dim minutos As Long = 0
            Dim horas As Integer = 0

            Dim dtM As DataTable = cnegocio.valida_minutos_cancelacion(strpoliza)
            If dtM.Rows.Count > 0 Then
                If Not dtM.Rows(0).IsNull("minutos") Then
                    minutos = dtM.Rows(0)("minutos")
                End If
            End If

            Dim dt As DataTable = cnegocio.valida_dias_cancelacion(minutos)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("dias") Then
                    valor = dt.Rows(0)("dias")
                End If
                If Not dt.Rows(0).IsNull("horas") Then
                    horas = dt.Rows(0)("horas")
                    If horas > 0 Then
                        valor = valor + 1
                    End If
                End If
                If Not dt.Rows(0).IsNull("minutos") Then
                    If horas = 0 Then
                        If dt.Rows(0)("minutos") > 0 Then
                            valor = valor + 1
                        End If
                    End If
                End If
            End If

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DiasCancelacion_Programa(ByVal IdPrograma As Integer) As Integer
        Dim valor As Integer = 0
        Try
            Dim dt As DataTable = cnegocio.DiasCancelacion_Programa(IdPrograma)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("DiasCancelacion_Programa") Then
                    valor = dt.Rows(0)("DiasCancelacion_Programa")
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 06/09/2011 cancelacion usuario
    Public Function inserta_cancelpol(ByVal strpoliza As String, ByVal intbid As Integer, ByVal strserie As String, _
    ByVal intcancelacion As Integer, ByVal intNivel As Integer, ByVal idusuario As Integer, ByVal Programa As Integer) As String
        Dim tipo As String
        Dim contador As Integer = 0
        Dim arrpol As String
        Dim intpoliza As Integer = 0
        Dim i, j As Integer
        Dim poliza As String
        Dim arrPolizas() As String = strpoliza.Split("|")
        Dim PolizasCanceladas As String = ""
        Dim PolizasNoCanceladas As String = ""
        Dim DiasCancelacion As Integer

        'DiasCancelacion = DiasCancelacion_Programa(Programa)

        For i = 0 To arrPolizas.Length - 1
            If Not arrPolizas(i) = "" Then
                If valida_dias_cancelacion(arrPolizas(i)) <= 5 Or intNivel = 0 Then
                    'If valida_dias_cancelacion(arrPolizas(i)) <= DiasCancelacion Or intNivel = 0 Then
                    Dim dt As DataTable = cnegocio.contador_polizas(arrPolizas(i), intbid)
                    If dt.Rows.Count > 0 Then
                        tipo = dt.Rows(0)("tipo_poliza")
                        contador = dt.Rows(0)("contador")
                    End If

                    Dim dtp As DataTable = cnegocio.carga_idpoliza(arrPolizas(i), intbid)
                    If dtp.Rows.Count > 0 Then
                        If Not dtp.Rows(0).IsNull("id_aseguradora") Then
                            intpoliza = dtp.Rows(0)("id_aseguradora")
                        End If
                        For j = 0 To dtp.Rows.Count - 1
                            If j = 0 Then
                                arrpol = dtp.Rows(j)("id_poliza") & ","
                            Else
                                arrpol = arrpol & dtp.Rows(j)("id_poliza") & ","
                            End If
                        Next
                    End If

                    If PolizasCanceladas = "" Then
                        PolizasCanceladas = arrPolizas(i) & ","
                    Else
                        PolizasCanceladas = PolizasCanceladas & arrPolizas(i) & ","
                    End If

                    cnegocio.inserta_cancelpol(arrpol, intbid, strserie, _
                    intcancelacion, tipo, contador, arrPolizas(i), intpoliza, _
                    idusuario)
                Else
                    If PolizasNoCanceladas = "" Then
                        PolizasNoCanceladas = arrPolizas(i) & ","
                    Else
                        PolizasNoCanceladas = PolizasNoCanceladas & arrPolizas(i) & ","
                    End If
                End If
            End If
        Next

        Return PolizasCanceladas & "|" & PolizasNoCanceladas

    End Function

    Public Function consulta_polzas(ByVal strpoliza As String, ByVal intbid As Integer) As DataTable
        Return cnegocio.consulta_polzas(strpoliza, intbid)
    End Function

    Public Function consulta_cancelacion(ByVal intbid As Integer) As DataTable
        Return cnegocio.consulta_cancelacion(intbid)
    End Function

    Public Function inserta_contenedor_reportes(ByVal intbid As Integer, ByVal strpoliza As String, ByVal strtipo As String) As Integer
        Return cnegocio.inserta_contenedor_reportes(intbid, strpoliza, strtipo)
    End Function

    Public Function inserta_contenedor_reportes_grupo(ByVal valor As Integer, ByVal intbid As Integer, ByVal strpoliza As String, ByVal strtipo As String) As Integer
        Return cnegocio.inserta_contenedor_reportes_grupo(valor, intbid, strpoliza, strtipo)
    End Function

    Public Function carga_tabla_impresion(ByVal llave As Integer) As DataTable
        Return cnegocio.carga_tabla_impresion(llave)
    End Function

    Public Function Consulta_contenedor(ByVal llave As Integer, ByVal llave1 As Integer) As DataTable
        Return cnegocio.Consulta_contenedor(llave, llave1)
    End Function

    Public Function sacando_poliza(ByVal intpoliza As Integer) As String
        Dim cadena As String = ""
        Dim dt As DataTable = cnegocio.sacando_poliza(intpoliza)
        If dt.Rows.Count > 0 Then
            cadena = dt.Rows(0)("poliza")
        End If
        Return cadena
    End Function

    Public Function Datos_aseguradora_poliza(ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.Datos_aseguradora_poliza(intaseguradora)
    End Function

    Public Function carga_cobertura_bid_cliente(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer, ByVal strestatus As String, _
    ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable
        Return cnegocio.carga_cobertura_bid_cliente(intcontrato, intbid, intaseguradora, intpaquete, strestatus, intprograma, intaonv)
    End Function

    Public Function contenedor_poliza(ByVal strpoliza As String, ByVal strtipo As String, ByVal intbid As Integer) As DataTable
        Return cnegocio.contenedor_poliza(strpoliza, strtipo, intbid)
    End Function

    Public Function contenedor_poliza_general(ByVal strpoliza As String, ByVal intbid As Integer, ByVal strtipo As String) As DataTable
        Return cnegocio.contenedor_poliza_general(strpoliza, intbid, strtipo)
    End Function

    Public Function maximo_cotiza() As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.maximo_cotiza
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("valor") Then
                    valor = dt.Rows(0)("valor")
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub inserta_cotiza(ByVal intcotiza As Integer, ByVal intcliente As Integer, _
    ByVal intvehiculo As Integer, ByVal intnum As Integer, _
    ByVal intplazo As Integer, ByVal dbprecio As Double, ByVal strtipo As String, _
    ByVal intbid As Integer, ByVal strtaza As String, _
    ByVal dbenganche As Double, ByVal strtipoplan As String, _
    ByVal intprograma As Integer, ByVal strpaquete As String, _
    ByVal strcontrato As String, ByVal strestatus As String, _
    ByVal intanio As Integer, ByVal strCveFscar As String)

        cnegocio.inserta_cotiza(intcotiza, intcliente, intvehiculo, _
        strpaquete, intnum, intplazo, dbprecio, strtipo, intbid, strtaza, _
        dbenganche, strtipoplan, _
        intprograma, strcontrato, strestatus, intanio, strCveFscar)
    End Sub

    Public Function valida_cotizaEmpleado(ByVal intcotiza As Integer) As Integer
        Dim valor As Integer = 0
        Try
            Dim dt As DataTable = cnegocio.valida_cotizaEmpleado(intcotiza)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("id_cotizacion") Then
                    valor = 1
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_recotizar(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_recotizar(intcliente, intbid)
    End Function

    Public Function carga_recotizarXCliente(ByVal intcotiza As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_recotizarXCliente(intcotiza, intbid)
    End Function

    Public Function carga_grupoPaquete(ByVal intcotiza As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.carga_grupoPaquete(intcotiza, intbid)
    End Function

    Public Function carga_PaquetePNeta(ByVal intcotizapoliza As Integer) As DataTable
        Return cnegocio.carga_PaquetePNeta(intcotizapoliza)
    End Function

    Public Function carga_busqueda_cotizacion(ByVal intbid As Integer, ByVal strnombre As String) As DataTable
        Return cnegocio.carga_busqueda_cotizacion(intbid, strnombre)
    End Function

    Public Function Carga_datos_cotizacion(ByVal intbid As Integer, ByVal strnombre As String, ByVal intcotizacion As Integer) As DataTable
        Return cnegocio.Carga_datos_cotizacion(intbid, strnombre, intcotizacion)
    End Function

    Public Function Carga_datos_clienteP(ByVal intcotiza As Integer) As DataTable
        Return cnegocio.Carga_datos_clienteP(intcotiza)
    End Function

    Public Function paquete_aseguradora_subramo(ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer, ByVal intaon As Integer, _
        ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal banunico As Integer, ByVal CadenaPaquete As String) As DataTable
        Return cnegocio.paquete_aseguradora_subramo(intsubramo, strestatus, intpaquete, intprograma, intaon, intcontrato, intbid, banunico, CadenaPaquete)
    End Function

    Public Function cuenta_paquete_aseguradora_subramo(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.cuenta_paquete_aseguradora_subramo(intcontrato, intbid, intsubramo, strestatus, intpaquete, intprograma)
        If dt.Rows.Count > 0 Then
            valor = dt.Rows(0)(0)
        End If
        Return valor
    End Function

    Public Function Carga_aseguradora_cobertura(ByVal intcotiza As Integer, ByVal intbid As Integer, _
    ByVal intcontrato As Integer, ByVal intversion As Integer, ByVal intprograma As Integer, _
    ByVal strfincon As String, ByVal strestatus As String) As DataTable
        Return cnegocio.Carga_aseguradora_cobertura(intcotiza, intbid, intcontrato, _
            intversion, intprograma, strfincon, strestatus)
    End Function

    Public Function carga_coberturas(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String) As DataTable
        Return cnegocio.carga_coberturas(strestatus, intversion, _
            intpaquete, intbid, intcotiza, intaseguradora, strcoberturadescripcion, _
            strdescripcion, intprograma, strfincon)
    End Function

    Public Function encabezado_coberturas_final(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, _
    ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable
        Return cnegocio.encabezado_coberturas_final(strestatus, intversion, intbid, intcontrato, intaseguradora, intpaquete)
    End Function

    Public Function encabezado_coberturas(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intcotiza As Integer, ByVal intbid As Integer, _
    ByVal intprograma As Integer, ByVal strfincon As String, ByVal CadenaPaquete As String) As DataTable
        Return cnegocio.encabezado_coberturas(strestatus, intpaquete, intversion, intcotiza, intbid, intprograma, strfincon, CadenaPaquete)
    End Function

    Public Function consulta_region_aseguradora(ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal intbid As Integer) As DataTable
        Return cnegocio.consulta_region_aseguradora(intsubramo, intaseguradora, intbid)
    End Function

    Public Function determinando_deducpl(ByVal intaseguradora As Integer, ByVal intpaquete As String, _
    ByVal intsubramo As Integer, ByVal intprograma As Integer, ByVal strestatus As String, _
    ByVal intban As Integer, ByVal intanio As Integer, _
    ByVal intvehiculo As Integer, ByVal pcontrol As Double, ByVal tipopol As String) As Double

        Dim retur As Double = 0
        Dim dudc As Double = 0
        Dim dt As DataTable
        'nuevo
        If UCase(strestatus) = "N" Then
            dt = cnegocio.determinando_deducpl(intaseguradora, intpaquete, intsubramo, intprograma)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("deduc_pl") Then
                    dudc = dt.Rows(0)("deduc_pl")
                End If
            End If
            'If tipopol = "M" Or tipopol = "F" Then
            '    retur = Math.Round(dudc * 100, 2)
            'Else
            '    retur = pcontrol * dudc
            'End If
            retur = Math.Round(dudc * 100, 2)
        Else
            If intban = 1 Then
                dt = cnegocio.carga_vehiculo_homologado(intvehiculo, intprograma, intanio, strestatus)
            Else
                dt = carga_vehiculo_general(intvehiculo, intanio, intaseguradora, intprograma)
            End If
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("deducpl_vehiculo") Then
                    retur = dt.Rows(0)("deducpl_vehiculo")
                End If
            End If
        End If
        Return retur
    End Function

    Public Function determinando_deducpl_panel(ByVal intaseguradora As Integer, ByVal intpaquete As Integer, _
    ByVal strestatus As String, ByVal strcatalogo As String, ByVal intanio As Integer, ByVal intvehiculo As Integer) As Double
        Dim retur As Double = 0
        Dim dudc As Double = 0
        Dim precontrol As Double = 0
        Dim dt As DataTable
        'nuevo
        If UCase(strestatus) = "N" Then
            dt = cnegocio.determinando_deducpl_paquete_panel(intaseguradora, intpaquete)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("deduc_pl") Then
                    dudc = dt.Rows(0)("deduc_pl")
                End If
            End If
            dt = cnegocio.determinando_precontrol_panel(intaseguradora, strcatalogo, intanio)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("precontrol") Then
                    precontrol = dt.Rows(0)("precontrol")
                End If
            End If
            retur = precontrol * dudc
        Else
            dt = cnegocio.determinando_deduc_vehiculo_panel(intaseguradora, intanio, intvehiculo, strestatus)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("deducpl_vehiculo") Then
                    retur = dt.Rows(0)("deducpl_vehiculo")
                End If
            End If
        End If
        Return retur
    End Function

    Public Function determina_banderas_X_programa(ByVal intprograma As Integer, ByVal intaseguradora As Integer, ByVal tipo As Integer) As Integer
        '0 = ban_deduc
        '1 = ban_precontrol
        '2 = ban_rate
        '3 = ban_constante 
        '4 = ban_tipopoliza
        '5 = multiple
        '6 = Ace
        '7 = Iva 
        '13 = 1=precio tecleado,2=calculo
        '14 = numero de factura
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull(tipo) Then
                valor = dt.Rows(0)(tipo)
            End If
        End If
        Return valor
    End Function

    Public Function traer_cobertura_DV(ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal nompaquete As String) As DataTable
        Return cnegocio.traer_cobertura_DV(intcontrato, intbid, intsubramo, nompaquete)
    End Function

    Public Function recarga_valore_cotizacion_Vida_desempleo(ByVal intprograma As Integer, ByVal intcotiza As Integer) As DataTable
        Return cnegocio.recarga_valore_cotizacion_Vida_desempleo(intprograma, intcotiza)
    End Function

    Public Function valida_vida_desempleo(ByVal intcotiza As Integer, ByVal intpaquete As Integer) As DataTable
        Return cnegocio.valida_vida_desempleo(intcotiza, intpaquete)
    End Function

    Public Function carga_reporte_poliza_desempleo(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_reporte_poliza_desempleo(intpoliza, intbid, intaseguradora)
    End Function

    Public Function carga_reporte_poliza_vida(ByVal intpoliza As Integer, ByVal intaseguradora As Integer) As DataTable
        Return cnegocio.carga_reporte_poliza_vida(intpoliza, intaseguradora)
    End Function

    Public Function busqueda_iva(ByVal striva As String) As DataTable
        Return cnegocio.busqueda_iva(striva)
    End Function

    Public Sub actualiza_cobertura_iva(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strcadena As String)
        cnegocio.actualiza_cobertura_iva(intcontrato, intbid, strcadena)
    End Sub

    Public Function Carga_Beneficiarios(ByVal intprograma As Integer) As DataTable
        Return cnegocio.Carga_Beneficiarios(intprograma)
    End Function

    Public Function determina_bid_consultas(ByVal intusuario As Integer, ByVal intprograma As Integer, ByVal intmarca As Integer) As DataTable
        Return cnegocio.determina_bid_consultas(intusuario, intprograma, intmarca)
    End Function

    Public Function valida_periodo_fnacimiento(ByVal strfecha As String, ByVal intbandera As Integer) As Integer
        Dim bandera As Integer = 0
        Dim FechaAct() As String
        Dim DiaFA As Integer = 0
        Dim MesFA As Integer = 0
        Dim FechaNac() As String
        Dim DiaFN As Integer = 0
        Dim MesFN As Integer = 0

        FechaAct = Split(Mid(Now(), 1, 10), "/")
        FechaNac = Split(strfecha, "/")

        DiaFA = FechaAct(0)
        MesFA = FechaAct(1)

        DiaFN = FechaNac(0)
        MesFN = FechaNac(1)

        Dim dt As DataTable = cnegocio.valida_periodo_fnacimiento(strfecha, intbandera)
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) <= 18 Then
                If MesFN = MesFA Then
                    If DiaFN > DiaFA Then
                        bandera = 1
                    End If
                ElseIf MesFN > MesFA Then
                    bandera = 1
                End If
            Else
                If dt.Rows(0)(0) >= 69 Then
                    If MesFN >= MesFA - 1 Then
                        If DiaFN > DiaFA Then
                            bandera = 2
                        End If
                    End If
                End If
            End If
        End If
        Return bandera
    End Function

    Public Function valida_noserie(ByVal strserie As String) As DataTable
        Return cnegocio.valida_noserie(strserie)
    End Function

    'HM 20/02/2014 se agrega validacion por bid
    Public Function validaBidGrupoIntSerie(ByVal strContrato As String, ByVal strSerie As String) As Integer
        Dim IntSal As Integer = 0
        Dim dt As DataTable = cnegocio.validaBidGrupoIntSerie(strContrato, strSerie)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("contador") Then
                IntSal = dt.Rows(0)("contador")
            End If
        End If
        Return IntSal
    End Function

    'EVI 27/06/2010
    Public Function CreaCotizacion(ByVal intversion As Integer, ByVal intbid As Integer, ByVal intprograma As Integer, _
   ByVal intmarca As Integer, ByVal intaonh As Integer, ByVal strnuevo As String, _
   ByVal strmodelo As String, ByVal intanio As Integer, _
   ByVal strfechaContrato As String, ByVal dbprecio As Double, ByVal strtipopersona As String, ByVal IntCampana As Integer, _
   ByVal intplazo As Integer, ByVal intplazovida As Integer, ByVal strfolio As String, ByVal strtipopoliza As String, _
   ByVal strseguroD As String, ByVal strseguroV As String, ByVal strsubramo As String, ByVal numContCobertura As Long, _
    ByVal intcliente As Integer, ByVal intpaquete As Integer, ByVal strfechaVida As String, ByVal intuso As Integer, _
    ByVal dbenganche As Double, ByVal strcadenavida As String, ByVal strcadenadesemp As String, _
    ByVal strcadenacob As String, ByVal strPaqSelCon As String, ByVal strcalculos As String, ByVal strsubsidios As String, _
    ByVal strArrPoliza As String, ByVal strResultado As String, ByVal strMultArrPolisa As String, ByVal dbiva As Double, _
    ByVal strcontrato As String) As Long
        Return cnegocio.CreaCotizacion(intversion, intbid, intprograma, intmarca, intaonh, strnuevo, _
        strmodelo, intanio, strfechaContrato, dbprecio, strtipopersona, IntCampana, _
        intplazo, intplazovida, strfolio, strtipopoliza, strseguroD, strseguroV, strsubramo, _
        numContCobertura, intcliente, intpaquete, strfechaVida, intuso, dbenganche, _
        strcadenavida, strcadenadesemp, strcadenacob, strPaqSelCon, strcalculos, _
        strsubsidios, strArrPoliza, strResultado, strMultArrPolisa, dbiva, strcontrato)
    End Function

    Public Function BuscaCotizacion(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable
        Return cnegocio.BuscaCotizacion(intbid, intprograma, StrCadena, StrCriterios)
    End Function

    Public Function recarga_cotizacion(ByVal idcotizacion As Long) As DataTable
        Return cnegocio.recarga_cotizacion(idcotizacion)
    End Function

    Public Function Carga_tipoPago(ByVal iPrograma As Integer) As DataTable
        Return cnegocio.Carga_tipoPago(iPrograma)
    End Function

    Public Function Carga_FactorTipoPago(ByVal idtipopago As Integer) As DataTable
        Return cnegocio.Carga_FactorTipoPago(idtipopago)
    End Function

    Public Function carga_Contenedor_reporte(ByVal strpoliza As String, ByVal intbid As Integer) As Long
        Dim IntSalida As Long = 0
        Dim dt As DataTable = cnegocio.carga_Contenedor_reporte(strpoliza, intbid)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("cve_reporte") Then
                IntSalida = dt.Rows(0)("cve_reporte")
            End If
        End If
        Return IntSalida
    End Function

#Region "Coberturas de inicio"
    Public Function Carga_aseguradora_cobertura_inicio(ByVal intbid As Integer, _
    ByVal intcontrato As Integer, ByVal intversion As Integer, ByVal strestatus As String) As DataTable
        Return cnegocio.Carga_aseguradora_cobertura_inicio(intbid, intcontrato, _
            intversion, strestatus)
    End Function

    Public Function cuenta_paquete_aseguradora_subramo_inicio(ByVal intbid As Integer, ByVal intcontrato As Integer) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.cuenta_paquete_aseguradora_subramo_inicio(intbid, intcontrato)
        If dt.Rows.Count > 0 Then
            valor = dt.Rows(0)(0)
        End If
        Return valor
    End Function

    Public Function paquete_aseguradora_subramo_inicio(ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer, ByVal intaon As Integer, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal banunico As Integer, ByVal cadenapaquete As String) As DataTable
        Return cnegocio.paquete_aseguradora_subramo_inicio(intsubramo, strestatus, intpaquete, intprograma, intaon, _
        intcontrato, intbid, banunico, cadenapaquete)
    End Function

    Public Function encabezado_coberturas_inicio(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, _
    ByVal strpaquete As String, ByVal Idtipocarga As Integer) As DataTable
        Return cnegocio.encabezado_coberturas_inicio(strestatus, intpaquete, intversion, intbid, intcontrato, strpaquete, Idtipocarga)
    End Function

    Public Function encabezado_coberturas_inicio_homologada(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, ByVal strpaquete As String) As DataTable
        Return cnegocio.encabezado_coberturas_inicio_homologada(strestatus, intpaquete, intversion, intbid, intcontrato, strpaquete)
    End Function

    Public Function encabezado_coberturas_final_inicio(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, _
    ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable
        Return cnegocio.encabezado_coberturas_final_inicio(strestatus, intversion, intbid, intcontrato, intaseguradora, intpaquete)
    End Function

    Public Function carga_coberturas_inicio(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String, ByVal intcontrato As Integer) As DataTable
        If cnegocio.ValidaCoberturasExtras(intaseguradora, strcoberturadescripcion).Rows.Count = 0 Then
            Return cnegocio.carga_coberturas_inicio(strestatus, intversion, _
                intpaquete, intbid, intcotiza, intaseguradora, strcoberturadescripcion, _
                strdescripcion, intprograma, strfincon)
        Else
            Return cnegocio.Carga_MontosCoberturasExtras(intcontrato, intbid, _
                    strcoberturadescripcion, intaseguradora)
        End If
    End Function

    Public Function carga_coberturas_inicio_homologada(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String, ByVal claveaon As Integer) As DataTable
        Dim dt As DataTable = cnegocio.Bandera_precio_danos(intaseguradora, intprograma)
        If dt.Rows.Count > 0 Then
            banfactura = dt.Rows(0)("ban_factura")
        End If
        Return cnegocio.carga_coberturas_inicio_homologada(strestatus, intversion, _
            intpaquete, intbid, intcotiza, intaseguradora, strcoberturadescripcion, _
            strdescripcion, intprograma, strfincon, claveaon)
    End Function
#End Region



#Region "Guardando Cotizacion"
    Public Sub inserta_poliza_cotizacion(ByVal intbid As Integer, _
    ByVal strtipopol As String, ByVal intseguro As Integer, _
    ByVal NumPlazoPoliza As Integer, ByVal fechaini As String, _
    ByVal strdeeler As String, ByVal strmoneda As String, ByVal intaonH As Integer, _
    ByVal arrpoliza() As String, ByVal strserie As String, ByVal intuso As Integer, _
    ByVal strcobertura As String, ByVal strmotor As String, _
    ByVal strcontrato As String, ByVal strprimaneta As String, _
    ByVal strderpol As String, ByVal vlivaseguro As String, ByVal strprimatotal As String, _
    ByVal bunidadfinanciada As String, ByVal strzurich As String, ByVal strcolor As String, _
    ByVal strplazo As String, ByVal arrsub() As String, ByVal strprecio_ctrl As String, _
    ByVal strprecio As String, ByVal intversion As Integer, ByVal strestatus As String, _
    ByVal strventa As String, _
    ByVal intcliente As Integer, ByVal intregion As Integer, _
    ByVal intcontratopol As Integer, ByVal tcontrato As Integer, _
    ByVal intbanf As Integer, ByVal fechavalor As String, _
    ByVal intusuario As Integer, ByVal strrenave As String, _
    ByVal intaseguradora As Integer, ByVal stranio As String, _
    ByVal intsubramo As Integer, ByVal strmodelo As String, _
    ByVal intpaquete As Integer, ByVal intprograma As Integer, _
    ByVal strfincon As String, ByVal strbansegvida As Integer, _
    ByVal bansegdesempleo As Integer, ByVal strfactura As String, _
    ByVal dbiva As Double, ByVal TipoFinanciamiento As String, _
    ByVal intcotizacionPoliza As Integer, _
    Optional ByVal Strfracciones As String = "", _
    Optional ByVal strvida As String = "", _
    Optional ByVal strdesempleo As String = "", _
    Optional ByVal MulFraccion As String = "")

        Dim strinsco As String = ""
        Dim stropcsa As String = ""
        '----
        Dim contador_M As Integer
        Dim NumPolizaCredito As Integer
        Dim poliza As Long = 0
        Dim paqueteA As Integer = 0
        Dim opcsa As String = ""
        Dim bantipopol As Integer = 0
        Dim bandeduc As Integer = 0
        Dim polizaAce As Long = 0
        Dim banAce As String = 0
        Dim BanEstatusParametro As Integer = 0
        Dim dbRecargo As Double = 0
        Dim BanRecargo As Integer = 0
        Dim BanFactor As Integer = 0


        Dim dtP As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
        If dtP.Rows.Count > 0 Then
            If Not dtP.Rows(0).IsNull("ban_tipopoliza") Then
                bantipopol = dtP.Rows(0)("ban_tipopoliza")
            End If
            If Not dtP.Rows(0).IsNull("ban_deduc") Then
                bandeduc = dtP.Rows(0)("ban_deduc")
            End If
            If Not dtP.Rows(0).IsNull("ban_ace") Then
                banAce = dtP.Rows(0)("ban_ace")
            End If
            If Not dtP.Rows(0).IsNull("ban_estatus_parametro") Then
                BanEstatusParametro = dtP.Rows(0)("ban_estatus_parametro")
            End If
            If Not dtP.Rows(0).IsNull("ban_recargo") Then
                BanRecargo = dtP.Rows(0)("ban_recargo")
            End If
            If Not dtP.Rows(0).IsNull("ban_factores") Then
                BanFactor = dtP.Rows(0)("ban_factores")
            End If
            If BanRecargo = 1 And strtipopol = "F" Then
                If Not dtP.Rows(0).IsNull("recargo") Then
                    dbRecargo = dtP.Rows(0)("recargo")
                    dbRecargo = dbRecargo / 100
                End If
            End If
        End If

        poliza = 0
        polizaAce = 0

        If strtipopol = "A" Or strtipopol = "F" Then
            contador_M = 0
        Else
            If intseguro = 0 Then
                contador_M = 0
            Else
                contador_M = 4
                'si es multianual se aplica esta
                If TipoFinanciamiento = "MO" Then
                    'financiado MO 25 y 37
                    NumPolizaCredito = Math.Round((NumPlazoPoliza / 12), 0)
                Else
                    'financiado WOR todos
                    NumPolizaCredito = Math.Round(((NumPlazoPoliza / 12) + 0.416666666676), 0)
                End If
                contador_M = NumPolizaCredito - 1
            End If
        End If

        Dim dtI As DataTable = cnegocio.carga_insco_homologado_inserta(intaonH, stranio, intaseguradora, strestatus, intsubramo)
        If dtI.Rows.Count > 0 Then
            If Not dtI.Rows(0).IsNull("insco") Then
                strinsco = dtI.Rows(0)("insco")
            End If
        End If

        Dim dtPaquete As DataTable = cnegocio.paquete_aseguradora(intsubramo, intaseguradora, intpaquete)
        If dtP.Rows.Count > 0 Then
            'subramoA = dtp.Rows(0)("subramo")
            paqueteA = dtPaquete.Rows(0)("paquete")

            'intpaquete = dtp.Rows(0)("id_paquete")
            If dtPaquete.Rows(0).IsNull("opcsa_paquete") Then
                stropcsa = ""
            Else
                stropcsa = dtPaquete.Rows(0)("opcsa_paquete")
            End If
        End If

        Dim dt As DataTable
        If strestatus = "N" Then
            dt = cnegocio.tarifac(strmoneda, intsubramo, intpaquete, _
                stranio, strmodelo, intregion, 1, _
                strtipopol, intaseguradora, stropcsa)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("opcsa_tarifac") Then
                    stropcsa = dt.Rows(0)("opcsa_tarifac")
                End If
            End If
        Else
            dt = cnegocio.tariprima(strmoneda, intsubramo, intpaquete, _
                stranio, strinsco, intregion, 1, _
                strtipopol, intaseguradora)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("opcsa_tariprima") Then
                    stropcsa = dt.Rows(0)("opcsa_tariprima")
                End If
            End If
        End If

        cnegocio.inserta_poliza_cotizacion(intbid, strtipopol, intseguro, contador_M, fechaini, _
        NumPlazoPoliza, intpaquete, intsubramo, stropcsa, strdeeler, strmoneda, intaonH, _
        arrpoliza, strserie, intuso, strcobertura, strmotor, _
        strcontrato, strprimaneta, strderpol, vlivaseguro, strprimatotal, _
        bunidadfinanciada, strzurich, strcolor, strplazo, arrsub, strprecio_ctrl, strprecio, _
        intversion, strestatus, intpaquete, poliza, strventa, _
        intcliente, intregion, intcontratopol, tcontrato, intbanf, fechavalor, _
        intusuario, strrenave, intaseguradora, stranio, bandeduc, intprograma, _
        strfincon, polizaAce, strbansegvida, bansegdesempleo, paqueteA, _
        strfactura, dbiva, TipoFinanciamiento, Strfracciones, dbRecargo, _
        BanRecargo, BanFactor, intcotizacionPoliza, strvida, strdesempleo, _
        MulFraccion)
    End Sub

#End Region


    Public Function FechaFinalFracciones(ByVal strfecha As String, ByVal intplazo As Integer) As String
        Dim strsalida As String = ""
        Dim dt As DataTable = cnegocio.FechaFinalFracciones(strfecha, intplazo)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("fecha") Then
                strsalida = dt.Rows(0)("fecha")
            End If
        End If
        Return strsalida
    End Function

    Public Function carga_tipocarga(ByVal idprograma As Integer, ByVal strmodelo As String) As DataTable
        Return cnegocio.carga_tipocarga(idprograma, strmodelo)
    End Function

    Public Sub DatosAseguradora(ByVal IdAseguradora As Integer)
        Dim dt As DataTable = cnegocio.DatosAseguradora(IdAseguradora)
        DescrAseg = ""
        UrlImagAseg = ""
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("descripcion_aseguradora") Then
                DescrAseg = dt.Rows(0)("descripcion_aseguradora")
            End If
            If Not dt.Rows(0).IsNull("url_imagenpeque") Then
                UrlImagAseg = dt.Rows(0)("url_imagenpeque")
            End If
        End If
    End Sub

    Public Function ValidaExclusion(ByVal IdPrograma As Integer, ByVal strmodelo As String, ByVal intanio As Integer, ByVal strestatus As String) As DataTable
        Return cnegocio.ValidaExclusion(IdPrograma, strmodelo, intanio, strestatus)
    End Function

    'evi 12/12/2013 se agrega funciones de recotizacion
    Public Function recarga_marca(ByVal intmarca As Integer, ByVal intprograma As Integer, ByVal strestatus As String, ByVal IdSubMarca As Integer) As DataTable
        Return cnegocio.recarga_marca(intmarca, intprograma, strestatus, IdSubMarca)
    End Function

    Public Function Recarga_modelo(ByVal intmarca As Integer, ByVal Strestatusauto As String, ByVal intprograma As Integer, ByVal intmarcabase As Integer, ByVal StrModelo As String) As DataTable
        Return cnegocio.Recarga_modelo(intmarca, Strestatusauto, intprograma, intmarcabase, StrModelo)
    End Function

    Public Function Recarga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAnio As Integer) As DataTable
        Return cnegocio.Recarga_anio(intmarca, strmodelo, StrEstatusAuto, intprograma, intmarcaoriginal, IntAnio)
    End Function

    Public Function Recarga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAonh As Integer) As DataTable
        Return cnegocio.Recarga_descripcion(intmarca, strmodelo, stranio, StrEstatusAuto, intprograma, intmarcaoriginal, IntAonh)
    End Function

    Public Function Recarga_Catalogo(ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAonh As Integer) As DataTable
        Return cnegocio.Recarga_Catalogo(StrEstatusAuto, intprograma, intmarca, intmarcaoriginal, IntAonh)
    End Function

    Public Function recargaDatos12meses(ByVal IdCotizacionUnica As Int16, ByVal bandTipo As String) As DataTable
        Return cnegocio.recargaDatos12meses(IdCotizacionUnica, bandTipo)
    End Function

    Public Function ReCargaCotizaPanelAseguradora(ByVal IdCotizacionUnica As Integer) As DataTable
        Return cnegocio.ReCargaCotizaPanelAseguradora(IdCotizacionUnica)
    End Function

    Public Function ReCargaCotizaPanel(ByVal IdCotizacionUnica As Integer, ByVal IdAseguradora As Integer) As DataTable
        Return cnegocio.ReCargaCotizaPanel(IdCotizacionUnica, IdAseguradora)
    End Function

    Public Function Recarga12MesesAseg(ByVal IdCotizacionUnica As Integer) As DataTable
        Return cnegocio.Recarga12MesesAseg(IdCotizacionUnica)
    End Function

    'HM 13/02/2014 Valida inf de cotizacion unica
    Public Function ValidandoInfInicial(ByVal IdCotizacionUnica As Integer) As Integer
        Dim StrCadena As String = ""
        Dim Ban As Integer = 0
        Try
            Dim dt As DataTable = cnegocio.ValidandoInfInicial(IdCotizacionUnica)
            If dt.Rows.Count > 0 Then
                Ban = 1
                If Not dt.Rows(0).IsNull("grupointegrante") Then
                    StrCadena = dt.Rows(0)("grupointegrante")
                    StrGrupo = StrCadena.Substring(0, 4)
                    StrIntegrante = StrCadena.Substring(4, (StrCadena.Length - StrGrupo.Length))
                End If
                If Not dt.Rows(0).IsNull("noserie") Then
                    StrSerie = dt.Rows(0)("noserie")
                End If
            End If
            Return Ban
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtiene_datos_administrador(ByVal intPrograma As Integer) As DataTable
        Return cnegocio.obtiene_datos_administrador(intPrograma)
    End Function
End Class
