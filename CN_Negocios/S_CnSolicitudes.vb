Public Class S_CnSolicitudes
    Public Function ObtenerRequisitos(ByVal dt As DataTable, ByVal ts As String) As String
        Dim html As String = String.Empty
        Try
            Select Case ts
                Case "Aplicaci�n de pagos de contado"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "<div style=padding-left:20px;>� En campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.</div><br/><br/>" & _
                        "Documentos a Anexar:" & _
                        "<div style=padding-left:20px;>1. Comprobante de Pago efectuado.</div>"
                Case "Cancelaci�n por PT (Perdida Total)"
                    html += "En el campo &quot;Descripci�n tipo de solicitud&quot; especifique el motivo de la cancelaci�n.<br/>" & _
                    "En el campo &quot;P�liza&quot; escriba el n�mero completo de la p�liza a la que aplica esta solicitud.<br/>" & _
                    "En el campo &quot;Observaciones&quot; indique fecha de cancelaci�n, esta no puede ser retroactiva a la fecha actual.<br/>" & _
                    "&nbsp;&nbsp;&nbsp;- Anexar la &quot;Fecha de Cancelaci�n&quot; de la p�liza.<br/>"
                Case "Cancelaci�n por P�rdida total"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                    "� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.<br/>" & _
                    "Documentos a Anexar:<br/>" & _
                    "<div style=padding-left:20px;>1. Carta Finiquito expedida por la Aseguradora.</div>" & _
                    "<div style=padding-left:20px;>2. Caratula de estado de cuenta a nombre de titular, de la cuenta donde se depositar� la devoluci�n.</div>" & _
                    "<div style=padding-left:20px;>3. Identificaci�n Oficial del Asegurado por ambos lados.</div>" & _
                    "<div style=padding-left:20px;>4. Comprobante de Domicilio del Asegurado no mayor a 3 meses.</div>"
                Case "Cancelaci�n con devoluci�n de primas"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                    "<div style=padding-left:20px;>� En el campo &quot;Descripci�n tipo de solicitud&quot; especifique el motivo de la cancelaci�n.</div><br/><br/>" & _
                    "<div style=padding-left:20px;>� En el campo &quot;P�liza&quot; escriba el n�mero completo de la p�liza a la que aplica esta solicitud.</div><br/><br/>" & _
                    "<div style=padding-left:20px;>� En el campo &quot;Observaciones&quot; indique fecha de cancelaci�n, esta no puede ser retroactiva a la fecha actual.</div><br/><br/>" & _
                    "Documentos a Anexar:<br/><br/>" & _
                    "<div style=padding-left:20px;>1. Carta firmada por el cliente especificando motivo de cancelaci�n</div>" & _
                    "<div style=padding-left:20px;>2. Caratula de estado de cuenta a nombre de titular, de la cuenta donde se depositar� la devoluci�n.</div>" & _
                    "<div style=padding-left:20px;>3. Identificaci�n Oficial del Asegurado por ambos lados.</div>" & _
                    "<div style=padding-left:20px;>4. Comprobante de Domicilio del asegurado no mayor a 3 meses.</div>" & _
                    "<div style=padding-left:20px;>5. Formato de Identificaci�n de Cliente llenado y firmado, en caso de que la Aseguradora sea Qu�litas o Chubb:<br/>" & _
                    "<div style=padding-left:25px;>- <a href=""..\Plantillas\FormatoQLS.pdf"" target=""_blank"" download><font color=""white""><u>FORMATO PARA DESCARGA QU�LITAS</font></u></a></div>" & _
                    "<div style=padding-left:25px;>- <a href=""..\Plantillas\FormatoCHUBB.zip"" download target=""_blank"" download><font color=""white""><u>FORMATO PARA DESCARGA CHUBB</u></font></a></div>"
                Case "Cotizaci�n"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Descripci�n tipo de solicitud&quot;: Ingresar los siguientes Datos:<br/><br/>" & _
                        "<div style=padding-left:20px;>1. Marca.</div>" & _
                        "<div style=padding-left:20px;>2. Tipo / Subtipo.</div>" & _
                        "<div style=padding-left:20px;>3. Descripci�n de la unidad.</div>" & _
                        "<div style=padding-left:20px;>4. Modelo.</div>" & _
                        "<div style=padding-left:20px;>5. Valor de la Unidad.</div>" & _
                        "<div style=padding-left:20px;>6. Tipo de Programa (Financiado o Contado).</div>" & _
                        "<div style=padding-left:20px;>7. Vigencia de la p�liza.</div>" & _
                        "<div style=padding-left:20px;>8. Domicilio con el que se emitir� la p�liza.</div><br/>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Aseguradora&quot;: Seleccionar la Aseguradora de su elecci�n</div><br/>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Observaciones&quot;: En caso de requerir la cotizaci�n de unidad con todas las opciones de Aseguradoras que se tienen, mencionarlo en �ste campo</div><br/>" & _
                        "Documentos a Anexar:<br/><br/>" & _
                        "<div style=padding-left:20px;><b>1. Factura de Remisi�n o Ficha T�cnica de la unidad.</b></div>"
                Case "Emisi�n"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar los siguientes datos:</div><br/>" & _
                        "<div style=padding-left:20px;>1. Nombre del Asegurado.</div>" & _
                        "<div style=padding-left:20px;>2. Beneficiario Preferente.</div>" & _
                        "<div style=padding-left:20px;>3. Domicilio completo.</div>" & _
                        "<div style=padding-left:20px;>4. RFC del Asegurado.</div>" & _
                        "<div style=padding-left:20px;>5. Vigencia.</div><br/>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Serie&quot;: Ingresar serie de la unidad.</div>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Nombre de Cliente&quot;: Indicar nombre de Titular de P�liza.</div>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Aseguradora&quot;: Seleccionar Aseguradora para emisi�n.</div>" & _
                        "<div style=padding-left:10px;>� En el campo &quot;Observaciones&quot;: Mencionar motivo de emisi�n de p�liza manual.</div>" & _
                        "Documentos a Anexar:<br/><br/>" & _
                        "<div style=padding-left:20px;><b>1. Cotizaci�n aceptada por el cliente.</b></div>" & _
                        "<div style=padding-left:20px;><b>2. Factura de la unidad.</b></div>" & _
                        "<div style=padding-left:20px;><b>3. Comprobante de Domicilio no mayor a 3 meses.</b></div>"
                Case "Endosos-Asegurar una adaptaci�n a la unidad"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar la adaptaci�n que desea asegurar.<br/><br/>" & _
                        "� En el campo &quot;P�liza&quot; escriba el n�mero completo de la p�liza a la que aplica esta solicitud.<br/><br/>" & _
                        "� Documentos a Anexar:<br/><br/>" & _
                        "<div style=padding-left:20px;><b>1. Factura de la Adaptaci�n.</b></div>"
                Case "Reporte de errores en cotizador-emisor AON"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "� En el campo &quot;Descripci�n tipo de solicitud&quot;: Describir el error que desea reportar.<br/><br/>" & _
                         "� Documentos a Anexar:<br/><br/>" & _
                        "<div style=padding-left:20px;><b>1. Adjuntar pantalla de error que desea reportar.</b></div>"
                Case "Modificar la Descripci�n de la Unidad"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar la descripci�n correcta de la unidad (este cambio puede causar aumento o disminuci�n de prima, en tal caso le ser� informado antes de realizar la modificaci�n).<br/><br/>" & _
                        "� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.<br/><br/>" & _
                        "� Documentos a Anexar:<br/><br/>" & _
                        "<div style=padding-left:20px;><b>1. Factura de la Unidad.</b></div>"
                Case "Endosos-Cambio de datos de la p�liza"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                         "� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar el motivo de la solicitud e indicar los datos a modificar, tales como:<br/>" & _
                        "<div style=padding-left:20px;><b>1. Correcci�n de Domicilio: Indicar domicilio correcto.</b></div>" & _
                        "<div style=padding-left:20px;><b>2. Cambio de Conductor Habitual: Indicar nombre completo de nuevo conductor habitual.</b></div>" & _
                        "<div style=padding-left:20px;><b>3. Correcci�n de RFC: Indicar RFC correcto.</b></div>" & _
                        "<div style=padding-left:20px;><b>4. Correcci�n de A�o Modelo: Indicar A�o Modelo correcto (este cambio puede causar aumento o disminuci�n de prima, en tal caso le ser� informado antes de realizar la modificaci�n).</b></div>" & _
                        "<div style=padding-left:20px;><b>5. Correcci�n de nombre de Asegurado Titular: Indicar nombre completo correcto.</b></div>" & _
                        "<div style=padding-left:20px;><b>6. Cambio de Titular Asegurado: Indicar el nombre completo del nuevo Asegurado Titular.</b></div>" & _
                        "<div style=padding-left:20px;><b>7. Correcci�n de Beneficiario Preferente: Indicar nombre o raz�n social completa correcta.</div>" & _
                        "<div style=padding-left:20px;><b>8. Cambio de Beneficiario Preferente: Indicar nombre o raz�n social completa del nuevo Beneficiario Preferente.</div><br/><br/>" & _
                        "� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.<br/><br/>" & _
                        "Documentos a Anexar:<br/><br/>" & _
                        "� Si requiere correcci�n de domicilio:<br/><div style=padding-left:20px;><b>1. Comprobante de domicilio no mayor a 3 meses.</b></div><br/>" & _
                        "� Si requiere Cambio de Asegurado Titular o Beneficiario Preferente para Unidad de Financiamiento:<br/><div style=padding-left:20px;><b>1. Carta Finiquito de Liquidaci�n de Financiamiento expedida y firmada por la Financiera.</br>2.	Carta solicitando el cambio mencionando datos completos del Asegurado Titular o del Beneficiario Preferente.</b></div><br/>" & _
                        "� Si requiere Cambio de Asegurado Titular o Beneficiario Preferente para Unidad de Contado:<br/><div style=padding-left:20px;><b>1. Carta firmada por el Asegurado Titular actual describiendo y autorizando el cambio e indicando los datos del nuevo titular o Beneficiario Preferente: Nombre completo, RFC, Direcci�n.<br/>2. Identificaci�n Oficial de quien firma la carta.</b></div><br/>" & _
                        "� Si requiere cambio de A�o Modelo:<br/><div style=padding-left:20px;><b>1. Factura de la unidad.</b></div>"

                Case "Endosos-Corregir No. de serie o placas"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "<div style=padding-left:20px;>� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar el n�mero de serie y/o placas actuales y el dato correcto **Para correcci�n de Serie s�lo aplica m�ximo 3 d�gitos. Si es mayor no procede endoso**.</div><br/>" & _
                        "<div style=padding-left:20px;>� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.</div><br/>" & _
                        "Documentos a Anexar:" & _
                        "<div style=padding-left:20px;>1. Factura de la Unidad.</div>"
                Case "Inclusi�n de unidad en cat�logo-modelos anteriores"
                    html += "� Por favor adjunte la ficha t�cnica o factura de la unidad."
                Case "Inclusi�n de unidad en cat�logo-tipos y subtipos semi nuevos"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                    "� El el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar el tipo o subtipo de la unidad que se desea agregar al cotizador/emisor.<br/><br/>" & _
                    "� Documentos a Anexar:" & _
                    "<div style=padding-left:20px;>1. Ficha T�cnica  o Factura de Unidad.</div>"
                Case "Modelos anteriores/actuales(Tipos y subtipos que ya existen)"
                    html += "En el campo &quot;Descripci�n tipo de solicitud&quot; especifique el motivo de la solicitud.<br/>" & _
                        "En el campo &quot;Observaciones&quot;:<br/>" & _
                        "&nbsp;&nbsp;&nbsp;- Adjunte un archivo con la imagen legible de la factura del veh�culo.<br/>" & _
                        "Puede anexar archivos en formato PDF, GIF, JPG, DOC, XLS  o PPT, si lo requiere."
                Case "Solicitud de facturas solo pagos de contado"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "� En el campo &quot;Descripci�n tipo de solicitud&quot;: Especificar los datos con los que se deben emitir la factura solicitada: Nombre completo, RFC y direcci�n completa.<br/>" & _
                        "� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.<br/>" & _
                        "� Documentos a Anexar:" & _
                        "<div style=padding-left:20px;>1. Adjuntar RFC (C�dula fiscal legible).</div>" & _
                        "<div style=padding-left:20px;>2. Por favor adjunte la confirmaci�n de aplicaci�n del pago de contado.</div>"
                Case "Tipos y subtipos nuevos(Versiones nuevas en el mercado)"
                    html += "En el campo &quot;Descripci�n tipo de solicitud&quot; especifique el motivo de la solicitud.<br/>" & _
                        "En el campo &quot;Observaciones&quot;: & _<br/>" & _
                        "&nbsp;&nbsp;&nbsp;* Suma asegurada de la unidad<br/>" & _
                        "&nbsp;&nbsp;&nbsp;- Anexar archivo con la ficha t�cnica del veh�culo.<br/>" & _
                        "Puede anexar archivos en formato PDF, GIF, JPG, DOC, XLS  o PPT, si lo requiere."
                Case "Devoluci�n por Pago Duplicado"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                    "<div style=padding-left:20px;><b>� En el campo &quot;Descripci�n tipo de solicitud&quot; Detallar motivo de solicitud de devoluci�n.</b></div><br/><br/>" & _
                    "<div style=padding-left:20px;><b>� En el campo &quot;P�liza&quot;: Indicar el n�mero de p�liza a la que aplica la solicitud.</b></div><br/><br/>" & _
                    "<div style=padding-left:20px;><b>� En el campo &quot;Observaciones&quot; Indicar fecha de comprobante de pago y monto del mismo.</b></div><br/><br/>" & _
                    "Documentos a Anexar:<br/><br/>" & _
                    "<div style=padding-left:20px;><b>1. Carta firmada por el cliente especificando motivo de solicitud de devoluci�n.</b></div>" & _
                    "<div style=padding-left:20px;><b>2. Caratula de estado de cuenta a nombre de titular, de la cuenta donde se depositar� la devoluci�n.</b></div>" & _
                    "<div style=padding-left:20px;><b>3. Identificaci�n Oficial del Asegurado por ambos lados.</b></div>" & _
                    "<div style=padding-left:20px;><b>4. Comprobante de Domicilio del Asegurado no mayor a 3 meses.</b></div>" & _
                    "<div style=padding-left:20px;><b>5. Comprobante de Pago legible.</b></div>"
                Case "Sugerencias para mejoras del programa"
                    html += "Llenado de campos para �sta solicitud:<br/><br/>" & _
                        "� En el campo &quot;Descripci�n tipo de solicitud&quot;: <br/>Especificar aportaciones para mejora  del programa con la finalidad de poder brindar un  mejor servicio.<br/><br/>"

            End Select

            html += "</ul>"
            Return html
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
