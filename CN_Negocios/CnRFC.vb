Imports CD_Datos
Public Class CnRFC
    Dim cnegocio As New CdPrincipal

    Private gint_cociente As Integer
    Private gint_total1 As Integer

    'creacion del RFC
    Public Function GeneraRFC(ByVal sApePat As String, ByVal sApeMat As String, ByVal sNombre As String, ByVal sFecNac As String) As String
        Dim sRFC As String
        Dim sHomoClave As String
        Dim sApePatDos As String

        sApePatDos = DameCV(sApePat)

        If Len(Trim(sApePatDos)) = 2 And _
            Len(Trim(sApeMat)) >= 1 And _
            Len(Trim(sNombre)) >= 1 And _
            sFecNac <> "  /  /    " Then
            sRFC = sApePatDos & _
                    Mid(sApeMat, 1, 1) & _
                    Mid(sNombre, 1, 1) & _
                    sFecNac

            sHomoClave = f_generahomoclave(sRFC, sApePat, sApeMat, sNombre)

            GeneraRFC = sApePatDos & _
                    Mid(sApeMat, 1, 1) & _
                    Mid(sNombre, 1, 1) & _
                    "-" & sFecNac & "-" & sHomoClave
        Else
            GeneraRFC = "    -      -   "
        End If
    End Function

    Private Function DameCV(ByVal sApePat As String) As String
        Dim ltam As Long
        Dim i As Long
        Dim sLetra As String
        ltam = Len(sApePat)
        For i = 2 To ltam
            sLetra = Mid(sApePat, i, 1)
            If InStr(1, "AEIOU", sLetra) Then
                DameCV = Mid(sApePat, 1, 1) & sLetra
                Exit For
            End If
        Next i
    End Function

    Private Function f_generahomoclave(ByRef str_RFC As String, ByRef str_ApPaterno As String, ByRef str_ApMaterno As String, ByRef str_nombre As String) As String
        Dim lstr_paterno As String        ' Var. almacena apellido paterno sin blancos
        Dim lstr_materno As String        ' Var. almacena apellido materno sin blancos
        Dim lstr_Nombre As String         ' Var. almacena nombre sin blancos
        Dim lsrf_c As String            ' Var. almacena el rfc tecleado sin la nomoclave y sin blancos
        Dim lstr_digito1 As String        ' Var. almacena primer digito de la homoclave
        Dim lstr_digito2 As String        ' Var. almacena segundo digito de la homoclave
        Dim lstr_digito3 As String        ' Var. almacena tercer digito de la homoclave
        Dim lstr_rfctem As String         ' Var. almacena el rfc tecleado, junto con los 2 primeros digitos de la homoclave
        Dim tamstr As String
        lstr_paterno = f_CambiaAcentos(UCase(Trim$(str_ApPaterno)))
        lstr_materno = f_CambiaAcentos(UCase(Trim$(str_ApMaterno)))
        lstr_Nombre = f_CambiaAcentos(UCase(Trim$(str_nombre)))

        lsrf_c = f_CambiaAcentos(Trim$(str_RFC))
        Call f_homonimia(lstr_paterno, lstr_materno, lstr_Nombre)
        lstr_digito1 = f_segundocar(gint_cociente)
        lstr_digito2 = f_segundocar(gint_total1)
        lstr_rfctem = Trim$(lsrf_c) & Trim$(lstr_digito1) & Trim$(lstr_digito2)
        lstr_digito3 = f_digito_ver(lstr_rfctem)
        f_generahomoclave = lstr_rfctem & Trim(lstr_digito3)
        tamstr = Len(f_generahomoclave)
        If (tamstr - 2) > 0 Then
            f_generahomoclave = Mid(f_generahomoclave, tamstr - 2, 3)
        Else
            f_generahomoclave = "XXX"
        End If
    End Function

    Private Function f_digito_ver(ByVal lstr_param As String) As String
        Dim lstr_digitover As String
        Dim lint_res As Integer
        Dim lint_auxilia As Integer
        Dim lint_Suma As Integer
        Dim lint_Cont As Integer
        lint_Suma = 0
        lint_Cont = 1
        While lint_Cont <= 12
            lint_auxilia = f_tercercar(Mid$(lstr_param, lint_Cont, 1))
            lint_Suma = lint_Suma + lint_auxilia * (14 - ((lint_Cont - 1) + 1))
            lint_Cont = lint_Cont + 1
        End While
        lint_res = lint_Suma Mod 11
        If lint_res = 0 Then
            lstr_digitover = "0"
            f_digito_ver = lstr_digitover
            Exit Function
        ElseIf lint_res = 1 Then
            lstr_digitover = "A"
            f_digito_ver = lstr_digitover
            Exit Function
        Else
            lstr_digitover = Chr(48 + (11 - lint_res))
            f_digito_ver = lstr_digitover
            Exit Function
        End If

    End Function

    Private Function f_tercercar(ByVal lstr_Caracter As String) As Integer
        Dim lint_valorasc As Integer     '! Var. almacena el codigo ascci
        Dim lint_diferencia As Integer
        lint_valorasc = f_cod_asc(lstr_Caracter)
        If lint_valorasc >= 65 And lint_valorasc <= 78 Then
            lint_diferencia = lint_valorasc - 55
            f_tercercar = lint_diferencia
            Exit Function
        End If

        If lint_valorasc >= 79 And lint_valorasc <= 90 Then
            lint_diferencia = lint_valorasc - 54
            f_tercercar = lint_diferencia
            Exit Function
        End If

        If lint_valorasc >= 48 And lint_valorasc <= 57 Then
            lint_diferencia = lint_valorasc - 48
            f_tercercar = lint_diferencia
            Exit Function
        End If

        If lint_valorasc = 38 Then
            lint_diferencia = 24
            f_tercercar = lint_diferencia
            Exit Function
        End If

        If lint_valorasc = 32 Then
            lint_diferencia = 37
            f_tercercar = lint_diferencia
            Exit Function
        Else
            lint_diferencia = 0
            f_tercercar = lint_diferencia
            Exit Function
        End If

    End Function

    Private Function f_segundocar(ByVal lint_param As Integer) As String
        Dim lstr_valor As String
        If lint_param >= 9 And lint_param <= 22 Then
            lstr_valor = Chr(lint_param + 56)
            f_segundocar = lstr_valor
            Exit Function
        Else
            If lint_param > 22 Then
                lstr_valor = Chr(lint_param + 57)
                f_segundocar = lstr_valor
                Exit Function
            Else
                If lint_param >= 0 And lint_param <= 8 Then
                    lstr_valor = Str$(lint_param + 1)
                    f_segundocar = lstr_valor
                    Exit Function
                End If
            End If
        End If
    End Function


    Private Function f_CambiaAcentos(ByVal lstr_cadena As String) As String
        Dim lstr_CadTmp As String        '!Cadena de caracteres que almacena cadena final
        Dim lint_LonCad As Integer       '!Longitud de la cadena pasada como parametro
        Dim lint_Cont As Integer         '!Contador del ciclo while
        lstr_CadTmp = ""
        lint_LonCad = 0
        lint_Cont = 1
        lint_LonCad = Len(Trim$(lstr_cadena))
        While lint_Cont < lint_LonCad + 1
            If "�" = Mid$(lstr_cadena, lint_Cont, 1) Then
                lstr_CadTmp = lstr_CadTmp & "A"
            ElseIf "�" = Mid$(lstr_cadena, lint_Cont, 1) Then
                lstr_CadTmp = lstr_CadTmp & "E"
            ElseIf "�" = Mid$(lstr_cadena, lint_Cont, 1) Then
                lstr_CadTmp = lstr_CadTmp & "I"
            ElseIf "�" = Mid$(lstr_cadena, lint_Cont, 1) Then
                lstr_CadTmp = lstr_CadTmp & "O"
            ElseIf "�" = Mid$(lstr_cadena, lint_Cont, 1) Then
                lstr_CadTmp = lstr_CadTmp & "U"
            Else
                lstr_CadTmp = lstr_CadTmp & Mid$(lstr_cadena, lint_Cont, 1)
            End If
            lint_Cont = lint_Cont + 1
        End While
        f_CambiaAcentos = lstr_CadTmp

    End Function

    Private Sub f_homonimia(ByVal lstr_parpat As String, ByVal lstr_parmat As String, ByVal lstr_parnom As String)
        Dim lstr_cadena As String
        Dim lstr_Aux As String
        Dim lstr_Cociaux As String
        Dim lstr_Tmp As String
        Dim lstr_a As String
        Dim lstr_b As String
        Dim lint_Longitud As Integer
        Dim lint_pos As Integer
        Dim lint_pos1 As Integer
        Dim lint_coci As Single
        Dim lint_Suma As Decimal
        Dim lint_x As Integer
        Dim lint_y As Integer
        lstr_cadena = "0"
        lint_Longitud = Len(Trim$(lstr_parpat))
        lint_pos = 1
        If lint_Longitud = 1 Then lint_Longitud = 2
        While lint_pos < lint_Longitud + 1                                  '(lint_Longitud + 1)
            lstr_Aux = Mid(lstr_parpat, lint_pos, 1)
            lstr_Tmp = Str(f_primercar(lstr_Aux))        'obtener el primer digito verificador con un valor
            lstr_cadena = Trim$(lstr_cadena) & Trim$(lstr_Tmp)
            lint_pos = lint_pos + 1
        End While
        lstr_cadena = Trim$(lstr_cadena) & Trim$("00")
        lint_Longitud = Len(Trim$(lstr_parmat))
        lint_pos = 1
        If lint_Longitud = 1 Then lint_Longitud = 2
        While lint_pos < lint_Longitud + 1                                    '(lint_Longitud + 1)
            lstr_Tmp = f_primercar(Mid(lstr_parmat, lint_pos, 1)) 'obtener el primer digito verificador  del ap mat con un valor
            lstr_cadena = Trim$(lstr_cadena) & lstr_Tmp
            lint_pos = lint_pos + 1
        End While
        lstr_cadena = Trim$(lstr_cadena) & Trim$("00")
        lint_Longitud = Len(Trim$(lstr_parnom))
        lint_pos = 1
        If lint_Longitud = 1 Then lint_Longitud = 2
        While lint_pos < lint_Longitud + 1                                       '(lint_Longitud + 1)
            lstr_Tmp = f_primercar(Mid$(lstr_parnom, lint_pos, 1)) 'obtener el primer digito verificador  del nombre con un valor
            lstr_cadena = Trim$(lstr_cadena) & lstr_Tmp
            lint_pos = lint_pos + 1
        End While
        lint_Suma = 0
        lint_Longitud = Len(lstr_cadena)
        lint_pos = 1
        While lint_pos < lint_Longitud
            lstr_a = Mid(lstr_cadena, lint_pos, 2)
            lint_x = Val(lstr_a)
            lstr_b = Mid(lstr_cadena, lint_pos + 1, 1)
            lint_y = Val(lstr_b)
            lint_Suma = lint_Suma + lint_x * lint_y
            lint_pos = lint_pos + 1
        End While
        lint_Suma = lint_Suma Mod 1000
        lint_coci = (lint_Suma / 34)
        If lint_coci < 1 Then lint_coci = 0
        lstr_Cociaux = Trim$(Str(lint_coci))
        gint_cociente = Val(Mid$(lstr_Cociaux, 1, 2))
        gint_total1 = lint_Suma Mod 34

    End Sub

    Private Function f_cod_asc(ByVal lstr_Caracter As String) As Integer
        Dim lint_codasc As Integer     ' Var. almacena el codigo ascci
        If lstr_Caracter = " " Then
            lint_codasc = 32
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "�" Then
            lint_codasc = 38
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "�" Then
            lint_codasc = 38
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "&" Then
            lint_codasc = 38
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "0" Then
            lint_codasc = Asc(lstr_Caracter)      'Set lncodasc = SalStrLop(lscaracter)   f_lop
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "1" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "2" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "3" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "4" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "5" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "6" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "7" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "8" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "9" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "A" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "B" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "C" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "D" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "E" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "F" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "G" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "H" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "I" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "J" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "K" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "L" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "M" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "N" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "O" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "P" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "Q" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "R" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "S" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "T" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "U" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "V" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "W" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "X" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "Y" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        ElseIf lstr_Caracter = "Z" Then
            lint_codasc = Asc(lstr_Caracter)
            f_cod_asc = (lint_codasc)
            Exit Function
        Else
            lint_codasc = 20
            f_cod_asc = (lint_codasc)

        End If


    End Function

    Private Function f_primercar(ByVal lstr_Caracter As String) As Integer
        Dim lnvalorasc As Integer           'Var. almacena el codigo ascci
        Dim lint_diferencia As Integer
        lnvalorasc = f_cod_asc(lstr_Caracter)
        If lnvalorasc >= 65 And lnvalorasc <= 73 Then
            lint_diferencia = lnvalorasc - 54
            f_primercar = lint_diferencia
            Exit Function
        End If
        If lnvalorasc >= 74 And lnvalorasc <= 82 Then
            lint_diferencia = lnvalorasc - 53
            f_primercar = lint_diferencia
            Exit Function
        End If
        If lnvalorasc >= 83 And lnvalorasc <= 90 Then
            lint_diferencia = lnvalorasc - 51
            f_primercar = lint_diferencia
            Exit Function
        End If

        If lnvalorasc >= 48 And lnvalorasc <= 57 Then
            lint_diferencia = lnvalorasc - 48
            f_primercar = lint_diferencia
            Exit Function
        End If
        If lnvalorasc = 32 Then
            lint_diferencia = 0
            f_primercar = lint_diferencia
            Exit Function
        End If
        If lnvalorasc = 38 Then
            lint_diferencia = 10
            f_primercar = lint_diferencia
            Exit Function
        Else
            lint_diferencia = 0
            f_primercar = lint_diferencia
            Exit Function
        End If

    End Function
End Class
