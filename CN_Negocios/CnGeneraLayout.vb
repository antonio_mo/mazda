Imports CD_Datos
Public Class CnGeneraLayout
    Dim cnegocio As New CdGeneraLayout

    'EVI 15/12/2014
    Public Function GeneraLayout(ByVal IdPrograma As Integer, ByVal TipoLayout As Integer, ByVal Strscript As String, ByVal Strlogs As String, ByVal Baninterface As Integer) As String
        Return cnegocio.GeneraLayout(IdPrograma, TipoLayout, Strscript, Strlogs, Baninterface)
    End Function

    Public Function GetInterfaceSAARS(ByVal dtFecha As DateTime, ByVal iTipo As Integer) As String
        Return cnegocio.GetInterfaceSAARS(dtFecha, iTipo)
    End Function
End Class
