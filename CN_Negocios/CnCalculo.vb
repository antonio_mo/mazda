Imports CD_Datos
Imports System.IO
Imports System.Configuration

Public Class CnCalculo
    Dim cnegocio As New CdCalculo
    Dim ccotizador As New CnCotizador
    Public ArrPolisa(15) As String
    Public Results(11) As String
    Public subsidios(4) As String
    Public CalculoArr As String
    Public MultArrPolisa As String

    Public ArrPolisa12(15) As String
    Public Results12(11) As String
    Public subsidios12(4) As String
    Public MultArrPolisa12 As String

    Public StrSalidaPanle As String = ""
    Public StrCadenaAseg As String = ""
    Public StrSalidaPanleAseg As String = ""

    Public Function precontrol(ByVal strcatalogo As String, ByVal stranio As String, ByVal strmoneda As String, ByVal strrate As String, ByVal intaseguradora As Integer, ByVal intprograma As Integer) As DataTable
        Return cnegocio.precontrol(strcatalogo, stranio, strmoneda, strrate, intaseguradora, intprograma)
    End Function

    Public Function calculo(ByVal banderafecha As Integer, ByVal intmoneda As Integer, _
    ByVal stranio As String, ByVal strmodelo As String, _
    ByVal intplan As Integer, _
    ByVal strtipopol As String, ByVal struso As String, _
    ByVal strestatus As String, ByVal strfincon As String, _
    ByVal strrate As String, ByVal bFecIni As String, ByVal bTipoSeguro As Integer, _
    ByVal bPlazo As Integer, ByVal bValor_Factura As Double, _
    ByVal intcontrato As Integer, ByVal bSeguro As Integer, _
    ByVal intbid As Integer, ByVal Cob_Adicional As Double, _
    ByVal intaonh As Integer, ByVal intprograma As Integer, _
    ByVal fincon As String, _
    Optional ByVal TipoFinanciamiento As String = "", _
    Optional ByVal bEnganche As Double = 0, Optional ByVal strcp As String = "", _
    Optional ByVal intmarcaMulti As Integer = 0, _
    Optional ByVal intmarcaOriginalMulti As Integer = 0, _
    Optional ByVal TipoRecargo As Integer = 0, _
    Optional ByVal PlazoRecargo As Integer = 0, _
    Optional ByVal IdTipoCarga As Integer = 0, _
    Optional ByVal idtipoprograma As Integer = -1, _
    Optional ByVal banderaPromocion As Integer = 0, _
    Optional ByVal Aseg12meses As Integer = 0) As String
        'evi 14/11/2013 se agrega bandera para conauto-opcion

        'intprograma nos indica que aseguradoras se utilizan
        'nuevo
        Dim intregion As Integer = 0
        Dim stropcsa As String = ""
        '------
        Dim strinsco As String
        Dim intplazas As Integer
        Dim dt As DataTable
        Dim VarFactPrima As Double = 0
        Dim VarCosntante As Double = 0
        Dim VarF1 As Double = 0
        Dim VarF2 As Double = 0
        Dim BandSUBSIDY_FLG As Integer = 0
        Dim VarSUBSIDY_F As Double = 0
        Dim VarSUBSIDY_FC As Double = 0
        Dim VarSUBSIDY_D As Double = 0
        Dim VarSUBSIDIO_TT As Double = 0
        Dim VarSUBSIDIO_LF1 As Double = 0
        Dim VarSUBSIDIO_LFC1 As Double = 0
        Dim VarSUBSIDIO_LD1 As Double = 0
        Dim VarSUBSIDIO_LS1 As Double = 0
        Dim VarSUBSIDIO_LTT As Double = 0
        Dim NumDias As Integer = 0
        Dim Facvigencia As Double = 0
        Dim precontrol As Double = 0
        Dim PrimaBasica As Double = 0
        Dim PrimaNeta As Double = 0
        Dim DerPol As Double = 0
        Dim IvaSeguro As Double = 0
        Dim NumPolizaCredito As Double = 0
        Dim TotalDerPol As Double = 0
        'Dim PrimaTotal As String = ""
        Dim PrimaTotal As Double = 0
        Dim i As Integer = 0
        Dim k As Integer = 0
        Dim cuenta1 As Double = 0
        Dim cuenta2 As Double = 0
        Dim cuenta3 As Double = 0
        Dim cuenta4 As Double = 0
        Dim cuenta5 As Double = 0
        Dim PriamSImp As Double = 0
        Dim TemFactVig As Double = 0
        Dim PrimaNetaM As Double = 0
        Dim PrimPolTotal As Double = 0
        Dim NumPlazoPoliza As Double = 0
        Dim subramoA As Integer = 0
        Dim paqueteA As Integer = 0
        Dim intpaquete As Integer = 0
        Dim intaseguradora As Integer = 0
        Dim dbFactorRegion As Double = 0
        Dim dbFactorRegioncte As Double = 0
        Dim Strcadena As String = ""
        'banderas de programa por aseguradora
        Dim band_deduc As Integer = 0
        Dim ban_precontrol As Integer = 0
        Dim ban_rate As Integer = 0
        Dim ban_constante As Integer = 0
        Dim ban_multiple As Integer = 0
        Dim nompaquete As String = ""
        Dim retur As String = ""
        Dim CreditLife As Double = 0
        Dim intmarca As Integer = 0
        Dim ban_Ace As Integer = 0
        Dim ban_Vida As Integer = 0
        Dim ban_Iva As Integer = 0
        Dim banDobleCalculo As Integer = 0
        Dim dbiva As Double = 0.16
        Dim ban_EstatusConst As Integer = 0
        Dim ArrFraccion(0, 0) As String
        Dim salidaCantidad As String = ""
        Dim BanDesempelo As Integer = 0
        Dim z As Integer
        Dim cadenapoliza As String
        Dim PorPagoFraccion As Double = 0
        Dim RecargoPagoFracc As Double = 0
        Dim BanRecargoFraccion As Byte = 0

        Dim PorTipoCarga As Double = 1

        dbiva = 0.16

        'estas variables son para las fracciones con recargos
        Dim dbRecargo As Double = 0
        Dim PnRecargo As Double = 0
        Dim BanRecargo As Integer = 0

        'estas variables son para las fracciones con factores
        Dim BanFactor As Integer = 0
        Dim NumPolizaFactor As Integer = 0
        Dim FraccionFactor As Integer = 0
        Dim dbFactor As Double = 0
        Dim PnFactor As Double = 0

        'inicio micha
        Dim strcatalogo As String = ""
        Dim intsubramo As Integer = 0
        'fin micha

        Dim BanTipoPago As Integer = 0
        Dim dtp As DataTable

        Try

            'Dim arrFecha() As String
            'arrFecha = Split(bFecIni, "/")
            'If banderafecha = 0 Then
            '    bFecIni = arrFecha(0) & "/" & arrFecha(1) & "/" & arrFecha(2)
            'Else
            '    bFecIni = arrFecha(1) & "/" & arrFecha(0) & "/" & arrFecha(2)
            'End If

            'evi 27/06/2013 se agrega la opcion de anual
            'igualamos los valores de multianual para que tome los paquetes opcionales abc_paquete  
            'IIf(idtipoprograma = 2, 1, idtipoprograma)
            ''''Dim dtp As DataTable = cnegocio.paquete(intprograma, strestatus, intaonh, intmarcaMulti, intmarcaOriginalMulti, "", IIf(idtipoprograma < 5, 0, idtipoprograma))
            'evi 14/11/2013 se agrega bandera para conauto-opcion
            'Dim dtp As DataTable = cnegocio.paquete(intprograma, strestatus, intaonh, intmarcaMulti, intmarcaOriginalMulti, "", IIf(idtipoprograma < 5, 0, 1))
            If intprograma = 33 Then
                dtp = cnegocio.paquete(intprograma, strestatus, intaonh, intmarcaMulti, intmarcaOriginalMulti, "", IIf(banderaPromocion = 0, 0, 1))
            ElseIf intprograma = 30 Then
                dtp = cnegocio.paquete(intprograma, strestatus, intaonh, intmarcaMulti, intmarcaOriginalMulti, "", IIf(Aseg12meses = 62 And idtipoprograma <= 3, 2, IIf(idtipoprograma < 5, 0, 1)))
            Else
                dtp = cnegocio.paquete(intprograma, strestatus, intaonh, intmarcaMulti, intmarcaOriginalMulti, "", IIf(Aseg12meses = 64 And idtipoprograma <= 3, 2, IIf(idtipoprograma < 5, 0, 1)))
            End If

            If dtp.Rows.Count > 0 Then
                cnegocio.actualiza_cobertura_aCero(intcontrato, intbid)
                For k = 0 To dtp.Rows.Count - 1
regresa:
                    If banderaPromocion = 1 Then
                        strmodelo = IIf(Carga_AbcPromocion(dtp.Rows(k)("Id_Aseguradora"), strmodelo) = "", strmodelo, Carga_AbcPromocion(dtp.Rows(k)("Id_Aseguradora"), strmodelo))
                    End If

                    subramoA = dtp.Rows(k)("subramo")
                    paqueteA = dtp.Rows(k)("paquete")
                    intpaquete = dtp.Rows(k)("id_paquete")
                    intaseguradora = dtp.Rows(k)("Id_Aseguradora")
                    nompaquete = dtp.Rows(k)("descripcion_paquete")
                    'inicio micha
                    If Not dtp.Rows(k).IsNull("catalogo_vehiculo") Then
                        strcatalogo = dtp.Rows(k)("catalogo_vehiculo")
                    Else
                        strcatalogo = ""
                    End If
                    If Not dtp.Rows(k).IsNull("subramo_vehiculo") Then
                        intsubramo = dtp.Rows(k)("subramo_vehiculo")
                    Else
                        intsubramo = 0
                    End If

                    'fin micha

                    If dtp.Rows(k).IsNull("opcsa_paquete") Then
                        stropcsa = ""
                    Else
                        stropcsa = dtp.Rows(k)("opcsa_paquete")
                    End If

                    Dim dtB As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
                    If dtB.Rows.Count > 0 Then
                        If Not dtB.Rows(0).IsNull("ban_deduc") Then
                            band_deduc = dtB.Rows(0)("ban_deduc")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_precontrol") Then
                            ban_precontrol = dtB.Rows(0)("ban_precontrol")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_rate") Then
                            ban_rate = dtB.Rows(0)("ban_rate")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_constante") Then
                            ban_constante = dtB.Rows(0)("ban_constante")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_Multianual") Then
                            ban_multiple = dtB.Rows(0)("Ban_Multianual")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_ace") Then
                            ban_Ace = dtB.Rows(0)("Ban_ace")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_vida") Then
                            ban_Vida = dtB.Rows(0)("Ban_vida")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_iva") Then
                            ban_Iva = dtB.Rows(0)("Ban_iva")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_estatus") Then
                            ban_EstatusConst = dtB.Rows(0)("ban_estatus")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_recargo") Then
                            BanRecargo = dtB.Rows(0)("ban_recargo")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_factores") Then
                            BanFactor = dtB.Rows(0)("ban_factores")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_RecargoFraccion") Then
                            BanRecargoFraccion = dtB.Rows(0)("Ban_RecargoFraccion")
                        End If
                        If BanRecargo = 1 And strtipopol = "F" Then
                            If Not dtB.Rows(0).IsNull("recargo") Then
                                dbRecargo = dtB.Rows(0)("recargo")
                                dbRecargo = dbRecargo / 100
                            End If
                        End If
                        If Not dtB.Rows(0).IsNull("ban_doblecalculo") Then
                            banDobleCalculo = dtB.Rows(0)("ban_doblecalculo")
                        End If
                    End If
                    dtB.Clear()
                    If ban_Iva = 1 Then
                        If Not strcp = "" Then
                            Dim dtcp As DataTable = cnegocio.determina_iva_parametro_aseguradora(strcp, intaseguradora)
                            If dtcp.Rows.Count > 0 Then
                                If Not dtcp.Rows(0).IsNull("iva") Then
                                    dbiva = dtcp.Rows(0)("iva")
                                End If
                            End If
                            dtcp.Clear()
                        End If
                    End If

                    Dim dtRegion As DataTable = cnegocio.consulta_region(subramoA, intaseguradora, intbid)
                    If dtRegion.Rows.Count > 0 Then
                        If Not dtRegion.Rows(0).IsNull("id_region") Then
                            intregion = dtRegion.Rows(0)("id_region")
                        End If
                    End If
                    dtRegion.Clear()

                    Cob_Adicional = calcula_coberturas(intcontrato, intbid, subramoA, intaseguradora, strestatus)

                    dt = cnegocio.carga_insco_homologado(intaonh, stranio, intaseguradora, strestatus)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("insco") Then
                            strinsco = dt.Rows(0)("insco")
                        End If
                        If Not dt.Rows(0).IsNull("id_marca") Then
                            intmarca = dt.Rows(0)("id_marca")
                        End If
                    Else
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                    End If
                    dt.Clear()

                    dt = Nothing
                    dt = cnegocio.datos_Vehiculo(intaseguradora, strinsco, stranio, intprograma, intmarcaMulti, intmarcaOriginalMulti)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("plaza_vehiculo") Then
                            'inicio cambio micha
                            If remplaza_vacio(dt.Rows(0)("plaza_vehiculo")) = "" Then
                                intplazas = 0
                            Else
                                intplazas = dt.Rows(0)("plaza_vehiculo")
                                'fin
                            End If
                        End If
                    End If
                    dt.Clear()

                    dt = Nothing
                    'evi 10/12/2013
                    If strestatus = "N" Or (intprograma = 29 Or intprograma = 30 Or intprograma = 31) Then
                        dt = cnegocio.tarifac(intmoneda, intsubramo, intpaquete, _
                            stranio, strmodelo, intregion, intplan, _
                            strtipopol, intaseguradora, intprograma, stropcsa)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("FACTPRIM_tarifac") Then
                                VarFactPrima = dt.Rows(0)("FACTPRIM_tarifac")
                            End If
                            If Not dt.Rows(0).IsNull("FactDm_Tarifac") Then
                                VarCosntante = dt.Rows(0)("FactDm_Tarifac")
                            End If
                            If Not dt.Rows(0).IsNull("opcsa_tarifac") Then
                                stropcsa = dt.Rows(0)("opcsa_tarifac")
                            End If
                        Else
                            k = k + 1
                            If dtp.Rows.Count = k Then
                                Exit For
                            Else
                                GoTo regresa
                            End If
                        End If
                        dt.Clear()
                    Else
                        dt = cnegocio.tariprima(intmoneda, intsubramo, intpaquete, _
                            stranio, strinsco, intregion, intplan, _
                            strtipopol, intaseguradora)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("prima_tariprima") Then
                                VarFactPrima = dt.Rows(0)("prima_tariprima")
                            End If
                            If Not dt.Rows(0).IsNull("opcsa_tariprima") Then
                                stropcsa = dt.Rows(0)("opcsa_tariprima")
                            End If
                        Else
                            k = k + 1
                            If dtp.Rows.Count = k Then
                                Exit For
                            Else
                                GoTo regresa
                            End If
                        End If
                        dt.Clear()
                    End If


                    'If Not strestatus = "N" Or Not intaseguradora = 1 Then
                    'If Not strestatus = "N" Then
                    'antes era Or
                    'emmanuell casa
                    If Not strestatus = "N" Or ban_constante = 1 Then
                        dt = Nothing
                        dt = cnegocio.constante(intmoneda, intsubramo, intplazas, _
                        stropcsa, intaseguradora, strestatus, ban_EstatusConst, _
                        intprograma, intmarca)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("CONSTANTE_constante") Then
                                VarCosntante = dt.Rows(0)("CONSTANTE_constante")
                            End If
                        End If
                        dt.Clear()

                        'inicio cambio micha
                        If VarCosntante <= 0 Then
                            k = k + 1
                            If dtp.Rows.Count = k Then
                                Exit For
                            Else
                                GoTo regresa
                            End If
                        End If
                        'fin cambio
                    End If


                    dt = Nothing
                    dt = cnegocio.uso(intsubramo, struso, intaseguradora)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("F1") Then
                            VarF1 = dt.Rows(0)("F1")
                        End If
                        If Not dt.Rows(0).IsNull("F2") Then
                            VarF2 = dt.Rows(0)("F2")
                        End If
                    Else
                        'inicio cambio micha
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                        'fin cambio
                    End If
                    dt.Clear()

                    dt = Nothing
                    dt = cnegocio.subsidio(intmoneda, strcatalogo, stranio, intregion, _
                        intplan, intpaquete, strestatus, fincon, stropcsa, intprograma, intaseguradora)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("SUBSIDY_FLG") Then
                            BandSUBSIDY_FLG = dt.Rows(0)("SUBSIDY_FLG")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_F") Then
                            VarSUBSIDY_F = dt.Rows(0)("SUBSIDY_F")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_FC") Then
                            VarSUBSIDY_FC = dt.Rows(0)("SUBSIDY_FC")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_D") Then
                            VarSUBSIDY_D = dt.Rows(0)("SUBSIDY_D")
                        End If
                        VarSUBSIDIO_TT = VarSUBSIDY_F + VarSUBSIDY_FC + VarSUBSIDY_D
                        'lealtad
                        If Not dt.Rows(0).IsNull("SUBSIDY_LF") Then
                            VarSUBSIDIO_LF1 = dt.Rows(0)("SUBSIDY_LF")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lFC") Then
                            VarSUBSIDIO_LFC1 = dt.Rows(0)("SUBSIDY_lFC")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lD") Then
                            VarSUBSIDIO_LD1 = dt.Rows(0)("SUBSIDY_lD")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lS") Then
                            VarSUBSIDIO_LS1 = dt.Rows(0)("SUBSIDY_lS")
                        End If
                        VarSUBSIDIO_LTT = VarSUBSIDIO_LF1 + VarSUBSIDIO_LFC1 + VarSUBSIDIO_LD1 + VarSUBSIDIO_LS1
                    End If
                    dt.Clear()

                    Dim bbFecIni As String
                    Dim arrfeci() As String
                    arrfeci = Split(bFecIni, "/")

                    bbFecIni = arrfeci(1) & "/" & arrfeci(0) & "/" & arrfeci(2)

                    'Dim dFechaIni As Date = Convert.ToDateTime(bFecIni)
                    Dim dFechaIni As Date = CDate(bbFecIni)

                    'NumDias = DateDiff("M", bFecIni, DateAdd("m", CDbl(IIf(bTipoSeguro = 1, Trim(Str(bPlazo)), "12")), bFecIni))
                    'NumDias = DateDiff("M", bFecIni, DateAdd("m", CDbl(bPlazo), bFecIni))


                    NumDias = DateDiff("M", dFechaIni, DateAdd("m", CDbl(bPlazo), dFechaIni))
                    If NumPlazoPoliza = 0 Then NumPlazoPoliza = 12
                    If NumPlazoPoliza <> NumDias Then NumPlazoPoliza = NumDias


                    dt = Nothing
                    'evi 14/11/2013 se agrega bandera para conauto-opcion
                    dt = cnegocio.Fvigencias(intmoneda, NumPlazoPoliza, strtipopol, intaseguradora)
                    If dt.Rows.Count > 0 Then
                        If (Aseg12meses = 63 And idtipoprograma <= 3 And intaseguradora = 62) Then
                            If Not dt.Rows(0).IsNull("FactVigCruzada") Then
                                Facvigencia = dt.Rows(0)("FactVigCruzada")
                            End If
                        Else
                            If Not dt.Rows(0).IsNull("FACTVIG") Then
                                Facvigencia = dt.Rows(0)("FACTVIG")
                            End If
                            If (intaseguradora = 58 Or intaseguradora = 61 Or intaseguradora = 63 Or intaseguradora = 65) And Not TipoRecargo = 1 Then
                                If Not dt.Rows(0).IsNull("FactVigMultianual") Then
                                    Facvigencia = dt.Rows(0)("FactVigMultianual")
                                End If
                            Else
                                If intaseguradora = 59 And banderaPromocion = 1 Then
                                    Facvigencia = dt.Rows(0)("FactVigMultianual")
                                End If
                            End If
                        End If
                    Else
                        'inicio cambio micha
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                        'fin cambio
                    End If
                    dt.Clear()

                    'Calculamos la prima basica
                    If ban_precontrol = 1 And strestatus = "N" Then
                        dt = Nothing
                        dt = cnegocio.precontrol(strcatalogo, stranio, intmoneda, IIf(ban_rate = 1, strrate, ""), intaseguradora, intprograma)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("PRECONTROL") Then
                                precontrol = dt.Rows(0)("PRECONTROL")
                            End If
                        End If
                        dt.Clear()
                        'inicio cambio micha
                        If precontrol <= 0 Then
                            k = k + 1
                            If dtp.Rows.Count = k Then
                                Exit For
                            Else
                                GoTo regresa
                            End If
                        End If
                        'fin cambio
                    End If

                    'Calculamos el factor por region
                    dt = Nothing
                    dt = cnegocio.Determina_Region_factor(intaseguradora, intbid, intregion, subramoA, strestatus)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("factor") Then
                            dbFactorRegion = dt.Rows(0)("factor")
                            'descuento sobre el factor
                        End If
                        If Not dt.Rows(0).IsNull("ctefactor") Then
                            dbFactorRegioncte = dt.Rows(0)("ctefactor")
                            'descuento sobre la constante
                        End If
                    Else
                        'esto lo puse yo hay que validarlo con ford ya que si no tiene estos valores
                        'el valor se va a derecho de poliza 17/06/209
                        'inicio cambio micha
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                        'fin cambio
                    End If
                    dt.Clear()

                    'validando que el auto realmente cumplacon la condiciones de venta y de seleccion
                    Dim dtVal As DataTable = cnegocio.valida_auto_cotizacion(intaseguradora, _
                    intmarcaOriginalMulti, intsubramo, strinsco, intaonh, strestatus, stranio, intmarca)
                    If dtVal.Rows.Count = 0 Then
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                    End If
                    dtVal.Clear()


                    If IdTipoCarga > 0 Then
                        Dim dtTC As DataTable = cnegocio.Recargos_tipocarga(intaseguradora, IdTipoCarga, strmodelo)
                        If dtTC.Rows.Count > 0 Then
                            If Not dtTC.Rows(0).IsNull("recargo_tipocarga") Then
                                PorTipoCarga = dtTC.Rows(0)("recargo_tipocarga")
                            End If
                        End If
                        dtTC.Clear()
                    End If

                    VarCosntante = VarCosntante * PorTipoCarga

                    'EVI
                    If strestatus = "N" Then
                        If ban_precontrol = 1 Then
                            If banDobleCalculo = 0 Then
                                If bValor_Factura <= precontrol Then
                                    PrimaBasica = ((precontrol * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                                Else
                                    PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                                End If
                            Else
                                If bValor_Factura <= precontrol Then
                                    PrimaBasica = ((precontrol * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                                Else
                                    PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                                End If
                            End If
                        Else
                            PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                        End If
                    Else
                        'evi 10/12/2013
                        If intprograma = 30 Or intprograma = 29 Or intprograma = 31 Then
                            PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                        Else
                            PrimaBasica = ((VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                        End If
                    End If
                    PrimaBasica = Math.Round(PrimaBasica, 2)

                    '***Calculamos la prima basica
                    If strtipopol = "F" Then
                        PrimaBasica = PrimaBasica * 1
                    Else
                        PrimaBasica = PrimaBasica * Facvigencia
                    End If
                    PrimaBasica = Math.Round(PrimaBasica, 2)

                    '***Calculamos la prima neta
                    PrimaNeta = Math.Round(PrimaBasica + Cob_Adicional, 2)

                    '***Sacamos el derecho de poliza
                    dt = Nothing
                    dt = cnegocio.tariderpol(intmoneda, intsubramo, intaseguradora, intprograma, intmarca)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("DERPOL") Then
                            DerPol = dt.Rows(0)("DERPOL")
                        End If
                    Else
                        'inicio cambio micha
                        k = k + 1
                        If dtp.Rows.Count = k Then
                            Exit For
                        Else
                            GoTo regresa
                        End If
                    End If
                    dt.Clear()

                    PorPagoFraccion = 0
                    BanTipoPago = 1
                    If intprograma = 30 Then
                        If (cnegocio.ValidaPagoFraccionadoAseguradora(intaseguradora, TipoRecargo).Rows.Count > 0) Then
                            PorPagoFraccion = cnegocio.Valor_Pago_fraccionado(intaseguradora, TipoRecargo, PlazoRecargo)
                        Else
                            BanTipoPago = 0
                        End If
                    Else
                        PorPagoFraccion = cnegocio.Valor_Pago_fraccionado(intaseguradora, TipoRecargo, PlazoRecargo)
                    End If

                    If BanRecargoFraccion = 0 Then
                        'Se Calcula antes de los derechos
                        RecargoPagoFracc = Math.Round(PrimaNeta * PorPagoFraccion, 2)
                    Else
                        'Se Calcula despues de los derechos
                        RecargoPagoFracc = Math.Round((PrimaNeta + DerPol) * PorPagoFraccion, 2)
                    End If
                    IvaSeguro = Math.Round((PrimaNeta + DerPol + RecargoPagoFracc) * dbiva, 2)
                    PrimaTotal = PrimaNeta + DerPol + RecargoPagoFracc + IvaSeguro
                    PrimPolTotal = Math.Round(PrimaTotal, 2)

                    salidaCantidad = intaseguradora
                    salidaCantidad = salidaCantidad & "|" & nompaquete
                    salidaCantidad = salidaCantidad & "|" & bValor_Factura
                    salidaCantidad = salidaCantidad & "|" & precontrol
                    salidaCantidad = salidaCantidad & "|" & VarFactPrima
                    salidaCantidad = salidaCantidad & "|" & VarCosntante
                    salidaCantidad = salidaCantidad & "|" & VarF1
                    salidaCantidad = salidaCantidad & "|" & VarF2
                    salidaCantidad = salidaCantidad & "|" & dbFactorRegion
                    salidaCantidad = salidaCantidad & "|" & VarCosntante
                    salidaCantidad = salidaCantidad & "|" & (PrimaBasica / Facvigencia) 'PrimaBasica
                    salidaCantidad = salidaCantidad & "|" & Facvigencia
                    salidaCantidad = salidaCantidad & "|" & Cob_Adicional
                    salidaCantidad = salidaCantidad & "|" & DerPol
                    If strcp = "" Then
                        If banderafecha = 1 Then
                            salidaCantidad = salidaCantidad & "|" & dbiva
                        Else
                            salidaCantidad = salidaCantidad & "|" & dbiva
                        End If
                    Else
                        salidaCantidad = salidaCantidad & "|" & dbiva
                    End If
                    salidaCantidad = salidaCantidad & "|" & PrimaBasica 'PrimaNetaM
                    salidaCantidad = salidaCantidad & "|" & IvaSeguro
                    salidaCantidad = salidaCantidad & "|" & PrimaTotal 'PrimPolTotal
                    If BandSUBSIDY_FLG = 0 Then
                        salidaCantidad = salidaCantidad & "|0" 'subsidios
                    Else
                        salidaCantidad = salidaCantidad & "|" & VarSUBSIDIO_TT 'subsidios
                    End If
                    salidaCantidad = salidaCantidad & "|" & RecargoPagoFracc 'total recargo pago fraccionado
                    salidaCantidad = salidaCantidad & "|" & PorPagoFraccion 'Porcentaje recargo pago fraccionado
                    salidaCantidad = salidaCantidad & "|" & PorTipoCarga 'porcentaje de tipo de carag si existiera

                    For i = 0 To 4
                        Select Case i
                            Case 0
                                cuenta1 = PrimPolTotal
                            Case 1
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    If Math.Round(PrimaTotal - PrimPolTotal, 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta2 = PrimPolTotal
                                    Else
                                        cuenta2 = Math.Round(PrimaTotal, 2) - Math.Round(PrimPolTotal, 2)
                                    End If
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - PrimPolTotal, 2) > Math.Round(cuenta1, 2) Then
                                        cuenta2 = cuenta1
                                    Else
                                        cuenta2 = PrimaTotal - PrimPolTotal
                                    End If
                                End If
                            Case 2
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2), 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta3 = PrimPolTotal
                                    Else
                                        cuenta3 = Math.Round(PrimaTotal, 2) - Math.Round((cuenta1 + cuenta2), 2)
                                    End If
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2), 2) > Math.Round(cuenta1, 2) Then
                                        cuenta3 = cuenta1
                                    Else
                                        cuenta3 = PrimaTotal - (cuenta1 + cuenta2)
                                    End If
                                End If
                            Case 3
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    cuenta4 = Math.Round(PrimaTotal - cuenta1 - cuenta2 - cuenta3, 2)
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2 + cuenta3), 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta4 = cuenta1
                                    Else
                                        cuenta4 = PrimaTotal - (cuenta1 + cuenta2 + cuenta3)
                                    End If
                                End If
                            Case 4
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    cuenta5 = Math.Round(PrimaTotal - cuenta1 - cuenta2 - cuenta3 - cuenta4, 2)
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2 + cuenta3 + cuenta4), 2) >= Math.Round(PrimPolTotal, 2) Then
                                        cuenta5 = cuenta1
                                    Else
                                        cuenta5 = PrimaTotal - (cuenta1 + cuenta2 + cuenta3 + cuenta4)
                                    End If
                                End If
                        End Select
                    Next

                    ArrPolisa(0) = Math.Round(cuenta1, 2) 'poliza 1
                    ArrPolisa(1) = Math.Round(cuenta2, 2) 'poliza 2
                    ArrPolisa(2) = IIf(Math.Round(cuenta3, 2) >= 1, Math.Round(cuenta3, 2), 0) 'poliza 3
                    ArrPolisa(3) = IIf(Math.Round(cuenta4, 2) >= 1, Math.Round(cuenta4, 2), 0) 'poliza 4
                    ArrPolisa(4) = IIf(Math.Round(cuenta5, 2) >= 1, Math.Round(cuenta5, 2), 0) 'poliza 5
                    ArrPolisa(5) = Math.Round(VarSUBSIDIO_TT, 2) 'sumatoria de subsidio normal
                    ArrPolisa(6) = Math.Round(VarSUBSIDIO_LTT, 2) 'sumatoria de subsidio lealtad

                    cuenta1 = 0
                    cuenta2 = 0
                    cuenta3 = 0
                    cuenta4 = 0
                    cuenta5 = 0

                    If strtipopol = "A" Then
                        ReDim Results(11)
                        Results(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                        '''If VarSUBSIDIO_TT >= PrimaTotal Then
                        ''If BandSUBSIDY_FLG = 0 Then
                        ''    Results(1) = PrimaTotal  'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        ''Else
                        ''    Results(1) = PrimaTotal - VarSUBSIDIO_TT 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        ''End If
                        Results(1) = PrimaTotal 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        Results(2) = DerPol
                        Results(3) = 0
                        Results(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                        Results(5) = precontrol
                        Results(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                        Results(7) = VarSUBSIDY_F
                        Results(8) = VarSUBSIDY_FC
                        Results(9) = VarSUBSIDY_D
                        Results(10) = IIf(BandSUBSIDY_FLG = 1, PrimaTotal, 0)
                        Results(11) = IIf(BandSUBSIDY_FLG = 1, 0, PrimaBasica)   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                        'ArrPolisa(0) = PrimaTotal
                        ArrPolisa(9) = VarSUBSIDIO_LF1
                        ArrPolisa(10) = VarSUBSIDIO_LFC1
                        ArrPolisa(11) = VarSUBSIDIO_LD1
                        ArrPolisa(12) = VarSUBSIDIO_LS1
                        ArrPolisa(13) = RecargoPagoFracc
                        ArrPolisa(14) = PorPagoFraccion
                        subsidios(0) = VarSUBSIDY_F
                        subsidios(1) = VarSUBSIDY_FC
                        subsidios(2) = VarSUBSIDY_D
                        subsidios(3) = 0
                        subsidios(4) = dbiva 'no indica el tipo de iva que tiene
                    Else
                        ReDim Results(15)
                        Dim cob(2)
                        PrimaTotal = 0
                        For i = 0 To 4
                            PrimaTotal = PrimaTotal + CDbl(ArrPolisa(i))
                        Next
                        'GetInsurance = PrimaTotal + CreditLife

                        Results(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                        'If VarSUBSIDIO_TT >= ArrPolisa(0) Then
                        ''If BandSUBSIDY_FLG = 0 Then
                        ''    'Results(1) = PrimaTotal - ArrPolisa(0) 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        ''    Results(1) = PrimaTotal
                        ''Else
                        ''    Results(1) = PrimaTotal - VarSUBSIDIO_TT 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        ''End If
                        Results(1) = PrimaTotal
                        Results(2) = DerPol
                        Results(3) = 0
                        Results(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                        Results(5) = precontrol
                        Results(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                        Results(7) = VarSUBSIDY_F
                        Results(8) = VarSUBSIDY_FC
                        Results(9) = VarSUBSIDY_D
                        Results(10) = PrimaTotal
                        Results(11) = PrimaBasica   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                        Results(12) = VarSUBSIDIO_LF1
                        Results(13) = VarSUBSIDIO_LFC1
                        Results(14) = VarSUBSIDIO_LD1
                        Results(15) = VarSUBSIDIO_LS1
                        ArrPolisa(13) = RecargoPagoFracc
                        ArrPolisa(14) = PorPagoFraccion
                        subsidios(0) = VarSUBSIDY_F
                        subsidios(1) = VarSUBSIDY_FC
                        subsidios(2) = VarSUBSIDY_D
                        subsidios(3) = 0
                        subsidios(4) = dbiva 'no indica el tipo de iva que tiene
                    End If


                    For z = 0 To ArrPolisa.Length - 1
                        If z = 0 Then
                            cadenapoliza = ArrPolisa(z) & "*"
                        Else
                            cadenapoliza = cadenapoliza & ArrPolisa(z) & "*"
                        End If
                    Next
                    cadenapoliza = Mid(cadenapoliza, 1, cadenapoliza.Length - 1)
                    If k = 0 Or MultArrPolisa = "" Then
                        MultArrPolisa = nompaquete & "*" & cadenapoliza & "|"
                    Else
                        MultArrPolisa = MultArrPolisa & nompaquete & "*" & cadenapoliza & "|"
                    End If

                    If intprograma = 16 Or ban_multiple = 1 Then

                        ''''cnegocio.actualiza_cobertura_totales_fscar(IIf(VarSUBSIDIO_TT >= PrimaTotal, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT), _
                        'Cnegocio.actualiza_cobertura_totales_fscar(IIf(BandSUBSIDY_FLG = 0, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT), _
                        'intcontrato, intbid, subramoA, nompaquete)
                        cnegocio.actualiza_cobertura_totales_fscar(PrimaTotal, intcontrato, intbid, subramoA, nompaquete, intaseguradora)

                        If ban_Vida = 1 Then
                            'vida
                            cnegocio.actualiza_cobertura_totales_fscar_VD(ArrPolisa(7), _
                                intcontrato, intbid, _
                                nompaquete, 1, intprograma, intaonh, strestatus, intsubramo)

                        End If
                        If ban_Ace = 1 Then
                            'desempleo
                            cnegocio.actualiza_cobertura_totales_fscar_VD(ArrPolisa(8), _
                                intcontrato, intbid, _
                                nompaquete, 2, intprograma, intaonh, strestatus, intsubramo)
                        End If
                    Else
                        ''''cnegocio.actualiza_cobertura_totales(IIf(VarSUBSIDIO_TT >= PrimaTotal, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT), _
                        '''Cnegocio.actualiza_cobertura_totales(IIf(BandSUBSIDY_FLG = 0, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT), _
                        '''    intcontrato, intbid, subramoA, intaseguradora)
                        'cnegocio.actualiza_cobertura_totales(PrimaTotal, intcontrato, intbid, subramoA, intaseguradora)
                        cnegocio.actualiza_cobertura_totales_fscar(PrimaTotal, intcontrato, intbid, subramoA, nompaquete, intaseguradora)
                    End If

                    If k = 0 Or Strcadena = "" Then
                        '''Strcadena = intaseguradora.ToString & "*" & intpaquete & "*" & IIf(VarSUBSIDIO_TT >= PrimaTotal, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT) & "|"
                        Strcadena = intaseguradora.ToString & "*" & intpaquete & "*" & PrimaTotal & "*" & Facvigencia & "*" & intsubramo & "|"
                        'Strcadena = intaseguradora.ToString & "*" & intpaquete & "*" & IIf(BandSUBSIDY_FLG = 0, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT) & "|"

                        StrSalidaPanle = intaseguradora.ToString & "*" & _
                        Results(0) & "*" & ArrPolisa(13) & "*" & _
                        Results(2) & "*" & Results(4) & "*" & _
                        Results(1) & "*" & subsidios(4) & "*" & _
                        ArrPolisa(14) & "*" & intpaquete & "*" & _
                        intsubramo & "*" & BanTipoPago & "|"

                    Else
                        'Strcadena = Strcadena & intaseguradora.ToString & "*" & intpaquete & "*" & IIf(BandSUBSIDY_FLG = 0, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT) & "|"
                        Strcadena = Strcadena & intaseguradora.ToString & "*" & intpaquete & "*" & PrimaTotal & "*" & Facvigencia & "*" & intsubramo & "|"
                        ''''Strcadena = Strcadena & intaseguradora.ToString & "*" & intpaquete & "*" & IIf(VarSUBSIDIO_TT >= PrimaTotal, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT) & "|"

                        StrSalidaPanle = StrSalidaPanle & intaseguradora.ToString & "*" & _
                        Results(0) & "*" & ArrPolisa(13) & "*" & _
                        Results(2) & "*" & Results(4) & "*" & _
                        Results(1) & "*" & subsidios(4) & "*" & _
                        ArrPolisa(14) & "*" & intpaquete & "*" & _
                        intsubramo & "*" & BanTipoPago & "|"
                    End If

                    If CalculoArr = "" Then
                        CalculoArr = salidaCantidad
                    Else
                        CalculoArr = CalculoArr & "*" & salidaCantidad
                    End If

                    strinsco = "" : intplazas = 0 : VarFactPrima = 0 : VarCosntante = 0 : VarF1 = 0 : VarF2 = 0
                    BandSUBSIDY_FLG = 0 : VarSUBSIDY_F = 0 : VarSUBSIDY_FC = 0 : VarSUBSIDY_D = 0 : VarSUBSIDIO_TT = 0
                    VarSUBSIDIO_LF1 = 0 : VarSUBSIDIO_LFC1 = 0 : VarSUBSIDIO_LD1 = 0 : VarSUBSIDIO_LS1 = 0 : VarSUBSIDIO_LTT = 0
                    NumDias = 0 : Facvigencia = 0 : precontrol = 0 : PrimaBasica = 0 : PrimaNeta = 0 : DerPol = 0 : IvaSeguro = 0
                    NumPolizaCredito = 0 : TotalDerPol = 0 : PrimaTotal = 0 : cuenta1 = 0 : cuenta2 = 0 : cuenta3 = 0 : cuenta4 = 0
                    cuenta5 = 0 : PriamSImp = 0 : TemFactVig = 0 : PrimaNetaM = 0 : PrimPolTotal = 0 : dbFactorRegion = 0
                    dbFactorRegioncte = 0 : band_deduc = 0 : ban_precontrol = 0 : ban_rate = 0 : ban_constante = 0
                    CreditLife = 0

                Next
            End If

            If Strcadena = "" Then
                retur = ""
            Else
                MultArrPolisa = Mid(MultArrPolisa, 1, MultArrPolisa.Length - 1)
                retur = Mid(Strcadena, 1, Strcadena.Length - 1)
                StrSalidaPanle = Mid(StrSalidaPanle, 1, StrSalidaPanle.Length - 1)
            End If

            Return retur

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function calculo_aseguradora(ByVal banderafecha As Integer, ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal stranio As String, ByVal strmodelo As String, _
    ByVal intplan As Integer, _
    ByVal strtipopol As String, ByVal struso As String, _
    ByVal strcatalogo As String, ByVal strestatus As String, ByVal strfincon As String, _
    ByVal strrate As String, ByVal bFecIni As String, ByVal bTipoSeguro As Integer, _
    ByVal bPlazo As Integer, ByVal bValor_Factura As Double, _
    ByVal intcontrato As Integer, ByVal bSeguro As Integer, _
    ByVal intbid As Integer, ByVal Cob_Adicional As Double, _
    ByVal intaonh As Integer, ByVal intaseguradora As Integer, _
    ByVal intpaquete As Integer, ByVal intprograma As Integer, _
    ByVal fincon As String, _
    Optional ByVal TipoFinanciamiento As String = "", _
    Optional ByVal bEnganche As Double = 0, Optional ByVal strcp As String = "", _
    Optional ByVal intmarcaMulti As Integer = 0, _
    Optional ByVal intmarcaOriginalMulti As Integer = 0, _
    Optional ByVal TipoRecargo As Integer = 0, _
    Optional ByVal PlazoRecargo As Integer = 0, _
    Optional ByVal IdTipoCarga As Integer = 0, _
    Optional ByVal banderaPromocion As Integer = 0, _
    Optional ByVal idtipoprograma As Integer = -1, _
    Optional ByVal Aseg12meses As Integer = 0, _
    Optional ByVal Tipoprograma As Integer = 0, _
    Optional ByVal Ban12Meses As Byte = 0, _
    Optional ByVal BanRecMsg As Byte = 0, Optional ByVal IdCotizaCobertura As Integer = 0) As String

        Dim intregion As Integer = 0
        Dim stropcsa As String = ""
        '------
        Dim strinsco As String
        Dim intplazas As Integer
        Dim dt As DataTable
        Dim VarFactPrima As Double = 0
        Dim VarCosntante As Double = 0
        Dim VarF1 As Double = 0
        Dim VarF2 As Double = 0
        Dim BandSUBSIDY_FLG As Integer = 0
        Dim VarSUBSIDY_F As Double = 0
        Dim VarSUBSIDY_FC As Double = 0
        Dim VarSUBSIDY_D As Double = 0
        Dim VarSUBSIDIO_TT As Double = 0
        Dim VarSUBSIDIO_LF1 As Double = 0
        Dim VarSUBSIDIO_LFC1 As Double = 0
        Dim VarSUBSIDIO_LD1 As Double = 0
        Dim VarSUBSIDIO_LS1 As Double = 0
        Dim VarSUBSIDIO_LTT As Double = 0
        Dim NumDias As Integer = 0
        Dim Facvigencia As Double = 0
        Dim precontrol As Double = 0
        Dim PrimaBasica As Double = 0
        Dim PrimaNeta As Double = 0
        Dim DerPol As Double = 0
        Dim IvaSeguro As Double = 0
        Dim NumPolizaCredito As Double = 0
        Dim TotalDerPol As Double = 0
        'Dim PrimaTotal As String = ""
        Dim PrimaTotal As Double = 0
        Dim i As Integer = 0
        Dim k As Integer = 0
        Dim cuenta1 As Double = 0
        Dim cuenta2 As Double = 0
        Dim cuenta3 As Double = 0
        Dim cuenta4 As Double = 0
        Dim cuenta5 As Double = 0
        Dim PriamSImp As Double = 0
        Dim TemFactVig As Double = 0
        Dim PrimaNetaM As Double = 0
        Dim PrimPolTotal As Double = 0
        Dim NumPlazoPoliza As Double = 0
        Dim subramoA As Integer = 0
        Dim paqueteA As Integer = 0
        Dim dbFactorRegion As Double = 0
        Dim dbFactorRegioncte As Double = 0
        Dim banDobleCalculo As Integer = 0
        Dim Strcadena As String = ""
        'banderas de programa por aseguradora
        Dim band_deduc As Integer = 0
        Dim ban_precontrol As Integer = 0
        Dim ban_rate As Integer = 0
        Dim ban_constante As Integer = 0
        Dim ban_multiple As Integer = 0
        Dim intmarca As Integer = 0
        Dim nompaquete As String = ""
        Dim CreditLife As Double = 0
        Dim ban_ace As Integer = 0
        Dim ban_vida As Integer = 0
        Dim Ban_Iva As Integer = 0
        Dim dbiva As Double = 0.16
        Dim ban_EstatusConst As Integer = 0
        Dim ArrFraccion(0, 0) As String
        Dim z As Integer
        Dim cadenapoliza As String
        Dim PorPagoFraccion As Double = 0
        Dim RecargoPagoFracc As Double = 0
        Dim BanRecargoFraccion As Byte
        Dim CuotaCalculo As Double = 0
        Dim ConstanteCalculo As Double = 0
        Dim q As Integer

        Dim PorTipoCarga As Double = 1

        If banderafecha = 1 Then
            dbiva = 0.16
        Else
            dbiva = 0.16
        End If

        'estas variables son para las fracciones con recargos
        Dim dbRecargo As Double = 0
        Dim PnRecargo As Double = 0
        Dim BanRecargo As Integer = 0

        'estas variables son para las fracciones con factores
        Dim BanFactor As Integer = 0
        Dim NumPolizaFactor As Integer = 0
        Dim FraccionFactor As Integer = 0
        Dim dbFactor As Double = 0
        Dim PnFactor As Double = 0
        Dim BanTipoPago As Integer = 0
        Dim primatotalhdi As Double = 0
        Dim ivahdi As Double = 0


        Try

            ''''Dim dtp As DataTable = cnegocio.paquete_aseguradora(intsubramo, intaseguradora)
            'Dim dtp As DataTable = cnegocio.paquete_aseguradora_unico(intsubramo, intaseguradora, intpaquete)
            'HMH
            Dim dtp As DataTable = cnegocio.paquete_aseguradora_unico(intsubramo, intaseguradora, intpaquete, _
            IIf(Tipoprograma <= 0, "", IIf(strmodelo = "FIESTA IKON" And intaseguradora = 63, "N", strestatus)), Tipoprograma)
            If dtp.Rows.Count > 0 Then
                For k = 0 To dtp.Rows.Count - 1
regresa:
                    If banderaPromocion = 1 Then
                        strmodelo = IIf(Carga_AbcPromocion(intaseguradora, strmodelo) = "", strmodelo, Carga_AbcPromocion(intaseguradora, strmodelo))
                    End If

                    If banderafecha = 1 Then
                        dbiva = 0.16
                    Else
                        dbiva = 0.16
                    End If
                    subramoA = dtp.Rows(k)("subramo")
                    paqueteA = dtp.Rows(k)("paquete")
                    intpaquete = dtp.Rows(k)("id_paquete")
                    nompaquete = dtp.Rows(k)("descripcion_paquete")

                    If dtp.Rows(k).IsNull("opcsa_paquete") Then
                        stropcsa = ""
                    Else
                        stropcsa = dtp.Rows(k)("opcsa_paquete")
                    End If

                    Dim dtB As DataTable = cnegocio.determina_banderas_X_programa(intprograma, intaseguradora)
                    If dtB.Rows.Count > 0 Then
                        If Not dtB.Rows(0).IsNull("ban_deduc") Then
                            band_deduc = dtB.Rows(0)("ban_deduc")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_precontrol") Then
                            ban_precontrol = dtB.Rows(0)("ban_precontrol")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_rate") Then
                            ban_rate = dtB.Rows(0)("ban_rate")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_constante") Then
                            ban_constante = dtB.Rows(0)("ban_constante")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_Multianual") Then
                            ban_multiple = dtB.Rows(0)("Ban_Multianual")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_ace") Then
                            ban_ace = dtB.Rows(0)("Ban_ace")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_vida") Then
                            ban_vida = dtB.Rows(0)("Ban_vida")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_iva") Then
                            Ban_Iva = dtB.Rows(0)("Ban_iva")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_estatus") Then
                            ban_EstatusConst = dtB.Rows(0)("ban_estatus")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_recargo") Then
                            BanRecargo = dtB.Rows(0)("ban_recargo")
                        End If
                        If Not dtB.Rows(0).IsNull("ban_factores") Then
                            BanFactor = dtB.Rows(0)("ban_factores")
                        End If
                        If Not dtB.Rows(0).IsNull("Ban_RecargoFraccion") Then
                            BanRecargoFraccion = dtB.Rows(0)("Ban_RecargoFraccion")
                        End If
                        If BanRecargo = 1 And strtipopol = "F" Then
                            If Not dtB.Rows(0).IsNull("recargo") Then
                                dbRecargo = dtB.Rows(0)("recargo")
                                dbRecargo = dbRecargo / 100
                            End If
                        End If
                        If Not dtB.Rows(0).IsNull("ban_doblecalculo") Then
                            banDobleCalculo = dtB.Rows(0)("ban_doblecalculo")
                        End If
                    End If
                    dtB.Clear()

                    If Ban_Iva = 1 Then
                        If Not strcp = "" Then
                            Dim dtcp As DataTable = cnegocio.determina_iva_parametro_aseguradora(strcp, intaseguradora)
                            If dtcp.Rows.Count > 0 Then
                                If Not dtcp.Rows(0).IsNull("iva") Then
                                    dbiva = dtcp.Rows(0)("iva")
                                End If
                            End If
                            dtcp.Clear()
                        End If
                    End If

                    Dim dtRegion As DataTable = cnegocio.consulta_region(subramoA, intaseguradora, intbid)
                    If dtRegion.Rows.Count > 0 Then
                        If Not dtRegion.Rows(0).IsNull("id_region") Then
                            intregion = dtRegion.Rows(0)("id_region")
                        End If
                    End If
                    dtRegion.Clear()

                    Cob_Adicional = calcula_coberturas(intcontrato, intbid, subramoA, intaseguradora, strestatus)

                    dt = cnegocio.carga_insco_homologado(intaonh, stranio, intaseguradora, strestatus)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("insco") Then
                            strinsco = dt.Rows(0)("insco")
                        End If
                        If Not dt.Rows(0).IsNull("id_marca") Then
                            intmarca = dt.Rows(0)("id_marca")
                        End If
                        If Not dt.Rows(0).IsNull("catalogo_vehiculo") Then
                            If strcatalogo Is Nothing Then
                                strcatalogo = dt.Rows(0)("catalogo_vehiculo")
                            Else
                                If strcatalogo = "0" Then
                                    strcatalogo = dt.Rows(0)("catalogo_vehiculo")
                                End If
                            End If
                        End If
                    End If
                    dt.Clear()

                    dt = Nothing
                    dt = cnegocio.datos_Vehiculo(intaseguradora, strinsco, stranio, intprograma, intmarcaMulti, intmarcaOriginalMulti)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("plaza_vehiculo") Then
                            intplazas = dt.Rows(0)("plaza_vehiculo")
                        End If
                    End If
                    dt.Clear()

                    dt = Nothing
                    'evi 10/12/2013

                    If strestatus = "N" Or (intprograma = 30 Or intprograma = 29 Or intprograma = 31) Then
                        If intprograma <> 1 And intprograma <> 2 Then
                            dt = cnegocio.tarifac(intmoneda, intsubramo, intpaquete, _
                            stranio, strmodelo, intregion, intplan, _
                            strtipopol, intaseguradora, intprograma, stropcsa)
                            If dt.Rows.Count > 0 Then
                                If Not dt.Rows(0).IsNull("FACTPRIM_tarifac") Then
                                    VarFactPrima = dt.Rows(0)("FACTPRIM_tarifac")
                                End If
                                If Not dt.Rows(0).IsNull("FactDm_Tarifac") Then
                                    VarCosntante = dt.Rows(0)("FactDm_Tarifac")
                                End If
                                If Not dt.Rows(0).IsNull("opcsa_tarifac") Then
                                    stropcsa = dt.Rows(0)("opcsa_tarifac")
                                End If
                            Else
                                k = k + 1
                                If dtp.Rows.Count = k Then
                                    Exit For
                                Else
                                    GoTo regresa
                                End If
                            End If
                            dt.Clear()
                        End If
                    Else
                        dt = cnegocio.tariprima(intmoneda, intsubramo, intpaquete, _
                            stranio, strinsco, intregion, intplan, _
                            strtipopol, intaseguradora)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("prima_tariprima") Then
                                VarFactPrima = dt.Rows(0)("prima_tariprima")
                            End If
                            If Not dt.Rows(0).IsNull("opcsa_tariprima") Then
                                stropcsa = dt.Rows(0)("opcsa_tariprima")
                            End If
                        Else
                            k = k + 1
                            If dtp.Rows.Count = k Then
                                Exit For
                            Else
                                GoTo regresa
                            End If
                        End If
                        dt.Clear()
                    End If

                    'If Not strestatus = "N" Or Not intaseguradora = 1 Then
                    'If Not strestatus = "N" Then
                    'If Not strestatus = "N" Or ban_constante = 1 Then
                    If Not strestatus = "N" Or ban_constante = 1 Then
                        dt = Nothing
                        dt = cnegocio.constante(intmoneda, intsubramo, intplazas, _
                        stropcsa, intaseguradora, strestatus, ban_EstatusConst, _
                        intprograma, intmarca)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("CONSTANTE_constante") Then
                                VarCosntante = dt.Rows(0)("CONSTANTE_constante")
                            End If
                        End If
                        dt.Clear()
                    End If

                    dt = Nothing
                    dt = cnegocio.uso(intsubramo, struso, intaseguradora)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("F1") Then
                            VarF1 = dt.Rows(0)("F1")
                        End If
                        If Not dt.Rows(0).IsNull("F2") Then
                            VarF2 = dt.Rows(0)("F2")
                        End If
                    End If
                    dt.Clear()

                    dt = Nothing
                    dt = cnegocio.subsidio(intmoneda, strcatalogo, stranio, intregion, _
                                    intplan, intpaquete, strestatus, strfincon, stropcsa, intprograma, intaseguradora)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("SUBSIDY_FLG") Then
                            BandSUBSIDY_FLG = dt.Rows(0)("SUBSIDY_FLG")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_F") Then
                            VarSUBSIDY_F = dt.Rows(0)("SUBSIDY_F")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_FC") Then
                            VarSUBSIDY_FC = dt.Rows(0)("SUBSIDY_FC")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_D") Then
                            VarSUBSIDY_D = dt.Rows(0)("SUBSIDY_D")
                        End If
                        VarSUBSIDIO_TT = VarSUBSIDY_F + VarSUBSIDY_FC + VarSUBSIDY_D
                        'lealtad
                        If Not dt.Rows(0).IsNull("SUBSIDY_LF") Then
                            VarSUBSIDIO_LF1 = dt.Rows(0)("SUBSIDY_LF")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lFC") Then
                            VarSUBSIDIO_LFC1 = dt.Rows(0)("SUBSIDY_lFC")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lD") Then
                            VarSUBSIDIO_LD1 = dt.Rows(0)("SUBSIDY_lD")
                        End If
                        If Not dt.Rows(0).IsNull("SUBSIDY_lS") Then
                            VarSUBSIDIO_LS1 = dt.Rows(0)("SUBSIDY_lS")
                        End If
                        VarSUBSIDIO_LTT = VarSUBSIDIO_LF1 + VarSUBSIDIO_LFC1 + VarSUBSIDIO_LD1 + VarSUBSIDIO_LS1
                    End If
                    dt.Clear()

                    Dim arrFecha As String()

                    If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "1" Then
                        arrFecha = bFecIni.Split("/")
                        bFecIni = arrFecha(1) & "/" & arrFecha(0) & "/" & arrFecha(2)
                    End If

                    Dim dFechaIni As Date

                    Try
                        dFechaIni = Convert.ToDateTime(bFecIni)
                    Catch ex As Exception
                        arrFecha = bFecIni.Split("/")
                        bFecIni = arrFecha(1) & "/" & arrFecha(0) & "/" & arrFecha(2)
                        dFechaIni = bFecIni
                    End Try

                    'NumDias = DateDiff("M", bFecIni, DateAdd("m", CDbl(IIf(bTipoSeguro = 1, Trim(Str(bPlazo)), "12")), bFecIni))
                    'NumDias = DateDiff("M", bFecIni, DateAdd("m", CDbl(bPlazo), bFecIni))
                    NumDias = DateDiff("M", dFechaIni, DateAdd("m", CDbl(bPlazo), dFechaIni))
                    If NumPlazoPoliza = 0 Then NumPlazoPoliza = 12
                    If NumPlazoPoliza <> NumDias Then NumPlazoPoliza = NumDias

                    'HMH
                    dt = Nothing
                    'evi 14/11/2013 se agrega bandera para conauto-opcion
                    dt = cnegocio.Fvigencias(intmoneda, NumPlazoPoliza, strtipopol, intaseguradora)
                    If dt.Rows.Count > 0 Then

                        If (Aseg12meses = 63 And idtipoprograma <= 3 And intaseguradora = 62) Then
                            If Not dt.Rows(0).IsNull("FactVigCruzada") Then
                                Facvigencia = dt.Rows(0)("FactVigCruzada")
                            End If
                        Else
                            If Not dt.Rows(0).IsNull("FACTVIG") Then
                                Facvigencia = dt.Rows(0)("FACTVIG")
                            End If
                            If (intaseguradora = 58 Or intaseguradora = 61 Or intaseguradora = 63 Or intaseguradora = 65 Or intaseguradora = 67) And Not TipoRecargo = 1 Then
                                If Not dt.Rows(0).IsNull("FactVigMultianual") Then
                                    Facvigencia = dt.Rows(0)("FactVigMultianual")
                                End If
                            Else
                                If intaseguradora = 59 And banderaPromocion = 1 Then
                                    Facvigencia = dt.Rows(0)("FactVigMultianual")
                                End If
                            End If
                        End If


                        'If (Aseg12meses = 63 And idtipoprograma <= 3 And intaseguradora = 62) Then
                        '    If Not dt.Rows(0).IsNull("FactVigCruzada") Then
                        '        Facvigencia = dt.Rows(0)("FactVigCruzada")
                        '    End If
                        'End If
                        'If Not dt.Rows(0).IsNull("FACTVIG") Then
                        '    Facvigencia = dt.Rows(0)("FACTVIG")
                        'End If
                        'If (intaseguradora = 58 Or intaseguradora = 61 Or intaseguradora = 63) And Not TipoRecargo = 1 Then
                        '    If Not dt.Rows(0).IsNull("FactVigMultianual") Then
                        '        Facvigencia = dt.Rows(0)("FactVigMultianual")
                        '    End If
                        'End If
                    End If
                    dt.Clear()

                    'Calculamos la prima basica
                    If ban_precontrol = 1 And strestatus = "N" Then
                        dt = Nothing
                        dt = cnegocio.precontrol(strcatalogo, stranio, intmoneda, IIf(ban_rate = 1, strrate, ""), intaseguradora, intprograma)
                        If dt.Rows.Count > 0 Then
                            If Not dt.Rows(0).IsNull("PRECONTROL") Then
                                precontrol = dt.Rows(0)("PRECONTROL")
                            End If
                        End If
                        dt.Clear()
                    End If

                    'Calculamos el factor por region
                    dt = Nothing
                    dt = cnegocio.Determina_Region_factor(intaseguradora, intbid, intregion, subramoA, strestatus)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("factor") Then
                            dbFactorRegion = dt.Rows(0)("factor")
                        End If
                        If Not dt.Rows(0).IsNull("ctefactor") Then
                            dbFactorRegioncte = dt.Rows(0)("ctefactor")
                        End If
                    End If
                    dt.Clear()

                    ''EVI
                    'If strestatus = "N" Then
                    '    If ban_precontrol = 1 Then
                    '        If bValor_Factura <= precontrol Then
                    '            PrimaBasica = ((precontrol * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                    '        Else
                    '            PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                    '        End If
                    '    Else
                    '        PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                    '    End If
                    'Else
                    '    PrimaBasica = ((VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                    'End If

                    If IdTipoCarga > 0 Then
                        Dim dtTC As DataTable = cnegocio.Recargos_tipocarga(intaseguradora, IdTipoCarga, strmodelo)
                        If dtTC.Rows.Count > 0 Then
                            If Not dtTC.Rows(0).IsNull("recargo_tipocarga") Then
                                PorTipoCarga = dtTC.Rows(0)("recargo_tipocarga")
                            End If
                        End If
                        dtTC.Clear()
                    End If

                    VarCosntante = VarCosntante * PorTipoCarga

                    'Cuota calculo y constante calculo
                    If intprograma = 1 Or intprograma = 2 Then
                        Dim dtCC As DataTable = cnegocio.Accesorio_Calculo(intaseguradora, strmodelo)
                        If dtCC.Rows.Count > 0 Then
                            If Not dtCC.Rows(0).IsNull("cuota_calculo") Then
                                CuotaCalculo = dtCC.Rows(0)("cuota_calculo")
                            End If
                            If Not dtCC.Rows(0).IsNull("const_calculo") Then
                                ConstanteCalculo = dtCC.Rows(0)("const_calculo")
                            End If
                        End If
                        dtCC.Clear()
                    End If

                    'EVI
                    If strestatus = "N" Then
                        If ban_precontrol = 1 Then
                            If banDobleCalculo = 0 Then
                                If bValor_Factura <= precontrol Then
                                    PrimaBasica = ((precontrol * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                                Else
                                    PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + (VarCosntante * VarF2)
                                End If
                            Else
                                If bValor_Factura <= precontrol Then
                                    PrimaBasica = ((precontrol * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                                Else
                                    PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                                End If
                            End If
                        Else
                            'HM
                            If intprograma = 1 Or intprograma = 2 Then
                                Dim arrCadena() As String
                                Dim Strcadena1 As String
                                Dim Strcadena2 As String
                                Dim iCotizacion As Integer = 2
                                banderaPromocion = 1

                                Dim sParameters As String

                                sParameters = "@iPrograma|" & intprograma & "#"
                                sParameters = sParameters & "@sEstatus|" & strestatus & "#"
                                sParameters = sParameters & "@iAonV|" & intaonh & "#"
                                sParameters = sParameters & "@iMarcaOriginal|" & intmarcaOriginalMulti & "#"
                                sParameters = sParameters & "@iMarca|" & intmarcaMulti & "#"
                                sParameters = sParameters & "@IdTipoPrograma|" & banderaPromocion & "#"
                                sParameters = sParameters & "@sCodPostal|" & strcp & "#"
                                sParameters = sParameters & "@IDBID|" & intbid & "#"
                                sParameters = sParameters & "@NUMCONTRATO|" & intcontrato & "#"
                                sParameters = sParameters & "@ANIOVEHICULO|" & stranio & "#"
                                sParameters = sParameters & "@USO|" & struso & "#"
                                sParameters = sParameters & "@IMONEDA|" & intmoneda & "#"
                                sParameters = sParameters & "@VALORFACTURA|" & bValor_Factura & "#"
                                sParameters = sParameters & "@TIPORECARGO|" & TipoRecargo & "#"
                                sParameters = sParameters & "@NUMPAGO|" & PlazoRecargo & "#"
                                sParameters = sParameters & "@COTIZACION|" & iCotizacion & "#"
                                sParameters = sParameters & "@PLAZOSEGURO|" & NumPlazoPoliza & "#"
                                sParameters = sParameters & "@IdCotizaCobertura|" & IdCotizaCobertura

                                Strcadena = cnegocio.CalculoSP(Dictionary.spCalculoGeneral_Zona, sParameters, iCotizacion, Nothing)

                                arrCadena = Strcadena.Split("$")
                                Strcadena1 = arrCadena(0)
                                Strcadena2 = arrCadena(1)

                                If Strcadena <> "" Then
                                    Dim ResultsAseg() As String
                                    Dim ResultsQts() As String
                                    Dim ResultsCHUBB() As String
                                    Dim ResultsHdi() As String

                                    ResultsAseg = Strcadena2.Split("|")

                                    'ResultsQts = ResultsAseg(0).Split("*")
                                    'ResultsAxa = ResultsAseg(1).Split("*")
                                    'ResultsHdi = ResultsAseg(2).Split("*")

                                    For i = 0 To (ResultsAseg.Length - 1)
                                        If (ResultsAseg(i).Split("*")(0) = 1 Or ResultsAseg(i).Split("*")(0) = 4) And ResultsAseg(i).Split("*")(8) = intpaquete Then
                                            ResultsQts = ResultsAseg(i).Split("*")
                                        ElseIf (ResultsAseg(i).Split("*")(0) = 2 Or ResultsAseg(i).Split("*")(0) = 5) And ResultsAseg(i).Split("*")(8) = intpaquete Then
                                            ResultsCHUBB = ResultsAseg(i).Split("*")
                                        ElseIf (ResultsAseg(i).Split("*")(0) = 3 Or ResultsAseg(i).Split("*")(0) = 6) And ResultsAseg(i).Split("*")(8) = intpaquete Then
                                            ResultsHdi = ResultsAseg(i).Split("*")
                                        End If
                                    Next

                                    If intaseguradora = 1 Or intaseguradora = 4 Then
                                        PrimaBasica = ResultsQts(1)
                                        RecargoPagoFracc = ResultsQts(2)
                                        PorPagoFraccion = ResultsQts(7)
                                        DerPol = ResultsQts(3)
                                        IvaSeguro = ResultsQts(4)
                                        PrimaTotal = ResultsQts(5)
                                    ElseIf intaseguradora = 2 Or intaseguradora = 5 Then
                                        PrimaBasica = ResultsCHUBB(1)
                                        RecargoPagoFracc = ResultsCHUBB(2)
                                        PorPagoFraccion = ResultsCHUBB(7)
                                        DerPol = ResultsCHUBB(3)
                                        IvaSeguro = ResultsCHUBB(4)
                                        PrimaTotal = ResultsCHUBB(5)
                                    ElseIf intaseguradora = 3 Or intaseguradora = 6 Then
                                        PrimaBasica = ResultsHdi(1)
                                        RecargoPagoFracc = ResultsHdi(2)
                                        PorPagoFraccion = ResultsHdi(7)
                                        DerPol = ResultsHdi(3)
                                        IvaSeguro = ResultsHdi(4)
                                        PrimaTotal = ResultsHdi(5)
                                    End If

                                End If
                            Else
                                PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                            End If
                        End If
                    Else
                        'evi 10/12/2013
                        If (intprograma = 29 Or intprograma = 30) Then
                            PrimaBasica = ((bValor_Factura * VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                        Else
                            PrimaBasica = ((VarFactPrima * VarF1) * dbFactorRegion) + ((VarCosntante * VarF2) * dbFactorRegioncte)
                        End If
                    End If
                    PrimaBasica = Math.Round(PrimaBasica, 2)
                    PrimaNeta = PrimaBasica

                    If (intaseguradora = 1 Or intaseguradora = 4 Or intaseguradora = 2 Or intaseguradora = 5) Then
                        RecargoPagoFracc = PrimaBasica * (PorPagoFraccion)
                    End If

                    '***Sacamos el derecho de poliza
                    dt = Nothing
                    dt = cnegocio.tariderpol(intmoneda, intsubramo, intaseguradora, intprograma, intmarca)
                    If dt.Rows.Count > 0 Then
                        If Not dt.Rows(0).IsNull("DERPOL") Then
                            DerPol = dt.Rows(0)("DERPOL")
                        End If
                    End If
                    dt.Clear()

                    BanTipoPago = 1
                    PrimPolTotal = Math.Round(PrimaTotal, 2)


                    For i = 0 To 4
                        Select Case i
                            Case 0
                                cuenta1 = PrimPolTotal
                            Case 1
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    If Math.Round(PrimaTotal - PrimPolTotal, 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta2 = PrimPolTotal
                                    Else
                                        cuenta2 = Math.Round(PrimaTotal, 2) - Math.Round(PrimPolTotal, 2)
                                    End If
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - PrimPolTotal, 2) > Math.Round(cuenta1, 2) Then
                                        cuenta2 = cuenta1
                                    Else
                                        cuenta2 = PrimaTotal - PrimPolTotal
                                    End If
                                End If
                            Case 2
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2), 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta3 = PrimPolTotal
                                    Else
                                        cuenta3 = Math.Round(PrimaTotal, 2) - Math.Round((cuenta1 + cuenta2), 2)
                                    End If
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2), 2) > Math.Round(cuenta1, 2) Then
                                        cuenta3 = cuenta1
                                    Else
                                        cuenta3 = PrimaTotal - (cuenta1 + cuenta2)
                                    End If
                                End If
                            Case 3
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    cuenta4 = Math.Round(PrimaTotal - cuenta1 - cuenta2 - cuenta3, 2)
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2 + cuenta3), 2) >= Math.Round(2 * PrimPolTotal, 2) Then
                                        cuenta4 = cuenta1
                                    Else
                                        cuenta4 = PrimaTotal - (cuenta1 + cuenta2 + cuenta3)
                                    End If
                                End If
                            Case 4
                                If TipoFinanciamiento = "MO" Then
                                    'financiado MO 25 y 37
                                    cuenta5 = Math.Round(PrimaTotal - cuenta1 - cuenta2 - cuenta3 - cuenta4, 2)
                                Else
                                    'financiado general
                                    If Math.Round(PrimaTotal - (cuenta1 + cuenta2 + cuenta3 + cuenta4), 2) >= Math.Round(PrimPolTotal, 2) Then
                                        cuenta5 = cuenta1
                                    Else
                                        cuenta5 = PrimaTotal - (cuenta1 + cuenta2 + cuenta3 + cuenta4)
                                    End If
                                End If
                        End Select
                    Next

                    If Ban12Meses = 0 Then
                        ArrPolisa(0) = Math.Round(cuenta1, 2) 'poliza 1
                    Else
                        ArrPolisa12(0) = Math.Round(cuenta1, 2) 'poliza 1
                    End If
                    'If VarSUBSIDIO_TT >= cuenta1 Then
                    '    ArrPolisa(0) = Math.Round(cuenta1, 6) 'poliza 1  'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                    'Else
                    '    ArrPolisa(0) = Math.Round(cuenta1 - VarSUBSIDIO_TT, 6) 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                    'End If

                    If Ban12Meses = 0 Then
                        ArrPolisa(1) = Math.Round(cuenta2, 2) 'poliza 2
                        ArrPolisa(2) = IIf(Math.Round(cuenta3, 2) >= 1, Math.Round(cuenta3, 2), 0) 'poliza 3
                        ArrPolisa(3) = IIf(Math.Round(cuenta4, 2) >= 1, Math.Round(cuenta4, 2), 0) 'poliza 4
                        ArrPolisa(4) = IIf(Math.Round(cuenta5, 2) >= 1, Math.Round(cuenta5, 2), 0) 'poliza 5
                        ArrPolisa(5) = Math.Round(VarSUBSIDIO_TT, 2) 'sumatoria de subsidio normal
                        ArrPolisa(6) = Math.Round(VarSUBSIDIO_LTT, 2) 'sumatoria de subsidio lealtad
                        'ArrPolisa(7) = vida
                        'ArrPolisa(8) = desempleo
                        ArrPolisa(9) = VarSUBSIDIO_LF1
                        ArrPolisa(10) = VarSUBSIDIO_LFC1
                        ArrPolisa(11) = VarSUBSIDIO_LD1
                        ArrPolisa(12) = VarSUBSIDIO_LS1
                    Else
                        ArrPolisa12(1) = Math.Round(cuenta2, 2) 'poliza 2
                        ArrPolisa12(2) = IIf(Math.Round(cuenta3, 2) >= 1, Math.Round(cuenta3, 2), 0) 'poliza 3
                        ArrPolisa12(3) = IIf(Math.Round(cuenta4, 2) >= 1, Math.Round(cuenta4, 2), 0) 'poliza 4
                        ArrPolisa12(4) = IIf(Math.Round(cuenta5, 2) >= 1, Math.Round(cuenta5, 2), 0) 'poliza 5
                        ArrPolisa12(5) = Math.Round(VarSUBSIDIO_TT, 2) 'sumatoria de subsidio normal
                        ArrPolisa12(6) = Math.Round(VarSUBSIDIO_LTT, 2) 'sumatoria de subsidio lealtad
                        'ArrPolisa12(7) = vida
                        'ArrPolisa12(8) = desempleo
                        ArrPolisa12(9) = VarSUBSIDIO_LF1
                        ArrPolisa12(10) = VarSUBSIDIO_LFC1
                        ArrPolisa12(11) = VarSUBSIDIO_LD1
                        ArrPolisa12(12) = VarSUBSIDIO_LS1
                    End If
                    cuenta1 = 0
                    cuenta2 = 0
                    cuenta3 = 0
                    cuenta4 = 0
                    cuenta5 = 0

                    If strtipopol = "A" Then
                        ReDim Results(11)
                        ReDim Results12(11)
                        'If VarSUBSIDIO_TT >= PrimaTotal Then
                        '    Results(1) = PrimaTotal  'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        'Else
                        '    Results(1) = PrimaTotal - VarSUBSIDIO_TT 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                        'End If
                        If Ban12Meses = 0 Then
                            Results(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                            Results(1) = PrimaTotal 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            Results(2) = DerPol
                            Results(3) = 0
                            Results(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                            Results(5) = precontrol
                            Results(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                            Results(7) = VarSUBSIDY_F
                            Results(8) = VarSUBSIDY_FC
                            Results(9) = VarSUBSIDY_D
                            Results(10) = IIf(BandSUBSIDY_FLG = 1, PrimaTotal, 0)
                            Results(11) = IIf(BandSUBSIDY_FLG = 1, 0, PrimaBasica)   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                        Else
                            Results12(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                            Results12(1) = PrimaTotal 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            Results12(2) = DerPol
                            Results12(3) = 0
                            Results12(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                            Results12(5) = precontrol
                            Results12(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                            Results12(7) = VarSUBSIDY_F
                            Results12(8) = VarSUBSIDY_FC
                            Results12(9) = VarSUBSIDY_D
                            Results12(10) = IIf(BandSUBSIDY_FLG = 1, PrimaTotal, 0)
                            Results12(11) = IIf(BandSUBSIDY_FLG = 1, 0, PrimaBasica)   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                        End If


                        If Ban12Meses = 0 Then
                            ArrPolisa(0) = PrimaTotal
                            ArrPolisa(9) = VarSUBSIDIO_LF1
                            ArrPolisa(10) = VarSUBSIDIO_LFC1
                            ArrPolisa(11) = VarSUBSIDIO_LD1
                            ArrPolisa(12) = VarSUBSIDIO_LS1
                            ArrPolisa(13) = RecargoPagoFracc
                            ArrPolisa(14) = PorPagoFraccion
                        Else
                            ArrPolisa12(0) = PrimaTotal
                            ArrPolisa12(9) = VarSUBSIDIO_LF1
                            ArrPolisa12(10) = VarSUBSIDIO_LFC1
                            ArrPolisa12(11) = VarSUBSIDIO_LD1
                            ArrPolisa12(12) = VarSUBSIDIO_LS1
                            ArrPolisa12(13) = RecargoPagoFracc
                            ArrPolisa12(14) = PorPagoFraccion
                        End If
                        If Ban12Meses = 0 Then
                            subsidios(0) = VarSUBSIDY_F
                            subsidios(1) = VarSUBSIDY_FC
                            subsidios(2) = VarSUBSIDY_D
                            subsidios(3) = 0
                            subsidios(4) = dbiva 'no indica el tipo de iva que tiene
                        Else
                            subsidios12(0) = VarSUBSIDY_F
                            subsidios12(1) = VarSUBSIDY_FC
                            subsidios12(2) = VarSUBSIDY_D
                            subsidios12(3) = 0
                            subsidios12(4) = dbiva 'no indica el tipo de iva que tiene
                        End If

                    Else
                        ReDim Results(15)
                        ReDim Results12(15)
                        Dim cob(2)
                        PrimaTotal = 0
                        If Ban12Meses = 0 Then
                            For i = 0 To 4
                                PrimaTotal = PrimaTotal + CDbl(ArrPolisa(i))
                            Next
                        Else
                            For i = 0 To 4
                                PrimaTotal = PrimaTotal + CDbl(ArrPolisa12(i))
                            Next
                        End If
                        'GetInsurance = PrimaTotal + CreditLife

                        If Ban12Meses = 0 Then
                            Results(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                            If VarSUBSIDIO_TT >= ArrPolisa(0) Then
                                Results(1) = PrimaTotal - ArrPolisa(0) 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            Else
                                Results(1) = PrimaTotal - VarSUBSIDIO_TT 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            End If
                            Results(2) = DerPol
                            Results(3) = 0
                            Results(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                            Results(5) = precontrol
                            Results(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                            Results(7) = VarSUBSIDY_F
                            Results(8) = VarSUBSIDY_FC
                            Results(9) = VarSUBSIDY_D
                            Results(10) = PrimaTotal
                            Results(11) = PrimaBasica   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                            Results(12) = VarSUBSIDIO_LF1
                            Results(13) = VarSUBSIDIO_LFC1
                            Results(14) = VarSUBSIDIO_LD1
                            Results(15) = VarSUBSIDIO_LS1
                        Else
                            Results12(0) = PrimaNeta 'Si hay subsidio se manda la prima neta subsidiada en otro caso la prima neta real
                            If VarSUBSIDIO_TT >= ArrPolisa12(0) Then
                                Results12(1) = PrimaTotal - ArrPolisa12(0) 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            Else
                                Results12(1) = PrimaTotal - VarSUBSIDIO_TT 'Si hay subsidio se manda la prima total subsidiada en otro caso la prima total real
                            End If
                            Results12(2) = DerPol
                            Results12(3) = 0
                            Results12(4) = IvaSeguro    'Si hay subsidio se manda el iva subsidiado en otro caso el iva real
                            Results12(5) = precontrol
                            Results12(6) = IIf(precontrol = 0, bValor_Factura, precontrol)
                            Results12(7) = VarSUBSIDY_F
                            Results12(8) = VarSUBSIDY_FC
                            Results12(9) = VarSUBSIDY_D
                            Results12(10) = PrimaTotal
                            Results12(11) = PrimaBasica   'Si hay subsidio se manda la prima basica subsidiada en otro caso la prima basica real
                            Results12(12) = VarSUBSIDIO_LF1
                            Results12(13) = VarSUBSIDIO_LFC1
                            Results12(14) = VarSUBSIDIO_LD1
                            Results12(15) = VarSUBSIDIO_LS1
                        End If

                        If Ban12Meses = 0 Then
                            ArrPolisa(13) = RecargoPagoFracc
                            ArrPolisa(14) = PorPagoFraccion
                        Else
                            ArrPolisa12(13) = RecargoPagoFracc
                            ArrPolisa12(14) = PorPagoFraccion
                        End If
                        If Ban12Meses = 0 Then
                            subsidios(0) = VarSUBSIDY_F
                            subsidios(1) = VarSUBSIDY_FC
                            subsidios(2) = VarSUBSIDY_D
                            subsidios(3) = 0
                            subsidios(4) = dbiva 'no indica el tipo de iva que tiene
                        Else
                            subsidios12(0) = VarSUBSIDY_F
                            subsidios12(1) = VarSUBSIDY_FC
                            subsidios12(2) = VarSUBSIDY_D
                            subsidios12(3) = 0
                            subsidios12(4) = dbiva 'no indica el tipo de iva que tiene
                        End If
                    End If

                    If Ban12Meses = 0 Then
                        For z = 0 To ArrPolisa.Length - 1
                            If z = 0 Then
                                cadenapoliza = ArrPolisa(z) & "*"
                            Else
                                cadenapoliza = cadenapoliza & ArrPolisa(z) & "*"
                            End If
                        Next
                    Else
                        For z = 0 To ArrPolisa12.Length - 1
                            If z = 0 Then
                                cadenapoliza = ArrPolisa12(z) & "*"
                            Else
                                cadenapoliza = cadenapoliza & ArrPolisa12(z) & "*"
                            End If
                        Next
                    End If
                    cadenapoliza = Mid(cadenapoliza, 1, cadenapoliza.Length - 1)
                    If Ban12Meses = 0 Then
                        If k = 0 Or MultArrPolisa = "" Then
                            MultArrPolisa = nompaquete & "*" & cadenapoliza & "|"
                        Else
                            MultArrPolisa = MultArrPolisa & nompaquete & "*" & cadenapoliza & "|"
                        End If
                    Else
                        If k = 0 Or MultArrPolisa12 = "" Then
                            MultArrPolisa12 = nompaquete & "*" & cadenapoliza & "|"
                        Else
                            MultArrPolisa12 = MultArrPolisa12 & nompaquete & "*" & cadenapoliza & "|"
                        End If
                    End If

                    If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Or intprograma = 11 Or ban_multiple = 1 Then
                        cnegocio.actualiza_cobertura_totales_fscar(IIf(VarSUBSIDIO_TT >= PrimaTotal, PrimaTotal, PrimaTotal - VarSUBSIDIO_TT), _
                            intcontrato, intbid, subramoA, nompaquete, intaseguradora)

                        If ban_vida = 1 Then
                            'vida
                            cnegocio.actualiza_cobertura_totales_fscar_VD(ArrPolisa(7), _
                                    intcontrato, intbid, _
                                    nompaquete, 1, intprograma, intaonh, _
                                    strestatus, intsubramo)
                        End If
                        If ban_ace = 1 Then
                            'desempleo
                            cnegocio.actualiza_cobertura_totales_fscar_VD(ArrPolisa(8), _
                                intcontrato, intbid, _
                                nompaquete, 2, intprograma, intaonh, _
                                strestatus, intsubramo)
                        End If
                    Else
                        'paneles contado
                        cnegocio.actualiza_cobertura_totales_fscar(PrimaTotal, intcontrato, intbid, subramoA, nompaquete, intaseguradora)
                    End If

                    If Tipoprograma > 0 Or BanRecMsg = 1 Then
                        StrCadenaAseg = intaseguradora.ToString & "*" & intpaquete & "*" & PrimaTotal & "*" & Facvigencia & "*" & intsubramo

                        StrSalidaPanleAseg = intaseguradora.ToString & "*" & _
                        Results(0) & "*" & ArrPolisa(13) & "*" & _
                        Results(2) & "*" & Results(4) & "*" & _
                        Results(1) & "*" & subsidios(4) & "*" & _
                        ArrPolisa(14) & "*" & intpaquete & "*" & _
                        intsubramo & "*1"
                    End If


                    strinsco = "" : intplazas = 0 : VarFactPrima = 0 : VarCosntante = 0 : VarF1 = 0 : VarF2 = 0
                    BandSUBSIDY_FLG = 0 : VarSUBSIDY_F = 0 : VarSUBSIDY_FC = 0 : VarSUBSIDY_D = 0 : VarSUBSIDIO_TT = 0
                    VarSUBSIDIO_LF1 = 0 : VarSUBSIDIO_LFC1 = 0 : VarSUBSIDIO_LD1 = 0 : VarSUBSIDIO_LS1 = 0 : VarSUBSIDIO_LTT = 0
                    NumDias = 0 : Facvigencia = 0 : precontrol = 0 : PrimaBasica = 0 : PrimaNeta = 0 : DerPol = 0 : IvaSeguro = 0
                    NumPolizaCredito = 0 : TotalDerPol = 0 : PrimaTotal = 0 : cuenta1 = 0 : cuenta2 = 0 : cuenta3 = 0 : cuenta4 = 0
                    cuenta5 = 0 : PriamSImp = 0 : TemFactVig = 0 : PrimaNetaM = 0 : PrimPolTotal = 0 : dbFactorRegion = 0
                    dbFactorRegioncte = 0 : band_deduc = 0 : ban_precontrol = 0 : ban_rate = 0 : ban_constante = 0
                    CreditLife = 0 : ban_multiple = 0
                Next
            End If

            If Ban12Meses = 0 Then
                MultArrPolisa = Mid(MultArrPolisa, 1, MultArrPolisa.Length - 1)
            Else
                MultArrPolisa12 = Mid(MultArrPolisa, 1, MultArrPolisa.Length - 1)
            End If

            Return PrimaTotal

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub GetInsurVida(ByVal intaseguradora As Integer, ByVal intmoneda As String, _
    ByVal bPlazo As Integer, ByVal intregion As Integer, ByVal StrtipoPol As String, _
    ByVal bValor_Factura As Double, ByVal bEnganche As Double, ByVal PrimaTotal As Double, _
    ByVal intmarca As Integer, ByVal strmodelo As String, ByVal intanio As Integer, _
    ByVal TipoSeguro As Integer, ByVal derpol As Double, ByVal banace As Integer)
        Dim FactMensLife As Double = 0
        Dim VarSumFinanc As Double = 0
        Dim PrimaTotalDesempleo As Double = 0
        Dim i As Integer = 0
        Dim TpolizaSum As Double = 0
        Dim regresa As Double = 0
        Try
            Dim dtv As DataTable = cnegocio.seguro_vida(intaseguradora, intmoneda, bPlazo, intregion)
            If dtv.Rows.Count > 0 Then
                If Not dtv.Rows(0).IsNull("factvida") Then
                    FactMensLife = dtv.Rows(0)("factvida")
                End If
            End If

            If banace = 1 Then
                Dim dtD As DataTable = cnegocio.Desempelo(intaseguradora, intmarca, strmodelo, intanio, intmoneda)
                If dtD.Rows.Count > 0 Then
                    If Not dtD.Rows(0).IsNull("MENS") Then
                        PrimaTotalDesempleo = dtD.Rows(0)("MENS") * bPlazo
                    End If
                End If
            End If

            If StrtipoPol <> "M" Then
                If TipoSeguro = 2 Then
                    VarSumFinanc = (((bValor_Factura - bEnganche) + PrimaTotal + PrimaTotalDesempleo) * FactMensLife) / 1000
                Else
                    VarSumFinanc = (((bValor_Factura - bEnganche)) * FactMensLife) / 1000
                End If
            Else
                For i = 0 To 4
                    If i = 0 Then
                        'TpolizaSum = TpolizaSum + Round(IIf(ArrPolisa(5) = 0, ArrPolisa(i), ArrPolisa(5)), 2)
                        TpolizaSum = TpolizaSum + Math.Round(IIf(CDbl(ArrPolisa(5)) = 0, CDbl(ArrPolisa(i)), IIf(CDbl(ArrPolisa(5)) >= CDbl(ArrPolisa(i)), 0, CDbl(ArrPolisa(i)) - CDbl(ArrPolisa(5)))), 2)
                    Else
                        TpolizaSum = TpolizaSum + Math.Round(CDbl(ArrPolisa(i)), 2)
                    End If
                Next
                VarSumFinanc = (((bValor_Factura - bEnganche) + TpolizaSum + PrimaTotalDesempleo) * FactMensLife) / 1000
            End If

            If TipoSeguro = 2 Then
                'seguro financiado
                'Segundo calculo del factor
                regresa = ((VarSumFinanc * FactMensLife) / 1000) + VarSumFinanc
                'If intaseguradora = 12 Then
                '    regresa = VarSumFinanc
                'Else
                '    regresa = ((VarSumFinanc * FactMensLife) / 1000) + VarSumFinanc
                'End If
            Else
                regresa = VarSumFinanc
            End If

            If regresa > 0 Then
                'derecho de poliza es fijo de 150
                regresa = regresa + 150
            End If

            'desempleo
            ArrPolisa(8) = PrimaTotalDesempleo
            'seguro de vida
            ArrPolisa(7) = regresa
            Results(3) = regresa

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function calcula_coberturas(ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal strestatus As String) As Double
        Dim seguro As Double = 0
        Dim dt As DataTable = cnegocio.calcula_coberturas(intcontrato, intbid, intsubramo, intaseguradora, strestatus)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("totales") Then
                    seguro = dt.Rows(0)("totales")
                End If
            End If
            Return seguro
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function calculando_total_coberturas(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer) As Double
        Dim seguro As Double = 0
        Dim dt As DataTable = cnegocio.calculando_total_coberturas(intcontrato, intbid, intsubramo)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("total") Then
                    seguro = dt.Rows(0)("total")
                End If
            End If

            cnegocio.actualiza_cobertura_totales(seguro, intcontrato, intbid, intsubramo, 0)

            Return seguro

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function actualiza_numplazo_cob(ByVal intnumplazo As Integer, ByVal intcontrato As Integer, ByVal intbid As Integer)
        cnegocio.actualiza_numplazo_cob(intnumplazo, intcontrato, intbid)
    End Function

    Public Sub realiza_calculo_plazo_cobertura(ByVal numcontrato As Integer, ByVal intbid As Integer, ByVal intprograma As Integer, ByVal intpalzo As Integer)
        Dim i As Integer = 0
        Dim dbFC As Double = 0
        'Dim intpalzo As Integer = 0
        Dim dbSumA As Double = 0
        Dim dbFV As Double = 0
        Dim dbtotal As Double = 0
        Try
            Dim dt As DataTable = cnegocio.realiza_calculo_plazo_cobertura(numcontrato, intbid, intprograma)
            If dt.Rows.Count > 0 Then
                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i).IsNull("FACTOR_COB") Then
                        dbFC = 0
                    Else
                        dbFC = dt.Rows(i)("FACTOR_COB")
                    End If
                    'If dt.Rows(i).IsNull("num_plazo") Then
                    '    intpalzo = 0
                    'Else
                    '    intpalzo = dt.Rows(i)("num_plazo")
                    'End If
                    If dt.Rows(i).IsNull("SUMAASEG_COB") Then
                        dbSumA = 0
                    Else
                        dbSumA = dt.Rows(i)("SUMAASEG_COB")
                    End If
                    If dt.Rows(i).IsNull("FACTVIGENCIA") Then
                        dbFV = 0
                    Else
                        dbFV = dt.Rows(i)("FACTVIGENCIA")
                    End If
                    If Not cnegocio.valida_COBERTURA_ADICIONAL_PRIMA(dt.Rows(i)("valida")) = 1 Then
                        dbtotal = dbSumA * ((dbFC / 12) * intpalzo)
                    Else
                        dbtotal = dbFV * dbFC
                    End If
                    cnegocio.actualiza_coberturas_plazo(dbtotal, dt.Rows(i)("id_cobertura"), _
                            numcontrato, intbid, dt.Rows(i)("subramo"), dt.Rows(i)("id_aseguradora"))
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_totales_fscar_VD(ByVal dbtotal As Double, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal nompaquete As String, ByVal inttipo As Integer, _
    ByVal intprograma As Integer, ByVal intaonv As Integer, _
    ByVal strestatus As String, ByVal intsubramo As Integer)
        cnegocio.actualiza_cobertura_totales_fscar_VD(dbtotal, intcontrato, intbid, _
        nompaquete, inttipo, intprograma, intaonv, strestatus, intsubramo)
    End Sub

    'inicia nuevo micha
    Public Function remplaza_vacio(ByVal strcadena As String) As String
        strcadena = strcadena.Replace(" ", "")
        strcadena = strcadena.Replace(" ", "")
        strcadena = strcadena.Replace(" ", "")
        Return strcadena
    End Function
    'fin nuevo

    Public Function carga_plazo(ByVal IdPrograma As Integer, ByVal opcion As String) As DataTable
        Return cnegocio.carga_plazo(IdPrograma, opcion)
    End Function

    Public Function Inst_Cotizacion_panel_12meses(ByVal strCadenaInsert As String, ByVal bandTipo As String) As Integer
        Return cnegocio.Inst_Cotizacion_panel_12meses(strCadenaInsert, bandTipo)
    End Function

    'HMH
    Public Function Update_Cotizacion_panel_12meses(ByVal IdCotizacion12meses As Long, ByVal strCadenaInsert As String, ByVal bandTipo As String) As DataTable
        Return cnegocio.Update_Cotizacion_panel_12meses(IdCotizacion12meses, strCadenaInsert, bandTipo)
    End Function

    'HMH
    Public Function Act_Cotizacion_panel(ByVal IdCotizacionPanel As Long, ByVal strCadenaInsert As String) As DataTable
        Return cnegocio.Act_Cotizacion_panel(IdCotizacionPanel, strCadenaInsert)
    End Function

    Public Sub Upd_Cotizacion_panel_12meses(ByVal IdCotizacion As Integer, ByVal IdAseguradora As Integer, ByVal bandTipo As String)
        cnegocio.Upd_Cotizacion_panel_12meses(IdCotizacion, IdAseguradora, bandTipo)
    End Sub

    Public Function Promocion12meses(ByVal IdCotizacion As Integer) As DataTable
        Return cnegocio.Promocion12meses(IdCotizacion)
    End Function

    Public Function Inst_Cotizacion_panel(ByVal strCadenaInsert As String, ByVal IdCotizaCobertura As Integer) As Integer
        Return cnegocio.Inst_Cotizacion_panel(strCadenaInsert, IdCotizaCobertura)
    End Function

    Public Function CargaCtoizaPanel(ByVal idcotizacion As Integer, ByVal IdAseguradora As Integer) As DataTable
        Return cnegocio.CargaCtoizaPanel(idcotizacion, IdAseguradora)
    End Function

    Public Function CargaCtoizaPanelAseguradora(ByVal idcotizacion As Integer) As DataTable
        Return cnegocio.CargaCtoizaPanelAseguradora(idcotizacion)
    End Function

    Public Function CargaTipoRecargo() As DataTable
        Return cnegocio.CargaTipoRecargo()
    End Function

    Public Function CargaTipoRecargo(ByVal iIdAseguradora As Integer, ByVal iPrograma As Integer) As DataTable
        Return cnegocio.CargaTipoRecargo(iIdAseguradora, iPrograma)
    End Function

    Public Function CargaPaquete(ByVal iIdAseguradora As Integer) As DataTable
        Return cnegocio.CargaPaquete(iIdAseguradora)
    End Function

    Public Function CargaTipoRecargo_AutoOpcion(ByVal idcotizacionPanel As Integer, ByVal idaseguradora As Integer) As DataTable
        Return cnegocio.CargaTipoRecargo_AutoOpcion(idcotizacionPanel, idaseguradora)
    End Function

    Public Function Carga_AbcPromocion(ByVal IdAseguradora As Integer, ByVal StrModelo As String) As String
        Dim strsalida As String = ""
        Try
            Dim dt As DataTable = cnegocio.Carga_AbcPromocion(IdAseguradora, StrModelo)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("modelo") Then
                    strsalida = dt.Rows(0)("modelo")
                End If
            End If
            Return strsalida
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 10/12/2013 se agrega cotizacion_unica
    'HM  20/02/2014 se agrega fecha de inicio vigencia
    Public Function inserta_cotizacion_unica(ByVal IdCotiza12 As Integer, ByVal IdCotiza As Integer, ByVal IntContrato As Integer, ByVal IdBid As Integer, ByVal IdAonh As Integer, _
                                             ByVal StrModelo As String, ByVal Intanio As Integer, ByVal DbSA As Double, ByVal IntPlazo As Integer, ByVal IntPlazorestante As Integer, _
                                             ByVal IdMarca As Integer, ByVal IdPrograma As Integer, ByVal Exclusion As Integer, ByVal IdPersona As Integer, ByVal strestatus As String, _
                                             ByVal strFecIni As String, ByVal StrModeloC As String, ByVal IntanioC As Integer, ByVal DbSAC As Double, ByVal DifSA As Double, _
                                             ByVal IntCuotas As Integer, ByVal IntMesesTranscurridos As Integer, ByVal IntMesesSeguroGratis As Integer, ByVal IdAonhC As Integer, _
                                             ByVal IdCotiza12C As Integer, ByVal strCodigoPostal As String) As Long
        Return cnegocio.inserta_cotizacion_unica(IdCotiza12, IdCotiza, IntContrato, IdBid, IdAonh, StrModelo, Intanio, DbSA, IntPlazo, IntPlazorestante, IdMarca, IdPrograma, Exclusion, _
                                                 IdPersona, strestatus, strFecIni, StrModeloC, IntanioC, DbSAC, DifSA, IntCuotas, IntMesesTranscurridos, IntMesesSeguroGratis, IdAonhC, _
                                                 IdCotiza12C, strCodigoPostal)
    End Function

    'HMH
    Public Function Prima12meses(ByVal idcotizacion12meses As Long) As DataTable
        Return cnegocio.Prima12meses(idcotizacion12meses)
    End Function


    Public Function PrimaMultianual(ByVal idcotizacionmultianul As Long, ByVal IdASeguardora As Integer) As DataTable
        Return cnegocio.PrimaMultianual(idcotizacionmultianul, IdASeguardora)
    End Function

    Public Function calculoSP(ByVal intprograma As Integer, ByVal strestatus As String, ByVal intaonh As Integer, _
                              ByVal intmarcaOriginalMulti As Integer, ByVal intmarcaMulti As Integer, _
                              ByVal idtipoprograma As Integer, ByVal strcp As String, ByVal intbid As Integer, ByVal intcontrato As Integer, _
        ByVal stranio As String, ByVal struso As String, ByVal intmoneda As Integer, ByVal bValor_Factura As Double, ByVal TipoRecargo As Integer, _
        ByVal PlazoRecargo As Integer, ByVal iCotizacion As Integer, ByVal iPlazoSeguro As Integer, ByRef IdCotizaCobertura As Integer) As String

        'ByVal banderafecha As Integer, _
        '    ByVal strmodelo As String, _
        '    ByVal intplan As Integer, _
        '    ByVal strtipopol As String, _
        '    ByVal strfincon As String, _
        '    ByVal strrate As String, ByVal bFecIni As String, ByVal bTipoSeguro As Integer, _
        '    ByVal bPlazo As Integer, _
        '    ByVal bSeguro As Integer, _
        '    ByVal Cob_Adicional As Double, _
        '    ByVal fincon As String, _
        '    Optional ByVal TipoFinanciamiento As String = "", _
        '    Optional ByVal bEnganche As Double = 0, _
        '    Optional ByVal IdTipoCarga As Integer = 0, _
        '    Optional ByVal banderaPromocion As Integer = 0, _
        '    Optional ByVal Aseg12meses As Integer = 0) As String

        Dim sParameters As String

        Try
            sParameters = "@iPrograma|" & intprograma & "#"
            sParameters = sParameters & "@sEstatus|" & strestatus & "#"
            sParameters = sParameters & "@iAonV|" & intaonh & "#"
            sParameters = sParameters & "@iMarcaOriginal|" & intmarcaOriginalMulti & "#"
            sParameters = sParameters & "@iMarca|" & intmarcaMulti & "#"
            sParameters = sParameters & "@IdTipoPrograma|" & idtipoprograma & "#"
            sParameters = sParameters & "@sCodPostal|" & strcp & "#"
            sParameters = sParameters & "@IDBID|" & intbid & "#"
            sParameters = sParameters & "@NUMCONTRATO|" & intcontrato & "#"
            sParameters = sParameters & "@ANIOVEHICULO|" & stranio & "#"
            sParameters = sParameters & "@USO|" & struso & "#"
            sParameters = sParameters & "@IMONEDA|" & intmoneda & "#"
            sParameters = sParameters & "@VALORFACTURA|" & bValor_Factura & "#"
            sParameters = sParameters & "@TIPORECARGO|" & TipoRecargo & "#"
            sParameters = sParameters & "@NUMPAGO|" & PlazoRecargo & "#"
            sParameters = sParameters & "@COTIZACION|" & iCotizacion & "#"
            sParameters = sParameters & "@PLAZOSEGURO|" & iPlazoSeguro & "#"
            sParameters = sParameters & "@IdCotizaCobertura|" & IdCotizaCobertura

            'If intprograma = 32 Or intprograma = 33 Then
            Return cnegocio.CalculoSP(Dictionary.spCalculoGeneral_Zona, sParameters, iCotizacion, IdCotizaCobertura)
            'Else
            'Return cnegocio.CalculoSP(Dictionary.spCalculoGeneral, sParameters, iCotizacion)
            'End If


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Accesorio_Calculo(ByVal IdAseguradora As Integer, ByVal strmodelo As String) As DataTable
        Return cnegocio.Accesorio_Calculo(IdAseguradora, strmodelo)
    End Function

    Public Function CargaCobertura(ByVal iIdAseguradora As Integer, ByVal iIdPaquete As Integer) As DataTable
        Return cnegocio.CargaCobertura(iIdAseguradora, iIdPaquete)
    End Function
End Class
