Public Class CnValidacion

    Public Function QuitaCaracteres(ByVal StrValor As String, Optional ByVal Mayusculas As Boolean = True, Optional ByVal CaracterEspecial As String = "", Optional ByVal Reemplaza As Boolean = True) As String
        If Reemplaza = True Then
            StrValor = Replace(StrValor, "%", "")
            'StrValor = Replace(StrValor, "&", " ")
            StrValor = Replace(StrValor, "'", "")
            StrValor = Replace(StrValor, "+", " ")
            StrValor = Replace(StrValor, """", " ")
            StrValor = Replace(StrValor, "*", " ")
            StrValor = Replace(StrValor, "�", " ")
            StrValor = Replace(StrValor, "=", " ")
            StrValor = Replace(StrValor, "<", " ")
            StrValor = Replace(StrValor, ">", " ")
            StrValor = Replace(StrValor, ">", " ")
        End If

        If CaracterEspecial <> "" Then
            If CaracterEspecial = "||" Then
                StrValor = Replace(StrValor, CaracterEspecial, "'")
            Else
                If CaracterEspecial = "'" Then
                    StrValor = Replace(StrValor, CaracterEspecial, "||")
                Else
                    StrValor = Replace(StrValor, CaracterEspecial, " ")
                End If
            End If
        End If

        StrValor = Trim(StrValor)
        If Mayusculas = True Then
            StrValor = UCase(StrValor)
        End If
        Return StrValor
    End Function

    Public Function Crea_Anio(Optional ByVal Intformato As Integer = 0) As String
        'Intformato  
        '= 0 d/m/y
        '= 1 m/d/y
        '= 2 y/m/d

        Dim StrCadena As String = ""
        Select Case Intformato
            Case 0
                StrCadena = complementa_fecha(Now.Day) & "/" & complementa_fecha(Now.Month) & "/" & Now.Year
            Case 1
                StrCadena = complementa_fecha(Now.Month) & "/" & complementa_fecha(Now.Day) & "/" & Now.Year
            Case 2
                StrCadena = complementa_fecha(Now.Year) & "/" & complementa_fecha(Now.Month) & "/" & Now.Day
        End Select

        Return StrCadena
    End Function

    Public Function complementa_fecha(ByVal strfecha As String) As String
        Dim fecha As String
        If strfecha.Length = 1 Then
            fecha = "0" & strfecha.Trim
        Else
            fecha = strfecha
        End If
        Return fecha
    End Function

    Public Function EsAlfaNumerico(ByVal PsString As String) As Boolean
        Dim LsAlfa As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz���������������������������������������������������������������������_-#."
        Dim LsNum As String = "0123456789"
        Dim banN As Boolean = False
        Dim banA As Boolean = False
        Dim ban As Boolean = False
        Dim i As Integer = 0
        Try
            For i = 0 To PsString.Length - 1
                If LsNum.IndexOf(PsString.Chars(i)) <> -1 Then
                    banN = True
                    Exit For
                End If
            Next
            For i = 0 To PsString.Length - 1
                If LsAlfa.IndexOf(PsString.Chars(i)) <> -1 Then
                    banA = True
                    Exit For
                End If
            Next

            If banA = True And banN = True Then
                ban = True
            Else
                ban = False
            End If
            Return ban
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidarCorreo(ByVal xCorreo As String) As Boolean
        Dim Indice
        Dim Caracter
        Dim Largo
        Dim Estado

        ValidarCorreo = False


        If xCorreo <> "" Then

            Largo = Len(xCorreo)

            Estado = 0 ' Estado inicial del aut�mata

            For Indice = 1 To Largo ' Comenzamos a recorrer la cadena
                Caracter = Mid(xCorreo, Indice, 1) ' Vamos tomando car�cter a car�cter
                ' Con lo que sigue comprobamos si el caracter est�
                ' en el rango A-Z , a-z , 0-9 (caracter aceptable tipo A - Alfanumerico)

                If (Caracter >= "a" And Caracter <= "z") Or (Caracter >= "A" And Caracter <= "Z") Or (Caracter >= "0" And Caracter <= "9") Then
                    Caracter = "A"
                End If


                ' Con lo que sigue comprobamos si el caracter es
                ' _ � - (caracter aceptable tipo - : Guion alto o bajo)


                If Caracter = "-" Or Caracter = "_" Then
                    Caracter = "-"
                End If


                Select Case Caracter
                    Case "A" ' Es un caracter aceptable tipo A
                        Select Case Estado
                            Case 0
                                Estado = 1 ' Era el primer caracter del EMAIL: pasamos a estado 1
                            Case 1
                                Estado = 1 ' Caracter intermedio ..x.. antes de arroba. Seguimos en 1
                            Case 2
                                Estado = 3 ' Caracter despu�s de arroba. Pasamos a estado 3
                            Case 3
                                Estado = 3 ' Caracter en dominio. Seguimos en estado 3
                            Case 4
                                Estado = 5 ' 1er caracter en extension de dominio/subdominio. Pasamos a estado 5
                            Case 5
                                Estado = 6 ' 2� caracter en extension de dominio/subdominio. Pasamos a estado 6 
                            Case 6
                                Estado = 7 ' 3er caracter en extension de dominio/sudominio. Pasamos a estado 7
                            Case 7
                                Estado = 8 ' 4� caracter en extension de dominio/subdominio. Pasamos a estado 8
                            Case 8
                                ValidarCorreo = False ' La longitud de la extensi�n .XXXX mayor que 4 caracteres
                                Exit Function ' Estado de error
                        End Select

                    Case "-" ' Es un caracter aceptable tipo "-" 
                        Select Case Estado
                            Case 1
                                Estado = 1 ' Caracter intermedio ..-.. antes de arroba. Seguimos en 1
                            Case 3
                                Estado = 3 ' Caracter en dominio. Seguimos en estado 3
                            Case Else
                                ValidarCorreo = False ' 
                                Exit Function ' Estado de error
                        End Select

                    Case "." '-----> Encuentra un punto
                        Select Case Estado
                            Case 1 ' Como lo anterior eran caracteres y puntos
                                Estado = 0 ' pasamos a estado inicial (espera un caracter)
                            Case 3 ' Lo anterior era una arroba y texto
                                Estado = 4 ' Pasamos a estado 4 (extension .com, .net, .shop, .info ...)
                            Case 6 ' Estaba en una extensi�n de dos letras como .ac y encontramos otro punto, para ir a algo como .ac.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n)
                            Case 7 ' Estaba en una extensi�n de tres letras como .ucr y encontramos otro punto, para ir a algo como .ucr.ac.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n)
                            Case 8 ' Estaba en una extensi�n de cuatro letras como .info y encontramos otro punto, para ir a algo como .info.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n).
                            Case Else ' Encontr� un punto despu�s de la arroba o al comienzo de la cadena
                                ValidarCorreo = False ' o antes de la arroba
                                Exit Function ' Estado de error
                        End Select

                    Case "@" '-----> Encuentra una arroba
                        Select Case Estado
                            Case 1 ' Si lo anterior eran caracteres y puntos,
                                Estado = 2 ' pasamos a estado 2
                            Case Else ' Si lo anterior era algo distinto
                                ValidarCorreo = False ' Estado de error
                                Exit Function
                        End Select
                        ' -----> Encuentra un caracter "raro"

                    Case Else ' Caracter inaceptable para email. Ej: * : !
                        ValidarCorreo = False ' Estado de error
                        Exit Function
                End Select
            Next Indice ' -----> Fin de comprobaci�n de cadena


            If (Estado = 6) Or (Estado = 7) Or (Estado = 8) Then ' El aut�mata termin� en un estado final
                ValidarCorreo = True ' Estado final: email correcto
            Else
                ValidarCorreo = False ' No era un estado final: email incorrecto
            End If
        End If
    End Function

    Public Function valida_caracteres(ByVal strcadena As String, ByVal inicio As Integer, ByVal final As Integer, ByVal inttipo As Integer) As String
        Dim i As Integer = 0
        Dim Caracter As String = ""
        For i = 1 To final
            Caracter = Mid(strcadena, inicio, 1)
            If inttipo = 1 Then
                If Not Caracter = "&" Then
                    If Not ((Caracter >= "a" And Caracter <= "z") Or (Caracter >= "A" And Caracter <= "Z")) Then
                        Caracter = "El RFC es incorrecto en las tres primeras letras, Verifiquelo"
                        Exit For
                    End If
                End If
            Else
                If Not (Caracter >= "0" And Caracter <= "9") Then
                    Caracter = "El RFC es incorrecto en los seis digitos, Verifiquelo"
                    Exit For
                End If
            End If
            Caracter = ""
            inicio = inicio + 1
        Next
        Return Caracter
    End Function
End Class
