Imports CD_Datos
Public Class CnCatalogo
    Dim cnegocio As New CdCatalogo
    Public contrasena As String = ""
    Private iIdApp As Integer = 8

    Public Function menu_administrador_padre(IdMarca As Integer) As DataTable
        Return cnegocio.menu_administrador_padre(IdMarca)
    End Function

    Public Function menu_administrador_hijo(ByVal ancestro As Integer) As DataTable
        Return cnegocio.menu_administrador_hijo(ancestro)
    End Function

    Public Function menu_administrador_subhijo(ByVal ancestro As Integer, ByVal hijo As Integer, _
    ByVal subhijo As Integer) As DataTable
        Return cnegocio.menu_administrador_subhijo(ancestro, hijo, subhijo)
    End Function

    Public Function cbo_corporativo_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
        Return cnegocio.cbo_corporativo_usuario(intusuario, intnivel)
    End Function

    Public Function cbo_corporativo_usuario_bid(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
        Return cnegocio.cbo_corporativo_usuario_bid(intusuario, intnivel)
    End Function

    Public Function inserta_usuario(ByVal intnivel As Integer, ByVal strlogin As String, _
    ByVal strcontrasena As String, ByVal strestatus As String, _
    ByVal stremail As String, ByVal intbid As Integer, _
    ByVal intusuario As Integer, ByVal strregistro As Integer, _
    ByVal strnombre As String) As Integer
        Dim valor As Integer = 1
        Dim Usuario As Integer
        Dim Nivel As Integer
        Try
            If valida_Usuario(strlogin, strcontrasena, intusuario, intbid) = 0 Then
                Usuario = cnegocio.inserta_usuario(intnivel, strlogin, _
                strcontrasena, strestatus, stremail, _
                intbid, strregistro, strnombre)
                valor = 0
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_Usuario(ByVal strlogin As String, _
    ByVal strpass As String, ByVal intusuario As Integer, _
    ByVal intbid As Integer) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.valida_Usuario(strlogin, strpass)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("usuario") Then
                    valor = dt.Rows(0)("usuario")
                End If
            End If
            Dim dtl As DataTable = cnegocio.valida_login(strlogin)
            If dtl.Rows.Count > 0 Then
                If Not dtl.Rows(0).IsNull("usuario") Then
                    valor = dtl.Rows(0)("usuario")
                End If
            End If
            If valor > 0 Then
                Dim dt1 As DataTable = cnegocio.valida_Usuario_corporativo(intusuario, intbid)
                If dt1.Rows.Count > 0 Then
                    If Not dt1.Rows(0).IsNull("usuario") Then
                        valor = dt1.Rows(0)("usuario")
                    End If
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtiene_idusuario() As DataTable
        Return cnegocio.obtiene_idusuario()
    End Function

    Public Sub modifica_usuario(ByVal intusuario As Integer, _
    ByVal intnivel As Integer, ByVal strlogin As String, _
    ByVal strestatus As String, ByVal stremail As String, _
    ByVal strregistro As Integer, ByVal strnombre As String, ByVal strIdEmpleado As String, ByVal iTypeAccount As Integer)
        cnegocio.modifica_usuario(intusuario, intnivel, _
        strlogin, strestatus, stremail, strregistro, strnombre, strIdEmpleado, iTypeAccount)
    End Sub

    Public Function listado_usuario(ByVal intbid As Integer, ByVal intbandera As Integer) As DataTable
        Return cnegocio.listado_usuario(intbid, intbandera)
    End Function

    Public Function buscar_usuario_grd(ByVal strUsuario As String, ByVal intbid As Integer, ByVal intnivel As Integer) As DataTable
        Return cnegocio.buscar_usuario_grd(strUsuario, intbid, intnivel)
    End Function

    Public Function carga_niveles() As DataTable
        Return cnegocio.carga_niveles
    End Function

    Public Function carga_datos_usuario(ByVal intusuario) As DataTable
        Return cnegocio.carga_datos_usuario(intusuario)
    End Function

    Public Function Carga_corporativo_usuario(ByVal intusuario As Integer) As DataTable
        Return cnegocio.Carga_corporativo_usuario(intusuario)
    End Function

    Public Function cuenta_corporativo_usuario(ByVal intusuario As Integer, ByVal intbid As Integer) As Integer
        Dim valor As Int16 = 0
        Try
            Dim dt As DataTable = cnegocio.cuenta_corporativo_usuario(intusuario, intbid)
            If dt.Rows.Count > 0 Then
                valor = dt.Rows.Count
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub inserta_usuario_corporativo(ByVal intusuario As Integer, ByVal intcorporativo As Integer)
        cnegocio.inserta_usuario_corporativo(intusuario, intcorporativo)
    End Sub

    Public Function carga_grd_bid() As DataTable
        Return cnegocio.carga_grd_bid
    End Function

    Public Function carga_marca() As DataTable
        Return cnegocio.carga_marca
    End Function

    Public Function carga_datos_distribuidor(IdBid As Integer) As DataTable
        Return cnegocio.carga_datos_distribuidor(IdBid)
    End Function

    Public Function inserta_bid(ByVal intbidC As Integer, ByVal strbidf As String, ByVal strbida As String, _
    ByVal strempresa As String, ByVal strestatus As String, ByVal intmarca As Integer) As Integer
        Return cnegocio.inserta_bid(intbidC, strbidf, strbida, strempresa, strestatus, intmarca)
    End Function

    Public Sub actualiza_bid(ByVal intbid As Integer, ByVal intbidC As Integer, ByVal strbidf As String, ByVal strbida As String, _
    ByVal strempresa As String, ByVal strestatus As String, ByVal intmarca As Integer)
        cnegocio.actualiza_bid(intbid, intbidC, strbidf, strbida, strempresa, strestatus, intmarca)
    End Sub

    Public Function numero_bitacora_max() As Integer
        Dim valor As Integer
        Dim dt As DataTable = cnegocio.numero_bitacora_max
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull(0) Then
                    valor = dt.Rows(0)(0)
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_tablas(ByVal intbitacora As Integer) As DataTable
        Return cnegocio.carga_tablas(intbitacora)
    End Function

    Public Sub inserta_bitacora_tabla(ByVal intbitacora As Integer, ByVal inttabla As Integer, ByVal strurl As String)
        cnegocio.inserta_bitacora_tabla(intbitacora, inttabla, strurl)
    End Sub

    Public Function carga_tb_bitacora_script(ByVal intbitacora As Integer) As DataTable
        Return cnegocio.carga_tb_bitacora_script(intbitacora)
    End Function

    Public Function carga_grd_bid_user(ByVal intusuario As Integer) As DataTable
        Return cnegocio.carga_grd_bid_user(intusuario)
    End Function

    Public Function valida_contrasena(ByVal intusuario As Integer)
        Dim dt As DataTable = cnegocio.valida_contrasena(intusuario)
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("contrasena") Then
                contrasena = dt.Rows(0)("contrasena")
            End If
        End If
    End Function

    Public Sub Actualiza_contrasena(ByVal intusuario As Integer, ByVal strNuevoPass As String)
        cnegocio.Actualiza_contrasena(intusuario, strNuevoPass)
    End Sub

    Public Function carga_grd_usuarios(ByVal intnivel As Integer, ByVal intbid As Integer, ByVal strnombre As String, ByVal intPrograma As Integer) As DataTable
        Return cnegocio.carga_grd_usuarios(intnivel, intbid, strnombre, intPrograma)
    End Function

    Public Function carga_grd_bid_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intMarca As Integer) As DataTable
        Return cnegocio.carga_grd_bid_usuario(intusuario, intnivel, intMarca)
    End Function

    Public Function carga_grd_programa(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intMarca As Integer) As DataTable
        Return cnegocio.carga_grd_programa(intusuario, intnivel, intMarca)
    End Function

    Public Function carga_datos_usuario_niveles(ByVal intnivel As Integer) As DataTable
        Return cnegocio.carga_datos_usuario_niveles(intnivel)
    End Function

    Public Function valida_datos_login_nombre(ByVal strlogin As String, ByVal strnombre As String) As Integer
        Return cnegocio.valida_datos_login_nombre(strlogin, strnombre)
    End Function

    Public Function Creacion_Pass_temp(ByVal strnombre As String) As String
        Dim cadena As String = ""
        Dim redondeo As String = ""
        Try
            cadena = Mid(strnombre, 1, 4)
            cadena = cadena.Trim
            redondeo = Mid(Math.Round(Rnd(), 2), 3, 2)
            cadena = redondeo.Trim & cadena & "779N"
            If cnegocio.valida_contrasena(cadena) = 1 Then
                'existe la contraseņa
                cadena = Creacion_Pass_temp(strnombre & "1")
            End If
            Return cadena
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_datos_usuario(ByVal intnivel As Integer, ByVal strlogin As String, ByVal strencriptado As String, _
    ByVal strestatus As String, ByVal strbandera As String, ByVal stremail As String, ByVal strnombre As String, ByVal strIdEmpleado As String, ByVal iTypeAccount As Integer) As Integer
        Return cnegocio.inserta_datos_usuario(intnivel, strlogin, strencriptado, strestatus, strbandera, stremail, strnombre, strIdEmpleado, iTypeAccount)
    End Function

    Public Sub inserta_bid_programa_usuario(ByVal intusuario As Integer, ByVal strprog As String, ByVal strbid As String)
        cnegocio.inserta_bid_programa_usuario(intusuario, strprog, strbid)
    End Sub

    Public Function carga_usuario_bid_detalle(ByVal intusuario As Integer, ByVal intprograma As Integer) As DataTable
        Return cnegocio.carga_usuario_bid_detalle(intusuario, intprograma)
    End Function

    Public Sub elimina_programa_usuario(ByVal intusuario As Integer, ByVal intprograma As Integer)
        cnegocio.elimina_programa_usuario(intusuario, intprograma)
    End Sub

    Public Sub elimina_bid_usuario(ByVal intusuario As Integer, ByVal intprograma As Integer, ByVal intbid As Integer)
        cnegocio.elimina_bid_usuario(intusuario, intprograma, intbid)
    End Sub

    Public Function carga_usuarios() As DataTable
        Return cnegocio.carga_usuarios
    End Function

    Public Sub Actualiza_masivo_excriptasion(ByVal strcadena As String)
        cnegocio.Actualiza_masivo_excriptasion(strcadena)
    End Sub

    Public Function cargar_estados() As DataTable
        Return cnegocio.cargar_estados
    End Function

    Public Function carga_grd_codigopostal(ByVal strCodigo As String) As DataTable
        Return cnegocio.carga_grd_codigopostal(strCodigo)
    End Function

    Public Function inserta_codigopostal(ByVal intestado As Integer, ByVal strestado As String, _
        ByVal strcodigopostal As String, ByVal strprovincia As String, _
        ByVal strmunicipio As String, ByVal strcolonia As String, _
        ByVal strpctiva As String, ByVal strestatus As String) As Integer
        Return cnegocio.inserta_codigopostal(intestado, strestado, strcodigopostal, _
        strprovincia, strmunicipio, strcolonia, strpctiva, strestatus)
    End Function


    Public Function actualiza_codigopostal(ByVal intiva As Integer, _
        ByVal intestado As Integer, ByVal strestado As String, _
        ByVal strcodigopostal As String, ByVal strprovincia As String, _
        ByVal strmunicipio As String, ByVal strcolonia As String, _
        ByVal strpctiva As String, ByVal strestatus As String) As Integer
        Return cnegocio.actualiza_codigopostal(intiva, intestado, strestado, strcodigopostal, _
        strprovincia, strmunicipio, strcolonia, strpctiva, strestatus)
    End Function

    Public Function carga_aseguradoras() As DataTable
        Return cnegocio.carga_aseguradoras()
    End Function

    Public Function carga_tablas() As DataTable
        Return cnegocio.carga_tablas()
    End Function

    Public Function obtener_MaxArchivo(ByVal intUsuario As String) As DataTable
        Return cnegocio.obtener_maxArchivo(intUsuario)
    End Function

    Public Function obtener_Archivo(ByVal intBitacora As Integer, ByVal intUsuario As Integer) As DataTable
        Return cnegocio.obtener_Archivo(intBitacora, intUsuario)
    End Function

    '20160615  MAVILA
    Public Function Valida_Password(ByVal strcontrasena As String) As Boolean
        Dim resultado As Boolean = False
        Dim ws As New WSUsers.Users
        If Convert.ToBoolean(ws.ValidatePassword(strcontrasena, iIdApp).Result) Then
            resultado = True
        End If
        Return resultado
    End Function
    '20160615  MAVILA
    Public Function Encripta(ByVal newPass As String) As String
        Dim resultado As String = ""
        Dim ws As New WSUsers.Users
        resultado = ws.EncryptArray(newPass, iIdApp).Result.ToString

        Return resultado
    End Function
    '20160615  MAVILA
    Public Function Cambiar_Password(ByVal userID As String, ByVal oldPass As String, ByVal newPass As String) As Boolean
        Dim resultado As Boolean = False
        Dim ws As New WSUsers.Users
        If Convert.ToBoolean(ws.ChangePassword(userID, oldPass, newPass, iIdApp).Result) Then
            resultado = True
        End If
        Return resultado
    End Function
End Class
