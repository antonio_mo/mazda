Imports CD_Datos
Public Class CnPrincipal
    Dim cnegocio As New CdPrincipal
    Public BanInicio As Integer = 0
    Public usuario As String = ""
    Public nivel As String = ""
    Public niveles As String
    Public NomNivel As String = ""
    Public Empresa As String = ""
    Public Bid As String
    Public contador As Integer = 0
    Public Region As String = ""
    Public idRegion As String = 0
    Public UsurarioRegistro As Integer = 0
    Public corporativo_administrador As Integer = 0
    Public intmarca As Integer = 0
    Public Distribuidor_Bid As String = ""
    Public BidConauto As String = ""
    Public banCss As Integer = 0
    Public programa As Integer = 0

    Public pestado As String = ""
    Public pciudad As String = ""

    Public iIdApp As Integer = 8
    Public noempleado As String = ""


#Region "Propiedades"
    Private _id As Integer
    Private _inicio As Integer
    Private _a As Integer
    Private _b As Integer
    Private _c As Integer
    Private _d As Integer
    Private _v As Integer
    Private _w As Integer
    Private _x As Integer
    Private _y As Integer
    Private _z As Integer

#End Region

#Region "Setters y Getters"
    Public Property id()
        Get
            Return _id
        End Get
        Set(ByVal Value)
            _id = Value
        End Set
    End Property

    Public Property inicio()
        Get
            Return _inicio
        End Get
        Set(ByVal Value)
            _inicio = Value
        End Set
    End Property

    Public Property a()
        Get
            Return _a
        End Get
        Set(ByVal Value)
            _a = Value
        End Set
    End Property

    Public Property b()
        Get
            Return _b
        End Get
        Set(ByVal Value)
            _b = Value
        End Set
    End Property

    Public Property c()
        Get
            Return _c
        End Get
        Set(ByVal Value)
            _c = Value
        End Set
    End Property

    Public Property d()
        Get
            Return _d
        End Get
        Set(ByVal Value)
            _d = Value
        End Set
    End Property

    Public Property v()
        Get
            Return _v
        End Get
        Set(ByVal Value)
            _v = Value
        End Set
    End Property
    Public Property w()
        Get
            Return _w
        End Get
        Set(ByVal Value)
            _w = Value
        End Set
    End Property
    Public Property x()
        Get
            Return _x
        End Get
        Set(ByVal Value)
            _x = Value
        End Set
    End Property
    Public Property y()
        Get
            Return _y
        End Get
        Set(ByVal Value)
            _y = Value
        End Set
    End Property
    Public Property z()
        Get
            Return _z
        End Get
        Set(ByVal Value)
            _z = Value
        End Set
    End Property

#End Region

    Public Function Algoritmo()
        Dim i As Integer
        Dim Codigo(8) As Integer
        Dim Resultado As String

        Codigo(0) = v
        Codigo(1) = w
        Codigo(2) = x
        Codigo(3) = y
        Codigo(4) = z

        Call BuscaMayor()

        For i = 4 To 1 Step -1
            If Codigo(inicio) - i >= 0 Then
                Codigo(inicio + 4) = Codigo(inicio) - i
            Else
                Codigo(inicio + 4) = 0
            End If
            inicio = inicio - 1
            If inicio < 1 Then
                inicio = 4
            End If
        Next i
        For i = 0 To 8
            Resultado = Resultado & Codigo(i)
        Next i
        a = Codigo(5)
        b = Codigo(6)
        c = Codigo(7)
        d = Codigo(8)
        Return Resultado
    End Function

    Public Sub BuscaMayor()
        If ((z >= y) And (z >= x) And (z >= w)) Then
            inicio = 4
        Else
            If ((y >= x) And (y >= w)) Then
                inicio = 3
            Else
                If (x > w) Then
                    inicio = 2
                Else
                    inicio = 1
                End If
            End If
        End If
    End Sub

    Public Function ordena_algoritmo() As DataTable
        Return cnegocio.ordena_algoritmo()
    End Function

    Public Function TraerMaxalgoritmo(ByVal intid As Integer) As DataTable
        Return cnegocio.TraerMaxalgoritmo(intid)
    End Function

    Public Function maximo_valor() As DataTable
        Return cnegocio.maximo_valor()
    End Function

    Public Function actualiza_valor(ByVal a As Integer, ByVal b As Integer, ByVal c As Integer, ByVal d As Integer, ByVal id As Integer, ByVal intid As Integer)
        cnegocio.actualiza_valor(a, b, c, d, id, intid)
    End Function

    Public Function poliza_valor(ByVal intpoliza As Integer) As DataTable
        Return cnegocio.poliza_valor(intpoliza)
    End Function

    'Private _vida As String = ""
    'Private _desempleo As String = ""

    'Public Property vida() As String
    '    Get
    '        Return _vida
    '    End Get
    '    Set(ByVal value As String)
    '        _vida = value
    '    End Set
    'End Property

    'Public Property desempleo() As String
    '    Get
    '        Return Me._desempleo
    '    End Get
    '    Set(ByVal value As String)
    '        Me._desempleo = value
    '    End Set
    'End Property

    Public Function valida_cadena(ByVal cadena As String, Optional ByVal banMay As Integer = 0, Optional ByVal bancaresp As String = "") As String
        cadena = Replace(cadena, "%", " ")
        'cadena = Replace(cadena, "&", " ")
        'cadena = Replace(cadena, "|", " ")
        cadena = Replace(cadena, "'", " ")
        cadena = Replace(cadena, "+", " ")
        cadena = Replace(cadena, """", " ")
        cadena = Replace(cadena, "*", " ")
        cadena = Replace(cadena, "�", " ")
        cadena = Replace(cadena, "=", " ")
        cadena = Trim(cadena)
        If Not bancaresp = "" Then
            cadena = Replace(cadena, bancaresp, "")
        End If
        If banMay = 1 Then
            cadena = UCase(cadena)
        End If
        Return cadena
    End Function

    Public Function Validar_Estado(ByVal Codigo As String) As String
        Dim Estado As String
        Select Case Codigo
            Case "MXDF0132"
                Estado = "M�xico"
            Case "MXJO0043"
                Estado = "Jalisco"
            Case "MXCA0022"
                Estado = "Chihuahua"
            Case "MXNL0068"
                Estado = "Nuevo Le�n"
            Case "MXPA0070"
                Estado = "Puebla"
            Case "MXTS0103"
                Estado = "Tamaulipas"
            Case "MXSP0079"
                Estado = "San Luis Potos�"
            Case "MXYN2357"
                Estado = "Quintana Roo"
            Case "MXSR0091"
                Estado = "Sonora"
            Case "MXCL0013"
                Estado = "Coahuila"
            Case "MXSA0080"
                Estado = "Sinaloa"
            Case "MXDO0030"
                Estado = "Durango"
            Case "MXZS0123"
                Estado = "Zacatecas"
            Case "MXNT0065"
                Estado = "Nayarit"
            Case "MXAS0002"
                Estado = "Aguascalientes"
            Case "MXGO0033"
                Estado = "Guanajuato"
            Case "MXQA0072"
                Estado = "Quer�taro"
            Case "MXHO0134"
                Estado = "Hidalgo"
            Case "MXMO0055"
                Estado = "Estado de M�xico"
            Case "MXCM0015"
                Estado = "Colima"
            Case "MXMN0059"
                Estado = "Michoac�n"
            Case "MXGR0150"
                Estado = "Guerrero"
            Case "MXTA0108"
                Estado = "Tlaxcala"
            Case "MXMS0064"
                Estado = "Morelos"
            Case "MXOA0069"
                Estado = "Oaxaca"
            Case "MXVZ0116"
                Estado = "Veracruz"
            Case "MXTO0152"
                Estado = "Tabasco"
            Case "MXCS0020"
                Estado = "Chiapas"
            Case "MXCE0008"
                Estado = "Campeche"
            Case "MXYN0117"
                Estado = "Yucat�n"
            Case "MXBC0004"
                Estado = "Baja California Norte"
            Case "MXBS0006"
                Estado = "Baja California Sur"
        End Select
        Return Estado
    End Function

    Public Function Login_acceso(ByVal strlogin As String, ByVal strcontrasena As String)
        Dim dt As DataTable = cnegocio.Login_acceso(strlogin, strcontrasena)
        Try
            If dt.Rows.Count > 0 Then
                BanInicio = 1
                If dt.Rows(0).IsNull("id_usuario") Then
                    usuario = ""
                Else
                    usuario = dt.Rows(0)("id_usuario")
                End If
                If dt.Rows(0).IsNull("id_nivel") Then
                    nivel = 0
                Else
                    nivel = dt.Rows(0)("id_nivel")
                End If
                If dt.Rows(0).IsNull("niveles") Then
                    niveles = ""
                Else
                    niveles = dt.Rows(0)("niveles")
                End If
                If dt.Rows(0).IsNull("empresa") Then
                    Distribuidor_Bid = ""
                Else
                    Distribuidor_Bid = dt.Rows(0)("empresa")
                End If
                If dt.Rows(0).IsNull("nombre") Then
                    Empresa = ""
                Else
                    Empresa = dt.Rows(0)("nombre")
                End If
                If dt.Rows(0).IsNull("id_Bid") Then
                    Bid = ""
                Else
                    Bid = dt.Rows(0)("id_Bid")
                End If
                If dt.Rows(0).IsNull("descripcion_nivel") Then
                    NomNivel = ""
                Else
                    NomNivel = dt.Rows(0)("descripcion_nivel")
                End If
                If dt.Rows(0).IsNull("descripcion_region") Then
                    Region = ""
                Else
                    Region = dt.Rows(0)("descripcion_region")
                End If
                'If dt.Rows(0).IsNull("id_region") Then
                '    idRegion = ""
                'Else
                '    idRegion = dt.Rows(0)("id_region")
                'End If
                If Not dt.Rows(0).IsNull("Bandera_Registro") Then
                    UsurarioRegistro = dt.Rows(0)("Bandera_Registro")
                Else
                    UsurarioRegistro = 0
                End If
                If Not dt.Rows(0).IsNull("id_marca") Then
                    intmarca = dt.Rows(0)("id_marca")
                Else
                    intmarca = 0
                End If
                If Not dt.Rows(0).IsNull("bandera_css") Then
                    banCss = dt.Rows(0)("bandera_css")
                Else
                    banCss = 0
                End If
                If Not dt.Rows(0).IsNull("id_programa") Then
                    programa = dt.Rows(0)("id_programa")
                Else
                    programa = 0
                End If
                If Not dt.Rows(0).IsNull("Bid_conauto") Then
                    BidConauto = dt.Rows(0)("Bid_conauto")
                Else
                    BidConauto = ""
                End If
                If Not dt.Rows(0).IsNull("id_empleado") Then
                    noempleado = dt.Rows(0)("id_empleado")
                Else
                    noempleado = ""
                End If
                contador = dt.Rows.Count
            Else
                BanInicio = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Login_acceso_admin(ByVal intusuario As Integer, ByVal intbid As Integer)
        Dim dt As DataTable = cnegocio.Login_acceso_admin(intusuario, intbid)
        Try
            If dt.Rows.Count > 0 Then
                BanInicio = 1
                If dt.Rows(0).IsNull("id_usuario") Then
                    usuario = ""
                Else
                    usuario = dt.Rows(0)("id_usuario")
                End If
                If dt.Rows(0).IsNull("id_nivel") Then
                    nivel = ""
                Else
                    nivel = dt.Rows(0)("id_nivel")
                End If
                If dt.Rows(0).IsNull("niveles") Then
                    niveles = ""
                Else
                    niveles = dt.Rows(0)("niveles")
                End If
                'If dt.Rows(0).IsNull("empresa") Then
                '    Empresa = ""
                'Else
                '    Empresa = dt.Rows(0)("empresa")
                'End If
                If dt.Rows(0).IsNull("nombre") Then
                    Empresa = ""
                Else
                    Empresa = dt.Rows(0)("nombre")
                End If
                If dt.Rows(0).IsNull("id_Bid") Then
                    Bid = ""
                Else
                    Bid = dt.Rows(0)("id_Bid")
                End If
                If dt.Rows(0).IsNull("descripcion_nivel") Then
                    NomNivel = ""
                Else
                    NomNivel = dt.Rows(0)("descripcion_nivel")
                End If
                If dt.Rows(0).IsNull("descripcion_region") Then
                    Region = ""
                Else
                    Region = dt.Rows(0)("descripcion_region")
                End If
                'If dt.Rows(0).IsNull("id_region") Then
                '    idRegion = ""
                'Else
                '    idRegion = dt.Rows(0)("id_region")
                'End If
                If Not dt.Rows(0).IsNull("Bandera_Registro") Then
                    UsurarioRegistro = dt.Rows(0)("Bandera_Registro")
                Else
                    UsurarioRegistro = 0
                End If
                If Not dt.Rows(0).IsNull("id_marca") Then
                    intmarca = dt.Rows(0)("id_marca")
                Else
                    intmarca = 0
                End If
                If Not dt.Rows(0).IsNull("bandera_css") Then
                    banCss = dt.Rows(0)("bandera_css")
                Else
                    banCss = 0
                End If
                If Not dt.Rows(0).IsNull("id_programa") Then
                    programa = dt.Rows(0)("id_programa")
                Else
                    programa = 0
                End If
                If Not dt.Rows(0).IsNull("Bid_conauto") Then
                    BidConauto = dt.Rows(0)("Bid_conauto")
                Else
                    BidConauto = ""
                End If
                If Not dt.Rows(0).IsNull("id_empleado") Then
                    noempleado = dt.Rows(0)("id_empleado")
                Else
                    noempleado = ""
                End If
                contador = dt.Rows.Count
            Else
                BanInicio = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function registra_accesos_negados(ByVal strlogin As String) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.registra_accesos_negados(strlogin)
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("usuario") > 0 Then
                valor = 1
            End If
        End If
        Return valor
    End Function

    Public Sub Update_Usuario_Bloqueado(ByVal strlogin As String)
        cnegocio.Update_Usuario_Bloqueado(strlogin)
    End Sub

    Public Function menu_portada(ByVal strtipo As Char, ByVal intcontador As Integer, ByVal intprograma As Integer, ByVal intnivel As Integer, ByVal iIdNivel As Integer) As DataTable
        Return cnegocio.menu_portada(strtipo, intcontador, intprograma, intnivel, iIdNivel)
    End Function

    Public Function PermisosMenu(ByVal iNivel As Integer, ByVal iPantalla As Integer) As Boolean
        Return (cnegocio.PermisosMenu(iNivel, iPantalla).Rows.Count > 0)
    End Function

    Public Function poliza_inicio() As String
        Dim cadena As String = ""
        Dim dt As DataTable = cnegocio.poliza_inicio
        If dt.Rows.Count > 0 Then
            cadena = dt.Rows(0)("poliza")
        End If
        Return cadena
    End Function

    Public Function carga_version(ByVal intprograma As Integer) As Integer
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.carga_version(intprograma)
        If dt.Rows.Count > 0 Then
            valor = dt.Rows(0)("id_version")
        End If
        Return valor
    End Function

    Public Function cp(ByVal scp As String) As DataTable
        Dim dt As DataTable = cnegocio.creando_cp(scp)
        If dt.Rows.Count > 0 Then
            Dim dte As DataTable = cnegocio.carga_estado(dt.Rows(0)("id_estado"))
            If dte.Rows.Count > 0 Then
                If Not dte.Rows(0).IsNull("NOMBRE_estado") Then
                    pestado = dte.Rows(0)("NOMBRE_estado")
                End If
            End If

            Dim dtc As DataTable = cnegocio.carga_ciudad(dt.Rows(0)("id_ciudad"), dt.Rows(0)("id_estado"))
            If dtc.Rows.Count > 0 Then
                If Not dtc.Rows(0).IsNull("nombre_ciudad") Then
                    pciudad = dtc.Rows(0)("nombre_ciudad")
                End If
            End If
        End If
        Return cnegocio.cargando_cp(scp)
    End Function

    Public Function valida_login(ByVal intusuario As Integer) As DataTable
        Try
            Dim dt As DataTable = cnegocio.valida_login(intusuario)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("id_bid") Then
                    corporativo_administrador = dt.Rows(0)("id_bid")
                Else
                    corporativo_administrador = 0
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_bid_admin(ByVal intusuario As Integer) As DataTable
        Return cnegocio.carga_bid_admin(intusuario)
    End Function

    Public Function inserta_contenedor_reportes(ByVal strcadena As String) As String
        Return cnegocio.inserta_contenedor_reportes(strcadena)
    End Function

    Public Function Consulta_contenedor(ByVal llave As Integer) As DataTable
        Return cnegocio.Consulta_contenedor(llave)
    End Function

    Public Function Consulta_contenedor_script(ByVal llave As String) As Integer
        Dim i As Integer = 0
        Dim valor As Integer = 0
        Dim dt As DataTable = cnegocio.Consulta_contenedor_script(llave)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                valor = dt.Rows(i)("estatus")
            Next
        End If
        Return valor
    End Function

    Public Function Consulta_pinta_descarga(ByVal llave As String, ByVal intprograma As Integer) As DataTable
        Return cnegocio.Consulta_pinta_descarga(llave, intprograma)
    End Function

    Public Function carga_marca_admin(ByVal intusuario As Integer, ByVal intprograma As Integer) As DataTable
        Return cnegocio.carga_marca_admin(intusuario, intprograma)
    End Function

    Public Function carga_distribuidor_admin(ByVal intusuario As Integer, ByVal intmarca As Integer, ByVal intprograma As Integer) As DataTable
        Return cnegocio.carga_distribuidor_admin(intusuario, intmarca, intprograma)
    End Function

    Public Sub InsertaBanderaUsuario(ByVal intusuario As Integer)
        cnegocio.InsertaBanderaUsuario(intusuario)
    End Sub

    'mazda
    Public Function fecha_servidor() As Integer
        Dim bandera As Integer = 0
        Dim dt As DataTable = cnegocio.fecha_servidor()
        If dt.Rows.Count > 0 Then
            bandera = dt.Rows(0)("bandera")
        End If
        Return bandera
    End Function

    Public Function valida_accesso(ByVal strlogin As String, ByVal strpass As String, intMarca As Integer) As DataTable
        Return cnegocio.valida_accesso(strlogin, strpass, intMarca)
    End Function

    Public Function pinta_menu_usuario(ByVal intusuario As Integer) As DataTable
        Return cnegocio.pinta_menu_usuario(intusuario)
    End Function

    Public Function determina_bandera_programa(ByVal intprograma As Integer) As DataTable
        Return cnegocio.determina_bandera_programa(intprograma)
    End Function

    Public Function programas_interface_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
        Return cnegocio.programas_interface_usuario(intusuario, intnivel)
    End Function


    Public Function ValidarCorreo(ByVal xCorreo As String) As Boolean
        Dim Indice
        Dim Caracter
        Dim Largo
        Dim Estado

        ValidarCorreo = False


        If xCorreo <> "" Then

            Largo = Len(xCorreo)

            Estado = 0 ' Estado inicial del aut�mata

            For Indice = 1 To Largo ' Comenzamos a recorrer la cadena
                Caracter = Mid(xCorreo, Indice, 1) ' Vamos tomando car�cter a car�cter
                ' Con lo que sigue comprobamos si el caracter est�
                ' en el rango A-Z , a-z , 0-9 (caracter aceptable tipo A - Alfanumerico)

                If (Caracter >= "a" And Caracter <= "z") Or (Caracter >= "A" And Caracter <= "Z") Or (Caracter >= "0" And Caracter <= "9") Then
                    Caracter = "A"
                End If


                ' Con lo que sigue comprobamos si el caracter es
                ' _ � - (caracter aceptable tipo - : Guion alto o bajo)


                If Caracter = "-" Or Caracter = "_" Then
                    Caracter = "-"
                End If


                Select Case Caracter
                    Case "A" ' Es un caracter aceptable tipo A
                        Select Case Estado
                            Case 0
                                Estado = 1 ' Era el primer caracter del EMAIL: pasamos a estado 1
                            Case 1
                                Estado = 1 ' Caracter intermedio ..x.. antes de arroba. Seguimos en 1
                            Case 2
                                Estado = 3 ' Caracter despu�s de arroba. Pasamos a estado 3
                            Case 3
                                Estado = 3 ' Caracter en dominio. Seguimos en estado 3
                            Case 4
                                Estado = 5 ' 1er caracter en extension de dominio/subdominio. Pasamos a estado 5
                            Case 5
                                Estado = 6 ' 2� caracter en extension de dominio/subdominio. Pasamos a estado 6 
                            Case 6
                                Estado = 7 ' 3er caracter en extension de dominio/sudominio. Pasamos a estado 7
                            Case 7
                                Estado = 8 ' 4� caracter en extension de dominio/subdominio. Pasamos a estado 8
                            Case 8
                                ValidarCorreo = False ' La longitud de la extensi�n .XXXX mayor que 4 caracteres
                                Exit Function ' Estado de error
                        End Select

                    Case "-" ' Es un caracter aceptable tipo "-" 
                        Select Case Estado
                            Case 1
                                Estado = 1 ' Caracter intermedio ..-.. antes de arroba. Seguimos en 1
                            Case 3
                                Estado = 3 ' Caracter en dominio. Seguimos en estado 3
                            Case Else
                                ValidarCorreo = False ' 
                                Exit Function ' Estado de error
                        End Select

                    Case "." '-----> Encuentra un punto
                        Select Case Estado
                            Case 1 ' Como lo anterior eran caracteres y puntos
                                Estado = 0 ' pasamos a estado inicial (espera un caracter)
                            Case 3 ' Lo anterior era una arroba y texto
                                Estado = 4 ' Pasamos a estado 4 (extension .com, .net, .shop, .info ...)
                            Case 6 ' Estaba en una extensi�n de dos letras como .ac y encontramos otro punto, para ir a algo como .ac.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n)
                            Case 7 ' Estaba en una extensi�n de tres letras como .ucr y encontramos otro punto, para ir a algo como .ucr.ac.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n)
                            Case 8 ' Estaba en una extensi�n de cuatro letras como .info y encontramos otro punto, para ir a algo como .info.cr
                                Estado = 4 ' Pasamos a estado 4 (para esperar otra extensi�n).
                            Case Else ' Encontr� un punto despu�s de la arroba o al comienzo de la cadena
                                ValidarCorreo = False ' o antes de la arroba
                                Exit Function ' Estado de error
                        End Select

                    Case "@" '-----> Encuentra una arroba
                        Select Case Estado
                            Case 1 ' Si lo anterior eran caracteres y puntos,
                                Estado = 2 ' pasamos a estado 2
                            Case Else ' Si lo anterior era algo distinto
                                ValidarCorreo = False ' Estado de error
                                Exit Function
                        End Select
                        ' -----> Encuentra un caracter "raro"

                    Case Else ' Caracter inaceptable para email. Ej: * : !
                        ValidarCorreo = False ' Estado de error
                        Exit Function
                End Select
            Next Indice ' -----> Fin de comprobaci�n de cadena


            If (Estado = 6) Or (Estado = 7) Or (Estado = 8) Then ' El aut�mata termin� en un estado final
                ValidarCorreo = True ' Estado final: email correcto
            Else
                ValidarCorreo = False ' No era un estado final: email incorrecto
            End If
        End If
    End Function

    Public Function programas_Reporte_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
        Return cnegocio.programas_Reporte_usuario(intusuario, intnivel)
    End Function

    Public Function inserta_contenedor(ByVal intprograma As Integer) As String
        Return cnegocio.inserta_contenedor(intprograma)
    End Function

    Public Function ImpReporte(ByVal intllave As Integer) As DataTable
        Return cnegocio.ImpReporte(intllave)
    End Function

    'evi 12/12/2013 se agrega datos cliente cotizacion conauto
    Public Function CargaDatosClienteCotiza(ByVal IdCotizacion As Long) As DataTable
        Return cnegocio.CargaDatosClienteCotiza(IdCotizacion)
    End Function

    Public Sub ActualizaCotizacionCliente(ByVal StrCliente As String, ByVal StrSerie As String, ByVal StrObs As String, ByVal IdCotizacion As Long)
        cnegocio.ActualizaCotizacionCliente(StrCliente, StrSerie, StrObs, IdCotizacion)
    End Sub

    Public Function UrlCotizacion(ByVal IdCotizacion As Long) As DataTable
        Return cnegocio.UrlCotizacion(IdCotizacion)
    End Function

    Public Function ValidaCotizacion(ByVal IdCotizacion As Long, ByVal IdBid As Integer, ByVal IdPrograma As Integer) As DataTable
        Return cnegocio.ValidaCotizacion(IdCotizacion, IdBid, IdPrograma)
    End Function

    Public Function SubRamoPanel(ByVal IdCotizacion As Long) As DataTable
        Return cnegocio.SubRamoPanel(IdCotizacion)
    End Function

    Public Function BuscaCotizacion(ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable
        Return cnegocio.BuscaCotizacion(intprograma, StrCadena, StrCriterios)
    End Function

    'HM 13/02/2014 agregando leyenda administrador
    Public Sub ConfirmaCotizacionAdmin(ByVal idusuario As Integer, ByVal IdCotizacion As Integer, ByVal StrObs As String)
        cnegocio.ConfirmaCotizacionAdmin(idusuario, IdCotizacion, StrObs)
    End Sub

    Public Function carga_bid() As DataTable
        Return cnegocio.carga_bid()
    End Function

    Public Function carga_region() As DataTable
        Return cnegocio.carga_region()
    End Function

    Public Sub inserta_bid_region(ByVal strprog As String, ByVal intbid As Integer, ByVal intregion As Integer, Region As String, Factor As Integer, CteFactor As Integer)
        cnegocio.inserta_bid_region(strprog, intbid, intregion, Region, Factor, CteFactor)
    End Sub

    Public Function CargaGridBidRegion(intBid As Integer, intRegion As Integer, strprog As String) As DataTable
        Return cnegocio.CargaGridBidRegion(intBid, intRegion, strprog)
    End Function

    Public Function VerificaInformacionBid(intBid As Integer, intBidFord As Integer, strBidAon As String) As DataTable
        Return cnegocio.VerificaInformacionBid(intBid, intBidFord, strBidAon)
    End Function

    Public Function programas(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intmarca As Integer) As DataTable
        Return cnegocio.programas(intusuario, intnivel, intmarca)
    End Function

    Public Function valida_nuevoacceso(ByVal intusuario As String, ByVal strpass As String) As DataTable
        Return cnegocio.valida_nuevoacceso(intusuario, strpass)
    End Function

    '20160615  MAVILA
    Public Function Valida_Password(ByVal strcontrasena As String) As Boolean
        Dim resultado As Boolean = False
        Dim ws As New WSUsers.Users
        If Convert.ToBoolean(ws.ValidatePassword(strcontrasena, iIdApp).Result) Then
            resultado = True
        End If
        Return resultado
    End Function

    '20160615  MAVILA
    Public Function Encripta(ByVal newPass As String) As String
        Dim resultado As String = ""
        Dim ws As New WSUsers.Users
        resultado = ws.EncryptArray(newPass, iIdApp).Result.ToString

        Return resultado
    End Function

    '20160615  MAVILA
    Public Function Verifica_Vigencia_Password(ByVal idUsuario As Integer) As Boolean
        Dim resultado As Boolean = False

        If cnegocio.Verifica_Vigencia_Password(idUsuario) Then
            resultado = True
        End If
        Return resultado
    End Function

    Public Function Cargar_InformacionMarca(ByVal IdMarca As Integer) As DataTable
        Return cnegocio.Cargar_InformacionMarca(IdMarca)
    End Function

    Public Function Cargar_InformacionPrograma(ByVal IdPrograma As Integer) As DataTable
        Return cnegocio.Cargar_InformacionPrograma(IdPrograma)
    End Function

End Class
