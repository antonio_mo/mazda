Public Class S_CnValidacion

    Public Function QuitaCaracteres(ByVal StrValor As String, Optional ByVal Mayusculas As Boolean = True, Optional ByVal CaracterEspecial As String = "", Optional ByVal Reemplaza As Boolean = True) As String
        If Reemplaza = True Then
            StrValor = Replace(StrValor, "%", "")
            StrValor = Replace(StrValor, "&", " ")
            StrValor = Replace(StrValor, "'", "")
            StrValor = Replace(StrValor, "+", " ")
            StrValor = Replace(StrValor, """", " ")
            StrValor = Replace(StrValor, "*", " ")
            StrValor = Replace(StrValor, "�", " ")
            StrValor = Replace(StrValor, "=", " ")
            StrValor = Replace(StrValor, "<", " ")
            StrValor = Replace(StrValor, ">", " ")
            StrValor = Replace(StrValor, ">", " ")
        End If

        If CaracterEspecial <> "" Then
            If CaracterEspecial = "||" Then
                StrValor = Replace(StrValor, CaracterEspecial, "'")
            Else
                If CaracterEspecial = "'" Then
                    StrValor = Replace(StrValor, CaracterEspecial, "||")
                Else
                    StrValor = Replace(StrValor, CaracterEspecial, " ")
                End If
            End If
        End If

        StrValor = Trim(StrValor)
        If Mayusculas = True Then
            StrValor = UCase(StrValor)
        End If
        Return StrValor
    End Function

End Class
