Imports System.Security.Cryptography
Imports System.Text
Imports CD_Datos
Public Class S_CnPrincipal
    Private cnegocio As New CD_Datos.S_CdPrincipal
    Public BanInicio As Integer = 0
    Public IdUsuario As Integer = 0
    Public usuario As String = ""
    Public perfil As String = ""
    Public tipoestatus As Integer = 0
    Public niveles As String
    Public NomNivel As String = ""
    Public Empresa As String = ""
    Public EmailUsuario As String = ""
    Public Bid As String
    Public contador As Integer = 0
    Public Region As String = ""
    Public Quejas As String = 0
    Public Acceso As String = ""
    Public idRegion As String = 0
    Public UsurarioRegistro As Integer = 0
    Public corporativo_administrador As Integer = 0
    Public intmarca As Integer = 0
    Public Distribuidor_Bid As String = ""
    Public banCss As Integer = 0
    Public programa As Integer = 0
    Public nombreusuario As String = ""
    Public aseguradora As String = String.Empty
    'Public rutaArchivo As String = "C:\Inetpub\wwwroot\Daimler_SolicitudesQA\Archivos\"
    'Public rutaReporte As String = "C:\Inetpub\wwwroot\Daimler_SolicitudesQA\Reportes\"
    'Public rutaExe As String = "D:\HTMLTools\HTMLTools.exe"

    Public rutaArchivo As String = System.Configuration.ConfigurationSettings.AppSettings("Raiz") & "\Archivos\"
    Public rutaReporte As String = System.Configuration.ConfigurationSettings.AppSettings("Raiz") & "\Reportes\"
    Public rutaExe As String = System.Configuration.ConfigurationSettings.AppSettings("rutaHtmlTools")
    Public rutaAcceso As String = System.Configuration.ConfigurationSettings.AppSettings("rutaAcceso")

    Public Function valida_acceso(ByVal strlogin As String) As DataTable
        Return cnegocio.valida_acceso(strlogin)
    End Function

    Public Function Login_acceso(ByVal strlogin As String)
        Dim dt As DataTable = cnegocio.Login_acceso(strlogin)
        Try
            If dt.Rows.Count > 0 Then
                BanInicio = 1
                If dt.Rows(0).IsNull("id_usuariosolicitud") Then
                    IdUsuario = ""
                Else
                    IdUsuario = dt.Rows(0)("id_usuariosolicitud")
                End If
                If dt.Rows(0).IsNull("username") Then
                    usuario = ""
                Else
                    usuario = dt.Rows(0)("username")
                End If
                If dt.Rows(0).IsNull("nombre_usuario") Then
                    nombreusuario = ""
                Else
                    nombreusuario = dt.Rows(0)("nombre_usuario")
                End If
                If dt.Rows(0).IsNull("perfil") Then
                    perfil = 0
                Else
                    perfil = dt.Rows(0)("perfil")
                End If
                If dt.Rows(0).IsNull("tipoestatus") Then
                    tipoestatus = 0
                Else
                    tipoestatus = dt.Rows(0)("tipoestatus")
                End If
                If dt.Rows(0).IsNull("mail") Then
                    EmailUsuario = 0
                Else
                    EmailUsuario = dt.Rows(0)("mail")
                End If
                If dt.Rows(0).IsNull("Quejas") Then
                    Quejas = 0
                Else
                    Quejas = dt.Rows(0)("Quejas")
                End If
                If dt.Rows(0).IsNull("Acceso") Then
                    Acceso = 0
                Else
                    Acceso = dt.Rows(0)("Acceso")
                End If
                If Not dt.Rows(0).IsNull("aseguradora") Then
                    aseguradora = dt.Rows(0)("aseguradora")
                End If
                contador = dt.Rows.Count
            Else
                BanInicio = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function autentificaUsuario(ByVal user As String, ByVal password As String) As Hashtable
        Dim dt As DataTable
        Dim valoresInicoSesion As Hashtable = New Hashtable
        Try
            ''realizar encriptacion de password
            password = Me.generarClaveSHA1(password)
            dt = cnegocio.autentificaUsuario(user, password)

            ''validar que los datos de usuario tienen valor
            If (dt.Rows.Count = 1) Then
                If (Not dt.Rows(0).IsNull("id_Usuario") And Not dt.Rows(0).IsNull("Login")) Then
                    ''cargar en sesion datos de usuario
                    valoresInicoSesion.Add("IdUser", dt.Rows(0)("id_Usuario"))
                    valoresInicoSesion.Add("NombreUser", dt.Rows(0)("Nombre"))
                    valoresInicoSesion.Add("Login", dt.Rows(0)("Login"))
                    valoresInicoSesion.Add("Email", dt.Rows(0)("Email"))
                    valoresInicoSesion.Add("Empresa", dt.Rows(0)("Empresa"))
                    valoresInicoSesion.Add("IdPerfil", dt.Rows(0)("Id_Perfil"))
                    valoresInicoSesion.Add("Perfil", dt.Rows(0)("Descripcion"))
                    If (dt.Rows(0)("Consulta") = "1") Then
                        valoresInicoSesion.Add("Consulta", True)
                    Else
                        valoresInicoSesion.Add("Consulta", False)
                    End If
                    If (dt.Rows(0)("Cancela") = "1") Then
                        valoresInicoSesion.Add("Cancela", True)
                    Else
                        valoresInicoSesion.Add("Cancela", False)
                    End If
                    If (dt.Rows(0)("VistoBueno") = "1") Then
                        valoresInicoSesion.Add("VistoBueno", True)
                    Else
                        valoresInicoSesion.Add("VistoBueno", False)
                    End If
                    If (dt.Rows(0)("Administracion") = "1") Then
                        valoresInicoSesion.Add("Administracion", True)
                    Else
                        valoresInicoSesion.Add("Administracion", False)
                    End If
                Else
                    valoresInicoSesion.Add("Error", "Usuario con errores de informacion")
                End If
            Else
                'valoresInicoSesion.Add("Error", "Usuario con errores de informacion")
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return valoresInicoSesion

    End Function

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos d�gitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()
    End Function

    Public Function Agencias(ByVal IdUsuario As Integer) As DataTable
        Return cnegocio.Agencias(IdUsuario)
    End Function

    Public Function TodasAgencias() As DataTable
        Return cnegocio.TodasAgencias()
    End Function

    Public Function Guardar_Archivos(ByVal IdArchivoSolicitud As Integer, ByVal NombreArchivo As String, _
    ByVal RutaArchivo As String, ByVal IdUsuario As Integer, ByVal IdSolicitud As Integer)
        cnegocio.Guardar_Archivos(IdArchivoSolicitud, NombreArchivo, RutaArchivo, IdUsuario, IdSolicitud)
    End Function

    Public Function Actualiza_Archivo(ByVal IdArchivoSolicitud As Integer, ByVal IdSolicitud As Integer)
        cnegocio.Actualiza_Archivo(IdArchivoSolicitud, IdSolicitud)
    End Function

    Public Function Eliminar_Archivos(ByVal IdSolicitud As Integer, ByVal IdArchivo As Integer)
        cnegocio.Eliminar_Archivos(IdSolicitud, IdArchivo)
    End Function

    Public Function Obtiene_IdArchivoSolicitud(ByVal IdSolicitud As Integer) As DataTable
        Return cnegocio.Obtiene_IdArchivoSolicitud(IdSolicitud)
    End Function

    Public Function Obtiene_Archivos(ByVal IdArchivoSolicitud As Integer, ByVal IdSolicitud As Integer) As DataTable
        Return cnegocio.Obtiene_Archivos(IdArchivoSolicitud, IdSolicitud)
    End Function

    Public Function Eliminar_TablaTemporal(ByVal IdSesion As String)
        cnegocio.Eliminar_TablaTemporal(IdSesion)
    End Function

    Public Function Inserta_InformacionTemporal(ByVal datSolicitud As Integer, ByVal datTipoSolicitud As String, _
    ByVal datPoliza As String, ByVal datSerie As String, ByVal datContrato As String, ByVal datCliente As String, _
    ByVal datAseguradora As String, ByVal datFECSOLICITADO As String, ByVal datStatus As String, _
    ByVal datFechaStatus As String, ByVal datUSUARIOSOLICITANTE As String, ByVal datNSSTATUS As String, _
    ByVal datFECCERRADO As String, ByVal datNSPENDIENTE As String, ByVal datNSTRAMITE As String, _
    ByVal datNSCERRADO As String, ByVal datFECPENDIENTE As String, ByVal datFECTRAMITE As String, _
    ByVal datFECCANCELADO As String, ByVal datDETALLE As String, ByVal datSLA As String, _
    ByVal datConformeSLA As String, ByVal datColor As String, ByVal IdSesion As String, _
    ByVal datDistribuidor As String, ByVal datNuevaPoliza As String, ByVal datBandera As String)
        cnegocio.Inserta_InformacionTemporal(datSolicitud, datTipoSolicitud, _
    datPoliza, datSerie, datContrato, datCliente, datAseguradora, datFECSOLICITADO, datStatus, _
     datFechaStatus, datUSUARIOSOLICITANTE, datNSSTATUS, datFECCERRADO, datNSPENDIENTE, datNSTRAMITE, _
     datNSCERRADO, datFECPENDIENTE, datFECTRAMITE, datFECCANCELADO, datDETALLE, datSLA, datConformeSLA, _
     datColor, IdSesion, datDistribuidor, datNuevaPoliza, datBandera)
    End Function

    Public Function Obtiene_IdReporte() As DataTable
        Return cnegocio.Obtiene_IdReporte()
    End Function

    Public Function Verifica_Reporte(ByVal IdReporte As Integer) As DataTable
        Return cnegocio.Verifica_Reporte(IdReporte)
    End Function

    Public Function Obtiene_UsuarioBid(ByVal IdUsuario As Integer) As DataTable
        Return cnegocio.Obtiene_UsuarioBid(IdUsuario)
    End Function

    Public Function Actualiza_Bandera(ByVal IdSolicitud As Integer, ByVal Bandera As String)
        cnegocio.Actualiza_Bandera(IdSolicitud, Bandera)
    End Function

    Public Function Actualiza_NuevaPoliza(ByVal IdSolicitud As Integer, ByVal NuevaPoliza As String)
        cnegocio.Actualiza_NuevaPoliza(IdSolicitud, NuevaPoliza)
    End Function

    Public Function IniciaSesion(ByVal IdUsuario)
        cnegocio.IniciaSesion(IdUsuario)
    End Function
    Public Function TerminaSesion(ByVal IdUsuario)
        cnegocio.TerminaSesion(IdUsuario)
    End Function
End Class
