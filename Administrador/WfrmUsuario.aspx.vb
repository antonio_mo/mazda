Imports CN_Negocios
Imports System.Security.Cryptography
Imports System.Text
Imports System.Web.Mail
Imports Cotizador_Mazda_Retail.WsCorreo

'Ambiente DRP
'http://192.168.166.14/MailServiceAON/MailSMTP.asmx

'Ambiente PROD
'http://192.168.66.233/MailServiceAON/MailSMTP.asmx

'Ambiente QA
'http://192.168.166.192/MailServiceAON/MailSMTP.asmx 

'Ambiente DEV
'http://192.168.166.193/MailServiceAON/MailSMTP.asmx 

Partial Class WfrmUsuario
    Inherits System.Web.UI.Page

    Private ccliente As New CnCatalogo
    Private cvalida As New CnPrincipal

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("cvebid") Is Nothing Then
                    ccliente.elimina_bid_usuario(Request.QueryString("cveuser"), _
                    Request.QueryString("cveprog"), _
                    Request.QueryString("cvebid"))
                    ViewState("idusuario") = Request.QueryString("cveuser")
                    desactiva_pnl(False)
                    A2.Style("BACKGROUND-POSITION") = "0% -150px"
                    B2.Style("BACKGROUND-POSITION") = "100% -150px"
                    header1.Visible = True
                    pnl_bid.Visible = True
                    carga_datos_grd()
                    carga_grd_usuario()
                Else
                    ViewState("idusuario") = 0
                    desactiva_pnl(False)
                    SetFocus(txt_buscar)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub EstablecerColor()
        A1.Style.Remove("BACKGROUND-POSITION")
        B1.Style.Remove("BACKGROUND-POSITION")
        A2.Style.Remove("BACKGROUND-POSITION")
        B2.Style.Remove("BACKGROUND-POSITION")
    End Sub

    Public Sub desactiva_pnl(ByVal ban As Boolean)
        pnl_listado.Visible = ban
        pnl_bid.Visible = ban
        pnl_usuario.Visible = ban
        header1.Visible = ban
        cmd_regenera.Visible = ban
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Public Function carga_niveles()
        Try
            Dim dt As DataTable = ccliente.carga_datos_usuario_niveles(Session("nivel"))
            If dt.Rows.Count > 0 Then
                cbo_nivel.DataSource = dt
                cbo_nivel.DataTextField = "descripcion_nivel"
                cbo_nivel.DataValueField = "id_nivel"
                cbo_nivel.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub cmd_Buscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Buscar.Click
        Try
            If txt_buscar.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el nombre del usuario a buscar")
                Exit Sub
            End If
            Dim dt As DataTable = ccliente.carga_grd_usuarios(Session("nivel"), Session("bid"), cvalida.valida_cadena(txt_buscar.Text), Session("IdMarca"))
            If dt.Rows.Count > 0 Then
                grdUbicacion.DataSource = dt
                grdUbicacion.DataBind()
                grdUbicacion_fondo.DataSource = dt
                grdUbicacion_fondo.DataBind()
                desactiva_pnl(False)
                pnl_listado.Visible = True
                SetFocus(txt_buscar)
            Else
                desactiva_pnl(False)
                MsgBox.ShowMessage("No existe informaci�n que mostrar")
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_nuevo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_nuevo.Click
        desactiva_pnl(False)
        header1.Visible = True
        pnl_usuario.Visible = True
        carga_niveles()
        EstablecerColor()
        A1.Style("BACKGROUND-POSITION") = "0% -150px"
        B1.Style("BACKGROUND-POSITION") = "100% -150px"
        cmd_regenera.Visible = False
        If viewstate("idusuario") = 0 Then
            cmd_regenera.Visible = False
        Else
            cmd_regenera.Visible = True
        End If
    End Sub

    Private Sub A1_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A1.ServerClick
        desactiva_pnl(False)
        header1.Visible = True
        EstablecerColor()
        A1.Style("BACKGROUND-POSITION") = "0% -150px"
        B1.Style("BACKGROUND-POSITION") = "100% -150px"
        pnl_usuario.Visible = True
        SetFocus(txtlogin)
        If viewstate("idusuario") = 0 Then
            cmd_regenera.Visible = False
        Else
            cmd_regenera.Visible = True
        End If
    End Sub

    Private Sub A2_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A2.ServerClick
        EstablecerColor()
        desactiva_pnl(False)
        header1.Visible = True
        If viewstate("idusuario") > 0 Then
            A2.Style("BACKGROUND-POSITION") = "0% -150px"
            B2.Style("BACKGROUND-POSITION") = "100% -150px"
            pnl_bid.Visible = True
            carga_datos_grd()
            carga_grd_usuario()
        Else
            A1.Style("BACKGROUND-POSITION") = "0% -150px"
            B1.Style("BACKGROUND-POSITION") = "100% -150px"
            MsgBox.ShowMessage("Favor de ingresar los datos del cliente para poder continuar")
            pnl_usuario.Visible = True
        End If
    End Sub

    Public Sub carga_datos_grd()
        Dim dt As DataTable
        Try
            dt = ccliente.carga_grd_bid_usuario(Session("usuario"), Session("nivel"), Session("IdMarca"))
            If dt.Rows.Count > 0 Then
                grd_bid.DataSource = dt
                grd_bid.DataBind()
                grdfondo_bid.DataSource = dt
                grdfondo_bid.DataBind()
                pnl_dbid.Visible = True
                lbl_bid.Text = "Seleccionar todo"
                cmd_bid.ImageUrl = "..\Imagenes\menu\caja_box.jpg"
            Else
                pnl_dbid.Visible = False
            End If

            dt = Nothing
            dt = ccliente.carga_grd_programa(Session("usuario"), Session("nivel"), Session("IdMarca"))
            If dt.Rows.Count > 0 Then
                grd_prog.DataSource = dt
                grd_prog.DataBind()
                grdfondo_prog.DataSource = dt
                grdfondo_prog.DataBind()
                pnl_dprograma.Visible = True
                lbl_prog.Text = "Seleccionar todo"
                cmd_prog.ImageUrl = "..\Imagenes\menu\caja_box.jpg"
            Else
                pnl_dprograma.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_cancelar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_cancelar.Click
        desactiva_pnl(False)
        pnl_listado.Visible = True
        SetFocus(txt_buscar)
    End Sub

    Private Sub cmd_guardar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_guardar.Click
        'Dim strpass As String = ""
        Dim encripta As String = ""
        Try
            'Login
            If txtlogin.Text = "" Then
                MsgBox.ShowMessage("El login del usuario no puede quedar en blanco, verifiquelo")
                Exit Sub
            Else
                txtlogin.Text = cvalida.valida_cadena(txtlogin.Text)
            End If

            'Nombre del usuario
            If txtnombre.Text = "" Then
                MsgBox.ShowMessage("El nombre del usuario no puede quedar en blanco, verifiquelo")
                Exit Sub
            Else
                If txtnombre.Text.Length <= 5 Then
                    MsgBox.ShowMessage("El n�mero m�nimo de car�cteres para el nombre es de 5, verifiquelo")
                    Exit Sub
                Else
                    txtnombre.Text = cvalida.valida_cadena(txtnombre.Text, 1, "&")
                End If
            End If

            If Txt_Correo.Text = "" Then
                MsgBox.ShowMessage("El correo electr�nico no puede quedar en blanco, verifiquelo")
                Exit Sub
            Else
                Txt_Correo.Text = cvalida.valida_cadena(Txt_Correo.Text)
            End If

            If cbo_nivel.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el nivel del usuario")
                Exit Sub
            End If

            'si no esta seleccionado el de bloqueo en automatico se agrega falta confirmar
            If cbo_bloqueo.SelectedValue = "P" Then
                cbo_bloqueo.SelectedIndex = cbo_bloqueo.Items.IndexOf(cbo_bloqueo.Items.FindByValue(0))
            End If

            'Estatus del usuario
            If cbo_estatus.SelectedValue = "P" Then
                cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(1))
            End If

            If viewstate("idusuario") = 0 Then
                'inserta
                Select Case ccliente.valida_datos_login_nombre(txtlogin.Text, txtnombre.Text)
                    Case 1
                        MsgBox.ShowMessage("El login ya esta registrado, favor de cambiarlo")
                        Exit Sub
                    Case 2
                        MsgBox.ShowMessage("El nombre de usuario ya esta registrado, favor de verificarlo")
                        Exit Sub
                End Select

                If (cbo_bloqueo.SelectedValue = cbo_bloqueo.Items.IndexOf(cbo_bloqueo.Items.FindByValue(0))) Then
                    txt_password.Text = New CN_Negocios.CnPrincipal().valida_cadena(UCase(ccliente.Creacion_Pass_temp(txtnombre.Text)))
                    encripta = generarClaveSHA1(txt_password.Text)
                ElseIf (txt_password.Text = String.Empty) Then
                    Dim strPassCode As String = System.Web.Security.Membership.GeneratePassword(12, 2)
                    txt_password.Text = UCase(New CN_Negocios.CnPrincipal().valida_cadena(strPassCode))
                    encripta = generarClaveSHA1(txt_password.Text)
                Else
                    encripta = txt_password.Text
                End If
                'encripta = ccliente.Encripta(txt_password.Text)

                ViewState("idusuario") = ccliente.inserta_datos_usuario(cbo_nivel.SelectedValue, txtlogin.Text, encripta, _
                    cbo_estatus.SelectedValue, cbo_bloqueo.SelectedValue, Txt_Correo.Text, _
                    txtnombre.Text, txtIdEmpleado.Text, IIf(chkAdmSol.Checked, 1, 0))
                If ViewState("idusuario") <= 0 Then
                    '0 = no existe
                    '1 = si existe
                    MsgBox.ShowMessage("No se puede registrar el usuario, verifique que la informaci�n se correcta")
                    txtPassword.Text = ""
                    Exit Sub
                Else
                    Dim I As Integer = 0
                    Enviacorreo(txt_password.Text)
                    MsgBox.ShowMessage("Se registraron correctamente los datos del usuario")
                    txtPassword.Text = ""
                    For I = 0 To txt_password.Text.Length - 1
                        txtPassword.Text = txtPassword.Text & "X"
                    Next
                End If


            Else
                'modifica
                ccliente.modifica_usuario(ViewState("idusuario"), cbo_nivel.SelectedValue, _
                cvalida.valida_cadena(txtlogin.Text), cbo_estatus.SelectedValue, _
                cvalida.valida_cadena(Txt_Correo.Text), cbo_bloqueo.SelectedValue, _
                cvalida.valida_cadena(txtnombre.Text), txtIdEmpleado.Text, IIf(chkAdmSol.Checked, 1, 0))

                If (cbo_bloqueo.SelectedValue = cbo_bloqueo.Items.FindByValue(0).Value) Then
                    txt_password.Text = New CN_Negocios.CnPrincipal().valida_cadena(UCase(ccliente.Creacion_Pass_temp(txtnombre.Text)))
                    encripta = generarClaveSHA1(txt_password.Text)
                    ccliente.Actualiza_contrasena(ViewState("idusuario"), encripta)
                    Enviacorreo(txt_password.Text)
                End If

                MsgBox.ShowMessage("Se actualiz� correctamente los datos del usuario")
            End If
            cmd_regenera.Visible = True

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos d�gitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()
    End Function

    Private Sub Enviacorreo(ByVal strpass As String)
        Dim Msubject As String = ""
        Dim Mbody As String = ""
        Dim Mailpara As String = ""
        Dim Mailcc As String = ""
        Dim Mailbcc As String = ""
        Dim strFrom As String = ""
        Dim objMail = New WsCorreo.Service1SoapClient

        Try

            If Session("IdMarca") = 1 Then
                strFrom = "Emisor de P�lizas Conauto mx.emisorconauto@aon.com"
            ElseIf Session("IdMarca") = 5 Then
                strFrom = "Emisor de P�lizas Mazda mx.emisorconauto@aon.com"
            End If

            If Not Txt_Correo.Text = "" Then
                Mailpara = Txt_Correo.Text
            End If

            Msubject = "Informaci�n del cotizador web del registro de usuario :"

            Mbody = " Por motivos de seguridad se le envia por medio de este correo electr�nico"
            Mbody = Mbody & " <br> Los datos de accesos  "
            Mbody = Mbody & " <br> "
            Mbody = Mbody & " <br> Usuario : <b>" & txtlogin.Text & "</b>"
            Mbody = Mbody & " <br> "
            Mbody = Mbody & " <br> Contrase�a : <b>" & strpass & "</b>"
            Mbody = Mbody & " <br>"
            Mbody = Mbody & " <br> Favor de no responder a este correo, si tiene dudas o comentarios "
            Mbody = Mbody & " <br> correspondientes a los datos de su usuario, favor de consultar "
            Mbody = Mbody & " <br> a su administrador"
            Mbody = Mbody & " <br> Contacto Email : mailto:" & Session("CorreoAdministrador")
            Mbody = Mbody & " <br> Visita: www.aon.com"
            Mbody = Mbody & " <br> Aon..."
            Mbody = Mbody & " <br> La seguridad de un gran respaldo"
            Mbody = Mbody & " <br>"

            objMail.MailAYB(strFrom, Mailpara, Msubject, Mbody, True, Nothing, "o9its725", "emisorconauto", Mailbcc, Mailcc, "AYB")
            'InvocaEmail("aon2302", strFrom, Mailpara, "", "", Msubject, Mbody, "", 5)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Public Sub MandarMail(ByVal strpass As String)
    '    Dim mensaje As New MailMessage
    '    Dim Mailpara As String = ""
    '    Dim Mailcc As String = ""
    '    Dim Msubject As String
    '    Dim Mbody As String

    '    Dim smtpServer As String = "200.52.83.165"
    '    Dim userName As String = "emisorconauto@aonseguros.com.mx"
    '    Dim password As String = "bEe%jun5To(sooty"
    '    Dim cdoBasic As Integer = 1
    '    Dim cdoSendUsingPort As Integer = 2


    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", smtpServer)
    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25)
    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", cdoSendUsingPort)
    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", cdoBasic)
    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", userName)
    '    mensaje.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password)

    '    If Not Txt_Correo.Text = "" Then
    '        Mailpara = Txt_Correo.Text
    '    End If

    '    Try
    '        Msubject = "Informaci�n del cotizador web del registro de usuario :"

    '        Mbody = " Por motivos de seguridad se le envia por medio de este correo electr�nico"
    '        Mbody = Mbody & " <br> Los datos de accesos  "
    '        Mbody = Mbody & " <br> "
    '        Mbody = Mbody & " <br> Usuario : <b>" & txtlogin.Text & "</b>"
    '        Mbody = Mbody & " <br> "
    '        Mbody = Mbody & " <br> Contrase�a : <b>" & strpass & "</b>"
    '        Mbody = Mbody & " <br>"
    '        Mbody = Mbody & " <br> Favor de no responder a este correo, si tiene dudas o comentarios "
    '        Mbody = Mbody & " <br> correspondientes a los datos de su usuario, favor de consultar "
    '        Mbody = Mbody & " <br> a su administrador"
    '        Mbody = Mbody & " <br> Contacto Email : mailto:ugarduno@conautoit.com.mx"
    '        Mbody = Mbody & " <br> Visita: www.aon.com"
    '        Mbody = Mbody & " <br> Aon..."
    '        Mbody = Mbody & " <br> La Seguridad de un gran respaldo"
    '        Mbody = Mbody & " <br>"

    '        With mensaje
    '            .From = "Emisor de P�lizas Conauto<emisorconauto@aonseguros.com.mx>"
    '            If Mailpara <> "" Then
    '                .To = Mailpara.ToString
    '            End If
    '            If Mailcc <> "" Then
    '                .Cc = Mailcc.ToString
    '            End If
    '            .Subject = Msubject.ToString
    '            .Body = Mbody.ToString
    '            .BodyFormat = MailFormat.Html
    '            .Priority = MailPriority.High
    '        End With
    '        SmtpMail.SmtpServer = smtpServer
    '        SmtpMail.Send(mensaje)

    '        'With mensaje
    '        '    .From = "Cotizadores Web Conauto<emisorconauto@aonseguros.com.mx>"
    '        '    .To = "Emmanuell_vital@aon.com.mx"
    '        '    .Subject = Msubject.ToString & "Datos del Administrador cotizadores Ford"
    '        '    .Body = Mbody.ToString
    '        '    .BodyFormat = MailFormat.Html
    '        '    .Priority = MailPriority.High
    '        'End With
    '        'SmtpMail.SmtpServer = smtpServer
    '        'SmtpMail.Send(mensaje)

    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '    End Try
    'End Sub

    Private Sub cmd_agregar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_agregar.Click
        Dim item As DataGridItem
        Dim strcadenabid As String = ""
        Dim strcadenaprog As String = ""
        Dim i As Integer = 0
        Try
            For i = 0 To grd_bid.Items.Count - 1
                item = grd_bid.Items(i)
                Dim ckb As CheckBox = item.FindControl("cbk_grd")
                If ckb.Checked = True Then
                    If strcadenabid = "" Then
                        strcadenabid = item.Cells(0).Text.Trim
                    Else
                        strcadenabid = strcadenabid & " , " & item.Cells(0).Text.Trim
                    End If
                End If
            Next
            If strcadenabid = "" Then
                MsgBox.ShowMessage("Favor de seleccionar el distribuidor")
                Exit Sub
            End If
            For i = 0 To grd_prog.Items.Count - 1
                item = grd_prog.Items(i)
                Dim ckb As CheckBox = item.FindControl("cbk_grd")
                If ckb.Checked = True Then
                    If strcadenaprog = "" Then
                        strcadenaprog = item.Cells(0).Text.Trim
                    Else
                        strcadenaprog = strcadenaprog & " , " & item.Cells(0).Text.Trim
                    End If
                End If
            Next
            If strcadenaprog = "" Then
                MsgBox.ShowMessage("Favor de seleccionar el programa")
                Exit Sub
            End If

            ccliente.inserta_bid_programa_usuario(viewstate("idusuario"), strcadenaprog, strcadenabid)
            carga_grd_usuario()
            MsgBox.ShowMessage("Los datos se registraron correctamente")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdUbicacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdUbicacion.SelectedIndexChanged
        Try
            carga_niveles()
            carga_datos_usuario(grdUbicacion.Items(grdUbicacion.SelectedIndex).Cells(0).Text)
            carga_grd_usuario()
            desactiva_pnl(False)
            header1.Visible = True
            EstablecerColor()
            A1.Style("BACKGROUND-POSITION") = "0% -150px"
            B1.Style("BACKGROUND-POSITION") = "100% -150px"
            pnl_usuario.Visible = True
            cmd_regenera.Visible = True
            SetFocus(txtlogin)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub carga_datos_usuario(ByVal intusuario As Integer)
        Dim cuenta As Integer = 0
        Dim i As Integer = 0
        Dim dt As DataTable = ccliente.carga_datos_usuario(intusuario)
        Try
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("id_usuario") Then
                    viewstate("idusuario") = 0
                Else
                    viewstate("idusuario") = dt.Rows(0)("id_usuario")
                End If
                If dt.Rows(0).IsNull("id_nivel") Then
                    cbo_nivel.SelectedIndex = cbo_nivel.Items.IndexOf(cbo_nivel.Items.FindByValue(0))
                Else
                    cbo_nivel.SelectedIndex = cbo_nivel.Items.IndexOf(cbo_nivel.Items.FindByValue(dt.Rows(0)("id_nivel")))
                End If
                If dt.Rows(0).IsNull("login") Then
                    txtlogin.Text = ""
                Else
                    txtlogin.Text = dt.Rows(0)("login")
                End If
                'password
                If dt.Rows(0).IsNull("contrasena") Then
                    txtPassword.Text = ""
                    txt_password.Text = ""
                Else
                    txt_password.Text = dt.Rows(0)("contrasena")
                    txtPassword.Text = dt.Rows(0).Item("contrasena")
                    cuenta = txtPassword.Text.Length
                    txtPassword.Text = ""
                    'For i = 0 To cuenta - 1
                    For i = 0 To 8
                        txtPassword.Text = txtPassword.Text & "X"
                    Next
                End If
                If dt.Rows(0).IsNull("estatus_usuario") Then
                    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(0))
                Else
                    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(dt.Rows(0)("estatus_usuario").ToString().Trim()))
                End If
                If dt.Rows(0).IsNull("bandera_registro") Then
                    cbo_bloqueo.SelectedIndex = cbo_bloqueo.Items.IndexOf(cbo_bloqueo.Items.FindByValue(0))
                Else
                    cbo_bloqueo.SelectedIndex = cbo_bloqueo.Items.IndexOf(cbo_bloqueo.Items.FindByValue(dt.Rows(0)("bandera_registro").ToString().Trim()))
                End If
                If dt.Rows(0).IsNull("email") Then
                    Txt_Correo.Text = ""
                Else
                    Txt_Correo.Text = dt.Rows(0)("email")
                End If
                If dt.Rows(0).IsNull("nombre") Then
                    txtnombre.Text = ""
                Else
                    txtnombre.Text = dt.Rows(0)("nombre")
                End If
                If dt.Rows(0).IsNull("id_empleado") Then
                    txtIdEmpleado.Text = ""
                Else
                    txtIdEmpleado.Text = dt.Rows(0)("id_empleado")
                End If
                If dt.Rows(0)("TypeAccount") = 0 Then
                    chkAdmSol.Checked = False
                Else
                    chkAdmSol.Checked = True
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_bid_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_bid.Click
        Dim item As DataGridItem
        Dim i As Integer = 0
        Dim ban As Boolean
        If cmd_bid.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg" Then
            cmd_bid.ImageUrl = "..\Imagenes\menu\caja_box.jpg"
            lbl_bid.Text = "Seleccionar todo"
            ban = False
        Else
            cmd_bid.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg"
            lbl_bid.Text = "Deseleccionar todo"
            ban = True
        End If

        For i = 0 To grd_bid.Items.Count - 1
            item = grd_bid.Items(i)
            CType(item.FindControl("cbk_grd"), CheckBox).Checked = ban
        Next
    End Sub

    Private Sub cmd_prog_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_prog.Click
        Dim item As DataGridItem
        Dim i As Integer = 0
        Dim ban As Boolean
        If cmd_prog.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg" Then
            cmd_prog.ImageUrl = "..\Imagenes\menu\caja_box.jpg"
            lbl_prog.Text = "Seleccionar todo"
            ban = False
        Else
            cmd_prog.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg"
            lbl_prog.Text = "Deseleccionar todo"
            ban = True
        End If

        For i = 0 To grd_prog.Items.Count - 1
            item = grd_prog.Items(i)
            CType(item.FindControl("cbk_grd"), CheckBox).Checked = ban
        Next
    End Sub

    Private Sub grd_usuario_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_usuario.SelectedIndexChanged

    End Sub

    Private Sub grd_usuario_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_usuario.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dt As DataTable = ccliente.carga_usuario_bid_detalle(viewstate("idusuario"), e.Item.Cells(1).Text)
            If dt.Rows.Count > 0 Then
                Dim sw As New System.IO.StringWriter
                Dim htw As New System.Web.UI.HtmlTextWriter(sw)
                carga_detalle_grd(dt, e.Item.Cells(1).Text).RenderControl(htw)
                Dim DivStart As String = "<DIV id='uniquename" + e.Item.ItemIndex.ToString() + "' style='DISPLAY: none;'>"
                Dim DivBody As String = sw.ToString()
                Dim DivEnd As String = "</DIV>"
                Dim FullDIV As String = DivStart + DivBody + DivEnd
                Dim LastCellPosition As Integer = e.Item.Cells.Count - 1
                Dim NewCellPosition As Integer = e.Item.Cells.Count - 2
                e.Item.Cells(1).ID = "CellInfo" + e.Item.ItemIndex.ToString()

                If e.Item.ItemType = ListItemType.Item Then
                    e.Item.Cells(LastCellPosition).Text() = e.Item.Cells(LastCellPosition).Text + "</td><tr><td bgcolor='f5f5f5'></td><td colspan='" + NewCellPosition.ToString() + "'>" + FullDIV
                Else
                    e.Item.Cells(LastCellPosition).Text = e.Item.Cells(LastCellPosition).Text + "</td><tr><td bgcolor='d3d3d3'></td><td colspan='" + NewCellPosition.ToString() + "'>" + FullDIV
                End If
                e.Item.Cells(0).Attributes("onclick") = "HideShowPanel('uniquename" + e.Item.ItemIndex.ToString() + "'); ChangePlusMinusText('" + e.Item.Cells(0).ClientID + "');"
                e.Item.Cells(0).Attributes("onmouseover") = "this.style.cursor='pointer'"
                e.Item.Cells(0).Attributes("onmouseout") = "this.style.cursor='pointer'"

            End If
        End If
    End Sub

    Public Sub carga_grd_usuario()
        Dim dt As DataTable = ccliente.carga_grd_programa(ViewState("idusuario"), 1, 0)
        If dt.Rows.Count > 0 Then
            grd_usuario.DataSource = dt
            grd_usuario.DataBind()
            grd_usuario.Visible = True
        Else
            grd_usuario.Visible = False
        End If
    End Sub

    Public Function carga_detalle_grd(ByVal dt As DataTable, ByVal intprograma As Integer) As Table
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim tb As New Table
        Dim hp As HyperLink
        Dim i As Integer = 0
        Try
            For i = 0 To dt.Rows.Count - 1
                tbrow = New TableRow

                tbcell = New TableCell
                If dt.Rows(i).IsNull("nombre") Then
                    tbcell.Text = ""
                Else
                    tbcell.Text = dt.Rows(i)("nombre")
                End If
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Left
                tbcell.Width = Unit.Percentage(90)
                tbcell.CssClass = "negrita"
                tbrow.Controls.Add(tbcell)

                hp = New HyperLink
                tbcell = New TableCell
                hp.Text = "Eliminar"
                hp.ImageUrl = "..\Imagenes\menu\trash.bmp"
                hp.NavigateUrl = "WfrmUsuario.aspx?cvebid=" & dt.Rows(i)("id_bid") & "&cveprog=" & intprograma & "&cveuser=" & viewstate("idusuario") & ""
                hp.Target = "contenido"
                tbcell.Controls.Add(hp)
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.Width = Unit.Percentage(10)
                tbcell.CssClass = "negrita"
                tbrow.Controls.Add(tbcell)

                tb.Controls.Add(tbrow)
            Next
            tb.Width = Unit.Percentage(100)
            Return tb
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub grd_usuario_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_usuario.DeleteCommand
        ccliente.elimina_programa_usuario(viewstate("idusuario"), e.Item.Cells(1).Text)
        desactiva_pnl(False)
        A2.Style("BACKGROUND-POSITION") = "0% -150px"
        B2.Style("BACKGROUND-POSITION") = "100% -150px"
        pnl_bid.Visible = True
        carga_datos_grd()
        carga_grd_usuario()
        MsgBox.ShowMessage("El programa fue eliminado satisfactoriamente")
    End Sub
    Private Sub cmd_regenera_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regenera.Click
        Dim strpass As String = ""
        Dim i As Integer = 0
        Dim cuenta As Integer = 0
        Dim encripta As String = 0
        Try
            If viewstate("idusuario") = 0 Then
                MsgBox.ShowMessage("Es necesario que exista un usuario activo, verifiquelo")
                Exit Sub
            End If
            If Txt_Correo.Text = "" Then
                MsgBox.ShowMessage("Favor de escribir el correo electr�nico del usuario")
                Exit Sub
            End If

            'txt_password.Text = UCase(ccliente.Creacion_Pass_temp(txtnombre.Text))

            'txtPassword.Text = generarClaveSHA1(txt_password.Text)

            Dim strPassCode As String = System.Web.Security.Membership.GeneratePassword(12, 2)
            If (cbo_bloqueo.SelectedValue = cbo_bloqueo.Items.IndexOf(cbo_bloqueo.Items.FindByValue(0))) Then
                txt_password.Text = New CN_Negocios.CnPrincipal().valida_cadena(UCase(ccliente.Creacion_Pass_temp(txtnombre.Text)))
            Else
                txt_password.Text = UCase(New CN_Negocios.CnPrincipal().valida_cadena(strPassCode))
            End If

            encripta = generarClaveSHA1(txt_password.Text)
            ccliente.Actualiza_contrasena(ViewState("idusuario"), encripta)
            cuenta = strPassCode.Length
            txtPassword.Text = ""
            For i = 0 To strPassCode.Length - 1
                txtPassword.Text = txtPassword.Text & "X"
            Next
            Enviacorreo(txt_password.Text)
            MsgBox.ShowMessage("Los datos de la contrase�a se actualizaron correctamente")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
    '    Response.Redirect("WfrmRegeneraPassword.aspx")
    'End Sub

    Protected Sub Txt_Correo_TextChanged(sender As Object, e As EventArgs) Handles Txt_Correo.TextChanged

    End Sub

    Protected Sub grd_prog_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grd_prog.SelectedIndexChanged

    End Sub
End Class
