<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCatCodigoPostal.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCatCodigoPostal" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmCatCodigoPostal</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Cat�logo 
							C�digo Postal</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" style="HEIGHT: 22px" width="20%">C�digo Postal&nbsp;:</TD>
					<TD style="HEIGHT: 22px" width="25%"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_cp" runat="server" Width="30%"
							CssClass="combos_small" MaxLength="5"></asp:textbox></TD>
					<TD style="HEIGHT: 22px" width="25%"><asp:imagebutton id="cmd_Buscar" runat="server" ImageUrl="..\Imagenes\botones\buscar.gif" Width="80px"></asp:imagebutton></TD>
					<TD style="HEIGHT: 22px" width="30%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Estado&nbsp;:</TD>
					<TD><asp:dropdownlist id="cbo_estado" runat="server" Width="100%" CssClass="combos" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD></TD>
					<TD><asp:textbox id="txt_id" runat="server" Width="16px" Visible="False" Height="16px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Provincia :</TD>
					<TD colSpan="2"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_provincia" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="100"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Municipio&nbsp;:</TD>
					<TD colSpan="2"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_municipio" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="100"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Colonia :</TD>
					<TD colSpan="2"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_colonia" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="100"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Pct Iva&nbsp;:</TD>
					<TD colSpan="2"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_pctiva" runat="server"
							Width="30%" CssClass="combos_small" MaxLength="2"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD><asp:dropdownlist id="cbo_Estatus" runat="server" Width="50%" CssClass="combos" Visible="False">
							<asp:ListItem Value="P" Selected="True">--Seleccionar--</asp:ListItem>
							<asp:ListItem Value="1">Activo</asp:ListItem>
							<asp:ListItem Value="0">Inactivo</asp:ListItem>
						</asp:dropdownlist></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"><asp:imagebutton id="cmd_Guardar" runat="server" ImageUrl="..\Imagenes\botones\guardar.gif" Width="80px"></asp:imagebutton></TD>
					<TD><asp:imagebutton id="cmd_Nuevo" runat="server" ImageUrl="..\Imagenes\botones\nuevo.gif" Width="80px"></asp:imagebutton></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:panel id="pnl_datos" runat="server" Width="100%" Height="136px">
							<DIV style="OVERFLOW: hidden; WIDTH: 100.11%; HEIGHT: 248px">
								<asp:datagrid id="grdcatalogo" runat="server" CssClass="Datagrid" Width="95%" Height="104px" AllowSorting="True"
									CellPadding="1" AutoGenerateColumns="False">
									<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
									<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
									<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="id_iva"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="Id_Estado" HeaderText="IdEstado">
											<ItemStyle Wrap="False"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Nombre_Estado" HeaderText="Estado">
											<HeaderStyle Width="20%"></HeaderStyle>
											<ItemStyle Wrap="False"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Codigo_postal" HeaderText="C&#243;digo Postal">
											<HeaderStyle Width="15%"></HeaderStyle>
											<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Nombre_Provincia" HeaderText="Provincia">
											<HeaderStyle Width="20%"></HeaderStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Municipio" HeaderText="Municipio">
											<HeaderStyle Width="20%"></HeaderStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Colonia" HeaderText="Colonia">
											<HeaderStyle Width="20%"></HeaderStyle>
											<ItemStyle Wrap="False"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="Pct_Iva" HeaderText="Pct Iva"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="Estatus_Iva"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="est" HeaderText="Estatus">
											<HeaderStyle Width="5%"></HeaderStyle>
											<ItemStyle Wrap="False"></ItemStyle>
										</asp:BoundColumn>
										<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
											HeaderText="Del." CommandName="Delete"></asp:ButtonColumn>
										<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;"
											HeaderText="Sel." CommandName="Select">
											<HeaderStyle Width="5%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
										</asp:ButtonColumn>
									</Columns>
									<PagerStyle Wrap="False"></PagerStyle>
								</asp:datagrid></DIV>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; LEFT: 296px; POSITION: absolute; TOP: 584px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
