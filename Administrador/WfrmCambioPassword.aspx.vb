Imports CN_Negocios
Imports System.Security.Cryptography
Imports System.Text
Imports MsgBox


Partial Class WfrmCambioPassword
    Inherits System.Web.UI.Page

    Private ccliente As New CnCatalogo
    Private cvalida As New CnPrincipal

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session.Item("mensaje") IsNot Nothing Then
                If Session("mensaje").ToString <> "" Then
                    Dim mensaje As String = Session("mensaje").ToString
                    Session("mensaje") = ""
                    Response.Write("<script>alert('" + mensaje + "'); </script>")
                End If
            End If

            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("cveban") Is Nothing Then
                    ViewState("BanderaHome") = 1
                Else
                    ViewState("BanderaHome") = 0
                End If
                ViewState("BanderaInserta") = 0
                limpia_campos()
                SetFocus(txt_actual)
            End If
            If ViewState("BanderaHome") = 1 Then
                cmd_regresar.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia_campos()
        txt_actual.Text = ""
        txt_nuevo.Text = ""
        txt_confirma.Text = ""
    End Sub

    Public Function Solo_Output()
        Response.Redirect("administrador/HPrincipal1.htm", True)
    End Function

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Function generarClaveSHA1(ByVal nombre As String) As String
        'Crear una clave SHA1 como la generada por
        'FormsAuthentication.HashPasswordForStoringInConfigFile
        'Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
        Dim i As Integer = 0
        Dim enc As New UTF8Encoding
        Dim data() As Byte = enc.GetBytes(nombre)
        Dim result() As Byte
        Dim sha As New SHA1CryptoServiceProvider
        'This is one implementation of the abstract class SHA1.
        result = sha.ComputeHash(data)
        'Convertir los valores en hexadecimal
        'cuando tiene una cifra hay que rellenarlo con cero
        'para que siempre ocupen dos d�gitos.
        Dim sb = New StringBuilder
        For i = 0 To result.Length - 1
            If (result(i) < 16) Then
                sb.Append("0")
            End If
            sb.Append(result(i).ToString("x"))
        Next
        Return sb.ToString().ToUpper()
    End Function

    Private Sub Imaguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Imaguardar.Click
        'Dim BanderaInserta As Integer = 0
        Dim i As Integer = 0
        Dim bannum As Integer = 0
        Dim banletra As Integer = 0
        Dim encripta As String = ""
        Dim encriptaNewPass As String = ""
        Dim encriptaUsuario As String = ""
        Dim NoProgramas As Integer = 0
        Dim strUsuario As String = ""
        Dim mensaje As String = ""
        Try
            ccliente.valida_contrasena(Session("usuario"))

            If txt_actual.Text = "" Then
                mensaje = "La contrase�a actual no puede quedar en blanco, verifiquelo"
                Response.Write("<script>alert('" + mensaje + "'); </script>")
                Exit Sub
            End If

            If txt_nuevo.Text = "" Then
                mensaje = "La nueva contrase�a no puede quedar en blanco, verifiquelo"
                Response.Write("<script>alert('" + mensaje + "'); </script>")
                Exit Sub
            End If

            If txt_confirma.Text = "" Then
                mensaje = "La confirmaci�n de la nueva contrase�a no puede quedar en blanco, verifiquelo"
                Response.Write("<script>alert('" + mensaje + "'); </script>")
                Exit Sub
            End If

            If UCase(ccliente.contrasena.Trim.ToString) <> generarClaveSHA1(UCase(cvalida.valida_cadena(txt_actual.Text.Trim.ToString))) Then
                MsgBox.ShowMessage("La contrase�a que se tiene registrada no es la misma, verifiquelo")
                Exit Sub
            End If

            If UCase(txt_actual.Text.Trim.ToString) = UCase(txt_nuevo.Text.Trim.ToString) Then
                MsgBox.ShowMessage("La nueva contrase�a no puede ser la misma que la actual, verifiquelo")
                Exit Sub
            End If

            If UCase(txt_nuevo.Text.Trim.ToString) <> UCase(txt_confirma.Text.Trim.ToString) Then
                MsgBox.ShowMessage("La confirmaci�n de la nueva contrase�a no son iguales, verifiquelo")
                Exit Sub
            End If

            'If ccliente.contrasena.Trim.ToString <> ccliente.Encripta(txt_actual.Text.Trim.ToString) Then
            '    mensaje = "La contrase�a que se tiene registrada no es la misma, verifiquelo"
            '    Response.Write("<script>alert('" + mensaje + "'); </script>")
            '    Exit Sub
            'End If

            'If txt_actual.Text.Trim.ToString = txt_nuevo.Text.Trim.ToString Then
            '    mensaje = "La nueva contrase�a no puede ser la misma que la actual, verifiquelo"
            '    Response.Write("<script>alert('" + mensaje + "'); </script>")
            '    Exit Sub
            'End If

            'If txt_nuevo.Text.Trim.ToString <> txt_confirma.Text.Trim.ToString Then
            '    mensaje = "La nueva contrase�a y la confirmacion no son iguales, verifiquelo"
            '    Response.Write("<script>alert('" + mensaje + "'); </script>")
            '    Exit Sub
            'End If

            If txt_nuevo.Text.Length < 8 Then
                mensaje = "La nueva contrase�a debe de ser al menos de 8 caracteres, Verifiquelo"
                Response.Write("<script>alert('" + mensaje + "'); </script>")
                Exit Sub
            End If

            For i = 1 To txt_nuevo.Text.Length
                If IsNumeric(Mid(txt_nuevo.Text, i, 1)) Then
                    bannum = 1
                Else
                    banletra = 1
                End If
            Next

            If bannum = 0 Or banletra = 0 Then
                mensaje = "La nueva contrase�a debe de ser alfan�merica, Verifiquelo"
                Response.Write("<script>alert('" + mensaje + "'); </script>")
                Exit Sub
            End If

            'Validaci�n anterior
            If UCase(txt_nuevo.Text.Trim.ToString) = UCase(txt_confirma.Text.Trim.ToString) Then
                encripta = generarClaveSHA1(cvalida.valida_cadena(UCase(txt_nuevo.Text)))
                ccliente.Actualiza_contrasena(Session("usuario"), encripta)
                'BanderaInserta = 1
                ViewState("BanderaInserta") = 1
                MsgBox.ShowMessage("La nueva contrase�a se registr� correctamente")
            End If

            limpia_campos()

            Dim dtV As DataTable = cvalida.valida_nuevoacceso(Session("usuario"), encripta)
            If dtV.Rows.Count > 0 Then
                NoProgramas = dtV.Rows(0)("programa")
            End If

            'If BanderaInserta = 1 Then
            If ViewState("BanderaHome") = 0 Then
                If ViewState("BanderaInserta") = 1 Then
                    cvalida.InsertaBanderaUsuario(Session("usuario"))
                    If Session("UsuarioRegistroBandera") > 0 Then
                        If NoProgramas > 1 Then
                            Response.Redirect("..\Principal\wfrmmenu.aspx")
                        Else
                            If Not Session("nivel") = 3 Then
                                If Session("contadoruser") > 1 Then
                                    If Session("IdMarca") = 1 Then
                                        Response.Redirect("..\Principal\WfrmInicioadmin.aspx")
                                    Else
                                        Response.Redirect("..\Principal\HPrincipal.htm")
                                    End If
                                Else
                                    Response.Redirect("..\Principal\HPrincipal.htm")
                                End If
                            Else
                                Solo_Output()
                            End If
                        End If
                    Else
                        Response.Redirect("..\Principal\HPrincipal.htm")
                    End If
                Else
                    'Response.Write("<script>window.open('../Principal/HPrincipal.htm','_parent');</script>")
                    If Session("IdMarca") = 1 Then
                        Response.Write("<script>window.open('../Principal/frmLogin.aspx','_parent');</script>")
                    Else
                        Response.Write("<script>window.open('../Principal/frmLogin.aspx?pr=5','_parent');</script>")
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regresar.Click
        If ViewState("BanderaInserta") = 1 Then
            If Session("UsuarioRegistroBandera") > 0 Then
                If Not Session("nivel") = 3 Then
                    If Session("contadoruser") > 1 Then
                        Response.Redirect("..\Principal\WfrmInicioadmin.aspx")
                    Else
                        Response.Redirect("..\Principal\HPrincipal.htm")
                    End If
                Else
                    Solo_Output()
                End If
            Else
                Response.Redirect("..\Principal\HPrincipal.htm")
            End If
        Else
            Response.Redirect("..\Principal\frmLogin.aspx")
        End If
    End Sub

    Private Sub Imaguardar_Load(sender As Object, e As EventArgs) Handles Imaguardar.Load

    End Sub
End Class
