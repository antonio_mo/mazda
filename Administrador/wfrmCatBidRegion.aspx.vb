﻿Imports CN_Negocios
Public Class wfrmCatBidRegion
    Inherits System.Web.UI.Page
    Private ccliente As New CnPrincipal
    Private cvalida As New CnPrincipal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                carga_distribuidor()
                carga_region()
                carga_programa()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_distribuidor()
        Dim dt As DataTable = ccliente.carga_bid()
        If dt.Rows.Count > 0 Then
            cbo_distribuidor.DataSource = dt
            cbo_distribuidor.DataTextField = "empresa"
            cbo_distribuidor.DataValueField = "id_bid"
            cbo_distribuidor.DataBind()
        End If
    End Sub

    Public Sub carga_region()
        Dim dt As DataTable = ccliente.carga_region()
        If dt.Rows.Count > 0 Then
            cbo_Region.DataSource = dt
            cbo_Region.DataTextField = "region"
            cbo_Region.DataValueField = "id_region"
            cbo_Region.DataBind()
        End If
    End Sub

    Public Sub carga_programa()
        Dim dt As DataTable = ccliente.programas(Session("usuario"), Session("nivel"), Session("IdMarca"))
        If dt.Rows.Count > 0 Then
            chkLCriterio.DataSource = dt
            chkLCriterio.DataTextField = "descripcion_programa"
            chkLCriterio.DataValueField = "id_programa"
            chkLCriterio.DataBind()
        End If
    End Sub

    Protected Sub cmd_Guardar_Click(sender As Object, e As ImageClickEventArgs) Handles cm_Guardar.Click
        Try
            Dim StrCriterios As String
            Dim Seleccion As Boolean
            Dim i As Integer

            If cbo_distribuidor.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el distribuidor")
                Exit Sub
            End If

            If cbo_Region.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar la región")
                Exit Sub
            End If

            If txt_factor.Text = "" Then
                MsgBox.ShowMessage("Escriba el factor")
                Exit Sub
            End If

            If txt_ctefactor.Text = "" Then
                MsgBox.ShowMessage("Escriba la constante del factor")
                Exit Sub
            End If

            StrCriterios = ""
            For i = 0 To chkLCriterio.Items.Count - 1
                If chkLCriterio.Items(i).Selected = True Then
                    Seleccion = True
                    If StrCriterios = "" Then
                        StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value)
                    Else
                        StrCriterios = StrCriterios & "," & UCase(chkLCriterio.Items(i).Value)
                    End If
                End If
            Next

            If StrCriterios = "" And Seleccion = False Then
                MsgBox.ShowMessage("Seleccione un programa")
                Exit Sub
            End If

            'Verifica si ya existe la información del distribuidor y los programas seleccionados
            'Dim dt As DataTable = ccliente.ObtieneBidRegion(cbo_distribuidor.SelectedValue, cbo_Region.SelectedValue, StrCriterios)
            'If dt.Rows.Count > 0 Then
            '    MsgBox.ShowMessage("El distribuidor ya tiene información cargada en la región seleccionada")
            '    Exit Sub
            'End If

            ccliente.inserta_bid_region(StrCriterios, cbo_distribuidor.SelectedValue, cbo_Region.SelectedValue, cbo_Region.SelectedItem.Text, _
            cvalida.valida_cadena(txt_factor.Text), _
            cvalida.valida_cadena(txt_ctefactor.Text))
            MsgBox.ShowMessage("Los datos se registraron correctamente")

            grdCatalogo.Visible = True
            CargaGridBidRegion(cbo_distribuidor.SelectedValue, cbo_Region.SelectedValue, StrCriterios)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub cmd_Nuevo_Click(sender As Object, e As ImageClickEventArgs) Handles cmd_Nuevo.Click
        Try
            Dim i As Integer

            carga_distribuidor()
            carga_region()

            txt_factor.Text = ""
            txt_ctefactor.Text = ""

            For i = 0 To chkLCriterio.Items.Count - 1
                chkLCriterio.Items(i).Selected = False
            Next
            grdCatalogo.Visible = False

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
    Public Sub CargaGridBidRegion(IdBid As Integer, IdRegion As Integer, StrCriterios As String)
        Try
            Dim dt As DataTable = ccliente.CargaGridBidRegion(IdBid, IdRegion, StrCriterios)
            If dt.Rows.Count > 0 Then
                grdCatalogo.DataSource = dt
                grdCatalogo.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class