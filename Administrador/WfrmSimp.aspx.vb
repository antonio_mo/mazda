Imports CN_Negocios
Partial Class WfrmSimp
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Dim dt As DataTable
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Request.QueryString("url") Is Nothing Then
                    If Not Session("Bandera_Espera") Is Nothing Then
                        If ccliente.Consulta_contenedor_script(Session("Llave")) = 0 Then
                            Lnk_imprime.Visible = False
                            Response.Redirect("HEspera.htm")
                        Else
                            Lnk_imprime.Visible = True
                            If Not dt.Rows(0).IsNull("nombre_reporte") Then
                                Lnk_imprime.Text = dt.Rows(0)("nombre_reporte")
                            Else
                                Lnk_imprime.Text = ""
                            End If
                            If Not dt.Rows(0).IsNull("Url_reporte") Then
                                Lnk_imprime.NavigateUrl = "WfrmSimp.aspx?url=" & dt.Rows(0)("Url_reporte") & " &nomb=" & dt.Rows(0)("nombre_reporte") & ""
                            Else
                                Lnk_imprime.NavigateUrl = ""
                            End If
                            Lnk_imprime.Target = "contenido"
                        End If
                    End If
                Else
                    Dim url As String = Request.QueryString("url")
                    Dim nombre As String = Request.QueryString("nomb")
                    'url = "C:/Inetpub/wwwroot/CotizadorContado_Conauto/" & url
                    url = "D:/Aplicaciones/CotizadorContado_Conauto/" & url
                    sd.FileName = nombre
                    sd.File = url
                    sd.ShowDialogBox()
                    Lnk_imprime.Visible = True
                    Lnk_imprime.NavigateUrl = "WfrmSimp.aspx?urlS=" & url & " &nomb=" & nombre & ""
                    Lnk_imprime.Target = "contenido"
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
