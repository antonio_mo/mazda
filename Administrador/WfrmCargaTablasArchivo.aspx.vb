Imports System.Threading
Imports CN_Negocios
Imports CP_CargaTablas

Partial Class WfrmCargaTablasArchivo
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents MsgBox As MsgBox.MsgBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private ccliente As New CnCatalogo
    'Private cTablas As New CP_CargaTablas.clsCargaClass

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Dim Datos() As String
        Dim IdBitacora As Integer

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            Datos = Split(Session("Cadena"), "|")

            Session("Bandera") = Datos(1)
            IdBitacora = Datos(8)

            Response.Write(ArchivoGenerado(Session("Bandera"), IdBitacora))

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            Response.Flush()
            Response.End()
        End Try
    End Sub

    Private Function ArchivoGenerado(ByVal Bandera As Integer, ByVal IdBitacora As Integer) As String
        Dim strsalida As String = "Exito"

        Try
            Session("Archivo") = Nothing

            'cTablas.carga(Session("Cadena"))

Archivo:

            Dim dt As DataTable = ccliente.obtener_Archivo(IdBitacora, Session("usuario"))
            If dt.Rows.Count = 0 Then
                GoTo archivo
            Else
                If dt.Rows(0).IsNull("Estatus_Error") Then
                    GoTo Archivo
                Else
                    If Bandera = 1 Then
                        Session("Archivo") = dt.Rows(0).Item("Archivo")
                    Else
                        Session("Archivo") = dt.Rows(0).Item("ArchivoLog")
                    End If

                    Session("Estatus_Error") = dt.Rows(0).Item("Estatus_Error")
                    If Session("Estatus_Error") = "1" Then
                        strsalida = "Error"
                    End If
                End If
            End If

        Catch ex As Exception
            strsalida = "Error"
        End Try
        Return strsalida
    End Function

End Class
