Imports CN_Negocios

Partial Class WfrmConfirmaCoti
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private cvalidar As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                txtCriterio.Text = ""
                txt_obs.Text = ""
                cmd_confirmar.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        carga_consulta()
    End Sub

    Public Sub carga_consulta()
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try
            txt_obs.Text = ""
            StrCriterios = ""
            For i = 0 To chkLCriterio.Items.Count - 1
                If chkLCriterio.Items(i).Selected = True Then
                    Seleccion = True
                    StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                End If
            Next
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If

            dt = cvalidar.BuscaCotizacion(Session("programa"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                grd_reimprime.DataSource = dt
                grd_reimprime.DataBind()
                cmd_confirmar.Visible = True
            Else
                MsgBox.ShowMessage("No hay registros con esos criterios de busqueda")
                cmd_confirmar.Visible = False
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Private Sub grd_reimprime_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_reimprime.ItemDataBound
        Try
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    If Not e.Item.Cells(3).Text = "No necesita confirmaci�n" Then
                        CType(e.Item.FindControl("opt_Seleccion"), RadioButton).Attributes.Add("onclick", "javascript: RadioChecked('" + CType(e.Item.FindControl("opt_Seleccion"), RadioButton).ClientID + "');")
                    Else
                        e.Item.Cells(0).Text = ""
                    End If
            End Select
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_confirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_confirmar.Click
        Dim IdCoti As Integer = 0
        Dim item_grd As DataGridItem
        Dim i As Integer = 0
        Dim bandera As Byte = 0
        Try
            For i = 0 To grd_reimprime.Items.Count - 1
                item_grd = grd_reimprime.Items(i)
                Dim opt As RadioButton = item_grd.FindControl("opt_Seleccion")
                If opt.Checked = True Then
                    If item_grd.Cells(3).Text = "Confirmado" Then
                        IdCoti = -1
                        bandera = 1
                        Exit For
                    End If
                    IdCoti = item_grd.Cells(4).Text
                    bandera = 1
                    Exit For
                End If
            Next
            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de selccionar la cotizaci�n a confirmar")
                Exit Sub
            End If
            If txt_obs.Text = "" Then
                MsgBox.ShowMessage("Las observaciones son obligatorias, favor de ingresarlas")
                Exit Sub
            Else
                txt_obs.Text = cvalidar.valida_cadena(txt_obs.Text)
            End If

            If IdCoti = 0 Then
                MsgBox.ShowMessage("La cotizaci�n no se puede confirmar, favor de consultar a sistemas")
                Exit Sub
            Else
                If IdCoti = -1 Then
                    MsgBox.ShowMessage("La cotizaci�n ya fue confirmada previamente")
                    Exit Sub
                End If
            End If

            cvalidar.ConfirmaCotizacionAdmin(Session("usuario"), IdCoti, txt_obs.Text)

            'Evi solo en mi pc local 
            'If Session("Programa") = "32" Then
            '    ReporteDPF.GeneraConautoPromoM(IdCoti)
            'Else
            Dim _reportePDF As New CC.Negocio.Reporte.N_Cotizacion()
            _reportePDF.GeneraPromoMazda(IdCoti)
            'End If

            carga_consulta()
            txt_obs.Text = ""
            MsgBox.ShowMessage("La cotizaci�n ya fue autorizada, favor de verificarla")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
    
End Class
