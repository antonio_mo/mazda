Imports CN_Negocios

Partial Class WfrmCargaTablas
    Inherits System.Web.UI.Page

    Private ccliente As New CnCatalogo

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Dim Cadena As String

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("nombre") Is Nothing Then
                    Cadena = "D:\Aplicaciones\SetTablas\Excel\Descarga\" & Request.QueryString("nombre")
                    sd.FileName = Request.QueryString("nombre")
                    sd.File = Cadena
                    sd.ShowDialogBox()
                End If
                lblTitulo.Text = "Carga de Tablas"
                header1.Visible = True
                pnlAseguradoras.Visible = False
                pnlVersiones.Visible = False
                pnlTablas.Visible = False
                pnlArchivo.Visible = False
                cmdGenerar.Visible = False
                cmdCargar.Visible = False
                pnlObservaciones.Visible = False
                ancrArchivo.Style("visibility") = "hidden"
                lblTablas.Text = "Seleccionar todo"
            End If
            'A2.Visible = False
            'B2.Visible = False
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub A1_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A1.ServerClick
        Try
            EstablecerColor()
            A1.Style("BACKGROUND-POSITION") = "0% -150px"
            B1.Style("BACKGROUND-POSITION") = "100% -150px"
            pnlAseguradoras.Visible = True
            pnlVersiones.Visible = True
            pnlTablas.Visible = True
            pnlArchivo.Visible = False
            cmdGenerar.Visible = True
            cmdCargar.Visible = False
            ancrArchivo.Style("visibility") = "hidden"
            CargarAseguradoras()
            CargarTablas()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub A2_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A2.ServerClick
        Try
            EstablecerColor()
            A2.Style("BACKGROUND-POSITION") = "0% -150px"
            B2.Style("BACKGROUND-POSITION") = "100% -150px"
            pnlAseguradoras.Visible = False
            pnlVersiones.Visible = False
            pnlTablas.Visible = False
            pnlArchivo.Visible = True
            cmdGenerar.Visible = False
            cmdCargar.Visible = True
            ancrArchivo.Style("visibility") = "hidden"

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdTablas_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmdTablas.Click
        Dim item As DataGridItem
        Dim i As Integer = 0
        Dim ban As Boolean
        If cmdTablas.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg" Then
            cmdTablas.ImageUrl = "..\Imagenes\menu\caja_box.jpg"
            lblTablas.Text = "Seleccionar todo"
            ban = False
        Else
            cmdTablas.ImageUrl = "..\Imagenes\menu\caja_box_paloma.jpg"
            lblTablas.Text = "Deseleccionar todo"
            ban = True
        End If

        For i = 0 To grdTablas.Items.Count - 1
            item = grdTablas.Items(i)
            CType(item.FindControl("chkTabla"), CheckBox).Checked = ban
        Next
    End Sub

    Private Sub cmdGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerar.Click
        Dim item As DataGridItem
        Dim i As Integer = 0
        Dim bandera As Boolean
        Dim Versiones As String
        Dim Aseguradoras As String
        Dim Tablas As String
        Dim Cadena As String
        Dim NombreArchivo As String = ""
        Dim IdBitacora As Integer

        Try
            'Verifica que tenga seleccinada al menos una aseguradora y una tabla.
            bandera = False
            'Aseguradoras
            For i = 0 To grdAseguradoras.Items.Count - 1
                item = grdAseguradoras.Items(i)
                If CType(item.FindControl("chkAseguradora"), CheckBox).Checked = True Then
                    bandera = True
                    Aseguradoras = Aseguradoras & item.Cells(0).Text & ","
                End If
            Next

            If bandera = False Then
                MsgBox.ShowMessage("Seleccione al menos una aseguradora")
                Exit Sub
            End If
            Aseguradoras = Mid(Aseguradoras, 1, Len(Aseguradoras) - 1)

            bandera = False
            'Versiones
            For i = 0 To grdVersiones.Items.Count - 1
                item = grdVersiones.Items(i)
                If CType(item.FindControl("chkVersion"), CheckBox).Checked = True Then
                    bandera = True
                    Versiones = Versiones & item.Cells(0).Text & ","
                End If
            Next

            If bandera = False Then
                MsgBox.ShowMessage("Seleccione al menos una versi�n")
                Exit Sub
            End If
            Versiones = Mid(Versiones, 1, Len(Versiones) - 1)

            bandera = False
            'Tablas
            Tablas = ""
            For i = 0 To grdTablas.Items.Count - 1
                item = grdTablas.Items(i)
                If CType(item.FindControl("chkTabla"), CheckBox).Checked = True Then
                    bandera = True
                    Tablas = Tablas & item.Cells(0).Text & ","
                End If
            Next

            If bandera = False Then
                MsgBox.ShowMessage("Seleccione al menos una tabla")
                Exit Sub
            End If

            Tablas = Mid(Tablas, 1, Len(Tablas) - 1)

            'Obtiene el consecutivo de la tabla Bitacoras
            Dim dt As DataTable = ccliente.obtener_MaxArchivo(Session("usuario"))
            IdBitacora = dt.Rows(0).Item("Id_Bitacora")

            'Par�metros
            Session("Cadena") = "D:\Aplicaciones\CotizadorContado_Conauto|1|9|" & Aseguradoras & "|" & Versiones & "|" & NombreArchivo & "|" & Tablas & "|" & Session("usuario") & "|" & IdBitacora & ""

            Page.RegisterStartupScript("activaEspera", "<script language='JavaScript'>muestraCapa();</script>")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCargar.Click
        Dim NombreArchivo As String = ""
        Dim Ubicacion As String = ""
        Dim Tablas As String = ""
        Dim Cadena As String = ""
        Dim Aseguradoras As String = ""
        Dim IdBitacora As Integer

        Try
            'Obtiene el archivo a cargar
            If flArchivoExcel.PostedFile Is Nothing Or flArchivoExcel.PostedFile.ContentLength = 0 Then
                MsgBox.ShowMessage("Seleccione el archivo comprimido")
                Exit Sub
                'ElseIf Not flArchivoExcel.PostedFile.ContentType = "application/vnd.ms-excel" Then
                '    MsgBox.ShowMessage("Seleccione un Archivo de tipo Excel")
                '    Exit Sub
            Else
                'quita la extencion del archivo
                'NombreArchivo = System.IO.Path.GetFileNameWithoutExtension(flArchivoExcel.PostedFile.FileName)
                NombreArchivo = System.IO.Path.GetFileName(flArchivoExcel.PostedFile.FileName)

                If NombreArchivo.Length > 50 Then
                    MsgBox.ShowMessage("El nombre del archivo no debe sobrepasar los 50 car�cteres")
                    Exit Sub
                End If

                If NombreArchivo <> "" Then
                    Ubicacion = "D:\Aplicaciones\SetTablas\Excel\Carga\" & NombreArchivo
                    flArchivoExcel.PostedFile.SaveAs(Ubicacion)
                End If
            End If

            'Obtiene el consecutivo de la tabla Bitacoras
            Dim dt As DataTable = ccliente.obtener_MaxArchivo(Session("usuario"))
            IdBitacora = dt.Rows(0).Item("Id_Bitacora")

            'Par�metros
            Session("Cadena") = "D:\Aplicaciones\CotizadorContado_Conauto|2|9|" & Aseguradoras & "|" & Session("Version") & "|" & NombreArchivo & "|" & Tablas & "|" & Session("usuario") & "|" & IdBitacora & ""

            Page.RegisterStartupScript("activaEspera", "<script language='JavaScript'>muestraCapa();</script>")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub CargarAseguradoras()
        Try

            Dim dt As DataTable = ccliente.carga_aseguradoras()
            If dt.Rows.Count > 0 Then
                grdAseguradoras.DataSource = dt
                grdAseguradoras.DataBind()
                pnlAseguradoras.Visible = True
            Else
                pnlAseguradoras.Visible = False
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub CargarTablas()
        Try

            Dim dt As DataTable = ccliente.carga_tablas()
            If dt.Rows.Count > 0 Then
                grdTablas.DataSource = dt
                grdTablas.DataBind()
                pnlTablas.Visible = True
            Else
                pnlTablas.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub EstablecerColor()
        A1.Style.Remove("BACKGROUND-POSITION")
        B1.Style.Remove("BACKGROUND-POSITION")
        A2.Style.Remove("BACKGROUND-POSITION")
        B2.Style.Remove("BACKGROUND-POSITION")
    End Sub

    Private Sub cmdCreaTabla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreaTabla.Click
        Try

            If Not Session("Archivo") Is Nothing Then
                If Session("Estatus_Error") = "0" Then
                    If Session("Bandera") = 1 Then
                        'MsgBox.ShowMessage("Descargue el archivo")
                        ancrArchivo.HRef = "WfrmCargaTablas.aspx?nombre=" & Session("Archivo")
                        ancrArchivo.Style("visibility") = "visible"
                    Else
                        MsgBox.ShowMessage("Se cargo correctamente el archivo")
                    End If
                Else
                    MsgBox.ShowMessage("Se genero un error en el proceso del archivo,\n favor de consultar con su administrador")
                End If
            Else
                MsgBox.ShowMessage("Se genero un error en el proceso del archivo,\n favor de consultar con su administrador")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdCreaError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreaError.Click
        MsgBox.ShowMessage("Se genero un error en el proceso del archivo,\n favor de consultar con su administrador")
    End Sub
End Class
