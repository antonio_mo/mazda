<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCargaTablas.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCargaTablas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmCargarTablas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="../JavaScript/Jscripts.js"></script>
		<%If session("bancss")=0 then%>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<%else%>
		<LINK href="../Css/Styles_aon.css" type="text/css" rel="stylesheet">
		<%end if%>
		<style type="text/css">#wait { DISPLAY: none; LEFT: 0px; WIDTH: 100%; POSITION: fixed; TOP: 0px; HEIGHT: 100% }
	#mask { DISPLAY: none; Z-INDEX: 9000; LEFT: 0px; POSITION: absolute; TOP: 0px; BACKGROUND-COLOR: #000 }
	#boxes .window { PADDING-RIGHT: 20px; DISPLAY: none; PADDING-LEFT: 20px; Z-INDEX: 9999; PADDING-BOTTOM: 20px; WIDTH: 440px; PADDING-TOP: 20px; POSITION: absolute; HEIGHT: 200px }
	#boxes #dialog { WIDTH: 375px; HEIGHT: 203px; BACKGROUND-COLOR: #fff; align: center }
		</style>
		<style type="text/css" rel="stylesheet">#header1 UL { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 10px; LIST-STYLE-TYPE: none }
	#header1 LI { PADDING-RIGHT: 0px; DISPLAY: inline; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px }
	#header1 A { PADDING-RIGHT: 0px; PADDING-LEFT: 9px; BACKGROUND: url(..\Imagenes\Pestanas\left_both.gif) no-repeat left top; FLOAT: left; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: #765 1px solid; TEXT-DECORATION: none }
	#header1 A SPAN { PADDING-RIGHT: 15px; DISPLAY: block; PADDING-LEFT: 6px; FONT-WEIGHT: bold; BACKGROUND: url(..\Imagenes\Pestanas\right_both.gif) no-repeat right top; FLOAT: left; PADDING-BOTTOM: 4px; COLOR: #333; PADDING-TOP: 5px }
	#header1 A SPAN { FLOAT: none }
	#header1 A:hover SPAN { COLOR: #333 }
	#header1 #current A { BORDER-TOP-WIDTH: 0px; BACKGROUND-POSITION: 0px -150px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px }
	#header1 #current A SPAN { BACKGROUND-POSITION: 100% -1px; PADDING-BOTTOM: 5px; COLOR: #333 }
	#header1 A:hover { BACKGROUND-POSITION: 0% -150px }
	#header1 A:hover SPAN { BACKGROUND-POSITION: 100% -150px }
		</style>
		<script src="../js/jquery.js"></script>
		<script language="javascript">

			function muestraCapa(){
			//select all the a tag with name equal to modal
			var muestra = function() {
				//Get the A tag
				var id = '#dialog';//$(this).attr('href');

				//Get the screen height and width
				var maskHeight = $(document).height();
				var maskWidth = $(window).width();
		     
				//Set height and width to mask to fill up the whole screen
				$('#mask').css({'width':maskWidth,'height':maskHeight});
		         
				//transition effect    
				$('#mask').fadeIn(1000);   
				$('#mask').fadeTo("slow",0.8); 
		     
				//Get the window height and width
				var winH = $(window).height();
				var winW = $(window).width();
		               
				//Set the popup window to center
				$(id).css('top',  winH/2-$(id).height()/2);
				$(id).css('left', winW/2-$(id).width()/2);
		     
				//transition effect
				$(id).fadeIn(2000);
				
				
				//Hacer llamada ajax; no necesita parametros, todos los datos estan en Session
				$.ajax({
					url : 'WfrmCargaTablasArchivo.aspx',
					complete : function(xhr, status) {
					var mensajeSalida =  xhr.responseText;
					
					ocultaCapa();
					
					//Asignar valores de retorno
					//document.write(mensajeSalida)	//Se puede manipular el mensaje si es un error				
					if (mensajeSalida=='Exito')
					{
						//EHG -  Se hace postback a un boton y ejecutar el dibujado del cuadro
						//__doPostBack('<%= cmdCreaTabla.ClientID %>','');
						__doPostBack('cmdCreaTabla','');
					}
					else
					{
						//EHG -  Se hace postback a un boton y ejecutar el dibujado del cuadro
						//__doPostBack('<%= cmdCreaError.ClientID %>','');
						__doPostBack('cmdCreaError','');
					}
					
					}
				});
		     
			}
			muestra();
			
			}
			
			function ocultaCapa(){
		        $('.window').hide();
		        $('#mask').hide();
			}
		    
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<div id="mask"></div>
		<div id="boxes">
			<div class="window" id="dialog" style="LEFT: 8px; TOP: 8px">
				<P align="center"><b>El archivo se esta procesando, por favor espere.<br>
						Este cuadro se cerrara automaticamente cuando el archivo se haya procesado</b></P>
				<P align="center"><B><IMG height="50" src="..\Imagenes\imagen_espera\spinner.gif" width="50"></B>&nbsp;
				</P>
			</div>
		</div>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;
							<asp:label id="lblTitulo" runat="server"></asp:label></FONT></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 102; LEFT: 272px; POSITION: absolute; TOP: 904px" runat="server"></cc1:msgbox>
			<TABLE id="Table6" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 40px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD style="HEIGHT: 40px" vAlign="top">
						<DIV id="header1" style="WIDTH: 100%; HEIGHT: 40px" noWrap runat="server">
							<UL>
								<LI>
									<A class="negrita" id="A1" style="WIDTH: 121px; HEIGHT: 24px" runat="server"><SPAN id="B1" runat="server">
											Generar Archivo</SPAN></A>
								<LI>
									<A class="negrita" id="A2" style="WIDTH: 112px; HEIGHT: 24px" runat="server"><SPAN id="B2" runat="server">
											Cargar Tablas</SPAN></A>&nbsp;<SPAN id="Span2" runat="server"></SPAN>&nbsp;&nbsp;
								</LI>
							</UL>
						</DIV>
					</TD>
				</TR>
				<TR>
					<TD><asp:panel id="pnlGeneral" runat="server" DESIGNTIMEDRAGDROP="160" Width="100%" Height="336px">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD vAlign="top">
										<asp:Panel id="pnlAseguradoras" runat="server" Width="100%">
											<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD class="obligatorio" width="60%">Seleccione la(s) Aseguradora(s) :</TD>
												</TR>
												<TR>
													<TD>
														<asp:datagrid id="grdAseguradoras" Width="100%" CssClass="datagrid" CellPadding="1" AllowSorting="True"
															AutoGenerateColumns="False" Runat="server">
															<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
															<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
															<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
															<ItemStyle CssClass="NormalItems"></ItemStyle>
															<HeaderStyle Wrap="False" CssClass="header"></HeaderStyle>
															<Columns>
																<asp:BoundColumn Visible="False" DataField="id_aseguradora" HeaderText="id_aseguradora"></asp:BoundColumn>
																<asp:BoundColumn DataField="Nombre_Completo" HeaderText="Aseguradoras">
																	<HeaderStyle Width="80%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Seleccionar">
																	<HeaderStyle Width="20%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:CheckBox id="chkAseguradora" Runat="server"></asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
															<PagerStyle Wrap="False"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<asp:Panel id="pnlVersiones" runat="server" Width="100%">
											<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD class="obligatorio" width="60%">Seleccione la(s) Versione(s) :</TD>
												</TR>
												<TR>
													<TD>
														<asp:datagrid id="grdVersiones" Width="100%" CssClass="datagrid" CellPadding="1" AllowSorting="True"
															AutoGenerateColumns="False" Runat="server">
															<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
															<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
															<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
															<ItemStyle CssClass="NormalItems"></ItemStyle>
															<HeaderStyle Wrap="False" CssClass="header"></HeaderStyle>
															<Columns>
																<asp:BoundColumn Visible="False" DataField="id_version" HeaderText="id_version"></asp:BoundColumn>
																<asp:BoundColumn DataField="id_version" HeaderText="Versiones">
																	<HeaderStyle Width="80%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Seleccionar">
																	<HeaderStyle Width="20%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:CheckBox id="chkVersion" Runat="server"></asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
															<PagerStyle Wrap="False"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<asp:Panel id="pnlTablas" runat="server" Width="100%">
											<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD class="obligatorio" width="60%">Seleccione la(s)&nbsp;Tabla(s) :</TD>
												</TR>
												<TR>
													<TD>
														<asp:datagrid id="grdTablas" Width="100%" CssClass="datagrid" CellPadding="1" AllowSorting="True"
															AutoGenerateColumns="False" Runat="server">
															<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
															<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
															<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
															<ItemStyle CssClass="NormalItems"></ItemStyle>
															<HeaderStyle Wrap="False" CssClass="header"></HeaderStyle>
															<Columns>
																<asp:BoundColumn DataField="Tablas" HeaderText="Tablas">
																	<HeaderStyle Width="80%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:TemplateColumn HeaderText="Seleccionar">
																	<HeaderStyle Width="20%"></HeaderStyle>
																	<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	<ItemTemplate>
																		<asp:CheckBox id="chkTabla" Runat="server"></asp:CheckBox>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
															<PagerStyle Wrap="False"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD>
														<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<TR>
																<TD width="90%">
																	<asp:Label id="lblTablas" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
																<TD align="center" width="10%">
																	<asp:imagebutton id="cmdTablas" tabIndex="13" runat="server" ImageUrl="..\Imagenes\menu\caja_box.jpg"></asp:imagebutton></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD><A id="ancrArchivo" style="VISIBILITY: hidden" href="" target="_blank" runat="server">[Descargar 
															Archivo]</A></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<asp:Panel id="pnlArchivo" runat="server" Width="100%">
											<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD class="obligatorio" width="60%">Seleccione&nbsp;el Archivo&nbsp;:</TD>
												</TR>
												<TR>
													<TD><INPUT class="combos_small" id="flArchivoExcel" style="WIDTH: 100%; HEIGHT: 20px" type="file"
															size="63" name="flArchivoExcel" runat="server"></TD>
												</TR>
											</TABLE>
										</asp:Panel></TD>
									<TD style="HEIGHT: 478px" vAlign="top">
										<asp:panel id="pnlObservaciones" runat="server" Height="56px" Width="168px">
											<asp:Literal id="ltlObservaciones" Runat="server"></asp:Literal>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD align="center" width="60%">
										<asp:button id="cmdGenerar" CssClass="boton" Runat="server" Text="Generar Archivo"></asp:button>
										<asp:button id="cmdCargar" CssClass="boton" Runat="server" Text="Cargar" visible="false"></asp:button></TD>
									<TD width="40%"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD>
						<ontranet:SaveDialog id="sd" runat="server"></ontranet:SaveDialog>
						<asp:linkbutton id="cmdCreaTabla" runat="server"></asp:linkbutton>
						<asp:linkbutton id="cmdCreaError" runat="server"></asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
