﻿Imports System.Collections.Generic

Public Class PolizasDetalle
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        divGrid.Visible = False
        divPoliza.Visible = False

        If Session("usuario") = "" Or Session("usuario") = Nothing Or Session("IdNivel") <> 1 Then
            Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
        End If
    End Sub

    Protected Sub btnFindPol_Click(sender As Object, e As EventArgs) Handles btnFindPol.Click
        LoadData()
    End Sub

    Protected Sub dgLista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dgLista.SelectedIndexChanged
        Dim iIdPoliza As Integer = dgLista.DataKeys(dgLista.SelectedIndex)
        txtIdPoliza.Text = iIdPoliza.ToString()
        Dim objPoliza As New CC.Modelo.Entities.POLIZA()
        objPoliza = New CC.Negocio.Administracion.Polizas().Get(iIdPoliza)
        txtPoliza.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Poliza1)
        txtIdBid.Text = CC.Negocio.General.Conversiones.ShortToString(objPoliza.Id_Bid)
        txtIdAseguradora.Text = CC.Negocio.General.Conversiones.ShortToString(objPoliza.Id_Aseguradora)
        txtFechaVig.Text = CC.Negocio.General.Conversiones.DateToString(objPoliza.FVigencia)
        txtFechaFin.Text = CC.Negocio.General.Conversiones.DateToString(objPoliza.FFin)
        txtFechaRegistro.Text = CC.Negocio.General.Conversiones.DateToString(objPoliza.Fecha_Registro)
        txtMarca.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Marca)
        txtModelo.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Modelo)
        txtDescripcion.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Descripcion)
        txtAnio.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Anio)
        txtSerie.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Serie)
        txtCobertura.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.Cobertura)
        txtCapacidad.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Capacidad)
        txtMotor.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Motor)
        txtPrimaNeta.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.PrimaNeta)
        txtDerPol.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.DerPol)
        txtIva.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.Iva)
        txtPrimaTotal.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.PrimaTotal)
        txtPagoInicial.Text = CC.Negocio.General.Conversiones.DecimalToString(objPoliza.PagoInicial)
        txtPlazo.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Plazo)
        txtTipoCarga.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Tipo_Carga)
        txtInterfaceCancelacion.Text = CC.Negocio.General.Conversiones.ToString(objPoliza.Interface_Cancelacion)
        txtIdCancelacion.Text = CC.Negocio.General.Conversiones.ByteToString(objPoliza.Id_Cancelacion)
        txtFechaCancelacion.Text = CC.Negocio.General.Conversiones.DateToString(objPoliza.Fecha_Cancelacion)

        ActivateEdit(True)
    End Sub

    Private Sub LoadData()
        Dim lstPolizas As New List(Of CC.Modelo.Administracion.PolizaDG)
        lstPolizas = New CC.Negocio.Administracion.Polizas().Listar(txtFdPoliza.Text)
        ActivateEdit(False)

        If (lstPolizas.Count > 0) Then
            dgLista.DataSource = lstPolizas
            dgLista.DataBind()
            divGrid.Visible = True
        Else
            lblResult.Text = "No se encontro la póliza"
        End If
    End Sub

    Private Sub ActivateEdit(ByVal isEdit As Boolean)
        If (isEdit) Then
            divPoliza.Visible = True
            divGrid.Visible = False
            lblResult.Text = String.Empty
        Else
            divGrid.Visible = True
            divPoliza.Visible = False
        End If
    End Sub
End Class