<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmGeneraScript.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmGeneraScript" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 110; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Genera Archivo 
							de Salida</FONT></TD>
				</TR>
			</TABLE>
			<cc1:msgbox style="Z-INDEX: 103; POSITION: absolute; TOP: 400px; LEFT: 328px" id="MsgBox" runat="server"></cc1:msgbox>
			<TABLE style="Z-INDEX: 105; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table3" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD class="obligatorio" width="25%" align="right">Generar Archivo :&nbsp;</TD>
					<TD class="obligatorio" width="25%"><asp:label id="lbl_programa" runat="server" CssClass="normal"></asp:label></TD>
					<TD width="25%" colspan="2" style="width: 50%">
                        <asp:Table ID="tb_imp" runat="server" Width="100%">
                        </asp:Table>
                    </TD>
				</TR>
				<TR>
					<TD class="obligatorio" width="25%" align="right"><asp:label id="lbl_Tipo" runat="server">Tipo : </asp:label>&nbsp;</TD>
					<TD width="25%"><asp:dropdownlist id="cbo_TipoLayout" runat="server" CssClass="combos_small" Width="90%" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD><asp:button style="Z-INDEX: 0" id="cmd_generar" runat="server" CssClass="boton" 
                            Text="Procesar"></asp:button></TD>
					<TD></TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
