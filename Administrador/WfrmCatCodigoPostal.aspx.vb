Imports CN_Negocios
Partial Class WfrmCatCodigoPostal
    Inherits System.Web.UI.Page

    Private ccliente As New CnCatalogo
    Private cvalida As New CnPrincipal

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                cargar_estados()
                limpia_campos()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_Guardar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Guardar.Click
        Try
            If Trim(txt_cp.Text) = "" Then
                MsgBox.ShowMessage("El c�digo postal no puede quedar en blanco, verif�quelo")
                Exit Sub
            End If

            If cbo_estado.SelectedValue = 0 Then
                MsgBox.ShowMessage("Debe de seleccionar el Estado, verif�quelo")
                Exit Sub
            End If

            If Trim(txt_provincia.Text) = "" Then
                MsgBox.ShowMessage("La provincia no puede quedar en blanco, verif�quelo")
                Exit Sub
            End If

            If Trim(txt_municipio.Text) = "" Then
                MsgBox.ShowMessage("El municipio no puede quedar en blanco, verif�quelo")
                Exit Sub
            End If

            If Trim(txt_colonia.Text) = "" Then
                MsgBox.ShowMessage("La colonia no puede quedar en blanco, verif�quelo")
                Exit Sub
            End If

            If Trim(txt_pctiva.Text) = "" Then
                MsgBox.ShowMessage("El pct del iva no puede quedar en blanco, verif�quelo")
                Exit Sub
            End If

            If cbo_Estatus.SelectedValue = "P" Then
                cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue(0))
            End If

            If txt_id.Text = "" Then
                ccliente.inserta_codigopostal(cbo_estado.SelectedValue, _
                cbo_estado.SelectedItem.Text, _
                cvalida.valida_cadena(txt_cp.Text), _
                cvalida.valida_cadena(txt_provincia.Text), _
                cvalida.valida_cadena(txt_municipio.Text), _
                cvalida.valida_cadena(txt_colonia.Text), _
                cvalida.valida_cadena(txt_pctiva.Text), _
                cbo_Estatus.SelectedValue)
                MsgBox.ShowMessage("Los datos se registraron correctamente")
            Else
                'ccliente.actualiza_codigopostal(txt_id.Text, _
                'cbo_estado.SelectedValue, _
                'cbo_estado.SelectedItem.Text, _
                'cvalida.valida_cadena(txt_cp.Text), _
                'cvalida.valida_cadena(txt_provincia.Text), _
                'cvalida.valida_cadena(txt_municipio.Text), _
                'cvalida.valida_cadena(txt_colonia.Text), _
                'cvalida.valida_cadena(txt_pctiva.Text), _
                'cbo_Estatus.SelectedValue)
                'MsgBox.ShowMessage("Los datos se actualizaron correctamente")
            End If
            Cargar_Grid()
            limpia_campos()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_Nuevo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Nuevo.Click
        Try
            limpia_campos()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cargar_estados()
        Try
            Dim dt As DataTable = ccliente.cargar_estados()
            If dt.Rows.Count > 0 Then
                cbo_estado.DataSource = dt
                cbo_estado.DataTextField = "Nombre_Estado"
                cbo_estado.DataValueField = "Id_Estado"
                cbo_estado.DataBind()
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia_campos()
        txt_id.Text = ""
        cbo_estado.SelectedIndex = cbo_estado.Items.IndexOf(cbo_estado.Items.FindByValue(0))
        txt_cp.Text = ""
        txt_provincia.Text = ""
        txt_municipio.Text = ""
        txt_colonia.Text = ""
        txt_pctiva.Text = "16"
        cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue("1"))
    End Sub

    Private Sub cmd_Buscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Buscar.Click
        Try
            If Trim(txt_cp.Text) = "" Then
                MsgBox.ShowMessage("Ingrese el c�digo postal")
                Exit Sub
            End If

            Cargar_Grid()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub Cargar_Grid()
        Try
            Dim dt As DataTable = ccliente.carga_grd_codigopostal(txt_cp.Text)
            If dt.Rows.Count = 0 Then
                pnl_datos.Visible = False
            Else
                pnl_datos.Visible = True
                grdcatalogo.DataSource = dt
                grdcatalogo.DataBind()
                'grdcatalogo_fondo.DataSource = dt
                'grdcatalogo_fondo.DataBind()
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grdcatalogo_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            ''Id Iva
            'If e.Item.Cells(0).Text() = "&nbsp;" Then
            '    txt_id.Text = 0
            'Else
            '    txt_id.Text = e.Item.Cells(0).Text()
            'End If

            ''Id Estado
            'If e.Item.Cells(1).Text() = "&nbsp;" Then
            '    cbo_estado.SelectedIndex = cbo_estado.Items.IndexOf(cbo_estado.Items.FindByValue("0"))
            'Else
            '    cbo_estado.SelectedIndex = cbo_estado.Items.IndexOf(cbo_estado.Items.FindByValue(e.Item.Cells(1).Text))
            'End If

            ''C�digo postal
            'If e.Item.Cells(3).Text() = "&nbsp;" Then
            '    txt_cp.Text = ""
            'Else
            '    txt_cp.Text = e.Item.Cells(3).Text()
            'End If

            ''Provincia
            'If e.Item.Cells(4).Text() = "&nbsp;" Then
            '    txt_provincia.Text = ""
            'Else
            '    txt_provincia.Text = e.Item.Cells(4).Text()
            'End If

            ''Municipio         
            'If e.Item.Cells(5).Text() = "&nbsp;" Then
            '    txt_municipio.Text = ""
            'Else
            '    txt_municipio.Text = e.Item.Cells(5).Text()
            'End If

            ''Colonia
            'If e.Item.Cells(6).Text() = "&nbsp;" Then
            '    txt_colonia.Text = ""
            'Else
            '    txt_colonia.Text = e.Item.Cells(6).Text()
            'End If

            ''Pct_Iva
            'If e.Item.Cells(7).Text() = "&nbsp;" Then
            '    txt_pctiva.Text = ""
            'Else
            '    txt_pctiva.Text = CInt(e.Item.Cells(7).Text())
            'End If

            ''Estatus
            'If e.Item.Cells(8).Text() = "&nbsp;" Then
            '    cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue("0"))
            'Else
            '    cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue(e.Item.Cells(8).Text))
            'End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
