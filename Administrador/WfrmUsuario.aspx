<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmUsuario.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmUsuario" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmUsuario</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<script language="javascript" src="..\JavaScript\HGridScript.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<style type="text/css" rel="stylesheet">#header1 UL { PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 0px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px; PADDING-TOP: 10px }
	#header1 LI { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; DISPLAY: inline; PADDING-TOP: 0px }
	#header1 A { BORDER-BOTTOM: #765 1px solid; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 9px; PADDING-RIGHT: 0px; BACKGROUND: url(..\Imagenes\Pestanas\left_both.gif) no-repeat left top; FLOAT: left; TEXT-DECORATION: none; PADDING-TOP: 0px }
	#header1 A SPAN { PADDING-BOTTOM: 4px; PADDING-LEFT: 6px; PADDING-RIGHT: 15px; DISPLAY: block; BACKGROUND: url(..\Imagenes\Pestanas\right_both.gif) no-repeat right top; FLOAT: left; COLOR: #333; FONT-WEIGHT: bold; PADDING-TOP: 5px }
	#header1 A SPAN { FLOAT: none }
	#header1 A:hover SPAN { COLOR: #333 }
	#header1 #current A { BORDER-RIGHT-WIDTH: 0px; BACKGROUND-POSITION: 0px -150px; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px }
	#header1 #current A SPAN { PADDING-BOTTOM: 5px; BACKGROUND-POSITION: 100% -1px; COLOR: #333 }
	#header1 A:hover { BACKGROUND-POSITION: 0% -150px }
	#header1 A:hover SPAN { BACKGROUND-POSITION: 100% -150px }
		</style>
		<script language="JScript">
		function DoScroll0()
		{
			document.all ("grdUbicacion_fondo").style.pixelLeft=divScroll0.scrollLeft * -1;
		}
		function DoScroll()
		{
			document.all ("grdfondo_bid").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		function DoScroll1()
		{
			document.all ("grdfondo_prog").style.pixelLeft=divScroll1.scrollLeft * -1;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" width="100%">
			<TABLE id="Table8" style="Z-INDEX: 102; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px"
				cellSpacing="0" cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="2%"><asp:image id="Image1" runat="server" Width="24px" ImageUrl="..\Imagenes\icono.gif" Height="24px"></asp:image></TD>
					<TD class="header_tabla" width="48%" background="..\Imagenes\header_menu_verde_1.GIF">&nbsp; 
						Registro de usuarios</TD>
					<TD align="right" width="47%" background="..\Imagenes\header_menu_verde_3.GIF"><A href="../wfrmblanco.aspx?bandera_inicio=1" target="contenido"><!--<LI>
								</LIO>Regresar</A></LI>--></A></TD>
					<TD width="3%" background="..\Imagenes\header_menu_verde_2.GIF"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD colSpan="4">
						<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="3"><asp:panel id="pnl_listado" runat="server" Width="100%">
										<DIV style="WIDTH: 95%; HEIGHT: 18px; OVERFLOW: hidden">
											<asp:datagrid id="grdUbicacion_fondo" runat="server" Width="592px" AutoGenerateColumns="False"
												CssClass="Datagrid" CellPadding="1">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
												<Columns>
													<asp:BoundColumn Visible="False" DataField="id_usuario"></asp:BoundColumn>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre Usuario">
														<ItemStyle Width="40%"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="descripcion_nivel" HeaderText="Nivel">
														<ItemStyle Width="20%"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="estatus" HeaderText="Estatus">
														<ItemStyle Width="15%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Sel."
														CommandName="Select">
														<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<DIV style="WIDTH: 95%; HEIGHT: 80px; OVERFLOW: auto" onscroll="DoScroll0()" id="divScroll0">
											<asp:datagrid id="grdUbicacion" runat="server" Width="592px" AutoGenerateColumns="False" CssClass="datagrid"
												CellPadding="1" ShowHeader="False">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
												<Columns>
													<asp:BoundColumn Visible="False" DataField="Id_usuario"></asp:BoundColumn>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre usuario">
														<ItemStyle Width="40%"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="descripcion_nivel" HeaderText="Nivel">
														<ItemStyle Width="20%"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Estatus" HeaderText="Estatus">
														<ItemStyle HorizontalAlign="Center" Width="15%" VerticalAlign="Middle"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Sel."
														CommandName="Select">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD width="25%"></TD>
								<TD width="30%"><asp:label id="Label12" runat="server" Width="100%" CssClass="negrita">Busqueda por nombre:</asp:label></TD>
								<TD width="45%"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD><asp:textbox onkeypress="javascript:onlyAllLetters(event);" id="txt_buscar" tabIndex="1" runat="server"
										Width="100%" CssClass="combos_small" MaxLength="30"></asp:textbox></TD>
								<TD><asp:imagebutton id="cmd_Buscar" tabIndex="2" runat="server" ImageUrl="..\Imagenes\Botones\buscar.gif"></asp:imagebutton></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle" align="center"><asp:imagebutton id="cmd_nuevo" tabIndex="3" runat="server" ImageUrl="..\Imagenes\Botones\nuevo.gif"></asp:imagebutton></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="bottom" colSpan="4">
						<DIV id="header1" style="WIDTH: 100%" noWrap runat="server">
							<UL>
								<LI>
									<A class="negrita" id="A1" runat="server"><SPAN id="B1" runat="server">Datos del 
											Usuario
											Usuario</SPAN></A>
								<LI>
									<A class="negrita" id="A2" runat="server"><SPAN id="B2" runat="server">Agregar 
											Dis</SPAN></A>
								<LI>
								</LI>
							</UL>
						</DIV>
					</TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:panel id="pnl_usuario" runat="server" Width="100%" BorderWidth="0px">
							<P>
								<TABLE style="HEIGHT: 128px" id="Table2" class="tablas" border="0" cellSpacing="1" cellPadding="1"
									width="100%">
									<TR>
										<TD class="Header" width="100%" colSpan="4">
											<asp:Label id="Label1" runat="server" Width="100%" CssClass="Header">Datos personales del  usuario</asp:Label></TD>
									</TR>
									<TR>
										<TD class="obligatorio" align="right">Login :</TD>
										<TD>
											<asp:TextBox id="txtlogin" tabIndex="5" onkeypress="javascript:CaracterNoValido(event);" runat="server"
												Width="80%" CssClass="combos_small" MaxLength="10"></asp:TextBox></TD>
										<TD class="obligatorio" align="right">Contrase�a :</TD>
										<TD>
											<asp:Label id="txtPassword" runat="server" Width="80%" CssClass="combos_small"></asp:Label>&nbsp;
											<asp:imagebutton id="cmd_regenera" tabIndex="3" runat="server" ImageUrl="..\Imagenes\Menu\script_key.png" style="width: 16px"></asp:imagebutton></TD>
									</TR>
									<TR>
										<TD class="obligatorio" align="right">Nombre:</TD>
										<TD>
											<asp:TextBox id="txtnombre" tabIndex="7" onkeypress="javascript:onlyAllLetters(event);" runat="server"
												Width="100%" CssClass="combos_small" MaxLength="75"></asp:TextBox></TD>
										<TD class="obligatorio" align="right">Correo&nbsp;electronico :</TD>
										<TD>
											<asp:TextBox id="Txt_Correo" tabIndex="10" onkeypress="javascript:CaracterNoValido(event);" runat="server"
												Width="100%" CssClass="combos_small" MaxLength="100"></asp:TextBox></TD>
									</TR>
									<TR>
										<TD class="obligatorio" align="right">Nivel Usuario :</TD>
										<TD>
											<asp:DropDownList id="cbo_nivel" tabIndex="12" runat="server" Width="80%" CssClass="combos"></asp:DropDownList></TD>
										<TD class="obligatorio" align="right">Usuario&nbsp;Bloqueado :</TD>
										<TD>
											<asp:DropDownList id="cbo_bloqueo" tabIndex="15" runat="server" Width="80%" CssClass="combos">
												<asp:ListItem Value="P" Selected="True">-- Seleccionar --</asp:ListItem>
												<asp:ListItem Value="0">falta Confirmar</asp:ListItem>
												<asp:ListItem Value="1">Confirmado</asp:ListItem>
												<asp:ListItem Value="2">Bloqueado</asp:ListItem>
											</asp:DropDownList></TD>
									</TR>
									<TR>
										<TD class="negrita" align="right">Estatus&nbsp;usuario&nbsp;:</TD>
										<TD>
											<asp:DropDownList id="cbo_estatus" tabIndex="12" runat="server" Width="80%" CssClass="combos">
												<asp:ListItem Value="P" Selected="True">-- Seleccionar --</asp:ListItem>
												<asp:ListItem Value="1">Activo</asp:ListItem>
												<asp:ListItem Value="0">Inactivo</asp:ListItem>
											</asp:DropDownList></TD>
										<TD class="obligatorio" align="right">Id Empleado:</TD>
										<TD>
											<asp:TextBox id="txtIdEmpleado" tabIndex="7" onkeypress="javascript:onlyDigitsLetters(event);" runat="server"
												Width="100%" CssClass="combos_small" MaxLength="10"></asp:TextBox></TD>
									</TR>
									<TR>
										<TD class="negrita" align="right">Administra Cat�logos</TD>
										<TD><asp:CheckBox ID="chkAdmSol" runat="server" /></TD>
										<TD class="obligatorio" align="right"></TD>
										<TD>
											<asp:TextBox id="txt_password" tabIndex="5" onkeypress="javascript:CaracterNoValido(event);"
												runat="server" CssClass="combos_small" MaxLength="15" style="display:none;"></asp:TextBox></TD>
									</TR>
									<TR>
										<TD class="obligatorio" colSpan="4" align="right">
											<TABLE style="HEIGHT: 24px" id="Table6" border="0" cellSpacing="1" cellPadding="1" width="100%">
												<TR>
													<TD width="40%"></TD>
													<TD width="10%">
														<asp:imagebutton id="cmd_guardar" tabIndex="13" runat="server" ImageUrl="..\Imagenes\Botones\guardar.gif"></asp:imagebutton></TD>
													<TD width="10%">
														<asp:imagebutton id="cmd_cancelar" tabIndex="14" runat="server" ImageUrl="..\Imagenes\Botones\cancelar.gif"></asp:imagebutton></TD>
													<TD width="40%" align="right"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD width="20%"></TD>
										<TD width="30%"></TD>
										<TD width="20%"></TD>
										<TD width="30%"></TD>
									</TR>
								</TABLE>
							</P>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:panel id="pnl_bid" runat="server">
							<TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
								<TR>
									<TD class="header" vAlign="top">Seleccione el Distribuidor</TD>
									<TD vAlign="top"></TD>
									<TD class="header" vAlign="top">Seleccione el programa</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD vAlign="top">
										<asp:panel id="pnl_dbid" runat="server" Width="100%">
											<DIV style="WIDTH: 100%; HEIGHT: 17px; OVERFLOW: hidden">
												<asp:datagrid style="POSITION: relative" id="grdfondo_bid" runat="server" Width="90%" AutoGenerateColumns="False"
													CssClass="datagrid" CellPadding="1">
													<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
													<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
													<EditItemStyle Wrap="False"></EditItemStyle>
													<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
													<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
													<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="id_bid"></asp:BoundColumn>
														<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
															<ItemStyle Width="70%"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Sel.">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="20%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:CheckBox id="cbk_grd" runat="server"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle Wrap="False"></PagerStyle>
												</asp:datagrid></DIV>
											<DIV style="WIDTH: 100%; HEIGHT: 160px; OVERFLOW: auto" onscroll="DoScroll()" id="divScroll">
												<asp:datagrid id="grd_bid" runat="server" Width="90%" AutoGenerateColumns="False" CssClass="datagrid"
													CellPadding="1" ShowHeader="False">
													<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
													<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
													<ItemStyle CssClass="NormalItems"></ItemStyle>
													<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="id_bid"></asp:BoundColumn>
														<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
															<ItemStyle Width="70%"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Sel.">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="20%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:CheckBox id="cbk_grd" runat="server"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle Wrap="False"></PagerStyle>
												</asp:datagrid></DIV>
										</asp:panel></TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top">
										<asp:panel id="pnl_dprograma" runat="server" Width="100%">
											<DIV style="WIDTH: 100%; HEIGHT: 17px; OVERFLOW: hidden">
												<asp:datagrid style="POSITION: relative" id="grdfondo_prog" runat="server" Width="90%" AutoGenerateColumns="False"
													CssClass="datagrid" CellPadding="1">
													<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
													<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
													<EditItemStyle Wrap="False"></EditItemStyle>
													<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
													<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
													<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="id_programa"></asp:BoundColumn>
														<asp:BoundColumn DataField="descripcion_programa" HeaderText="Programa">
															<ItemStyle Width="70%"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Sel.">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="20%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:CheckBox id="cbk_grd" runat="server"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle Wrap="False"></PagerStyle>
												</asp:datagrid></DIV>
											<DIV style="WIDTH: 100%; HEIGHT: 160px; OVERFLOW: auto" onscroll="DoScroll1()" id="divScroll1">
												<asp:datagrid id="grd_prog" runat="server" Width="90%" AutoGenerateColumns="False" CssClass="datagrid"
													CellPadding="1" ShowHeader="False">
													<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
													<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
													<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
													<ItemStyle CssClass="NormalItems"></ItemStyle>
													<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="id_programa"></asp:BoundColumn>
														<asp:BoundColumn DataField="descripcion_programa" HeaderText="Programa">
															<ItemStyle Width="70%"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Sel.">
															<ItemStyle Wrap="False" HorizontalAlign="Center" Width="20%" VerticalAlign="Middle"></ItemStyle>
															<ItemTemplate>
																<asp:CheckBox id="cbk_grd" runat="server"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle Wrap="False"></PagerStyle>
												</asp:datagrid></DIV>
										</asp:panel></TD>
									<TD vAlign="top">
										<TABLE id="Table4" border="0" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD width="50%" align="center"></TD>
												<TD class="negrita" width="50%"></TD>
											</TR>
											<TR>
												<TD class="negrita" colSpan="2" align="center">Agregar</TD>
											</TR>
											<TR>
												<TD height="10" colSpan="2" align="center">
													<asp:imagebutton id="cmd_agregar" runat="server" ImageUrl="..\Imagenes\menu\arrow_right.png"></asp:imagebutton></TD>
											</TR>
											<TR>
												<TD align="center"></TD>
												<TD class="negrita"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD vAlign="top">
										<TABLE id="Table7" border="1" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD width="90%">
													<asp:Label id="lbl_bid" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
												<TD width="10%" align="center">
													<asp:imagebutton id="cmd_bid" tabIndex="13" runat="server" ImageUrl="..\Imagenes\menu\caja_box.jpg"></asp:imagebutton></TD>
											</TR>
										</TABLE>
									</TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top">
										<TABLE id="Table9" border="1" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD width="90%">
													<asp:Label id="lbl_prog" runat="server" Width="100%" CssClass="combos_small"></asp:Label></TD>
												<TD width="10%" align="center">
													<asp:imagebutton id="cmd_prog" tabIndex="13" runat="server" ImageUrl="..\Imagenes\menu\caja_box.jpg"></asp:imagebutton></TD>
											</TR>
										</TABLE>
									</TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD vAlign="top" colSpan="3">
										<asp:datagrid id="grd_usuario" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="datagrid"
											CellPadding="1">
											<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
											<SelectedItemStyle CssClass="SelectedItems"></SelectedItemStyle>
											<AlternatingItemStyle CssClass="AlternatingItems"></AlternatingItemStyle>
											<ItemStyle CssClass="NormalItems"></ItemStyle>
											<HeaderStyle Wrap="False" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:HyperLinkColumn Text="+">
													<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
												</asp:HyperLinkColumn>
												<asp:BoundColumn Visible="False" DataField="id_programa"></asp:BoundColumn>
												<asp:BoundColumn DataField="descripcion_programa" HeaderText="Programa">
													<ItemStyle Width="90%"></ItemStyle>
												</asp:BoundColumn>
												<asp:ButtonColumn Text="&lt;img src='..\Imagenes\menu\trash.bmp' border='0'&gt;" HeaderText="Del."
													CommandName="Delete">
													<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn>
													<ItemStyle Width="0%"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Wrap="False"></PagerStyle>
										</asp:datagrid></TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
									<TD vAlign="top"></TD>
								</TR>
								<TR>
									<TD width="50%"></TD>
									<TD width="5%"></TD>
									<TD width="35%"></TD>
									<TD width="10%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; POSITION: absolute; TOP: 1432px; LEFT: 392px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
