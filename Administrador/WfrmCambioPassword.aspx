<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCambioPassword.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCambioPassword" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AonRiskWeb</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="..\imagenes\Original.ico" type="image/x-icon" rel="shortcut icon">
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
	    <style type="text/css">
            .auto-style1 {
                height: 23px;
            }
        </style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 103; LEFT: 168px; POSITION: absolute; TOP: 248px" runat="server"></cc1:msgbox>
			<TABLE id="Table4" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Cambio de 
							Contraseņa</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" width="25%">&nbsp;</TD>
					<TD class="obligatorio" width="25%">Contraseņa Actual :</TD>
					<TD width="25%"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_actual" tabIndex="1" runat="server"
							Width="70%" TextMode="Password" CssClass="combos_small" MaxLength="15"></asp:textbox></TD>
					<TD width="20%"></TD>
					<TD width="4%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD class="Obligatorio">Nueva Contraseņa :</TD>
					<TD><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_nuevo" tabIndex="2" runat="server"
							Height="20px" Width="70%" TextMode="Password" CssClass="combos_small" MaxLength="15"></asp:textbox></TD>
					<TD>
                        <cc1:MsgBox runat="server" />
					</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="auto-style1">&nbsp;</TD>
					<TD class="Obligatorio">Confirme Contraseņa :</TD>
					<TD class="auto-style1"><asp:textbox onkeypress="javascript:CaracterNoValido(event);" id="txt_confirma" tabIndex="3"
							runat="server" Height="20px" Width="70%" TextMode="Password" CssClass="combos_small" MaxLength="15"></asp:textbox></TD>
					<TD class="auto-style1"></TD>
					<TD class="auto-style1"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD class="obligatorio"></TD>
					<TD align="right">
						&nbsp;</TD>
					<TD align="center">
						<asp:Button id="cmd_regresar" runat="server" CssClass="boton" Text="Regresar" Visible="False"></asp:Button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD class="obligatorio" align="center" colspan="2" rowspan="1">
						<asp:Button id="Imaguardar" runat="server" CssClass="boton" Text="Guardar"></asp:Button></TD>
					<TD align="center">
						&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
			<HR class="lineas" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
		</form>
	</body>
</HTML>
