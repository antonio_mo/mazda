<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmReimprimir.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmReimprimir"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmReimprimir</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function RadioChecked(param)
		{
			{ 
				var frm = document.forms[0];
				// Take all elements of the form
				for (i=0; i < frm.length; i++)
				{
					// itinerate the elements searching "RadioButtons"
					if (frm.elements[i].type == "radio")
					{
					// Unchecked if the RadioButton is != param
					if (param != frm.elements[i].id )
					{
						frm.elements[i].checked = false;
					}
					}
				}
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 102; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Reimpresi�n de 
							P�lizas</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="15%"></TD>
					<TD width="35%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="Header_Gris">B�squeda
					</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="25%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" Width="100%" CssClass="combos_small"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="70%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="4"
										RepeatDirection="Horizontal" Height="56px">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="2">Motor</asp:ListItem>
										<asp:ListItem Value="3">Placas</asp:ListItem>
										<asp:ListItem Value="4">Marca</asp:ListItem>
										<asp:ListItem Value="5">Modelo</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3">
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Height="24px" Text="Buscar"></asp:button></TD>
				</TR>
				<TR>
					<TD width="25%" colSpan="4">
						<asp:datagrid id="grd_reimprime" runat="server" Width="100%" CssClass="datagrid" AllowSorting="True"
							CellPadding="1" AutoGenerateColumns="False">
							<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
							<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sel.">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:RadioButton id="opt_Seleccion" runat="server" CssClass="negrita"></asp:RadioButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Poliza" HeaderText="P&#243;liza">
									<ItemStyle Width="20%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Serie" HeaderText="Serie">
									<ItemStyle Width="20%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Marca" HeaderText="Marca">
									<ItemStyle Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Modelo" ReadOnly="True" HeaderText="Modelo">
									<ItemStyle Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="anio" ReadOnly="True" HeaderText="A&#241;o">
									<ItemStyle Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="descripcion" ReadOnly="True" HeaderText="Descripci&#243;n">
									<ItemStyle Width="20%"></ItemStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Da&#241;os">
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="ckb_grd" runat="server" CssClass="negrita"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="cadena_impresiond"></asp:BoundColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="obligatorio">
						<asp:Button id="cmd_procesar" runat="server" CssClass="boton" Text="Procesar"></asp:Button></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 104; POSITION: absolute; TOP: 472px; LEFT: 280px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
