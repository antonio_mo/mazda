Imports CN_Negocios

Partial Class wfrmBlanco
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Public ccliente As New CnCatalogo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''Introducir aqu� el c�digo de usurio para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                Dim bandera As Integer = 0

                If Not Request.QueryString("bandera_padre") = Nothing _
                And Request.QueryString("bandera_hijo") = Nothing Then
                    carga_padre(Request.QueryString("bandera_padre"), Session("IdMarca"))
                Else
                    If Not Request.QueryString("bandera_hijo") = Nothing _
                    And Not Request.QueryString("bandera_padre") = Nothing _
                    And Request.QueryString("bandera_subhijo") = Nothing Then
                        carga_hijo(Request.QueryString("bandera_padre"), Request.QueryString("bandera_hijo"))
                    Else
                        If Not Request.QueryString("bandera_hijo") = Nothing _
                        And Not Request.QueryString("bandera_padre") = Nothing _
                        And Not Request.QueryString("bandera_subhijo") = Nothing Then
                            carga_subhijo(Request.QueryString("bandera_padre"), Request.QueryString("bandera_hijo"), Request.QueryString("bandera_subhijo"))
                        Else
                            carga_padre(0, Session("IdMarca"))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub carga_padre(ByVal Intpadre As Integer, ByVal IdMarca As Integer)
        Dim dt As DataTable
        Dim filas As Integer
        Dim fila As Integer
        Dim SubFila As Integer
        Dim columnas As Integer
        Dim columna As Integer
        Dim padre As Integer = 0
        Dim contador As Integer = 0
        Dim bandera_carga As Integer = 0

        dt = ccliente.menu_administrador_padre(IdMarca)
        filas = dt.Rows.Count
        Try
            For fila = 1 To filas
                If fila = 1 Then
                    columnas = 2
                End If

                Dim tbrow As New TableRow
                For columna = 1 To columnas
                    Dim tbcell As New TableCell
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim i As Integer
                    If columna = 1 Then
                        bandera_carga = 1
                        Dim img As New HtmlImage
                        img.Src = "..\Imagenes\Menu\flecha.gif"
                        tbcell.Controls.Add(img)
                        tbcell.HorizontalAlign = HorizontalAlign.Right
                        tbcell.Width = Unit.Percentage(5)
                        tbcell.Height = Unit.Pixel(25)
                        tbrow.Controls.Add(tbcell)
                    Else
                        If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                            hp.Text = dt.Rows(fila - 1)("nombre_menu")
                        Else
                            hp.Text = ""
                        End If
                        If dt.Rows(fila - 1).IsNull("url") Or dt.Rows(fila - 1)("url") = "" Then
                            hp.Target = "menu"
                            hp.NavigateUrl = "WfrmBlanco.aspx?Bandera_padre=" & dt.Rows(fila - 1)("ancestro") & " &Bandera_hijo=" & dt.Rows(fila - 1)("Id_menu")
                        Else
                            hp.Target = "contenido"
                            hp.NavigateUrl = dt.Rows(fila - 1)("url")
                        End If
                        hp.Width = Unit.Percentage(100)
                        hp.Height = Unit.Pixel(25)
                        hp.CssClass = "rojo_administrador"
                        tbcell.Controls.Add(hp)
                        tbcell.HorizontalAlign = HorizontalAlign.Left
                        tbcell.Width = Unit.Percentage(95)
                        tbrow.Controls.Add(tbcell)
                        tbrow.CssClass = "rojo_administrador"
                    End If
                Next columna
                If bandera_carga = 1 Then
                    tbl_menu.Rows.Add(tbrow)
                    bandera_carga = 0
                End If
            Next fila
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub carga_hijo(ByVal Intpadre As Integer, _
        ByVal Inthijo As Integer)

        Dim dt As DataTable
        Dim filas As Integer
        Dim fila As Integer
        Dim SubFila As Integer
        Dim columnas As Integer
        Dim columna As Integer
        Dim padre As Integer = 0
        Dim hijo As Integer = 0
        Dim contador As Integer = 0
        Dim bandera_carga_subhijo As Integer = 0

        dt = ccliente.menu_administrador_hijo(Inthijo)
        filas = dt.Rows.Count
        Try
            For fila = 1 To filas
                If fila = 1 Then
                    columnas = 3
                End If

                Dim tbrow As New TableRow
                For columna = 1 To columnas
                    Dim tbcell As New TableCell
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim i As Integer

                    If dt.Rows(fila - 1)("ancestro") <> Inthijo Then
                        Dim tbcell1 As New TableCell

                        Dim img As New HtmlImage
                        img.Src = "..\Imagenes\Menu\flecha.gif"
                        tbcell1.Controls.Add(img)
                        tbcell1.ColumnSpan = 1
                        tbcell1.HorizontalAlign = HorizontalAlign.Right
                        tbcell1.Width = Unit.Pixel(5)
                        tbcell1.Height = Unit.Pixel(25)
                        tbrow.Controls.Add(tbcell1)

                        If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                            hp.Text = dt.Rows(fila - 1)("nombre_menu")
                        Else
                            hp.Text = ""
                        End If
                        If dt.Rows(fila - 1).IsNull("url") Or dt.Rows(fila - 1)("url") = "" Then
                            hp.Target = "menu"
                            hp.NavigateUrl = "WfrmBlanco.aspx?Bandera_padre=" & dt.Rows(fila - 1)("ancestro") & " "
                        Else
                            hp.Target = "contenido"
                            hp.NavigateUrl = dt.Rows(fila - 1)("url")
                        End If
                        hp.Width = Unit.Percentage(100)
                        hp.Height = Unit.Pixel(25)
                        tbcell.Controls.Add(hp)
                        tbcell.HorizontalAlign = HorizontalAlign.Justify
                        tbcell.ColumnSpan = 2
                        tbcell.VerticalAlign = VerticalAlign.Middle
                        tbcell.Width = Unit.Percentage(95)
                        tbrow.Controls.Add(tbcell)
                        tbrow.CssClass = "rojo_administrador"
                        bandera_carga_subhijo = 1
                        Exit For
                    Else
                        If Inthijo <> dt.Rows(fila - 1)("ancestro") Then Exit For

                        If dt.Rows(fila - 1)("ancestro") = Inthijo Then
                            If columna = 1 Then
                                bandera_carga_subhijo = 1
                                Dim lbl1 As New Label
                                lbl.Text = "&nbsp;"
                                tbcell.Controls.Add(lbl)
                                tbcell.Width = Unit.Percentage(5)
                                tbrow.Controls.Add(tbcell)
                            Else
                                If columna = 2 Then
                                    Dim img As New HtmlImage
                                    img.Src = "..\Imagenes\flecha.gif"
                                    img.Width = 10
                                    tbcell.Controls.Add(img)
                                    tbcell.HorizontalAlign = HorizontalAlign.Right
                                    tbcell.Width = Unit.Percentage(5)
                                    tbcell.Height = Unit.Pixel(25)
                                    tbrow.Controls.Add(tbcell)
                                    tbrow.Height = Unit.Pixel(25)
                                Else
                                    If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                                        hp.Text = dt.Rows(fila - 1)("nombre_menu")
                                    Else
                                        hp.Text = ""
                                    End If
                                    If dt.Rows(fila - 1).IsNull("url") Or dt.Rows(fila - 1)("url") = "" Then
                                        hp.Target = "menu"
                                        hp.NavigateUrl = "WfrmBlanco.aspx?Bandera_padre=0 &Bandera_hijo=" & dt.Rows(fila - 1)("ancestro") & " &Bandera_subhijo=" & dt.Rows(fila - 1)("id_menu")
                                    Else
                                        hp.Target = "contenido"
                                        hp.NavigateUrl = dt.Rows(fila - 1)("url")
                                    End If
                                    hp.Width = Unit.Percentage(100)
                                    hp.Height = Unit.Pixel(25)
                                    tbcell.Controls.Add(hp)
                                    tbcell.HorizontalAlign = HorizontalAlign.Left
                                    tbcell.Width = Unit.Percentage(90)
                                    tbrow.Controls.Add(tbcell)
                                    tbrow.Height = Unit.Pixel(25)
                                    tbrow.CssClass = "rojo_hijo"
                                    hijo = dt.Rows(fila - 1)("id_menu")
                                End If
                            End If
                        End If
                    End If
                Next columna
                If bandera_carga_subhijo = 1 Then
                    tbl_menu.Rows.Add(tbrow)
                    bandera_carga_subhijo = 0
                End If
            Next fila
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub carga_subhijo(ByVal Intpadre As Integer, _
        ByVal Inthijo As Integer, ByVal Intsubhijo As Integer)

        Dim dt As DataTable
        Dim filas As Integer
        Dim fila As Integer
        Dim SubFila As Integer
        Dim columnas As Integer
        Dim columna As Integer
        Dim padre As Integer = 0
        Dim hijo As Integer = 0
        Dim contador As Integer = 0
        Dim bandera_carga_subhijo As Integer = 0

        dt = ccliente.menu_administrador_subhijo(Intpadre, Inthijo, Intsubhijo)
        filas = dt.Rows.Count
        Try
            For fila = 1 To filas
                If fila = 1 Then
                    columnas = 4
                End If

                Dim tbrow As New TableRow
                For columna = 1 To columnas
                    Dim tbcell As New TableCell
                    Dim lbl As New Label
                    Dim hp As New HyperLink
                    Dim i As Integer

                    If dt.Rows(fila - 1)("ancestro") <> Inthijo And _
                    dt.Rows(fila - 1)("ancestro") <> Intsubhijo Then
                        Dim tbcell1 As New TableCell

                        Dim img As New HtmlImage
                        img.Src = "..\Imagenes\Menu\flecha.gif"
                        tbcell1.Controls.Add(img)
                        tbcell1.ColumnSpan = 1
                        tbcell1.HorizontalAlign = HorizontalAlign.Right
                        tbcell1.Width = Unit.Percentage(5)
                        tbcell1.Height = Unit.Pixel(25)
                        tbrow.Controls.Add(tbcell1)

                        If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                            hp.Text = dt.Rows(fila - 1)("nombre_menu")
                        Else
                            hp.Text = ""
                        End If
                        If dt.Rows(fila - 1).IsNull("url") Or dt.Rows(fila - 1)("url") = "" Then
                            hp.Target = "menu"
                            hp.NavigateUrl = "WfrmBlanco.aspx?Bandera_padre=" & dt.Rows(fila - 1)("ancestro") & " "
                        Else
                            hp.Target = "contenido"
                            hp.NavigateUrl = dt.Rows(fila - 1)("url")
                        End If
                        hp.Width = Unit.Percentage(100)
                        hp.Height = Unit.Pixel(25)
                        tbcell.Controls.Add(hp)
                        tbcell.HorizontalAlign = HorizontalAlign.Justify
                        tbcell.ColumnSpan = 3
                        tbcell.VerticalAlign = VerticalAlign.Middle
                        tbcell.Width = Unit.Percentage(95)
                        tbrow.Controls.Add(tbcell)
                        tbrow.CssClass = "rojo_administrador"
                        bandera_carga_subhijo = 1
                        Exit For
                    Else
                        If Inthijo <> dt.Rows(fila - 1)("ancestro") And _
                        dt.Rows(fila - 1)("ancestro") <> Intsubhijo Then Exit For

                        If dt.Rows(fila - 1)("ancestro") = Inthijo Then
                            If columna <> 4 Then
                                If columna = 1 Then
                                    bandera_carga_subhijo = 1
                                    Dim lbl1 As New Label
                                    lbl.Text = "&nbsp;"
                                    tbcell.Controls.Add(lbl)
                                    tbcell.Width = Unit.Percentage(5)
                                    tbrow.Controls.Add(tbcell)
                                Else
                                    If columna = 2 Then
                                        Dim img As New HtmlImage
                                        img.Src = "..\Imagenes\flecha.gif"
                                        img.Width = 10
                                        tbcell.Controls.Add(img)
                                        tbcell.HorizontalAlign = HorizontalAlign.Right
                                        tbcell.Width = Unit.Percentage(5)
                                        tbcell.Height = Unit.Pixel(25)
                                        tbrow.Controls.Add(tbcell)
                                        tbrow.Height = Unit.Pixel(25)
                                    Else
                                        If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                                            hp.Text = dt.Rows(fila - 1)("nombre_menu")
                                        Else
                                            hp.Text = ""
                                        End If
                                        If dt.Rows(fila - 1).IsNull("url") Or dt.Rows(fila - 1)("url") = "" Then
                                            hp.Target = "menu"
                                            hp.NavigateUrl = "WfrmBlanco.aspx?Bandera_padre=0 &Bandera_hijo=" & dt.Rows(fila - 1)("ancestro") & " &Bandera_subhijo=" & dt.Rows(fila - 1)("id_menu")
                                        Else
                                            hp.Target = "contenido"
                                            hp.NavigateUrl = dt.Rows(fila - 1)("url")
                                        End If
                                        hp.Width = Unit.Percentage(100)
                                        hp.Height = Unit.Pixel(25)
                                        tbcell.Controls.Add(hp)
                                        tbcell.HorizontalAlign = HorizontalAlign.Left
                                        tbcell.ColumnSpan = 2
                                        tbcell.Width = Unit.Percentage(90)
                                        tbrow.Controls.Add(tbcell)
                                        tbrow.Height = Unit.Pixel(25)
                                        tbrow.CssClass = "rojo_administrador"
                                        bandera_carga_subhijo = 1
                                    End If
                                End If
                            End If
                        Else
                            If columna = 1 Then
                                bandera_carga_subhijo = 1
                                Dim lbl1 As New Label
                                lbl.Text = "&nbsp;"
                                tbcell.Controls.Add(lbl)
                                tbcell.Width = Unit.Percentage(5)
                                tbrow.Controls.Add(tbcell)
                            Else
                                If columna = 2 Then
                                    bandera_carga_subhijo = 1
                                    Dim lbl1 As New Label
                                    lbl.Text = "&nbsp;"
                                    tbcell.Controls.Add(lbl)
                                    tbcell.Width = Unit.Percentage(5)
                                    tbrow.Controls.Add(tbcell)
                                Else
                                    If columna = 3 Then
                                        Dim img As New HtmlImage
                                        img.Src = "..\Imagenes\bullet_hl.gif"
                                        img.Width = 10
                                        tbcell.Controls.Add(img)
                                        tbcell.HorizontalAlign = HorizontalAlign.Right
                                        tbcell.Width = Unit.Percentage(5)
                                        tbcell.Height = Unit.Pixel(25)
                                        tbrow.Controls.Add(tbcell)
                                        tbrow.Height = Unit.Pixel(25)
                                    Else
                                        If Not dt.Rows(fila - 1).IsNull("nombre_menu") Then
                                            hp.Text = dt.Rows(fila - 1)("nombre_menu")
                                        Else
                                            hp.Text = ""
                                        End If
                                        hp.Target = "contenido"
                                        hp.NavigateUrl = dt.Rows(fila - 1)("url")
                                        hp.Width = Unit.Percentage(100)
                                        hp.Height = Unit.Pixel(25)
                                        tbcell.Controls.Add(hp)
                                        tbcell.HorizontalAlign = HorizontalAlign.Left
                                        tbcell.Width = Unit.Percentage(85)
                                        tbrow.Controls.Add(tbcell)
                                        tbrow.Height = Unit.Pixel(25)
                                        tbrow.CssClass = "rojo_hijo"
                                        hijo = dt.Rows(fila - 1)("id_menu")
                                        bandera_carga_subhijo = 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next columna
                If bandera_carga_subhijo = 1 Then
                    tbl_menu.Rows.Add(tbrow)
                    bandera_carga_subhijo = 0
                End If
            Next fila
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
