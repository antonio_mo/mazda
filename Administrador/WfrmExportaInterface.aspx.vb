﻿Imports CN_Negocios

Public Class WfrmExportaInterface
    Inherits System.Web.UI.Page
    Private nombreArchivo As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hlSalida.Visible = False
        imgSalida.Visible = False
        If (Not IsPostBack) Then
            ddlPoliza.Items.Clear()
            ddlPoliza.Items.Add(New System.Web.UI.WebControls.ListItem("Emitidas", 0))
            ddlPoliza.Items.Add(New System.Web.UI.WebControls.ListItem("Canceladas", 1))
            ddlPoliza.DataBind()
            ddlPoliza.SelectedValue = 0
        End If
    End Sub

    Protected Sub cmd_generar_Click(sender As Object, e As EventArgs) Handles cmd_generar.Click
        Try
            Dim ccliente As New CnGeneraLayout
            'Dim NombreArchivo As String = String.Empty

            If (txt_diaRep.Text.Trim() <> String.Empty) Then
                nombreArchivo = ccliente.GetInterfaceSAARS(txt_diaRep.Text, ddlPoliza.SelectedValue)
            Else
                MsgBox.ShowMessage("Es necesario ingresar una fecha")
                Exit Sub
            End If

            If NombreArchivo = "" Then
                MsgBox.ShowMessage("No hay información para enviar por interface")
                Exit Sub
            Else
                Dim url As String = ConfigurationManager.AppSettings("Script") & nombreArchivo & ".txt"

                Response.Clear()
                Response.ContentType = "text/plain"
                Response.AddHeader("Content-Disposition", "attachment; filename=Archivo de " & IIf(ddlPoliza.SelectedValue = 1, "Bajas", "Altas") & ".txt")
                Response.TransmitFile(url)
                Response.End()
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Protected Sub pc_reporte_SelectionChanged(sender As Object, e As EventArgs) Handles pc_reporte.SelectionChanged
        Dim ban As Integer = 0
        Dim fechas As String

        If Not txt_diaRep.Text = "" Then
            Dim fecha() As String = txt_diaRep.Text.Split("/")
            fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
        End If
    End Sub

    Public Function valida_fecha(ByVal strfecha As String) As String
        If strfecha.Length = 1 Then
            strfecha = "0" & strfecha
        End If
        Return strfecha
    End Function

    Protected Sub txt_diaRep_TextChanged(sender As Object, e As EventArgs) Handles txt_diaRep.TextChanged
        Dim ban As Integer = 0
        Dim fechas As String

        If Not txt_diaRep.Text = "" Then
            Dim fecha() As String = txt_diaRep.Text.Split("/")
            fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
        End If
    End Sub

    Protected Sub imgSalida_Click(sender As Object, e As ImageClickEventArgs)
        Dim url As String = ConfigurationManager.AppSettings("Script") & nombreArchivo & ".txt"
        Dim nombre As String = ""

        Response.Buffer = False
        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "text/plain"
        Response.AddHeader("Content-Disposition", "attachment; filename='Archivo de Altas.txt'")
        Response.WriteFile(url)
        Response.End()
    End Sub
End Class