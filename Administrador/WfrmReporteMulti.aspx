<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmReporteMulti.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmReporteMulti" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmReporteMulti</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function PonDiagonal()
		{
			var stCadena=document.reporte.txt_fechai.value;
			if (IsNumeric(stCadena))
			{
				if(stCadena.length == 2)
				{
					document.reporte.txt_fechai.value=document.reporte.txt_fechai.value+"/"
				}
				if(stCadena.length == 5)
				{
					document.reporte.txt_fechai.value=document.reporte.txt_fechai.value+"/"
				}	
			}
			else
			{
				document.reporte.txt_fechai.value='';
			}
		}
		function PonDiagonal1()
		{
			var stCadena=document.reporte.txt_fechaf.value;
			if (IsNumeric(stCadena))
			{
				if(stCadena.length == 2)
				{
					document.reporte.txt_fechaf.value=document.reporte.txt_fechaf.value+"/"
				}
				if(stCadena.length == 5)
				{
					document.reporte.txt_fechaf.value=document.reporte.txt_fechaf.value+"/"
				}	
			}
			else
			{
				document.reporte.txt_fecha.value='';
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="reporte" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Reporte 
							General</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio">Seleccione el Programa :</TD>
					<TD><asp:dropdownlist id="cbo_programa" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD align="right">
						<asp:Label id="lbl_reporte" runat="server" Width="100%" CssClass="obligatorio" Visible="False">Descargar Reporte :</asp:Label></TD>
					<TD>
						<asp:HyperLink id="hp_reporte" runat="server" ImageUrl="..\imagenes\menu\page_down.gif" Target="contenido"
							Visible="False">Reporte</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD class="negrita">Fecha Inicial :</TD>
					<TD><asp:textbox id="txt_fechai" onkeyup="PonDiagonal()" tabIndex="10" runat="server" Width="45%"
							CssClass="combos_small" MaxLength="10" AutoPostBack="True"></asp:textbox>&nbsp;&nbsp;
						<rjs:popcalendar id="Popcalendar1" runat="server" Width="8px" AutoPostBack="False" ShowErrorMessage="False"
							Control="txt_fechai" Separator="/" BorderStyle="Solid" InvalidDateMessage="D�a Inv�lido" Shadow="True"
							ShowWeekend="True" Move="True" Fade="0.5" RequiredDateMessage="La Fecha es Requerida" Culture="es-MX Espa�ol (M�xico)"
							Buttons="[<][m][y]  [>]" TextMessage="La fecha es Incorrecta" BackColor="Yellow" BorderWidth="1px"
							BorderColor="Black" Height="8px" To-Today="True"></rjs:popcalendar></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="negrita">Fecha Final :&nbsp;</TD>
					<TD><asp:textbox id="txt_fechaf" onkeyup="PonDiagonal1()" tabIndex="10" runat="server" Width="45%"
							CssClass="combos_small" MaxLength="10" AutoPostBack="True"></asp:textbox>&nbsp;&nbsp;
						<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" AutoPostBack="False" ShowErrorMessage="False"
							Control="txt_fechaf" Separator="/" BorderStyle="Solid" InvalidDateMessage="D�a Inv�lido" Shadow="True"
							ShowWeekend="True" Move="True" Fade="0.5" RequiredDateMessage="La Fecha es Requerida" Culture="es-MX Espa�ol (M�xico)"
							Buttons="[<][m][y]  [>]" TextMessage="La fecha es Incorrecta" BackColor="Yellow" BorderWidth="1px"
							BorderColor="Black" Height="8px" To-Today="True" From-Control="txt_fechai"></rjs:popcalendar></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="negrita">Producto :</TD>
					<TD><asp:dropdownlist id="cbo_producto" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Generar Reporte :</TD>
					<TD>
						<asp:Button id="cmd_generar" runat="server" CssClass="boton" Text="Procesar"></asp:Button></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%">
						<Ontranet:SaveDialog id="sd" runat="server"></Ontranet:SaveDialog></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; LEFT: 392px; POSITION: absolute; TOP: 528px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
