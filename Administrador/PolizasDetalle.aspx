﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PolizasDetalle.aspx.vb" Inherits="Cotizador_Mazda_Retail.PolizasDetalle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        .tdLabel {
            text-align:right;
        }
    </style> 
    <LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
    <style type="text/css" rel="stylesheet">
        #header1 UL { PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 0px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px; PADDING-TOP: 10px }
	    #header1 LI { PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; DISPLAY: inline; PADDING-TOP: 0px }
	    #header1 A { BORDER-BOTTOM: #765 1px solid; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 9px; PADDING-RIGHT: 0px; BACKGROUND: url(..\Imagenes\Pestanas\left_both.gif) no-repeat left top; FLOAT: left; TEXT-DECORATION: none; PADDING-TOP: 0px }
	    #header1 A SPAN { PADDING-BOTTOM: 4px; PADDING-LEFT: 6px; PADDING-RIGHT: 15px; DISPLAY: block; BACKGROUND: url(..\Imagenes\Pestanas\right_both.gif) no-repeat right top; FLOAT: left; COLOR: #333; FONT-WEIGHT: bold; PADDING-TOP: 5px }
	    #header1 A SPAN { FLOAT: none }
	    #header1 A:hover SPAN { COLOR: #333 }
	    #header1 #current A { BORDER-RIGHT-WIDTH: 0px; BACKGROUND-POSITION: 0px -150px; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px }
	    #header1 #current A SPAN { PADDING-BOTTOM: 5px; BACKGROUND-POSITION: 100% -1px; COLOR: #333 }
	    #header1 A:hover { BACKGROUND-POSITION: 0% -150px }
	    #header1 A:hover SPAN { BACKGROUND-POSITION: 100% -150px }
	</style>
</head>
<body style="font-family:Arial;     ">
    <form id="frmPolizas" runat="server">
        <div style="align-content:center;width:100%;">
            <table class="frm">
                <tr>
                    <td style="width:15%;"></td>
                    <td style="width:10%;" class="title">
                        <asp:Label ID="lblPoliza" runat="server" Text="Poliza:" />
                    </td>
                    <td style="width:40%;">
                        <asp:TextBox ID="txtFdPoliza" runat="server" Width="100%" CssClass="searchBox box_200" />
                    </td>
                    <td style="width:20%;">
                        <asp:Button ID="btnFindPol" runat="server" Text="Buscar" Width="100%" CssClass="button" />
                    </td>
                    <td style="width:15%;"></td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblResult" runat="server" style="color:red;font-weight:bold;"></asp:Label>
            <br />
            <br />
            <div id="divGrid" runat="server" style="width:100%;align-content:center;">
                <asp:DataGrid ID="dgLista" runat="server" AutoGenerateColumns="False" BorderColor="Silver" BorderStyle="None" CellPadding="4" Width="80%"
                        DataKeyField="IdPoliza" AllowPaging="True" PageSize="50" CssClass="Datagrid" HeaderStyle-CssClass="thader" AlternatingItemStyle-CssClass="alt">
                    <FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
					<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
					<EditItemStyle Wrap="False"></EditItemStyle>
					<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
					<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
                    <SelectedItemStyle Font-Bold="True" ForeColor="#663399" BackColor="#FFCC66"></SelectedItemStyle>
                    <Columns>                        
                        <asp:BoundColumn DataField="Poliza" HeaderText="Poliza"></asp:BoundColumn>
                        <asp:ButtonColumn Text="&lt;img border ='0' src='..\Imagenes\general\grid01.gif' /&gt;" HeaderText="Ver" CommandName="Select">

                            <HeaderStyle HorizontalAlign="Center" CssClass="header"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:ButtonColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Position="TopAndBottom" Mode="NumericPages" CssClass="pgr">
                    </PagerStyle>
                </asp:DataGrid>
            </div>
            <div id="divPoliza" runat="server" style="align-content:center;">
                <table style="width:70%;">
                    <tr>
                        <td style="text-align:right;width:30%;">Id Poliza:</td>
                        <td><asp:TextBox ID="txtIdPoliza" runat="server" Width="98%" ReadOnly="true" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Poliza:</td>
                        <td><asp:TextBox ID="txtPoliza" runat="server" Width="98%" /></td>
                    </tr>                    
                    <tr>
                        <td style="text-align:right;width:30%;">IdBid:</td>
                        <td><asp:TextBox ID="txtIdBid" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">IdAseguradora:</td>
                        <td><asp:TextBox ID="txtIdAseguradora" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Fecha Vigencia:</td>
                        <td><asp:TextBox ID="txtFechaVig" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Fecha Fin:</td>
                        <td><asp:TextBox ID="txtFechaFin" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Fecha Registro:</td>
                        <td><asp:TextBox ID="txtFechaRegistro" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Marca:</td>
                        <td><asp:TextBox ID="txtMarca" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Modelo:</td>
                        <td><asp:TextBox ID="txtModelo" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Descripcion:</td>
                        <td><asp:TextBox ID="txtDescripcion" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Anio:</td>
                        <td><asp:TextBox ID="txtAnio" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Serie:</td>
                        <td><asp:TextBox ID="txtSerie" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Cobertura:</td>
                        <td><asp:TextBox ID="txtCobertura" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Capacidad:</td>
                        <td><asp:TextBox ID="txtCapacidad" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Motor:</td>
                        <td><asp:TextBox ID="txtMotor" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Prima Neta:</td>
                        <td><asp:TextBox ID="txtPrimaNeta" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Derecho de Poliza:</td>
                        <td><asp:TextBox ID="txtDerPol" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Iva:</td>
                        <td><asp:TextBox ID="txtIva" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Prima Total:</td>
                        <td><asp:TextBox ID="txtPrimaTotal" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Pago Inicial:</td>
                        <td><asp:TextBox ID="txtPagoInicial" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Plazo:</td>
                        <td><asp:TextBox ID="txtPlazo" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Tipo de Carga:</td>
                        <td><asp:TextBox ID="txtTipoCarga" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Interface Cancelacion:</td>
                        <td><asp:TextBox ID="txtInterfaceCancelacion" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Id Cancelacion:</td>
                        <td><asp:TextBox ID="txtIdCancelacion" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;width:30%;">Fecha Cancelacion:</td>
                        <td><asp:TextBox ID="txtFechaCancelacion" runat="server" Width="98%" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
