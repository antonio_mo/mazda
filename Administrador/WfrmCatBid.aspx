<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCatBid.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCatBid" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmCatBid</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" width="20%">Bid Ford :</TD>
					<TD width="25%">
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_bidf" runat="server" Width="30%"
							CssClass="combos_small" MaxLength="5" TabIndex="1"></asp:TextBox></TD>
					<TD width="25%"></TD>
					<TD width="30%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Bid Aon :
					</TD>
					<TD>
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_bida" runat="server" Width="30%"
							CssClass="combos_small" MaxLength="6" TabIndex="1"></asp:TextBox></TD>
					<TD></TD>
					<TD>
						<asp:TextBox id="txt_id" runat="server" Visible="False" Width="20px"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Nombre Distribuidor :</TD>
					<TD colSpan="2">
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_nombre" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="100" TabIndex="2"></asp:TextBox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Estatus :</TD>
					<TD>
						<asp:DropDownList id="cbo_Estatus" runat="server" Width="60%" CssClass="combos" TabIndex="3">
							<asp:ListItem Value="P" Selected="True">--Seleccionar--</asp:ListItem>
							<asp:ListItem Value="1">Activo</asp:ListItem>
							<asp:ListItem Value="0">Inactivo</asp:ListItem>
						</asp:DropDownList></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD>
						<asp:DropDownList id="cbo_marca" runat="server" Width="100%" CssClass="combos" AutoPostBack="True" TabIndex="4" Visible="False"></asp:DropDownList></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center">
						<asp:ImageButton id="cm_Guardar" runat="server" ImageUrl="..\Imagenes\botones\guardar.gif" Width="80px" TabIndex="5"></asp:ImageButton></TD>
					<TD>
						<asp:ImageButton id="cmd_Nuevo" runat="server" ImageUrl="..\Imagenes\botones\nuevo.gif" Width="80px" TabIndex="6"></asp:ImageButton></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="4" class="auto-style1">
						<asp:datagrid id="grdCatalogo" runat="server" CssClass="Datagrid"
									Width="592px" CellPadding="1" AutoGenerateColumns="False" TabIndex="7">
									<SelectedItemStyle Wrap="False" CssClass="NormalItems"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
									<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
									<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="id_bid"></asp:BoundColumn>
										<asp:BoundColumn DataField="bid_ford" HeaderText="Bid_Ford">
											<ItemStyle Wrap="False" Width="15%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="bid_aon" HeaderText="Bid_Aon">
											<ItemStyle Wrap="False" Width="15%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="empresa" HeaderText="Distribuidor">
											<ItemStyle Wrap="False" Width="25%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="estatus"></asp:BoundColumn>
										<asp:BoundColumn DataField="marca" HeaderText="Marca">
											<ItemStyle Wrap="False" Width="20%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="est" HeaderText="Estatus">
											<ItemStyle Wrap="False" Width="20%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="Id_Marca"></asp:BoundColumn>
										<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
											HeaderText="Del." CommandName="Delete"></asp:ButtonColumn>
										<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Sel."
											CommandName="Select">
											<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
										</asp:ButtonColumn>
									</Columns>
									<PagerStyle Wrap="False"></PagerStyle>
								</asp:datagrid>
						</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 104; LEFT: 360px; POSITION: absolute; TOP: 512px" runat="server"></cc1:MsgBox>
			<HR class="lineas" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table5" style="Z-INDEX: 112; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0"> &nbsp;Cat�logo 
							Distribuidor</FONT></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
