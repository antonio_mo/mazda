<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmConfirmaCoti.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmConfirmaCoti"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmConfirmaCoti</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function RadioChecked(param)
		{
			{ 
				var frm = document.forms[0];
				// Take all elements of the form
				for (i=0; i < frm.length; i++)
				{
					// itinerate the elements searching "RadioButtons"
					if (frm.elements[i].type == "radio")
					{
					// Unchecked if the RadioButton is != param
					if (param != frm.elements[i].id )
					{
						frm.elements[i].checked = false;
					}
					}
				}
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 8px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 102; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Confirma 
							cotizaci�n</FONT></TD>
				</TR>
			</TABLE>
			<TABLE style="Z-INDEX: 103; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table1" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD width="15%"></TD>
					<TD width="35%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="Header_Gris">B�squeda
					</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE style="HEIGHT: 16px" id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
							<TR>
								<TD vAlign="top" width="25%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" Width="100%" CssClass="combos_small"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" width="5%" align="right"></TD>
								<TD vAlign="top" width="70%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="1"
										Height="56px">
										<asp:ListItem Value="0">No. Cotizaci&#243;n</asp:ListItem>
										<asp:ListItem Value="1">Distribuidor</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3">
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Height="24px" Text="Buscar"></asp:button></TD>
				</TR>
				<TR>
					<TD width="25%" colSpan="4">
						<asp:datagrid id="grd_reimprime" runat="server" Width="100%" CssClass="datagrid" AllowSorting="True"
							CellPadding="1" AutoGenerateColumns="False">
							<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
							<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Sel.">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:RadioButton id="opt_Seleccion" runat="server" CssClass="negrita"></asp:RadioButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="noCotizacion" HeaderText="No. Cotizaci&#243;n">
									<ItemStyle Width="15%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="empresa" HeaderText="Distribuidor">
									<ItemStyle Width="60%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="confirmada" HeaderText="Estatus">
									<ItemStyle Width="20%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="id_cotizacionunica"></asp:BoundColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Observaciones :</TD>
					<TD class="obligatorio" colSpan="2">
						<asp:textbox style="Z-INDEX: 0" id="txt_obs" tabIndex="1" runat="server" MaxLength="50" CssClass="combos_small"
							Width="100%" Height="60px" TextMode="MultiLine"></asp:textbox></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">
						<asp:Button id="cmd_confirmar" runat="server" CssClass="boton" Text="Confirmar"></asp:Button></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox style="Z-INDEX: 104; POSITION: absolute; TOP: 504px; LEFT: 328px" id="MsgBox" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
