Imports CN_Negocios
Partial Class WfrmCatBid
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
    Private ccliente As New CnCatalogo
    Private cvalida As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                'carga_marca()
                carga_grd()
                limpia_campos()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Public Sub carga_marca()
    '    Dim dt As DataTable = ccliente.carga_marca
    '    If dt.Rows.Count > 0 Then
    '        cbo_marca.DataSource = dt
    '        cbo_marca.DataTextField = "marca"
    '        cbo_marca.DataValueField = "id_marca"
    '        cbo_marca.DataBind()
    '    End If
    'End Sub

    Public Sub carga_grd()
        Dim dt As DataTable = ccliente.carga_grd_bid
        If dt.Rows.Count > 0 Then
            grdCatalogo.DataSource = dt
            grdCatalogo.DataBind()
        End If
    End Sub

    Private Sub cm_Guardar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cm_Guardar.Click
        Try
            Dim Marca As Integer = 1
            Dim BidConauto As String
            Dim BidAon As String
            Dim i As Integer

            'Bid Ford
            If txt_bidf.Text = "" Then
                MsgBox.ShowMessage("El n�mero de Bid Ford no puede quedar en blanco, verifiquelo")
                Exit Sub
            ElseIf Not IsNumeric(txt_bidf.Text) Then
                MsgBox.ShowMessage("El n�mero de Bid Ford debe de ser n�merico, verifiquelo")
                Exit Sub
            ElseIf Len(txt_bidf.Text) <> 5 Then
                MsgBox.ShowMessage("El n�mero de Bid Ford debe de ser de 5 d�gitos, verifiquelo")
                Exit Sub
            End If

            BidConauto = txt_bidf.Text

            'Bid Aon
            If txt_bida.Text = "" Then
                MsgBox.ShowMessage("El n�mero de Bid Aon no puede quedar en blanco, verifiquelo")
                Exit Sub
            ElseIf Len(txt_bida.Text) <> 6 Then
                MsgBox.ShowMessage("El n�mero de Bid Aon debe de ser de 6 caracteres, verifiquelo")
                Exit Sub
            ElseIf Mid(txt_bida.Text, 1, 1) <> "F" And Mid(txt_bida.Text, 1, 1) <> "f" Then
                MsgBox.ShowMessage("El primer car�cter de Bid Aon esta incorrecto debe ser la letra F, verifiquelo")
                Exit Sub
            ElseIf Len(txt_bida.Text) = 6 Then
                BidAon = Mid(txt_bida.Text, 2, 6)
                For i = 1 To Len(txt_bida.Text)
                    If i > 1 Then
                        BidAon = Mid(txt_bida.Text, i, 1)
                        If Not IsNumeric(BidAon) Then
                            MsgBox.ShowMessage("Los �ltimos 5 caracteres de Bid Aon deben ser n�mericos, verifiquelo")
                            Exit Sub
                        End If
                    End If
                Next
            End If

            If Mid(txt_bida.Text, 2, 5) <> txt_bidf.Text Then
                MsgBox.ShowMessage("El n�mero del Bid Aon es diferente al bid ford, verifiquelo")
                Exit Sub
            End If

            'Distribuidor
            If txt_nombre.Text = "" Then
                MsgBox.ShowMessage("El nombre del distribuidor no puede quedar en blanco, verifiquelo")
                Exit Sub
            End If

            'If cbo_marca.SelectedValue = 0 Then
            '    MsgBox.ShowMessage("Debe de seleccionar la marca a la que pertenece el Bid, verifiquelo")
            '    Exit Sub
            'End If

            If cbo_Estatus.SelectedValue = "P" Then
                cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue(0))
            End If

            If txt_id.Text = "" Then
                'valida si ya existe el bid Conauto, bid Ford y el bid Aon                
                Dim dt As DataTable = cvalida.VerificaInformacionBid(cvalida.valida_cadena(BidConauto), cvalida.valida_cadena(txt_bidf.Text), _
                cvalida.valida_cadena(UCase(txt_bida.Text)))
                If dt.Rows.Count > 0 Then
                    MsgBox.ShowMessage("Ya existe el Bid, verifiquelo")
                    Exit Sub
                End If

                ccliente.inserta_bid(cvalida.valida_cadena(BidConauto), cvalida.valida_cadena(txt_bidf.Text), _
                cvalida.valida_cadena(UCase(txt_bida.Text)), cvalida.valida_cadena(txt_nombre.Text), cbo_Estatus.SelectedValue, Marca)
                MsgBox.ShowMessage("Los datos del Bid se registraron correctamente")
            Else
                ccliente.actualiza_bid(txt_id.Text, cvalida.valida_cadena(BidConauto), cvalida.valida_cadena(txt_bidf.Text), _
                cvalida.valida_cadena(UCase(txt_bida.Text)), cvalida.valida_cadena(txt_nombre.Text), cbo_Estatus.SelectedValue, Marca)
                MsgBox.ShowMessage("Los datos del Bid se actualizaron correctamente")
            End If
            carga_grd()
            limpia_campos()

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
            'MsgBox.ShowMessage("Los datos del Bid no se registraron correctamente")
        End Try
    End Sub

    Protected Sub cmd_Nuevo_Click(sender As Object, e As ImageClickEventArgs) Handles cmd_Nuevo.Click
        carga_grd()
        limpia_campos()
    End Sub

    Public Sub limpia_campos()
        txt_id.Text = ""
        txt_bidf.Text = ""
        txt_bida.Text = ""
        txt_nombre.Text = ""
        cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue("P"))
        'cbo_marca.SelectedIndex = cbo_marca.Items.IndexOf(cbo_marca.Items.FindByValue(0))
    End Sub

    Protected Sub grdCatalogo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCatalogo.SelectedIndexChanged
        Dim cuenta As Integer = 0
        Dim i As Integer = 0
        Dim idBid As Integer = grdCatalogo.Items(grdCatalogo.SelectedIndex).Cells(0).Text

        Try
            Dim dt As DataTable = ccliente.carga_datos_distribuidor(idBid)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("id_bid") Then
                    txt_id.Text = ""
                Else
                    txt_id.Text = dt.Rows(0)("id_bid")
                End If

                If dt.Rows(0).IsNull("bid_ford") Then
                    txt_bidf.Text = ""
                Else
                    txt_bidf.Text = Trim(dt.Rows(0)("bid_ford"))
                End If

                If dt.Rows(0).IsNull("bid_aon") Then
                    txt_bida.Text = ""
                Else
                    txt_bida.Text = Trim(dt.Rows(0)("bid_aon"))
                End If

                If dt.Rows(0).IsNull("empresa") Then
                    txt_nombre.Text = ""
                Else
                    txt_nombre.Text = Trim(dt.Rows(0)("empresa"))
                End If
               
                If dt.Rows(0).IsNull("estatus") Then
                    cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue(0))
                Else
                    cbo_Estatus.SelectedIndex = cbo_Estatus.Items.IndexOf(cbo_Estatus.Items.FindByValue(dt.Rows(0)("estatus")))
                End If

            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
