Imports Emisor_Conauto
Imports CN_Negocios

Partial Class WfrmReporteMulti
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Request.QueryString("strnombre") Is Nothing Then
                    If Request.QueryString("imp") Is Nothing Then
                        limpia()
                        carga_programa()
                        SetFocus(cbo_programa)
                    Else
                        visualiza_reporte()
                    End If
                Else
                    descarga_archivo(Request.QueryString("strnombre"), Request.QueryString("strurl"))
                    limpia()
                    carga_programa()
                    SetFocus(cbo_programa)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        txt_fechai.Text = ""
        txt_fechaf.Text = ""
        cbo_producto.SelectedIndex = cbo_producto.Items.IndexOf(cbo_producto.Items.FindByText("-- Seleccione --"))
    End Sub

    Public Sub carga_programa()
        Dim dt As DataTable = ccliente.programas_Reporte_usuario(Session("usuario"), Session("nivel"))
        If dt.Rows.Count > 0 Then
            cbo_programa.DataSource = dt
            cbo_programa.DataTextField = "descripcion_programa"
            cbo_programa.DataValueField = "id_programa"
            cbo_programa.DataBind()
        End If
    End Sub

    Public Sub carga_producto()
        Dim arr As New ArrayList
        arr.Add("-- Seleccione --")
        Select Case cbo_programa.SelectedValue
            Case 1, 19
                arr.Add("Da�os")
            Case 3
                arr.Add("Da�os")
            Case 5
                arr.Add("Todo")
                arr.Add("Da�os")
                arr.Add("Vida")
                'arr.Add("Desempleo")
            Case 7, 8, 12, 13
                arr.Add("Da�os")
            Case 9, 11
                arr.Add("Todo")
                arr.Add("Da�os")
                arr.Add("Vida")
                'arr.Add("Desempleo")
        End Select

        cbo_producto.DataSource = arr
        cbo_producto.DataBind()
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub cbo_programa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_programa.SelectedIndexChanged
        If cbo_programa.SelectedValue > 0 Then
            carga_producto()
        End If
    End Sub

    Public Sub visualiza_reporte()
        Dim strurl As String
        Dim dt As DataTable = ccliente.ImpReporte(Session("Llave"))
        Try
            If dt.Rows.Count > 0 Then
                hp_reporte.Visible = True
                lbl_reporte.Visible = True
                hp_reporte.NavigateUrl = "WfrmReporteMulti.aspx?strnombre=" & dt.Rows(0)("nombre_reporte") & "&strurl=" & dt.Rows(0)("url_reporte") & ""
                limpia()
                carga_programa()
                SetFocus(cbo_programa)
            Else
                Response.Redirect("HEsperaR.htm")
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub descarga_archivo(ByVal strnombre As String, ByVal strurl As String)
        Try
            'strurl = "C:/Inetpub/wwwroot/CotizadorContado_Conauto/" & strurl
            strurl = "D:/Aplicaciones/CotizadorContado_Conauto/" & strurl
            sd.FileName = strnombre
            sd.File = strurl
            sd.ShowDialogBox()
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_generar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_generar.Click
        Dim inttipo As Integer
        Dim cadenaEnvio As String = ""
        Try
            If cbo_programa.SelectedValue = 0 Then
                MsgBox.ShowMessage("Favor de seleccionar el programa")
                Exit Sub
            End If
            If cbo_producto.SelectedValue = "-- Seleccione --" Then
                MsgBox.ShowMessage("Favor de seleccionar el producto")
                Exit Sub
            End If
            Select Case cbo_producto.SelectedValue
                Case "Da�os"
                    inttipo = 1
                Case "Vida"
                    inttipo = 2
                Case "Desempleo"
                    inttipo = 3
                Case "Todo"
                    inttipo = 0
            End Select
            Dim llave As String = ccliente.inserta_contenedor(cbo_programa.SelectedValue)
            Session("Llave") = llave
            cadenaEnvio = "9|" & cbo_programa.SelectedValue & "|" & txt_fechai.Text & "|" & txt_fechaf.Text & "|" & inttipo & "|" & llave

            Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            _bsPolizas.Export(cadenaEnvio)
            'reporte.carga(cadenaEnvio, "E")
            Response.Redirect("HEsperaR.htm")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
