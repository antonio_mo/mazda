﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmCatBidRegion.aspx.vb" Inherits="Cotizador_Mazda_Retail.wfrmCatBidRegion" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WfrmCatBid</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio">Distribuidor :</TD>
					<TD colspan="2">
						<asp:dropdownlist id="cbo_distribuidor" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD width="30%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" width="20%">Región :</TD>
					<TD width="25%">
						<asp:dropdownlist id="cbo_Region" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD width="30%"></TD>
					<TD width="20%">
						<asp:TextBox id="txt_id" runat="server" Visible="False" Width="20px"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Factor :</TD>
					<TD>
						<asp:TextBox onkeypress="javascript:onlyDigits(event);" id="txt_factor" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="2" TabIndex="2"></asp:TextBox></TD>
					<TD>&nbsp;</TD>
					<TD>
						&nbsp;</TD>
				</TR>
				<TR>
					<TD class="obligatorio">Constante Factor :</TD>
					<TD>
						<asp:TextBox onkeypress="javascript:onlyDigits(event);" id="txt_ctefactor" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="2" TabIndex="2"></asp:TextBox></TD>
					<TD>&nbsp;</TD>
					<TD>
						&nbsp;</TD>
				</TR>
				<TR>
					<TD class="obligatorio">Programa :</TD>
					<TD colSpan="2">
							<asp:checkboxlist id="chkLCriterio" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="4"
								RepeatDirection="Horizontal" Height="26px">
							</asp:checkboxlist></TD>
				    <td></td>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD>
						&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD class="obligatorio">&nbsp;</TD>
					<TD>
						&nbsp;</TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center">
						<asp:ImageButton id="cm_Guardar" runat="server" ImageUrl="..\Imagenes\botones\guardar.gif" Width="80px" TabIndex="5"></asp:ImageButton></TD>
					<TD>
						<asp:ImageButton id="cmd_Nuevo" runat="server" ImageUrl="..\Imagenes\botones\nuevo.gif" Width="80px" TabIndex="6"></asp:ImageButton></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD align="center" height="25">
						&nbsp;</TD>
					<TD>
						&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD colSpan="4" class="auto-style1">
						<asp:datagrid id="grdCatalogo" runat="server" CssClass="Datagrid"
									Width="650px" CellPadding="1" AutoGenerateColumns="False" TabIndex="7" Visible="False">
									<SelectedItemStyle Wrap="False" CssClass="NormalItems"></SelectedItemStyle>
									<EditItemStyle Wrap="False"></EditItemStyle>
									<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
									<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
									<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="header" VerticalAlign="Top"></HeaderStyle>
									<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="id_programa" HeaderText="id_programa"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="id_region" HeaderText="id_region"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="id_bid" HeaderText="id_bid"></asp:BoundColumn>
<asp:BoundColumn DataField="Descripcion_Programa" HeaderText="Programa">
    <HeaderStyle Width="15px" />
                                        </asp:BoundColumn>
										<asp:BoundColumn DataField="Descripcion_Aseguradora" HeaderText="Aseguradora">
										    <HeaderStyle Width="15%" />
										    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
										</asp:BoundColumn>
										<asp:BoundColumn DataField="empresa" HeaderText="Distribuidor">
											<HeaderStyle Width="15%" />
											<ItemStyle Wrap="False" Width="15%"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Descripcion_Region" HeaderText="Región">
											<HeaderStyle Width="15%" />
											<ItemStyle Wrap="False" Width="15%" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
                                        <asp:BoundColumn DataField="Subramo" HeaderText="Subramo">
                                            <HeaderStyle Width="10%" />
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                        </asp:BoundColumn>
										<asp:BoundColumn DataField="Estatus" HeaderText="Estatus">
											<HeaderStyle Width="10%" />
											<ItemStyle Wrap="False" Width="10%" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center"></ItemStyle>
										    <ItemStyle Width="10%" Wrap="False" />
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Factor" HeaderText="Factor" DataFormatString="{0:#} ">
											<HeaderStyle Width="10%" />
											<ItemStyle Wrap="False" Width="10%" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="CteFactor" HeaderText="Constante Factor" DataFormatString="{0:#}">
                                            <HeaderStyle Width="10%" />
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                        </asp:BoundColumn>
										<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
											HeaderText="Del." CommandName="Delete"></asp:ButtonColumn>
										<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Sel."
											CommandName="Select" Visible="False">
											<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
										</asp:ButtonColumn>
									</Columns>
									<PagerStyle Wrap="False"></PagerStyle>
								</asp:datagrid>
						</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 104; LEFT: 360px; POSITION: absolute; TOP: 512px" runat="server"></cc1:MsgBox>
			<HR class="lineas" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table5" style="Z-INDEX: 112; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0"> &nbsp;Catálogo 
							Distribuidor-Región</FONT></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

