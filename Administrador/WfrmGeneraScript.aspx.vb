Imports CN_Negocios

Public Class WfrmGeneraScript
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnGeneraLayout

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            Dim i As Integer = 0

            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Session("programa") = 29 Then
                    lbl_programa.Text = "Conauto"
                    lbl_Tipo.Visible = True
                    cbo_TipoLayout.Visible = True
                ElseIf Session("programa") = 30 Then
                    lbl_programa.Text = "Conautopcion"
                    lbl_Tipo.Visible = True
                    cbo_TipoLayout.Visible = True
                ElseIf Session("programa") = 31 Then
                    lbl_programa.Text = "Arrendadora"
                    lbl_Tipo.Visible = True
                    cbo_TipoLayout.Visible = True

                ElseIf Session("programa") = 32 Then
                    lbl_programa.Text = "Mazda"
                    lbl_Tipo.Visible = True
                    cbo_TipoLayout.Visible = True
                End If

                cbo_TipoLayout.Items.Clear()
                For i = 1 To 3
                    If i = 1 Then
                        cbo_TipoLayout.Items.Add("--Seleccionar--")
                    ElseIf i = 2 Then
                        cbo_TipoLayout.Items.Add("P�lizas activas")
                        'ElseIf i = 3 And Session("programa") = 29 Then 'solo Conauto
                    ElseIf i = 3 Then
                        cbo_TipoLayout.Items.Add("P�lizas canceladas")
                    End If
                Next

                If Not Request.QueryString("urlS") Is Nothing Then
                    ''''Dim Cadena As String = "C:\inetpub\wwwroot\CotizadorContado_Conauto_QA\Script\" & Request.QueryString("nombre")
                    'Dim Cadena As String = "D:\Aplicaciones\CotizadorContado_Conauto_QA\Script\" & Request.QueryString("nombre")
                    Dim url As String = ConfigurationSettings.AppSettings("Script") & Request.QueryString("urlS") & ".txt"
                    Dim nombre As String = ""

                    Response.Buffer = False
                    Response.Clear()
                    Response.ClearContent()
                    Response.ClearHeaders()
                    'Response.ContentType = "application/pdf"
                    Response.ContentType = "application/txt"
                    If Request.QueryString("Tipo") = 1 Then
                        nombre = "Archivo de Altas.txt"
                    Else
                        nombre = "Archivo de Cancelaciones.txt"
                    End If
                    Response.AddHeader("Content-Disposition", "attachment; filename=""" & nombre & "")
                    'Response.AddHeader("Content-Disposition", "attachment; filename=""" & Request.QueryString("urlS") & ".txt""")
                    Response.TransmitFile(url)
                    Response.End()
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_generar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim NombreArchivo As String = ""
        Dim i As Integer = 0
        Dim TipoLayout As Integer = 0
        Try
            If Session("programa") = 29 Then    'Conauto
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            ElseIf Session("programa") = 30 Then    'Conautopcion
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            ElseIf Session("programa") = 31 Then    'Arrendadora
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If

            ElseIf Session("programa") = 32 Then    'Mazda
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            End If

            'EVI 15/12/2014
            NombreArchivo = ccliente.GeneraLayout(Session("programa"), TipoLayout, _
                    ConfigurationSettings.AppSettings("Script"), _
                    ConfigurationSettings.AppSettings("ArchivosLog"), _
                    ConfigurationSettings.AppSettings("BanInterface"))
            ''NombreArchivo = ccliente.GeneraLayout(Session("programa"), TipoLayout)

            If NombreArchivo = "" Then
                MsgBox.ShowMessage("No hay informaci�n para enviar por interface")
                Exit Sub
            Else
                crea_tabla(NombreArchivo, TipoLayout)
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Protected Sub cmd_generar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_generar.Click
        Dim NombreArchivo As String = ""
        Dim i As Integer = 0
        Dim TipoLayout As Integer = 0
        Try
            If Session("programa") = 29 Then    'Conauto
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            ElseIf Session("programa") = 30 Then    'Conautopcion
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            ElseIf Session("programa") = 31 Then    'Arrendadora
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If

            ElseIf Session("programa") = 32 Then    'Mazda
                TipoLayout = cbo_TipoLayout.SelectedIndex
                If TipoLayout = 0 Then
                    MsgBox.ShowMessage("Seleccione el tipo")
                    Exit Sub
                End If
            End If

            'EVI 15/12/2014
            NombreArchivo = ccliente.GeneraLayout(Session("programa"), TipoLayout, _
                    ConfigurationSettings.AppSettings("Script"), _
                    ConfigurationSettings.AppSettings("ArchivosLog"), _
                    ConfigurationSettings.AppSettings("BanInterface"))
            ''NombreArchivo = ccliente.GeneraLayout(Session("programa"), TipoLayout)

            If NombreArchivo = "" Then
                MsgBox.ShowMessage("No hay informaci�n para enviar por interface")
                Exit Sub
            Else
                crea_tabla(NombreArchivo, TipoLayout)
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub crea_tabla(ByVal urlDanos As String, ByVal tipo As Integer)
        Dim hp As HyperLink
        Dim hp1 As HyperLink
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tb_imp.Controls.Clear()
        Try
            tbrow = New TableRow
            tbcell = New TableCell
            hp1 = New HyperLink
            hp = New HyperLink
            Select Case tipo
                Case 1
                    hp.Text = "Archivo de Salida  Altas   "
                    hp1.Text = "Archivo de Salida Altas"
                Case 2
                    hp.Text = "Archivo de Salida Cancelaciones   "
                    hp1.Text = "Archivo de Salida Cancelaciones"
            End Select
            hp.NavigateUrl = "WfrmGeneraScript.aspx?urlS=" & urlDanos & "&Tipo=" & tipo & ""
            hp1.NavigateUrl = "WfrmGeneraScript.aspx?urlS=" & urlDanos & "&Tipo=" & tipo & ""
            hp1.ImageUrl = "..\imagenes\general\page_down.gif"
            hp.ForeColor = New System.Drawing.Color().DarkBlue
            tbcell.Controls.Add(hp)
            tbcell.Controls.Add(hp1)
            tbcell.CssClass = "SubTituloPoliza"
            tbrow.Controls.Add(tbcell)
            tb_imp.Controls.Add(tbrow)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
