﻿<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WfrmExportaInterface.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmExportaInterface" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK rel="stylesheet" type="text/css" href="..\Css\Styles.css">
        <script language="javascript" type="text/javascript">
            function PonDiagonal() {
                var stCadena = document.cliente.txt_fnacimiento.value;
                if (IsNumeric(stCadena)) {
                    if (stCadena.length == 2) {
                        document.cliente.txt_fnacimiento.value = document.cliente.txt_fnacimiento.value + "/"
                    }
                    if (stCadena.length == 5) {
                        document.cliente.txt_fnacimiento.value = document.cliente.txt_fnacimiento.value + "/"
                    }
                }
                else {
                    document.cliente.txt_fnacimiento.value = '';
                }
            };
        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR style="Z-INDEX: 101; POSITION: absolute; TOP: 0px; LEFT: 0px" class="lineas" width="100%">
			<TABLE style="Z-INDEX: 110; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px" id="Table4"
				border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD class="negrita_marco_color" height="20" vAlign="middle" width="2%" align="center"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Genera interface
							de Salida SAARS</FONT></TD>
				</TR>
			</TABLE>
			<cc1:msgbox style="Z-INDEX: 103; POSITION: absolute; TOP: 400px; LEFT: 328px" id="MsgBox" runat="server"></cc1:msgbox>
			<TABLE style="Z-INDEX: 105; POSITION: absolute; TOP: 48px; LEFT: 8px" id="Table3" border="0"
				cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD class="obligatorio" width="25%" align="right">Generar Archivo del día:&nbsp;</TD>
					<TD class="obligatorio" width="25%"><asp:textbox id="txt_diaRep" tabIndex="1" onkeyup="PonDiagonal()" runat="server" MaxLength="10"
											CssClass="combos_small" Width="60%" AutoPostBack="True"></asp:textbox>&nbsp;&nbsp;
										<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" AutoPostBack="True" Height="8px" From-Control="txt_fsiniestro"
											BorderColor="Black" BorderWidth="1px" BackColor="Yellow" TextMessage="La fecha es Incorrecta"
											Buttons="[<][m][y]  [>]" Culture="es-MX Español (México)" RequiredDateMessage="La Fecha es Requerida"
											Fade="0.5" Move="True" ShowWeekend="True" Shadow="True" InvalidDateMessage="Día Inválido" BorderStyle="Solid"
											Separator="/" Control="txt_diaRep" To-Today="True" ShowErrorMessage="False"></rjs:popcalendar></TD>
					<TD width="25%" colspan="2" style="width: 50%">
                        <asp:Table ID="tb_imp" runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell CssClass="SubTituloPoliza">
                                    <asp:HyperLink runat="server" ID="hlSalida" Text="Archivo de Salida Altas"></asp:HyperLink>
                                    <asp:ImageButton runat="server" ID="imgSalida" ImageUrl="..\imagenes\general\page_down.gif" ForeColor="DarkBlue" OnClick="imgSalida_Click"></asp:ImageButton>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </TD>
				</TR>				
				<TR>
					<TD class="obligatorio" width="25%" align="right">Tipo de interface:&nbsp;</TD>
					<TD><asp:DropDownList runat="server" ID="ddlPoliza"></asp:DropDownList></TD>
					<TD></TD>
					<TD></TD>
				</TR>
                <TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD><asp:button style="Z-INDEX: 0" id="cmd_generar" runat="server" CssClass="boton" 
                            Text="Procesar"></asp:button></TD>
					<TD></TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
