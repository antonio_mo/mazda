Imports CN_Negocios
Imports Emisor_Conauto

Partial Class WfrmReimprimir
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private cvalidar As New CnPrincipal
    Private ccliente As New CnPrincipal
    Private Ccotizador As New CnCotizador

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                txtCriterio.Text = ""
                cmd_procesar.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        carga_consulta()
    End Sub


    Public Sub carga_consulta()
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try
            StrCriterios = ""
            For i = 0 To chkLCriterio.Items.Count - 1
                If chkLCriterio.Items(i).Selected = True Then
                    Seleccion = True
                    StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                End If
            Next
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If

            dt = Ccotizador.BuscaPoliza(intbid, Session("programa"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                grd_reimprime.DataSource = dt
                grd_reimprime.DataBind()
                cmd_procesar.Visible = True
            Else
                MsgBox.ShowMessage("No hay registros con esos criterios de busqueda")
                cmd_procesar.Visible = False
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_procesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_procesar.Click
        Dim strcadena As String = ""
        Dim item_grd As DataGridItem
        Dim i As Integer = 0
        Dim bandera As Byte = 0
        Try
            For i = 0 To grd_reimprime.Items.Count - 1
                item_grd = grd_reimprime.Items(i)
                Dim opt As RadioButton = item_grd.FindControl("opt_Seleccion")
                If opt.Checked = True Then
                    Dim ckb As CheckBox = item_grd.FindControl("ckb_grd")
                    If ckb.Checked = True Then
                        strcadena = item_grd.Cells(8).Text
                        bandera = 1
                        Exit For
                    End If
                End If
            Next
            If bandera = 0 Then
                MsgBox.ShowMessage("Favor de selccionar la p�liza a Regenerar")
                Exit Sub
            End If

            If strcadena = "&nbsp;" Or strcadena = "" Then
                MsgBox.ShowMessage("La p�liza no se puede regenerar, favor de consultar a sistemas")
                Exit Sub
            End If

            'CreaPDF.carga(strcadena, "P")
            'Dim opp As New System.Diagnostics.Process
            'opp.StartInfo.FileName = ConfigurationManager.AppSettings("RutaExe")
            'opp.StartInfo.Arguments = strcadena
            'opp.Start()
            'System.Threading.Thread.Sleep(3500)
            Dim _bsPoliza As New CC.Negocio.Reporte.Polizas()
            _bsPoliza.Export(strcadena)

            System.Threading.Thread.Sleep(3500)
            MsgBox.ShowMessage("La p�liza se regenero correctamente, favor de verificarla")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_reimprime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_reimprime.SelectedIndexChanged

    End Sub

    Private Sub grd_reimprime_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_reimprime.ItemDataBound
        Try
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    CType(e.Item.FindControl("opt_Seleccion"), RadioButton).Attributes.Add("onclick", "javascript: RadioChecked('" + CType(e.Item.FindControl("opt_Seleccion"), RadioButton).ClientID + "');")
            End Select
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
