Imports CN_Negocios
Partial Class Wfrmcliente1
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private crfc As New CnRFC
    Private cprincipal As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                limpia()
                desabilita()
                'cmd_modificar.Visible = False
                pnl_clientes.Visible = False
                SetFocus(txt_nombre)
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_fnacimiento.Text = ""
        txt_rfc.Text = ""
        txt_lugarN.Text = ""
        txt_ocupacion.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_contacto.Text = ""
        txt_tel.Text = ""
        txt_empleado.Text = ""
        txt_agencia.Text = ""
        txt_cp.Text = ""
        txt_dir.Text = ""
        txt_ciudad.Text = ""
        txt_estado.Text = ""
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            If valor = 2 Then
                habilita()
            Else
                desabilita()
            End If
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub habilita()
        txt_razon.Enabled = True
        txt_legal.Enabled = True
        txt_contacto.Enabled = True
        txt_rfcrazon.Enabled = True
        txt_contacto.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_rfcrazon.Text = ""
        txt_contacto.CssClass = "combos_small"
        txt_razon.CssClass = "combos_small"
        txt_legal.CssClass = "combos_small"
        txt_rfcrazon.CssClass = "combos_small"

        pc_reporte.Enabled = False
        txt_nombre.Enabled = False
        txt_paterno.Enabled = False
        txt_materno.Enabled = False
        txt_rfc.Enabled = False
        txt_lugarN.Enabled = False
        txt_ocupacion.Enabled = False
        txt_fnacimiento.Enabled = False
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_rfc.Text = ""
        txt_fnacimiento.Text = ""
        txt_lugarN.Text = ""
        txt_ocupacion.Text = ""
        txt_nombre.CssClass = "negrita"
        txt_paterno.CssClass = "negrita"
        txt_materno.CssClass = "negrita"
        txt_rfc.CssClass = "negrita"
        txt_fnacimiento.CssClass = "negrita"
        txt_lugarN.CssClass = "negrita"
        txt_ocupacion.CssClass = "negrita"
    End Sub

    Public Sub desabilita()
        txt_razon.Enabled = False
        txt_legal.Enabled = False
        txt_contacto.Enabled = False
        txt_rfcrazon.Enabled = False
        txt_contacto.Text = ""
        txt_razon.Text = ""
        txt_legal.Text = ""
        txt_rfcrazon.Text = ""
        txt_contacto.CssClass = "negrita"
        txt_razon.CssClass = "negrita"
        txt_legal.CssClass = "negrita"
        txt_rfcrazon.CssClass = "negrita"

        pc_reporte.Enabled = True
        txt_nombre.Enabled = True
        txt_paterno.Enabled = True
        txt_materno.Enabled = True
        txt_rfc.Enabled = True
        txt_fnacimiento.Enabled = True
        txt_ocupacion.Enabled = True
        txt_fnacimiento.Enabled = True
        txt_nombre.Text = ""
        txt_paterno.Text = ""
        txt_materno.Text = ""
        txt_rfc.Text = ""
        txt_fnacimiento.Text = ""
        txt_ocupacion.Text = ""
        txt_fnacimiento.Text = ""
        txt_nombre.CssClass = "combos_small"
        txt_paterno.CssClass = "combos_small"
        txt_materno.CssClass = "combos_small"
        txt_rfc.CssClass = "combos_small"
        txt_fnacimiento.CssClass = "combos_small"
        txt_ocupacion.CssClass = "combos_small"
        txt_fnacimiento.CssClass = "combos_small"
    End Sub

    Private Sub rb_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tipo.SelectedIndexChanged
        personalidad(rb_tipo)
    End Sub

    Private Sub grdcatalogo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdcatalogo.SelectedIndexChanged
        txt_clave.Text = 0
        viewstate("NombreCliente") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(0).Text
        viewstate("TipoGrd") = grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(3).Text
        If txt_tipocliente.Text = "1" Then
            carga_cliente()
        Else
            carga_clientep()
        End If
        pnl_clientes.Visible = False
        'cmd_modificar.Visible = True
    End Sub

    Public Sub carga_clientep()
        Dim dt As DataTable = ccliente.DatosClienteP(Session("bid"), viewstate("NombreCliente"), viewstate("TipoGrd"))
        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).IsNull("nombreP") Then
                txt_nombre.Text = dt.Rows(0)("nombreP")
            Else
                txt_nombre.Text = ""
            End If
            If Not dt.Rows(0).IsNull("apaternoP") Then
                txt_paterno.Text = dt.Rows(0)("apaternoP")
            Else
                txt_paterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("amaternop") Then
                txt_materno.Text = dt.Rows(0)("amaternop")
            Else
                txt_materno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("rfcp") Then
                txt_rfc.Text = dt.Rows(0)("rfcp")
            Else
                txt_rfc.Text = ""
            End If
            If Not dt.Rows(0).IsNull("tipopersonap") Then
                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("tipopersonap")))
                If dt.Rows(0)("tipopersonap") = 2 Then
                    habilita()
                    If Not dt.Rows(0).IsNull("razon_socialp") Then
                        txt_razon.Text = dt.Rows(0)("razon_socialp")
                    Else
                        txt_razon.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("contactop") Then
                        txt_contacto.Text = dt.Rows(0)("contactop")
                    Else
                        txt_contacto.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("rep_legal") Then
                        txt_legal.Text = dt.Rows(0)("rep_legal")
                    Else
                        txt_legal.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("rfc") Then
                        txt_rfcrazon.Text = dt.Rows(0)("rfc")
                    Else
                        txt_rfcrazon.Text = ""
                    End If
                    'txt_contacto.Enabled = True
                    'txt_razon.Enabled = True
                    'txt_legal.Enabled = True
                    'txt_rfcrazon.Enabled = True
                    txt_rfc.Text = ""
                    txt_fnacimiento.Text = ""
                Else
                    txt_razon.Text = ""
                    txt_contacto.Text = ""
                    txt_legal.Text = ""
                    txt_rfcrazon.Text = ""
                    txt_razon.Enabled = False
                    txt_legal.Enabled = False
                    txt_contacto.Enabled = False
                    txt_rfcrazon.Enabled = False
                    txt_contacto.CssClass = "negrita"
                    txt_razon.CssClass = "negrita"
                    txt_legal.CssClass = "negrita"
                    txt_rfcrazon.CssClass = "negrita"

                    pc_reporte.Enabled = True
                    txt_nombre.Enabled = True
                    txt_paterno.Enabled = True
                    txt_materno.Enabled = True
                    txt_rfc.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_ocupacion.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_nombre.CssClass = "combos_small"
                    txt_paterno.CssClass = "combos_small"
                    txt_materno.CssClass = "combos_small"
                    txt_rfc.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                    txt_ocupacion.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                End If
            End If
            If Not dt.Rows(0).IsNull("telefonop") Then
                txt_tel.Text = dt.Rows(0)("telefonop")
            Else
                txt_tel.Text = ""
            End If
            'If Not dt.Rows(0).IsNull("estatusp") Then
            '    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(dt.Rows(0)("estatusp")))
            'Else
            '    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue("P"))
            'End If
        End If
    End Sub

    Public Sub carga_cliente()
        Dim dt As DataTable = ccliente.carga_inf_cliente(Session("bid"), viewstate("NombreCliente"), viewstate("TipoGrd"))
        Try
            If dt.Rows(0).IsNull("nombre") Then
                txt_nombre.Text = ""
            Else
                txt_nombre.Text = dt.Rows(0)("nombre")
            End If
            If dt.Rows(0).IsNull("apaterno") Then
                txt_paterno.Text = ""
            Else
                txt_paterno.Text = dt.Rows(0)("apaterno")
            End If
            If dt.Rows(0).IsNull("amaterno") Then
                txt_materno.Text = ""
            Else
                txt_materno.Text = dt.Rows(0)("amaterno")
            End If
            If dt.Rows(0).IsNull("fnacimiento") Then
                txt_fnacimiento.Text = ""
            Else
                txt_fnacimiento.Text = dt.Rows(0)("fnacimiento")
            End If
            If dt.Rows(0).IsNull("rfc") Then
                txt_rfc.Text = ""
            Else
                txt_rfc.Text = dt.Rows(0)("rfc")
            End If
            If dt.Rows(0).IsNull("lugar_nac") Then
                txt_lugarN.Text = ""
            Else
                txt_lugarN.Text = dt.Rows(0)("lugar_nac")
            End If
            If dt.Rows(0).IsNull("ocupacion") Then
                txt_ocupacion.Text = ""
            Else
                txt_ocupacion.Text = dt.Rows(0)("ocupacion")
            End If
            If Not dt.Rows(0).IsNull("per_fiscal") Then
                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("Per_Fiscal")))
                If dt.Rows(0)("Per_Fiscal") = 2 Then
                    habilita()
                    If Not dt.Rows(0).IsNull("razon_social") Then
                        txt_razon.Text = dt.Rows(0)("razon_social")
                    Else
                        txt_razon.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("contacto") Then
                        txt_contacto.Text = dt.Rows(0)("contacto")
                    Else
                        txt_contacto.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("rep_legal") Then
                        txt_legal.Text = dt.Rows(0)("rep_legal")
                    Else
                        txt_legal.Text = ""
                    End If
                    If Not dt.Rows(0).IsNull("rfc") Then
                        txt_rfcrazon.Text = dt.Rows(0)("rfc")
                    Else
                        txt_rfcrazon.Text = ""
                    End If
                    'txt_contacto.Enabled = True
                    'txt_razon.Enabled = True
                    'txt_rfcrazon.Enabled = True
                    txt_fnacimiento.Text = ""
                    txt_rfc.Text = ""
                Else
                    txt_razon.Text = ""
                    txt_contacto.Text = ""
                    txt_rfcrazon.Text = ""
                    txt_legal.Text = ""
                    txt_razon.Enabled = False
                    txt_legal.Enabled = False
                    txt_contacto.Enabled = False
                    txt_rfcrazon.Enabled = False
                    txt_contacto.CssClass = "negrita"
                    txt_razon.CssClass = "negrita"
                    txt_legal.CssClass = "negrita"
                    txt_rfcrazon.CssClass = "negrita"

                    pc_reporte.Enabled = True
                    txt_lugarN.Enabled = True
                    txt_nombre.Enabled = True
                    txt_paterno.Enabled = True
                    txt_materno.Enabled = True
                    txt_rfc.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_ocupacion.Enabled = True
                    txt_fnacimiento.Enabled = True
                    txt_nombre.CssClass = "combos_small"
                    txt_paterno.CssClass = "combos_small"
                    txt_materno.CssClass = "combos_small"
                    txt_lugarN.CssClass = "combos_small"
                    txt_rfc.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                    txt_ocupacion.CssClass = "combos_small"
                    txt_fnacimiento.CssClass = "combos_small"
                End If
                If dt.Rows(0).IsNull("telefono") Then
                    txt_tel.Text = ""
                Else
                    txt_tel.Text = dt.Rows(0)("telefono")
                End If
                'If dt.Rows(0).IsNull("estatus") Then
                '    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue("P"))
                'Else
                '    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(dt.Rows(0)("estatus")))
                'End If
                If dt.Rows(0).IsNull("datos_empleado") Then
                    txt_empleado.Text = ""
                Else
                    txt_empleado.Text = dt.Rows(0)("datos_empleado")
                End If
                If dt.Rows(0).IsNull("agencia_empleado") Then
                    txt_agencia.Text = ""
                Else
                    txt_agencia.Text = dt.Rows(0)("agencia_empleado")
                End If
                If dt.Rows(0).IsNull("cp_cliente") Then
                    txt_cp.Text = ""
                Else
                    txt_cp.Text = dt.Rows(0)("cp_cliente")
                End If
                If dt.Rows(0).IsNull("calle_cliente") Then
                    txt_dir.Text = ""
                Else
                    txt_dir.Text = dt.Rows(0)("calle_cliente")
                End If
                If dt.Rows(0).IsNull("ciudad_cliente") Then
                    txt_ciudad.Text = ""
                Else
                    txt_ciudad.Text = dt.Rows(0)("ciudad_cliente")
                End If
                If dt.Rows(0).IsNull("estado_cliente") Then
                    txt_estado.Text = ""
                Else
                    txt_estado.Text = dt.Rows(0)("estado_cliente")
                End If
                If Not txt_cp.Text = "" Then
                    'Dim dtC As DataTable = ccliente.cargando_cp(txt_cp.Text)
                    'If dtC.Rows.Count > 0 Then
                    '    cbo_colonia.DataSource = dtC
                    '    cbo_colonia.DataValueField = "id_municipio"
                    '    cbo_colonia.DataTextField = "nombre_colonia"
                    '    cbo_colonia.DataBind()
                    'End If
                    'If dt.Rows(0).IsNull("colonia_cliente") Then
                    '    cbo_colonia.SelectedIndex = cbo_colonia.Items.IndexOf(cbo_colonia.Items.FindByValue(0))
                    'Else
                    '    cbo_colonia.SelectedIndex = cbo_colonia.Items.IndexOf(cbo_colonia.Items.FindByText(dt.Rows(0)("colonia_cliente")))
                    'End If
                    txt_col.Text = dt.Rows(0)("colonia_cliente")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub cmd_modificar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_modificar.Click
    '    Dim Dnacimiento As String
    '    Dim asFecha() As String
    '    If valida_informacion() = 0 Then
    '        If rb_tipo.SelectedValue <> 2 Then
    '            Dnacimiento = txt_fnacimiento.Text
    '            'asFecha = Split(txt_fnacimiento.Text.ToString, "/")
    '            'If Session("fecha") = 1 Then
    '            '    Dnacimiento = asFecha(1) & "/" & asFecha(0) & "/" & asFecha(2)
    '            'Else
    '            '    Dnacimiento = asFecha(0) & "/" & asFecha(1) & "/" & asFecha(2)
    '            'End If
    '        End If
    '        txt_clave.Text = ccliente.actualiza_cliente(cprincipal.valida_cadena(txt_nombre.Text), cprincipal.valida_cadena(txt_paterno.Text), cprincipal.valida_cadena(txt_materno.Text), _
    '            Dnacimiento, _
    '            IIf(txt_rfc.Enabled = True, cprincipal.valida_cadena(txt_rfc.Text), cprincipal.valida_cadena(txt_rfcrazon.Text)), _
    '            cprincipal.valida_cadena(txt_lugarN.Text), cprincipal.valida_cadena(txt_ocupacion.Text), _
    '            rb_tipo.SelectedValue, txt_razon.Text, txt_legal.Text, txt_contacto.Text, _
    '            cprincipal.valida_cadena(txt_tel.Text), 1, cprincipal.valida_cadena(txt_empleado.Text), cprincipal.valida_cadena(txt_agencia.Text), _
    '            cprincipal.valida_cadena(txt_cp.Text), cprincipal.valida_cadena(txt_dir.Text), cprincipal.valida_cadena(txt_col.Text), cprincipal.valida_cadena(txt_ciudad.Text), _
    '            cprincipal.valida_cadena(txt_estado.Text), Session("bid"), txt_clave.Text, txt_tipocliente.Text, Session("fecha"), _
    '            cprincipal.valida_cadena(txt_beneficiario.Text), cprincipal.valida_cadena(txt_email.Text))
    '        cmd_modificar.Visible = True
    '        txt_tipocliente.Text = 1
    '    End If
    'End Sub

    Public Function valida_informacion() As Integer
        Dim bandera As Integer = 0
        'If cbo_estatus.SelectedValue = "P" Then
        '    cbo_estatus.SelectedIndex = cbo_estatus.Items.IndexOf(cbo_estatus.Items.FindByValue(0))
        'End If

        If rb_tipo.SelectedValue <> 2 Then
            If txt_nombre.Text = "" Then
                MsgBox.ShowMessage("El Nombre del Cliente no puede quedar en blanco")
                bandera = 1
            End If
            If txt_paterno.Text = "" Then
                MsgBox.ShowMessage("El Apellido Paterno no puede quedar en blanco")
                bandera = 1
            End If
            If txt_materno.Text = "" Then
                MsgBox.ShowMessage("El Apellido Materno no puede quedar en blanco")
                bandera = 1
            End If
            If txt_rfc.Text = "" Then
                MsgBox.ShowMessage("El R.F.C. no puede quedar en blanco")
                bandera = 1
            End If
        Else
            If txt_razon.Text = "" Then
                MsgBox.ShowMessage("La raz�n social no puede quedar en blanco")
                bandera = 1
            End If
            If txt_legal.Text = "" Then
                MsgBox.ShowMessage("El representante Legal no puede quedar en blanco")
                bandera = 1
            End If
            If txt_contacto.Text = "" Then
                MsgBox.ShowMessage("El Contacto no puede quedar en blanco")
                bandera = 1
            End If
            If txt_rfcrazon.Text = "" Then
                MsgBox.ShowMessage("El R.F.C. no puede quedar en blanco")
                bandera = 1
            End If
        End If
        If txt_tel.Text = "" Then
            MsgBox.ShowMessage("El tel�fono no puede quedar en blanco")
            bandera = 1
        Else
            If txt_tel.Text.Length < 8 Then
                MsgBox.ShowMessage("El n�mero del tel�fono debe de contener m�nimo 8 d�gitos")
                bandera = 1
            Else
                If txt_tel.Text.Length > 13 Then
                    MsgBox.ShowMessage("El n�mero del tel�fono debe de contener como m�ximo 13 d�gitos")
                    bandera = 1
                End If
            End If
        End If
        If txt_cp.Text = "" Then
            MsgBox.ShowMessage("El C.P. no puede quedar en blanco")
            bandera = 1
        End If
        If txt_dir.Text = "" Then
            MsgBox.ShowMessage("La Direcci�n no puede quedar en blanco")
            bandera = 1
        End If
        If txt_ciudad.Text = "" Then
            MsgBox.ShowMessage("La Ciudad no puede quedar en blanco")
            bandera = 1
        End If
        If txt_col.Text = "" Then
            MsgBox.ShowMessage("La colonia no puede quedar en blanco")
            bandera = 1
        End If
        If txt_estado.Text = "" Then
            MsgBox.ShowMessage("El Estado no puede quedar en blanco")
            bandera = 1
        End If
        Return bandera
    End Function

    'Private Sub cmd_limpiar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_limpiar.Click
    '    limpia()
    '    pnl_clientes.Visible = False
    '    cmd_modificar.Visible = False
    'End Sub

    Private Sub txt_fnacimiento_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_fnacimiento.TextChanged
        Dim fechas As String
        Dim fecha() As String = txt_fnacimiento.Text.Split("/")
        fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
        txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
    End Sub

    Public Function valida_fecha(ByVal strfecha As String) As String
        If strfecha.Length = 1 Then
            strfecha = "0" & strfecha
        End If
        Return strfecha
    End Function

    Private Sub pc_reporte_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pc_reporte.SelectionChanged
        Dim fechas As String
        Dim fecha() As String = txt_fnacimiento.Text.Split("/")
        fechas = Mid(fecha(2), 3, 2) & valida_fecha(fecha(1)) & valida_fecha(fecha(0))
        txt_rfc.Text = crfc.GeneraRFC(UCase(txt_paterno.Text), UCase(txt_materno.Text), UCase(txt_nombre.Text), fechas)
    End Sub

    'Private Sub txt_cp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cp.TextChanged
    '    Dim dt As DataTable = cprincipal.cp(txt_cp.Text)
    '    txt_estado.Text = cprincipal.pestado
    '    txt_ciudad.Text = cprincipal.pciudad
    '    cbo_colonia.Items.Clear()
    '    If dt.Rows.Count > 0 Then
    '        cbo_colonia.DataSource = dt
    '        cbo_colonia.DataTextField = "nombre_colonia"
    '        cbo_colonia.DataValueField = "id_municipio"
    '        cbo_colonia.DataBind()
    '    End If
    'End Sub

    Private Sub cmd_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cliente.Click
        Try
            txt_tipocliente.Text = 1
            If pnl_clientes.Visible = False Then
                pnl_clientes.Visible = True
                Dim dt As DataTable = ccliente.consulta_cliente(Session("bid"), txt_cliente.Text)
                If dt.Rows.Count > 0 Then
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                    Exit Sub
                End If
            Else
                pnl_clientes.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar.Click
        Dim dt As DataTable
        If txt_tipocliente.Text = "1" Then
            dt = ccliente.consulta_cliente(Session("bid"), cprincipal.valida_cadena(txt_cliente.Text))
        Else
            dt = ccliente.Consulta_cliente_prospecto(Session("bid"), cprincipal.valida_cadena(txt_cliente.Text))
        End If
        Try
            If dt.Rows.Count > 0 Then
                grdcatalogo_fondo.DataSource = dt
                grdcatalogo.DataSource = dt
                grdcatalogo.DataBind()
                grdcatalogo_fondo.DataBind()
                grdcatalogo_fondo.Visible = True
                grdcatalogo.Visible = True
            Else
                grdcatalogo_fondo.Visible = False
                grdcatalogo.Visible = False
                MsgBox.ShowMessage("No existe informaci�n que Mostrar")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

End Class
