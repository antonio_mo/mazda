<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Wfrmcliente.aspx.vb" Inherits="Cotizador_Mazda_Retail.Wfrmcliente1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		function PonDiagonal()
		{
			var stCadena=document.cliente.txt_fnacimiento.value;
			if(stCadena.length == 2)
			{
				document.cliente.txt_fnacimiento.value=document.cliente.txt_fnacimiento.value+"/"
			}
			if(stCadena.length == 5)
			{
				document.cliente.txt_fnacimiento.value=document.cliente.txt_fnacimiento.value+"/"
			}	
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="cliente" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="Interiro_tabla_centro" width="20%">Cliente :</TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
					<TD class="obligatorio" width="20%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Nombre (s) :
					</TD>
					<TD class="obligatorio">Apellido Paterno :</TD>
					<TD class="obligatorio">Apellido Materno :</TD>
					<TD class="obligatorio">Fecha de Nacimiento dd/mm/yyyy</TD>
					<TD class="obligatorio">R.F.C. :</TD>
				</TR>
				<TR>
					<TD>
						<asp:TextBox id="txt_nombre" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="20"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_paterno" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="20"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_materno" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="20"></asp:TextBox></TD>
					<TD>
						<asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_fnacimiento" onkeyup="PonDiagonal()"
							tabIndex="10" runat="server" Width="60%" CssClass="combos_small" MaxLength="10" AutoPostBack="True" ReadOnly="True"></asp:textbox>&nbsp;&nbsp;
						<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" AutoPostBack="True" Height="8px" From-Control="txt_fsiniestro"
							BorderColor="Black" BorderWidth="1px" BackColor="Yellow" TextMessage="La fecha es Incorrecta"
							Buttons="[<][m][y]  [>]" Culture="es-MX Espa�ol (M�xico)" RequiredDateMessage="La Fecha es Requerida"
							Fade="0.5" Move="True" ShowWeekend="True" Shadow="True" InvalidDateMessage="D�a Inv�lido" BorderStyle="Solid"
							Separator="/" Control="txt_fnacimiento" To-Today="True" ShowErrorMessage="False"></rjs:popcalendar>
					</TD>
					<TD>
						<asp:TextBox id="txt_rfc" onkeypress="javascript:CaracterNoValido(event);" runat="server" Width="70%"
							CssClass="combos_small" MaxLength="15"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Lugar Nacimiento :</TD>
					<TD>
						<asp:TextBox id="txt_lugarN" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="25"></asp:TextBox></TD>
					<TD class="obligatorio" align="right">Ocupaci�n :</TD>
					<TD>
						<asp:TextBox id="txt_ocupacion" onkeypress="javascript:onlyAllLetters(event);" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="25"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_tipocliente" runat="server" Width="24px" CssClass="combos_small" Visible="False"></asp:TextBox>
						<asp:TextBox id="txt_clave" runat="server" Width="24px" CssClass="combos_small" Visible="False"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top">Personalidad fiscal :</TD>
					<TD colSpan="2">
						<asp:RadioButtonList id="rb_tipo" runat="server" Width="100%" CssClass="negrita" AutoPostBack="True"
							RepeatDirection="Horizontal" RepeatColumns="3">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:RadioButtonList></TD>
					<TD class="obligatorio" align="right">Tel�fonos :</TD>
					<TD>
						<asp:TextBox id="txt_tel" onkeypress="javascript:onlyDigits(event,'decOK');" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="13"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Razon Social :</TD>
					<TD colSpan="2">
						<asp:TextBox id="txt_razon" onkeypress="javascript:CaracterNoValido(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="30"></asp:TextBox></TD>
					<TD class="obligatorio"></TD>
					<TD>
						<asp:TextBox id="txt_empleado" runat="server" Width="100%" CssClass="combos_small" Visible="False"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Representante Legal :</TD>
					<TD colSpan="2">
						<asp:TextBox id="txt_legal" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="30"></asp:TextBox></TD>
					<TD class="obligatorio"></TD>
					<TD>
						<asp:TextBox id="txt_agencia" runat="server" Width="100%" CssClass="combos_small" DESIGNTIMEDRAGDROP="2843"
							Visible="False"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Contacto :</TD>
					<TD colSpan="2">
						<asp:TextBox id="txt_contacto" onkeypress="javascript:onlyAllLetters(event);" runat="server"
							Width="100%" CssClass="combos_small" MaxLength="30"></asp:TextBox></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">R.F.C. :</TD>
					<TD>
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_rfcrazon" runat="server"
							MaxLength="15" CssClass="combos_small" Width="70%"></asp:TextBox></TD>
					<TD class="negrita" align="right">Email :</TD>
					<TD>
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_email" runat="server" MaxLength="75"
							CssClass="combos_small" Width="100%"></asp:TextBox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Beneficiario Preferente:</TD>
					<TD colSpan="4">
						<asp:TextBox onkeypress="javascript:CaracterNoValido(event);" id="txt_beneficiario" runat="server"
							MaxLength="150" CssClass="combos_small" Width="100%"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Interiro_tabla_centro">Domicilio :</TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">C�digo Postal :</TD>
					<TD class="obligatorio">Calle y N�mero :</TD>
					<TD class="obligatorio">Ciudad :</TD>
					<TD class="obligatorio">Colonia :</TD>
					<TD class="obligatorio">Estado :</TD>
				</TR>
				<TR>
					<TD class="obligatorio">
						<asp:TextBox id="txt_cp" onkeypress="javascript:onlyDigits(event,'decOK');" runat="server" Width="40%"
							CssClass="combos_small" MaxLength="5"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_dir" runat="server" Width="100%" onkeypress="javascript:CaracterNoValido(event);"
							CssClass="combos_small" MaxLength="30"></asp:TextBox></TD>
					<TD class="obligatorio" align="right">
						<asp:TextBox id="txt_ciudad" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="20"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_col" onkeypress="javascript:CaracterNoValido(event);" runat="server" CssClass="combos_small"
							Width="100%" MaxLength="20"></asp:TextBox></TD>
					<TD>
						<asp:TextBox id="txt_estado" onkeypress="javascript:onlyAllLetters(event);" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="20"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="5">
						<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD align="center" width="20%"></TD>
								<TD align="center" width="20%">
									<asp:Button id="cmd_cliente" runat="server" CssClass="boton" Text="Buscar Cliente"></asp:Button></TD>
								<TD align="center" width="20%"></TD>
								<TD align="center"></TD>
								<TD align="center" width="20%"></TD>
							</TR>
							<TR>
								<TD align="center"></TD>
								<TD align="center" width="20%"></TD>
								<TD align="center"></TD>
								<TD align="center"></TD>
								<TD align="center"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="4">
									<asp:panel id="pnl_clientes" runat="server" Width="100%" Height="176px">
										<DIV style="OVERFLOW: hidden; WIDTH: 100%; HEIGHT: 18px">
											<asp:datagrid id="grdcatalogo_fondo" style="POSITION: relative" runat="server" CssClass="Datagrid"
												Width="500px" AutoGenerateColumns="False" CellPadding="1">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="Interiro_tabla_centro" VerticalAlign="Top"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle HorizontalAlign="Center" Width="15%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataFormatString="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<DIV id="divScroll" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 120px" onscroll="DoScroll()">
											<asp:datagrid id="grdcatalogo" runat="server" CssClass="datagrid" Width="500px" AutoGenerateColumns="False"
												CellPadding="1" AllowSorting="True" ShowHeader="False">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="15%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:BoundColumn Visible="False" DataField="tipo"></asp:BoundColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD width="25%"></TD>
												<TD class="obligatorio_negro" width="50%">Nombre Cliente :</TD>
												<TD width="25%"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD>
													<asp:TextBox onkeypress="javascript:onlyAllLetters(event);" id="txt_cliente" runat="server" MaxLength="15"
														CssClass="combos_small" Width="100%"></asp:TextBox></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD align="center">
													<asp:Button id="cmd_buscar" runat="server" CssClass="boton" Text="Consultar"></asp:Button></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<TABLE id="Table5" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="2%" height="20" class="negrita_marco_color" vAlign="middle" align="center">
						<asp:image id="Image2" runat="server" Width="24px" Height="24px" ImageUrl="..\Imagenes\icono.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Consulta de 
							Clientes&nbsp;</FONT></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 102; LEFT: 352px; POSITION: absolute; TOP: 848px" runat="server"></cc1:MsgBox>
			<HR class="lineas" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
		</form>
	</body>
</HTML>
