Imports CN_Negocios

Partial Class WfrmCotizacion
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalidar As New CnPrincipal


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            Session("cotizacion") = 0
            Session("BanCotiza") = 1
            If Not Page.IsPostBack Then
                If Not Request.QueryString("idcotiza") Is Nothing Then
                    Session("cotizacion") = Request.QueryString("idcotiza")
                    If Request.QueryString("tipo") = 1 Then
                        'cotizacion completa
                        Session("TipoCotizacion") = 1
                        RecargaCotizacion(Request.QueryString("idcotiza"))
                        Response.Redirect("..\cotizar\WfrmCotiza.aspx?valcost=" & Session("precioEscrito") & "")
                    Else
                        'ya no se puede modificar nada
                        Session("TipoCotizacion") = 2
                        RecargaCotizacion(Request.QueryString("idcotiza"))
                        Response.Redirect("..\cotizar\WfrmRecotiza.aspx?valcost1=" & Session("precioEscrito") & "")
                    End If
                Else
                    limpia()
                    SetFocus(txtCriterio)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub RecargaCotizacion(ByVal idcotizacion As Long)
        Dim arrbbva(3) As String
        Dim plazoF As Int16 = 0
        Dim i As Integer = 0
        Dim Arr() As String
        Dim strcadena As String = ""
        Dim dt As DataTable = ccliente.recarga_cotizacion(idcotizacion)
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("iva") Then
                    Session("iva") = dt.Rows(0)("iva")
                End If
                If Not dt.Rows(0).IsNull("marca") Then
                    Session("MarcaDesc") = dt.Rows(0)("marca")
                End If
                If Not dt.Rows(0).IsNull("descripcion_homologado") Then
                    Session("vehiculoDesc") = dt.Rows(0)("descripcion_homologado")
                End If
                If Not dt.Rows(0).IsNull("bandera") Then
                    Session("BanCotiza") = dt.Rows(0)("bandera")
                End If
                If Not dt.Rows(0).IsNull("id_paquete") Then
                    Session("CotiPaquete") = dt.Rows(0)("id_paquete")
                End If
                If Not dt.Rows(0).IsNull("catalogo_vehiculo") Then
                    Session("catalogo") = dt.Rows(0)("catalogo_vehiculo")
                End If
                If Not dt.Rows(0).IsNull("nuevo") Then
                    Session("EstatusV") = dt.Rows(0)("nuevo")
                End If
                If Not dt.Rows(0).IsNull("uso") Then
                    Session("uso") = dt.Rows(0)("uso")
                End If
                If Not dt.Rows(0).IsNull("tipo_persona") Then
                    Session("contribuyente") = dt.Rows(0)("tipo_persona")
                End If
                If Not dt.Rows(0).IsNull("precio_unidad") Then
                    Session("precioEscrito") = dt.Rows(0)("precio_unidad")
                End If
                If Not dt.Rows(0).IsNull("id_marca") Then
                    Session("MarcaC") = dt.Rows(0)("id_marca")
                End If
                If Not dt.Rows(0).IsNull("modelo") Then
                    Session("modelo") = dt.Rows(0)("modelo")
                End If
                If Not dt.Rows(0).IsNull("anio") Then
                    Session("anio") = dt.Rows(0)("anio")
                End If
                If Not dt.Rows(0).IsNull("id_aonh") Then
                    Session("vehiculo") = dt.Rows(0)("id_aonh")
                End If
                If Not dt.Rows(0).IsNull("id_uso") Then
                    Session("intuso") = dt.Rows(0)("id_uso")
                End If
                If Not dt.Rows(0).IsNull("plazo") Then
                    Session("plazo") = dt.Rows(0)("plazo")
                End If
                If Not dt.Rows(0).IsNull("tipopoliza") Then
                    Session("tipopol") = dt.Rows(0)("tipopoliza")
                End If
                If Not dt.Rows(0).IsNull("segurodanos") Then
                    Session("seguro") = dt.Rows(0)("segurodanos")
                End If
                If Not dt.Rows(0).IsNull("segurovida") Then
                    Session("ForPagVida") = dt.Rows(0)("segurovida")
                End If
                If Not dt.Rows(0).IsNull("subramo") Then
                    Session("subramo") = dt.Rows(0)("subramo")
                End If
                If Not dt.Rows(0).IsNull("num_contratocobertura") Then
                    Session("idgrdcontrato") = dt.Rows(0)("num_contratocobertura")
                End If
                If Not dt.Rows(0).IsNull("id_cliente") Then
                    Session("cliente") = dt.Rows(0)("id_cliente")
                End If
                If Not dt.Rows(0).IsNull("enganche") Then
                    Session("Enganche") = dt.Rows(0)("enganche")
                End If
                If Not dt.Rows(0).IsNull("cadena_vida") Then
                    Session("txtvida") = dt.Rows(0)("cadena_vida")
                End If
                If Not dt.Rows(0).IsNull("cadena_desempleo") Then
                    Session("txtdesempelo") = dt.Rows(0)("cadena_desempleo")
                End If
                If Not dt.Rows(0).IsNull("nom") Then
                    Session("NomCliente") = dt.Rows(0)("nom")
                End If
                If Not dt.Rows(0).IsNull("Cadena_Cob") Then
                    Session("CadenaCob") = dt.Rows(0)("Cadena_Cob")
                End If
                If Not dt.Rows(0).IsNull("PaqSelCon") Then
                    Session("PaqSelCon") = dt.Rows(0)("PaqSelCon")
                End If
                'bancomer
                If Not dt.Rows(0).IsNull("contrato_bbva") Then
                    arrbbva(0) = dt.Rows(0)("contrato_bbva")
                End If
                If Not dt.Rows(0).IsNull("fecha_contrato") Then
                    Session("FechaInicio") = dt.Rows(0)("fecha_contrato")
                End If
                If Not dt.Rows(0).IsNull("fecha_contrato") Then
                    arrbbva(1) = dt.Rows(0)("fecha_contrato")
                End If
                If Not dt.Rows(0).IsNull("fecha_vida") Then
                    arrbbva(2) = dt.Rows(0)("fecha_vida")
                End If
                If Not dt.Rows(0).IsNull("importe_bbva") Then
                    arrbbva(3) = dt.Rows(0)("importe_bbva")
                End If
                Session("DatosBBVA") = arrbbva
                If Not dt.Rows(0).IsNull("plazovida") Then
                    Session("plazoFinanciamiento") = dt.Rows(0)("plazovida")
                End If
                If Not dt.Rows(0).IsNull("bancampana") Then
                    Session("Promocion") = dt.Rows(0)("bancampana")
                End If
                If Not dt.Rows(0).IsNull("foliosolicitud") Then
                    Session("Folio") = dt.Rows(0)("foliosolicitud")
                End If
                If Not dt.Rows(0).IsNull("calculos") Then
                    Session("Calculos") = dt.Rows(0)("calculos")
                End If
                If Not dt.Rows(0).IsNull("MultArrPoliza") Then
                    Session("MultArrPolisa") = dt.Rows(0)("MultArrPoliza")
                End If
                If Not dt.Rows(0).IsNull("contrato") Then
                    Session("contrato") = dt.Rows(0)("contrato")
                End If
                'arreglos
                strcadena = ""
                If Not dt.Rows(0).IsNull("subsidios") Then
                    strcadena = dt.Rows(0)("subsidios")
                End If
                If Not strcadena = "" Then
                    Arr = strcadena.Split("|")
                    Session("subsidios") = Arr
                End If
                strcadena = ""
                If Not dt.Rows(0).IsNull("arrpoliza") Then
                    strcadena = dt.Rows(0)("arrpoliza")
                End If
                If Not strcadena = "" Then
                    Arr = strcadena.Split("|")
                    Session("ArrPoliza") = Arr
                End If
                strcadena = ""
                If Not dt.Rows(0).IsNull("resultado") Then
                    strcadena = dt.Rows(0)("resultado")
                End If
                If Not strcadena = "" Then
                    Arr = strcadena.Split("|")
                    Session("resultado") = Arr
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Public Sub limpia()
        chkLCriterio.Items(0).Selected = False
        chkLCriterio.Items(1).Selected = False
        chkLCriterio.Items(2).Selected = False
        txtCriterio.Text = ""
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        carga_consulta()
    End Sub

    Public Sub carga_consulta()
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try
            StrCriterios = ""
            For i = 0 To chkLCriterio.Items.Count - 1
                If chkLCriterio.Items(i).Selected = True Then
                    Seleccion = True
                    StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                End If
            Next
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If


            dt = ccliente.BuscaCotizacion(Session("bid"), Session("programa"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                carga_tb_cotizacion(dt)
                tb_cotiza.Visible = True
            Else
                tb_cotiza.Visible = False
                MsgBox.ShowMessage("No hay registros con esos criterios de busqueda")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function carga_tb_cotizacion(ByVal dt As DataTable)
        Dim strfecha As String = ""
        Dim i As Integer = 0
        Dim hp As HyperLink
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tb_cotiza.Controls.Clear()

        encabezado()
        For i = 0 To dt.Rows.Count - 1
            tbrow = New TableRow
            tbcell = New TableCell
            If dt.Rows(i).IsNull("id_cotizacion") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("id_cotizacion")
            End If
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i).IsNull("nom") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("nom")
            End If
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i).IsNull("anio") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("anio")
            End If
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i).IsNull("descripcion_homologado") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("descripcion_homologado")
            End If
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i)("CotizacionActiva") = 1 Then
                If dt.Rows(i)("bandera") = 1 Then
                    hp = New HyperLink
                    hp.Text = "Comprar P�liza"
                    hp.ImageUrl = "..\Imagenes\general\Modif.gif"
                    hp.NavigateUrl = "WfrmCotizacion.aspx?Tipo=1&idcotiza=" & dt.Rows(i)("id_cotizacion") & ""
                    tbcell.Controls.Add(hp)
                Else
                    Dim lbl As New Label
                    lbl.Text = ""
                    tbcell.Controls.Add(lbl)
                End If
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "negrita"
                tbrow.Controls.Add(tbcell)

                tbcell = New TableCell
                hp = New HyperLink
                hp.Text = "Comprar P�liza"
                hp.ImageUrl = "..\Imagenes\general\Modif.gif"
                hp.NavigateUrl = "WfrmCotizacion.aspx?Tipo=2&idcotiza=" & dt.Rows(i)("id_cotizacion") & ""
                tbcell.Controls.Add(hp)
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "negrita"
                tbrow.Controls.Add(tbcell)
            Else
                Dim lbl As New Label
                lbl.Text = "La cotizaci�n ya no esta vigente"
                tbcell.Controls.Add(lbl)
                tbcell.ColumnSpan = 2
                tbcell.VerticalAlign = VerticalAlign.Middle
                tbcell.HorizontalAlign = HorizontalAlign.Center
                tbcell.CssClass = "negrita"
                tbrow.Controls.Add(tbcell)
            End If

            tb_cotiza.Controls.Add(tbrow)
        Next

    End Function

    Public Sub encabezado()
        Dim tbcell As TableCell
        Dim tbrow As TableRow

        tbrow = New TableRow

        tbcell = New TableCell
        tbcell.Text = "No. Cotizaci�n"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Cliente"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(20)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Modelo"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Descripci�n"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(30)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Modificar Cotizaci�n"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Comprar P�liza"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tb_cotiza.Controls.Add(tbrow)

    End Sub
End Class
