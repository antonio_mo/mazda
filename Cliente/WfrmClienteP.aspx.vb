Imports CN_Negocios
Partial Class WfrmClienteP
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cprinicpal As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("idcotiza") Is Nothing Then
                    carga_datos_recotiza(Request.QueryString("idcotiza"))
                End If
                pnl_clientes.Visible = False
                txt_razon.Enabled = False
                txt_razon.CssClass = ""
                txt_clave.Text = 0
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub grdcatalogo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdcatalogo.SelectedIndexChanged
        Dim dt As DataTable = ccliente.Carga_datos_cotizacion(Session("bid"), grdcatalogo.Items(grdcatalogo.SelectedIndex).Cells(0).Text, 0)
        If dt.Rows.Count > 0 Then
            carga_tb_cotizacion(dt)
            carga_clientep(dt.Rows(0)("id_cotizacion"))
            'revision
            'Session("opcsa") = "F"
            'Response.Redirect("..\Cotizar\WfrmCotiza1.aspx?valcost1=" & dt.Rows(0)("precio") & "")
        End If
        pnl_clientes.Visible = False
    End Sub


    Public Sub carga_clientep(ByVal intcotiza As Integer)
        'Dim dt As DataTable = ccliente.DatosClienteP(clientep, Session("bid"), 0)
        Dim dt As DataTable = ccliente.Carga_datos_clienteP(intcotiza)
        If dt.Rows.Count > 0 Then
            viewstate("idclientep") = dt.Rows(0)("id_clientep")

            If Not dt.Rows(0).IsNull("nombreP") Then
                txt_nombre.Text = dt.Rows(0)("nombreP")
            Else
                txt_nombre.Text = ""
            End If
            If Not dt.Rows(0).IsNull("apaternoP") Then
                txt_apaterno.Text = dt.Rows(0)("apaternoP")
            Else
                txt_apaterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("amaternop") Then
                txt_amaterno.Text = dt.Rows(0)("amaternop")
            Else
                txt_amaterno.Text = ""
            End If
            If Not dt.Rows(0).IsNull("tipopersonap") Then
                rb_tipo.SelectedIndex = rb_tipo.Items.IndexOf(rb_tipo.Items.FindByValue(dt.Rows(0)("tipopersonap")))
                If dt.Rows(0)("tipopersonap") = 2 Then
                    habilita()
                    If Not dt.Rows(0).IsNull("razon_socialp") Then
                        txt_razon.Text = dt.Rows(0)("razon_socialp")
                    Else
                        txt_razon.Text = ""
                    End If
                Else
                    txt_razon.Enabled = False
                    txt_razon.CssClass = ""
                    txt_razon.CssClass = "negrita"
                    txt_nombre.Enabled = True
                    txt_apaterno.Enabled = True
                    txt_amaterno.Enabled = True
                    txt_nombre.CssClass = "combos_small"
                    txt_apaterno.CssClass = "combos_small"
                    txt_amaterno.CssClass = "combos_small"
                End If
            End If
            If Not dt.Rows(0).IsNull("telefonop") Then
                txt_tel.Text = dt.Rows(0)("telefonop")
            Else
                txt_tel.Text = ""
            End If
        End If
    End Sub

    Public Function valida_informacion() As Integer
        Dim bandera As Integer = 0
        If txt_nombre.Text = "" Then
            MsgBox.ShowMessage("El Nombre del Cliente no puede quedar en blanco")
            Return bandera = 1
            Exit Function
        End If
        If txt_apaterno.Text = "" Then
            MsgBox.ShowMessage("El Apellido Paterno no puede quedar en blanco")
            Return bandera = 1
            Exit Function
        End If
        If txt_tel.Text = "" Then
            MsgBox.ShowMessage("El Telefono no puede quedar en blanco")
            bandera = 1
        Else
            If txt_tel.Text.Length < 8 Then
                MsgBox.ShowMessage("El n�mero del Telefono debe de contener minimo 8 digitos")
                bandera = 1
            Else
                If txt_tel.Text.Length > 13 Then
                    MsgBox.ShowMessage("El n�mero del Telefono debe de contener como maximo 13 digitos")
                    bandera = 1
                End If
            End If
        End If
        Return bandera
    End Function

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            If valor = 2 Then
                habilita()
            Else
                desabilita()
            End If
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Public Sub habilita()
        txt_razon.Enabled = True
        txt_razon.Text = ""
        txt_razon.CssClass = "combos_small"

        txt_nombre.Enabled = False
        txt_apaterno.Enabled = False
        txt_amaterno.Enabled = False
        txt_nombre.Text = ""
        txt_apaterno.Text = ""
        txt_amaterno.Text = ""
        txt_nombre.CssClass = "negrita"
        txt_apaterno.CssClass = "negrita"
        txt_amaterno.CssClass = "negrita"
    End Sub

    Public Sub desabilita()
        txt_razon.Enabled = False
        txt_razon.CssClass = ""
        txt_razon.CssClass = "negrita"

        txt_nombre.Enabled = True
        txt_apaterno.Enabled = True
        txt_amaterno.Enabled = True
        txt_nombre.Text = ""
        txt_apaterno.Text = ""
        txt_amaterno.Text = ""
        txt_nombre.CssClass = "combos_small"
        txt_apaterno.CssClass = "combos_small"
        txt_amaterno.CssClass = "combos_small"
    End Sub

    'Private Sub cmd_modificar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_modificar.Click
    '    If txt_clave.Text <> 0 Then
    '        If valida_informacion() = 0 Then
    '            ccliente.modifica_clientep(cprinicpal.valida_cadena(txt_apaterno.Text), cprinicpal.valida_cadena(txt_amaterno.Text), _
    '                cprinicpal.valida_cadena(txt_nombre.Text), rb_tipo.SelectedValue, _
    '                 cprinicpal.valida_cadena(txt_razon.Text), cprinicpal.valida_cadena(txt_tel.Text), "", _
    '                 "1", txt_clave.Text, Session("bid"), "", "", "")
    '        End If
    '    Else
    '        MsgBox.ShowMessage("No existe informaci�n que modificar")
    '        Exit Sub
    '    End If
    'End Sub

    Private Sub rb_tipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tipo.SelectedIndexChanged
        personalidad(rb_tipo)
    End Sub

    Public Sub carga_datos_recotiza(ByVal intcotiza As Integer)
        Dim strvida As String = ""
        Dim strDesempleo As String = ""
        Dim arr(8) As String
        Dim valcost As Double
        Dim i As Integer = 0
        Dim dt As DataTable = ccliente.carga_recotizarXCliente(intcotiza, Session("bid"))
        Try
            If dt.Rows.Count > 0 Then
                Session("ClienteCotiza") = 1
                Session("fincon") = "C"
                'If Not dt.Rows(0).IsNull("id_clientep") Then
                '    Session("cliente") = dt.Rows(0)("id_clientep")
                'End If
                If Not dt.Rows(0).IsNull("Clave_Fscar") Then
                    Session("cveFscar") = dt.Rows(0)("Clave_Fscar")
                Else
                    Session("cveFscar") = Nothing
                End If
                If Not dt.Rows(0).IsNull("id_cotiza_poliza") Then
                    Session("CotizaPoliza") = dt.Rows(0)("id_cotiza_poliza")
                End If
                If Not dt.Rows(0).IsNull("id_aonh") Then
                    Session("vehiculo") = dt.Rows(0)("id_aonh")
                End If
                If Not dt.Rows(0).IsNull("marca") Then
                    Session("MarcaDesc") = dt.Rows(0)("marca")
                End If
                If Not dt.Rows(0).IsNull("anio") Then
                    Session("anio") = dt.Rows(0)("anio")
                End If
                If Not dt.Rows(0).IsNull("modelo") Then
                    Session("modelo") = dt.Rows(0)("modelo")
                End If
                If Not dt.Rows(0).IsNull("descripcion") Then
                    Session("vehiculoDesc") = dt.Rows(0)("descripcion")
                End If
                If Not dt.Rows(0).IsNull("catalogo_vehiculo") Then
                    Session("catalogo") = dt.Rows(0)("catalogo_vehiculo")
                End If
                If Not dt.Rows(0).IsNull("precio") Then
                    Session("precioEscrito") = dt.Rows(0)("precio")
                End If
                If Not dt.Rows(0).IsNull("plazo") Then
                    Session("plazo") = dt.Rows(0)("plazo")
                End If
                If Not dt.Rows(0).IsNull("Tipo_Poliza") Then
                    Session("tipopol") = dt.Rows(0)("Tipo_Poliza")
                End If
                If Not dt.Rows(0).IsNull("num_contrato") Then
                    Session("idgrdcontrato") = dt.Rows(0)("num_contrato")
                End If
                If Not dt.Rows(0).IsNull("enganche") Then
                    Session("Enganche") = dt.Rows(0)("enganche")
                End If
                If Not dt.Rows(0).IsNull("cadenavida") Then
                    strvida = dt.Rows(0)("cadenavida")
                End If
                If Not dt.Rows(0).IsNull("cadenaace") Then
                    strDesempleo = dt.Rows(0)("cadenaace")
                End If
                If Not dt.Rows(0).IsNull("Estatus") Then
                    Session("EstatusV") = dt.Rows(0)("Estatus")
                End If
                If Not dt.Rows(0).IsNull("fecha_registro") Then
                    Session("FechaInicio") = Mid(dt.Rows(0)("fecha_registro"), 1, 10)
                End If
                If Not dt.Rows(0).IsNull("Precio_Ctrl") Then
                    arr(0) = 0
                    arr(1) = 0
                    arr(2) = 0
                    arr(3) = 0
                    arr(4) = 0
                    arr(5) = dt.Rows(0)("Precio_Ctrl")
                    Session("resultado") = arr
                End If

                Session("PaqSelCon") = Nothing
                Dim dtP As DataTable = ccliente.carga_grupoPaquete(intcotiza, Session("bid"))
                If dtP.Rows.Count > 0 Then
                    Dim strcadena As String = ""
                    For i = 0 To dtP.Rows.Count - 1
                        If i = 0 Then
                            strcadena = dtP.Rows(i)("id_paquete")
                        Else
                            strcadena = strcadena & "," & dtP.Rows(i)("id_paquete")
                        End If
                    Next
                    Session("PaqSelCon") = strcadena
                End If

                Dim MultArrPolisa As String
                Dim auxdescripcion As String = ""
                Dim dtPn As DataTable = ccliente.carga_PaquetePNeta(Session("CotizaPoliza"))
                If dtPn.Rows.Count > 0 Then
                    If Session("programa") = 4 Or Session("programa") = 5 Or Session("programa") = 9 Then
                        Dim k As Integer
                        For k = 0 To dtPn.Rows.Count - 1
                            If auxdescripcion <> dtPn.Rows(k)("descripcion_paquete") Then
                                If k = 0 Or MultArrPolisa = "" Then
                                    MultArrPolisa = dtPn.Rows(k)("descripcion_paquete") & "*"
                                Else
                                    MultArrPolisa = Mid(MultArrPolisa, 1, MultArrPolisa.Length - 1)
                                    MultArrPolisa = MultArrPolisa & "|" & dtPn.Rows(k)("descripcion_paquete") & "*"
                                End If
                                auxdescripcion = dtPn.Rows(k)("descripcion_paquete")
                            End If
                            MultArrPolisa = MultArrPolisa & dtPn.Rows(k)("total_recibo") & "*" & dtPn.Rows(k)("subsidyf") & "*"
                        Next
                    End If
                    Session("MultArrPolisa") = Mid(MultArrPolisa, 1, MultArrPolisa.Length - 1)
                End If

                Response.Redirect("..\multi\WfrmRecotiza.aspx?cvevida=" & strvida & "&cvedesempleo=" & strDesempleo & "")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_cliente.Click
        Try
            If pnl_clientes.Visible = False Then
                pnl_clientes.Visible = True
                'Dim dt As DataTable = ccliente.Consulta_cliente_prospecto(Session("bid"), txt_cliente.Text)
                Dim dt As DataTable = ccliente.carga_busqueda_cotizacion(Session("bid"), "")
                If dt.Rows.Count > 0 Then
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe cotizaci�n registrada que Mostrar")
                    Exit Sub
                End If
            Else
                pnl_clientes.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_buscar.Click
        Dim dt As DataTable
        If txt_cliente.Text = "" And txt_cotiza.Text = "" Then
            MsgBox.ShowMessage("Los campos de busqueda no pueden quedar en blanco, Verifiquelos")
            Exit Sub
        End If
        Try
            If Not txt_cliente.Text = "" Then
                'dt = ccliente.Consulta_cliente_prospecto(Session("bid"), cprinicpal.valida_cadena(txt_cliente.Text))
                dt = ccliente.carga_busqueda_cotizacion(Session("bid"), cprinicpal.valida_cadena(txt_cliente.Text))
                If dt.Rows.Count > 0 Then
                    grdcatalogo_fondo.DataSource = dt
                    grdcatalogo.DataSource = dt
                    grdcatalogo.DataBind()
                    grdcatalogo_fondo.DataBind()
                    grdcatalogo_fondo.Visible = True
                    grdcatalogo.Visible = True
                Else
                    grdcatalogo_fondo.Visible = False
                    grdcatalogo.Visible = False
                    MsgBox.ShowMessage("No existe cotizaci�n registrada que Mostrar, Verifiquelo")
                    Exit Sub
                End If
            Else
                dt = ccliente.Carga_datos_cotizacion(Session("bid"), "", cprinicpal.valida_cadena(txt_cotiza.Text))
                If dt.Rows.Count > 0 Then
                    carga_tb_cotizacion(dt)
                    carga_clientep(cprinicpal.valida_cadena(txt_cotiza.Text))
                    'revision
                    'Session("opcsa") = "F"
                    'Response.Redirect("..\Cotizar\WfrmCotiza1.aspx?valcost1=" & dt.Rows(0)("precio") & "")
                Else
                    MsgBox.ShowMessage("No existe el n�mero de Cotizaci�n solicitado, Verifiquelo")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function carga_tb_cotizacion(ByVal dt As DataTable)
        Dim strfecha As String = ""
        Dim i As Integer = 0
        Dim hp As HyperLink
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        tb_cotiza.Controls.Clear()

        encabezado()
        For i = 0 To dt.Rows.Count - 1
            tbrow = New TableRow
            tbcell = New TableCell
            If dt.Rows(i).IsNull("id_cotizacion") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("id_cotizacion")
            End If
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i).IsNull("anio") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("anio")
            End If
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            If dt.Rows(i).IsNull("descripcion") Then
                tbcell.Text = ""
            Else
                tbcell.Text = dt.Rows(i)("descripcion")
            End If
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            'tbcell = New TableCell
            'If dt.Rows(i).IsNull("fecha_registro") Then
            '    tbcell.Text = ""
            'Else
            '    'strfecha = Format(dt.Rows(i)("fecha_registro"), "dd/mm/yyyy")
            '    tbcell.Text = Mid(dt.Rows(i)("fecha_registro"), 1, 10)
            'End If
            'tbcell.CssClass = "negrita"
            'tbcell.VerticalAlign = VerticalAlign.Middle
            'tbcell.HorizontalAlign = HorizontalAlign.Center
            'tbrow.Controls.Add(tbcell)

            tbcell = New TableCell
            hp = New HyperLink
            hp.Text = "Comprar P�liza"
            hp.ImageUrl = "..\Imagenes\general\Modif.gif"
            hp.NavigateUrl = "WfrmClienteP.aspx?idcotiza=" & dt.Rows(i)("id_cotizacion") & ""
            tbcell.Controls.Add(hp)
            tbcell.VerticalAlign = VerticalAlign.Middle
            tbcell.HorizontalAlign = HorizontalAlign.Center
            tbcell.CssClass = "negrita"
            tbrow.Controls.Add(tbcell)

            tb_cotiza.Controls.Add(tbrow)
        Next

    End Function

    Public Sub encabezado()
        Dim tbcell As TableCell
        Dim tbrow As TableRow

        tbrow = New TableRow

        tbcell = New TableCell
        tbcell.Text = "No. Cotizaci�n"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Modelo"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(15)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Descripci�n"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(60)
        tbrow.Controls.Add(tbcell)

        'tbcell = New TableCell
        'tbcell.Text = "Fecha Cotizaci�n"
        'tbcell.CssClass = "header"
        'tbcell.Width = Unit.Percentage(30)
        'tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Comprar P�liza"
        tbcell.CssClass = "header"
        tbcell.Width = Unit.Percentage(10)
        tbrow.Controls.Add(tbcell)

        tb_cotiza.Controls.Add(tbrow)

    End Sub
End Class
