<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotizacion.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotizacion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
      <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<title>WfrmCotizacion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="../JavaScript/Jscripts.js"></script>
		<LINK href="../Css/Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="Header_Gris">B�squeda
					</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="35%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" Width="100%" CssClass="combos_small"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="60%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" Width="100%" CssClass="combos_small" RepeatColumns="1"
										Height="56px" RepeatDirection="Horizontal">
										<asp:ListItem Value="1">Vehiculo</asp:ListItem>
										<asp:ListItem Value="2">Nombre Cliente</asp:ListItem>
										<asp:ListItem Value="3">N&#250;m. Cotizaci&#243;n</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3">
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Height="24px" Text="Buscar"></asp:button></TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3">
						<asp:table id="tb_cotiza" runat="server" Width="95%"></asp:table></TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<TABLE id="Table5" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Consulta 
							Cotizaci�n</FONT></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 104; LEFT: 488px; POSITION: absolute; TOP: 608px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
