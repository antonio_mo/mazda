<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmClienteP.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmClienteP" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function DoScroll()
		{
			document.all ("grdcatalogo_fondo").style.pixelLeft=divScroll.scrollLeft * -1;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table5" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0"> &nbsp;Cotizaci�n 
							Prospectos</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="obligatorio" width="20%">Nombre (s) :
					</TD>
					<TD class="obligatorio" width="20%">Apellido Paterno :</TD>
					<TD class="obligatorio" width="20%">Apellido Materno :</TD>
					<TD class="obligatorio" width="40%" rowSpan="1"></TD>
				</TR>
				<TR>
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_nombre" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="25"></asp:textbox></TD>
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_apaterno" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="25"></asp:textbox></TD>
					<TD><asp:textbox onkeypress="convierteMayusculas();" id="txt_amaterno" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="25"></asp:textbox></TD>
					<TD vAlign="top" rowSpan="10"><asp:table id="tb_cotiza" runat="server" Width="100%"></asp:table></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top">Personalidad fiscal :</TD>
					<TD colSpan="2"><asp:radiobuttonlist id="rb_tipo" runat="server" Width="100%" CssClass="negrita" RepeatDirection="Horizontal"
							RepeatColumns="3" AutoPostBack="True">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Actividad Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Razon Social :</TD>
					<TD colSpan="2"><asp:textbox onkeypress="convierteMayusculas();" id="txt_razon" runat="server" Width="100%" CssClass="combos_small"
							MaxLength="30"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Tel�fonos :</TD>
					<TD><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_tel" runat="server" Width="100%"
							CssClass="combos_small" MaxLength="13"></asp:textbox></TD>
					<TD class="obligatorio" align="right"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"><asp:textbox id="txt_clave" runat="server" Width="40px" CssClass="combos_small" Visible="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="center"></TD>
					<TD align="center"><asp:button id="cmd_cliente" runat="server" CssClass="boton" Text="Buscar Cliente"></asp:button></TD>
					<TD class="obligatorio" align="center"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" align="center"></TD>
					<TD align="center"></TD>
					<TD class="obligatorio" align="center"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="3"><asp:panel id="pnl_clientes" runat="server" Width="100%" Height="176px">
										<DIV style="OVERFLOW: hidden; WIDTH: 100%; HEIGHT: 18px">
											<asp:datagrid id="grdcatalogo_fondo" runat="server" CssClass="Datagrid" Width="500px" AutoGenerateColumns="False"
												CellPadding="1">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="encabezados"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" HorizontalAlign="Center" CssClass="Interiro_tabla_centro" VerticalAlign="Top"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="Nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle HorizontalAlign="Center" Width="15%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<DIV id="divScroll" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 120px" onscroll="DoScroll()">
											<asp:datagrid id="grdcatalogo" runat="server" CssClass="datagrid" Width="500px" AutoGenerateColumns="False"
												CellPadding="1" AllowSorting="True" ShowHeader="False">
												<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
												<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
												<EditItemStyle Wrap="False"></EditItemStyle>
												<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
												<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
												<HeaderStyle Wrap="False" CssClass="esconder"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="nombre" HeaderText="Nombre">
														<ItemStyle Wrap="False" Width="45%"></ItemStyle>
													</asp:BoundColumn>
													<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
														HeaderText="Eliminar" CommandName="Delete">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="0%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
													<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\grid01.gif' border='0'&gt;" HeaderText="Seleccionar"
														CommandName="Select">
														<ItemStyle Wrap="False" HorizontalAlign="Center" Width="15%" VerticalAlign="Middle"></ItemStyle>
													</asp:ButtonColumn>
												</Columns>
												<PagerStyle Wrap="False"></PagerStyle>
											</asp:datagrid></DIV>
										<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
											<TR>
												<TD width="25%"></TD>
												<TD class="obligatorio" align="center" width="25%">Nombre Cliente :</TD>
												<TD class="negrita" align="center" width="5%">�</TD>
												<TD class="obligatorio" width="25%">No. Cotizaci�n :</TD>
												<TD width="20%"></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 23px"></TD>
												<TD style="HEIGHT: 23px">
													<asp:TextBox onkeypress="javascript:onlyAllLetters(event);" id="txt_cliente" runat="server" MaxLength="15"
														CssClass="combos_small" Width="100%"></asp:TextBox></TD>
												<TD style="HEIGHT: 23px"></TD>
												<TD style="HEIGHT: 23px">
													<asp:TextBox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_cotiza" runat="server"
														MaxLength="15" CssClass="combos_small" Width="50%"></asp:TextBox></TD>
												<TD style="HEIGHT: 23px"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD align="center" colSpan="3">
													<asp:Button id="cmd_buscar" runat="server" CssClass="boton" Text="Consultar"></asp:Button></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD width="33%"></TD>
								<TD width="33%"></TD>
								<TD width="34%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD></TD>
					<TD class="obligatorio" align="right"></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 103; LEFT: 352px; POSITION: absolute; TOP: 680px" runat="server"></cc1:msgbox></form>
	</body>
</HTML>
