﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;

namespace CC.Negocio.Reporte
{
    public class N_Cotizacion
    {
        private CC.Datos.Reporte.Cotizacion _daCotizacion;

        public N_Cotizacion()
        {
            _daCotizacion = new Datos.Reporte.Cotizacion();
        }

        public void GeneraPromoMazda (int iIdCotizacionUnica)
        {
            Document documento = new Document();
            string filename = string.Empty;
            try
            {
                documento = new Document(iTextSharp.text.PageSize.A4);
                DateTime dateFecha = DateTime.Now, dateFechaCancelar = DateTime.Now.AddDays(30);
                string strArchivoLog = "CotiPromo" + iIdCotizacionUnica + "_" + dateFecha.ToString("ddMMyyyy hhmmss") + ".txt", strFechaCancelar = string.Empty,
                        strPathRoot = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString();
                if (!Directory.Exists(strPathRoot + CC.General.Diccionario.Carpetas.LOGCOTIZACION))
                    Directory.CreateDirectory(strPathRoot + CC.General.Diccionario.Carpetas.LOGCOTIZACION);

                var objCotizacion = _daCotizacion.GetCotizacionUnica(iIdCotizacionUnica);
                if (objCotizacion != null)
                {
                    string strArchivoPDF = "CotiPromo_" + objCotizacion.Id_CotizacionUnica + "_" + dateFecha.ToString("ddmmyyyy hhmmss") + ".pdf";
                    if (!Directory.Exists(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION))
                    { Directory.CreateDirectory(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION); }

                    filename = strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoPDF;
                    PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(filename, FileMode.OpenOrCreate));
                    documento.SetMargins(0, 0, 0, 0);
                    documento.Open();
                    PdfContentByte cb = writer.DirectContent;

                    documento.NewPage();
                    var pagina = documento.Top;

                    iTextSharp.text.Image imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION + CC.General.Diccionario.Cotizacion.LOGO_AON);
                    imagenpdf.SetAbsolutePosition(25, (pagina - 10) - 95);
                    //imagenpdf.ScaleAbsoluteWidth(100);
                    //imagenpdf.ScaleAbsoluteHeight(44);
                    imagenpdf.ScaleAbsoluteWidth(121);
                    imagenpdf.ScaleAbsoluteHeight(87);
                    cb.MoveTo(25, (pagina - 10) - 70);
                    cb.AddImage(imagenpdf);

                    imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION + CC.General.Diccionario.Cotizacion.LOGO_MAZDA);
                    imagenpdf.SetAbsolutePosition(490, (pagina - 25) - 70);
                    imagenpdf.ScaleAbsoluteWidth(77);
                    imagenpdf.ScaleAbsoluteHeight(71);
                    cb.MoveTo(490, (pagina - 25) - 70);
                    cb.AddImage(imagenpdf);


                    if (objCotizacion.Id_Programa == 1)
                    {
                        var coloresPag = ColorTexto("000000");
                        iTextSharp.text.Font letraPag = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        Paragraph parrafoPag = new Paragraph("COTIZACIÓN \n MAZDA \n FINANCIADO", letraPag);
                        ColumnText colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(250, (pagina - 25 - 10), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_CENTER;
                        colPag.Go();
                    }
                    else if (objCotizacion.Id_Programa == 2)
                    {
                        var coloresPag = ColorTexto("000000");
                        iTextSharp.text.Font letraPag = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        Paragraph parrafoPag = new Paragraph("COTIZACIÓN \n MAZDA \n CONTADO", letraPag);
                        ColumnText colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(250, (pagina - 25 - 10), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_CENTER;
                        colPag.Go();
                    }

                    PdfPTable tablapdf = new PdfPTable(2); //columnas);
                    tablapdf.DefaultCell.Border = 1;
                    tablapdf.DefaultCell.BorderColor = new BaseColor(0, 0, 0);
                    PdfPCell celda = new PdfPCell(new Phrase("Vigencia de la Cotización", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.Colspan = 2;
                    celda.BorderColorTop = new BaseColor(0, 0, 0);
                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("De las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);


                    celda = new PdfPCell(new Phrase("Hasta las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;                    
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(dateFechaCancelar.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("Núm Cotización:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(iIdCotizacionUnica.ToString().PadLeft(6, '0'), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    tablapdf.TotalWidth = 200;
                    tablapdf.WriteSelectedRows(0, 4, 365, (pagina - 110), cb);


                    tablapdf = new PdfPTable(2); //columnas);
                    tablapdf.DefaultCell.Border = 1;
                    tablapdf.DefaultCell.BorderColor = new BaseColor(255, 255, 255);
                    celda = new PdfPCell(new Phrase("Automóvil:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Modelo, string.Empty, objCotizacion.Modelo),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("Modelo:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Anio.ToString(), string.Empty,
                            objCotizacion.Anio.ToString()), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);


                    celda = new PdfPCell(new Phrase("Unidad:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Id_Aonh.ToString(), string.Empty, objCotizacion.DescripcionHomo),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("Valor Factura:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueMoney(objCotizacion.Suma_Asegurada, 0, objCotizacion.Suma_Asegurada),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    tablapdf.TotalWidth = 200;
                    tablapdf.WriteSelectedRows(0, 4, 25, (pagina - 190), cb);

                    tablapdf = new PdfPTable(2); //columnas);
                    tablapdf.DefaultCell.Border = 1;
                    celda = new PdfPCell(new Phrase("Cliente:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase("Prueba Cliente", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("Distribuidora:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(objCotizacion.DescripcionBID, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("No. de Serie:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.NoSerie, string.Empty, objCotizacion.NoSerie),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    celda = new PdfPCell(new Phrase("Plazo Total:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Plazo, string.Empty, objCotizacion.Plazo.ToString()) + " meses",
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.NoWrap = false;
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);

                    tablapdf.TotalWidth = 315;
                    tablapdf.WriteSelectedRows(0, 4, 250, (pagina - 190), cb);

                    GeneraAseguradoras(strPathRoot, ref documento, ref cb, objCotizacion.Id_CotizacionPanel, objCotizacion, 270);

                    GeneraPaquetes(ref documento, ref cb, objCotizacion.Id_CotizacionPanel, (decimal)objCotizacion.Suma_Asegurada, strPathRoot, dateFechaCancelar, iIdCotizacionUnica);

                    documento.Close();
                    var bSuccess = _daCotizacion.ActualizaCotizacion(strArchivoPDF, dateFechaCancelar, objCotizacion.Id_CotizacionUnica);
                }
            }

            catch (Exception ex)
            {
                if (System.IO.File.Exists(filename))
                    System.IO.File.Delete(filename);

                throw ex;
            }
            finally
            {                
                documento.Close();                
            }
        }

        private void GeneraAseguradoras(string strPathRoot, ref Document documento, ref PdfContentByte cb, int iId_CotizacionPanel, Modelo.Reporte.Cotizacion.CotizacionUnica objCotizacion, int iPosicion)
        {
            var pagina = documento.Top;
            var lstAseguradoras = _daCotizacion.GetAseguradorasCotiza(iId_CotizacionPanel);
            
            foreach (var objAseguradora in lstAseguradoras.Select(x => new { x.IdAseguradora, x.DescripcionAseguradora, x.UrlImage }).
                        GroupBy(x => new { x.IdAseguradora, x.DescripcionAseguradora, x.UrlImage }).OrderBy(x => x.Key.IdAseguradora).ToList())
            {
                PdfPTable tablapdf = new PdfPTable(lstAseguradoras.Where(x => x.IdAseguradora == objAseguradora.Key.IdAseguradora).Select(x => new { x.IdPaquete, x.DescripcionPaquete, x.IdRecargo })
                    .Distinct().Count() + 1);
                                                
                PdfPCell celda = null;

                celda = new PdfPCell(new Phrase(string.Empty, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                celda.BorderColorBottom = new BaseColor(0, 0, 0);
                tablapdf.AddCell(celda);
                lstAseguradoras.Where(x=>x.IdAseguradora == objAseguradora.Key.IdAseguradora).Select(x => new { x.IdPaquete, x.DescripcionPaquete }).Distinct().OrderBy(x => x.IdPaquete)
                    .ToList().ForEach(x =>
                {
                    celda = new PdfPCell(new Phrase(x.DescripcionPaquete, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;                    
                    celda.Colspan = (objCotizacion.Id_Programa ==1 ? 2 : 1);
                    tablapdf.AddCell(celda);                    
                });

                celda = new PdfPCell(new Phrase(string.Empty, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                celda.BorderColorTop = new BaseColor(0, 0, 0);
                tablapdf.AddCell(celda);

                var lstRecargos = lstAseguradoras.Where(x => x.IdAseguradora == objAseguradora.Key.IdAseguradora)
                            .Select(x => new { x.DescripcionRecargo, x.IdRecargo, x.PrimerRecibo, x.ReciboSubsecuente, x.PrimaTotal, x.IdPaquete, x.orden })
                            .OrderBy(x => x.IdPaquete).ThenBy(x => x.orden).ToList();
                foreach (var objRecargo in lstRecargos)
                {                    
                    celda = new PdfPCell(new Phrase(objRecargo.DescripcionRecargo.Replace("Contado", (objCotizacion.Id_Programa == 2 ? "Contado" : "Financiado") + " pago único")
                        .Replace("Anual", "Financiado pago anual") , FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    tablapdf.AddCell(celda);
                }

                celda = new PdfPCell(new Phrase("Meses de seguro", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tablapdf.AddCell(celda);
                foreach (var objRecargo in lstRecargos)
                {
                    celda = new PdfPCell(new Phrase(objCotizacion.Plazo.ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                }

                celda = new PdfPCell(new Phrase("No. de pagos", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tablapdf.AddCell(celda);
                foreach (var objRecargo in lstRecargos)
                {
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.Plazos.TotalPagos((int)objCotizacion.Plazo, objRecargo.IdRecargo).ToString(),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                }

                if (objCotizacion.Id_Programa == 1)
                {
                    celda = new PdfPCell(new Phrase("Primer Recibo", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);
                    foreach (var objRecargo in lstRecargos)
                    {
                        celda = new PdfPCell(new Phrase((objRecargo.PrimerRecibo == objRecargo.PrimaTotal ? string.Empty : 
                            CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.PrimerRecibo)), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                    }
                                
                    celda = new PdfPCell(new Phrase("Recibo Subsecuente", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    tablapdf.AddCell(celda);
                    foreach (var objRecargo in lstRecargos)
                    {
                        celda = new PdfPCell(new Phrase((objRecargo.ReciboSubsecuente == 0 ? string.Empty : CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.ReciboSubsecuente)),
                            FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                    }
                }

                celda = new PdfPCell(new Phrase("Prima Total", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tablapdf.AddCell(celda);
                foreach (var objRecargo in lstRecargos)
                {
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.PrimaTotal), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                }
                celda = new PdfPCell(new Phrase("Pago Mensual Aprox", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                tablapdf.AddCell(celda);
                foreach (var objRecargo in lstRecargos)
                {
                    celda = new PdfPCell(new Phrase(CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.PrimaTotal / (int)objCotizacion.Plazo),
                        FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                    celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tablapdf.AddCell(celda);
                }
                //Fuera del cuadro
                //tablapdf.TotalWidth = 490;
                //tablapdf.WriteSelectedRows(0, (objCotizacion.Id_Programa == 1 ? 8 : 7), 75, (pagina - iPosicion), cb);

                tablapdf.TotalWidth = 540;
                tablapdf.WriteSelectedRows(0, (objCotizacion.Id_Programa == 1 ? 8 : 7), 25, (pagina - iPosicion), cb);

                iTextSharp.text.Image imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION + @"Logos\Minis\Vacia.png");                
                if (objCotizacion.Id_Programa == 1)
                {
                    imagenpdf.SetAbsolutePosition(25.5f, (pagina - iPosicion - 37.5f));
                    imagenpdf.ScaleAbsoluteWidth(107);
                    imagenpdf.ScaleAbsoluteHeight(37);
                }
                else
                {
                    imagenpdf.SetAbsolutePosition(25.5f, (pagina - iPosicion - 27.5f));
                    imagenpdf.ScaleAbsoluteWidth(179f);
                    imagenpdf.ScaleAbsoluteHeight(27);
                }
                cb.MoveTo(25, (pagina - iPosicion - 45));
                cb.AddImage(imagenpdf);

                imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION +
                    objAseguradora.Key.UrlImage.Replace(@"..\Imagenes\", string.Empty));
                if (objAseguradora.Key.DescripcionAseguradora.ToUpper().Contains("QUALITAS"))
                {

                    if (objCotizacion.Id_Programa == 1)
                    {
                        imagenpdf.SetAbsolutePosition(66, (pagina - iPosicion - 36));
                        imagenpdf.ScaleAbsoluteWidth(24);
                        imagenpdf.ScaleAbsoluteHeight(34);
                    }
                    else
                    {
                        imagenpdf.SetAbsolutePosition(105, (pagina - iPosicion - 27));
                        imagenpdf.ScaleAbsoluteWidth(18.3f);
                        imagenpdf.ScaleAbsoluteHeight(25);
                    }
                }
                else if (objAseguradora.Key.DescripcionAseguradora.ToUpper().Contains("CHUBB"))
                {
                    if (objCotizacion.Id_Programa == 1)
                    {
                        imagenpdf.SetAbsolutePosition(50, (pagina - iPosicion - 34));
                        imagenpdf.ScaleAbsoluteWidth(61);
                        imagenpdf.ScaleAbsoluteHeight(33);
                    }
                    else
                    {
                        imagenpdf.SetAbsolutePosition(90, (pagina - iPosicion - 27));
                        imagenpdf.ScaleAbsoluteWidth(46.2f);
                        imagenpdf.ScaleAbsoluteHeight(25);
                    }
                }
                else
                {
                    if (objCotizacion.Id_Programa == 1)
                    {
                        imagenpdf.SetAbsolutePosition(55, (pagina - iPosicion - 35));                        
                        imagenpdf.ScaleAbsoluteWidth(42.3f);
                        imagenpdf.ScaleAbsoluteHeight(31.77f);
                    }
                    else
                    {
                        imagenpdf.SetAbsolutePosition(95, (pagina - iPosicion - 27));
                        imagenpdf.ScaleAbsoluteWidth(29.97f);
                        imagenpdf.ScaleAbsoluteHeight(22.5f);
                    }
                }

                cb.MoveTo(25, (pagina - 350));
                cb.AddImage(imagenpdf);

                iPosicion = iPosicion + (objCotizacion.Id_Programa == 1 ? 160 : 140);                
            }            
        }

        private void GeneraPaquetes(ref Document documento, ref PdfContentByte cb, int iId_CotizacionPanel, decimal dValorFactura, string strPathRoot, DateTime dateFechaCancelar, int iId_CotizacionUnica)
        {            
            CD_Datos.CdSql _cdConecta = new CD_Datos.CdSql();
            var objCotizacion = _daCotizacion.GetCotizacionUnica(iId_CotizacionUnica);
            System.Data.DataSet ds = _cdConecta.EjecutaSP("SP_COBERTURAS_COTIZA", "@dValorFactura|" + dValorFactura.ToString() + "#@iIdCotizacion|" + iId_CotizacionPanel.ToString());
            if (ds.Tables.Count > 0)
            {                
                System.Data.DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    documento.NewPage();
                    var pagina = documento.Top;
                    

                    iTextSharp.text.Image imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION + CC.General.Diccionario.Cotizacion.LOGO_AON);
                    imagenpdf.SetAbsolutePosition(25, (pagina - 10) - 95);
                    imagenpdf.ScaleAbsoluteWidth(121);
                    imagenpdf.ScaleAbsoluteHeight(87);
                    cb.MoveTo(25, (pagina - 10) - 70);
                    cb.AddImage(imagenpdf);

                    imagenpdf = iTextSharp.text.Image.GetInstance(strPathRoot + CC.General.Diccionario.Carpetas.IMAGENESCOTIZACION + CC.General.Diccionario.Cotizacion.LOGO_MAZDA);
                    imagenpdf.SetAbsolutePosition(490, (pagina - 25) - 70);
                    imagenpdf.ScaleAbsoluteWidth(77);
                    imagenpdf.ScaleAbsoluteHeight(71);
                    cb.MoveTo(490, (pagina - 25) - 70);
                    cb.AddImage(imagenpdf);


                    if (objCotizacion.Id_Programa == 1)
                    {

                        var coloresPag = ColorTexto("000000");
                        iTextSharp.text.Font letraPag = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        Paragraph parrafoPag = new Paragraph("COTIZACIÓN \n MAZDA \n FINANCIADO", letraPag);
                        ColumnText colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(250, (pagina - 25 - 10), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_CENTER;
                        colPag.Go();

                        PdfPTable tablapdf = new PdfPTable(2);
                        tablapdf.DefaultCell.Border = 1;
                        tablapdf.DefaultCell.BorderColor = new BaseColor(0, 0, 0);
                        PdfPCell celda = new PdfPCell(new Phrase("Vigencia de la Cotización", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.Colspan = 2;
                        celda.BorderColorTop = new BaseColor(0, 0, 0);
                        celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("De las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("Hasta las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(dateFechaCancelar.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("Núm Cotización:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(iId_CotizacionUnica.ToString().PadLeft(6, '0'), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        tablapdf.TotalWidth = 200;
                        tablapdf.WriteSelectedRows(0, 4, 365, (pagina - 110), cb);

                        tablapdf = new PdfPTable(dt.Columns.Count);
                        tablapdf.DefaultCell.Border = 1;
                        tablapdf.DefaultCell.BorderColor = new BaseColor(255, 255, 255);
                        float[] widths = new float[dt.Columns.Count];
                        widths[0] = 150;
                        for (int x = 1; x < dt.Columns.Count; x++)
                            widths[x] = 390 / dt.Columns.Count;
                        tablapdf.SetWidths(widths);

                        celda = new PdfPCell(new Phrase("COBERTURAS", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        //celda.BackgroundColor = new iTextSharp.text.BaseColor(192, 192, 192);
                        celda.Colspan = dt.Columns.Count;
                        tablapdf.AddCell(celda);

                        for (int x = 0; x < dt.Rows.Count; x++)
                        {
                            if (x == 0)
                            {
                                celda = new PdfPCell(new Phrase(dt.Rows[x][0].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tablapdf.AddCell(celda);

                                for (int y = 1; y < dt.Columns.Count; y = y + 2)
                                {
                                    celda = new PdfPCell(new Phrase(dt.Rows[x][y].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    celda.Colspan = 2;
                                    tablapdf.AddCell(celda);
                                }
                            }
                            else if (x == 1)
                            {
                                for (int y = 0; y < dt.Columns.Count; y++)
                                {
                                    celda = new PdfPCell(new Phrase(dt.Rows[x][y].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    tablapdf.AddCell(celda);
                                }
                            }
                            else
                            {
                                celda = new PdfPCell(new Phrase(dt.Rows[x][0].ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                tablapdf.AddCell(celda);

                                for (int y = 1; y < dt.Columns.Count; y++)
                                {
                                    decimal dOutConv = 0;
                                    string strDed = string.Empty;

                                    if (dt.Rows[x][y].ToString().Split('_').Length > 1)
                                    {
                                        string[] strArr = dt.Rows[x][y].ToString().Split('_');
                                        dt.Rows[x][y] = strArr[0];
                                        strDed = " \n " + strArr[1];
                                    }

                                    celda = new PdfPCell(new Phrase((decimal.TryParse(dt.Rows[x][y].ToString(), out dOutConv) ? CC.General.Funciones.ValidacionesPol.GetValueMoneyNotDec(dOutConv) :
                                        (dt.Rows[x][y].ToString().ToUpper() == string.Empty ? "EXCLUIDO" : dt.Rows[x][y].ToString().ToUpper())) + strDed, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    celda.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                    tablapdf.AddCell(celda);
                                }
                            }
                        }

                        tablapdf.TotalWidth = 540;
                        tablapdf.WriteSelectedRows(0, dt.Rows.Count + 1, 25, (pagina - 190), cb);

                        letraPag = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        parrafoPag = new Paragraph("*AVF - Año Valor Factura", letraPag);
                        colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(25, (pagina - 25 - 480), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_LEFT;
                        colPag.Go();
                    } //Termina Programa 1
                    else if (objCotizacion.Id_Programa == 2)
                    {

                        var coloresPag = ColorTexto("000000");
                        iTextSharp.text.Font letraPag = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        Paragraph parrafoPag = new Paragraph("COTIZACIÓN \n MAZDA \n CONTADO", letraPag);
                        ColumnText colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(250, (pagina - 25 - 10), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_CENTER;
                        colPag.Go();

                        PdfPTable tablapdf = new PdfPTable(2);
                        tablapdf.DefaultCell.Border = 1;
                        tablapdf.DefaultCell.BorderColor = new BaseColor(0, 0, 0);
                        PdfPCell celda = new PdfPCell(new Phrase("Vigencia de la Cotización", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.Colspan = 2;
                        celda.BorderColorTop = new BaseColor(0, 0, 0);
                        celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("De las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(DateTime.Now.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("Hasta las 12 hrs. de:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(dateFechaCancelar.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        celda = new PdfPCell(new Phrase("Núm Cotización:", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        tablapdf.AddCell(celda);
                        celda = new PdfPCell(new Phrase(iId_CotizacionUnica.ToString().PadLeft(6, '0'), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL)));
                        celda.NoWrap = false;
                        celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        tablapdf.AddCell(celda);

                        tablapdf.TotalWidth = 200;
                        tablapdf.WriteSelectedRows(0, 4, 365, (pagina - 110), cb);

                        tablapdf = new PdfPTable(dt.Columns.Count);
                        tablapdf.DefaultCell.Border = 1;
                        tablapdf.DefaultCell.BorderColor = new BaseColor(255, 255, 255);
                        float[] widths = new float[dt.Columns.Count];
                        widths[0] = 150;
                        for (int x = 1; x < dt.Columns.Count; x++)
                            widths[x] = 390 / dt.Columns.Count;
                        tablapdf.SetWidths(widths);

                        celda = new PdfPCell(new Phrase("COBERTURAS", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                        celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        //celda.BackgroundColor = new iTextSharp.text.BaseColor(192, 192, 192);
                        celda.Colspan = dt.Columns.Count;
                        tablapdf.AddCell(celda);

                        for (int x = 0; x < dt.Rows.Count; x++)
                        {
                            if (x == 0)
                            {
                                celda = new PdfPCell(new Phrase(dt.Rows[x][0].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                tablapdf.AddCell(celda);

                                for (int y = 1; y < dt.Columns.Count; y = y + 2)
                                {
                                    celda = new PdfPCell(new Phrase(dt.Rows[x][y].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    celda.Colspan = 2;
                                    tablapdf.AddCell(celda);
                                }
                            }
                            else if (x == 1)
                            {
                                for (int y = 0; y < dt.Columns.Count; y++)
                                {
                                    celda = new PdfPCell(new Phrase(dt.Rows[x][y].ToString(), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    tablapdf.AddCell(celda);
                                }
                            }
                            else
                            {
                                celda = new PdfPCell(new Phrase(dt.Rows[x][0].ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                                celda.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                tablapdf.AddCell(celda);

                                for (int y = 1; y < dt.Columns.Count; y++)
                                {
                                    decimal dOutConv = 0;
                                    string strDed = string.Empty;

                                    if (dt.Rows[x][y].ToString().Split('_').Length > 1)
                                    {
                                        string[] strArr = dt.Rows[x][y].ToString().Split('_');
                                        dt.Rows[x][y] = strArr[0];
                                        strDed = " \n " + strArr[1];
                                    }

                                    celda = new PdfPCell(new Phrase((decimal.TryParse(dt.Rows[x][y].ToString(), out dOutConv) ? CC.General.Funciones.ValidacionesPol.GetValueMoneyNotDec(dOutConv) :
                                        (dt.Rows[x][y].ToString().ToUpper() == string.Empty ? "EXCLUIDO" : dt.Rows[x][y].ToString().ToUpper())) + strDed, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                                    celda.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    celda.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                                    tablapdf.AddCell(celda);
                                }
                            }
                        }

                        tablapdf.TotalWidth = 540;
                        tablapdf.WriteSelectedRows(0, dt.Rows.Count + 1, 25, (pagina - 190), cb);

                        letraPag = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
                        letraPag.SetColor(coloresPag.red, coloresPag.green, coloresPag.blue);
                        parrafoPag = new Paragraph("*AVF - Año Valor Factura", letraPag);
                        colPag = new ColumnText(cb);
                        colPag.SetSimpleColumn(25, (pagina - 25 - 480), 400, 20);
                        colPag.AddText(parrafoPag);
                        colPag.Alignment = PdfPCell.ALIGN_LEFT;
                        colPag.Go();
                    } //Termina Programa 1
                }                
            }
        }

        private colorBas ColorTexto(string hexColor)
        {
            colorBas colores = new colorBas();

            //Quitar el simbolo de #
            if (hexColor.IndexOf('#') != -1)
                hexColor = hexColor.Replace("#", "");

            if (hexColor.Length == 6)
            {
                //#RRGGBB
                colores.red = int.Parse(hexColor.Substring(0, 2), NumberStyles.AllowHexSpecifier);
                colores.green = int.Parse(hexColor.Substring(2, 2), NumberStyles.AllowHexSpecifier);
                colores.blue = int.Parse(hexColor.Substring(4, 2), NumberStyles.AllowHexSpecifier);
            }

            return colores;
        }

        private class colorBas
        {
            public int red { get; set; }
            public int blue { get; set; }
            public int green { get; set; }
        }  
    }
}