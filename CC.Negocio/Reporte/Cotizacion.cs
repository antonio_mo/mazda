﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace CC.Negocio.Reporte
{
    public class Cotizacion_old
    {
        private CC.Datos.Reporte.Cotizacion _daCotizacion;

        public Cotizacion_old()
        {
            _daCotizacion = new Datos.Reporte.Cotizacion();
        }

        public void GeneraPromoMazda (int iIdCotizacionUnica)
        {
            DateTime dateFecha = DateTime.Now, dateFechaCancelar = DateTime.MinValue;
            string strArchivoLog = "CotiPromo" + iIdCotizacionUnica + "_" + dateFecha.ToString("ddMMyyyy hhmmss") + ".txt", strFechaCancelar = string.Empty,
                    strPathRoot = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString();

            try
            {
                if(!Directory.Exists(strPathRoot + CC.General.Diccionario.Carpetas.LOGCOTIZACION))
                    Directory.CreateDirectory(strPathRoot + CC.General.Diccionario.Carpetas.LOGCOTIZACION);
                StreamWriter _fileLog = new StreamWriter(strPathRoot + CC.General.Diccionario.Carpetas.LOGCOTIZACION + strArchivoLog);
                _fileLog.WriteLine("Inicio: " + dateFecha.ToString("dd/MM/yyyy hh:mm:ss:mm"));
                _fileLog.WriteLine("Datos : " + iIdCotizacionUnica.ToString());

                //HM 06/02/2014 se modifica para que tome los valores correctos Promocion / Multianual
                //Si cuenta con promocion = "Exclusion" = 0
                //No cuenta con promocion = "Exclusion" = 1                
                var objCotizacion = _daCotizacion.GetCotizacionUnica(iIdCotizacionUnica);
                if (objCotizacion != null)
                {
                    if(!Directory.Exists(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION))
                    Directory.CreateDirectory(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION);

                    string strArchivoHTML = strArchivoLog.Replace(".txt", ".html");
                    StreamWriter _fileHtml = new StreamWriter(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoHTML);

                    //Verifica que se encuentre el archivo
                    if(!File.Exists(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoHTML))
                        _fileLog.WriteLine("No existe el archivo : " + strArchivoHTML + " en : " + strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION);

                    _fileLog.WriteLine ("Número de registros: 1");
                    string strCuerpo = "<html>" + System.Environment.NewLine +
                        "<link href='../Css/Styles.css' rel='stylesheet' type='text/css'>" + System.Environment.NewLine +
                        "</head>" + System.Environment.NewLine +
                        "<body>" + System.Environment.NewLine +
                        "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +                        
                        "<tr>" + System.Environment.NewLine +
                        "<td width='2%' style='border-top: 1px solid #000000;border-left: 1px solid #000000;border-bottom: 1px solid #000000'>&nbsp;</td>" + System.Environment.NewLine +
                        "<td width='96%' style='border-top: 1px solid #000000;border-bottom: 1px solid #000000'>" + System.Environment.NewLine +                        
                        "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='25%'>" + System.Environment.NewLine +
                        "<img src='../Imagenes/aon.jpg'></img>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td width='25%' class='LetraTitulo' align='center'>" + System.Environment.NewLine +
                        "C O T I Z A C I O N<br>M A Z D A" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td width='55%' align='right'>"+ System.Environment.NewLine +
                        "<img src='../Imagenes/LogoMazda.jpg'></img>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</table>" + System.Environment.NewLine +
                        "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='35%'>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td width='30%' class='LetraTitulo' align='center'>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td width='35%'>"+ System.Environment.NewLine +
                        "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='100%' colspan='2' class='LetraEncabezadoNegra'><center>Vigencia de la Cotizaci&oacute;n&nbsp;</center></td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='60%' align='right' class='LetraEncabezado'>De las 12 hrs. de&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td width='40%' class='LetraEncabezado'>" + System.Environment.NewLine +
                        "&nbsp;" + DateTime.Now.ToString("dd/MM/yyyy") + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Hasta las 12 hrs. de&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td class='LetraEncabezado'>" + System.Environment.NewLine;

                    dateFechaCancelar = dateFecha.AddDays(30);
                    strFechaCancelar = dateFechaCancelar.ToString("dd/MM/yyyy");
                    strCuerpo += "&nbsp;" + strFechaCancelar + System.Environment.NewLine;

                    strCuerpo += "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezadoNegra'>N&uacute;m Cotizaci&oacute;n&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td class='LetraEncabezadoNegra'>" + System.Environment.NewLine +
                        "&nbsp;" +iIdCotizacionUnica.ToString().PadLeft(6, '0') + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        "</table>" + System.Environment.NewLine +
                        "</table>" + System.Environment.NewLine +
                        "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='20%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='20%'></td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Automovil&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Modelo, string.Empty, objCotizacion.Modelo) +
                            System.Environment.NewLine +
                         "</td>" + System.Environment.NewLine +
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        
                        "<td align='right' class='LetraEncabezado'>Promocion un a&ntilde;o&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        string.Format("<td class='Interior_tabla_centro_inicio'>{0}</td>", /*(objCotizacion.Promocion == 1 ? "SI" : */"NO"/*)*/) + System.Environment.NewLine +
                        
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Modelo:&nbsp;</td>" + System.Environment.NewLine +
                        "<td align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Anio.ToString(), string.Empty, 
                            objCotizacion.Anio.ToString()) + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td>&nbsp;</td>" + System.Environment.NewLine +

                        "<td align='right' class='LetraEncabezado'>Cliente&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td colspan='2' align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.DatosCliente, string.Empty, objCotizacion.DatosCliente) + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Unidad:&nbsp;</td>" + System.Environment.NewLine +
                        "<td colspan='2' align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Id_Aonh.ToString(), string.Empty, objCotizacion.DescripcionHomo) + 
                            System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +

                        "<td align='right' class='LetraEncabezado'>Gpo.-Int.&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td colspan='2' align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.GrupoIntegrante, string.Empty, objCotizacion.GrupoIntegrante) + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Valor factura:&nbsp;</td>" + System.Environment.NewLine +
                        "<td align='left' class='LetraEncabezado'>" + System.Environment.NewLine +

                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueMoney(objCotizacion.Suma_Asegurada, 0, objCotizacion.Suma_Asegurada) + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        
                        "<td align='right' class='LetraEncabezado'>Distribuidora&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td colspan='2' align='left' class='LetraEncabezado'>" + System.Environment.NewLine +

                        "&nbsp;" + objCotizacion.DescripcionBID + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        
                        //Plazo Total
                        "<tr>" + System.Environment.NewLine +
                        "<td align='right' class='LetraEncabezado'>Plazo total:&nbsp;</td>" + System.Environment.NewLine +
                        "<td align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Plazo, string.Empty, objCotizacion.Plazo.ToString()) + "&nbsp;meses" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        
                        "<td align='right' class='LetraEncabezado'>No. De serie&nbsp;:&nbsp;</td>" + System.Environment.NewLine +
                        "<td colspan='2' align='left' class='LetraEncabezado'>" + System.Environment.NewLine +
                        
                        "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.NoSerie, string.Empty, objCotizacion.NoSerie) + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        
                        "<td>&nbsp;</td>" + System.Environment.NewLine +
                        "</td>" + System.Environment.NewLine +
                        "</tr>" + System.Environment.NewLine +
                        
                        "</table>" + System.Environment.NewLine +

                        "<br/>" + System.Environment.NewLine +
                        "<br/>" + System.Environment.NewLine;
                    
                    var lstAseguradoras = _daCotizacion.GetAseguradorasCotiza(objCotizacion.Id_CotizacionPanel);
                    strCuerpo += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td width='10%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +
                        "<td width='15%'></td>" + System.Environment.NewLine +                        
                        "</tr>" + System.Environment.NewLine +
                        "<tr>" + System.Environment.NewLine +
                        "<td></td>" + System.Environment.NewLine +
                        "<td></td>" + System.Environment.NewLine +
                        "<td></td>" + System.Environment.NewLine;
                    
                    lstAseguradoras.Select(x=>new {x.IdPaquete, x.DescripcionPaquete}).Distinct().OrderBy(x=>x.IdPaquete).ToList().ForEach(x=>{ 
                        strCuerpo += "<td colspan='2'>" + x.DescripcionPaquete + "</td>" + System.Environment.NewLine; });
                    
                    strCuerpo += "</tr>" + System.Environment.NewLine;


                    foreach (var objAseguradora in lstAseguradoras.Select(x => new { x.IdAseguradora, x.DescripcionAseguradora, x.UrlImage }).
                        GroupBy(x => new { x.IdAseguradora, x.DescripcionAseguradora, x.UrlImage }).OrderBy(x => x.Key.IdAseguradora).ToList())
                    {
                        strCuerpo += "<tr>" + System.Environment.NewLine +
                            "<tr>" + System.Environment.NewLine +
                            "<td rowspan='4' align='center' valign='top'>" + System.Environment.NewLine +
                            "<img valign='top' src='" + objAseguradora.Key.UrlImage + "'></img>" + System.Environment.NewLine +
                            "</td>" + System.Environment.NewLine;
                        
                        string strCuerpoDescAseg = string.Empty, strCuerpoMesesSeguro = string.Empty, strCuerpoNoPagos = string.Empty, strCuerpoPrimaParcial = string.Empty,
                            strCuerpoPrimaTotal = string.Empty;
                        strCuerpoDescAseg = "<td align='center' class='Interior_tabla_centro_inicio'>" + objAseguradora.Key.DescripcionAseguradora + "</td>" + System.Environment.NewLine;
                        strCuerpoMesesSeguro = "<tr>" + System.Environment.NewLine +
                            "<td class='Interior_tabla_Izquierda'>&nbsp;&nbsp;Meses de seguro</td>" + System.Environment.NewLine;
                        strCuerpoNoPagos = "<tr>" + System.Environment.NewLine +
                            "<td class='Interior_tabla_Izquierda'>&nbsp;&nbsp;No. De pagos</td>" + System.Environment.NewLine;
                        strCuerpoPrimaParcial = "<tr>" + System.Environment.NewLine +
                            "<td class='Interior_tabla_Izquierda'>&nbsp;&nbsp;Prima parcial</td>" + System.Environment.NewLine;
                        strCuerpoPrimaTotal = "<tr>" + System.Environment.NewLine +
                            "<td></td>" + System.Environment.NewLine +
                            "<td class='Interior_tabla_Izquierda'>&nbsp;&nbsp;Prima total</td>" + System.Environment.NewLine;

                        foreach (var objRecargo in lstAseguradoras.Where(x => x.IdAseguradora == objAseguradora.Key.IdAseguradora)
                            .Select(x => new { x.DescripcionRecargo, x.IdRecargo, x.PrimerRecibo, x.PrimaTotal, x.IdPaquete, x.orden })
                            .OrderBy(x => x.IdPaquete).ThenBy(x => x.orden).ToList())
                        {
                            strCuerpoDescAseg += "<td align='center' class='Interior_tabla_centro_inicio'>" + objRecargo.DescripcionRecargo + "</td>" + System.Environment.NewLine;
                            strCuerpoMesesSeguro += "<td align='right' class='LetraBorde'>" + objCotizacion.Plazo.ToString() + "&nbsp;&nbsp;</td>" +
                                System.Environment.NewLine;
                            strCuerpoNoPagos += "<td align='right' class='LetraBorde'>" + CC.General.Funciones.Plazos.TotalPagos((int)objCotizacion.Plazo, objRecargo.IdRecargo) + "&nbsp;&nbsp;</td>" +
                                System.Environment.NewLine;
                            strCuerpoPrimaParcial += "<td align='right' class='LetraBorde'>" + CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.PrimerRecibo) + "&nbsp;&nbsp;</td>" +
                                System.Environment.NewLine;
                            strCuerpoPrimaTotal += "<td align='right' class='LetraBorde'>" + CC.General.Funciones.ValidacionesPol.GetValueMoney(objRecargo.PrimaTotal) + "&nbsp;&nbsp;</td>" +
                                System.Environment.NewLine;
                        }

                        strCuerpoDescAseg += "</tr>" + System.Environment.NewLine +
                            "</tr>" + System.Environment.NewLine;
                        strCuerpoMesesSeguro += "</tr>" + System.Environment.NewLine;
                        strCuerpoNoPagos += "</tr>" + System.Environment.NewLine;
                        strCuerpoPrimaParcial += "</tr>" + System.Environment.NewLine;
                        strCuerpoPrimaTotal += "</tr>" + System.Environment.NewLine;

                        strCuerpo += strCuerpoDescAseg + strCuerpoMesesSeguro + strCuerpoNoPagos + strCuerpoPrimaParcial + strCuerpoPrimaTotal +
                            
                            "<tr>" + System.Environment.NewLine +
                            "<td>&nbsp;</td>" + System.Environment.NewLine +
                            "<td>&nbsp;</td>" + System.Environment.NewLine +
                            "<td>&nbsp;</td>" + System.Environment.NewLine +
                            "<td>&nbsp;</td>" + System.Environment.NewLine +
                            "<td>&nbsp;</td>" + System.Environment.NewLine +                            
                            "</tr>" + System.Environment.NewLine;
                    }

                    strCuerpo += "</table>" + System.Environment.NewLine +

                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>La fecha de cotizaci&oacute;n es " + dateFecha.ToString("dd/MM/yyyy") + "</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>Cotizaci&oacute;n informativa sujeta a cambios sin previo aviso</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "<tr><td height='20px'>&nbsp;</td></tr>" + System.Environment.NewLine +
                    "</table>" + System.Environment.NewLine +

                    //HM 04/06/2014 agregando leyenda
                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraEncabezadoNegra'>Observaciones</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +

                    //HM 04/06/2014 agregando leyenda
                    "<td class='TablaMarcoObservaciones'>" + System.Environment.NewLine +
                        //HM 06/02/2014 agregando las observaciones
                    "<p><p><p><p><p>" + System.Environment.NewLine +
                        //HM 04/06/2014 agregando leyenda
                    "&nbsp;<br>" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.Observaciones, string.Empty, objCotizacion.Observaciones) +
                                System.Environment.NewLine +
                    "</p></p></p></p></p>" + System.Environment.NewLine +
                    "</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "</table>" + System.Environment.NewLine +

                    //HM leyenda observaciones legales
                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraEncabezadoNegra'>Notas importantes:</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>" + System.Environment.NewLine +
                    "La suma asegurada aplica sobre el valor factura los primeros " + (objCotizacion.Plazo <= 12 ? 12 : 24).ToString() + " meses y valor comercial en los subsecuentes; del mismo modo aplican los deducibles." + System.Environment.NewLine +
                    "<br>El deducible en pérdidas parciales aplica el 5% para las tres aseguradoras." + System.Environment.NewLine +
                    "<br>La suma asegurada de Responsabilidad Civil de $4,000,000.00 aplica para Axa y HDI." + System.Environment.NewLine +
                    "<br>La suma asegurada de Responsabilidad Civil de $1,000,000.00 y Responsabilidad Civil Complementaria $3,000,000.00 aplica solo para Qualitas." + System.Environment.NewLine +
                    "<br>La cobertura de Responsabilidad Civil en el Extranjero aplica para Axa, HDI y Qualitas." + System.Environment.NewLine +
                    "</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "</table>" + System.Environment.NewLine +

                    "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" + System.Environment.NewLine +
                    "<tr>" + System.Environment.NewLine +
                    "<td width='25%'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td width='25%'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td width='20%'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td width='30%'>&nbsp;</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal' align='right'>Acepto la aseguradora&nbsp;:</td>" + System.Environment.NewLine +
                    "<td class='LetraGrandeBordeAbajo'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal' align='center'>Firma&nbsp;:</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal' align='right'>Con forma de pago&nbsp;:</td>" + System.Environment.NewLine +
                    "<td class='LetraGrandeBordeAbajo'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraGrandeBordeAbajo'>&nbsp;</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal' align='center'>" + System.Environment.NewLine +

                    "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.DatosCliente, string.Empty, objCotizacion.DatosCliente) + System.Environment.NewLine +
                    "</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +

                    "<tr>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal'>&nbsp;</td>" + System.Environment.NewLine +
                    "<td class='LetraNormal' align='center'>" + System.Environment.NewLine +
                    "&nbsp;" + CC.General.Funciones.ValidacionesPol.GetValueString(objCotizacion.GrupoIntegrante, string.Empty, objCotizacion.GrupoIntegrante) +
                                System.Environment.NewLine +
                    "</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "</table>" + System.Environment.NewLine +

                    "</td>" + System.Environment.NewLine +
                    "<td width='2%' style='border-top: 1px solid #000000;border-right: 1px solid #000000;border-bottom: 1px solid #000000'>&nbsp;</td>" + System.Environment.NewLine +
                    "</tr>" + System.Environment.NewLine +
                    "</table>" + System.Environment.NewLine +

                    "</body>" + System.Environment.NewLine +
                    "</html>" + System.Environment.NewLine;

                    _fileLog.Write("Fin: " + dateFecha.ToString("dd/mm/yyyy hh:mm:ss"));
                    _fileLog.Flush();
                    _fileLog.Close();

                    strCuerpo = strCuerpo.Replace("á", "&aacute;").Replace("é", "&eacute;").Replace("í", "&iacute;").Replace("ó", "&oacute;").Replace("ú", "&uacute;").Replace("ñ", "&ntilde;")
                        .Replace("Á", "&Aacute;").Replace("É", "&Eacute;").Replace("I", "&Íacute;").Replace("Ó", "&Oacute;").Replace("Ú", "&Uacute;").Replace("Ñ    ", "&Ntilde;");

                    _fileHtml.Write(strCuerpo);
                    _fileHtml.Flush();
                    _fileHtml.Close();

                    string strArchivoPDF = "CotiPromo_" + objCotizacion.Id_CotizacionUnica + "_" + dateFecha.ToString("ddmmyyyy hhmmss") + ".pdf";
                    
                    //HM 06/02/2014 Horizontal                    
                    //Shell (RutaExe & " -html2pdf2 -margin 7x7x7x7 -width 750 -height 700 " & Chr(34) & RutaArchivo & ArchivoHTML & Chr(34) & " " & Chr(34) & RutaPDF & ArchivoPDF & Chr(34))
                    //HM 06/02/2014 Vertical                    
                    string strArguments = " -html2pdf2 -margin 7x7x7x7 \"" +
                        strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoHTML + "\" \"" + strPathRoot +
                        CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoPDF + "\"";
                    ProcessStartInfo psi = new ProcessStartInfo(System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_HTML_TOOLS].ToString());
                    psi.Arguments = strArguments;
                    Process proc = Process.Start(psi);
                    proc.WaitForExit();
                    proc.Close();

                    var bSuccess = _daCotizacion.ActualizaCotizacion(strArchivoPDF, dateFechaCancelar, objCotizacion.Id_CotizacionUnica);

                    if (File.Exists(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoHTML))
                        File.Delete(strPathRoot + CC.General.Diccionario.Carpetas.GENERADOCOTIZACION + strArchivoHTML);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
