﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Negocio.Reporte
{
    public class Coberturas
    {
        public bool ValidaQualitasRiesgo(string strCodigoPostal, string strModelo)
        {
            return new Datos.Reporte.Coberturas().ValidaQualitasRiesgo(strCodigoPostal, strModelo);
        }
    }
}
