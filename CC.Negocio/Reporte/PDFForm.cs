﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CC.Negocio.Reporte
{
    public class PDFForm
    {
        [DllImport(CC.General.Diccionario.PDFForm.LIBRARY)]
        private static extern void PDFForm_SetLicenseKey(string lpLicenseKey);
        [DllImport(CC.General.Diccionario.PDFForm.LIBRARY)]
        private static extern int PDFForm_Alloc();
        [DllImport(CC.General.Diccionario.PDFForm.LIBRARY)]
        private static extern int PDFForm_Free(long id);
        [DllImport(CC.General.Diccionario.PDFForm.LIBRARY)]
        private static extern int PDFForm_SetTextField(int id, string lpFieldName, string lpFieldValue);
        [DllImport(CC.General.Diccionario.PDFForm.LIBRARY)]
        private static extern int PDFForm_Apply(int id, string lpInFile, string lpOutFile, int bFlatten);
        
        private int iResult = 0, iIdProcess = 0;

        public PDFForm()
        {
            PDFForm_SetLicenseKey(CC.General.Diccionario.PDFForm.SERIAL);
            iIdProcess = PDFForm_Alloc();
        }

        public void SetValue(CC.General.Diccionario.PDFForm.TipoDato enTipoDato, string strNameField, object objValue)
        {
            string strValue = string.Empty;
            switch (enTipoDato)
            {
                case CC.General.Diccionario.PDFForm.TipoDato.ValidaNulo:
                    strValue = CC.General.Funciones.ValidacionesPol.GetValueString(objValue, string.Empty, objValue);
                    break;
                case CC.General.Diccionario.PDFForm.TipoDato.ValidaFecha:
                    strValue = CC.General.Funciones.ValidacionesPol.GetValueDate(objValue);
                    break;
                case CC.General.Diccionario.PDFForm.TipoDato.ValidaMoneda0:
                    strValue = CC.General.Funciones.ValidacionesPol.GetValueMoney(objValue, 0, objValue);
                    break;
                case CC.General.Diccionario.PDFForm.TipoDato.ValidaMonedaEmpty:
                    strValue = CC.General.Funciones.ValidacionesPol.GetValueMoney(objValue, string.Empty, objValue);
                    break;
                case CC.General.Diccionario.PDFForm.TipoDato.ValidaMonedaSinSimbolo:
                    strValue = CC.General.Funciones.ValidacionesPol.GetValueMoneySSimbolo(objValue, string.Empty, objValue);
                    break;
                case CC.General.Diccionario.PDFForm.TipoDato.Default:
                    strValue = objValue.ToString();
                    break;
            }

            iResult = PDFForm_SetTextField(iIdProcess, strNameField, strValue);
        }

        public void GuardarPDF(string strPlantilla, string strPDFFile)
        {
            iResult = PDFForm_Apply(iIdProcess, strPlantilla, strPDFFile, 1);
            iResult = PDFForm_Free(iIdProcess);
        }
    }
}
