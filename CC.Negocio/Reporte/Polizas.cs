﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Funciones = CC.General.Funciones;
using Dic = CC.General.Diccionario;
using System.Globalization;

namespace CC.Negocio.Reporte
{    
    public class Polizas
    {        
        private CC.Datos.Reporte.Polizas _daPolizas;
        private PDFForm _bsPDF;

        public Polizas()
        {
            _daPolizas = new Datos.Reporte.Polizas();
            _bsPDF = new PDFForm();
        }

        public int Export(string strParametro)
        {
            var ArrParametro = strParametro.Split('|');
            int iIdAseguradora = int.Parse(ArrParametro[0]);
            int iIdPoliza = int.Parse(ArrParametro[1]);
            int iIdBid = int.Parse(ArrParametro[2]);
            int iIdCliente = int.Parse(ArrParametro[3]);
            int iIdContrato = int.Parse(ArrParametro[4]);
            int iIntnumContrato = int.Parse(ArrParametro[5]);
            int iIdllave = int.Parse(ArrParametro[6]);
            int iIdllaveC = int.Parse(ArrParametro[7]);
            int iIdprograma = int.Parse(ArrParametro[8]);
            int iTipoFscar = int.Parse(ArrParametro[9]);

            switch (iIdAseguradora)
            {
                case 1:
                case 4:
                    iIdllave = GetQTS(iIdAseguradora, iIdPoliza, iIdBid, iIdCliente, iIdContrato, iIntnumContrato, iIdllave, iIdllaveC, iIdprograma, iTipoFscar);
                    break;
                case 2:
                case 5:
                    iIdllave = GetCHUBB(iIdAseguradora, iIdPoliza, iIdBid, iIdCliente, iIdContrato, iIntnumContrato, iIdllave, iIdllaveC, iIdprograma, iTipoFscar);
                    break;
                case 3:
                case 6:
                    iIdllave = GetHDI(iIdAseguradora, iIdPoliza, iIdBid, iIdCliente, iIdContrato, iIntnumContrato, iIdllave, iIdllaveC, iIdprograma, iTipoFscar);
                    break;
            }

            return iIdllave;
        }

        private int GetHDI(int iIdAseguradora, int iIdPoliza, int iIdBid, int iIdCliente, int iIdContrato, int inumContrato, int iIdllave, int iIdllaveC, int iIdprograma, int iTipoFscar)
        {
            string strNameFile = string.Empty, strPlantilla = string.Empty;
            Modelo.Reporte.Polizas.ObjHDI _objHDI = new Modelo.Reporte.Polizas.ObjHDI();

            var objPoliza = _daPolizas.GetData(iIdPoliza);
            if (objPoliza != null)
            {
                if (iIdprograma == 1)
                {
                    strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                CC.General.Diccionario.PDFForm.PLANTILLA_HDI_F;
                }
                else if (iIdprograma == 2)
                {
                    strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                CC.General.Diccionario.PDFForm.PLANTILLA_HDI_C;
                }


                //strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                //    CC.General.Diccionario.PDFForm.PLANTILLA_HDI;
                strNameFile = "PolizaHDI_" + iIdPoliza.ToString() + "_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Nombre", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Direccion", Funciones.ValidacionesPol.GetValueString(objPoliza.calle, string.Empty, objPoliza.calle) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", NO. EXT." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", NO. INT." + objPoliza.NoInterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.Colonia_Cliente, string.Empty, ", COL." + objPoliza.Colonia_Cliente) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.Estado_Cliente, string.Empty, ", Edo." + objPoliza.Estado_Cliente));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Direccion2", Funciones.ValidacionesPol.GetValueString(objPoliza.calle, string.Empty, objPoliza.calle) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", NO. EXT." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", NO. INT." + objPoliza.NoInterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.Colonia_Cliente, string.Empty, ", COL." + objPoliza.Colonia_Cliente));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "RFC", objPoliza.Rfc);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Poliza", objPoliza.Poliza);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "PolizaAnterior", objPoliza.Poliza_Multianual);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "FechaInicio", objPoliza.inicio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "FechaFinal", objPoliza.fin);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Documento", "POLIZA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Agente", "063433 AON RISK SOLUTIONS AGENTE DE SEGUROS Y DE FIANZAS SA DE CV");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "DescripcionVehiculo", objPoliza.insco + " " + objPoliza.Marca + " " + objPoliza.Descripcion);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Version", objPoliza.Version);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Serie", objPoliza.Serie);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Motor", objPoliza.Motor);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Carga", "NO APLICA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Uso", "AUTOMÓVILES RESIDENTES");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Ocupantes", objPoliza.Capacidad);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Placas", objPoliza.Placas);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Paquete", objPoliza.Descripcion_Paquete);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "TipoSuma", "VALOR FACTURA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Servicio", "PARTICULAR");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Remolque", "NO");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Circulacion", objPoliza.Estado_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtFormaPago", objPoliza.Descripcion_Periodo.ToUpper());

                int iIndex = 0;
                var lstCoberturas = new Datos.Reporte.Coberturas().GetData(iIdPoliza);
                decimal dSuma = 0;
                lstCoberturas.ForEach(x =>
                {
                    iIndex++;   
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Cobertura" + iIndex.ToString() + "_", x.Cobertura.ToUpper());
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "LimiteResponsabilidad" + iIndex.ToString() + "_", (x.Descripcion_Suma.Contains("Valor Factura") ?
                        Funciones.ValidacionesPol.GetValueMoneyNotDec(objPoliza.CADENA.Split('/')[2]) : (decimal.TryParse(x.Descripcion_Suma, out dSuma) ?
                            Funciones.ValidacionesPol.GetValueMoneyNotDec(x.Descripcion_Suma) : x.Descripcion_Suma)));
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Deducible" + iIndex.ToString() + "_", Funciones.ValidacionesPol.GetValueString(x.Deducible, string.Empty, x.Deducible));
                });
                                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "PrimaNeta", objPoliza.PrimaNeta);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "Descuento", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "PrimaModulos", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "RecPagoFraccionado", objPoliza.Pago_Fraccionado);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "ReduccionAutorizada", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "DerechoPoliza", objPoliza.DerPol);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "Iva", objPoliza.Iva);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "PrimaTotal", objPoliza.PrimaTotal);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Submarca", objPoliza.Modelo);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "ConductorHabitual", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Moneda", "PESOS");

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Modelo", objPoliza.Modelo);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "EstadoCliente", objPoliza.Estado_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Ciudad", objPoliza.Estado_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Submarca", objPoliza.Modelo);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "CP", objPoliza.Cp_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Email", objPoliza.Email);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Anio", objPoliza.Anio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "Marca", objPoliza.Marca);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "FormaPago", objPoliza.Descripcion_Periodo.ToUpper() + " EFECTIVO");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "FechaEmision", objPoliza.inicio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "FechaNac", objPoliza.FechaNacimiento);
                DateTimeFormatInfo dtinfo = new CultureInfo("es-MX", false).DateTimeFormat;                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtLugarFecha", "León, Gto. " + DateTime.Now.Day.ToString() + " de " + dtinfo.GetMonthName(DateTime.Now.Month) + " del " + DateTime.Now.Year.ToString());
                

                var lstRecibos = _daPolizas.GetRecibos(objPoliza.Id_Poliza);
                for (int x = 0; x < lstRecibos.Count; x++)
                {
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "NoRecibo" + (x + 1).ToString(), lstRecibos[x].Num_Fraccion.ToString());
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "FechaRecibo" + (x + 1).ToString(),
                        ((DateTime)lstRecibos[x].Iniciovigencia_Fraccion).ToString("dd/MM/yyyy") + " al " + ((DateTime)lstRecibos[x].FinVigencia_Fraccion).ToString("dd/MM/yyyy"));
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "ImporteRecibo" + (x + 1).ToString(), lstRecibos[x].PTotal_Recibo);
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "FechaPagoRecibo" + (x + 1).ToString(),
                        ((DateTime)lstRecibos[x].Iniciovigencia_Fraccion).AddMonths((x == 0 ? 1 : 0)).ToString("dd/MM/yyyy"));
                }

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "RC_PrimaNeta", objPoliza.RCUSA_MULTI);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "RC_DerPol", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "RC_IVA", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "RC_PrimaTotal", objPoliza.RCUSA_TOTAL);

                    _bsPDF.GuardarPDF(strPlantilla, System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                        CC.General.Diccionario.Carpetas.POLIZA_PDF + strNameFile);

                _daPolizas.ActualizaContenedor(iIdllave, strNameFile);                                
            }

            return iIdllave;
        }

        private int GetQTS(int iIdAseguradora, int iIdPoliza, int iIdBid, int iIdCliente, int iIdContrato, int inumContrato, int iIdllave, int iIdllaveC, int iIdprograma, int iTipoFscar)
        {
            string strNameFile = string.Empty, strPlantilla = string.Empty;
            Modelo.Reporte.Polizas.ObjHDI _objHDI = new Modelo.Reporte.Polizas.ObjHDI();

            var objPoliza = _daPolizas.GetData(iIdPoliza);
            if (objPoliza != null)
            {
                if (iIdprograma == 1){
                        strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                    CC.General.Diccionario.PDFForm.PLANTILLA_QTS_F;
                }
                else if (iIdprograma == 2){
                        strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                    CC.General.Diccionario.PDFForm.PLANTILLA_QTS_C;
                }


                strNameFile = "Qualitas_" + iIdPoliza.ToString() + "_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtNombreAseg", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtInciso", "0001");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtEndoso", "000000");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtOficina", "TOLUCA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtLocalizacion", "ESTADO DE MEXICO");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtDomicilio", "ALLENDE SUR #406");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtColonia", "FRANCISCO MURGIA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtCP", "50000");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtCPAseg", objPoliza.Cp_Cliente);                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtLocal", "(722) 8001020");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtFax", "01-800-004-9600");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtNombreAgente", "72743 - AON RISK SOLUTIONS, AGENTE DE SEGUROS Y FIANZAS, S.A. DE C.V.");

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDomicilioAseg", objPoliza.calle +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", NO. EXT." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", NO. INT." + objPoliza.NoInterior));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtCiudadAseg", objPoliza.Ciudad_Cliente + ", " + objPoliza.Estado_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtRFCAseg", objPoliza.Rfc);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPago", objPoliza.PagoInicial);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtPagoSubsecuente", (objPoliza.PagoSubsecuente == null || objPoliza.PagoSubsecuente == 0 ? string.Empty :
                    "Pagos Subsecuentes: " + CC.General.Funciones.ValidacionesPol.GetValueMoney(objPoliza.PagoSubsecuente, string.Empty, objPoliza.PagoSubsecuente)));
                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtRFCAseg", objPoliza.Rfc);
                //_bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "", objPoliza.Plazo);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPoliza", objPoliza.Poliza);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "txtVigenciaDesde", objPoliza.inicio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "txtVigenciaHasta", objPoliza.fin);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtRenaveVeh", objPoliza.Renave);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtColoniaAseg", objPoliza.Colonia_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtServicio", "PARTICULAR");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "txtFecVenc", ((DateTime)objPoliza.inicio).AddDays(14));

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtdeeler", (objPoliza.PagoSubsecuente == null || objPoliza.PagoSubsecuente == 0 ? 
                    objPoliza.nom + (objPoliza.Num_Contrato == null || objPoliza.Num_Contrato == string.Empty ? string.Empty : " - " + objPoliza.Num_Contrato) : objPoliza.Benef_Pref));

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDescVeh", objPoliza.insco + " " + objPoliza.Marca + " " + objPoliza.Descripcion );
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtVersion", objPoliza.Version);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtSerieVeh", objPoliza.Serie);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtMotorVeh", objPoliza.Motor);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtMovimiento", "ALTA");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtMoneda", "PESOS");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtTipoVeh", "AUTOMÓVIL");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtOcupantesVeh", objPoliza.Capacidad);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtModeloVeh", objPoliza.Anio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtCarga", objPoliza.Id_TipoCarga);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtUso", "PRIVADO");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPlacas", objPoliza.Placas);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPaquete", objPoliza.Paquete);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtImporteTotal", objPoliza.PrimaTotal);

                int iIndex = 0;
                var lstCoberturas = new Datos.Reporte.Coberturas().GetData(iIdPoliza);
                decimal dSuma = 0;
                lstCoberturas.ForEach(x =>
                {                    
                    iIndex++;
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtCoberturas" + iIndex.ToString() + "_", x.Cobertura.ToUpper());
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtSumaAsegurada" + iIndex.ToString() + "_", (x.Descripcion_Suma.Contains("Valor Factura") ?
                        Funciones.ValidacionesPol.GetValueMoneyNotDec(objPoliza.CADENA.Split('/')[2]) : (decimal.TryParse(x.Descripcion_Suma, out dSuma) ?
                            Funciones.ValidacionesPol.GetValueMoneyNotDec(x.Descripcion_Suma) : x.Descripcion_Suma)));

                    if (x.Cobertura.ToUpper().Contains("ROBO TOTAL") && new Datos.Reporte.Coberturas().ValidaQualitasRiesgo(objPoliza.Cp_Cliente, objPoliza.Modelo))
                        _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtDeducible" + iIndex.ToString() + "_", "20%");
                    else
                        _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDeducible" + iIndex.ToString() + "_", Funciones.ValidacionesPol.GetValueString(x.Deducible, string.Empty, x.Deducible));

                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtSimbolo" + iIndex.ToString(), "$");
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMonedaSinSimbolo, "txtPrimas" + iIndex.ToString() + "_", x.PrimaCobertura);
                });
                DateTimeFormatInfo dtinfo = new CultureInfo("es-MX", false).DateTimeFormat;
                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtFecEmision", "A " + DateTime.Now.Day.ToString() + " DE " + dtinfo.GetMonthName(DateTime.Now.Month).ToUpper() + 
                    " DEL " + DateTime.Now.Year.ToString());                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtPlazo", "14");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPrimaNeta", objPoliza.PrimaNeta);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtGastos", objPoliza.der_pol);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtTasa", objPoliza.Pago_Fraccionado);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtDescuento", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPrimaModulos", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtRecPagoFraccionado", objPoliza.Pago_Fraccionado);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtReduccionAutorizada", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtDerechoPoliza", objPoliza.DerPol);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtIva", objPoliza.Iva);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtFormaPago", objPoliza.Descripcion_Periodo);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtSubtotal", objPoliza.PrimaNeta + objPoliza.der_pol + objPoliza.Pago_Fraccionado);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPrimaTotal", objPoliza.PrimaTotal);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtConductorHabitual", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "Moneda", "PESOS");

                _bsPDF.GuardarPDF(strPlantilla, System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                    CC.General.Diccionario.Carpetas.POLIZA_PDF + strNameFile);

                _daPolizas.ActualizaContenedor(iIdllave, strNameFile);                                
            }

            return iIdllave;
        }

        private int GetCHUBB(int iIdAseguradora, int iIdPoliza, int iIdBid, int iIdCliente, int iIdContrato, int inumContrato , int iIdllave, int iIdllaveC, int iIdprograma, int iTipoFscar)
        {
            string strNameFile = string.Empty, strPlantilla = string.Empty;

            var objPoliza = _daPolizas.GetData(iIdPoliza);
            if (objPoliza != null)
            {
                if (iIdprograma == 1)
                {
                    strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                CC.General.Diccionario.PDFForm.PLANTILLA_CHUBB_F;
                }
                else if (iIdprograma == 2)
                {
                    strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                        CC.General.Diccionario.PDFForm.PLANTILLA_CHUBB_C;
                }


                //strPlantilla = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() + CC.General.Diccionario.Carpetas.POLIZA_PLANTILLAS +
                //    CC.General.Diccionario.PDFForm.PLANTILLA_CHUBB;
                strNameFile = "PolizaCHUBB_" + iIdPoliza.ToString() + "_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPoliza", objPoliza.Poliza);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtVigencia", "Del " + CC.General.Funciones.ValidacionesPol.GetValueDate(objPoliza.inicio) + " 12:00 horas al " +
                    CC.General.Funciones.ValidacionesPol.GetValueDate(objPoliza.fin) + " 12:00 horas");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtInciso", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtEndoso", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtAsegClave", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPaquete", string.Empty);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtAsegurado", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPropietario", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDomicilio", Funciones.ValidacionesPol.GetValueString(objPoliza.calle, string.Empty, objPoliza.calle) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", No.\n Ext." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", No. Int." + objPoliza.NoInterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.Colonia_Cliente, string.Empty, ", Col." + objPoliza.Colonia_Cliente));                
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtCodigoPostal", objPoliza.Cp_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtTelefono", objPoliza.Telcliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtRFC", objPoliza.Rfc);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtPolizaAnt", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtMoneda", "Pesos");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtFormaPago", objPoliza.Descripcion_Periodo);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "txtFechaEmision", objPoliza.Fecha_Registro);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtReferencia", objPoliza.Num_Contrato);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtClaveAgente", objPoliza.Clave_Agente);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDescVehiculo", objPoliza.Marca + " " + objPoliza.Descripcion);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtModelo", objPoliza.Anio);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtSerie", objPoliza.Serie);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtMarca", objPoliza.Marca);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtCapacidad", objPoliza.Capacidad);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtMotor", objPoliza.Motor);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtClaveVehicular", objPoliza.insco);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtServicio", "Particular");
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtPlacas", objPoliza.Placas);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtUso", "Privado");
                DateTimeFormatInfo dtinfo = new CultureInfo("es-MX", false).DateTimeFormat;
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtLugarFecha", "Ciudad de México a " + DateTime.Now.Day.ToString() + " de " + dtinfo.GetMonthName(DateTime.Now.Month) + " del " + DateTime.Now.Year.ToString());

                int iIndex = 0;
                var lstCoberturas = new Datos.Reporte.Coberturas().GetData(iIdPoliza);
                decimal dSuma = 0;
                lstCoberturas.ForEach(x =>
                {
                    iIndex++;
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtCoberturas" + iIndex.ToString(), x.Cobertura);
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtSuma" + iIndex.ToString(), (x.Descripcion_Suma.Contains("Valor Factura") ?
                        Funciones.ValidacionesPol.GetValueMoneyNotDec(objPoliza.CADENA.Split('/')[2]) : (decimal.TryParse(x.Descripcion_Suma, out dSuma) ?
                            Funciones.ValidacionesPol.GetValueMoneyNotDec(x.Descripcion_Suma) : x.Descripcion_Suma)));
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDeducible" + iIndex.ToString(), Funciones.ValidacionesPol.GetValueString(x.Deducible, "NO APLICA", x.Deducible));
                    _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMonedaEmpty, "txtPrima" + iIndex.ToString(), string.Empty);
                });

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPrimaNeta", objPoliza.PrimaNeta);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtDescuentos", 0);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtFPPF", objPoliza.Pago_Fraccionado);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtDerPol", objPoliza.DerPol);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtIva", objPoliza.Iva);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaMoneda0, "txtPrimaTotal", objPoliza.PrimaTotal);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtConductor", objPoliza.nom);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDomicilioCond", Funciones.ValidacionesPol.GetValueString(objPoliza.calle, string.Empty, objPoliza.calle) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", No. Ext." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", No. Int." + objPoliza.NoInterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.Colonia_Cliente, string.Empty, ", Col." + objPoliza.Colonia_Cliente));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtEstado", objPoliza.Estado_Cliente);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtVersion", objPoliza.Version);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtTipo", objPoliza.Tipo_Carga);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtRepuve", objPoliza.Renave);

                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtAnioIni", objPoliza.inicio.Value.Year.ToString());
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtMesIni", objPoliza.inicio.Value.Month.ToString().PadLeft(2, '0'));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtDiaIni", objPoliza.inicio.Value.Day.ToString().PadLeft(2, '0'));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtAnioFin", objPoliza.fin.Value.Year.ToString());
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtMesFin", objPoliza.fin.Value.Month.ToString().PadLeft(2, '0'));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtDiaFin", objPoliza.fin.Value.Day.ToString().PadLeft(2, '0'));
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaFecha, "txtNacimiento", objPoliza.FechaNacimiento);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtCel", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.Default, "txtSexo", string.Empty);
                _bsPDF.SetValue(Dic.PDFForm.TipoDato.ValidaNulo, "txtDireccion", Funciones.ValidacionesPol.GetValueString(objPoliza.calle, string.Empty, objPoliza.calle) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoExterior, string.Empty, ", No. Ext." + objPoliza.NoExterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", No. Int." + objPoliza.NoInterior) +
                    Funciones.ValidacionesPol.GetValueString(objPoliza.NoInterior, string.Empty, ", Col." + objPoliza.Colonia_Cliente));

                _bsPDF.GuardarPDF(strPlantilla, System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                    CC.General.Diccionario.Carpetas.POLIZA_PDF + strNameFile);

                _daPolizas.ActualizaContenedor(iIdllave, strNameFile);                
            }

            return iIdllave;
        }
    }
}
