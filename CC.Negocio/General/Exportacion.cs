﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModeloST = Serti.Tools.Exp.Modelo.Excel;

namespace CC.Negocio.General
{
    public class Exportacion
    {
        public static string Excel(System.Data.DataTable dt, string funcionalidad)
        {
            Serti.Tools.Exp.Modelo.Excel.ObjExcel objetoExcel = new Serti.Tools.Exp.Modelo.Excel.ObjExcel();

            try
            {
                if (dt.Rows.Count > 0)
                {
                    if (!System.IO.Directory.Exists(System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                        CC.General.Diccionario.Carpetas.REPORTES))
                        System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                        CC.General.Diccionario.Carpetas.REPORTES);

                    objetoExcel.NombreArchivo = System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.RUTA_MAZDA].ToString() +
                        CC.General.Diccionario.Carpetas.REPORTES + "Solicitudes" + DateTime.Now.ToString("ddMMyyhhmmss") + ".xlsx";

                    List<ModeloST.ObjTexto> lstObjTexto = new List<ModeloST.ObjTexto>();

                    for (int x = 0; x < dt.Columns.Count; x++)
                        lstObjTexto.Add(new ModeloST.ObjTexto()
                        {
                            MIzquierda = x + 1,
                            MSuperior = 1,
                            Estilo = ModeloST.EstiloCell.Negrita_Negro_Centrado_Fondo_Gris,
                            Tamanio = 8,
                            Texto = dt.Columns[x].ColumnName
                        });

                    for (int x = 0; x < dt.Rows.Count; x++)
                    {
                        for (int y = 0; y < dt.Columns.Count; y++)
                        {
                            lstObjTexto.Add(new ModeloST.ObjTexto()
                            {
                                MIzquierda = y + 1,
                                MSuperior = x + 2,
                                Estilo = ModeloST.EstiloCell.Ninguno,
                                Tamanio = 8,
                                Texto = (dt.Rows[x].IsNull(y) ? String.Empty : dt.Rows[x][y].ToString())
                            });
                        }
                    }

                    List<ModeloST.ColumnasHoja> lstColumnas = new List<ModeloST.ColumnasHoja>();
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 1, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 2, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 3, Ancho = 60 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 4, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 5, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 6, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 7, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 8, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 9, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 10, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 11, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 12, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 13, Ancho = 30 });
                    lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 14, Ancho = 30 });

                    if (funcionalidad == CC.General.Diccionario.Solicitudes.FUNC_SOLICITUDES)
                    {
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 15, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 16, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 17, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 18, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 19, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 20, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 21, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 22, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 23, Ancho = 80 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 24, Ancho = 15 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 25, Ancho = 10 });
                    }
                    else
                    {
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 15, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 16, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 17, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 18, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 19, Ancho = 30 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 20, Ancho = 80 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 21, Ancho = 15 });
                        lstColumnas.Add(new ModeloST.ColumnasHoja() { Columna = 22, Ancho = 10 });
                    }

                    List<ModeloST.FilasHoja> lstFilas = new List<ModeloST.FilasHoja>();
                    lstFilas.Add(new ModeloST.FilasHoja() { Fila = 1, Alto = 23 });

                    objetoExcel.LstPropSheet.Add(new ModeloST.Propiedades_Hoja()
                    {
                        NombreHoja = "Hoja1",
                        NumeroHoja = 1,
                        ListaCeldas = lstObjTexto,
                        MuestraCuadricula = true,
                        PropColumnas = lstColumnas,
                        PropFilas = lstFilas
                    });

                    Serti.Tools.Exp.OpenXMLExcel.Create crear = new Serti.Tools.Exp.OpenXMLExcel.Create();
                    crear.CrearArchivoExcel(objetoExcel);
                }
                return objetoExcel.NombreArchivo;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }
}
