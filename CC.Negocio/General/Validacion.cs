﻿using System;
using System.Text.RegularExpressions;

namespace CC.Negocio.General
{
    public class Validacion
    {
        public static bool Email(string strEMail)
        {
            return Regex.IsMatch(strEMail, "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
        }

        public static bool DropDown(string strValue)
        {
            return (strValue == "P");
        }

        public static bool NombreUsuario(string strValue)
        {
            return (strValue == string.Empty);
        }
    }
}
