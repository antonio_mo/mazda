﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Negocio.General
{
    public class Conversiones
    {
        public static string ToString(string strTexto)
        {
            if (strTexto == null)
                return "_";
            else
                return strTexto.ToString();
        }

        public static string DateToString(DateTime? datTexto)
        {
            if (datTexto == null)
                return "_";
            else
                return ((DateTime)datTexto).ToString("dd/MM/yyyy hh:mm:ss");
        }

        public static string ShortToString(short? srtTexto)
        {
            if (srtTexto == null)
                return "_";
            else
                return srtTexto.ToString();
        }

        public static string DecimalToString(decimal? decTexto)
        {
            if (decTexto == null)
                return "_";
            else
                return decTexto.ToString();
        }

        public static string ByteToString(byte? byteTexto)
        {
            if (byteTexto == null)
                return "_";
            else
                return byteTexto.ToString();
        }
    }
}
