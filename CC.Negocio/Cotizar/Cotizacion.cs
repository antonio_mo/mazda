﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Negocio.Cotizar
{
    public class Cotizacion
    {
        private Datos.Cotizar.Cotizacion _daCotizacion;

        public Cotizacion()
        {
            _daCotizacion = new Datos.Cotizar.Cotizacion();
        }

        public Modelo.Entities.ABC_ESTADO GetStateByZipCode(string strZipCode)
        {
            return _daCotizacion.GetStateByZipCode(strZipCode);
        }
    }
}
