﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CC.Negocio.Consultas
{
    public class Solicitudes
    {
        public string Exportar(string funcionalidad, string idSesion)
        {
            string strSql = string.Empty;

            try
            {
                strSql = "SELECT [IdSolicitud] as [Id_Solicitud], [TipoSolicitud] as [Tipo Solicitud], [DETALLE] as [Detalle], [Poliza] as [Póliza], [Serie] as [Serie], " +
                    "[Contrato] as [Contrato], [Cliente] as [Nombre del Cliente], [Aseguradora] as [Aseguradora], [FECSOLICITADO] as [Fecha Solicitado], [Status] as [Status], " +
                    "[FechaStatus] as [Fecha Status], [USUARIOSOLICITANTE] as [Quien Solicita], [NSSTATUS] as [Nivel Servicio a Status], [FECCERRADO] as [Fecha Cerrado], " +
                    "{0} [NSTRAMITE] as [Nivel Servicio Trámite], [NSCERRADO] as [Nivel Servicio Cierre], {1} [FECTRAMITE] as [Fecha Trámite], {2} " +
                    "[SLA] as [SLA], [CONFORMESLA] as [Conforme SLA], (select top 1 Empresa from BID b where Bid_Aon = [DISTRIBUIDOR]) as [Distribuidor], " +
                    "[NuevaPoliza] as [Nueva Poliza], [Bandera] as [VIP] FROM [SOLICITUD_REPORTE_TEMPORAL] where idsesion = '" + idSesion + "'";

                if (funcionalidad == CC.General.Diccionario.Solicitudes.FUNC_SOLICITUDES)
                    strSql = string.Format(strSql, "[NSPENDIENTE] as [Nivel Servicio Pendiente], ", "[FECPENDIENTE] as [Fecha Pendiente], ",
                        "[FECCANCELADO] as [Fecha Cancelado], ");
                else
                    strSql = string.Format(strSql, string.Empty, string.Empty, string.Empty);

                CC.Datos.General.Conexion conexionClass = new CC.Datos.General.Conexion();
                System.Data.DataTable dt = conexionClass.EjecutaConsulta(strSql);

                return General.Exportacion.Excel(dt, funcionalidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Modelo.Solicitudes.Poliza GetPol(string strPoliza, int iIdBid)
        {
            Datos.Reporte.Polizas _daPolizas = new Datos.Reporte.Polizas();
            return _daPolizas.GetPol(strPoliza, iIdBid);
        }

        public bool PolizaDerivada(string strPoliza)
        {
            string strSql = string.Empty;
            bool result = false;
            try
            {
                strSql = "SELECT idSaf FROM POLIZA WHERE Poliza='" + strPoliza.Trim() + "'";
                CC.Datos.General.Conexion conexionClass = new CC.Datos.General.Conexion();
                System.Data.DataTable dt = conexionClass.EjecutaConsulta(strSql);

                foreach (DataRow row in dt.Rows)
                {
                    object value = row["idSaf"];

                    if (value == DBNull.Value)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
