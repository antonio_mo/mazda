﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Negocio.Administracion
{
    public class BID
    {
        private Datos.Administracion.BID _daBid;

        public BID()
        {
            _daBid = new Datos.Administracion.BID();
        }

        public Modelo.Entities.BID GetBid(int iIdBid)
        {
            return _daBid.GetBid(iIdBid);
        }
    }
}
