﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model = CC.Modelo;

namespace CC.Negocio.Administracion
{
    public class Usuarios
    {
        CC.Datos.Administracion.Usuarios _daUsuarios;

        public Usuarios()
        {
            _daUsuarios = new Datos.Administracion.Usuarios();
        }

        public Modelo.Entities.USUARIO GetData(int iIdUser)
        {
            return _daUsuarios.GetData(iIdUser);
        }        
    }
}