﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Negocio.Administracion
{
    public class Polizas
    {
        private Datos.Administracion.Polizas _daPolizas;

        public Polizas()
        {
            _daPolizas = new Datos.Administracion.Polizas();
        }

        public List<Modelo.Administracion.PolizaDG> Listar(string strPoliza)
        {
            return _daPolizas.Listar(strPoliza);
        }

        public Modelo.Entities.POLIZA Get(int iIdPoliza)
        {
            return _daPolizas.Get(iIdPoliza);
        }
    }
}
