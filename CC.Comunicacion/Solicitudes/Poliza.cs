﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Comunicacion.Solicitudes
{
    public class Poliza
    {
        public bool ValidaPoliza (string strPoliza)
        {
            WCFSolicitudes.WCFSolicitudesClient _wcfSolicitudes = new WCFSolicitudes.WCFSolicitudesClient();
            return _wcfSolicitudes.ValidaPoliza(strPoliza);
        }
    }
}
