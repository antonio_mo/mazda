﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Comunicacion.General
{
    public class Correos
    {
        public void EnviaCorreo(CC.General.Diccionario.Correos.Tipos enTipos, CC.Modelo.Solicitudes.Correo objCorreo)
        {
            string strSsubject = string.Empty, strBody = string.Empty, strMailpara = string.Empty, strMailcc = string.Empty, strMailbcc = string.Empty, strFrom = string.Empty;
            WsCorreo.Service1SoapClient wsObjMail = new WsCorreo.Service1SoapClient();

            try
            {
                strFrom = "Emisor de Pólizas Mazda Retail <mx.emisormazda@aon.com>";
                strMailpara = objCorreo.Para;
                strMailcc = objCorreo.CC + "," + CC.General.Diccionario.Solicitudes.MAIL_CC;
                //strMailbcc = CC.General.Diccionario.Solicitudes.MAIL_CC;

                switch (enTipos)
                {
                    case CC.General.Diccionario.Correos.Tipos.ConfirmaSolicitud:
                        strSsubject = "Confirmación de solicitud cargada en el emisor Mazda Retail";
                        strBody = "<br> Se ha creado una solicitud en el emisor Mazda Retail con la siguiente información: " +
                            "<br/>Id de Solicitud:<br><b>" + objCorreo.IdSolicitud + "</b></br>" +
                            "<br/>Detalle:<br><b>" + objCorreo.Detalle + "</b></br>" +
                            "<br/>Observaciones:<br><b>" + objCorreo.Observaciones + "</b></br>" +
                            "<br/><br/> <a href='http://192.168.166.194/MAZDARETAIL/principal/frmlogin.aspx'>Da click aquí para ingresar a la aplicación</a>" +
                            "<br/>";

                        break;
                    case CC.General.Diccionario.Correos.Tipos.ActualizaSolicitud:
                        strSsubject = "Actualización de solicitud cargada en el emisor Mazda Retail";
                        strBody = "<br> Se ha actualizado la solicitud en el emisor Mazda Retail con la siguiente información: " +
                            "<br/>Id de Solicitud:<br><b>" + objCorreo.IdSolicitud + "</b></br>" +
                            "<br/>Detalle:<br><b>" + objCorreo.Detalle + "</b></br>" +
                            "<br/>Observaciones:<br><b>" + objCorreo.Observaciones + "</b></br>" +
                            "<br/><br/> <a href='http://192.168.166.194/MAZDARETAIL/principal/frmlogin.aspx'>Da click aquí para ingresar a la aplicación</a>" +
                            "<br/>";

                        break;
                }

                strBody += " <br>" +
                    " <br> Favor de no responder a este correo, si tiene dudas o comentarios " +
                    " <br> correspondientes a los datos de su usuario, favor de consultar " +
                    " <br> a su administrador" +
                    " <br> Contacto Email : mailto:" + CC.General.Diccionario.Solicitudes.MAIL_CONTACTO +
                    " <br> Visita: www.aon.com" +
                    " <br> Aon..." +
                    " <br> La seguridad de un gran respaldo" +
                    " <br>";

                wsObjMail.MailAYB(strFrom, strMailpara, strSsubject, strBody, true, null, "o9its725", "emisorMazda", strMailcc, strMailbcc, "AYB");
            }
            catch(Exception ex)
            {

            }       
        }
    }
}
