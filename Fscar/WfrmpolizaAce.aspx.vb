Imports CN_Negocios
Imports Emisor_Conauto

Partial Class WfrmpolizaAce
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    'EHG - Cambio de DLL

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                If Not Request.QueryString("ban") Is Nothing Then
                    Session("ban") = Request.QueryString("ban")
                End If
                limpia()
                carga_leyenda()
                carga_informacion()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        lbl_titulo.Text = ""
        lbl_seguro.Text = ""
        lbl_campo1.Text = ""
        lbl_nombre.Text = ""
        lbl_campo2.Text = ""
        lbl_fechaInicio.Text = ""
        lbl_fechaFin.Text = ""
        lbl_primat.Text = ""
        lbl_campo3.Text = ""
    End Sub

    Public Sub carga_leyenda()
        Dim cadena As String = ""
        cadena = "<br>ACE Seguros, S.A."
        cadena = cadena & "<br>Edificio Arcos Oriente"
        cadena = cadena & "<br>Bosques de Alisos 47-A"
        cadena = cadena & "<br>Bosques de las Lomas"
        cadena = cadena & "<br>11520, M�xico, D.F."
        cadena = cadena & "<br>Tel. 52 58 58 64"
        cadena = cadena & "<br>01 800 90 99 800"
        cadena = cadena & "<br>R.F.C. ASE901221SM4"
        lbl_titulo.Text = cadena
    End Sub

    Public Sub carga_informacion()
        Dim cadena As String = ""
        Dim dt As DataTable = ccliente.carga_reporte_poliza_desempleo(Session("idpoliza"), Session("bid"), Session("aseguradora"))
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("poliza") Then
                    lbl_pol.Text = dt.Rows(0)("poliza")
                End If
                If Not dt.Rows(0).IsNull("polizaace") Then
                    lbl_seguro.Text = dt.Rows(0)("polizaace")
                End If
                cadena = "<br>Al amparo de la p�liza N� " & lbl_seguro.Text & " de p�liza emitida a favor de FORD CREDIT DE MEXICO, S.A. DE C.V., </br>"
                cadena = cadena & "<br>SOCIEDAD FINANCIERA DE OBJETO M�LTIPLE, ENTIDAD NO REGULADA se cubre a :</br>"
                lbl_campo1.Text = cadena
                If Not dt.Rows(0).IsNull("NOMBRE") Then
                    lbl_nombre.Text = dt.Rows(0)("NOMBRE")
                End If
                cadena = ""
                If Not dt.Rows(0).IsNull("Descripcion") Then
                    cadena = "<br>Unidad " & Trim(dt.Rows(0)("Descripcion")) & "<br/>"
                    cadena = cadena & "<br>por el seguro Desempleo o Incapacidad Temporal Total por Accidente de acuerdo a las Condiciones Generales<br>"
                    cadena = cadena & "<br>que se anexan<br>"
                    lbl_campo2.Text = cadena
                End If
                If Not dt.Rows(0).IsNull("fi") Then
                    lbl_fechaInicio.Text = dt.Rows(0)("fi")
                End If
                If Not dt.Rows(0).IsNull("ff") Then
                    lbl_fechaFin.Text = dt.Rows(0)("ff")
                End If
                If Not dt.Rows(0).IsNull("prima_desempleo") Then
                    lbl_primat.Text = Format(dt.Rows(0)("prima_desempleo"), "$ #,###,##0.00")
                End If
                If Not dt.Rows(0).IsNull("descripcion_region") Then
                    cadena = ""
                    cadena = "Se firma el presente en la Ciudad de " & dt.Rows(0)("descripcion_region") & " a " & lbl_fechaInicio.Text
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regresar.Click
        Response.Redirect("..\cotizar\WfrmImp.aspx?band=2")
    End Sub

    Private Sub cmd_imprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_imprimir.Click
        Dim cadenaEnvio As String
        Try
            Session("Bandera_Espera") = 1
            Dim llave As Integer = ccliente.inserta_contenedor_reportes(Session("bid"), lbl_pol.Text, "P")
            Session("Llave") = llave
            Session("LlaveC") = 0

            'Parametros = "6 |60|38|0|0|0|170|0|4|3"

            cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
                    Session("bid") & "|0|0|0|" & Session("Llave") & "|0|" & Session("programa") & "|3"

            Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            _bsPolizas.Export(cadenaEnvio)

            Response.Redirect("..\cotizar\EsperaReportes.html")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
