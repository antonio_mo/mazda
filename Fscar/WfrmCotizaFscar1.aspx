<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotizaFscar1.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotizaFscar1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 101; LEFT: 376px; POSITION: absolute; TOP: 456px" runat="server"></cc1:msgbox>
			<HR class="lineas" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 112; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Datos 
							del&nbsp;Cotizador</FONT></TD>
				</TR>
			</TABLE>
			<asp:textbox id="txt_plan" style="Z-INDEX: 115; LEFT: 512px; POSITION: absolute; TOP: 376px"
				runat="server" Visible="False" Height="16px" Width="16px"></asp:textbox><asp:textbox id="txt_anio" style="Z-INDEX: 116; LEFT: 464px; POSITION: absolute; TOP: 376px"
				runat="server" Visible="False" Height="16px" Width="16px"></asp:textbox>
			<TABLE id="Table1" style="Z-INDEX: 117; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="80%" border="0">
				<TR>
					<TD class="obligatorio" width="25%"><asp:label id="lbl_fscar" runat="server" Width="100%">Tipo Venta Vehiculo :</asp:label></TD>
					<TD class="obligatorio" width="25%" colSpan="2"><asp:radiobuttonlist id="rb_fscar" runat="server" Width="100%" AutoPostBack="True" RepeatDirection="Horizontal"
							CssClass="combos_small">
							<asp:ListItem Value="1">Financiado</asp:ListItem>
							<asp:ListItem Value="2">Contado</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD class="obligatorio" width="25%"></TD>
				</TR>
				<TR>
					<TD class="obligatorio" width="25%">Automóvil :</TD>
					<TD class="obligatorio" width="25%">Moneda :</TD>
					<TD class="obligatorio" width="25%">Marca :</TD>
					<TD class="obligatorio" width="25%">Tipo :</TD>
				</TR>
				<TR>
					<TD vAlign="top"><asp:radiobuttonlist id="rb_auto" runat="server" Width="100%" AutoPostBack="True" RepeatDirection="Horizontal"
							CssClass="negrita">
							<asp:ListItem Value="1" Selected="True">Nuevo</asp:ListItem>
							<asp:ListItem Value="2">Seminuevo</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD vAlign="top"><asp:label id="lbl_pesos" runat="server" Width="100%" CssClass="negrita">Pesos</asp:label></TD>
					<TD vAlign="top"><asp:dropdownlist id="cbo_marca" runat="server" Width="100%" AutoPostBack="True" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD vAlign="top"><asp:dropdownlist id="cbo_modelo" runat="server" Width="100%" AutoPostBack="True" CssClass="combos_small"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Modelo :</TD>
					<TD class="obligatorio">Descripción :</TD>
					<TD></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD><asp:dropdownlist id="cbo_anio" runat="server" Width="100%" AutoPostBack="True" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD colSpan="3"><asp:dropdownlist id="cbo_descripcion" runat="server" Width="100%" AutoPostBack="True" CssClass="combos_small"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="obligatorio">
						<P>Uso :</P>
					</TD>
					<TD class="obligatorio">
						<P>Tipo de Contribuyente :</P>
					</TD>
					<TD class="obligatorio">
						<P>&nbsp;</P>
					</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top"><asp:dropdownlist id="cbo_uso" runat="server" Width="100%" CssClass="combos_small"></asp:dropdownlist></TD>
					<TD vAlign="top" colSpan="2"><asp:radiobuttonlist id="rb_tipo" runat="server" Width="100%" AutoPostBack="True" RepeatDirection="Horizontal"
							CssClass="combos_small" RepeatColumns="3">
							<asp:ListItem Value="0" Selected="True">F&#237;sica</asp:ListItem>
							<asp:ListItem Value="1">Act. Empresarial</asp:ListItem>
							<asp:ListItem Value="2">Moral</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio">Precio de la Unidad :</TD>
					<TD><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_precio" runat="server"
							Width="50%" CssClass="Texto_Cantidad" MaxLength="8"></asp:textbox></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
					<TD align="right"><asp:imagebutton id="cmd_continuar" tabIndex="12" runat="server" ImageUrl="..\Imagenes\botones\continuar.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD align="right" width="25%"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
