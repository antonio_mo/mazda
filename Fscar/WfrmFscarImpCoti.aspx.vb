Imports Emisor_Conauto
Imports CN_Negocios

Partial Class WfrmFscarImpCoti
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    'EHG - Cambio de DLL
    Private ccliente As New CnCotizador
    Private arrTotal(0) As String
    Private arrPol(0) As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                Dim dt As DataTable
                If Request.QueryString("cveporenganche") Is Nothing Then
                    ViewState("cveporenganche") = 0
                Else
                    ViewState("cveporenganche") = Request.QueryString("cveporenganche")
                End If
                If Not Request.QueryString("ban") Is Nothing Then
                    Session("ban") = Request.QueryString("ban")
                Else
                    Session("ban") = 0
                End If

                limpia()
                lbl_fecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                'lbl_version.Text = "Fecha de Impresi�n  " & ccliente.carga_version(Session("version"), IIf(Session("aseguradora") Is Nothing, 0, Session("aseguradora")))

                lbl_version.Text = "Fecha de Impresi�n  " & Now.Day & "/" & Now.Month & "/" & Now.Year

                'lbl_distribuidor.Text = Session("nombre")
                lbl_distribuidor.Text = Session("NombreDistribuidor")
                carga_datos_cliente()

                carga_datos_vehiculo()

                carga_grd(Session("idgrdcontrato"))

                'carga_costos()
                'lbl_contrato.Text = Session("contrato")
                'lbl_plazo.Text = Session("plazo")
                'lbl_seguro.Text = Session("precioEscrito")
                'lbl_poliza.Text = ccliente.descripcion_paquete(Session("paquete"))
                'If Session("tipopol") = "M" Then
                '    'lbl_danos.Text = "Multianual"
                '    'lbl_danos.Text = "Financiado"
                'Else
                '    'lbl_danos.Text = "Anual"
                '    'lbl_danos.Text = "Contado"
                'End If

                '''''''Session("idgrdcontrato")
                ''''''If Session("ban") = 1 Then
                ''''''    dt = ccliente.paquete_aseguradora(Session("subramo"), Session("aseguradora"))
                ''''''Else
                ''''''    'dt = ccliente.carga_reporte_poliza(Session("idpoliza"), Session("bid"), Session("cliente"), Session("idcontrato"), Session("aseguradora"))
                ''''''End If
                ''''''carga_datos_cob(dt)


                Dim dth As DataTable
                If Session("ban") = 1 Then
                    dth = ccliente.Carga_aseguradora_cobertura(Session("cotiza"), Session("bid"), Session("idgrdcontrato"), Session("version"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("EstatusV"))
                Else
                    dth = ccliente.carga_asegurador_coberturas_contrato(Session("idcontrato"), Session("cliente"), Session("bid"), Session("idgrdcontrato"))
                End If
                If dth.Rows.Count > 0 Then
                    If dth.Rows.Count > 1 Then
                        carga_datos_cob()
                    Else
                        carga_datos_cob_unico()
                        carga_tb()
                    End If
                End If

                If Session("ban") = 1 Then
                    lbl_cotizacion.Text = "<p align=""right"">Num. Cotizaci�n : " & Session("cotiza") & "</p>"
                    lbl_fscar.Text = "<p align=""right"">Clave Seguro Especial : " & ViewState("cvefscar") & "</p>"
                Else
                    lbl_cotizacion.Text = ""
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_tb()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim dth As DataTable
        Dim aseguradora As Integer
        tb_pago.Controls.Clear()

        'tb_pago.BorderWidth = Unit.Pixel(1)
        'tb_pago.GridLines = GridLines.Both

        Try
            tbrow = New TableRow
            tbcell = New TableCell
            tbcell.Text = ""
            tbcell.ColumnSpan = 4
            tbcell.Width = Unit.Percentage(100)
            tbrow.Controls.Add(tbcell)
            tbrow.CssClass = "negrita"
            tbrow.Height = Unit.Pixel(10)
            tb_pago.Controls.Add(tbrow)
            'tbrow.CssClass = "linea_abajo"

            If Session("ban") = 1 Then
                dth = ccliente.Carga_aseguradora_cobertura(Session("cotiza"), Session("bid"), Session("idgrdcontrato"), Session("version"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("EstatusV"))
            Else
                dth = ccliente.carga_asegurador_coberturas_contrato(Session("idcontrato"), Session("cliente"), Session("bid"), Session("idgrdcontrato"))
            End If
            If dth.Rows.Count > 0 Then
                If dth.Rows(i).IsNull("id_aseguradora") Then
                    aseguradora = 0
                Else
                    aseguradora = dth.Rows(i)("id_aseguradora")
                End If

                Dim dt As DataTable = ccliente.paquete_aseguradora_subramo(Session("subramo"), _
                    txt_estatus.Text, Session("paquete"), Session("programa"), Session("vehiculo"), _
                    Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
                If dt.Rows.Count > 0 Then
                    For i = 0 To dt.Rows.Count - 1
                        Dim tb1 As Table
                        Dim tbcell1 As TableCell
                        Dim tbrow1 As TableRow
                        Dim total As Double = 0

                        tbrow = New TableRow

                        tbcell = New TableCell
                        tb1 = New Table

                        'tb1.BorderWidth = Unit.Pixel(1)
                        'tb1.GridLines = GridLines.Both

                        For j = 0 To 1
                            tbrow1 = New TableRow
                            tbcell1 = New TableCell
                            If j = 0 Then
                                tbcell1.Text = "Seguro de Da�os : " & IIf(Session("tipopol") = "A", "Anual", "Multianual")
                            Else
                                'If Session("programa") = 9 Then
                                '    tbcell1.Text = "S.Contado"
                                'Else
                                '    tbcell1.Text = IIf(Session("seguro") = 0, "S.Contado", "S.Financiado")
                                'End If
                                tbcell1.Text = IIf(Session("seguro") = 0, "Seguro de Contado", "Seguro Financiado")
                            End If
                            tbcell1.VerticalAlign = VerticalAlign.Middle
                            tbcell1.HorizontalAlign = HorizontalAlign.Left
                            'tbcell1.CssClass = "dt_celda"
                            tbcell1.CssClass = "negrita"
                            tbrow1.Controls.Add(tbcell1)
                            tb1.Controls.Add(tbrow1)
                            tb1.Width = Unit.Percentage(100)
                        Next
                        tb1.CssClass = "dt_celda"
                        tbcell.Controls.Add(tb1)
                        tbcell.Width = Unit.Percentage(25)
                        tbcell.VerticalAlign = VerticalAlign.Top
                        tbrow.Controls.Add(tbcell)

                        tbcell = New TableCell
                        tb1 = New Table
                        If Not Session("MultArrPolisa") = "" Then
                            Dim k As Integer = 0
                            Dim arr() As String = Split(Session("MultArrPolisa"), "|")
                            For k = 0 To arr.Length - 1
                                If Not arr(k) Is Nothing Then
                                    Dim arrpoliza() As String = arr(k).Split("*")
                                    If arrpoliza(0) = dt.Rows(i)("descripcion_paquete") Then
                                        For j = 1 To 5
                                            If arrpoliza(j) <> "" Then
                                                If arrpoliza(j) > 0 Then
                                                    tbrow1 = New TableRow
                                                    tbcell1 = New TableCell
                                                    total = arrpoliza(j)
                                                    Select Case j
                                                        Case 1
                                                            tbcell1.Text = "1er. Pago :" & Format(total, "$ ##,##0.00")
                                                        Case 2
                                                            tbcell1.Text = "2do. Pago :" & Format(total, "$ ##,##0.00")
                                                        Case 3
                                                            tbcell1.Text = "3er. Pago :" & Format(total, "$ ##,##0.00")
                                                        Case 4
                                                            tbcell1.Text = "4to. Pago :" & Format(total, "$ ##,##0.00")
                                                        Case 5
                                                            tbcell1.Text = "5to. Pago :" & Format(total, "$ ##,##0.00")
                                                    End Select
                                                    'tbcell1.CssClass = "dt_celda"
                                                    tbcell1.CssClass = "negrita"
                                                    tbrow1.Controls.Add(tbcell1)
                                                    tb1.Controls.Add(tbrow1)
                                                    tb1.Width = Unit.Percentage(100)
                                                    total = 0
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                        tb1.CssClass = "dt_celda"
                        tbcell.Controls.Add(tb1)
                        tbcell.Width = Unit.Percentage(25)
                        tbcell.VerticalAlign = VerticalAlign.Top
                        tbrow.Controls.Add(tbcell)


                        Dim dtCDV As DataTable = ccliente.traer_cobertura_DV(Session("idgrdcontrato"), _
                            Session("bid"), dt.Rows(i)("subramo"), dt.Rows(i)("descripcion_paquete"))
                        If dtCDV.Rows.Count > 0 Then
                            Dim dtV As DataTable = ccliente.valida_vida_desempleo(Session("cotiza"), dt.Rows(i)("id_paquete"))
                            If dtV.Rows.Count > 0 Then
                                tbcell = New TableCell
                                tb1 = New Table
                                For j = 0 To 3
regresa:
                                    Select Case j
                                        Case 1
                                            If dtV.Rows(0)("bansegdesempleo") = 0 Then
                                                j = j + 1
                                                GoTo regresa
                                            Else
                                                j = 1
                                            End If
                                        Case 2
                                            If dtV.Rows(0)("bansegvida") = 0 Then
                                                j = j + 1
                                                GoTo regresa
                                            Else
                                                j = 2
                                            End If
                                    End Select
                                    tbrow1 = New TableRow
                                    tbcell1 = New TableCell
                                    Select Case j
                                        Case 0
                                            'tbcell1.Text = "Tipo de Poliza : " & IIf(dt.Rows(i).IsNull("descripcion_paquete"), "", "<b>" & dt.Rows(i)("descripcion_paquete") & "</B>")
                                            tbcell1.Text = IIf(dt.Rows(i).IsNull("descripcion_paquete"), "", "<b>" & dt.Rows(i)("descripcion_paquete") & "</B>")
                                        Case 1
                                            tbcell1.Text = "Seguro de Desempleo :" & IIf(dtCDV.Rows(0).IsNull("desempleo"), "", Format(dtCDV.Rows(0)("desempleo"), "$ ##,##0.00"))
                                        Case 2
                                            tbcell1.Text = "Seguro de Vida : " & IIf(dtCDV.Rows(0).IsNull("vida"), "", Format(dtCDV.Rows(0)("vida"), "$ ##,##0.00"))
                                        Case 3
                                            tbcell1.Text = "Plazo de Seguro : " & IIf(Session("tipopol") = "A", "12", lbl_plazo.Text)
                                    End Select
                                    'tbcell1.CssClass = "dt_celda"
                                    If j = 0 Then
                                        tbcell1.CssClass = "header"
                                    Else
                                        tbcell1.CssClass = "negrita"
                                    End If
                                    tbrow1.Controls.Add(tbcell1)
                                    tb1.Controls.Add(tbrow1)
                                    tb1.Width = Unit.Percentage(100)
                                Next
                                tb1.CssClass = "dt_celda"
                                tbcell.Controls.Add(tb1)
                                tbcell.Width = Unit.Percentage(25)
                                tbcell.VerticalAlign = VerticalAlign.Top
                                tbrow.Controls.Add(tbcell)
                            End If
                        Else
                            tbcell = New TableCell
                            tbcell.Text = ""
                            tbcell.Width = Unit.Percentage(25)
                            tbcell.VerticalAlign = VerticalAlign.Top
                            tbrow.Controls.Add(tbcell)
                        End If

                        tbcell = New TableCell
                        tbcell.Text = ""
                        tbcell.Width = Unit.Percentage(25)
                        tbcell.CssClass = "negrita"
                        tbcell.VerticalAlign = VerticalAlign.Top
                        tbrow.Controls.Add(tbcell)

                        tb_pago.Controls.Add(tbrow)

                        tbrow = New TableRow
                        tbcell = New TableCell
                        tbcell.Text = ""
                        tbcell.ColumnSpan = 4
                        tbcell.Width = Unit.Percentage(100)
                        tbrow.Controls.Add(tbcell)
                        tbrow.CssClass = "linea_abajo"
                        tbrow.Height = Unit.Pixel(10)
                        tb_pago.Controls.Add(tbrow)
                    Next
                End If
            End If

            tbrow = New TableRow
            tbcell = New TableCell
            tbcell.Text = ""
            tbcell.ColumnSpan = 4
            tbcell.Width = Unit.Percentage(100)
            tbrow.Controls.Add(tbcell)
            tbrow.CssClass = "negrita"
            tbrow.Height = Unit.Pixel(10)
            tb_pago.Controls.Add(tbrow)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_grd(ByVal intcontrato As Integer)
        Dim dt As DataTable
        If Session("ban") = 1 Then
            'If Session("programa") = 4 Then
            dt = ccliente.carga_cobertura_bid_sincob(intcontrato, Session("bid"), Session("EstatusV"), Session("programa"), Session("vehiculo"))
            'dt = ccliente.carga_cobertura_bid(intcontrato, Session("bid"), Session("EstatusV"), Session("subramo"))
        Else
            dt = ccliente.carga_cobertura_bid_cliente(intcontrato, Session("bid"), Session("aseguradora"), Session("paquete"), Session("EstatusV"), Session("programa"), Session("vehiculo"))
        End If
        If dt.Rows.Count > 0 Then
            grd_cobertura.DataSource = dt
            grd_cobertura.DataBind()
            grd_cobertura.Visible = True
        Else
            grd_cobertura.Visible = False
        End If
    End Sub

    Public Sub limpia()
        lbl_distribuidor.Text = ""
        lbl_paterno.Text = ""
        lbl_materno.Text = ""
        lbl_nombre.Text = ""
        lbl_razon.Text = ""
        'lbl_rfc.Text = ""
        lbl_tel.Text = ""
        lbl_fecha.Text = ""
        lbl_plazo.Text = ""
        lbl_seguro.Text = ""
        lbl_marca.Text = ""
        lbl_modelo.Text = ""
        lbl_catalogo.Text = ""
        lbl_descripcion.Text = ""
        lbl_anio.Text = ""
        lbl_seguro.Text = ""
        'lbl_poliza.Text = ""
        lbl_version.Text = ""
        lbl_ley.Text = ""
        lbl_cotizacion.Text = ""
    End Sub

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Public Sub carga_datos_cob_unico()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 5
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        Dim celdas As Integer
        Dim deduc As Double = 0

        'tb.BorderWidth = Unit.Pixel(1)
        'tb.GridLines = GridLines.Both

        tb.Controls.Clear()
        Try
            If Session("ban") = 1 Then
                dth = ccliente.Carga_aseguradora_cobertura(Session("cotiza"), Session("bid"), Session("idgrdcontrato"), Session("version"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("EstatusV"))
            Else
                dth = ccliente.carga_asegurador_coberturas_contrato(Session("idcontrato"), Session("cliente"), Session("bid"), Session("idgrdcontrato"))
            End If
            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If Session("EstatusV") = "N" Then
                    objcell.Text = "COBERTURAS DE CONTADO"
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                celdas = ccliente.cuenta_paquete_aseguradora_subramo(Session("idgrdcontrato"), Session("bid"), Session("subramo"), _
                                txt_estatus.Text, Session("paquete"), Session("programa"))

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    'objcell.Width = Unit.Percentage(celdas * 25)
                    objcell.Width = Unit.Percentage(celdas * 18.75)
                    objcell.ColumnSpan = celdas
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                For i = contador To totalceldas - 1 - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(25)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)
            End If

            objrow = New TableRow
            objcell = New TableCell
            objcell.Text = "COBERTURA"
            objcell.CssClass = "Combos_small_celda"
            objcell.VerticalAlign = VerticalAlign.Middle
            objcell.HorizontalAlign = HorizontalAlign.Center
            objrow.Controls.Add(objcell)

            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo(Session("subramo"), txt_estatus.Text, _
                    Session("paquete"), Session("programa"), Session("vehiculo"), _
                    Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                For j = 0 To dt.Rows.Count - 1
                    objcell = New TableCell
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dt.Rows(j)("descripcion_paquete")
                    End If
                    objcell.CssClass = "Combos_small_celda"
                    objcell.ColumnSpan = dth.Rows.Count
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                Next
                For i = 0 To totalceldas - dt.Rows.Count - celdas
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    'objcell.ColumnSpan = totalceldas - (dt.Rows.Count + 1)
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)

                If dt.Rows.Count > 0 Then
                    Dim dtec As DataTable
                    If Session("ban") = 1 Then
                        dtec = ccliente.encabezado_coberturas(txt_estatus.Text, 0, Session("version"), Session("cotiza"), Session("bid"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("PaqSelCon"))
                    Else
                        txt_idpaquete.Text = dt.Rows(j - celdas)("id_paquete")
                        dtec = ccliente.encabezado_coberturas_final(txt_estatus.Text, Session("version"), Session("bid"), Session("idgrdcontrato"), arraseg(0), txt_idpaquete.Text)
                    End If
                    If dtec.Rows.Count > 0 Then
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                            Else
                                strdescrcobertura = dtec.Rows(i)("cobertura")
                            End If
                            objcell.Text = strdescrcobertura
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)

                            Dim dtc As DataTable
                            For j = 0 To dt.Rows.Count - 1
                                If Session("ban") = 1 Then
                                    'dtec.Rows(i)("id_limresp")
                                    dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                                        Session("version"), dt.Rows(j)("id_paquete"), _
                                        Session("bid"), Session("cotiza"), _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                Else
                                    dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                                        Session("version"), dt.Rows(j)("id_paquete"), _
                                        Session("bid"), 0, _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                End If
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                dbdedu = dbdedu * 100
                                                If (strdescrcobertura = "Danos Materiales" Or strdescrcobertura = "Da�os Materiales") And _
                                                    ccliente.determina_banderas_X_programa(Session("programa"), arraseg(k), 0) = 1 Then
                                                    deduc = ccliente.determinando_deducpl(arraseg(k), dt.Rows(j)("id_paquete"), _
                                                        Session("subramo"), Session("programa"), Session("EstatusV"), _
                                                        Session("ban"), Session("anio"), _
                                                        Session("vehiculo"), Session("resultado")(5), Session("tipopol"))
                                                    If Session("tipopol") = "M" Or Session("tipopol") = "F" Then
                                                        'objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %, " & deduc & "% Deducible"
                                                        Dim strdescripcion As String
                                                        Select Case dtc.Rows(0)("descripcion_tipo")
                                                            Case "Auto Nuevo"
                                                                strdescripcion = "6"
                                                            Case "Valor Comercial"
                                                                strdescripcion = "7"
                                                            Case "Valor Factura"
                                                                strdescripcion = "7"
                                                        End Select
                                                        If dbdedu > 0 Then
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %, " & strdescripcion & "% Deducible"
                                                        Else
                                                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & " 0%, " & strdescripcion & "% Deducible"
                                                        End If
                                                    Else
                                                        objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %, " & Format(deduc, "$##,##0.##") & " Deducible"
                                                    End If
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                                End If
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.##")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.##")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                            Next
                            tb.Controls.Add(objrow)
                        Next
                        objcell = New TableCell
                        objcell.Text = ""
                        objcell.CssClass = "negrita"
                        'objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                        objrow.Controls.Add(objcell)
                    End If
                End If

                'Dim dtec As DataTable
                'If Session("ban") = 1 Then
                '    dtec = ccliente.encabezado_coberturas(txt_estatus.Text, dt.Rows(j)("id_paquete"), Session("version"), Session("cotizacionP"), Session("bid"))
                'Else
                '    txt_idpaquete.Text = dt.Rows(j)("id_paquete")
                '    dtec = ccliente.encabezado_coberturas_final(txt_estatus.Text, Session("version"), Session("bid"), Session("idgrdcontrato"), arraseg(0), dt.Rows(j)("id_paquete"))
                'End If
                'If dtec.Rows.Count > 0 Then
                '    For i = 0 To dtec.Rows.Count - 1
                '        objrow = New TableRow
                '        objcell = New TableCell
                '        If dtec.Rows(i).IsNull("cobertura") Then
                '            strdescrcobertura = ""
                '        Else
                '            strdescrcobertura = dtec.Rows(i)("cobertura")
                '        End If
                '        objcell.Text = strdescrcobertura
                '        objcell.CssClass = "dt_celda"
                '        objcell.VerticalAlign = VerticalAlign.Middle
                '        objcell.HorizontalAlign = HorizontalAlign.Left
                '        objrow.Controls.Add(objcell)

                '        Dim dtc As DataTable
                '        For k = 0 To arraseg.Length - 1
                '            If Session("ban") = 1 Then
                '                'dtec.Rows(i)("id_limresp")
                '                dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                '                    Session("version"), dt.Rows(j)("id_paquete"), _
                '                    Session("bid"), Session("cotizacionP"), _
                '                    arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"))
                '            Else
                '                dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                '                    Session("version"), dt.Rows(j)("id_paquete"), _
                '                    Session("bid"), 0, _
                '                    arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"))
                '            End If
                '            objcell = New TableCell
                '            If dtc.Rows.Count > 0 Then
                '                If dtc.Rows(0).IsNull("descripcion_tipo") Then
                '                    objcell.Text = "-"
                '                Else
                '                    If Not dtc.Rows(0).IsNull("deducible") Then
                '                        If IsNumeric(dtc.Rows(0)("deducible")) Then
                '                            Dim dbdedu As Double = dtc.Rows(0)("deducible")
                '                            dbdedu = dbdedu * 100
                '                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                '                        Else
                '                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                '                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                '                                objcell.Text = Format(dbvalor, "$##,##0.##")
                '                            Else
                '                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                '                            End If
                '                        End If
                '                    Else
                '                        If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                '                            dbvalor = dtc.Rows(0)("descripcion_tipo")
                '                            objcell.Text = Format(dbvalor, "$##,##0.##")
                '                        Else
                '                            objcell.Text = dtc.Rows(0)("descripcion_tipo")
                '                        End If
                '                    End If
                '                End If
                '            Else
                '                objcell.Text = "-"
                '            End If
                '            objcell.CssClass = "dt_celda"
                '            objcell.VerticalAlign = VerticalAlign.Middle
                '            objcell.HorizontalAlign = HorizontalAlign.Center
                '            objrow.Controls.Add(objcell)
                '        Next
                '        objcell = New TableCell
                '        objcell.Text = ""
                '        objcell.CssClass = "negrita"
                '        objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                '        objrow.Controls.Add(objcell)
                '        tb.Controls.Add(objrow)
                '    Next
                'End If
                'Next
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'emmanuell
    Public Sub carga_datos_cob()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0
        Dim dbvalor As Double = 0
        Dim contador As Integer = 0
        Dim dth As DataTable
        Dim objrow As TableRow
        Dim objcell As TableCell
        Dim totalceldas As Integer = 6
        Dim arraseg() As String
        Dim strdescrcobertura As String = ""
        tb.Controls.Clear()
        Try
            If Session("ban") = 1 Then
                dth = ccliente.Carga_aseguradora_cobertura(Session("cotiza"), Session("bid"), Session("idgrdcontrato"), Session("version"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("EstatusV"))
            Else
                dth = ccliente.carga_asegurador_coberturas_contrato(Session("idcontrato"), Session("cliente"), Session("bid"), Session("idgrdcontrato"))
            End If
            If dth.Rows.Count > 0 Then

                objrow = New TableRow
                objcell = New TableCell
                If Session("EstatusV") = "N" Then
                    objcell.Text = "COBERTURAS DE CONTADO"
                Else
                    objcell.Text = "COBERTURAS DE SEMINUEVOS"
                End If
                objcell.ColumnSpan = totalceldas
                objcell.CssClass = "Interiro_tabla_centro"
                objrow.Controls.Add(objcell)
                tb.Controls.Add(objrow)

                objrow = New TableRow
                objcell = New TableCell
                objcell.Text = ""
                objcell.Width = Unit.Percentage(25)
                objcell.CssClass = "negrita"
                objrow.Controls.Add(objcell)

                ReDim arraseg(dth.Rows.Count - 1)
                For i = 0 To dth.Rows.Count - 1
                    objcell = New TableCell
                    arraseg(i) = dth.Rows(i)("id_aseguradora")
                    If dth.Rows(i).IsNull("descripcion_aseguradora") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dth.Rows(i)("descripcion_aseguradora")
                    End If
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "Interiro_tabla_centro"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)
                    contador = contador + 1
                Next
                contador = contador + 1
                For i = contador To totalceldas - 1
                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.Width = Unit.Percentage(15)
                    objcell.CssClass = "negrita"
                    objrow.Controls.Add(objcell)
                Next
                tb.Controls.Add(objrow)
            End If

            'objrow = New TableRow

            'objcell = New TableCell
            'objcell.Text = "COBERTURA"
            'objcell.CssClass = "Combos_small_celda"
            'objcell.VerticalAlign = VerticalAlign.Middle
            'objcell.HorizontalAlign = HorizontalAlign.Center
            'objrow.Controls.Add(objcell)
            Dim dt As DataTable = ccliente.paquete_aseguradora_subramo(Session("subramo"), txt_estatus.Text, _
                Session("paquete"), Session("programa"), Session("vehiculo"), _
                Session("idgrdcontrato"), Session("bid"), 1, Session("PaqSelCon"))
            If dt.Rows.Count > 0 Then
                For j = 0 To dt.Rows.Count - 1
                    objrow = New TableRow
                    objcell = New TableCell
                    objcell.Text = "COBERTURA"
                    objcell.CssClass = "Combos_small_celda"
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)

                    objcell = New TableCell
                    If dt.Rows(j).IsNull("descripcion_paquete") Then
                        objcell.Text = ""
                    Else
                        objcell.Text = dt.Rows(j)("descripcion_paquete")
                    End If
                    objcell.CssClass = "Combos_small_celda"
                    objcell.ColumnSpan = dth.Rows.Count
                    objcell.VerticalAlign = VerticalAlign.Middle
                    objcell.HorizontalAlign = HorizontalAlign.Center
                    objrow.Controls.Add(objcell)

                    objcell = New TableCell
                    objcell.Text = ""
                    objcell.CssClass = "negrita"
                    objcell.ColumnSpan = totalceldas - (dth.Rows.Count + 1)
                    objrow.Controls.Add(objcell)

                    tb.Controls.Add(objrow)

                    Dim dtec As DataTable
                    If Session("ban") = 1 Then
                        dtec = ccliente.encabezado_coberturas(txt_estatus.Text, dt.Rows(j)("id_paquete"), Session("version"), Session("cotiza"), Session("bid"), Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")), Session("PaqSelCon"))
                    Else
                        txt_idpaquete.Text = dt.Rows(j)("id_paquete")
                        dtec = ccliente.encabezado_coberturas_final(txt_estatus.Text, Session("version"), Session("bid"), Session("idgrdcontrato"), arraseg(0), dt.Rows(j)("id_paquete"))
                    End If
                    If dtec.Rows.Count > 0 Then
                        For i = 0 To dtec.Rows.Count - 1
                            objrow = New TableRow
                            objcell = New TableCell
                            If dtec.Rows(i).IsNull("cobertura") Then
                                strdescrcobertura = ""
                            Else
                                strdescrcobertura = dtec.Rows(i)("cobertura")
                            End If
                            objcell.Text = strdescrcobertura
                            objcell.CssClass = "dt_celda"
                            objcell.VerticalAlign = VerticalAlign.Middle
                            objcell.HorizontalAlign = HorizontalAlign.Left
                            objrow.Controls.Add(objcell)

                            Dim dtc As DataTable
                            For k = 0 To arraseg.Length - 1
                                If Session("ban") = 1 Then
                                    'dtec.Rows(i)("id_limresp")
                                    dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                                        Session("version"), dt.Rows(j)("id_paquete"), _
                                        Session("bid"), Session("cotiza"), _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                Else
                                    dtc = ccliente.carga_coberturas(txt_estatus.Text, _
                                        Session("version"), dt.Rows(j)("id_paquete"), _
                                        Session("bid"), 0, _
                                        arraseg(k), strdescrcobertura, dt.Rows(j)("descripcion_paquete"), _
                                        Session("programa"), IIf(Session("fincon") Is Nothing, "", Session("fincon")))
                                End If
                                objcell = New TableCell
                                If dtc.Rows.Count > 0 Then
                                    If dtc.Rows(0).IsNull("descripcion_tipo") Then
                                        objcell.Text = "-"
                                    Else
                                        If Not dtc.Rows(0).IsNull("deducible") Then
                                            If IsNumeric(dtc.Rows(0)("deducible")) Then
                                                Dim dbdedu As Double = dtc.Rows(0)("deducible")
                                                dbdedu = dbdedu * 100
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
                                            Else
                                                If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                    dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                    objcell.Text = Format(dbvalor, "$##,##0.##")
                                                Else
                                                    objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                                End If
                                            End If
                                        Else
                                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
                                                dbvalor = dtc.Rows(0)("descripcion_tipo")
                                                objcell.Text = Format(dbvalor, "$##,##0.##")
                                            Else
                                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
                                            End If
                                        End If
                                    End If
                                Else
                                    objcell.Text = "-"
                                End If
                                objcell.CssClass = "dt_celda"
                                objcell.VerticalAlign = VerticalAlign.Middle
                                objcell.HorizontalAlign = HorizontalAlign.Center
                                objrow.Controls.Add(objcell)
                            Next
                            objcell = New TableCell
                            objcell.Text = ""
                            objcell.CssClass = "negrita"
                            objcell.ColumnSpan = totalceldas - (arraseg.Length + 1)
                            objrow.Controls.Add(objcell)
                            tb.Controls.Add(objrow)
                        Next
                    End If
                Next
            End If
            'contador = 0
            'For i = 0 To arrAseg.Length - 1
            '    Dim dt As DataTable = ccliente.paquete_aseguradora_subramo(Session("subramo"), dth.Rows(i)("id_aseguradora"), txt_estatus.Text)
            '    If dt.Rows.Count > 0 Then
            '        ReDim arrpaquete(dt.Rows.Count - 1)
            '        For j = 0 To dt.Rows.Count - 1
            '            arrpaquete(j) = dt.Rows(j)("id_paquete")
            '            objcell = New TableCell
            '            If dt.Rows(j).IsNull("descripcion_paquete") Then
            '                objcell.Text = ""
            '            Else
            '                objcell.Text = dt.Rows(j)("descripcion_paquete")
            '            End If
            '            objcell.CssClass = "Combos_small_celda"
            '            objcell.VerticalAlign = VerticalAlign.Middle
            '            objcell.HorizontalAlign = HorizontalAlign.Center
            '            objrow.Controls.Add(objcell)
            '            contador = contador + 1
            '        Next
            '    End If
            '    For j = contador To 4
            '        objcell = New TableCell
            '        objcell.Text = ""
            '        objcell.CssClass = "negrita"
            '        objrow.Controls.Add(objcell)
            '    Next
            '    tb.Controls.Add(objrow)



            '    Dim dtec As DataTable
            '    If Session("ban") = 1 Then
            '        dtec = ccliente.encabezado_coberturas(txt_estatus.Text, arrpaquete(0), Session("version"), Session("cotizacionP"), Session("bid"))
            '    Else
            '        dtec = ccliente.encabezado_coberturas_final(txt_estatus.Text, txt_insco.Text, Session("version"), Session("bid"), Session("idgrdcontrato"))
            '    End If
            '    If dtec.Rows.Count > 0 Then
            '        For j = 0 To dtec.Rows.Count - 1
            '            objrow = New TableRow
            '            objcell = New TableCell
            '            If dtec.Rows(j).IsNull("cobertura") Then
            '                objcell.Text = ""
            '            Else
            '                objcell.Text = dtec.Rows(j)("cobertura")
            '            End If
            '            objcell.CssClass = "Combos_small_celda"
            '            objcell.VerticalAlign = VerticalAlign.Middle
            '            objcell.HorizontalAlign = HorizontalAlign.Left
            '            objrow.Controls.Add(objcell)



            '            objcell = New TableCell
            '            Dim dtc As DataTable = ccliente.carga_coberturas(dth.Rows(i)("id_aseguradora"), txt_estatus.Text, _
            '                    Session("version"), arrpaquete(0), dtec.Rows(i)("id_limresp"), _
            '                    Session("bid"), Session("cotizacionP"))
            '            If dtc.Rows.Count > 0 Then
            '                If dtc.Rows(0).IsNull("descripcion_tipo") Then
            '                    objcell.Text = ""
            '                Else
            '                    If Not dtc.Rows(0).IsNull("deducible") Then
            '                        If IsNumeric(dtc.Rows(0)("deducible")) Then
            '                            Dim dbdedu As Double = dtc.Rows(0)("deducible")
            '                            dbdedu = dbdedu * 100
            '                            objcell.Text = dtc.Rows(0)("descripcion_tipo") & "  " & dbdedu & " %"
            '                        Else
            '                            If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
            '                                dbvalor = dtc.Rows(0)("descripcion_tipo")
            '                                objcell.Text = Format(dbvalor, "$##,##0.##")
            '                            Else
            '                                objcell.Text = dtc.Rows(0)("descripcion_tipo")
            '                            End If
            '                        End If
            '                    Else
            '                        If IsNumeric(dtc.Rows(0)("descripcion_tipo")) Then
            '                            dbvalor = dtc.Rows(0)("descripcion_tipo")
            '                            objcell.Text = Format(dbvalor, "$##,##0.##")
            '                        Else
            '                            objcell.Text = dtc.Rows(0)("descripcion_tipo")
            '                        End If
            '                    End If
            '                End If
            '            Else
            '                objcell.Text = ""
            '            End If
            '            objcell.CssClass = "Combos_small_celda"
            '            objcell.VerticalAlign = VerticalAlign.Middle
            '            objcell.HorizontalAlign = HorizontalAlign.Center
            '            objrow.Controls.Add(objcell)

            '            For k = contador To 4
            '                objcell = New TableCell
            '                objcell.Text = ""
            '                objcell.CssClass = "negrita"
            '                objrow.Controls.Add(objcell)
            '            Next
            '            tb.Controls.Add(objrow)
            '        Next
            '    End If
            'Next
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_cliente()
        Dim dt As DataTable
        If Session("ban") = 1 Then
            dt = ccliente.carga_reporte_poliza_coti(Session("cotiza"), Session("bid"), Session("cliente"), Session("idgrdcontrato"), Session("programa"), Session("anio"), IIf(Session("fincon") Is Nothing, "", Session("fincon"))) ', Session("EstatusV"), Session("anio"))
        Else
            dt = ccliente.carga_cliente_impresion(Session("cliente"), Session("bid"))
        End If
        Try
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("paterno") Then
                    viewstate("cvefscar") = ""
                Else
                    viewstate("cvefscar") = dt.Rows(0)("clave_fscar")
                End If
                If dt.Rows(0).IsNull("paterno") Then
                    lbl_paterno.Text = ""
                Else
                    lbl_paterno.Text = dt.Rows(0)("paterno")
                End If
                If dt.Rows(0).IsNull("materno") Then
                    lbl_materno.Text = ""
                Else
                    lbl_materno.Text = dt.Rows(0)("materno")
                End If
                If dt.Rows(0).IsNull("nombre") Then
                    lbl_nombre.Text = ""
                Else
                    lbl_nombre.Text = dt.Rows(0)("nombre")
                End If
                If dt.Rows(0).IsNull("rs") Then
                    lbl_razon.Text = ""
                Else
                    lbl_razon.Text = dt.Rows(0)("rs")
                End If
                'If dt.Rows(0).IsNull("rfc") Then
                '    lbl_rfc.Text = ""
                'Else
                '    lbl_rfc.Text = dt.Rows(0)("rfc")
                'End If
                If dt.Rows(0).IsNull("tel") Then
                    lbl_tel.Text = ""
                Else
                    lbl_tel.Text = dt.Rows(0)("tel")
                End If
                If Session("ban") = 1 Then
                    If dt.Rows(0).IsNull("plazo") Then
                        lbl_plazo.Text = ""
                    Else
                        lbl_plazo.Text = dt.Rows(0)("plazo")
                    End If
                    If dt.Rows(0).IsNull("precio") Then
                        lbl_seguro.Text = ""
                    Else
                        lbl_seguro.Text = Format(dt.Rows(0)("precio"), "$##,##0.##")
                    End If
                Else
                    lbl_plazo.Text = Session("plazo")
                    lbl_seguro.Text = Session("precioescrito")
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_datos_vehiculo()
        Dim dt As DataTable
        If Session("ban") = 1 Then
            'If Session("programa") = 4 Then
            dt = ccliente.carga_reporte_poliza_coti(Session("cotiza"), Session("bid"), Session("cliente"), Session("idgrdcontrato"), Session("programa"), Session("anio"), IIf(Session("fincon") Is Nothing, "", Session("fincon"))) ', Session("EstatusV"), Session("anio"))
            'dt = ccliente.carga_vehiculo_homologado(Session("vehiculo"), Session("programa"), Session("anio"), Session("EstatusV"))
        Else
            dt = ccliente.carga_vehiculo_general(Session("vehiculo"), Session("anio"), Session("aseguradora"), Session("programa"))
        End If
        Try
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).IsNull("marca") Then
                    lbl_marca.Text = ""
                Else
                    lbl_marca.Text = dt.Rows(0)("marca")
                End If
                If dt.Rows(0).IsNull("modelo") Then
                    lbl_modelo.Text = ""
                Else
                    lbl_modelo.Text = dt.Rows(0)("modelo")
                End If
                If dt.Rows(0).IsNull("catalogo") Then
                    lbl_catalogo.Text = ""
                Else
                    lbl_catalogo.Text = dt.Rows(0)("catalogo")
                End If
                If dt.Rows(0).IsNull("descripcion_homologado") Then
                    lbl_descripcion.Text = ""
                Else
                    lbl_descripcion.Text = dt.Rows(0)("descripcion_homologado")
                End If
                If dt.Rows(0).IsNull("anio") Then
                    lbl_anio.Text = ""
                Else
                    lbl_anio.Text = dt.Rows(0)("anio")
                End If
                If dt.Rows(0).IsNull("taza") Then
                    lbl_tasa.Text = ""
                Else
                    If dt.Rows(0)("taza") = 1 Then
                        lbl_tasa.Text = "Si"
                    Else
                        lbl_tasa.Text = "No"
                    End If
                End If
                If dt.Rows(0).IsNull("enganche") Then
                    lbl_enganche.Text = ""
                Else
                    lbl_enganche.Text = Format(dt.Rows(0)("enganche"), "$##,##0.##")
                End If
                If Session("ban") = 1 Then
                    'If dt.Rows(0).IsNull("insco") Then
                    '    txt_insco.Text = ""
                    'Else
                    '    txt_insco.Text = dt.Rows(0)("insco")
                    'End If
                    If dt.Rows(0).IsNull("estatus") Then
                        txt_estatus.Text = ""
                    Else
                        txt_estatus.Text = dt.Rows(0)("estatus")
                    End If
                Else
                    If dt.Rows(0).IsNull("insco_vehiculo") Then
                        txt_insco.Text = ""
                    Else
                        txt_insco.Text = dt.Rows(0)("insco_vehiculo")
                    End If
                    txt_estatus.Text = ccliente.statusCotiza
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'emmanuell
    'Public Sub carga_costos()
    '    Dim total As Double = 0
    '    Dim i As Integer
    '    Dim objrow As TableRow
    '    Dim objcell As TableCell
    '    For i = 0 To 4
    '        If Not Session("ArrPoliza")(i) Is Nothing Then
    '            If Session("ArrPoliza")(i) > 0 Then
    '                objrow = New TableRow
    '                objcell = New TableCell
    '                total = Session("ArrPoliza")(i)
    '                objcell.Text = Format(total, "$##,##0.00")
    '                'objcell.Text = Session("ArrPoliza")(i)
    '                objcell.CssClass = "obligatorio_negro"
    '                objcell.Width = Unit.Percentage(100)
    '                objrow.Controls.Add(objcell)
    '                'tb_costo.Controls.Add(objrow)
    '                total = 0
    '            End If
    '        End If
    '    Next
    'End Sub

    Private Sub grd_cobertura_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_cobertura.ItemDataBound
        Dim dt As DataTable
        Dim Dbvalor As Double = 0
        If e.Item.ItemType = ListItemType.Header Then
            Dim cuenta As Integer = 7
            Dim i As Integer = 0
            If Session("ban") = 1 Then
                dt = ccliente.carga_cobertura_bid_aseguradora(Session("idgrdcontrato"), Session("bid"))
            Else
                dt = ccliente.carga_cobertura_bid_aseguradora_imp(Session("idgrdcontrato"), Session("bid"), Session("aseguradora"))
            End If
            If dt.Rows.Count > 0 Then
                ReDim arrTotal(dt.Rows.Count - 1)
                ReDim arrPol(dt.Rows.Count - 1)
                For i = 0 To dt.Rows.Count - 1
                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                    e.Item.Cells(cuenta).Text = dt.Rows(i)("descripcion_aseguradora")
                    arrPol(i) = dt.Rows(i)("id_aseguradora")
                    grd_cobertura.Columns(cuenta).Visible = True
                    cuenta = cuenta + 1
                Next
            End If
        End If

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim recorrepoliza As Integer = 0
            Dim validarecorrido As Integer = 0
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim cuenta As Integer = 7
            If IsNumeric(e.Item.Cells(2).Text) Then
                If e.Item.Cells(2).Text = 0 Then 'coberturas
                    If Session("ban") = 1 Then
                        dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), e.Item.Cells(3).Text, 0, "")
                    Else
                        dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), e.Item.Cells(3).Text, arrPol(0), "")
                    End If
                    'If Session("ban") = 1 Then
                    '    dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(Session("idgrdcontrato"), Session("bid"), Session("EstatusV"), Session("subramo"), e.Item.Cells(3).Text)
                    'Else
                    '    dt = ccliente.carga_cobertura_bid_aseguradora_total_cob_contrato(Session("idgrdcontrato"), Session("bid"), e.Item.Cells(2).Text, Session("aseguradora"))
                    'End If
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            If arrPol(recorrepoliza) = dt.Rows(i)("id_aseguradora") Then

                                validarecorrido = 0

                                Dim DbRvalor As Double = 0
                                arrTotal(i) = dt.Rows(i)("total_cob")
                                Dbvalor = dt.Rows(i)("total_cob")
                                If Session("ban") > 1 Then
                                    Dim dtR As DataTable = ccliente.restale_carga_cobertura_total_cob_aseguradora_ramo(Session("idgrdcontrato"), Session("bid"), e.Item.Cells(2).Text, dt.Rows(i)("id_aseguradora"), dt.Rows(i)("id_paquete"))
                                    If dtR.Rows.Count > 0 Then
                                        For j = 0 To dtR.Rows.Count - 1
                                            If dtR.Rows.Count > 0 Then
                                                DbRvalor = dtR.Rows(j)("total_cob")
                                            End If
                                            If Session("banderafecha") = 1 Then
                                                Dbvalor = Dbvalor - (DbRvalor * 1.16)
                                            Else
                                                Dbvalor = Dbvalor - (DbRvalor * 1.16)
                                            End If
                                            'e.Item.Cells(cuenta).Attributes.Add("align", "left")
                                            'e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                                        Next
                                    End If
                                End If
                                'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                e.Item.Cells(cuenta).CssClass = "negrita"
                                e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                            Else
                                validarecorrido = 1
                                e.Item.Cells(cuenta).Text = "-"
                                e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                            End If

                            recorrepoliza = recorrepoliza + 1
                            cuenta = cuenta + 1

                            If validarecorrido = 1 Then
                                i = i - 1
                                validarecorrido = 0
                            End If
                        Next
                    End If
                Else
                    'paquetes especiales
                    dt = ccliente.carga_cobertura_total_cob_aseguradora_ramo(Session("idgrdcontrato"), _
                            Session("bid"), e.Item.Cells(2).Text, Session("ArrPolValida")(j))
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            For j = 0 To arrTotal.Length - 1
                                If Session("ban") = 1 Then
                                    If Session("ArrPolValida")(j) = dt.Rows(i)("id_aseguradora") _
                                        And dt.Rows(i)("total_cob") > 0 _
                                        And e.Item.Cells(12).Text = dt.Rows(i)("cap") _
                                        And e.Item.Cells(13).Text = dt.Rows(i)("sumaaseg_cob") Then
                                        Dbvalor = 0
                                        Dbvalor = dt.Rows(i)("total_cob")
                                        If Session("banderafecha") = 1 Then
                                            Dbvalor = Dbvalor * 1.16
                                        Else
                                            Dbvalor = Dbvalor * 1.16
                                        End If
                                        'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                        e.Item.Cells(cuenta).CssClass = "negrita"
                                        e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                        e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                                        Exit For
                                    Else
                                        cuenta = cuenta + 1
                                    End If
                                Else
                                    If arrPol(0) = dt.Rows(i)("id_aseguradora") _
                                    And dt.Rows(i)("total_cob") > 0 _
                                    And e.Item.Cells(12).Text = dt.Rows(i)("cap") _
                                    And e.Item.Cells(13).Text = dt.Rows(i)("sumaaseg_cob") Then
                                        Dbvalor = 0
                                        Dbvalor = dt.Rows(i)("total_cob")
                                        If Session("banderafecha") = 1 Then
                                            Dbvalor = Dbvalor * 1.16
                                        Else
                                            Dbvalor = Dbvalor * 1.16
                                        End If
                                        'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                        e.Item.Cells(cuenta).CssClass = "negrita"
                                        e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                        e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                                        Exit For
                                    Else
                                        cuenta = cuenta + 1
                                    End If
                                End If
                            Next
                            cuenta = cuenta + 1
                        Next
                    End If
                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            If arrTotal.Length > 0 Then
                If Not Session("ban") = 1 Then
                    e.Item.Cells(6).Text = "Prima Neta"
                    e.Item.Cells(6).VerticalAlign = VerticalAlign.Middle
                    e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Center
                    Dim i As Integer = 0
                    Dim cuenta As Integer = 7
                    For i = 0 To arrTotal.Length - 1
                        Dbvalor = 0
                        Dbvalor = arrTotal(i)
                        e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                        cuenta = cuenta + 1
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_regresar.Click
        If Session("ban") = 1 Then
            Response.Redirect("..\cotizar\WfrmClientesP.aspx?cveclicoti=" & Session("cliente") & "&cvecoti=1")
        Else
            Response.Redirect("..\cotizar\WfrmImp.aspx?band=" & Session("ban") & "")
        End If
    End Sub

    Private Sub cmd_imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd_imprimir.Click
        Dim cadenaEnvio As String = ""
        Dim strpoliza As String = ""
        Dim llave As Integer
        Session("LlaveC") = 0
        Dim version As String = ccliente.carga_version(Session("version"), IIf(Session("aseguradora") Is Nothing, 0, Session("aseguradora")))
        Try
            If Not Session("idpoliza") Is Nothing Then
                strpoliza = ccliente.sacando_poliza(Session("idpoliza"))
            End If

            Session("Bandera_Espera") = 1
            If strpoliza = "" Then
                llave = ccliente.inserta_contenedor_reportes(Session("bid"), "", "C")
            Else
                llave = ccliente.inserta_contenedor_reportes(Session("bid"), strpoliza, "C")
            End If
            Session("Llave") = llave
            If Session("ban") = 1 Then
                cadenaEnvio = "1|" & Session("ban") & "|" & Session("cotiza") & "|" & _
                    Session("bid") & "|" & Session("cliente") & "|" & Session("idgrdcontrato") & "|" & _
                    Session("vehiculo") & "|" & Session("version") & "|" & Session("EstatusV") & "|" & _
                    llave & "|" & Session("programa") & "|" & Session("anio") & "|" & Session("subramo") & "|" & _
                    Replace(Session("MultArrPolisa"), "|", "+") & "|" & Session("seguro") & "|" & _
                    Session("PaqSelCon") & "|" & Session("resultado")(5) & "|" & viewstate("cveporenganche")
            Else
                Dim strinsco As String = ""
                Dim dti As DataTable = ccliente.carga_insco_homologado_vehiculo(Session("vehiculo"), Session("anio"), Session("aseguradora"))
                If dti.Rows.Count > 0 Then
                    If Not dti.Rows(0).IsNull("insco") Then
                        strinsco = dti.Rows(0)("insco")
                    End If
                End If

                cadenaEnvio = "7|" & Session("ban") & "|" & Session("bid") & "|" & _
                Session("cliente") & "|" & Session("idgrdcontrato") & "|" & _
                Session("vehiculo") & "|" & Session("version") & "|" & _
                Session("EstatusV") & "|" & Session("aseguradora") & "|" & _
                Session("plazo") & "|" & Session("idcontrato") & "|" & llave & "|" & _
                Session("anio") & "|" & Session("programa") & "|" & Session("subramo") & "|" & txt_idpaquete.Text
            End If

            Dim _bsPolizas As New CC.Negocio.Reporte.Polizas()
            _bsPolizas.Export(cadenaEnvio)
            Response.Redirect("..\cotizar\EsperaReportes.html")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
