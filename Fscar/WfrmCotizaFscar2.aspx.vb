Imports CN_Negocios

Partial Class WfrmCotizaFscar2
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private ccalculo As New CnCalculo
    Private cprincipal As New CnPrincipal
    Private arrTotal(0) As String
    Private arrPol(0) As String
    Private arrNombrePol(0) As String
    Private cuentaJava As Integer = 11

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Dim total As Double = 0
        Dim strCadenaS As String = ""
        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                txt_fecha.Text = acompleta_Cero(Date.Now.Day) & "/" & acompleta_Cero(Date.Now.Month) & "/" & Date.Now.Year
                If Not Request.QueryString("valcost") Is Nothing Then
                    txt_precio.Text = Request.QueryString("valcost")
                End If
                txt_sumcob.Text = 0
                limpia()
                carga_datos_vehiculo()
                'cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))

                If Not Request.QueryString("cvevida") Is Nothing Then
                    txt_vida.Text = Request.QueryString("cvevida")
                End If
                If Not Request.QueryString("cvedesempleo") Is Nothing Then
                    txt_desempleo.Text = Request.QueryString("cvedesempleo")
                End If


                If Request.QueryString("cveAseg") Is Nothing Then
                    If Not Request.QueryString("valcost1") Is Nothing Then
                        Dim condicion As Integer = 0
                        'revisar esto ya que las variable de regreso no las estoy considerando
                        txt_grdcontrato.Text = Session("idgrdcontrato")
                        txt_precio.Text = Request.QueryString("valcost1")
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(1))
                        Seg_financiado(rb_seguro)
                        cbo_financiamiento.SelectedIndex = cbo_financiamiento.Items.IndexOf(cbo_financiamiento.Items.FindByValue(0))
                        carga_plazo_wor()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(CInt(Session("plazo"))))
                        txt_enganche.Text = 10
                        lbl_enganche.Text = txt_precio.Text * (txt_enganche.Text / 100)
                        cbo_seguro.SelectedIndex = cbo_seguro.Items.IndexOf(cbo_seguro.Items.FindByValue(1))
                    Else
                        txt_grdcontrato.Text = ccliente.carga_contrato(Session("bid"))
                        Session("idgrdcontrato") = txt_grdcontrato.Text
                        rb_seguro.SelectedIndex = rb_seguro.Items.IndexOf(rb_seguro.Items.FindByValue(1))
                        Seg_financiado(rb_seguro)
                        txt_banseguro.Text = 0
                        cbo_financiamiento.SelectedIndex = cbo_financiamiento.Items.IndexOf(cbo_financiamiento.Items.FindByValue(0))
                        carga_plazo_wor()
                        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
                        cbo_seguro.SelectedIndex = cbo_seguro.Items.IndexOf(cbo_seguro.Items.FindByValue(1))
                        tipo_poliza(1)
                        txt_enganche.Text = 10
                        lbl_enganche.Text = txt_precio.Text * (txt_enganche.Text / 100)
                    End If

                    'nuevo
                    'Seg_financiado(rb_seguro)

                    strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                    Session("anio"), Session("modelo"), _
                    1, _
                    Session("tipopol"), _
                    Session("uso"), Session("EstatusV"), _
                    tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                    IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                    cbo_plazo.SelectedValue, txt_precio.Text, _
                    txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                    txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                     Session("fincon"), _
                    cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)
                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    End If
                Else
                    'nuevo
                    tipo_poliza(cbo_seguro.SelectedValue)
                    Seg_financiado(rb_seguro)
                    Session("primaNeta") = ccalculo.calculo_aseguradora(Session("banderafecha"), Session("moneda"), _
                           Request.QueryString("subramo"), _
                           Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo")), Session("EstatusV"), _
                           tasa_std(rb_tasa), Session("InterestRate"), Request.QueryString("strfecha"), _
                           IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                           Request.QueryString("cveplazo"), txt_precio.Text, _
                           Session("idgrdcontrato"), Session("seguro"), Session("bid"), _
                           txt_sumcob.Text, Session("vehiculo"), Request.QueryString("cveAseg"), _
                           Request.QueryString("cvepaquete"), Session("programa"), _
                           Session("fincon"), Request.QueryString("cvefinancia"), Request.QueryString("cveenganche"))

                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results

                    Session("DatosGenerales") = ""
                    Dim arregloG(23) As String
                    arregloG(0) = Session("moneda")
                    arregloG(1) = Request.QueryString("subramo")
                    arregloG(2) = Session("anio")
                    arregloG(3) = Session("modelo")
                    arregloG(4) = 1
                    arregloG(5) = Session("seguro")
                    arregloG(6) = Session("uso")
                    arregloG(7) = IIf(Session("catalogo") = "0", Request.QueryString("catalogo"), Session("catalogo"))
                    arregloG(8) = Session("EstatusV")
                    arregloG(9) = 0
                    arregloG(10) = Session("InterestRate")
                    arregloG(11) = Request.QueryString("strfecha")
                    arregloG(12) = Session("tiposeguro")
                    arregloG(13) = Request.QueryString("cveplazo")
                    arregloG(14) = txt_precio.Text
                    arregloG(15) = Session("idgrdcontrato")
                    arregloG(16) = Session("seguro")
                    arregloG(17) = Session("bid")
                    arregloG(18) = txt_sumcob.Text
                    arregloG(19) = Session("vehiculo")
                    arregloG(20) = Request.QueryString("cveAseg")
                    arregloG(21) = Request.QueryString("cvepaquete")
                    arregloG(22) = Session("programa")
                    arregloG(23) = Session("fincon")
                    Session("DatosGenerales") = arregloG

                    If Not IsDate(Request.QueryString("strfecha")) Then
                        MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
                        Exit Sub
                    End If
                    'manolito
                    Session("region") = Request.QueryString("cveregion")
                    Session("aseguradora") = Request.QueryString("cveAseg")
                    Session("plazo") = Request.QueryString("cveplazo")
                    Session("FechaInicio") = Request.QueryString("strfecha")
                    Session("precioEscrito") = txt_precio.Text
                    Session("paquete") = Request.QueryString("cvepaquete")
                    Session("Enganche") = Request.QueryString("cveenganche")
                    Session("contrato") = Request.QueryString("cvecontrato")
                    Response.Redirect("..\cotizar\WfrmCliente.aspx?cvevida=" & Request.QueryString("cvevida") & "&cvedesempleo=" & Request.QueryString("cvedesempleo") & "&cvevidau=" & Request.QueryString("cvevidau") & "&cvedesempleou=" & Request.QueryString("cvedesempleou") & "")
                End If

                If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                    Session("subsidios") = ccalculo.subsidios
                    Session("ArrPoliza") = ccalculo.ArrPolisa
                    Session("resultado") = ccalculo.Results
                End If

                If Request.QueryString("valcost1") Is Nothing Then
                    Dim i, j As Integer
                    Dim arr() As String

                    Dim dt As DataTable = ccliente.inserta_coberturas( _
                        strCadenaS, txt_grdcontrato.Text, Session("bid"), Session("programa"), _
                        Session("vehiculo"), Session("EstatusV"), Session("anio"))

                    If Session("programa") = 4 Or Session("programa") = 5 Then
                        If Not Session("MultArrPolisa") = "" Then
                            arr = Split(Session("MultArrPolisa"), "|")
                            For i = 0 To arr.Length - 1
                                If Not arr(i) Is Nothing Then
                                    Dim arrpoliza() As String = arr(i).Split("*")
                                    If arrpoliza(0) <> "" Then
                                        ccalculo.actualiza_cobertura_totales_fscar_VD(arrpoliza(8), _
                                            txt_grdcontrato.Text, Session("bid"), _
                                            arrpoliza(0), 1, Session("programa"), Session("vehiculo"), _
                                            Session("EstatusV"), Session("subramo"))

                                        ccalculo.actualiza_cobertura_totales_fscar_VD(arrpoliza(9), _
                                            txt_grdcontrato.Text, Session("bid"), _
                                            arrpoliza(0), 2, Session("programa"), Session("vehiculo"), _
                                            Session("EstatusV"), Session("subramo"))
                                        If txt_vida.Text = "" Then
                                            txt_vida.Text = arrpoliza(0) & "|" & 0
                                        Else
                                            txt_vida.Text = txt_vida.Text & "*" & arrpoliza(0) & "|" & 0
                                        End If
                                        If txt_desempleo.Text = "" Then
                                            txt_desempleo.Text = arrpoliza(0) & "|" & 0
                                        Else
                                            txt_desempleo.Text = txt_desempleo.Text & "*" & arrpoliza(0) & "|" & 0
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If

                    If dt.Rows.Count > 0 Then
                        grd_cobertura.DataSource = dt
                        grd_cobertura.DataBind()
                        grd_cobertura.Visible = True
                    Else
                        grd_cobertura.Visible = False
                    End If
                Else
                    carga_grd(txt_grdcontrato.Text)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub


    Public Sub desabilita()
        txt_marca.Enabled = False
        txt_tipo.Enabled = False
        txt_subtipo.Enabled = False
        txt_modelo.Enabled = False
        txt_descripcion.Enabled = False
        txt_clasificacion.Enabled = False
    End Sub

    Public Sub limpia()
        txt_marca.Text = ""
        txt_tipo.Text = ""
        txt_subtipo.Text = ""
        txt_modelo.Text = ""
        txt_descripcion.Text = ""
        txt_clasificacion.Text = ""

        txt_grdcontrato.Text = ""
        txt_grdcobertura.Text = ""
        txt_gdescripcion.Text = ""
        txt_ban.Text = 0
    End Sub

    Public Sub carga_datos_vehiculo()
        Try
            txt_marca.Text = Session("MarcaDesc")
            txt_modelo.Text = Session("anio")
            txt_tipo.Text = Session("modelo")
            txt_descripcion.Text = Session("vehiculoDesc")
            txt_subtipo.Text = Session("catalogo")
            txt_clasificacion.Text = IIf(Session("subramo") = 1, "Autom�vil", "Cami�n")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub Seg_financiado(ByVal rb As RadioButtonList)
        Dim tipo As Integer
        Dim i As Integer
        '0 = si (1)
        '1 = no (2)
        If Session("multianual") = 1 Then
            For i = 0 To rb.Items.Count - 1
                If rb.Items(0).Selected Then
                    tipo = 0
                    Session("seguro") = 2
                    'Session("tipopol") = "M"
                    Session("bUnidadfinanciada") = True
                Else
                    tipo = 1
                    Session("seguro") = 0
                    'Session("tipopol") = "A"
                    Session("bUnidadfinanciada") = False
                End If
                Exit For
            Next
        Else
            tipo = 1
            Session("seguro") = 0
            'Session("tipopol") = "A"
            Session("bUnidadfinanciada") = False
        End If
        If Session("programa") = 4 Or Session("programa") = 5 Then
            cbo_seguro.SelectedIndex = cbo_seguro.Items.IndexOf(cbo_seguro.Items.FindByValue(tipo))
        End If
    End Sub

    Public Sub tipo_poliza(ByVal valor As Integer)
        If Session("multianual") = 1 Then
            If valor = 0 Then
                'Session("seguro") = 2
                Session("tipopol") = "M"
                'Session("bUnidadfinanciada") = True
            Else
                'Session("seguro") = 0
                Session("tipopol") = "A"
                'Session("bUnidadfinanciada") = False
            End If
        Else
            Session("tipopol") = "A"
            'Session("seguro") = 0
            'Session("bUnidadfinanciada") = False
        End If
    End Sub

    Private Sub grd_cobertura_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.EditCommand
        Dim total As Double = 0
        Dim factV As Double = 0
        Dim strCadenaS As String = ""
        Try
            If txt_grdcobertura.Text <> 0 Then

                tipo_poliza(cbo_seguro.SelectedValue)
                Seg_financiado(rb_seguro)

                'If txt_gdescripcion.Text = "Auto Sustituto" Or txt_gdescripcion.Text = "Ext. Res. Civil" Then
                If ccliente.valida_grid_edit(Session("ArrPolValida"), txt_ban.Text) = 1 Then

                    ccliente.determina_facvigencia_aseguradora(Session("moneda"), _
                        cbo_plazo.SelectedValue, Session("tipopol"), _
                        txt_grdcontrato.Text, Session("bid"), "", "", _
                        txt_grdcobertura.Text, e.Item.Cells(14).Text, _
                        cbo_plazo.SelectedValue, e.Item.Cells(13).Text)

                    grd_cobertura.EditItemIndex = -1

                    txt_sumcob.Text = 0
                    'nuevo
                    strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                           Session("anio"), Session("modelo"), _
                           1, _
                           Session("tipopol"), _
                           Session("uso"), Session("EstatusV"), _
                           tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                           IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                           cbo_plazo.SelectedValue, txt_precio.Text, _
                           txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                           txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                           Session("fincon"), _
                           cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

                    If strCadenaS = "" Then
                        MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                        Exit Sub
                    End If

                    carga_grd(txt_grdcontrato.Text)
                Else
                    grd_cobertura.EditItemIndex = e.Item.ItemIndex
                    carga_grd(txt_grdcontrato.Text)
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cobertura_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.UpdateCommand
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        Dim descripcion As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grddesc")
        Dim suma As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdsuma")
        Dim factura As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdfact")

        If suma.Text = "" Then
            MsgBox.ShowMessage("Escriba el Monto de la Suma Asegurada")
            Exit Sub
        End If

        If suma.Text > 0 Then
            If descripcion.Text = "" Then
                MsgBox.ShowMessage("Escriba la Descripci�n de la Cobertura")
                Exit Sub
            End If
            If factura.Text = "" Then
                MsgBox.ShowMessage("Escriba el N�mero de Factura")
                Exit Sub
            End If
        Else
            If suma.Text < 0 Then
                MsgBox.ShowMessage("La cantidad del Monto no debe de ser Menor a Cero")
                Exit Sub
            End If
        End If
        Try
            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            ccliente.actualiza_cobertura(suma.Text, descripcion.Text, factura.Text, _
            txt_grdcobertura.Text, txt_grdcontrato.Text, Session("bid"), cbo_plazo.SelectedValue)

            grd_cobertura.EditItemIndex = -1

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                    Session("anio"), Session("modelo"), _
                    1, _
                    Session("tipopol"), _
                    Session("uso"), Session("EstatusV"), _
                    tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                    IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                    cbo_plazo.SelectedValue, txt_precio.Text, _
                    txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                    txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                    Session("fincon"), _
                    cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)
            carga_grd(txt_grdcontrato.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_grd(ByVal intcontrato As Integer)
        Dim dt As DataTable = ccliente.carga_cobertura_bid(intcontrato, Session("bid"), Session("EstatusV"), Session("programa"), Session("vehiculo"))
        If dt.Rows.Count > 0 Then
            grd_cobertura.DataSource = dt
            grd_cobertura.DataBind()
            grd_cobertura.Visible = True
        Else
            grd_cobertura.Visible = False
        End If
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub cbo_plazo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_plazo.SelectedIndexChanged
        Dim total As Double = 0
        Dim strCadenaS As String = ""
        If cbo_plazo.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione el Plazo de Arrendamiento")
            Exit Sub
        End If
        Try
            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            ccalculo.actualiza_numplazo_cob(cbo_plazo.SelectedValue, txt_grdcontrato.Text, Session("bid"))

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                    Session("anio"), Session("modelo"), _
                    1, _
                    Session("tipopol"), _
                    Session("uso"), Session("EstatusV"), _
                    tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                    IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                    cbo_plazo.SelectedValue, txt_precio.Text, _
                    txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                    txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                    Session("fincon"), _
                    cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_cotizar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_cotizar.Click
        If Not IsDate(txt_fecha.Text) Then
            MsgBox.ShowMessage("La fecha de Inicio de vigencia es Incorrecta")
            Exit Sub
        End If

        Session("plazo") = cbo_plazo.SelectedValue
        Session("FechaInicio") = txt_fecha.Text
        Session("precioEscrito") = txt_precio.Text
        Session("idgrdcontrato") = txt_grdcontrato.Text
        Session("Enganche") = lbl_enganche.Text
        Session("paquete") = 0
        Session("contrato") = txt_contrato.Text
        Response.Redirect("..\cotizar\WfrmClientesP.aspx?cvevida=" & txt_vida.Text & "&cvedesempleo=" & txt_desempleo.Text & "")
    End Sub

    Private Sub grd_cobertura_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.ItemCommand
        If Not e.Item.Cells(2).Text = "" Then txt_grdcobertura.Text = e.Item.Cells(2).Text
        If Not e.Item.Cells(3).Text = "" Then txt_gdescripcion.Text = e.Item.Cells(3).Text
        If Not e.Item.Cells(12).Text = "" Then txt_ban.Text = e.Item.Cells(12).Text
    End Sub

    Private Sub grd_cobertura_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.CancelCommand
        grd_cobertura.EditItemIndex = -1
        carga_grd(txt_grdcontrato.Text)
        txt_grdcobertura.Text = 0
    End Sub

    Public Function acompleta_Cero(ByVal cadena As String) As String
        Dim valor As String = ""
        If cadena.Length = 1 Then
            valor = "0" & cadena
        Else
            valor = cadena
        End If
        Return valor
    End Function

    Private Sub cmd_regrear_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regrear.Click
        Response.Redirect("WfrmCotizaFscar1.aspx?valcost=" & txt_precio.Text & "")
    End Sub

    Private Sub grd_cobertura_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cobertura.SelectedIndexChanged

    End Sub

    Private Sub grd_cobertura_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cobertura.DeleteCommand
        Dim total As Double = 0
        Dim descripcion As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grddesc")
        Dim suma As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdsuma")
        Dim factura As TextBox = grd_cobertura.Items(e.Item.ItemIndex).FindControl("txt_grdfact")
        Dim strCadenaS As String = ""
        Try
            If grd_cobertura.EditItemIndex <> -1 Then
                carga_grd(txt_grdcontrato.Text)
                Exit Sub
            End If
            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            ccliente.actualiza_cobertura_VALOR_cero("", "", _
                txt_grdcobertura.Text, txt_grdcontrato.Text, Session("bid"), _
                cbo_plazo.SelectedValue, e.Item.Cells(13).Text)

            grd_cobertura.EditItemIndex = -1

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                    Session("anio"), Session("modelo"), _
                    1, _
                    Session("tipopol"), _
                    Session("uso"), Session("EstatusV"), _
                    tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                    IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                    cbo_plazo.SelectedValue, txt_precio.Text, _
                    txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                    txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                    Session("fincon"), _
                    cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cobertura_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_cobertura.ItemDataBound
        Dim dt As DataTable
        Dim intregion As Integer = 0
        Dim Dbvalor As Double = 0
        If e.Item.ItemType = ListItemType.Header Then
            cuentaJava = 11
            Dim cuenta As Integer = 7
            Dim i As Integer = 0
            Dim hp As HyperLink
            dt = ccliente.carga_cobertura_bid_aseguradora(txt_grdcontrato.Text, Session("bid"))
            If dt.Rows.Count > 0 Then
                ReDim arrTotal(dt.Rows.Count - 1)
                ReDim arrPol(dt.Rows.Count - 1)
                ReDim arrNombrePol(dt.Rows.Count - 1)
                For i = 0 To dt.Rows.Count - 1
                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                    e.Item.Cells(cuenta).Text = dt.Rows(i)("descripcion_aseguradora")

                    'hp = New HyperLink
                    'hp.Text = dt.Rows(i)("descripcion_aseguradora")
                    'hp.NavigateUrl = "WfrmCotiza1.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & ""
                    'hp.CssClass = "negrita"
                    'hp.ForeColor = New System.Drawing.Color().Black
                    'e.Item.Cells(cuenta).Controls.Add(hp)

                    arrPol(i) = dt.Rows(i)("id_aseguradora")
                    arrNombrePol(i) = dt.Rows(i)("descripcion_aseguradora")
                    grd_cobertura.Columns(cuenta).Visible = True
                    cuenta = cuenta + 1
                Next
                If Session("programa") = 4 Or Session("programa") = 5 Then
                    For i = 0 To 1
                        e.Item.Cells(cuenta).Attributes.Add("align", "center")
                        If i = 0 Then
                            e.Item.Cells(cuenta).Text = "Seguro de Vida"
                        Else
                            e.Item.Cells(cuenta).Text = "Seguro de Desempleo"
                        End If
                        grd_cobertura.Columns(cuenta).Visible = True
                        cuenta = cuenta + 1
                    Next
                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim recorrepoliza As Integer = 0
            Dim validarecorrido As Integer = 0
            Dim hp As HyperLink
            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim cuenta As Integer = 7
            cuentaJava = cuentaJava + 1
            If IsNumeric(e.Item.Cells(2).Text) Then
                If e.Item.Cells(2).Text = 0 Then 'coberturas
                    e.Item.Cells(0).Text = ""
                    e.Item.Cells(1).Text = ""
                    dt = ccliente.carga_cobertura_bid_aseguradora_total_cob(txt_grdcontrato.Text, Session("bid"), Session("EstatusV"), e.Item.Cells(3).Text, 0, "")
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            If arrPol(recorrepoliza) = dt.Rows(i)("id_aseguradora") Then

                                validarecorrido = 0

                                Dim DbRvalor As Double = 0
                                arrTotal(i) = dt.Rows(i)("total_cob")
                                Dbvalor = dt.Rows(i)("total_cob")
                                'esto solo se aplica cuando compramo para mostrar la suma general
                                'Dim dtR As DataTable = ccliente.restale_carga_cobertura_total_cob_aseguradora_ramo(txt_grdcontrato.Text, Session("bid"), e.Item.Cells(2).Text, dt.Rows(i)("id_aseguradora"), dt.Rows(i)("id_paquete"))
                                'If dtR.Rows.Count > 0 Then
                                '    For j = 0 To dtR.Rows.Count - 1
                                '        If dtR.Rows.Count > 0 Then
                                '            DbRvalor = dtR.Rows(j)("total_cob")
                                '        End If
                                '        Dbvalor = Dbvalor - (DbRvalor * 1.16)
                                '    Next
                                'End If

                                Dim dtRegion As DataTable = ccliente.consulta_region_aseguradora(dt.Rows(i)("subramo"), dt.Rows(i)("id_aseguradora"), Session("bid"))
                                If dtRegion.Rows.Count > 0 Then
                                    If Not dtRegion.Rows(0).IsNull("id_region") Then
                                        intregion = dtRegion.Rows(0)("id_region")
                                    End If
                                End If

                                'hp = New HyperLink
                                'hp.ImageUrl = "..\Imagenes\Menu\arrow_right.gif"
                                'hp.NavigateUrl = "WfrmCotiza1.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & ""
                                'e.Item.Cells(cuenta).Controls.Add(hp)
                                'e.Item.Cells(cuenta).VerticalAlign = VerticalAlign.Bottom

                                hp = New HyperLink
                                hp.Text = Dbvalor.ToString("c")
                                'hp.NavigateUrl = "WfrmCotizaFscar2.aspx?cveAseg=" & dt.Rows(i)("id_aseguradora") & " &valcost=" & txt_precio.Text & " &cveplazo=" & cbo_plazo.SelectedValue & " &strfecha=" & txt_fecha.Text & " &cveregion=" & intregion & " &cvepaquete=" & dt.Rows(i)("id_paquete") & "&cveenganche=" & lbl_enganche.Text & "&cvecontrato=" & txt_contrato.Text & ""
                                hp.CssClass = "negrita"
                                hp.ForeColor = New System.Drawing.Color().Black
                                hp.ToolTip = "Cobertura : " & e.Item.Cells(3).Text & " " & "Aseguradora : " & arrNombrePol(recorrepoliza)
                                If (Session("nivel") = 0 Or Session("nivel") = 1) Then
                                    hp.Attributes.Add("onclick", "redireccion(" & dt.Rows(i)("id_aseguradora") & "," & intregion & "," & dt.Rows(i)("id_paquete") & "," & cbo_plazo.SelectedValue & ",'" & e.Item.Cells(3).Text & "','" & cbo_financiamiento.SelectedItem.Text & "','" & dt.Rows(i)("subramo") & "')")
                                    hp.Style.Add("cursor", "pointer")
                                End If
                                e.Item.Cells(cuenta).Controls.Add(hp)
                                e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                'e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")

                            Else
                                validarecorrido = 1
                                e.Item.Cells(cuenta).Text = "-"
                                e.Item.Cells(cuenta).HorizontalAlign = HorizontalAlign.Center
                            End If

                            recorrepoliza = recorrepoliza + 1
                            cuenta = cuenta + 1

                            If validarecorrido = 1 Then
                                i = i - 1
                                validarecorrido = 0
                            End If

                            If Session("programa") = 4 Or Session("programa") = 5 Then
                                'vida y desempleo
                                Dim k As Integer
                                Dim ckb As CheckBox
                                Dim ban As Integer = 0
                                For k = 0 To 1
                                    Dim z As Integer = 0
                                    Dbvalor = 0
                                    ckb = New CheckBox
                                    If k = 0 Then
                                        'vida
                                        If Not dt.Rows(i).IsNull("vida") Then
                                            Dbvalor = dt.Rows(i)("vida")
                                            If Dbvalor > 0 Then
                                                ckb.Text = Dbvalor.ToString("c")
                                                ckb.ID = "Vida_" & e.Item.Cells(3).Text
                                                agrega_java(ckb, cuentaJava, e.Item.Cells(3).Text, 1)
                                                '''''ckb.Attributes.Add("Onclick", "ckb_vida()")
                                                Dim arrv() As String = txt_vida.Text.Split("*")
                                                For z = 0 To UBound(arrv)
                                                    Dim arrV1() As String = arrv(z).Split("|")
                                                    If arrV1.IndexOf(arrV1, e.Item.Cells(3).Text) = 0 Then
                                                        If arrV1(1) = 1 Then
                                                            ckb.Checked = True
                                                        End If
                                                    End If
                                                Next
                                                ban = 1
                                            Else
                                                ban = 0
                                            End If
                                        Else
                                            ban = 0
                                        End If
                                    Else
                                        'desempleo
                                        If Not dt.Rows(i).IsNull("Desempleo") Then
                                            Dbvalor = dt.Rows(i)("Desempleo")
                                            If Dbvalor > 0 Then
                                                ckb.Text = Dbvalor.ToString("c")
                                                ckb.ID = "Desempleo_" & e.Item.Cells(3).Text
                                                agrega_java(ckb, cuentaJava, e.Item.Cells(3).Text, 2)
                                                'ckb.Attributes.Add("Onclick", "ckb_desempleo()")
                                                Dim arrD() As String = txt_desempleo.Text.Split("*")
                                                For z = 0 To UBound(arrD)
                                                    Dim arrD1() As String = arrD(z).Split("|")
                                                    If arrD1.IndexOf(arrD1, e.Item.Cells(3).Text) = 0 Then
                                                        If arrD1(1) = 1 Then
                                                            ckb.Checked = True
                                                        End If
                                                    End If
                                                Next
                                                ban = 1
                                            Else
                                                ban = 0
                                            End If
                                        Else
                                            ban = 0
                                        End If
                                    End If
                                    If ban = 1 Then
                                        ckb.CssClass = "combos_small"
                                        e.Item.Cells(cuenta).Controls.Add(ckb)
                                    Else
                                        e.Item.Cells(cuenta).Text = "-"
                                    End If
                                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                    cuenta = cuenta + 1
                                Next
                            End If
                        Next
                    End If
                Else
                    'paquetes especiales
                    dt = ccliente.carga_cobertura_total_cob_aseguradora_ramo(txt_grdcontrato.Text, _
                            Session("bid"), e.Item.Cells(2).Text, Session("ArrPolValida")(j))
                    If dt.Rows.Count > 0 Then
                        For i = 0 To dt.Rows.Count - 1
                            For j = 0 To arrTotal.Length - 1
                                If Session("ArrPolValida")(j) = dt.Rows(i)("id_aseguradora") _
                                    And dt.Rows(i)("total_cob") > 0 _
                                    And e.Item.Cells(12).Text = dt.Rows(i)("cap") _
                                    And e.Item.Cells(13).Text = dt.Rows(i)("sumaaseg_cob") Then
                                    Dbvalor = 0
                                    Dbvalor = dt.Rows(i)("total_cob")
                                    If Session("banderafecha") = 1 Then
                                        Dbvalor = Dbvalor * 1.16
                                    Else
                                        Dbvalor = Dbvalor * 1.16
                                    End If
                                    'e.Item.Cells(cuenta).Attributes.Add("align", "right")
                                    e.Item.Cells(cuenta).CssClass = "negrita"
                                    e.Item.Cells(cuenta).Attributes.Add("align", "center")
                                    e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                                    Exit For
                                Else
                                    cuenta = cuenta + 1
                                End If
                            Next
                            cuenta = cuenta + 1
                        Next
                    End If
                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            If arrTotal.Length > 0 Then
                e.Item.Cells(6).Text = "Total"
                e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Right
                Dim i As Integer = 0
                Dim cuenta As Integer = 7
                For i = 0 To arrTotal.Length - 1
                    Dbvalor = 0
                    Dbvalor = arrTotal(i)
                    e.Item.Cells(cuenta).Text = Dbvalor.ToString("c")
                    cuenta = cuenta + 1
                Next
            End If
        End If
        Session("ArrPolValida") = arrPol
    End Sub

    Public Sub agrega_java(ByVal ckb As CheckBox, ByVal cuenta As Integer, ByVal intpaquete As String, ByVal bantipo As Integer)
        If bantipo = 1 Then
            ckb.Attributes.Add("Onclick", "ckb_vida('" & cuenta & "','" & intpaquete & "')")
        Else
            ckb.Attributes.Add("Onclick", "ckb_desempleo('" & cuenta & "','" & intpaquete & "')")
        End If
    End Sub

    Private Sub cbo_financiamiento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_financiamiento.SelectedIndexChanged
        Dim strCadenaS As String = ""
        Try
            If cbo_financiamiento.SelectedValue = -1 Then
                cbo_plazo.Controls.Clear()
            Else
                If cbo_financiamiento.SelectedValue = 0 Then
                    'wor
                    carga_plazo_wor()
                Else
                    'mo
                    carga_plazo_mo()
                End If
            End If
            ccalculo.actualiza_numplazo_cob(cbo_plazo.SelectedValue, txt_grdcontrato.Text, Session("bid"))

            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                Session("anio"), Session("modelo"), _
                1, _
                Session("tipopol"), _
                Session("uso"), Session("EstatusV"), _
                tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                cbo_plazo.SelectedValue, txt_precio.Text, _
                txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                Session("fincon"), _
                cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_plazo_wor()
        Dim i As Integer = 0
        Dim arr As New ArrayList
        Dim max As Integer = 60
        arr.Add("-- Seleccione --")
        arr.Add(3)
        arr.Add(6)
        For i = 12 To max
            arr.Add(i)
        Next
        cbo_plazo.DataSource = arr
        cbo_plazo.DataBind()
        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(12))
    End Sub

    Public Sub carga_plazo_mo()
        Dim arr As New ArrayList
        arr.Add("-- Seleccione --")
        arr.Add(25)
        arr.Add(37)
        cbo_plazo.DataSource = arr
        cbo_plazo.DataBind()
        cbo_plazo.SelectedIndex = cbo_plazo.Items.IndexOf(cbo_plazo.Items.FindByValue(25))
    End Sub

    Private Sub rb_seguro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_seguro.SelectedIndexChanged
        Dim strCadenaS As String = ""
        Dim i As Integer = 0
        Try
            For i = 0 To rb_seguro.Items.Count - 1
                If rb_seguro.Items(i).Selected Then
                    If rb_seguro.Items(i).Value = 0 Then
                        MsgBox.ShowMessage("Opci�n no Disponible por el Momento")
                        Exit Sub
                    End If
                End If
            Next

            Seg_financiado(rb_seguro)
            tipo_poliza(cbo_seguro.SelectedValue)

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                Session("anio"), Session("modelo"), _
                1, _
                Session("tipopol"), _
                Session("uso"), Session("EstatusV"), _
                tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                cbo_plazo.SelectedValue, txt_precio.Text, _
                txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                Session("fincon"), _
                cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_tipo_seguro()
        Dim i As Integer = 0
        For i = 0 To rb_seguro.Items.Count - 1
            If rb_seguro.Items(i).Selected Then
                If rb_seguro.Items(i).Value = 0 Then
                    txt_banseguro.Text = 2
                Else
                    txt_banseguro.Text = 0
                End If
                Exit For
            End If
        Next
    End Sub

    Private Sub rb_tasa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_tasa.SelectedIndexChanged
        Dim strCadenaS As String = ""
        Try
            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                Session("anio"), Session("modelo"), _
                1, _
                Session("tipopol"), _
                Session("uso"), Session("EstatusV"), _
                tasa_std(rb_tasa), Session("InterestRate"), txt_fecha.Text, _
                IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                cbo_plazo.SelectedValue, txt_precio.Text, _
                txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                Session("fincon"), _
                cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results

            End If

            carga_grd(txt_grdcontrato.Text)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function tasa_std(ByVal rb As RadioButtonList) As String
        Dim tipo As String = "F"
        Dim valor As Double = 0
        Dim i As Integer
        '1 = si
        '2 = no
        For i = 0 To rb.Items.Count - 1
            If rb.Items(0).Selected Then
                Dim dt As DataTable = ccalculo.precontrol(Session("catalogo"), Session("anio"), Session("moneda"), Session("InterestRate"), 0, Session("programa"))
                If dt.Rows.Count > 0 Then
                    If Not dt.Rows(0).IsNull("precontrol") Then
                        valor = dt.Rows(0)("precontrol")
                    End If
                    If valor <= txt_precio.Text Then
                        tipo = "3"
                    Else
                        tipo = "F"
                    End If
                End If
                Session("taza") = 1
                Session("InterestRate") = "B"
            Else
                tipo = "F"
                Session("taza") = 2
                Session("InterestRate") = "C"
            End If
            Exit For
        Next
        Return tipo
    End Function

    Private Sub cbo_seguro_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_seguro.SelectedIndexChanged
        Dim strCadenaS As String = ""
        If cbo_seguro.SelectedValue = -1 Then
            MsgBox.ShowMessage("Favor de seleccionar el tipo de seguro")
            Exit Sub
        End If
        Try
            tipo_poliza(cbo_seguro.SelectedValue)
            Seg_financiado(rb_seguro)

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                Session("anio"), Session("modelo"), _
                1, _
                Session("tipopol"), _
                Session("uso"), Session("EstatusV"), _
                0, Session("InterestRate"), txt_fecha.Text, _
                IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                cbo_plazo.SelectedValue, txt_precio.Text, _
                txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                Session("fincon"), _
                cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub carga_tabla_pagos()
        Dim tbcell As TableCell
        Dim tbrow As TableRow
        Dim i, j As Integer
        Dim arr() As String
        Dim baninicio As Integer = 0
        tb_pago.Controls.Clear()
        Try
            If Not Session("MultArrPolisa") = "" Then
                arr = Split(Session("MultArrPolisa"), "|")
                arr.Sort(arr)
                For i = 0 To arr.Length - 1
                    If Not arr(i) Is Nothing Then
                        Dim arrpoliza() As String = arr(i).Split("*")
                        baninicio = 0
                        For j = 0 To 5
                            If arrpoliza(j) <> "" Then
                                If j = 0 Then
                                    tbrow = New TableRow
                                    tbcell = New TableCell
                                    tbcell.Text = arrpoliza(j)
                                    tbcell.Width = Unit.Percentage(100)
                                    If j = 0 Then
                                        tbcell.CssClass = "header"
                                        tbcell.ColumnSpan = 3
                                    Else
                                        tbcell.CssClass = "negrita"
                                    End If
                                    tbrow.Controls.Add(tbcell)
                                    tb_pago.Controls.Add(tbrow)
                                Else
                                    If CDbl(arrpoliza(j)) <> 0 Then
                                        If baninicio = 0 Then
                                            crea_titulos()
                                            baninicio = 1
                                        End If
                                        tbrow = New TableRow

                                        tbcell = New TableCell
                                        tbcell.Text = Format(CDbl(arrpoliza(j)), "$ #,###,##0.00")
                                        If j = 0 Then
                                            tbcell.CssClass = "header"
                                        Else
                                            tbcell.CssClass = "negrita"
                                        End If
                                        tbrow.Controls.Add(tbcell)
                                        If j = 1 Then
                                            j = 6
                                            tbcell = New TableCell
                                            tbcell.Text = Format(CDbl(arrpoliza(j)), "$ #,###,##0.00")
                                            tbcell.CssClass = "negrita"
                                            tbrow.Controls.Add(tbcell)

                                            j = 7
                                            tbcell = New TableCell
                                            tbcell.Text = Format(CDbl(arrpoliza(j)), "$ #,###,##0.00")
                                            tbcell.CssClass = "negrita"
                                            tbrow.Controls.Add(tbcell)
                                            j = 1
                                        Else
                                            tbcell = New TableCell
                                            tbcell.Text = ""
                                            tbcell.CssClass = "negrita"
                                            tbrow.Controls.Add(tbcell)
                                        End If
                                        tb_pago.Controls.Add(tbrow)
                                    End If
                                End If
                            End If
                        Next
                        tbrow = New TableRow
                        tbcell = New TableCell
                        tbcell.Text = ""
                        tbcell.CssClass = "negrita"
                        tbcell.Height = Unit.Pixel(5)
                        tbrow.Controls.Add(tbcell)
                        tb_pago.Controls.Add(tbrow)
                    End If
                Next
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub crea_titulos()
        Dim tbcell As TableCell
        Dim tbrow As TableRow

        tbrow = New TableRow
        tbcell = New TableCell
        tbcell.Text = "P�liza(s)"
        tbcell.CssClass = "dt_celda"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(40)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Subsidios"
        tbcell.CssClass = "dt_celda"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(30)
        tbrow.Controls.Add(tbcell)

        tbcell = New TableCell
        tbcell.Text = "Subsidios Lealtad"
        tbcell.CssClass = "dt_celda"
        tbcell.VerticalAlign = VerticalAlign.Middle
        tbcell.HorizontalAlign = HorizontalAlign.Center
        tbcell.Width = Unit.Percentage(30)
        tbrow.Controls.Add(tbcell)

        tb_pago.Controls.Add(tbrow)
    End Sub

    'Public Sub carga_tabla_pagos()
    '    Dim tbcell As TableCell
    '    Dim tbrow As TableRow
    '    Dim i, j As Integer
    '    Dim arr() As String
    '    tb_pago.Controls.Clear()
    '    Try
    '        If Not Session("MultArrPolisa") = "" Then
    '            arr = Split(Session("MultArrPolisa"), "|")
    '            arr.Sort(arr)
    '            For i = 0 To arr.Length - 1
    '                If Not arr(i) Is Nothing Then
    '                    Dim arrpoliza() As String = arr(i).Split("*")
    '                    For j = 0 To arrpoliza.Length - 1
    '                        If arrpoliza(j) <> "" Then
    '                            If j = 0 Then
    '                                tbrow = New TableRow
    '                                tbcell = New TableCell
    '                                tbcell.Text = arrpoliza(j)
    '                                tbcell.Width = Unit.Percentage(100)
    '                                If j = 0 Then
    '                                    tbcell.CssClass = "header"
    '                                Else
    '                                    tbcell.CssClass = "negrita"
    '                                End If
    '                                tbrow.Controls.Add(tbcell)
    '                                tb_pago.Controls.Add(tbrow)
    '                            Else
    '                                If CDbl(arrpoliza(j)) <> 0 Then
    '                                    tbrow = New TableRow
    '                                    tbcell = New TableCell
    '                                    tbcell.Text = Format(CDbl(arrpoliza(j)), "$ #,###,##0.00")
    '                                    tbcell.Width = Unit.Percentage(100)
    '                                    If j = 0 Then
    '                                        tbcell.CssClass = "header"
    '                                    Else
    '                                        tbcell.CssClass = "negrita"
    '                                    End If
    '                                    tbrow.Controls.Add(tbcell)
    '                                    tb_pago.Controls.Add(tbrow)
    '                                End If
    '                            End If
    '                        End If
    '                    Next
    '                    tbrow = New TableRow
    '                    tbcell = New TableCell
    '                    tbcell.Text = ""
    '                    tbcell.CssClass = "negrita"
    '                    tbcell.Height = Unit.Pixel(5)
    '                    tbrow.Controls.Add(tbcell)
    '                    tb_pago.Controls.Add(tbrow)
    '                End If
    '            Next
    '        End If

    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '    End Try
    'End Sub

    Private Sub txt_enganche_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_enganche.TextChanged
        Dim strCadenaS As String
        Dim dbvalor As Double = 0
        Try
            dbvalor = txt_precio.Text * (txt_enganche.Text / 100)
            lbl_enganche.Text = dbvalor

            txt_sumcob.Text = 0
            'nuevo
            strCadenaS = ccalculo.calculo(Session("banderafecha"), Session("moneda"), _
                Session("anio"), Session("modelo"), _
                1, _
                Session("tipopol"), _
                Session("uso"), Session("EstatusV"), _
                0, Session("InterestRate"), txt_fecha.Text, _
                IIf(cbo_seguro.SelectedValue = 1, 2, Session("tiposeguro")), _
                cbo_plazo.SelectedValue, txt_precio.Text, _
                txt_grdcontrato.Text, Session("seguro"), Session("bid"), _
                txt_sumcob.Text, Session("vehiculo"), Session("programa"), _
                Session("fincon"), _
                cbo_financiamiento.SelectedItem.Text, lbl_enganche.Text)

            If strCadenaS = "" Then
                MsgBox.ShowMessage("No existe informaci�n del auto a cotizar, favor de consultar al administrador")
                Exit Sub
            End If

            If Session("programa") = 1 Or Session("programa") = 3 Or Session("programa") = 4 Or Session("programa") = 5 Then
                Session("subsidios") = ccalculo.subsidios
                Session("ArrPoliza") = ccalculo.ArrPolisa
                Session("resultado") = ccalculo.Results
            End If

            carga_grd(txt_grdcontrato.Text)
            lbl_enganche.Text = Format(dbvalor, "#,##0.00")

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub pc_reporte_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pc_reporte.SelectionChanged

    End Sub
End Class
