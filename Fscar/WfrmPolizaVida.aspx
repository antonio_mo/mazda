<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmPolizaVida.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmPolizaVida" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 101; LEFT: 376px; POSITION: absolute; TOP: 816px" runat="server"></cc1:MsgBox>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center" width="25%">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\logos\VIDA.GIF" ImageAlign="Baseline"></asp:image></TD>
					<TD align="center" width="50%"></TD>
					<TD vAlign="top" align="right" width="25%">
						<asp:image id="Image4" runat="server" ImageUrl="..\Imagenes\logos\Zurich.GIF" ImageAlign="Baseline"></asp:image></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="obligatorio_negro">
									P�liza No.</TD>
								<TD class="obligatorio_negro">
									Certificado</TD>
								<TD class="obligatorio_negro">Vigencia Desde</TD>
								<TD class="obligatorio_negro">Vigencia Hasta</TD>
							</TR>
							<TR>
								<TD>
									<asp:Label id="Label1" runat="server" Width="100%" CssClass="small">000003</asp:Label></TD>
								<TD>
									<asp:Label id="lbl_poliza" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
								<TD>
									<asp:Label id="lbl_FI" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
								<TD>
									<asp:Label id="lbl_FF" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="negrita" align="center" colSpan="4">A las 12 horas (medio d�a) del lugar 
									de expedic�n</TD>
							</TR>
							<TR>
								<TD colSpan="4">
									<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
										<TR>
											<TD vAlign="top" width="50%">
												<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro" colSpan="4">Nombre del Asegurado</TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Nombre :</TD>
														<TD colSpan="3">
															<asp:label id="lbl_nombre" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Domicilio :</TD>
														<TD colSpan="3">
															<asp:label id="lbl_dir" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">RFC :</TD>
														<TD>
															<asp:label id="lbl_rfc" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">Tel. :&nbsp;
														</TD>
														<TD>
															<asp:label id="lbl_tel" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Poblaci�n :</TD>
														<TD>
															<asp:label id="lbl_poblacion" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD class="obligatorio_negro" align="right">C.P. :&nbsp;
														</TD>
														<TD>
															<asp:label id="lbl_cp" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="1%"></TD>
											<TD vAlign="top" width="49%">
												<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="obligatorio_negro" colSpan="2">Fecha de Nacimiento</TD>
														<TD class="obligatorio_negro" align="center" colSpan="2">Edad Asegurado</TD>
													</TR>
													<TR>
														<TD colSpan="2">
															<asp:label id="lbl_anio" runat="server" Width="100%" CssClass="small"></asp:label></TD>
														<TD colSpan="2">
															<asp:label id="lbl_edad" runat="server" Width="100%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro">Ocupaci�n</TD>
														<TD></TD>
														<TD class="obligatorio_negro" align="right"></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" colSpan="3"></TD>
														<TD></TD>
													</TR>
													<TR>
														<TD class="obligatorio_negro" width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
														<TD width="25%"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="negrita" vAlign="top" width="50%">DATOS DEL SEGURO</TD>
											<TD width="1%"></TD>
											<TD vAlign="top" width="49%"></TD>
										</TR>
										<TR>
											<TD vAlign="top" width="50%">
												<TABLE id="Table6" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="negrita" width="35%">Fecha de Emisi�n
														</TD>
														<TD width="65%">
															<asp:label id="lbl_femision" runat="server" Width="60%" CssClass="small"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="1%"></TD>
											<TD vAlign="top" width="49%">
												<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="negrita" width="35%">Plazo del Seguro</TD>
														<TD width="65%">
															<asp:label id="lbl_plazo" runat="server" Width="60%" CssClass="small"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" width="50%">
												<TABLE id="Table9" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="negrita" width="35%">Coberturas :</TD>
														<TD width="65%"></TD>
													</TR>
													<TR>
														<TD class="negrita" colSpan="2">
															<P>Temporal por fallecimiento</P>
														</TD>
													</TR>
													<TR>
														<TD class="negrita">Gastos funerarios</TD>
														<TD></TD>
													</TR>
													<TR>
														<TD class="negrita" colSpan="2">Edad: Minima de aceptaci�n 18 a�os, maximo 70 
															a�os&nbsp;</TD>
													</TR>
													<TR>
														<TD class="negrita" colSpan="2">Suma asegurada : suma asegurada fija</TD>
													</TR>
													<TR>
														<TD class="negrita" colSpan="2">Tipo de moneda : Pesos</TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="1%"></TD>
											<TD vAlign="top" width="49%">
												<TABLE id="Table10" cellSpacing="1" cellPadding="1" width="100%" border="0">
													<TR>
														<TD class="negrita" width="60%">Suma Asegurada por Fallecimiento</TD>
														<TD width="40%">
															<asp:label id="lbl_fallecimiento" runat="server" Width="70%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="negrita">Suma Asegurada Gastos Funerarios</TD>
														<TD>
															<asp:label id="lbl_funerario" runat="server" Width="70%" CssClass="small">$ 20,000.00</asp:label></TD>
													</TR>
													<TR>
														<TD class="negrita">Prima</TD>
														<TD>
															<asp:label id="lbl_prima" runat="server" Width="70%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="negrita">Derecho de P�liza</TD>
														<TD>
															<asp:label id="lbl_derpol" runat="server" Width="70%" CssClass="small"></asp:label></TD>
													</TR>
													<TR>
														<TD class="negrita">Prima Total</TD>
														<TD>
															<asp:label id="lbl_pt" runat="server" Width="70%" CssClass="small"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" width="50%"></TD>
											<TD width="1%"></TD>
											<TD vAlign="top" width="49%"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD width="25%"></TD>
								<TD width="25%"></TD>
								<TD width="25%"></TD>
								<TD width="25%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2">
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="60%"></TD>
								<TD align="right" width="20%">
									<asp:imagebutton id="cmd_regresar" runat="server" ImageUrl="..\Imagenes\Botones\regresar.gif"></asp:imagebutton></TD>
								<TD align="right" width="20%">
									<asp:imagebutton id="cmd_imprimir" runat="server" ImageUrl="..\Imagenes\Botones\imprimir.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
