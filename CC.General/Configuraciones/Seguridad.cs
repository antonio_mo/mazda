﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace CC.General.Configuraciones
{
    public class Seguridad
    {
        private const string C_PassPhrase = "Pas5pr@se";
        private const string C_SaltValue = "s@1tValue";
        private const string C_HashAlgorithm = "SHA1";
        private const int C_PasswordIterations = 2;
        private const string C_InitVector = "@1B2c3D4e5F6g7H8";
        private const int C_KeySize = 256;

        /// <summary>
        /// Permite encriptar un texto determinado según una personalización
        /// </summary>
        /// <param name="textoSinEncriptar"></param>
        /// <returns></returns>
        public static string Encrypt(string textoSinEncriptar)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(C_InitVector);
            byte[] bytes2 = Encoding.ASCII.GetBytes(C_SaltValue);
            byte[] bytes3 = Encoding.UTF8.GetBytes(textoSinEncriptar);
            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(C_PassPhrase, bytes2, C_HashAlgorithm, C_PasswordIterations);
            byte[] bytes4 = passwordDeriveBytes.GetBytes(C_KeySize / 8);
            ICryptoTransform transform = new RijndaelManaged().CreateEncryptor(bytes4, bytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes3, 0, bytes3.Length);
            cryptoStream.FlushFinalBlock();
            byte[] inArray = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(inArray);
        }

        /// <summary>
        /// Permite desencriptar un texto determinado según una personalización
        /// </summary>
        /// <param name="textoEncriptado"></param>
        /// <returns></returns>
        public static string Decrypt(string textoEncriptado)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(C_InitVector);
            byte[] bytes2 = Encoding.ASCII.GetBytes(C_SaltValue);
            byte[] array = Convert.FromBase64String(textoEncriptado.Replace(" ", "+"));
            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(C_PassPhrase, bytes2, C_HashAlgorithm, C_PasswordIterations);
            byte[] bytes3 = passwordDeriveBytes.GetBytes(C_KeySize / 8);
            ICryptoTransform transform = new RijndaelManaged().CreateDecryptor(bytes3, bytes);
            MemoryStream memoryStream = new MemoryStream(array);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read);
            byte[] array2 = new byte[array.Length];
            int count = cryptoStream.Read(array2, 0, array2.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(array2, 0, count);
        }
    }
}
