﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.General.Funciones
{
    public class ValidacionesPol
    {
        public static int GetValueInt(object sValue, int iReturnTrue, object iReturnFalse)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return iReturnTrue;
            else
                return (int)iReturnFalse;
        }

        public static string GetValueString(object sValue, string strReturnNull, object strReturnFalse)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return strReturnNull;
            else
                return strReturnFalse.ToString();
        }

        public static string GetValueMoney(object sValue, decimal decReturnTrue, object decReturnFalse)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return decReturnTrue.ToString("$ ###,##0.00");
            else
                return (decimal.Parse(decReturnFalse.ToString())).ToString("$ ###,##0.00");
        }

        public static string GetValueMoney(object sValue, string strReturnTrue, object decReturnFalse)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return strReturnTrue;
            else
                return (decimal.Parse(decReturnFalse.ToString())).ToString("$ ###,##0.00");
        }

        public static string GetValueMoneySSimbolo(object sValue, string strReturnTrue, object decReturnFalse)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return strReturnTrue;
            else
                return (decimal.Parse(decReturnFalse.ToString())).ToString("###,##0.00");
        }

        public static string GetValueMoney(object sValue)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return 0.ToString("$ ###,##0.00");
            else
                return (decimal.Parse(sValue.ToString())).ToString("$ ###,##0.00");
        }

        public static string GetValueMoneyNotDec(object sValue)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return 0.ToString("$ ###,##0");
            else
                return (decimal.Parse(sValue.ToString())).ToString("$ ###,##0");
        }

        public static string GetValueDate(object sValue)
        {
            if (sValue == null || sValue.ToString() == string.Empty)
                return string.Empty;
            else
                return DateTime.Parse(sValue.ToString()).ToString("dd/MM/yyyy");
        }
    }
}
