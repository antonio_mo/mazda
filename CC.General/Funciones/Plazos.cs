﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.General.Funciones
{
    public class Plazos
    {
        public static int TotalPagos(int iPlazo, int iIdRecargo)
        {
            switch((CC.General.Diccionario.Recargo.Descripcion)iIdRecargo)
            {
                case CC.General.Diccionario.Recargo.Descripcion.Contado:
                    return 1;
                case CC.General.Diccionario.Recargo.Descripcion.Anual:
                    return iPlazo / 12;
                default:
                    return 0;
            }
        }
    }
}
