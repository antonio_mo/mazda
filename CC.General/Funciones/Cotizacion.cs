﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.General.Funciones
{
    public class Cotizacion
    {
        public static int DeterminaDiaCotizacion(int iDia)
        {
            int iBandSal = 0;

            if(iDia >= 1 && iDia <= 15)
                iBandSal = 0;
            else
                iBandSal = 1;

            return iBandSal;
        }        
    }
}
