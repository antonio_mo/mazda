﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.General.Diccionario
{
    public class PDFForm
    {
        public const string LIBRARY = "pdfform.dll";
        public const string SERIAL = "55XSD5234455E6DKET38";
        public const string PLANTILLA_CHUBB_F = "PolizaCHUBB_F.pdf";
        public const string PLANTILLA_CHUBB_C = "PolizaCHUBB_C.pdf";
        public const string PLANTILLA_HDI_F = "Poliza_HDI_F.pdf";
        public const string PLANTILLA_HDI_C = "Poliza_HDI_C.pdf";
        public const string PLANTILLA_QTS_F = "Poliza_Qualitas_F.pdf";
        public const string PLANTILLA_QTS_C = "Poliza_Qualitas_C.pdf";

        public enum TipoDato
        {
            ValidaNulo,
            ValidaMonedaEmpty,
            ValidaMonedaSinSimbolo,
            ValidaMoneda0,
            ValidaFecha,
            Default
        }
    }
}
