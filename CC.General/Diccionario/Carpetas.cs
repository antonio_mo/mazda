﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CC.General.Diccionario
{
    public class Carpetas
    {
        public const string POLIZA_LOG = @"ArchivosLog\";
        public const string POLIZA_PDF = @"Polizas\";
        public const string POLIZA_PLANTILLAS = @"Plantillas\";

        public const string REPORTES = @"Reportes\";
        public const string LOGCOTIZACION = @"Reporte\ArchivosLog\";
        public const string GENERADOCOTIZACION = @"Reporte\Generados\";
        public const string IMAGENESCOTIZACION = @"Reporte\Imagenes\";
    }
}
