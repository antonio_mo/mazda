use AONSEGUROS_FORD_CONAUTO

begin tran uno

delete FROM PARAMETRO where id_aseguradora = 59


/*******************************************************/

update menu_portada set url_indice = '..\cotizar\WfrmReCotizaPromo.aspx?unico=1' where id_portada = 2


/*******************************************************/

USE [AONSEGUROS_FORD_CONAUTO]
GO
/****** Object:  Trigger [dbo].[UPDPOLIZA]    Script Date: 11/30/2014 23:33:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER TRIGGER [dbo].[UPDPOLIZA] ON [dbo].[POLIZA] 
FOR UPDATE 
AS
BEGIN TRAN

DECLARE @IDASEGURADORA		INT
DECLARE @IDPAQUETE		INT
DECLARE @TARIFA			VARCHAR(10)
DECLARE @IDPOLIZA		INT
DECLARE @IDBID			SMALLINT

IF UPDATE(ID_PAQUETE)
BEGIN 
	SELECT @IDPAQUETE = ID_PAQUETE, @IDASEGURADORA = ID_ASEGURADORA, @IDPOLIZA = ID_POLIZA, @IDBID = ID_BID FROM INSERTED
	
	IF (@IDASEGURADORA = 63)
	BEGIN
		SELECT @TARIFA = ISNULL(TARIFA,'') from ABC_PAQUETE where id_aseguradora = @IDASEGURADORA AND ID_PAQUETE = @IDPAQUETE
	END
	ELSE
	BEGIN
		IF (@IDASEGURADORA = 61) -- AXA DEL PROGRAMA DE CONAUTO
		BEGIN
			SELECT @TARIFA = ISNULL(TARIFA,'') from ABC_PAQUETE where id_aseguradora = @IDASEGURADORA AND ID_PAQUETE = @IDPAQUETE
		END
	END
	
	IF NOT @TARIFA =''
	BEGIN 
		UPDATE POLIZA SET TARIFA = @TARIFA WHERE ID_POLIZA = @IDPOLIZA AND ID_BID = @IDBID AND ID_ASEGURADORA = @IDASEGURADORA
	END
END

COMMIT TRAN

IF (@@ERROR>0)
BEGIN
	RAISERROR('ERROR EN INSERCCION P',16,1)
	ROLLBACK TRAN
END



/*******************************************************/

ALTER PROCEDURE RECARGA12MESESCOTIZACIONCONTRATADO(@COTIZAUNICA INT)
AS
BEGIN 
BEGIN TRAN

/*
select Maximo = max(isnull(primatotal,0))
from COTIZACION_PANEL_12meses cp, COTIZACION_UNICA cu
where cp.Id_Cotizacion12meses = cu.Id_Cotizacion12meses
and cu.id_CotizacionUnica = @COTIZAUNICA
*/

select a.Id_Aseguradora, a.descripcion_aseguradora, a.url_imagenpeque, PT = isnull(primatotal,0), bandera = isnull(cp.banderacompra,0)
from COTIZACION_PANEL_12MESESCONTRATADO cp, COTIZACION_UNICA cu, abc_aseguradora a 
where cp.Id_Cotizacion12mesesC = cu.Id_Cotizacion12mesesC
and cp.id_aseguradora = a.id_aseguradora
and cu.id_CotizacionUnica = @COTIZAUNICA
/*order by a.id_aseguradora*/
order by a.descripcion_aseguradora

COMMIT TRAN

IF (@@ERROR>0)
BEGIN
	RAISERROR('ERRORNULL',16,1)
	ROLLBACK TRAN
	RETURN 0
END
END

/*******************************************************/


Alter VIEW dbo.vw_carga_interfase_conauto29
AS
SELECT     dbo.POLIZA.Id_Poliza, dbo.POLIZA.Poliza, 0 AS endoso, dbo.POLIZA.Insco, dbo.POLIZA.Anio, dbo.POLIZA.Descripcion, dbo.POLIZA.Placas, 
                      dbo.CONTRATO.Num_Contrato, dbo.POLIZA.Vigencia_FIniP, dbo.POLIZA.Vigencia_FFinp, '01' AS uso, dbo.POLIZA.Num_Pago, dbo.ABC_TIPORECARGO.Periodo, 
                      dbo.POLIZA.Motor, dbo.POLIZA.Serie, dbo.POLIZA.Renave, dbo.POLIZA.Capacidad, dbo.ABC_TIPOCARGA.Descripcion_TipoCarga, dbo.POLIZA.Precio_Vehiculo, 
                      dbo.POLIZA.PrimaNeta, dbo.POLIZA.DerPol, dbo.POLIZA.Pago_Fraccionado, 0 AS Descuentos, dbo.POLIZA.Iva, 0 AS Recargos, 0 AS SesionComision, 0 AS Comision, 
                      dbo.POLIZA.PrimaTotal, dbo.CLIENTE.Razon_Social, dbo.CLIENTE.Nombre, dbo.CLIENTE.APaterno, dbo.CLIENTE.AMaterno, dbo.CLIENTE.Per_Fiscal, 
                      dbo.CLIENTE.Calle_Cliente, dbo.CLIENTE.NoInterior, dbo.CLIENTE.NoExterior, dbo.CLIENTE.Colonia_Cliente, dbo.CLIENTE.Estado_Cliente, 
                      dbo.CLIENTE.Ciudad_Cliente, dbo.CLIENTE.Cp_Cliente, dbo.CLIENTE.Rfc, dbo.POLIZA.Interface, dbo.CLIENTE.Telefono, dbo.CLIENTE.FNacimiento, 
                      dbo.CLIENTE.Email, dbo.POLIZA.Clave_Agente, dbo.POLIZA.Id_Aseguradora, dbo.POLIZA.Id_Paquete, dbo.POLIZA.Id_TipoRecargo, dbo.POLIZA.Plazo, 
                      dbo.ABC_PAQUETE.SubRamo, dbo.POLIZA.TipoProducto, dbo.POLIZA.Benef_Pref, dbo.CLIENTE.Sexo, dbo.CLIENTE.Estado_Civil, dbo.POLIZA.Tipo_Poliza, 
                      dbo.POLIZA.Poliza_Multianual, dbo.POLIZA.Contrato, dbo.POLIZA.Caratula, dbo.POLIZA.Fecha_Registro, dbo.POLIZA.Marca, dbo.POLIZA.Id_Cancelacion, 
                      dbo.POLIZA.Fecha_Cancelacion, dbo.POLIZA.Interface_Cancelacion, dbo.ABC_CANCELACION.Descripcion_Cancelacion, dbo.POLIZA.Contador_Poliza, 
                      dbo.POLIZA.TARIFA, dbo.POLIZA.ContadorPoliza, dbo.poliza.id_bid, dbo.poliza.Cobertura, dbo.poliza.Modelo
FROM         dbo.CLIENTE_POLIZA INNER JOIN
                      dbo.CONTRATO ON dbo.CLIENTE_POLIZA.Id_Contrato = dbo.CONTRATO.Id_Contrato AND dbo.CLIENTE_POLIZA.Id_Cliente = dbo.CONTRATO.Id_cliente AND 
                      dbo.CLIENTE_POLIZA.Id_Bid = dbo.CONTRATO.Id_Bid INNER JOIN
                      dbo.POLIZA ON dbo.CLIENTE_POLIZA.Id_Poliza = dbo.POLIZA.Id_Poliza AND dbo.CLIENTE_POLIZA.Id_Bid = dbo.POLIZA.Id_Bid INNER JOIN
                      dbo.ABC_TIPORECARGO ON dbo.POLIZA.Id_TipoRecargo = dbo.ABC_TIPORECARGO.Id_TipoRecargo INNER JOIN
                      dbo.CLIENTE ON dbo.CLIENTE_POLIZA.Id_Cliente = dbo.CLIENTE.Id_Cliente AND dbo.CLIENTE_POLIZA.Id_Bid = dbo.CLIENTE.Id_Bid INNER JOIN
                      dbo.ABC_PAQUETE ON dbo.POLIZA.Id_Aseguradora = dbo.ABC_PAQUETE.Id_Aseguradora AND 
                      dbo.POLIZA.Id_Paquete = dbo.ABC_PAQUETE.Id_Paquete LEFT OUTER JOIN
                      dbo.ABC_CANCELACION ON dbo.POLIZA.Id_Cancelacion = dbo.ABC_CANCELACION.Id_Cancelacion LEFT OUTER JOIN
                      dbo.ABC_TIPOCARGA ON dbo.POLIZA.Id_TipoCarga = dbo.ABC_TIPOCARGA.Id_TipoCarga


/*******************************************************/



Alter VIEW dbo.VW_REPORTE_POLIZA_CONTADO
AS
SELECT     dbo.POLIZA.Id_Aseguradora, dbo.CLIENTE_POLIZA.Id_Bid, dbo.CLIENTE_POLIZA.Id_Cliente, dbo.CLIENTE_POLIZA.Id_Contrato, dbo.CLIENTE_POLIZA.Id_Poliza, 
                      CONVERT(varchar(2), DATEPART(dd, dbo.POLIZA.Vigencia_FIniP)) + ' de ' + CONVERT(varchar(20), dbo.Mese_Esp(DATEPART(month, dbo.POLIZA.Vigencia_FIniP))) 
                      + ' de ' + CONVERT(varchar(4), DATEPART(yyyy, dbo.POLIZA.Vigencia_FIniP)) AS inicio1, CONVERT(varchar(2), DATEPART(dd, dbo.POLIZA.Vigencia_FFinp)) 
                      + ' de ' + CONVERT(varchar(20), dbo.Mese_Esp(DATEPART(month, dbo.POLIZA.Vigencia_FFinp))) + ' de ' + CONVERT(varchar(4), DATEPART(yyyy, 
                      dbo.POLIZA.Vigencia_FFinp)) AS fin1, CASE per_fiscal WHEN 2 THEN isnull(razon_social, '') ELSE isnull(nombre, '') + ' ' + isnull(apaterno, '') + ' ' + isnull(amaterno, '') 
                      END AS nom, ISNULL(dbo.CLIENTE.Calle_Cliente, '') + ' Col. ' + ISNULL(dbo.CLIENTE.Colonia_Cliente, '') AS dir, dbo.CLIENTE.Rfc, 
                      ISNULL(dbo.CLIENTE.Ciudad_Cliente, '') + ', ' + ISNULL(dbo.CLIENTE.Estado_Cliente, '') AS poblacion, dbo.POLIZA.Benef_Pref, dbo.CLIENTE.Telefono, 
                      dbo.CLIENTE.Cp_Cliente, dbo.POLIZA.Fecha_Registro, dbo.ABC_PAQUETE.Paquete, dbo.POLIZA.Producto, ISNULL(dbo.POLIZA.Marca, '') 
                      + ' - ' + ISNULL(dbo.POLIZA.Modelo, '') + ' - ' + ISNULL(dbo.POLIZA.Descripcion, '') AS autos, dbo.POLIZA.Anio, dbo.POLIZA.Serie, dbo.POLIZA.Placas, 
                      dbo.POLIZA.Motor, dbo.ABC_USO.Uso, dbo.POLIZA.Capacidad, dbo.POLIZA.Renave, 
                      CASE poliza.Fscstatus WHEN 'N' THEN 'AUTO-NUEVO' ELSE 'AUTO-SEMINUEVO' END AS vehiculo, dbo.PN(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, 
                      dbo.POLIZA.SubSidyD, dbo.POLIZA.PrimaNeta, dbo.POLIZA.PrimaTotal, dbo.POLIZA.DerPol) AS PN, dbo.POLIZA.PrimaTotal, dbo.VERSION.Version, 
                      dbo.ivaC(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, dbo.POLIZA.SubSidyD, dbo.POLIZA.PrimaNeta, dbo.POLIZA.PrimaTotal, dbo.POLIZA.DerPol, dbo.POLIZA.Iva) 
                      AS ivaC, dbo.CUENTA_POLIZAS(dbo.CLIENTE_POLIZA.Id_Cliente, dbo.CLIENTE_POLIZA.Id_Bid, dbo.CLIENTE_POLIZA.Id_Contrato) AS numeros_polizas, 
                      dbo.iva_individual(dbo.POLIZA.Camnum01, dbo.POLIZA.Iva, dbo.CLIENTE_POLIZA.Id_Cliente, dbo.CLIENTE_POLIZA.Id_Bid, dbo.CLIENTE_POLIZA.Id_Contrato) 
                      AS iva_individual, dbo.der_pol(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, dbo.POLIZA.SubSidyD, dbo.POLIZA.DerPol, dbo.POLIZA.PrimaTotal) AS der_pol, 
                      dbo.POLIZA.Tipo_Poliza, CASE WHEN dbo.poliza.bansegvida = 1 THEN dbo.FIRMAABAJO.ISCO + '/' + ISNULL(dbo.FIRMAABAJO.Tipo_Venta, '') 
                      + ISNULL(dbo.FIRMAABAJO.Tipo_Poliza, '') + '/' + CONVERT(varchar(20), ISNULL(dbo.FIRMAABAJO.Precio_Auto, 0)) + '/' + CONVERT(varchar(20), 
                      ISNULL(dbo.FIRMAABAJO.Precio_vida, 0)) ELSE dbo.FIRMAABAJO.ISCO + '/' + ISNULL(dbo.FIRMAABAJO.Tipo_Venta, '') + ISNULL(dbo.FIRMAABAJO.Tipo_Poliza, '') 
                      + '/' + CONVERT(varchar(20), ISNULL(dbo.FIRMAABAJO.Precio_Auto, 0)) END AS CADENA, dbo.PrimTOTAL(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, 
                      dbo.POLIZA.SubSidyD, dbo.POLIZA.PrimaTotal) AS PrimTOTAL, dbo.pt(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, dbo.POLIZA.SubSidyD, dbo.POLIZA.PrimaNeta, 
                      dbo.POLIZA.PrimaTotal, dbo.POLIZA.DerPol, dbo.POLIZA.Iva) AS pt, dbo.POLIZA.Cobaddf1_D, dbo.POLIZA.Cobaddf1_F, CASE WHEN NOT COBADDF1_D IS NULL 
                      THEN CASE WHEN NOT COBADDF1_D = '' THEN 'Equipo Especial' ELSE '' END ELSE '' END AS nom1D, dbo.POLIZA.Cobaddf2_D, dbo.POLIZA.Cobaddf2_F, 
                      CASE WHEN NOT COBADDF2_D IS NULL THEN CASE WHEN NOT COBADDF2_D = '' THEN 'Adapataciones y Conversiones' ELSE '' END ELSE '' END AS nom2D, 
                      dbo.POLIZA.Cobaddf3_D, dbo.POLIZA.Cobaddf3_F, CASE WHEN NOT COBADDF3_D IS NULL 
                      THEN CASE WHEN NOT COBADDF3_D = '' THEN 'Blindaje' ELSE '' END ELSE '' END AS nom3D, 
                      CASE WHEN PART_THEFT_COV = '0' THEN '' ELSE 'Robo parcial de interiores. Ampara solo equipo original Ford, no ampara da�os materiales ocasionados a consecuencia del robo parcial o intento del mismo.Suma asegurada '
                       + CONVERT(varchar(20), Part_Theft_Cov) + ' deducible de ' + CONVERT(varchar(20), Part_Theft_Ded) + ' No existe reinstalaci�n de suma asegurada' END AS Leyenda,
                       dbo.POLIZA.Camnum01, dbo.valida_subsidios(dbo.POLIZA.SubSidyF, dbo.POLIZA.SubSidyFC, dbo.POLIZA.SubSidyD) AS sub, dbo.POLIZA.Poliza_Multianual, 
                      dbo.ABC_PAQUETE.Descripcion_Paquete, dbo.ABC_PAQUETE.Id_Paquete, dbo.VERSION.Id_Version, dbo.VERSION.Vigencia, dbo.POLIZA.Cobaddp1_M, 
                      dbo.POLIZA.Cobaddp3_M, dbo.POLIZA.Cobaddf1_M, dbo.POLIZA.Cobaddf3_M, dbo.FIRMAABAJO.ISCO, dbo.ABC_PAQUETE.SubRamo, dbo.POLIZA.Poliza, 
                      dbo.POLIZA.Fscstatus, dbo.POLIZA.Deduc_Pl, dbo.POLIZA.Marca, dbo.CLIENTE.FNacimiento, dbo.POLIZA.Recargos, dbo.POLIZA.PrimaNeta, 
                      dbo.CONTRATO.Num_Contrato, dbo.CLIENTE.Per_Fiscal, dbo.POLIZA.Precio_Vehiculo, ISNULL(dbo.CLIENTE.Calle_Cliente, '') AS calle, 
                      ISNULL(dbo.CLIENTE.Colonia_Cliente, '') AS col, dbo.POLIZA.BanSegVida, dbo.CLIENTE.NoExterior, dbo.CLIENTE.NoInterior, dbo.CLIENTE.Colonia_Cliente, 
                      dbo.POLIZA.Pago_Fraccionado, dbo.POLIZA.PorPago_Fraccionado, dbo.CLIENTE.Telefono AS Telcliente, dbo.POLIZA.Cobaddf2_M, '' AS nom1, '' AS dir1, 
                      '' AS poblacion1, '' AS cp_cliente1, '' AS telefono1, '' AS rfc1, '' AS calle1, '' AS noexterior1, '' AS nointerior1, '' AS direccion1, dbo.POLIZA.Vigencia_FIniP AS inicio, 
                      dbo.POLIZA.Vigencia_FFinp AS fin, dbo.CLIENTE.Calle_Cliente, dbo.CLIENTE.Ciudad_Cliente, dbo.CLIENTE.Estado_Cliente, dbo.POLIZA.Plazo, 
                      dbo.POLIZA.Descripcion, dbo.POLIZA.DerPol, dbo.POLIZA.Iva, dbo.POLIZA.Cadena_ImpresionD, dbo.POLIZA.Cadena_ImpresionV, dbo.POLIZA.PagoInicial, 
                      dbo.POLIZA.PagoSubsecuente, dbo.POLIZA.Id_TipoRecargo, dbo.ABC_TIPORECARGO.Descripcion_Periodo, dbo.POLIZA.Cobaddf1_LC, dbo.POLIZA.Cobaddf2_LC, 
                      dbo.POLIZA.Id_TipoCarga, dbo.POLIZA.Tipo_Carga, dbo.POLIZA.Modelo, dbo.DETERMINA_NUMERO_RECIBOS(dbo.POLIZA.Id_Poliza) AS norecibos, 
                      dbo.DETERMINA_RECIBO_SUBSECUENTE(dbo.POLIZA.Id_Poliza, 2) AS ReciboSub, dbo.POLIZA.Num_Pago, dbo.POLIZA.TipoProducto, dbo.POLIZA.Contador_Poliza, 
                      dbo.ABC_TIPORECARGO.Periodo, CASE per_fiscal WHEN 2 THEN isnull(razon_social, '') ELSE isnull(nombre, '') END AS NombreCliente, 
                      CASE per_fiscal WHEN 2 THEN '' ELSE isnull(apaterno, '') END AS APCliente, CASE per_fiscal WHEN 2 THEN '' ELSE isnull(amaterno, '') END AS AMCliente, 
                      dbo.CLIENTE.Ciudad_Cliente AS Ciudad, dbo.CLIENTE.Estado_Cliente AS Estado, CASE WHEN per_fiscal = 2 THEN NULL ELSE fnacimiento END AS FechaNacimiento, 
                      dbo.CLIENTE.Email, CASE per_fiscal WHEN 0 THEN 'Fisica' WHEN 1 THEN 'Fisica Act. Empresarial' ELSE 'Moral' END AS TipoPersona, 
                      
			CASE dbo.poliza.id_aseguradora 
			WHEN 58 THEN CASE WHEN dbo.abc_paquete.subramo = 1 THEN 'TA6110' ELSE 'TA5192' END 
			/* WHEN 61 THEN CASE WHEN dbo.abc_paquete.subramo = 1 THEN 'TA6110' ELSE 'TA5192' END */
			WHEN 61 THEN dbo.poliza.tarifa
			WHEN 63 THEN CASE WHEN dbo.abc_paquete.subramo = 1 THEN 'TA6110' ELSE 'TA5192' END 
			WHEN 57 THEN '27876' 
			WHEN 60 THEN '27876' 
			WHEN 62 THEN '53496022' 
			WHEN 59 THEN '53496022' 
			WHEN 56 THEN '53496022' 
			WHEN 45 THEN '' ELSE '' END AS tarifa, 

                      CASE dbo.poliza.id_aseguradora WHEN 58 THEN '029580' WHEN 61 THEN '029580' WHEN 63 THEN '029580' WHEN 57 THEN '37219' WHEN 60 THEN '37219' WHEN
                       62 THEN '52276' WHEN 59 THEN '52276' WHEN 56 THEN '52276' WHEN 45 THEN '1035' ELSE '' END AS Agente, dbo.POLIZA.Interface, dbo.POLIZA.Clave_Agente, 
                      dbo.POLIZA.Contrato, dbo.POLIZA.Caratula, ISNULL(dbo.POLIZA.TARIFA, '') AS tarifaaxa, dbo.POLIZA.Id_Uso
FROM         dbo.POLIZA INNER JOIN
                      dbo.ABC_TIPORECARGO ON dbo.POLIZA.Id_TipoRecargo = dbo.ABC_TIPORECARGO.Id_TipoRecargo INNER JOIN
                      dbo.CLIENTE_POLIZA ON dbo.POLIZA.Id_Poliza = dbo.CLIENTE_POLIZA.Id_Poliza AND dbo.POLIZA.Id_Bid = dbo.CLIENTE_POLIZA.Id_Bid INNER JOIN
                      dbo.CONTRATO ON dbo.CLIENTE_POLIZA.Id_Contrato = dbo.CONTRATO.Id_Contrato AND dbo.CLIENTE_POLIZA.Id_Cliente = dbo.CONTRATO.Id_cliente AND 
                      dbo.CLIENTE_POLIZA.Id_Bid = dbo.CONTRATO.Id_Bid INNER JOIN
                      dbo.CLIENTE ON dbo.CLIENTE_POLIZA.Id_Cliente = dbo.CLIENTE.Id_Cliente AND dbo.CLIENTE_POLIZA.Id_Bid = dbo.CLIENTE.Id_Bid INNER JOIN
                      dbo.ABC_PAQUETE ON dbo.POLIZA.Id_Paquete = dbo.ABC_PAQUETE.Id_Paquete AND dbo.POLIZA.Id_Aseguradora = dbo.ABC_PAQUETE.Id_Aseguradora INNER JOIN
                      dbo.ABC_USO ON dbo.POLIZA.Id_Uso = dbo.ABC_USO.Id_Uso AND dbo.POLIZA.Id_Aseguradora = dbo.ABC_USO.Id_Aseguradora INNER JOIN
                      dbo.VERSION ON dbo.POLIZA.Id_Version = dbo.VERSION.Id_Version AND dbo.POLIZA.Id_Aseguradora = dbo.VERSION.Id_Aseguradora INNER JOIN
                      dbo.FIRMAABAJO ON dbo.POLIZA.Id_Poliza = dbo.FIRMAABAJO.Id_poliza AND dbo.POLIZA.Id_Bid = dbo.FIRMAABAJO.Id_Bid

/*******************************************************/


CREATE PROCEDURE INSERTA_BID_USUARIO(@USUARIO INT, @BID SMALLINT, @PROGRAMA SMALLINT)
AS
BEGIN
BEGIN TRAN

	IF NOT EXISTS (SELECT ID_USUARIO FROM usuario_bid WHERE id_usuario = @USUARIO AND id_bid = @BID AND id_programa = @PROGRAMA)
	BEGIN
		insert into usuario_bid (id_usuario, id_bid, id_programa)
		values (@USUARIO,@BID,@PROGRAMA)
	END

COMMIT TRAN

IF (@@ERROR>0)
BEGIN
	RAISERROR('ERRORNULL',16,1)
	ROLLBACK TRAN
	RETURN 0
END
END






commit tran uno


-- rollback tran uno

