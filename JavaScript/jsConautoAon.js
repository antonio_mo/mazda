﻿+function ($) {
    'use strict';

    var Cotiza = function (element, options) {

        this.$element = $(element)

        this.$el = null

        this.options = $.extend({}, Cotiza.DEFAULTS, options)

    }

    Cotiza.DEFAULTS = {
        Folder: 'XMLHttpRequestASHX',
        file: 'cotizaPromo',
        ext: 'ashx',
        callBack: null,
        event: null,
        loader: null,
        dtOptions: {
            m_c: null,
            t_p: null,
            m_l: null,
            d_s: null,
            u_s: null,
            t_p_c: null,
            m_l_c: null,
            d_s_c: null,
            u_s_c: null
        },
        dtValues: {
            p: 'm_c',
            v_m_c: null,
            v_t_p: null,
            v_t_p_c: null,
            v_m_l: null,
            v_m_l_c: null,
            v_d_s: null,
            v_d_s_c: null,
            v_u_s: null,
            v_u_s_c: null,
            t_m_c: null,
            t_t_p: null,
            t_t_p_c: null,
            t_m_l: null,
            t_m_l_c: null,
            t_d_s: null,
            t_d_s_c: null,
            t_u_s: null,
            t_u_s_c: null,
            c_c: null,
            c_p: null
        }
    }



    Cotiza.prototype = {

        getData: function (callBack) {

            _a = this.options.Folder + '/' + this.options.file + '.' + this.options.ext, _b = document.location.protocol + '//' + document.location.host + '/' + document.location.pathname.split('/')[1] + '/' + _a + '?token=' + this.getUniqueId("get-info")

            $('.' + Cotiza.DEFAULTS.dtValues.p).show()

            var jqxhr = $.get(_b, Cotiza.DEFAULTS.dtValues, function (ashx_rst) {

                var rdt = null, rJson = false

                try {

                    rdt = $.parseJSON(ashx_rst)

                } catch (e) {

                    rdt = ashx_rst

                }

                rdt != null && typeof rdt === 'object' ? rJson = true : rJson = false

                if (rJson) {
                    if (typeof callBack === 'function') {

                        callBack(rdt)

                    } else if (typeof this.options.callBack === "function") {

                        this.options.callBack(rdt)

                    } else {

                        alert('Undefined result!!')

                    }
                } else {

                    alert(ashx_rst)

                }

            }).fail(function (d, s, x) { alert(d) })
        },

        getUniqueId: function (prefix) {

            var d = new Date().getTime()

            d += (parseInt(Math.random() * 100)).toString()

            if (undefined === prefix) {
                prefix = 'uid-'
            }

            d = prefix + d

            return d

        },

        ex_fn: function (opt) {

            var $this = this.$element
            var e = this.options.event

            this.getData(function (data) {

                if (typeof data === 'object' && data.msg.indexOf('satisfactoriamente') !== -1) {

                    alert(data.msg)
                }

            })

        },

        fill: function () {

            this.getData(function (data) {

                $(data).each(function (r, s) {

                    var key, sel_val

                    for (key in s) {


                        var el = $("select.cbAonCbCotiza[targetcontrolid*=':" + key + ":']").empty()

                        if (el.length > 0) {

                            var w = el.width()

                            if (s[key] !== null) {
                                $.each(s[key], function (key, value) {
                                    el.append($("<option></option>").val
                                        (value.optionValue).html(value.optionDisplay))
                                })

                                if (sel_val !== '' && sel_val.length > 0) {

                                    el.val(sel_val)

                                }
                                else {

                                    el.selectedIndex = 0

                                }
                            }

                            el.width(w + 4)


                        } else {

                            sel_val = s[key]

                        }

                    }
                })

                $('.' + Cotiza.DEFAULTS.dtValues.p).hide()

            })


        },

        setValues: function (data) {

            return this.getOpts(data)

        },

        getOpts: function (opList) {

            var options = ''

            if (opList === null || opList.length === 0) {

                options += '<option value="">-- Seleccione --</option>'

            } else {

                for (var i = 0; i < opList.length; i++) {
                    options += '<option value="' + opList[i].optionValue + '">' + opList[i].optionDisplay + '</option>'
                }

            }

            return options

        },

        action: function () {

            var $this = this.$element, el = $this.attr('targetcontrolid')



            if (el !== undefined) {

                var ctrlID = el.split(":")

                if (ctrlID.length > 1) {

                    var spt_empty = ctrlID

                    $('.' + el[1]).show()

                    var $that = $(document.getElementById(spt_empty[0])), $that_next = null
                    do {

                        Cotiza.DEFAULTS.dtValues['v_' + spt_empty[2]] = null
                        Cotiza.DEFAULTS.dtValues['t_' + spt_empty[2]] = null

                        spt_empty = $(document.getElementById($that.context.id)).attr('targetcontrolid').split(':')
                        $that = $(document.getElementById(spt_empty[2] !== '' ? spt_empty[0] : ''))

                    }
                    while ($that.length > 0)


                    if ($this[0].tagName === 'INPUT') {

                        $el = $('#' + ctrlID[0]).cotiza('ex_fn', Cotiza.DEFAULTS.dtValues)

                    } else {

                        var a = ctrlID[0], b = ctrlID[1], c = ctrlID[2]

                        var its_value = $this.val(), its_text = $this.find('option:selected').text()

                        if (its_value !== null && its_value.indexOf('Seleccione') == -1) {

                            Cotiza.DEFAULTS.dtValues['v_' + b] = its_value

                            Cotiza.DEFAULTS.dtValues['t_' + b] = its_text

                            Cotiza.DEFAULTS.loader = b

                            this.fill()

                        }

                    }
                }

            }

        }
    }

    function Plugin(option, param) {

        return this.each(function () {

            var $this = $(this)

            var data = $this.data('aon.cot')

            var attr = $this.attr('targetcontrolid').split(':')

            if (!data) $this.data('aon.cot', (data = new Cotiza(this, param)))

            if (typeof option == 'string') data[option](param)

        })

    }

    var old = $.fn.cotiza

    $.fn.cotiza = Plugin

    $.fn.cotiza.Constructor = Cotiza


    $.fn.cotiza.noConflict = function () {

        $.fn.cotiza = old

        return this

    }


    var changeHandler = function (e) {

        e.preventDefault()

        Plugin.call($(document.getElementById(e.target.id)), 'action')

    }

    $(document)
        .on('change.combos_small.cbAonCbCotiza.aon.cot', 'select.cbAonCbCotiza', changeHandler)
        .on('click.boton.btAonCbCotiza.aon.cot', 'input.btAonCbCotiza', function (e) {

            e.preventDefault()

            Plugin.call($(document.getElementById(e.target.id)), 'action', { event: e })

        })

}(jQuery);



