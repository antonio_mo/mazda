<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmpolizaAce.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmpolizaAce"%>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center" width="25%">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\logos\ace.jpg" ImageAlign="Baseline"></asp:image></TD>
					<TD width="50%"></TD>
					<TD vAlign="top" align="right" width="25%">
						<asp:Label id="lbl_titulo" runat="server" Width="100%" CssClass="negrita"></asp:Label></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="Header_tabla_centrado" colSpan="2">C E R T I F I C A D O&nbsp; D E 
									&nbsp; S E G U R O S</TD>
								<TD>
									<asp:Label id="lbl_seguro" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
								<TD>
									<asp:Label id="lbl_pol" runat="server" Visible="False">Label</asp:Label></TD>
							</TR>
							<TR>
								<TD colSpan="4" height="5"></TD>
							</TR>
							<TR>
								<TD colSpan="4">
									<asp:Label id="lbl_campo1" runat="server" Width="100%" CssClass="negrita"></asp:Label></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<asp:Label id="lbl_nombre" runat="server" Width="100%" CssClass="Header_tabla_centrado_subrallado"></asp:Label></TD>
								<TD colSpan="2"></TD>
							</TR>
							<TR>
								<TD class="linea_abajo" colSpan="4"></TD>
							</TR>
							<TR>
								<TD colSpan="4">
									<asp:Label id="lbl_campo2" runat="server" Width="100%" CssClass="negrita"></asp:Label></TD>
							</TR>
							<TR>
								<TD class="negrita">Vigencia :</TD>
								<TD class="negrita" align="left">Desde</TD>
								<TD align="left">
									<asp:Label id="lbl_fechaInicio" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD class="negrita" align="left">Hasta</TD>
								<TD align="left">
									<asp:Label id="lbl_fechaFin" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD class="negrita">Periodo de cobertura :</TD>
								<TD class="negrita" align="left">Tres meses</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD class="negrita">Regla de Suma Asegurada :</TD>
								<TD class="negrita" align="left" colSpan="2">Renta mensual del contrato de cr�dito</TD>
								<TD></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD class="negrita">Periodo de espera :</TD>
								<TD class="negrita" align="left">No aplica</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD class="negrita" align="left" height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:image id="Image1" runat="server" ImageUrl="..\Imagenes\logos\firmadesempleo.gif" ImageAlign="Baseline"></asp:image></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="Header_tabla_centrado">Director de Seguros de Personas</TD>
								<TD></TD>
								<TD class="negrita" align="right">Prima Total:</TD>
								<TD>
									<asp:Label id="lbl_primat" runat="server" Width="50%" CssClass="small"></asp:Label></TD>
							</TR>
							<TR>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
								<TD height="5"></TD>
							</TR>
							<TR>
								<TD colSpan="4">
									<asp:Label id="lbl_campo3" runat="server" Width="100%" CssClass="negrita"></asp:Label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD width="35%"></TD>
								<TD width="10%"></TD>
								<TD width="25%"></TD>
								<TD width="30%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD colSpan="2">
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="60%"></TD>
								<TD align="right" width="20%">
									<asp:imagebutton id="cmd_regresar" runat="server" ImageUrl="..\Imagenes\Botones\regresar.gif"></asp:imagebutton></TD>
								<TD align="right" width="20%">
									<asp:imagebutton id="cmd_imprimir" runat="server" ImageUrl="..\Imagenes\Botones\imprimir.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 102; LEFT: 448px; POSITION: absolute; TOP: 1160px" runat="server"></cc1:MsgBox></form>
	</body>
</HTML>
