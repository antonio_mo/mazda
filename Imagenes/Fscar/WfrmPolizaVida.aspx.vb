'..\Imagenes\logos\FordInsure.GIF
Imports CN_Negocios
Imports Emisor_Conauto

Partial Class WfrmPolizaVida
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    'EHG - Cambio de DLL
    Private Reporte As New Emisor_Conauto.inicializa_Conauto

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Not Page.IsPostBack Then
                If Not Request.QueryString("ban") Is Nothing Then
                    Session("ban") = Request.QueryString("ban")
                End If
                limpia()
                carga_informacion()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        lbl_poliza.Text = ""
        lbl_FI.Text = ""
        lbl_FF.Text = ""
        lbl_nombre.Text = ""
        lbl_dir.Text = ""
        lbl_rfc.Text = ""
        lbl_poblacion.Text = ""
        lbl_tel.Text = ""
        lbl_cp.Text = ""
        lbl_anio.Text = ""
        lbl_edad.Text = ""
        lbl_femision.Text = ""
        lbl_plazo.Text = ""
        lbl_fallecimiento.Text = ""
        lbl_prima.Text = ""
        lbl_derpol.Text = ""
        lbl_pt.Text = ""
    End Sub

    Public Sub carga_informacion()
        Dim dt As DataTable = ccliente.carga_reporte_poliza_vida(Session("idpoliza"), Session("aseguradora"))
        Try
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("polizavida") Then
                    lbl_poliza.Text = dt.Rows(0)("polizavida")
                End If
                If Not dt.Rows(0).IsNull("vigencia_inicial") Then
                    lbl_FI.Text = dt.Rows(0)("vigencia_inicial")
                End If
                If Not dt.Rows(0).IsNull("vigencia_final") Then
                    lbl_FF.Text = dt.Rows(0)("vigencia_final")
                End If
                If Not dt.Rows(0).IsNull("NOMBRE") Then
                    lbl_nombre.Text = dt.Rows(0)("NOMBRE")
                End If
                If Not dt.Rows(0).IsNull("direccion") Then
                    lbl_dir.Text = dt.Rows(0)("direccion")
                End If
                If Not dt.Rows(0).IsNull("rfc") Then
                    lbl_rfc.Text = dt.Rows(0)("rfc")
                End If
                If Not dt.Rows(0).IsNull("estado_cliente") Then
                    lbl_poblacion.Text = dt.Rows(0)("estado_cliente")
                End If
                If Not dt.Rows(0).IsNull("cp_cliente") Then
                    lbl_cp.Text = dt.Rows(0)("cp_cliente")
                End If
                If Not dt.Rows(0).IsNull("telefono") Then
                    lbl_tel.Text = dt.Rows(0)("telefono")
                End If
                If Not dt.Rows(0).IsNull("fnacimiento") Then
                    lbl_anio.Text = dt.Rows(0)("fnacimiento")
                End If
                If Not dt.Rows(0).IsNull("edad") Then
                    lbl_edad.Text = dt.Rows(0)("edad")
                End If
                If Not dt.Rows(0).IsNull("FI") Then
                    lbl_femision.Text = dt.Rows(0)("FI")
                End If
                If Not dt.Rows(0).IsNull("plazo_financiamiento") Then
                    lbl_plazo.Text = dt.Rows(0)("plazo_financiamiento")
                End If
                If Not dt.Rows(0).IsNull("cobertura") Then
                    lbl_fallecimiento.Text = Format(dt.Rows(0)("cobertura"), "$ #,###,##0.00")
                End If
                If Not dt.Rows(0).IsNull("primaneta_vida") Then
                    lbl_prima.Text = Format(dt.Rows(0)("primaneta_vida"), "$ #,###,##0.00")
                End If
                If Not dt.Rows(0).IsNull("derpol_vida") Then
                    lbl_derpol.Text = Format(dt.Rows(0)("derpol_vida"), "$ #,###,##0.00")
                End If
                If Not dt.Rows(0).IsNull("prima_vida") Then
                    lbl_pt.Text = Format(dt.Rows(0)("prima_vida"), "$ #,###,##0.00")
                End If
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmd_regresar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_regresar.Click
        Response.Redirect("..\cotizar\WfrmImp.aspx?band=2")
    End Sub

    Private Sub cmd_imprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_imprimir.Click
        Dim cadenaEnvio As String
        Try
            Session("Bandera_Espera") = 1
            Dim llave As Integer = ccliente.inserta_contenedor_reportes(Session("bid"), lbl_poliza.Text, "P")
            Session("Llave") = llave
            Session("LlaveC") = 0

            'Parametros = "6 |60|0|0|0|0|170|0|4|2"

            cadenaEnvio = Session("aseguradora") & "|" & Session("idpoliza") & "|" & _
                    "0|0|0|0|" & Session("Llave") & "|0|" & Session("programa") & "|2"

            Reporte.carga(cadenaEnvio, "P")
            Response.Redirect("..\cotizar\EsperaReportes.html")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub
End Class
