<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<%@ Register TagPrefix="rjs" Namespace="RJS.Web.WebControl" Assembly="RJS.Web.WebControl.PopCalendar" %>
<%@ Register TagPrefix="uc1" TagName="WfrmUCobertura" Src="../Cotizar/WfrmUCobertura.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCotizaFscar2.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCotizaFscar2"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
		<script language="JScript">
		function PonDiagonal()
		{
			var stCadena=document.fscar2.txt_fecha.value;
			if(stCadena.length == 2)
			{
				document.fscar2.txt_fecha.value=document.fscar2.txt_fecha.value+"/"
			}
			if(stCadena.length == 5)
			{
				document.fscar2.txt_fecha.value=document.fscar2.txt_fecha.value+"/"
			}	
		}
		
		function ckb_vida(cuenta, strpaquete)
		{
			switch(cuenta)
			{
				case "12":
					var origen = "grd_cobertura__ctl2_Vida_" + strpaquete
					break
				case "13":
					var origen = "grd_cobertura__ctl3_Vida_" + strpaquete
					break
				case "14":
					var origen = "grd_cobertura__ctl4_Vida_" + strpaquete
					break
				case "15":
					var origen = "grd_cobertura__ctl5_Vida_" + strpaquete
					break
				case "16":
					var origen = "grd_cobertura__ctl6_Vida_" + strpaquete
					break
			}
			var elementos = document.getElementById(origen)
			//alert(" Elemento: " + elementos.value + "\n Seleccionado: " + elementos.checked);
			if(elementos.checked==true)
			{
				//Palomeado
				valor =1;
			}
			else
			{
				//no Palomeado
				valor =0;
			}
			var vida = document.getElementById("txt_vida").value
			document.getElementById("txt_vida").value = creaCadena(vida,strpaquete,valor)
		}
		
		function ckb_desempleo(cuenta, strpaquete)
		{
			switch(cuenta)
			{
				case "12":
					var origen = "grd_cobertura__ctl2_Desempleo_" + strpaquete
					break
				case "13":
					var origen = "grd_cobertura__ctl3_Desempleo_" + strpaquete
					break
				case "14":
					var origen = "grd_cobertura__ctl4_Desempleo_" + strpaquete
					break
				case "15":
					var origen = "grd_cobertura__ctl5_Desempleo_" + strpaquete
					break
				case "16":
					var origen = "grd_cobertura__ctl6_Desempleo_" + strpaquete
					break
			}
			var elementos = document.getElementById(origen)
			//alert(" Elemento: " + elementos.value + "\n Seleccionado: " + elementos.checked);
			if(elementos.checked==true)
			{
				//Palomeado
				valor =1;
			}
			else
			{
				//no Palomeado
				valor =0;
			}
			
			var desempleo = document.getElementById("txt_desempleo").value
			document.getElementById("txt_desempleo").value = creaCadena(desempleo,strpaquete,valor)
		}
		
		function creaCadena(nombreVD,strpaquete,valor)
		{
			var cadena='';
			var arr1 = nombreVD.split("*");	
			for (i=0;  i<arr1.length; i++)
			{
				var arr2 = arr1[i].split("|");	
				if (arr2[0] == strpaquete)
				{
					if (cadena=='')
					{
						cadena = strpaquete +'|'+ valor
					}
					else
					{
						cadena = cadena +'*'+ strpaquete +'|'+ valor
					}
				}
				else
				{
					if (cadena=='')
					{
						cadena = arr2[0] +'|'+ arr2[1]
					}
					else
					{
						cadena = cadena +'*'+ arr2[0] +'|'+ arr2[1]
					}
				}
			}
			return cadena
		}
		
		function SoloUnValor(nombreVD,strpaquete)
		{
			var cadena='';
			var arr1 = nombreVD.split("*");	
			for (i=0;  i<arr1.length; i++)
			{
				var arr2 = arr1[i].split("|");	
				if (arr2[0] == strpaquete)
				{
					return arr2[1]
				}
			}
		}
				
		function redireccion(intaseguradora, intregion, intpaquete, intplazo, nombrepaquete,financiamiento, subramoI)
		{
			if ((document.getElementById("txt_contrato").value)=='')
			{
				alert("El numero de contrato no puede quedar en blanco,  Verifiquelo")
				document.getElementById("txt_contrato").focus();
				return 0
			}
			else
			{
				var checkOK = "0123456789"; 
				var checkStr = document.getElementById("txt_contrato").value;
				var allValid = true; 
				var decPoints = 0; 
				var allNum = ""; 
				for (i = 0; i < checkStr.length; i++) { 
					ch = checkStr.charAt(i); 
					for (j = 0; j < checkOK.length; j++) 
					if (ch == checkOK.charAt(j))
						break; 
					if (j == checkOK.length) { 
					allValid = false; 
					break; 
					} 
					allNum += ch; 
				} 
				if (!allValid) { 
					alert("Escriba s�lo d�gitos en el campo \"No. Contrato\".");
					document.getElementById("txt_contrato").focus();
					return 0
				} 
			}
			//alert(intaseguradora)
			//alert(intregion)
			//alert(intpaquete)
			//alert(intplazo)
			//alert(nombrepaquete)
			var valcost1 = document.getElementById("txt_precio").value;
			//alert(valcost1)
			var strfecha1 = document.getElementById("txt_fecha").value;
			//alert(strfecha1)
			var cveenganche1 = document.getElementById("txt_enganche").value
			//alert(cveenganche1)
			cveenganche1=valcost1.trim()*(cveenganche1/100)
			//alert(cveenganche1)
			var cvecontrato1 = document.getElementById("txt_contrato").value;
			//alert(cvecontrato1)
			var cvevida1 = document.getElementById("txt_vida").value;
			//alert(cvevida1)
			var cvedesempleo1 = document.getElementById("txt_desempleo").value;
			//alert(cvedesempleo1)
			var cvevidaU = SoloUnValor(document.getElementById("txt_vida").value,nombrepaquete)
			var cvedesempleoU = SoloUnValor(document.getElementById("txt_desempleo").value,nombrepaquete)
			var path = "WfrmCotizaFscar2.aspx?cveAseg="+intaseguradora+"&cvepaquete="+intpaquete+"&cveregion="+intregion+"&cveplazo="+intplazo+"&valcost="+valcost1+"&strfecha="+strfecha1+"&cveenganche="+cveenganche1+"&cvecontrato="+cvecontrato1+"&cvevida="+cvevida1+"&cvedesempleo="+cvedesempleo1+"&cvevidaU="+cvevidaU+"&cvedesempleoU="+cvedesempleoU+"&cvefinancia="+financiamiento+"&subramo="+subramoI+""
			//alert(path)
			window.location.href=path;
		}
		String.prototype.trim = function() 
		{
			return this.replace(',','');
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="fscar2" method="post" runat="server">
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 100; POSITION: absolute; TOP: 704px; LEFT: 408px" runat="server"></cc1:msgbox>
			<TABLE id="Table1" style="Z-INDEX: 105; POSITION: absolute; TOP: 48px; LEFT: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="Obligatorio_negro">Marca :</TD>
					<TD><asp:textbox id="txt_marca" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
					<TD class="Obligatorio_negro" align="right">Modelo :</TD>
					<TD><asp:textbox id="txt_modelo" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
					<TD vAlign="top" rowSpan="6">
						<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="3"><asp:table id="tb_pago" runat="server" Width="100%"></asp:table></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro">Tipo :</TD>
					<TD><asp:textbox id="txt_tipo" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
					<TD class="Obligatorio_negro" align="right">Descripci�n :</TD>
					<TD vAlign="top" rowSpan="2"><asp:textbox id="txt_descripcion" runat="server" Width="100%" CssClass="combos_small" TextMode="MultiLine"
							Height="50px" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro">SubTipo :</TD>
					<TD><asp:textbox id="txt_subtipo" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
					<TD class="Obligatorio_negro" align="right"></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro">Precio :</TD>
					<TD><asp:textbox id="txt_precio" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
					<TD class="Obligatorio_negro" align="right">Clasificaci�n :</TD>
					<TD><asp:textbox id="txt_clasificacion" runat="server" Width="100%" CssClass="combos_small" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD class="obligatorio" align="right">Tipo Financiamiento :</TD>
								<TD><asp:dropdownlist id="cbo_financiamiento" runat="server" Width="100%" CssClass="combos_small" Enabled="False"
										AutoPostBack="True">
										<asp:ListItem Value="-1" Selected="True">-- Seleccione --</asp:ListItem>
										<asp:ListItem Value="0">WOR</asp:ListItem>
										<asp:ListItem Value="1">MO</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD class="Obligatorio" align="right">Plazo&nbsp;Financiamiento&nbsp;:</TD>
								<TD><asp:dropdownlist id="cbo_plazo" runat="server" Width="70%" CssClass="combos_small" AutoPostBack="True"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="obligatorio" vAlign="middle" align="right">Forma de Pago&nbsp;:</TD>
								<TD vAlign="middle"><asp:radiobuttonlist id="rb_seguro" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"
										RepeatDirection="Horizontal">
										<asp:ListItem Value="0">Financiado</asp:ListItem>
										<asp:ListItem Value="1">Contado</asp:ListItem>
									</asp:radiobuttonlist></TD>
								<TD class="Obligatorio" vAlign="middle" align="right">Seguro :</TD>
								<TD vAlign="middle"><asp:dropdownlist id="cbo_seguro" runat="server" Width="60%" CssClass="combos_small" Enabled="False"
										AutoPostBack="True">
										<asp:ListItem Value="-1" Selected="True">-- Seleccione --</asp:ListItem>
										<asp:ListItem Value="1">Anual</asp:ListItem>
										<asp:ListItem Value="0">MultiAnual</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="obligatorio" vAlign="middle" align="right">Enganche: % :</TD>
								<TD vAlign="middle"><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_enganche" runat="server"
										Width="25%" CssClass="combos_small" AutoPostBack="True" MaxLength="3"></asp:textbox></TD>
								<TD class="Obligatorio_negro" vAlign="middle" align="right">Monto $ :</TD>
								<TD vAlign="middle"><asp:label id="lbl_enganche" runat="server" Width="100%" CssClass="negrita"></asp:label></TD>
							</TR>
							<TR>
								<TD class="obligatorio" vAlign="middle" align="right">No. Contrato :</TD>
								<TD vAlign="middle"><asp:textbox onkeypress="javascript:onlyDigits(event,'decOK');" id="txt_contrato" runat="server"
										Width="50%" CssClass="combos_small" MaxLength="6"></asp:textbox></TD>
								<TD class="Obligatorio_negro" vAlign="middle" align="right">Inicio Contrato&nbsp;:</TD>
								<TD vAlign="middle"><asp:textbox id="txt_fecha" onkeyup="PonDiagonal()" tabIndex="10" runat="server" Width="45%"
										CssClass="combos_small" AutoPostBack="True" MaxLength="10"></asp:textbox>&nbsp;&nbsp;
									<rjs:popcalendar id="pc_reporte" runat="server" Width="8px" Height="8px" AutoPostBack="False" ShowErrorMessage="False"
										Control="txt_fecha" Separator="/" BorderStyle="Solid" InvalidDateMessage="D�a Inv�lido" Shadow="True"
										ShowWeekend="True" Move="True" Fade="0.5" RequiredDateMessage="La Fecha es Requerida" Culture="es-MX Espa�ol (M�xico)"
										Buttons="[<][m][y]  [>]" TextMessage="La fecha es Incorrecta" BackColor="Yellow" BorderWidth="1px"
										BorderColor="Black" From-Increment="-20" From-Today="True"></rjs:popcalendar></TD>
							</TR>
							<TR>
								<TD class="obligatorio" vAlign="top" align="right">Tasa Estandar :</TD>
								<TD vAlign="top"><asp:radiobuttonlist id="rb_tasa" runat="server" Width="100%" CssClass="combos_small" AutoPostBack="True"
										RepeatDirection="Horizontal">
										<asp:ListItem Value="1">si</asp:ListItem>
										<asp:ListItem Value="2" Selected="True">no</asp:ListItem>
									</asp:radiobuttonlist></TD>
								<TD class="obligatorio" vAlign="top"></TD>
								<TD vAlign="top" class="negrita">dd/mm/yyyy</TD>
							</TR>
							<TR>
								<TD class="obligatorio" colSpan="4"><asp:datagrid id="grd_cobertura" runat="server" Width="100%" CssClass="datagrid" AllowSorting="True"
										CellPadding="1" AutoGenerateColumns="False">
										<FooterStyle Wrap="False" CssClass="Footer_azul"></FooterStyle>
										<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
										<EditItemStyle Wrap="False"></EditItemStyle>
										<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
										<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
										<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
												HeaderText="Sel." CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;" HeaderText="Del."
												CommandName="Delete">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
											</asp:ButtonColumn>
											<asp:BoundColumn Visible="False" DataField="cobertura"></asp:BoundColumn>
											<asp:BoundColumn DataField="descripcion_cob" ReadOnly="True" HeaderText="Cobertura">
												<ItemStyle Wrap="False" Width="18%"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n">
												<ItemStyle Wrap="False" Width="20%"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label1 runat="server" Width="100%" DataFormatString="{0:c}" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grddesc runat="server" CssClass="combos_small" Width="100%" DataFormatString="{0:c}" TextMode="MultiLine" Text='<%# DataBinder.Eval(Container, "DataItem.desc_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Suma Aseg.">
												<ItemStyle Wrap="False" HorizontalAlign="Right" Width="12%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label2 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdsuma runat="server" CssClass="combos_small" Width="70%" Text='<%# DataBinder.Eval(Container, "DataItem.sumaaseg_cob", "{0:c}") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="No. Fact.">
												<ItemStyle Wrap="False" HorizontalAlign="Center" Width="10%" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id=Label3 runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>Label</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox id=txt_grdfact runat="server" CssClass="combos_small" Width="70%" Text='<%# DataBinder.Eval(Container, "DataItem.numfact_cob") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="cap" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="SUMAASEG_COB"></asp:BoundColumn>
										</Columns>
										<PagerStyle Wrap="False"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="obligatorio" width="22%"></TD>
								<TD width="25%"></TD>
								<TD class="obligatorio" align="right" width="25%"></TD>
								<TD align="right" width="28%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro" colSpan="4">
						<uc1:WfrmUCobertura id="WfrmUCobertura1" runat="server"></uc1:WfrmUCobertura></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro"><asp:textbox id="txt_vida" runat="server" Width="0px" CssClass="combos_small" Height="0px"></asp:textbox></TD>
					<TD><asp:imagebutton id="cmd_regrear" tabIndex="12" runat="server" ImageUrl="..\Imagenes\botones\Regresar.gif"></asp:imagebutton></TD>
					<TD class="Obligatorio_negro"><asp:imagebutton id="cmd_cotizar" tabIndex="12" runat="server" ImageUrl="..\Imagenes\botones\cotizar.gif"></asp:imagebutton></TD>
					<TD><asp:textbox id="txt_desempleo" runat="server" Width="0px" CssClass="combos_small" Height="0px"></asp:textbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="Obligatorio_negro">
						<asp:textbox id="txt_grdcobertura" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox>
						<asp:textbox id="txt_sumcob" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox>
						<asp:textbox id="txt_ban" runat="server" CssClass="combos_small" Width="16px" Height="16px" Visible="False"></asp:textbox>
						<asp:textbox id="txt_banseguro" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox>
						<asp:textbox id="txt_gdescripcion" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox>
						<asp:textbox id="txt_grdcontrato" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False"></asp:textbox>
						<asp:textbox id="txt_grdramo" runat="server" CssClass="combos_small" Width="16px" Height="16px"
							Visible="False" DESIGNTIMEDRAGDROP="51"></asp:textbox></TD>
					<TD></TD>
					<TD class="Obligatorio_negro"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
					<TD width="15%"></TD>
					<TD width="25%"></TD>
					<TD width="20%"></TD>
				</TR>
			</TABLE>
			<TABLE id="Table8" style="Z-INDEX: 106; POSITION: absolute; HEIGHT: 24px; TOP: 8px; LEFT: 8px"
				cellSpacing="0" cellPadding="1" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" align="center" width="2%"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3">Datos del Cotizador &nbsp; </T></TD>
				</TR>
			</TABLE>
			<HR class="lineas" style="Z-INDEX: 107; POSITION: absolute; TOP: 40px; LEFT: 8px" width="100%">
		</form>
	</body>
</HTML>
