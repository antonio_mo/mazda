Imports CN_Negocios
Partial Class WfrmCotizaFscar1
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cprincipal As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Session("ClienteCotiza") = 0
        Try
            If Not Page.IsPostBack Then
                'Session("ClienteCotiza") = 0
                'determina_bandera_programa
                Dim dtB As DataTable = cprincipal.determina_bandera_programa(Session("programa"))
                If dtB.Rows.Count > 0 Then
                    If dtB.Rows(0).IsNull("Ban_Multianual") Then
                        Session("multianual") = 0
                    Else
                        Session("multianual") = dtB.Rows(0)("Ban_Multianual")
                    End If
                End If

                Session("cotiza") = 0
                carga_marca()
                limpia()
                If Not Request.QueryString("valcost") Is Nothing Then
                    rb_fscar.SelectedIndex = rb_fscar.Items.IndexOf(rb_fscar.Items.FindByValue(IIf(Session("fincon") = "F", 1, 2)))
                    'txt_zurich.Text = Session("zurich")
                    'txt_catalogo.Text = Session("catalogo")
                    viewstate("catalogo") = Session("catalogo")
                    'txt_subramo.Text = Session("subramo")
                    'txt_plan.Text = Session("plan")
                    'lbl_plaza.Text = Session("plazas")
                    txt_precio.Text = Request.QueryString("valcost")
                    cbo_marca.SelectedIndex = cbo_marca.Items.IndexOf(cbo_marca.Items.FindByValue(Session("MarcaC")))
                    cbo_carga_marca(cbo_marca.SelectedValue)
                    cbo_carga_modelo(cbo_marca.SelectedValue, Session("modelo"))
                    cbo_modelo.SelectedIndex = cbo_modelo.Items.IndexOf(cbo_modelo.Items.FindByValue(Session("modelo")))
                    cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(Session("anio")))
                    cbo_carga_descripcion()
                    cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(Session("vehiculo")))
                    datos_descripcion(cbo_descripcion.SelectedValue)
                    cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText(Session("uso")))
                End If
                SetFocus(rb_fscar)
            End If
            Session("Llave") = Nothing
            Session("LlaveC") = Nothing
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub limpia()
        'lbl_plaza.Text = ""
        txt_precio.Text = ""
    End Sub

    Public Sub carga_marca()
        Dim valor As String
        valor = estatus_auto(rb_auto)
        Dim dt As DataTable = ccliente.carga_marca(Session("intmarcabid"), Session("programa"), valor)
        Try
            If dt.Rows.Count > 0 Then
                cbo_marca.DataSource = dt
                cbo_marca.DataTextField = "marca"
                cbo_marca.DataValueField = "id_marca"
                cbo_marca.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    'Private Sub rb_auto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_auto.SelectedIndexChanged
    '    If rb_auto.Items(1).Selected Then
    '        MsgBox.ShowMessage("Esta opcion no esta habilitada")
    '        rb_auto.Items(1).Selected = False
    '        rb_auto.Items(0).Selected = True
    '    End If
    'End Sub

    Private Sub cbo_marca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_marca.SelectedIndexChanged
        If cbo_marca.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione la Marca")
            Exit Sub
        End If
        cbo_carga_marca(cbo_marca.SelectedValue)
    End Sub

    Public Sub cbo_carga_marca(ByVal intmcarca As Integer)
        Dim valor As String
        valor = estatus_auto(rb_auto)
        carga_modelo(intmcarca, valor)
    End Sub

    Public Sub carga_modelo(ByVal intmarca As Integer, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_modelo(intmarca, strtipoa, Session("programa"), 0)
        Try
            If dt.Rows.Count > 0 Then
                cbo_modelo.DataSource = dt
                cbo_modelo.DataTextField = "modelo_vehiculo"
                cbo_modelo.DataValueField = "modelo_vehiculo"
                cbo_modelo.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub SetFocus(ByVal ctrl As Control)
        Dim focusScript As String = "<script language='JavaScript'>" & _
        "document.getElementById('" + ctrl.ID & _
        "').focus();</script>"
        Page.RegisterStartupScript("FocusScript", focusScript)
    End Sub

    Private Sub cbo_modelo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_modelo.SelectedIndexChanged
        If cbo_modelo.SelectedItem.Text = "-- Seleccione --" Then
            MsgBox.ShowMessage("Seleccione el Tipo")
            Exit Sub
        End If
        limpia_campos()
        cbo_carga_modelo(cbo_marca.SelectedValue, cbo_modelo.SelectedValue)
    End Sub

    Public Sub limpia_campos()
        cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(-1))
        cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(-1))
        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByValue(-1))
        txt_precio.Text = ""
    End Sub


    Public Sub cbo_carga_modelo(ByVal intmarca As Integer, ByVal intmodelo As String)
        Dim valor As String
        valor = estatus_auto(rb_auto)
        carga_anio(intmarca, intmodelo, valor)
    End Sub

    Public Sub carga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_anio(intmarca, strmodelo, strtipoa, Session("programa"), 0)
        Try
            If dt.Rows.Count > 0 Then
                cbo_anio.DataSource = dt
                cbo_anio.DataTextField = "anio_vehiculo"
                cbo_anio.DataValueField = "anio_vehiculo"
                cbo_anio.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_anio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_anio.SelectedIndexChanged
        If cbo_anio.SelectedItem.Text = "-- Seleccione --" Then
            MsgBox.ShowMessage("Seleccione el a�o del vehiculo")
            Exit Sub
        End If
        cbo_carga_descripcion()
        'carga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, valor)
    End Sub

    Public Sub cbo_carga_descripcion()
        Dim valor As String
        valor = estatus_auto(rb_auto)
        carga_descripcion(cbo_marca.SelectedValue, cbo_modelo.SelectedValue, cbo_anio.SelectedValue, valor)
    End Sub

    Public Sub carga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal strtipoa As String)
        Dim dt As DataTable = ccliente.carga_descripcion(intmarca, strmodelo, stranio, strtipoa, Session("programa"), 0)
        Try
            If dt.Rows.Count > 0 Then
                cbo_descripcion.DataSource = dt
                cbo_descripcion.DataTextField = "descripcion_vehiculo"
                cbo_descripcion.DataValueField = "id_vehiculo"
                cbo_descripcion.DataBind()
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cbo_descripcion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbo_descripcion.SelectedIndexChanged
        If cbo_descripcion.SelectedValue = 0 Then
            MsgBox.ShowMessage("Seleccione la descripci�n del vehiculo")
            Exit Sub
        End If
        datos_descripcion(cbo_descripcion.SelectedValue)
    End Sub

    Public Sub datos_descripcion(ByVal intdescripcion As Integer)
        Dim valor As String

        Dim dth As DataTable = ccliente.carga_subramo_homologado(intdescripcion, estatus_auto(rb_auto), Session("programa"), 0, 0)
        If dth.Rows.Count > 0 Then
            viewstate("subramo") = ccliente.subramo
            viewstate("catalogo") = ccliente.Catalogo
        Else
            viewstate("subramo") = 0
            viewstate("catalogo") = 0
        End If


        valor = estatus_auto(rb_auto)

        Dim dt As DataTable = ccliente.carga_uso(Session("programa"), cbo_descripcion.SelectedValue, valor, viewstate("subramo"), Session.SessionID)
        If dt.Rows.Count > 0 Then
            cbo_uso.DataSource = dt
            cbo_uso.DataTextField = "uso"
            cbo_uso.DataValueField = "id_uso"
            cbo_uso.DataBind()
            If Session("programa") = 5 Or Session("programa") = 9 Then
                cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByText("Pasticular"))
            End If
        End If
    End Sub

    Public Function estatus_auto(ByVal rb As RadioButtonList) As String
        Dim tipo As String
        Dim i As Integer

        For i = 0 To rb.Items.Count - 1
            If rb.Items(0).Selected Then
                tipo = "N"
            Else
                tipo = "U"
            End If
            Exit For
        Next
        Return tipo
    End Function

    Private Sub txt_precio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_precio.TextChanged
        Dim total As Double
        Try
            If txt_precio.Text = "" Then
                txt_precio.Text = ""
            Else
                total = cprincipal.valida_cadena(txt_precio.Text)
                txt_precio.Text = Format(total, "##,##0.00")
            End If
            SetFocus(cmd_continuar)

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Function personalidad(ByVal rb As RadioButtonList) As Integer
        Dim i As Integer
        Dim valor As Integer
        Try
            For i = 0 To rb.Items.Count - 1
                If rb.Items(i).Selected Then
                    valor = rb.Items(i).Value
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Function

    Private Sub cmd_continuar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_continuar.Click
        Dim i As Integer = 0
        Dim bandera As Integer = 0
        Dim banoption As Integer = 0

        For i = 0 To rb_fscar.Items.Count - 1
            If rb_fscar.Items(i).Selected = True Then
                banoption = rb_fscar.Items(i).Value
                Exit For
            End If
        Next
        If banoption = 0 Then
            MsgBox.ShowMessage("Favor de seleccionar el tipo de venta")
            Exit Sub
        End If
        If cbo_marca.SelectedValue = 0 Then
            MsgBox.ShowMessage("Favor de seleccionar la Marca")
            Exit Sub
        End If
        If cbo_modelo.SelectedValue = "-- Seleccione --" Then
            MsgBox.ShowMessage("Favor de seleccionar el Tipo de Vehiculo")
            Exit Sub
        End If
        If cbo_anio.SelectedValue = "-- Seleccione --" Then
            MsgBox.ShowMessage("Favor de seleccionar el A�o del Vehiculo ")
            Exit Sub
        End If
        If cbo_descripcion.SelectedValue = "0" Then
            MsgBox.ShowMessage("Favor de seleccionar la Descripci�n del Vehiculo")
            Exit Sub
        End If
        If cbo_uso.SelectedValue = 0 Then
            MsgBox.ShowMessage("Favor de seleccionar el Uso del Vehiculo")
            Exit Sub
        End If
        If txt_precio.Text = "" Then
            MsgBox.ShowMessage("Favor de escribir el monto del Vehiculo")
            Exit Sub
        End If
        If txt_precio.Text <= 0 Then
            MsgBox.ShowMessage("El monto del Vehiculo debe de ser mayo a 0 ")
            Exit Sub
        End If
        For i = 0 To rb_tipo.Items.Count - 1
            If rb_tipo.Items(i).Selected = True Then
                bandera = 1
                Exit For
            End If
        Next
        If bandera = 0 Then
            MsgBox.ShowMessage("Favor de seleccionar el Tipo de Contribuyente")
            Exit Sub
        End If

        ''''Session("plazas") = lbl_plaza.Text
        Session("vehiculo") = cbo_descripcion.SelectedValue
        Session("vehiculoDesc") = cbo_descripcion.SelectedItem.Text
        'Session("subramo") = txt_subramo.Text
        Session("subramo") = viewstate("subramo")
        Session("anio") = cbo_anio.SelectedItem.Text
        Session("modelo") = cbo_modelo.SelectedValue
        ''''Session("plan") = txt_plan.Text
        Session("uso") = cbo_uso.SelectedItem.Text
        Session("intuso") = cbo_uso.SelectedValue
        'Session("catalogo") = txt_catalogo.Text
        Session("catalogo") = IIf(viewstate("catalogo") = "", "0", viewstate("catalogo"))
        Session("EstatusV") = estatus_auto(rb_auto)
        ''''Session("zurich") = txt_zurich.Text
        Session("MarcaC") = cbo_marca.SelectedValue
        Session("MarcaDesc") = cbo_marca.SelectedItem.Text
        Session("contribuyente") = personalidad(rb_tipo)
        ''''Session("opcsa") = "F"
        If banoption = 1 Then
            Response.Redirect("WfrmCotizaFscar2.aspx?valcost=" & cprincipal.valida_cadena(txt_precio.Text) & "")
        Else
            Response.Redirect("..\cotizar\WfrmCotiza1.aspx?valcost=" & cprincipal.valida_cadena(txt_precio.Text) & "")
        End If
    End Sub

    Private Sub rb_auto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_auto.SelectedIndexChanged
        carga_marca()
        cbo_marca.SelectedIndex = cbo_marca.Items.IndexOf(cbo_marca.Items.FindByValue(0))
        cbo_modelo.SelectedIndex = cbo_modelo.Items.IndexOf(cbo_modelo.Items.FindByValue(0))
        cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(0))
        cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(0))
        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByValue(0))
        txt_precio.Text = ""
        If rb_auto.Items(1).Selected Then
            MsgBox.ShowMessage("Esta opcion no esta habilitada")
            rb_auto.Items(1).Selected = False
            rb_auto.Items(0).Selected = True
        End If
    End Sub

    Private Sub rb_fscar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_fscar.SelectedIndexChanged
        Dim i As Integer = 0
        Dim valor As Integer = 0
        For i = 0 To rb_fscar.Items.Count - 1
            If rb_fscar.Items(i).Selected Then
                Session("fincon") = IIf(rb_fscar.Items(i).Value = 1, "F", "C")
                Exit For
            End If
        Next
        carga_marca()
        rb_auto.Items(0).Selected = True
        rb_auto.Items(1).Selected = False
        cbo_marca.SelectedIndex = cbo_marca.Items.IndexOf(cbo_marca.Items.FindByValue(0))
        cbo_modelo.SelectedIndex = cbo_modelo.Items.IndexOf(cbo_modelo.Items.FindByValue(0))
        cbo_anio.SelectedIndex = cbo_anio.Items.IndexOf(cbo_anio.Items.FindByValue(0))
        cbo_descripcion.SelectedIndex = cbo_descripcion.Items.IndexOf(cbo_descripcion.Items.FindByValue(0))
        cbo_uso.SelectedIndex = cbo_uso.Items.IndexOf(cbo_uso.Items.FindByValue(0))
        txt_precio.Text = ""
    End Sub
End Class
