﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Cotizar
{
    public class Cotizacion : General.Core
    {
        public Modelo.Entities.ABC_ESTADO GetStateByZipCode(string strZipCode)
        {
            var objEstado = (from z in _conautoEntities.ABC_ZONA_CP
                               join e in _conautoEntities.ABC_ESTADO on z.Id_Estado equals e.Id_Estado
                               where z.Codigo_Postal == strZipCode
                               select e).FirstOrDefault();

            return objEstado;
        }
    }
}
