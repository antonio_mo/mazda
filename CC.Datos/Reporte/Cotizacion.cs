﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Reporte
{
    public class Cotizacion : General.Core
    {
        public Modelo.Reporte.Cotizacion.CotizacionUnica GetCotizacionUnica(int iIdCotizacionUnica)
        {
            var objCotizacion = (from cu in _conautoEntities.COTIZACION_UNICA
                                 join h in _conautoEntities.HOMOLOGACION on cu.Id_Aonh equals h.Id_AonH
                                 join b in _conautoEntities.BID on cu.Id_bid equals b.Id_Bid
                                 where cu.Id_CotizacionUnica == iIdCotizacionUnica
                                 select new { cu, h.Descripcion_Homologado, b.Empresa }).AsEnumerable().Select(x => new Modelo.Reporte.Cotizacion.CotizacionUnica() {
                                     Promocion = CC.General.Funciones.ValidacionesPol.GetValueInt(x.cu.Exclusion, 0, (int)x.cu.Exclusion),
                                     Promocion12Meses = CC.General.Funciones.ValidacionesPol.GetValueInt(x.cu.Id_Cotizacion12meses, 0, x.cu.Id_Cotizacion12meses),
                                     Anio = x.cu.Anio,
                                     Anio_AutoCont = x.cu.Anio_AutoCont,
                                     Cuotas_Anticipadas = x.cu.Cuotas_Anticipadas,
                                     DatosCliente = x.cu.DatosCliente,
                                     Diferencia = x.cu.Diferencia,
                                     Estatus_Auto = x.cu.Estatus_Auto,
                                     Exclusion = x.cu.Exclusion,
                                     Fecha_a_Cancelar = x.cu.Fecha_a_Cancelar,
                                     Fecha_Cancelada = x.cu.Fecha_Cancelada,
                                     Fecha_Confirmacion = x.cu.Fecha_Confirmacion,
                                     Fecha_Entrega = x.cu.Fecha_Entrega,
                                     FechaEmision = x.cu.FechaEmision,
                                     FechaRegistro = x.cu.FechaRegistro,
                                     GrupoIntegrante = x.cu.GrupoIntegrante,
                                     Id_Aonh = x.cu.Id_Aonh,
                                     Id_AonhAutoCont = x.cu.Id_AonhAutoCont,
                                     Id_bid = x.cu.Id_bid,
                                     Id_Cotizacion12meses = x.cu.Id_Cotizacion12meses,
                                     Id_Cotizacion12mesesC = x.cu.Id_Cotizacion12mesesC,
                                     Id_CotizacionPanel = x.cu.Id_CotizacionPanel,
                                     Id_CotizacionUnica = x.cu.Id_CotizacionUnica,
                                     Id_Marca = x.cu.Id_Marca,
                                     Id_Usuario = x.cu.Id_Usuario,
                                     Id_Programa = x.cu.Id_Programa,
                                     IdConfirmacion = x.cu.IdConfirmacion,
                                     IdEmision = x.cu.IdEmision,
                                     Meses_SeguroGratis = x.cu.Meses_SeguroGratis,
                                     Meses_Transcurridos = x.cu.Meses_Transcurridos,
                                     Modelo = x.cu.Modelo,
                                     Modelo_AutoCont = x.cu.Modelo_AutoCont,
                                     NoSerie = x.cu.NoSerie,
                                     num_contrato = x.cu.num_contrato,
                                     Observaciones = x.cu.Observaciones,
                                     Plazo = x.cu.Plazo,
                                     RestoPlazo = x.cu.RestoPlazo,
                                     Suma_Asegurada = x.cu.Suma_Asegurada,
                                     Suma_aseguradaAutoCont = x.cu.Suma_aseguradaAutoCont,
                                     TipoPersona = x.cu.TipoPersona,
                                     UrlCotizacion = x.cu.UrlCotizacion,
                                     DescripcionBID = x.Empresa,
                                     DescripcionHomo = x.Descripcion_Homologado
                                 }).FirstOrDefault();
            
            return objCotizacion;
        }

        public List<Modelo.Reporte.Cotizacion.Aseguradora> GetAseguradorasCotiza(int iIdCotizacionPanel)
        {
            var lstAseguradora = (from cp in _conautoEntities.COTIZACION_PANEL
                                  join tr in _conautoEntities.ABC_TIPORECARGO on cp.Id_TipoRecargo equals tr.Id_TipoRecargo
                                  join p in _conautoEntities.ABC_PAQUETE on cp.IdPaquete equals p.Id_Paquete
                                  join a in _conautoEntities.ABC_ASEGURADORA on cp.Id_Aseguradora equals a.Id_Aseguradora
                                  where cp.Id_CotizacionPanel == iIdCotizacionPanel
                                  group a by new { a.Id_Aseguradora, a.Descripcion_Aseguradora, a.Url_ImagenPeque, tr.Id_TipoRecargo, tr.Descripcion_Periodo, cp.IdPaquete, p.Descripcion_Paquete,
                                      tr.Orden, cp.PrimaConsecutiva, cp.PrimaTotal, cp.PrimaConsecutiva2 } into gp
                                  orderby gp.Key.Id_Aseguradora
                                  select new Modelo.Reporte.Cotizacion.Aseguradora()
                                  {
                                      IdAseguradora = gp.Key.Id_Aseguradora,
                                      DescripcionAseguradora = gp.Key.Descripcion_Aseguradora,
                                      UrlImage = gp.Key.Url_ImagenPeque,
                                      IdRecargo = gp.Key.Id_TipoRecargo,
                                      DescripcionRecargo = gp.Key.Descripcion_Periodo,
                                      IdPaquete = gp.Key.IdPaquete,
                                      DescripcionPaquete = gp.Key.Descripcion_Paquete,
                                      orden = gp.Key.Orden,
                                      PrimerRecibo = gp.Key.PrimaConsecutiva,
                                      ReciboSubsecuente = gp.Key.PrimaConsecutiva2,
                                      PrimaTotal = gp.Key.PrimaTotal
                                  }).ToList();

            return lstAseguradora;
        }

        public bool ActualizaCotizacion(string strPDF, DateTime dateFechaCancelar, int iIdCotizacionUnica)
        {
            bool bSuccess = false;
            var objCotizacion = (from cu in _conautoEntities.COTIZACION_UNICA
                                    where cu.Id_CotizacionUnica == iIdCotizacionUnica
                                    select cu).FirstOrDefault();

            if (objCotizacion != null)
            {
                objCotizacion.UrlCotizacion = strPDF;
                objCotizacion.Fecha_a_Cancelar = dateFechaCancelar;
                _conautoEntities.SaveChanges();
                bSuccess = true;
            }

            return bSuccess;
        }                
    }
}
