﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Reporte
{
    public class Polizas : General.Core
    {
        public Modelo.Entities.VW_REPORTE_POLIZA_CONTADO GetData(int iIdPoliza)
        {
            var objPoliza = (from vp in _conautoEntities.VW_REPORTE_POLIZA_CONTADO
                             where vp.Id_Poliza == iIdPoliza
                             select vp).FirstOrDefault();

            return objPoliza;
        }

        public Modelo.Solicitudes.Poliza GetPol(string strPoliza, int iIdBid)
        {
            var objPoliza = (from p in _conautoEntities.POLIZA
                             join a in _conautoEntities.ABC_ASEGURADORA on p.Id_Aseguradora equals a.Id_Aseguradora
                             join pc in _conautoEntities.CLIENTE_POLIZA on p.Id_Poliza equals pc.Id_Poliza
                             join c in _conautoEntities.CONTRATO on new { pc.Id_Bid, pc.Id_Cliente } equals new { c.Id_Bid, Id_Cliente = c.Id_cliente }
                             join cl in _conautoEntities.CLIENTE on new { pc.Id_Bid, pc.Id_Cliente } equals new { cl.Id_Bid, cl.Id_Cliente }
                             where p.Poliza1 == strPoliza && p.Id_Bid == iIdBid
                             select new Modelo.Solicitudes.Poliza() { Id = p.Id_Poliza, Numero = p.Poliza1, IdBid = p.Id_Bid, Aseguradora = a.Descripcion_Aseguradora, NumeroSerie = p.Serie, Contrato = c.Num_Contrato,
                                                                      NombreCliente = cl.Nombre, APaternoCliente = cl.APaterno, AMaternoCliente = cl.AMaterno }).FirstOrDefault();

            return objPoliza;
        }

        public bool ActualizaContenedor(int iCveReporte, string strNamePdf)
        {
            bool bResult = false;
            var objPoliza = (from cr in _conautoEntities.CONTENEDOR_REPORTE
                             where cr.Cve_Reporte == iCveReporte
                             select cr).FirstOrDefault();

            if (objPoliza != null)
            {
                objPoliza.Url_Reporte = CC.General.Diccionario.Carpetas.POLIZA_PDF + strNamePdf;
                objPoliza.Nombre_Reporte = strNamePdf;
                objPoliza.Estatus = "1";
                _conautoEntities.SaveChanges();
                bResult = true;
            }

            return bResult;
        }

        public List<Modelo.Entities.POLIZA_RECIBO> GetRecibos(int iIdPoliza)
        {
            var lsrRecibos = (from r in _conautoEntities.POLIZA_RECIBO
                              where r.Id_Poliza == iIdPoliza
                              select r).ToList();

            return lsrRecibos;
        }
    }
}
