﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Reporte
{
    public class Coberturas : General.Core
    {
        public List<Modelo.Reporte.Polizas.Coberturas> GetData(int iIdPoliza)
        {
            var lstCoberturas = (from l in _conautoEntities.LIMRESP
                                 join p in _conautoEntities.POLIZA on new { l.Id_Aseguradora, l.Id_Version, l.Id_Paquete } equals new { p.Id_Aseguradora,
                                     Id_Version = p.Id_Version.Value, Id_Paquete = p.Id_Paquete.Value }
                                 join a in _conautoEntities.ABC_ASEGURADORA on l.Id_Aseguradora equals a.Id_Aseguradora
                                 join pc in _conautoEntities.POLIZA_COBERTURA on new { l.Id_LimResp, Id_Poliza = iIdPoliza } equals new { pc.Id_LimResp, pc.Id_Poliza }
                                 where p.Id_Poliza == iIdPoliza
                                 select new Modelo.Reporte.Polizas.Coberturas() { Cobertura = l.Cobertura, Deducible = l.Deducible, Descripcion_Suma = l.Descripcion_Suma,
                                     PrimaCobertura = pc.PRIMACOBERTURA }).ToList();

            return lstCoberturas;
        }

        public bool ValidaQualitasRiesgo(string strCodigoPostal, string strModelo)
        {
            var iConteo = (from r in _conautoEntities.ABC_GrupoRiesgo
                           join cp in _conautoEntities.ABC_ZONA_CP on r.Id_Estado equals cp.Id_Estado
                           where cp.Codigo_Postal == strCodigoPostal && r.GrupoModelo == strModelo
                             select r).Count();

            return iConteo > 0;
        }
    }
}
