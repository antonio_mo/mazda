﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace CC.Datos.General
{
    public class Conexion
    {
        public DataTable EjecutaConsulta(string strSQL)
        {
            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection(
                CC.General.Configuraciones.Seguridad.Decrypt(System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.CONN_STRING].ToString()));
            if (con.State != System.Data.ConnectionState.Closed)
                con.Open();

            SqlCommand cmd = new SqlCommand(strSQL, con);
            cmd.CommandType = System.Data.CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            if (con.State != System.Data.ConnectionState.Closed)
                con.Close();

            return ds.Tables[0];
        }
    }
}
