﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CC.Datos.General
{
    public class S_Core
    {
        public static CC.Modelo.Entities.DAIMLER_QAEntities _daimlerEntity;

        static S_Core()
        {
            _daimlerEntity = new CC.Modelo.Entities.DAIMLER_QAEntities(
                "metadata=res://*/Daimler.csdl|res://*/Daimler.ssdl|res://*/Daimler.msl;provider=System.Data.SqlClient;provider connection string=\"" + 
                CC.General.Configuraciones.Seguridad.Decrypt(
                System.Configuration.ConfigurationManager.AppSettings[CC.General.Diccionario.AppSettings.CONN_STRING].ToString()) +
                "\"");
        }
    }
}
