﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model = CC.Modelo;

namespace CC.Datos.Administracion
{
    public class Usuarios : General.Core
    {
        public Modelo.Entities.USUARIO GetData(int iIdUser)
        {
            var objUser = (from u in _conautoEntities.USUARIO
                           where u.Id_Usuario == iIdUser
                           select u).FirstOrDefault();

            return objUser;
        }        
    }
}