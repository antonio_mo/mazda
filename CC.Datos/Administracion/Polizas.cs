﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Administracion
{
    public class Polizas : General.Core
    {
        public List<Modelo.Administracion.PolizaDG> Listar(string strPoliza)
        {
            var lstPolizas = (from p in _conautoEntities.POLIZA
                              where p.Poliza1.Contains(strPoliza)
                              select new Modelo.Administracion.PolizaDG() { IdPoliza = p.Id_Poliza, Poliza = p.Poliza1 }).ToList();

            return lstPolizas;
        }

        public Modelo.Entities.POLIZA Get(int iIdPoliza)
        {
            var objPoliza = (from p in _conautoEntities.POLIZA
                              where p.Id_Poliza == iIdPoliza
                              select p).FirstOrDefault();

            return objPoliza;
        }
    }
}
