﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CC.Datos.Administracion
{
    public class BID : General.Core
    {
        public Modelo.Entities.BID GetBid(int iIdBid)
        {
            Modelo.Entities.BID objBId = (from b in _conautoEntities.BID
                          where b.Id_Bid == iIdBid
                          select b).FirstOrDefault();

            return objBId;
        }
    }
}
