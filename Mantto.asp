<%
'        Copyright(C) 2001 Ingenier�a Aplicada, All rights reserved
'        System:  AON ALTAS Y BAJAS
'        Program: Login.asp
'        Contents: 
'        Objetives: Obtener los datos que se validaran en el servidor para permitir o no el acceso al sistema.
'        Autor : Antonio Alcantara
'        Changer: Angel Gonz�lez Serrano
'        Creation: 15 de Enero de 2003
'        Changed: 15 de Enero de 2003
'
'		 Changer: Jos� Gregorio Hernandez Trejo (Softtek)
'		 Changed: 27 de Diciembre de 2012
%>
<!-- Copyright(C) 2001 Ingenier�a Aplicada, All rights reserved  -->
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="X-UA-Compatible" content="chrome=1" />

<TITLE>Conauto</TITLE>
<LINK REL="SHORTCUT ICON" HREF="http://www.aon.com/favicon.ico">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

<SCRIPT language="JavaScript">
<!--
// Esta funci�n verifica que los campos no esten vac�os para poder ejecutar la forma.
function fnVerify(){
	if ((fnLogin.txtUserName.value == "") || (fnLogin.pwdPassword.value == ""))	{
		window.open ('Message.asp?Message=Error;Debe proporcionar todos los datos.', 'MessageWindow','resizable=no,width=300,height=200', false)
	}else{
		window.document.fnLogin.submit();
	}
}
//-->
</SCRIPT>
		<LINK rel="stylesheet" type="text/css" href="cssMantto/default.css">
		<LINK rel="stylesheet" type="text/css" href="cssMantto/block.css" media="screen">
		<LINK rel="stylesheet" type="text/css" href="cssMantto/button.css" media="screen">
		<!--link rel="stylesheet" href="css/styles.css" type="text/css"-->
</HEAD>
<BODY >

<div align="center">
	
	<div id="global-header-block" align="center"><A class="aon-logo" href="#"><IMG src="img/aon-logo.png" alt="Aon" width="106" height="45"></A>
		<ul class="links">
			<li class="first"><a href="Login.asp">
				<img src="img/home.png" alt="Inicio" title="Inicio"
					class="world" align="top" /></a> &nbsp;&nbsp;&nbsp;<a href="http://www.aon.com/site/aonworldwide.jsp"><img 

src="img/world.png" alt="" class="world" /></a></li>
			<li><a href="http://www.aon.com/site/aonworldwide.jsp">Aon Global</a></li>
			<li class="last"><a href="http://www.aon.com/mexico/about-aon/empleos.jsp">Carreras</a></li>
		</ul>
		
		<div class="titulo">Conauto</div>
		
		<div class="slogan">			
			<a href="http://www.aon.com/mexico/products-and-services/risk-services/riesgos.jsp">Riesgos</a>
			&nbsp;&nbsp;
			<a href="http://www.aon.com/reinsurance/default.jsp">Reaseguro</a>
			&nbsp;&nbsp;
			<a href="http://www.aon.com/mexico/products-and-services/human-capital-consulting/consultoria.jsp">Recursos Humanos</a>			

								</div>

		<div id="simple-navigation--"> 
			<ul id="simple-navigation"></ul>
		</div>

	</div>
	
	
	<!--
	<form name= "fnLogin" method="post" action="Authentication.asp" autocomplete="Off">
		<table align="center" border="0" cellSpacing="0" cellPadding="0" width="940">
			<tr>
				<td align="center"><img src="img/imagenppal.jpg" alt="" width="940" height="263"></td>
			</tr>
		</table>
		
		<table border="0"  align = "center" cellSpacing="0" cellPadding="0" width="940">
			<tr>
				<td width="70">&nbsp;</td>
				<td>&nbsp;</td>
				<td height="30">&nbsp;</td>
				<td width="190" height="30">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left" rowspan=2>La p�gina web de Altas y Bajas ha sido actualizada, por favor ingrese con su usuario y contrase�a habituales.</td>
				<td height="30" align="right"><span id="Anthem_lblUsuario__"><span id="lblUsuario" class="settings-login">Usuario:&nbsp;</span></span></td>
				<td height="30"><input name="txtUserName" type="text" id="txtUserName" autocomplete="Off" tabindex="1" class="Texto" style="width:190px;" value="<%=Clave%>" /></td>
			</tr>
			<tr>				
				<td>&nbsp;</td>				
				<td height="30" width="200" align="right"><span id="Anthem_lblpassword__"><span id="lblpassword" class="settings-login">Contrase&ntilde;a:&nbsp;</span></span></td>
				<td height="30"><input name="pwdPassword" type="password" id="pwdPassword" autocomplete="Off" tabindex="2" class="Texto" style="width:190px;" value="<%=Apellido%>" /></td>
			</tr>
			<tr>
				<td align="right">&nbsp;</td>
				<td>&nbsp;</td>
				<td align="right">&nbsp;</td>
				<td align="right"><input type="submit" name="lblaceptar" value="Entrar" id="lblaceptar" tabindex="3" class="pageBtns" onClick="javascript: fnVerify(this.form);" style="background-color:White;border-color:Transparent;Z-INDEX: 0" /></td>
			</tr>
		</table>
	</form>	
	-->
		
		<!--Letrero para cuando la Pagina se encuentre en Mantenimiento-->
			<div>	
				<table border="0" cellSpacing="0" cellPadding="0" width="950">
					<tr align="center">
						<td><FONT SIZE=4 COLOR="Red">La p�gina se encuentra en mantenimiento.</font></td>
					</tr>					
					<tr align="center">
						<td align="right">&nbsp;</td>
					</tr>
					<tr align="center">
						<td><FONT SIZE=4 COLOR="Blue" align="Center">Disculpe las molestias.</font></td>
					</tr>
					<tr align="center">
						<td><FONT SIZE=4 COLOR="Blue" align ="Center">Trabajamos para brindarle un mejor servicio. </font></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>	
				</table>		
			</div>
			<div>
				<img src="images/Mantenimiento.jpg" width="200" height="250">
			</div>
		<!--FIN del Letrero para cuando la Pagina se encuentre en Mantenimiento-->
	
	<div id="global-footer-block" class="clearfix">
				<div class="buttons icons-block"><a href="http://www.facebook.com/AonCorp"><IMG src="img/icon_facebook.gif" 

alt="Facebook" width="23" height="23">
					</a><a href="http://www.linkedin.com/company/2041"><IMG src="img/icon_linkedin.gif" alt="LinkedIn" 

width="23" height="23">
					</a><a href="http://www.twitter.com/AonCorp"><IMG src="img/icon_twitter.gif" alt="Twitter" width="23" 

height="23" class="last">
					</a>					
				</div>
				
				<div class="row">
					<ul class="links">
						<li class="first">
							<A href="http://www.aon.com/mexico/">Inicio</A>
						<li>
							<A href="http://www.aon.com/mexico/">Productos y Servicios</A>
						<li>
							<A href="#">Relaciones con Inversores</A>
						<li>
							<A href="http://www.aon.com/mexico/about-aon/empleos.jsp">Carreras</A>
						<li>
							<A href="http://www.aon.com/mexico/about-aon/politica-de-privacidad.jsp">Politica de 

Privacidad</A>
						<li>
							<A href="http://www.aon.com/about-aon/legal-notice.jsp">Legal</A>
						<li class="last">
							<A href="http://www.aon.com/mexico/sitemap.jsp">Mapa del Sitio</A>				

		</li>
					</ul>
				</div>
									
				<div  class="copyright">
					<ul class="links">
						<li class="first">
							Copyright 2012 � 						
						<li>
							<A href="http://www.aon.com/mexico/about-aon/aviso-de-privacidad-ARS.jsp">Aviso de 

Privacidad</A>
						<li>
							T�rminos y Condiciones
						<li class="last">
							 Aon M�xico es parte de Aon Corporation situada en Chicago Illinois, E.U.A. Aon es el 

intermediario de seguros m�s grande del mundo con presencia en 120 pa�ses, 500 oficinas y 60,000 asesores. <b>Somos asesores en seguros, fianzas, 

beneficios para empleados, retiro, talento y compensaciones.</b>							</li>
					</ul>
				</div>
		</div>
</div>

</BODY>
</HTML>