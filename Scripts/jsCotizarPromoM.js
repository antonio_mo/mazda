﻿$(document).ready(function () {
    $('#btnCalPagos').on('click', function () {
        if (validaInfo()) {
            CalPagos();
        }
    });
});

function CalPagos() {

    contextPath = document.getElementById("pathContext").value + "Cotizar/WfrmCotizaPromoM.aspx/cmdCalPagos";

    alert('Recuerda que debes imprimir y firmar tu cotización');

    $.ajax({
        type: 'POST',
        url: contextPath,
        data: "",
        contentType: 'application/json; charset=utf-8',
        async: true,
        dataType: 'json',
        success: function (data) {
            alert(data.d);
        },
        error: function (xhr, status, err) {
            alert(err.Message);
        }
    });

    return false;
}

function validaInfo() {

    var cboPlazo = $('#cbo_plazo').val();
    var txtPlazoRest = $('#txt_plazosrest').val();

    if (cboPlazo === '-- Seleccione --') {
        alert('No hay información para cotizar');
    }
    if (txtPlazoRest === '') {
        alert('No hay información para cotizar');
    }

}