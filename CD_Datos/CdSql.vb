Imports System.Data.SqlClient
Imports System.Data.SqlDbType

Public Class CdSql

    Private Shared StrConexion As String = j.GAM.Utility.CCrypto.Decrypt(Configuration.ConfigurationManager.AppSettings("ConautoConn"), "Pas5pr@se", "s@1tValue", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256) '"server=192.168.176.195,8083;database=AONSEGUROS_FORD_CONAUTO;uid=usrEmisorConauto;password=kODWwcO@swMdZSQ;Connection Timeout=600" '= "server=localhost;database=AONSEGUROS_FORD_CONAUTO;uid=sa;password=sa;Connection Timeout=600"
    Private Shared cnnBase As SqlConnection
    Private Shared cnnTran As SqlTransaction

#Region "Procesos de la DB"

    Private Sub AsignaBase()

        StrConexion = j.GAM.Utility.CCrypto.Decrypt(Configuration.ConfigurationManager.AppSettings("ConautoConn"), "Pas5pr@se", "s@1tValue", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256)

        'Produccion RS
        'StrConexion = "server=192.168.77.228;database=AONSEGUROS_FORD_CONAUTO;uid=usrEmisorConauto;password=;"

        'QA RS
        'StrConexion = "server=192.168.176.195;database=AONSEGUROS_FORD_CONAUTO;uid=usrEmisorConauto;password=;"

        'DESARROLLO RS
        'StrConexion = "server=192.168.176.195,8083;database=AONSEGUROS_FORD_CONAUTO;uid=usrEmisorConauto;password=kODWwcO@swMdZSQ;Connection Timeout=600"

        'StrConexion = "server=localhost;database=AONSEGUROS_FORD_CONAUTO;uid=sa;password=sa;Connection Timeout=600"

        'LOCAL
        'StrConexion = "server=(local);database=AonSeguros_ford_Conauto;uid=ALD;password=ALDFORDAON2007;"

    End Sub

    Public Function AbrirConsulta(ByVal StrSql As String, Optional ByVal DataTable As Boolean = False, Optional ByVal ConectarBase As Boolean = True, Optional ByVal DesConectarBase As Boolean = True, Optional ByVal ConsultaTrans As Boolean = False)
        Dim ds As New DataSet
        Try
            If ConsultaTrans = False Then
                If ConectarBase = True And DesConectarBase = True Then


                    Using cn As New SqlConnection(StrConexion)
                        Try
                            'If cn.State = ConnectionState.Closed Then
                            '    cn.Open()
                            'End If

                            Using da As New SqlDataAdapter(StrSql, cn)
                                da.SelectCommand.CommandType = CommandType.Text
                                'Dim da As New SqlDataAdapter
                                'Dim cmd As New SqlCommand(StrSql, cnnBase)
                                da.SelectCommand.CommandTimeout = 600
                                'da.SelectCommand = cmd
                                da.Fill(ds)
                            End Using
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End Using


                Else
                    If ConectarBase = True Then
                        ConectaBase()
                    End If

                    Using da As New SqlDataAdapter(StrSql, cnnBase)
                        da.SelectCommand.CommandType = CommandType.Text
                        da.SelectCommand.CommandTimeout = 600
                        da.Fill(ds)
                    End Using
                    If DesConectarBase = True Then
                        DesConectaBase()
                    End If
                End If
            Else

                If Not cnnBase Is Nothing Then
                    If cnnBase.ConnectionString = String.Empty Then
                        cnnBase.ConnectionString = StrConexion
                    End If
                Else
                    ConectaBase()
                End If

                Using cmd As New SqlCommand(StrSql, cnnBase, cnnTran)
                    Dim da As New SqlDataAdapter
                    cmd.CommandType = CommandType.Text
                    cmd.CommandTimeout = 300
                    da.SelectCommand = cmd
                    da.Fill(ds)
                End Using

                If Not cnnBase Is Nothing Then
                    DesConectaBase()
                End If

            End If
            If DataTable = True Then
                'Regresa un datatable
                Return ds.Tables(0)
            Else
                'Regresa un Dataset
                Return ds
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ConectaBase()
        Try

            AsignaBase()

            cnnBase = New SqlConnection(StrConexion)

            If cnnBase.State = ConnectionState.Closed Then
                cnnBase.Open()
            End If
        Catch ex As Exception
            Throw ex
        End Try
        'evi 12/12/2013
        'cnnBase.Open()
    End Function

    Private Shared Function DesConectaBase()
        Try
            'evi 12/12/2013
            'cnnBase.Close()
            If Not cnnBase Is Nothing Then
                If cnnBase.State <> ConnectionState.Closed Then
                    cnnBase.Close()
                    cnnBase.Dispose()
                    cnnBase = Nothing
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Transaccion(ByVal StrTransaccion As String, Optional ByVal BTran As Boolean = False, Optional ByVal CTran As Boolean = False, Optional ByVal ConectarBase As Boolean = True, Optional ByVal DesConectarBase As Boolean = False)
        'Try

        '    If ConectarBase = True Then
        '        ConectaBase()
        '    End If
        '    If BTran = True Then
        '        cnnTran = cnnBase.BeginTransaction(IsolationLevel.ReadUncommitted, StrTransaccion)
        '    End If
        '    If CTran = True Then
        '        cnnTran.Commit()
        '        cnnTran.Dispose()
        '        cnnTran = Nothing
        '    End If
        '    If DesConectarBase = True Then
        '        DesConectaBase()
        '    End If
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Function

    Public Function EjecutaSQL(ByVal StrTransaccion As String, ByVal StrSql As String, Optional ByRef FilasAfectadas As Integer = 0) As Boolean


        Using cn As New SqlConnection(StrConexion)

            cn.Open()

            Dim tran As SqlTransaction
            tran = cn.BeginTransaction(IsolationLevel.ReadUncommitted, StrTransaccion)

            Try

                Using cnnCmd As New SqlCommand(StrSql, cn, tran)
                    cnnCmd.CommandType = CommandType.Text
                    cnnCmd.CommandTimeout = 300
                    FilasAfectadas = cnnCmd.ExecuteNonQuery()
                End Using

                tran.Commit()

            Catch ex As Exception
                EjecutaSQL = False
                tran.Rollback(StrTransaccion)
                Throw ex
            End Try

        End Using

        EjecutaSQL = True
    End Function

    'Public Function EjecutaSQL(ByVal StrTransaccion As String, ByVal StrSql As String, Optional ByRef FilasAfectadas As Integer = 0) As Boolean
    '    Try
    '        Using cnnCmd As New SqlCommand(StrSql, cnnBase, cnnTran)
    '            cnnCmd.CommandType = CommandType.Text
    '            cnnCmd.CommandTimeout = 300
    '            FilasAfectadas = cnnCmd.ExecuteNonQuery()
    '        End Using
    '    Catch ex As Exception
    '        EjecutaSQL = False
    '        cnnTran.Rollback(StrTransaccion)
    '        Throw ex
    '    End Try
    'End Function

    Public Function EjecutaSP(ByVal sNameSP As String, ByVal sParameters As String) As DataSet
        Dim dsResp As New DataSet
        Dim sNameParameter As String = "" : Dim sValueParameter As String = "" : Dim sParameterByOne As String = ""
        Dim arrParameters As String() = Nothing : Dim arrParameterByOne As String() = Nothing

        Try
            Using cn As New SqlConnection(StrConexion)
                cn.Open()
                Using cnnCmd As New SqlCommand(sNameSP, cn)
                    cnnCmd.CommandType = CommandType.StoredProcedure
                    cnnCmd.CommandTimeout = 600

                    arrParameters = sParameters.Split("#")
                    For Each sParameterByOne In arrParameters
                        arrParameterByOne = sParameterByOne.Split("|")
                        sNameParameter = arrParameterByOne(0)
                        sValueParameter = arrParameterByOne(1)
                        cnnCmd.Parameters.AddWithValue(sNameParameter, sValueParameter)
                    Next

                    Using sqlDa As New SqlDataAdapter(cnnCmd)
                        sqlDa.Fill(dsResp)
                    End Using
                End Using
            End Using

            Return dsResp

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
