Imports CD_Datos.CdSql
Imports CP_FRACCIONES

Public Class CdCotizador
    Private StrSql As String = ""
    Private StrSql2 As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New CdSql
    Public StrSqlSalida As String

    Public Sub New()
    End Sub

    Public Sub carga_cadena_impresion(ByVal Idpoliza As Long, ByVal strpoliza As String, ByVal strcadena As String, ByVal strtipo As Byte)
        Try
            StrSql = "update poliza set "
            If strtipo = 0 Then
                'da�os
                StrSql = StrSql & " cadena_impresionD ='" & strcadena & "'"
            Else
                'vida
                StrSql = StrSql & " cadena_impresionV ='" & strcadena & "'"
            End If
            StrSql = StrSql & " where id_poliza =" & Idpoliza & ""
            StrSql = StrSql & " and poliza ='" & strpoliza & "'"
            StrTransaccion = "IntPolizaVida"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function usuario_bid(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = " select id_bid = 0, empresa = '-- Seleccione --' union "
            StrSql = StrSql & " select id_bid, empresa  "
            StrSql = StrSql & " from bid"
            StrSql = StrSql & " where estatus = 1"
            StrSql = StrSql & " and id_bid in (select id_bid "
            StrSql = StrSql & " from usuario_bid where id_usuario =" & intusuario & ")"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_region(ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select id_region=0, descripcion_region='-- Seleccione --' union"
            StrSql = StrSql & " select id_region, descripcion_region"
            StrSql = StrSql & " from abc_region"
            StrSql = StrSql & " where estatus_region = 1"
            StrSql = StrSql & " and id_region (select id_region "
            StrSql = StrSql & " from bid_region where id_bid=" & intbid & ")"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_marca(ByVal intmarca As Integer, ByVal intprograma As Integer, ByVal strestatus As String) As DataTable

        Try
            'If intprograma = 9 Then

            StrSql = "select id_marca = 0, marca = '-- Seleccione --' union "
            StrSql = StrSql & " SELECT distinct a.id_submarca  , m.Marca "
            StrSql = StrSql & " FROM ABC_MARCA_agrupador a, vehiculo v, ABC_MARCA M, ABC_PROGRAMA_ASEGURADORA p "
            StrSql = StrSql & " where a.id_aseguradora = v.id_aseguradora"
            StrSql = StrSql & " and a.id_marca = v.id_marca"
            StrSql = StrSql & " and a.id_submarca = v.id_submarca"
            StrSql = StrSql & " and v.Id_submarca = m.Id_Marca "
            StrSql = StrSql & " AND v.id_aseguradora = p.id_aseguradora "
            StrSql = StrSql & " and a.id_marca = " & intmarca & ""
            StrSql = StrSql & " AND p.id_programa =" & intprograma & ""
            StrSql = StrSql & " and v.estatus_vehiculo ='" & strestatus & "'"
            StrSql = StrSql & " and a.estatus_agrupador ='1' "
            StrSql = StrSql & " AND p.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            If strestatus = "N" Then
                StrSql = StrSql & " AND m.Estatus_Marca = '1' "
            End If
            'Else
            'strsql = " select id_marca = 0, marca = '-- Seleccione --' union"
            'strsql = strsql & " SELECT DISTINCT v.Id_Marca, m.Marca"
            'strsql = strsql & " FROM vehiculo v, ABC_MARCA M, ABC_PROGRAMA_ASEGURADORA a"
            'strsql = strsql & " WHERE v.Id_Marca = m.Id_Marca"
            'strsql = strsql & " AND v.id_aseguradora = a.id_aseguradora"
            'strsql = strsql & " AND m.Estatus_Marca = '1'"
            'strsql = strsql & " AND v.status_vehiculo='1'"
            'strsql = strsql & " AND a.estatus_programaaseguradora = '1'"
            'strsql = strsql & " AND a.id_programa =" & intprograma & ""
            'strsql = strsql & " AND v.Id_Marca=" & intmarca & ""
            'strsql = strsql & " AND v.estatus_vehiculo ='" & strestatus & "'"
            'strsql = strsql & " order by id_marca"
            'End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_modelo(ByVal intmarca As Integer, ByVal strtipoA As String, ByVal intprograma As Integer, ByVal intmarcabase As Integer) As DataTable

        Try
            StrSql = " SELECT modelo_vehiculo='-- Seleccione --', desc_vehiculo = '-- Seleccione --' union "
            StrSql = StrSql & " SELECT distinct modelo_vehiculo = v.Modelo_vehiculo, modelo_vehiculo = v.Modelo_vehiculo"
            StrSql = StrSql & " FROM vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            'emmanuell multimarca
            'If intprograma = 9 Then
            StrSql = StrSql & " AND v.Id_Marca = " & intmarcabase & ""
            StrSql = StrSql & " AND v.Id_subMarca = " & intmarca & ""
            'Else
            '    strsql = strsql & " AND v.Id_Marca = " & intmarca & ""
            'End If
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & strtipoA & "'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"

            StrSql = StrSql & " AND v.id_aonh in(select Id_AonH"
            StrSql = StrSql & " from homologacion"
            StrSql = StrSql & " where Estatus_Homologado=1)"

            StrSql = StrSql & " ORDER BY modelo_vehiculo"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal strtipoa As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer) As DataTable

        Try
            StrSql = " SELECT anio_vehiculo='-- Seleccione --','-- Seleccione --' UNION "
            StrSql = StrSql & " SELECT distinct anio_vehiculo = v.anio_vehiculo, v.anio_vehiculo "
            StrSql = StrSql & " FROM vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            'emmanuell multimarca
            'If intprograma = 9 Then
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""
            'Else
            '    StrSql = StrSql & " AND v.id_marca = " & intmarca & ""
            'End If
            StrSql = StrSql & " AND v.Modelo_vehiculo ='" & strmodelo & "'"
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & strtipoa & "'"
            StrSql = StrSql & " AND v.status_vehiculo= '1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function carga_clave_zurich(ByVal strestatus As String, ByVal strmodelo As String, _
    'ByVal strcatalogo As String, ByVal strdescripcion As String, ByVal stranio As String, ByVal intmarca As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        If strcatalogo <> "" Then
    '            strsql = "Select v.INSCO_vehiculo "
    '            strsql = strsql & " from VEHICULO v, abc_marca m"
    '            strsql = strsql & " where v.id_marca = m.id_marca"
    '            strsql = strsql & " and v.id_marca=" & intmarca & ""
    '            strsql = strsql & " and m.estatus_marca ='1'"
    '            strsql = strsql & " and estatus_vehiculo='" & strestatus & "' "
    '            strsql = strsql & " and modelo_vehiculo = '" & strmodelo & "'"
    '            strsql = strsql & " and catalogo_vehiculo = '" & strcatalogo & "'"
    '            strsql = strsql & " and descripcion_vehiculo = '" & strdescripcion & "'"
    '            strsql = strsql & " and anio_vehiculo = '" & stranio & "'"
    '            strsql = strsql & " and status_vehiculo = '1'"
    '        Else
    '            strsql = "Select v.INSCO_vehiculo "
    '            strsql = strsql & " from VEHICULO v, abc_marca m"
    '            strsql = strsql & " where v.id_marca = m.id_marca"
    '            strsql = strsql & " and m.estatus_marca ='1'"
    '            strsql = strsql & " and estatus_vehiculo='" & strestatus & "' "
    '            strsql = strsql & " and modelo_vehiculo = '" & strmodelo & "'"
    '            strsql = strsql & " and descripcion_vehiculo = '" & strdescripcion & "'"
    '            strsql = strsql & " and anio_vehiculo = '" & stranio & "'"
    '            strsql = strsql & " and status_vehiculo = '1'"
    '        End If
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    Public Function carga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal strtipoa As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer) As DataTable

        Try
            StrSql = "SELECT id_vehiculo=0, descripcion_vehiculo='-- Seleccione --' UNION "
            StrSql = StrSql & " SELECT distinct id_vehiculo = h.Id_AonH, descripcion_vehiculo = h.Descripcion_Homologado "
            StrSql = StrSql & " FROM HOMOLOGACION h, ABC_PROGRAMA_ASEGURADORA a , vehiculo v"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora "
            StrSql = StrSql & " AND v.id_aonh = h.id_aonh"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            If Not (intprograma = 9 And strtipoa = "U") Then
                StrSql = StrSql & " AND v.id_marca= h.id_marca"
            End If
            'emmanuell multimarca
            'If intprograma = 9 Then
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""
            'Else
            '    strsql = strsql & " AND v.id_marca = " & intmarca & ""
            'End If
            StrSql = StrSql & " AND v.modelo_vehiculo ='" & strmodelo & "'"
            StrSql = StrSql & " AND v.anio_vehiculo = '" & stranio & "'"
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & strtipoa & "'"
            StrSql = StrSql & " AND h.Estatus_Homologado = '1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " ORDER BY Descripcion_Vehiculo"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_plaza(ByVal intvehiculo As Integer) As DataTable

        Try
            StrSql = "select plaza_vehiculo, subramo_vehiculo, anio_vehiculo, "
            StrSql = StrSql & " plan_vehiculo, catalogo_vehiculo"
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where id_vehiculo = " & intvehiculo & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_subramo_homologado(ByVal intvehiculo As Integer, ByVal strestatus As String, _
    ByVal intprograma As Integer, ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer) As DataTable

        Try
            StrSql = "select distinct subramo_vehiculo = v.subramo_vehiculo, catalogo_vehiculo = v.catalogo_vehiculo "
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " AND v.Id_AonH = " & intvehiculo & ""
            StrSql = StrSql & " and v.estatus_vehiculo ='" & strestatus & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & " "
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & " "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_uso(ByVal intprograma As Integer, ByVal intaon As Integer, ByVal strestatus As String, ByVal intsubramo As Integer, ByVal strsession As String) As DataTable
        Try
            StrSql = "EXEC AGRUPA_USO " & intprograma & "," & intaon & ",'" & strestatus & "','" & strsession & "'," & intsubramo & ""
            Dim dt As DataTable = CConecta.AbrirConsulta(StrSql, True)

            StrSql = "DELETE FROM TEMP_USO WHERE idsession = '" & strsession & "'"
            StrTransaccion = "EliminaTempUso"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return dt

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_definicion_uso(ByVal intprograma As Integer, ByVal intsubramo As Integer, ByVal struso As String, ByVal intversion As Integer) As DataTable
        Try
            StrSql = "select id_uso, uso "
            StrSql = StrSql & " FROM  abc_uso u, ABC_PROGRAMA_ASEGURADORA p "
            StrSql = StrSql & " WHERE u.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and p.id_programa =" & intprograma & " "
            StrSql = StrSql & " and u.subramo = " & intsubramo & ""
            StrSql = StrSql & " and u.uso = '" & struso & "'"
            StrSql = StrSql & " and u.id_version =" & intversion & ""
            StrSql = StrSql & " and u.estatus_uso = '1' "
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_vehiculo_general(ByVal strinsco As String, ByVal intprograma As Integer, ByVal strestatus As String, ByVal intaseguradora As Integer, ByVal intvehiculo As Integer, ByVal intanio As Integer) As DataTable

        Try
            StrSql = "select distinct v.insco_vehiculo, m.id_marca, m.marca, anio = v.anio_vehiculo, modelo = v.modelo_vehiculo, "
            StrSql = StrSql & " descripcion_homologado = v.descripcion_vehiculo, catalogo = v.catalogo_vehiculo, "
            StrSql = StrSql & " clas= case v.subramo_vehiculo when  '1' then 'Autom�vil' else 'Cami�n' end, plaza_vehiculo, v.deducpl_vehiculo"
            StrSql = StrSql & " from vehiculo v, abc_marca m, ABC_PROGRAMA_ASEGURADORA a "
            StrSql = StrSql & " where v.Id_marca = m.id_marca"
            StrSql = StrSql & " AND v.id_aseguradora = a.id_aseguradora "
            StrSql = StrSql & " and m.estatus_marca = '1'"
            StrSql = StrSql & " and v.status_vehiculo ='1'"
            StrSql = StrSql & " and v.insco_vehiculo = '" & strinsco & "'"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & strestatus & "'"
            StrSql = StrSql & " AND v.id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " AND v.id_aonh=" & intvehiculo & ""
            StrSql = StrSql & " AND v.anio_vehiculo=" & intanio & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_vehiculo_homologado(ByVal intvehiculo As Integer, ByVal intprograma As Integer, _
    ByVal stranio As String, ByVal strestatus As String) As DataTable

        Try
            StrSql = "select DISTINCT estatus = v.estatus_vehiculo, m.marca, modelo = v.modelo_vehiculo, catalogo = v.catalogo_vehiculo, h.descripcion_homologado, anio = v.anio_vehiculo , v.deducpl_vehiculo"
            StrSql = StrSql & " from HOMOLOGACION h , abc_marca m, ABC_PROGRAMA_ASEGURADORA a , vehiculo v"
            StrSql = StrSql & " where h.id_aonh = v.id_aonh "
            If Not intprograma = 9 Then
                StrSql = StrSql & " AND m.id_marca = h.id_marca "
            End If
            StrSql = StrSql & " and m.id_marca = v.id_marca"
            StrSql = StrSql & " AND v.id_aseguradora = a.id_aseguradora "
            StrSql = StrSql & " and m.estatus_marca='1'"
            StrSql = StrSql & " and h.estatus_homologado='1'"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " and h.id_aonh = " & intvehiculo & ""
            StrSql = StrSql & " AND v.anio_vehiculo =" & stranio & ""
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & strestatus & "'"
            StrSql = StrSql & " group by v.estatus_vehiculo, m.marca, v.modelo_vehiculo, v.catalogo_vehiculo, h.descripcion_homologado, v.anio_vehiculo, v.deducpl_vehiculo "
            StrSql = StrSql & " order by v.catalogo_vehiculo DESC"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Function vehiculo_general(ByVal intvehiculo As Integer) As DataTable
    '    Dim da As New SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = " select m.id_marca, m.marca, v.anio_vehiculo, v.modelo_vehiculo, "
    '        strsql = strsql & " v.descripcion_vehiculo, v.catalogo_vehiculo, "
    '        strsql = strsql & " clas= case v.subramo_vehiculo when  '1' then 'Autom�vil' else 'Cami�n' end, plaza_vehiculo"
    '        strsql = strsql & " from vehiculo v, abc_marca m"
    '        strsql = strsql & " where v.Id_marca = m.id_marca"
    '        strsql = strsql & " and m.estatus_marca = '1'"
    '        strsql = strsql & " and v.status_vehiculo ='1'"
    '        strsql = strsql & " and id_vehiculo = " & intvehiculo & ""
    '        Dim cmdV As New SqlCommand(strsql, Cnn, trans)
    '        cmdV.CommandType = CommandType.Text
    '        da.SelectCommand = cmdV
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    Private Function vehiculo_general(ByVal strinsco As String, ByVal intversion As Integer, _
    ByVal strestatus As String, ByVal intsubramo As Integer, ByVal stranio As Integer, _
    ByVal intaseguradora As Integer, ByVal intaonh As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = " select m.id_marca, m.marca, v.anio_vehiculo, v.modelo_vehiculo, "
            sql = sql & " v.descripcion_vehiculo, v.catalogo_vehiculo, "
            sql = sql & " clas= case v.subramo_vehiculo when  '1' then 'Autom�vil' else 'Cami�n' end, plaza_vehiculo"
            sql = sql & " from vehiculo v, abc_marca m"
            sql = sql & " where v.Id_marca = m.id_marca"
            sql = sql & " and v.id_aseguradora = " & intaseguradora & ""
            sql = sql & " and m.estatus_marca = '1'"
            sql = sql & " and v.status_vehiculo ='1'"
            sql = sql & " and insco_vehiculo = '" & strinsco & "'"
            sql = sql & " and v.id_version=" & intversion & ""
            sql = sql & " and v.estatus_vehiculo = '" & strestatus & "'"
            sql = sql & " and subramo_vehiculo = " & intsubramo & ""
            sql = sql & " and anio_vehiculo = '" & stranio & "'"
            sql = sql & " and id_aonh = " & intaonh & ""
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_paquete(ByVal strsubramo As String) As DataTable

        Try
            StrSql = " select id_paquete=0, descripcion_paquete='-- Seleccione --' union"
            StrSql = StrSql & " select id_paquete, descripcion_paquete"
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where subramo = " & strsubramo & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_contrato(ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select numero= isnull(max(num_contrato),0) + 1 "
            StrSql = StrSql & " from cobertura"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub inserta_coberturas(ByVal Strseguro As String, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intprograma As Integer, _
    ByVal intaonv As Integer, ByVal strestatus As String, ByVal stranio As Integer)
        Dim ds As New DataSet
        Dim Arr() As String = Strseguro.Split("|")
        Dim i As Integer
        Dim dbseguro As Double = 0
        Dim datos As String = ""
        'Dim datos As String = "Valor Factura"
        Dim j As Integer = 0
        Dim cuenta As Integer = 0
        Dim intban As Integer = 0
        Dim CobIntAseg As Integer = 0
        Dim idcve_Aux As Integer = 0

        'Dim strIdpaquete As String = ""

        'For i = 0 To UBound(Arr)
        '    Dim arrs() As String = Arr(i).Split("*")
        '    'nos da el valor de idpaquete
        '    If strIdpaquete = "" Then
        '        strIdpaquete = arrs(1) & ","
        '    Else
        '        strIdpaquete = strIdpaquete & arrs(1) & ","
        '    End If
        'Next
        'If Not strIdpaquete = "" Then
        '    strIdpaquete = Mid(strIdpaquete, 1, strIdpaquete.Length - 1)
        'End If

        Dim strparametro As String = ""
        For i = 0 To UBound(Arr)
            Dim arrs() As String = Arr(i).Split("*")
            'nos da el valor de idpaquete
            If strparametro = "" Then
                strparametro = arrs(0) & "," & arrs(1) & "*"
            Else
                strparametro = strparametro & arrs(0) & "," & arrs(1) & "*"
            End If
        Next
        If Not strparametro = "" Then
            strparametro = Mid(strparametro, 1, strparametro.Length - 1)
        End If

        Try

            StrSql = "EXEC CONFORMA_COBERTURA " & intprograma & "," & intaonv & ",'" & strestatus & "'," & stranio & ",'" & strparametro & "'"
            'StrSql = "SELECT id_aseguradora, subramo , descripcion_cobadic=descripcion_paquete, orden=0, factor = 0, deducible='', suma_asegurada =0, idcve=0, ban=0, id_paquete"
            'StrSql = StrSql & " FROM ABC_PAQUETE"
            'StrSql = StrSql & " WHERE id_paquete in (" & strIdpaquete & ")"
            ''inicia micha
            ''StrSql = StrSql & " and subramo = " & intramo & ""
            'StrSql = StrSql & " and subramo in (select distinct v.subramo_vehiculo"
            'StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            'StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            'StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            'StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            'StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            'StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            'StrSql = StrSql & " AND v.status_vehiculo ='1')"
            ''fin micha

            'StrSql = StrSql & " and estatus = '" & strestatus & "'"
            'StrSql = StrSql & " AND ID_ASEGURADORA IN ("
            'StrSql = StrSql & " SELECT distinct p.id_aseguradora FROM ABC_PROGRAMA_ASEGURADORA p, vehiculo v"
            'StrSql = StrSql & " WHERE v.id_aseguradora = p.id_aseguradora "
            'StrSql = StrSql & " and p.id_programa = " & intprograma & ""
            'StrSql = StrSql & " and v.id_aonh = " & intaonv & ""
            'StrSql = StrSql & " and v.anio_vehiculo = " & stranio & ""
            'StrSql = StrSql & " AND p.estatus_programaaseguradora = '1')"
            'StrSql = StrSql & " GROUP BY id_aseguradora, subramo, descripcion_paquete, id_paquete"
            ''If intprograma = 4 Then
            ''    StrSql = StrSql & " UNION  "
            ''    StrSql = StrSql & " select id_aseguradora = -1, subramo  = " & intramo & ", descripcion_cobadic = 'Seguro de Vida Cr�dito', orden=1, factor = 0, deducible='', suma_asegurada =0, idcve=0, ban=0, id_paquete =0"
            ''    StrSql = StrSql & " UNION"
            ''    StrSql = StrSql & " select id_aseguradora = -1, subramo  = " & intramo & ", descripcion_cobadic = 'Seg. Desempleo', orden=2, factor = 0, deducible='', suma_asegurada =0, idcve=0, ban=0, id_paquete =0"
            ''End If
            'StrSql = StrSql & " UNION"
            'StrSql = StrSql & " SELECT id_aseguradora, subramo, descripcion_cobadic, orden, factor, deducible, suma_asegurada , idcve = id_cobadic, ban=0, id_paquete = 0"
            'StrSql = StrSql & " FROM COBERTURA_ADICIONAL_CUOTA"
            ''inicia micha
            ''StrSql = StrSql & " WHERE subramo = " & intramo & ""
            'StrSql = StrSql & " WHERE subramo in (select distinct v.subramo_vehiculo"
            'StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            'StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            'StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            'StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            'StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            'StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            'StrSql = StrSql & " AND v.status_vehiculo ='1')"
            ''fin micha
            'StrSql = StrSql & " AND ID_ASEGURADORA IN ("
            'StrSql = StrSql & " SELECT distinct p.id_aseguradora FROM ABC_PROGRAMA_ASEGURADORA p, vehiculo v"
            'StrSql = StrSql & " WHERE v.id_aseguradora = p.id_aseguradora "
            'StrSql = StrSql & " and p.id_programa = " & intprograma & ""
            'StrSql = StrSql & " and v.id_aonh = " & intaonv & ""
            'StrSql = StrSql & " and v.anio_vehiculo = " & stranio & ""
            'StrSql = StrSql & " AND p.estatus_programaaseguradora = '1')"
            'StrSql = StrSql & " GROUP BY id_aseguradora, subramo, descripcion_cobadic, orden, factor, deducible, suma_asegurada , id_cobadic"
            'StrSql = StrSql & " UNION"
            'StrSql = StrSql & " SELECT id_aseguradora, subramo, descripcion_cobadic, orden, factor, deducible, suma_asegurada , idcve = id_cobadip, ban = id_cobadip, id_paquete = 0"
            'StrSql = StrSql & " FROM COBERTURA_ADICIONAL_PRIMA"
            ''inicia micha
            ''StrSql = StrSql & " WHERE subramo = " & intramo & ""
            'StrSql = StrSql & " WHERE subramo in (select distinct v.subramo_vehiculo"
            'StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            'StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            'StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            'StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            'StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            'StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            'StrSql = StrSql & " AND v.status_vehiculo ='1')"
            ''fin micha
            'StrSql = StrSql & " AND ID_ASEGURADORA IN ("
            'StrSql = StrSql & " SELECT distinct p.id_aseguradora FROM ABC_PROGRAMA_ASEGURADORA p, vehiculo v"
            'StrSql = StrSql & " WHERE v.id_aseguradora = p.id_aseguradora "
            'StrSql = StrSql & " and p.id_programa = " & intprograma & ""
            'StrSql = StrSql & " and v.id_aonh = " & intaonv & ""
            'StrSql = StrSql & " and v.anio_vehiculo = " & stranio & ""
            'StrSql = StrSql & " AND p.estatus_programaaseguradora = '1')"
            'StrSql = StrSql & " GROUP BY id_aseguradora, subramo, descripcion_cobadic, orden, factor, deducible, suma_asegurada , id_cobadip"
            'StrSql = StrSql & " ORDER BY orden, descripcion_cobadic"
            ds = CConecta.AbrirConsulta(StrSql, False, , , True)
            '0 = aseguradora
            '1 = paquete
            '2 = poliza
            '"1*1*9055.34962935

            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    For j = 0 To Arr.Length - 1
                        Dim arrs() As String = Arr(j).Split("*")
                        If arrs(0) = ds.Tables(0).Rows(i)("id_aseguradora") And arrs(1) = ds.Tables(0).Rows(i)("id_paquete") Then
                            dbseguro = arrs(2)
                        End If
                    Next
                    'If ds.Tables(0).Rows(i)("idcve") = 0 Then
                    '    Dim arrs() As String = Arr(i).Split("*")
                    '    If arrs(0) = ds.Tables(0).Rows(i)("id_aseguradora") Then
                    '        dbseguro = arrs(1)
                    '    End If
                    'Else
                    '    dbseguro = 0
                    'End If
                    StrSql = "insert into COBERTURA (id_cobertura, num_contrato, id_aseguradora, id_bid, "
                    If ds.Tables(0).Rows(i)("ban") > 0 Then
                        StrSql = StrSql & "subramo, id_cap,"
                    Else
                        StrSql = StrSql & "subramo, id_cap,"
                    End If
                    'If ds.Tables(0).Rows(i)("descripcion_cobadic") = "AMPLIA" Or ds.Tables(0).Rows(i)("descripcion_cobadic") = "TOP DRIVER" Then
                    If determina_descripcion_paquete(IIf(ds.Tables(0).Rows(i)("id_aseguradora") = -1, CobIntAseg, ds.Tables(0).Rows(i)("id_aseguradora")), ds.Tables(0).Rows(i)("descripcion_cobadic"), ds.Tables(0).Rows(i)("subramo")) = 1 Then
                        StrSql = StrSql & " descripcion_cob, orden_cob, factor_cob, sumaaseg_cob, total_cob)"
                    Else
                        StrSql = StrSql & " descripcion_cob, orden_cob, factor_cob, deducible_cob, sumaaseg_cob)"
                    End If
                    'emmanuell datos de prueba
                    cuenta = cuenta + 1
                    If ds.Tables(0).Rows(i)("idcve") = intban Then
                        StrSql = StrSql & " values(" & cuenta & ", "
                    Else
                        'cuenta = cuenta + 1
                        If idcve_Aux = ds.Tables(0).Rows(i)("idcve") Then
                            cuenta = cuenta - 1
                        End If
                        StrSql = StrSql & " values(" & cuenta & ", "
                        idcve_Aux = ds.Tables(0).Rows(i)("idcve")
                    End If
                    StrSql = StrSql & intcontrato & "," & IIf(ds.Tables(0).Rows(i)("id_aseguradora") = -1, CobIntAseg, ds.Tables(0).Rows(i)("id_aseguradora")) & "," & intbid & ","
                    If ds.Tables(0).Rows(i)("ban") > 0 Then
                        'inicia micha
                        'StrSql = StrSql & ds.Tables(0).Rows(i)("subramo") & "," & ds.Tables(0).Rows(i)("ban") & ",'" & ds.Tables(0).Rows(i)("descripcion_cobadic") & "',"
                        StrSql = StrSql & ds.Tables(0).Rows(i)("subramo") & "," & ds.Tables(0).Rows(i)("idcve") & ",'" & ds.Tables(0).Rows(i)("descripcion_cobadic") & "',"
                    Else
                        'StrSql = StrSql & ds.Tables(0).Rows(i)("subramo") & ",'" & ds.Tables(0).Rows(i)("descripcion_cobadic") & "',"
                        StrSql = StrSql & ds.Tables(0).Rows(i)("subramo") & "," & ds.Tables(0).Rows(i)("idcve") & ",'" & ds.Tables(0).Rows(i)("descripcion_cobadic") & "',"
                        'termina micha
                    End If
                    StrSql = StrSql & ds.Tables(0).Rows(i)("orden") & "," & ds.Tables(0).Rows(i)("factor") & ",'"
                    'If ds.Tables(0).Rows(i)("descripcion_cobadic") = "AMPLIA" Or ds.Tables(0).Rows(i)("descripcion_cobadic") = "TOP DRIVER" Then
                    If determina_descripcion_paquete(IIf(ds.Tables(0).Rows(i)("id_aseguradora") = -1, CobIntAseg, ds.Tables(0).Rows(i)("id_aseguradora")), ds.Tables(0).Rows(i)("descripcion_cobadic"), ds.Tables(0).Rows(i)("subramo")) = 1 Then
                        StrSql = StrSql & datos & "'," & dbseguro & ")"
                    Else
                        StrSql = StrSql & ds.Tables(0).Rows(i)("deducible") & "','" & ds.Tables(0).Rows(i)("suma_asegurada") & "')"
                    End If
                    StrTransaccion = "InsertaCoberturas"
                    CConecta.Transaccion(StrTransaccion, True, False, True)
                    CConecta.EjecutaSQL(StrTransaccion, StrSql)
                    CConecta.Transaccion(StrTransaccion, False, True, False, True)

                    'intban = ds.Tables(0).Rows(i)("idcve")
                    intban = 0
                    CobIntAseg = IIf(ds.Tables(0).Rows(i)("id_aseguradora") = -1, CobIntAseg, ds.Tables(0).Rows(i)("id_aseguradora"))
                Next
            End If

            StrSql = "DELETE FROM TEMP_COBERTURA "
            StrSql = StrSql & " WHERE ID_PROGRAMA = " & intprograma & " "
            StrSql = StrSql & " AND ID_AONH = " & intaonv & ""
            StrSql = StrSql & " AND ESTATUS_AUTO = '" & strestatus & "'"
            StrSql = StrSql & " AND ANIO_AUTO = " & stranio & ""
            StrTransaccion = "EliminarTempCobertura"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function determina_descripcion_paquete(ByVal intaseguradora As Integer, ByVal strdescripcion As String, _
    ByVal intsubramo As Integer) As Integer
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim strsqld As String

        strsqld = "select descripcion_paquete "
        strsqld = strsqld & " from abc_paquete"
        strsqld = strsqld & " WHERE ID_ASEGURADORA = " & intaseguradora & ""
        strsqld = strsqld & " AND SUBRAMO = " & intsubramo & ""
        strsqld = strsqld & " and descripcion_paquete='" & strdescripcion & "'"
        ds = CConecta.AbrirConsulta(strsqld, False)
        'ds = CConecta.AbrirConsulta(StrSql, false)
        If ds.Tables(0).Rows.Count > 0 Then
            If Not ds.Tables(0).Rows(0).IsNull("descripcion_paquete") Then
                valor = 1
            End If
        End If
        Return valor
    End Function
    'Public Function paquete(ByVal intpaquete As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = "select subramo, paquete"
    '        strsql = strsql & " from abc_paquete"
    '        strsql = strsql & " where id_paquete = " & intpaquete & ""
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Function paquete(ByVal intsubramo As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = "select distinct Id_Aseguradora"
    '        strsql = strsql & " from abc_paquete"
    '        strsql = strsql & " where SUBRAMO = " & intsubramo & ""
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function carga_cobertura_bid(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strestatus As String, ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable

        Try
            'emmanuell este es el buieno pasado
            'strsql = " SELECT distinct id_cobertura, descripcion_cob, "
            'strsql = strsql & " sumaaseg_cob, desc_cob, numfact_cob, orden_cob, cap=isnull(id_cap,0)"
            'strsql = strsql & " FROM cobertura"
            'strsql = strsql & " where num_contrato =" & intcontrato & ""
            'strsql = strsql & " and id_bid = " & intbid & ""
            'emmanuell multimarca
            'emmanuell casa
            If intprograma = 9 Then
                StrSql = "SELECT distinct c.id_cobertura, c.descripcion_cob, "
            Else
                StrSql = "SELECT distinct c.descripcion_cob, "
            End If
            StrSql = StrSql & " c.sumaaseg_cob, c.desc_cob, c.numfact_cob, c.orden_cob, cap=isnull(c.id_cap,0),"
            StrSql = StrSql & " cobertura = case when descripcion_cob in "
            StrSql = StrSql & " (select distinct a.descripcion_paquete"
            StrSql = StrSql & " from abc_paquete a"
            StrSql = StrSql & " where a.id_aseguradora = c.id_aseguradora"
            StrSql = StrSql & " and a.estatus = '" & strestatus & "'"
            'inicia micha
            'strsql = strsql & " and a.subramo= " & strsubramo & ") then 0 else c.id_cobertura end"
            StrSql = StrSql & " and a.subramo in (select distinct v.subramo_vehiculo"
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1'))then 0 else c.id_cobertura end"
            'fin micha
            StrSql = StrSql & " from COBERTURA c"
            StrSql = StrSql & " where num_contrato =" & intcontrato & ""
            StrSql = StrSql & " and id_bid = " & intbid & ""
            If Not intprograma = 9 Then
                StrSql = StrSql & " order by orden_cob, descripcion_cob"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_sincob(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strestatus As String, _
    ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable

        Try
            'emmanuell este es el buieno pasado
            StrSql = "SELECT distinct c.descripcion_cob, "
            StrSql = StrSql & " c.sumaaseg_cob, c.desc_cob, c.numfact_cob, c.orden_cob, cap=isnull(c.id_cap,0),"
            StrSql = StrSql & " cobertura = case when descripcion_cob in "
            StrSql = StrSql & " (select distinct a.descripcion_paquete"
            StrSql = StrSql & " from abc_paquete a"
            StrSql = StrSql & " where a.id_aseguradora = c.id_aseguradora"
            StrSql = StrSql & " and a.estatus = '" & strestatus & "'"
            'inicia micha
            'strsql = strsql & " and a.subramo= " & strsubramo & ") then 0 else c.id_cobertura end"
            StrSql = StrSql & " and a.subramo in (select distinct v.subramo_vehiculo"
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1'))then 0 else c.id_cobertura end"
            'fin micha
            StrSql = StrSql & " from COBERTURA c"
            StrSql = StrSql & " where num_contrato =" & intcontrato & ""
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " and not orden_cob = 0 "
            StrSql = StrSql & " order by orden_cob, descripcion_cob"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function valida_cobertura_bid_cliente_auno(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer, ByVal strestatus As String) As Integer
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim sql As String = ""
        Try
            sql = " SELECT distinct valor = count(p.descripcion_cob)"
            sql = sql & " FROM cobertura p"
            sql = sql & " where p.num_contrato =" & intcontrato & ""
            sql = sql & " and p.id_bid = " & intbid & ""
            sql = sql & " and p.id_aseguradora = " & intaseguradora & ""
            sql = sql & " and orden_cob = 0"
            ds = CConecta.AbrirConsulta(sql, False)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)("valor")
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_cliente(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer, ByVal strestatus As String, _
    ByVal intprograma As Integer, ByVal intaonv As Integer) As DataTable

        Try
            StrSql = " SELECT distinct p.descripcion_cob, "
            StrSql = StrSql & " p.sumaaseg_cob, p.desc_cob, p.numfact_cob, p.orden_cob, cap=isnull(p.id_cap,0),"
            StrSql = StrSql & " cobertura = case when descripcion_cob in "
            StrSql = StrSql & " (select distinct a.descripcion_paquete"
            StrSql = StrSql & " from abc_paquete a"
            StrSql = StrSql & " where a.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and a.estatus = '" & strestatus & "'"
            'inicia micha
            'strsql = strsql & " and a.subramo= " & strsubramo & ") then 0 else p.id_cobertura end"
            StrSql = StrSql & " and a.subramo in (select distinct v.subramo_vehiculo"
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1'))then 0 else p.id_cobertura end"
            'fin micha
            StrSql = StrSql & " FROM cobertura p"
            StrSql = StrSql & " where p.num_contrato =" & intcontrato & ""
            StrSql = StrSql & " and p.id_bid = " & intbid & ""
            StrSql = StrSql & " and p.id_aseguradora = " & intaseguradora & ""
            If valida_cobertura_bid_cliente_auno(intcontrato, intbid, intaseguradora, intpaquete, strestatus) > 1 Then
                StrSql = StrSql & " and not p.descripcion_cob in( select distinct ap.descripcion_paquete from abc_paquete ap"
                StrSql = StrSql & " where ap.id_aseguradora = " & intaseguradora & ""
                StrSql = StrSql & " and not ap.id_paquete = " & intpaquete & ""
                StrSql = StrSql & " and ap.estatus ='" & strestatus & "') "
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_grid_edit(ByVal arr() As String, ByVal intcap As Integer) As Integer

        Dim ds As New DataSet
        Dim i As Integer
        Dim valor As Integer = 0
        Try
            For i = 0 To arr.Length - 1
                StrSql = " select distinct id_cobadip  from COBERTURA_ADICIONAL_PRIMA"
                StrSql = StrSql & " where id_aseguradora = " & arr(i) & ""
                StrSql = StrSql & " and id_cobadip=" & intcap & ""
                ds = CConecta.AbrirConsulta(StrSql, False)
                If ds.Tables(0).Rows.Count > 0 Then
                    valor = 1
                    Exit For
                End If
            Next
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_aseguradora(ByVal intcontrato As Integer, ByVal intbid As Integer) As DataTable

        Try
            'karla
            StrSql = " SELECT distinct c.id_aseguradora, a.descripcion_aseguradora, a.url_imagen"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.num_contrato = " & intcontrato & ""
            StrSql = StrSql & " and c.id_bid = " & intbid & ""
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            'strsql = strsql & " order by a.id_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_aseguradora_imp(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            'karla
            StrSql = " SELECT distinct c.id_aseguradora, a.descripcion_aseguradora"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.num_contrato = " & intcontrato & ""
            StrSql = StrSql & " and c.id_bid = " & intbid & ""
            StrSql = StrSql & " and c.id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            'strsql = strsql & " order by a.id_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_aseguradora_total_cob(ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal strestatus As String, ByVal strdescripcionPaquete As String, ByVal intaseguradora As Integer, _
    ByVal strpaquete As String) As DataTable

        Try
            'emmanuell este era bueno antes
            'strsql = " SELECT distinct c.id_aseguradora, a.descripcion_aseguradora, c.total_cob"
            'strsql = strsql & " FROM cobertura c, abc_aseguradora a"
            'strsql = strsql & " where c.id_aseguradora = a.id_aseguradora"
            'strsql = strsql & " and c.num_contrato = " & intcontrato & ""
            'strsql = strsql & " and c.id_bid = " & intbid & ""
            'strsql = strsql & " and c.id_cobertura = " & intcob & ""
            'strsql = strsql & " order by a.descripcion_aseguradora"
            StrSql = " SELECT distinct c.id_aseguradora, a.descripcion_aseguradora, c.total_cob, p.id_paquete , c.vida, c.desempleo, c.subramo"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a , abc_paquete p"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and c.descripcion_cob = p.descripcion_paquete"
            StrSql = StrSql & " and c.num_contrato = " & intcontrato & ""
            StrSql = StrSql & " and c.id_bid = " & intbid & ""
            StrSql = StrSql & " and p.estatus = '" & strestatus & "'"
            'strsql = strsql & " and p.subramo= " & intsubramo & ""
            StrSql = StrSql & " AND P.subramo = c.subramo"
            StrSql = StrSql & " and c.descripcion_cob ='" & strdescripcionPaquete & "'"
            If intaseguradora > 0 Then
                StrSql = StrSql & " and c.id_aseguradora =" & intaseguradora & " "
            End If
            If Not strpaquete = "" Then
                StrSql = StrSql & " and p.id_paquete in (" & strpaquete & ")"
            End If
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_bid_aseguradora_total_cob_contrato(ByVal intcontrato As Integer, _
  ByVal intbid As Integer, ByVal intcob As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = " SELECT distinct c.id_aseguradora, a.descripcion_aseguradora, c.total_cob"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.num_contrato = " & intcontrato & ""
            StrSql = StrSql & " and c.id_bid = " & intbid & ""
            StrSql = StrSql & " and c.id_cobertura = " & intcob & ""
            StrSql = StrSql & " and c.id_aseguradora=" & intaseguradora & ""
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function restale_carga_cobertura_total_cob_aseguradora_ramo(ByVal intcontrato As Integer, _
  ByVal intbid As Integer, ByVal intcobertura As Integer, ByVal intaseguradora As Integer, _
    ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = " SELECT distinct a.descripcion_aseguradora, c.total_cob"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and not c.descripcion_cob in ("
            StrSql = StrSql & " select distinct descripcion_paquete from abc_paquete"
            StrSql = StrSql & " where id_aseguradora =" & intaseguradora & ")"
            'strsql = strsql & " and not c.id_cobertura = 0"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & " "
            'strsql = strsql & " and id_cobertura=" & intcobertura & ""
            StrSql = StrSql & " and a.id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and total_cob >0"
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_coberturas_tmep() As DataTable

        Try
            StrSql = " SELECT id_cobertura_temp, descripcion_temp"
            StrSql = StrSql & " FROM COBERTURA_TEMP_NOMBRE"
            StrSql = StrSql & " where estatus_temp ='1'"
            StrSql = StrSql & " group by id_cobertura_temp, descripcion_temp"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_coberturas_detalle_tmep(ByVal strestatus As String, ByVal intcobtemp As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = " SELECT Id_Cobertura_Temp, Id_ASeguradora, Estatus, Descripcion_ConDetalle_Temp, tipo"
            StrSql = StrSql & " FROM COBERTURA_TEMP_NOMBRE_DETALLE"
            StrSql = StrSql & " WHERE  Estatus_ConDetalle_Temp = '1'"
            StrSql = StrSql & " AND Estatus = '" & strestatus & "'"
            StrSql = StrSql & " and id_cobertura_temp=" & intcobtemp & ""
            StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_asegurador_coberturas_detalle_tmep(ByVal intcotiza As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer) As DataTable

        Try
            StrSql = " SELECT Id_aseguradora, descripcion_aseguradora"
            StrSql = StrSql & " FROM VW_ORDEN_COBERTURA_TEMP"
            StrSql = StrSql & " WHERE  id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " AND id_bid = " & intbid & ""
            StrSql = StrSql & " and num_contrato=" & intcontrato & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function Carga_aseguradora_cobertura(ByVal intcotiza As Integer, ByVal intbid As Integer, _
        ByVal intcontrato As Integer, ByVal intversion As Integer, ByVal intprograma As Integer, _
        ByVal strfincon As String, ByVal strestatus As String) As DataTable

        Try
            StrSql = " SELECT distinct Id_aseguradora, descripcion_aseguradora"
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                StrSql = StrSql & " FROM VW_CARGA_COBERTURAS_CLIENTE"
            Else
                StrSql = StrSql & " FROM VW_CARGA_COBERTURAS"
            End If
            StrSql = StrSql & " WHERE  id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " AND id_bid = " & intbid & ""
            StrSql = StrSql & " and num_contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus='" & strestatus & "'"
            StrSql = StrSql & " order by descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_asegurador_coberturas_contrato(ByVal intcontrato As Integer, ByVal intcliente As Integer, _
    ByVal intbid As Integer, ByVal intnumcontrato As Integer) As DataTable

        Try
            StrSql = " SELECT Id_aseguradora, descripcion_aseguradora"
            StrSql = StrSql & " FROM VW_ORDEN_COBERTURA_CONTRATO"
            StrSql = StrSql & " WHERE  id_contrato = " & intcontrato & ""
            StrSql = StrSql & " AND id_cliente=" & intcliente & ""
            StrSql = StrSql & " AND id_bid = " & intbid & ""
            StrSql = StrSql & " and num_contrato=" & intnumcontrato & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cobertura_total_cob_aseguradora_ramo(ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intcobertura As Integer, ByVal idaseguradora As Integer) As DataTable

        Try
            StrSql = " SELECT distinct a.id_aseguradora, cap = isnull(c.id_cap,0), c.sumaaseg_cob,"
            StrSql = StrSql & " a.descripcion_aseguradora, c.total_cob"
            StrSql = StrSql & " FROM cobertura c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and not c.id_cobertura = 0"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & " "
            StrSql = StrSql & " and id_cobertura=" & intcobertura & ""
            StrSql = StrSql & " and c.id_aseguradora =" & idaseguradora & ""
            'strsql = strsql & " and total_cob >0"
            StrSql = StrSql & " order by a.descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function cobertura_bid(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select * from cobertura"
            sql = sql & " where num_contrato =" & intcontrato & ""
            sql = sql & " and id_bid = " & intbid & ""
            If intaseguradora > 0 Then
                sql = sql & " and id_aseguradora =" & intaseguradora & ""
            End If
            sql = sql & " order by orden_cob"
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_cobertura(ByVal dbsuma As Double, ByVal strdescripcion As String, _
    ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intnumplazo As Integer)

        Try
            StrTransaccion = "ActualizaCobertura"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set sumaaseg_Cob=" & dbsuma & ","
            StrSql = StrSql & " desc_cob='" & strdescripcion & "',"
            StrSql = StrSql & " num_plazo=" & intnumplazo & ","
            StrSql = StrSql & " numfact_cob='" & strfactura & "'"
            StrSql = StrSql & " where id_cobertura = " & intcobertura & ""
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            'strsql = strsql & " and subramo=" & intsubramo & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function determina_facvigencia(ByVal strmoneda As String, ByVal strvigencia As String, ByVal strtipopol As String) As DataTable

        Try
            StrSql = "select valor = isnull(factvig,0) from FACTVIGENCIA"
            StrSql = StrSql & " where moneda = '" & strmoneda & "'"
            StrSql = StrSql & " and vigencia = '" & strvigencia & "'"
            'strsql = strsql & " and tipopol ='" & strtipopol & "'"
            StrSql = StrSql & " AND tipopol ='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            StrSql = StrSql & " and estatus_factvigencia = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determina_facvigencia_aseguradora(ByVal strmoneda As String, _
    ByVal strvigencia As String, ByVal strtipopol As String, _
    ByVal intcontrato As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select distinct valor = isnull(factvig,0) , a.id_aseguradora"
            StrSql = StrSql & " FROM cobertura c, FACTVIGENCIA a "
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and moneda = '" & strmoneda & "' "
            StrSql = StrSql & " and vigencia = '" & strvigencia & "' "
            'strsql = strsql & " and tipopol ='" & strtipopol & "' "
            StrSql = StrSql & " AND tipopol ='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            StrSql = StrSql & " and estatus_factvigencia = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_cobertura_VALOR(ByVal strdescripcion As String, _
    ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intsubramo As Integer, ByVal intnumplazo As Integer, _
    ByVal strfactV As Double, ByVal intaseguradora As Integer, ByVal strsumcob As String)

        Try
            StrTransaccion = "ActualizaCobVALOR"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set desc_cob='" & strdescripcion & "',"
            'strsql = strsql & " num_plazo=" & intnumplazo & ","
            StrSql = StrSql & " numfact_cob='" & strfactura & "',"
            StrSql = StrSql & " FactVigencia='" & strfactV & "'"
            StrSql = StrSql & " where id_cobertura = " & intcobertura & ""
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            If intsubramo > 0 Then
                StrSql = StrSql & " and subramo=" & intsubramo & ""
            End If
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and SUMAASEG_COB='" & strsumcob & "'"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_VALOR_cero(ByVal strdescripcion As String, _
    ByVal strfactura As String, ByVal intcobertura As Integer, ByVal intcontrato As Integer, _
    ByVal intbid As Integer, ByVal intnumplazo As Integer, _
    ByVal strsumcob As String)

        Try
            StrTransaccion = "ActualizaCobVALORCero"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set total_cob = -1,"
            StrSql = StrSql & " desc_cob='" & strdescripcion & "',"
            'strsql = strsql & " num_plazo=" & intnumplazo & ","
            StrSql = StrSql & " numfact_cob='" & strfactura & "'"
            StrSql = StrSql & " where id_cobertura = " & intcobertura & ""
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            'strsql = strsql & " and subramo=" & intsubramo & ""
            If Not strsumcob = "" Then
                StrSql = StrSql & " and sumaaseg_cob='" & strsumcob & "'"
            End If
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function Consulta_cliente_prospecto(ByVal intbid As Integer, ByVal strnombre As String) As DataTable

        Try
            StrSql = " select nombre = razon_socialp, tipo = 'M' "
            'strsql = strsql & " num_cotizacion, id_clientep, Estatus = case estatusP when '1' then 'Activo' else 'Inactivo' end, estatusP"
            StrSql = StrSql & " FROM CLIENTE_PROSPECTO"
            StrSql = StrSql & " where id_bid =" & intbid & ""
            If Not strnombre = "" Then
                StrSql = StrSql & " and razon_socialp like '%" & strnombre & "%'"
            End If
            StrSql = StrSql & " and (not razon_socialp is null or razon_socialp <>'')"
            StrSql = StrSql & " and razon_socialp <>''"
            StrSql = StrSql & " union"
            StrSql = StrSql & " select nombre = isnull(nombreP,'')+' '+ isnull(apaternoP,'')+' '+ isnull(amaternop,''), tipo = 'F' "
            'strsql = strsql & " num_cotizacion, id_clientep, Estatus = case estatusP when '1' then 'Activo' else 'Inactivo' end, estatusP"
            StrSql = StrSql & " FROM CLIENTE_PROSPECTO"
            StrSql = StrSql & " where id_bid =" & intbid & ""
            If Not strnombre = "" Then
                StrSql = StrSql & " and isnull(nombreP,'')+' '+ isnull(apaternoP,'')+' '+ isnull(amaternop,'') like '%" & strnombre & "%'"
            End If
            StrSql = StrSql & " and (nombrep is null or nombrep <>'')"
            StrSql = StrSql & " order by nombre"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DatosClienteP(ByVal intbid As Integer, ByVal strnombre As String, ByVal strtipo As String, ByVal intclientep As Integer) As DataTable

        Try
            StrSql = " select * from cliente_prospecto"
            If intclientep = 0 Then
                StrSql = StrSql & " where id_bid=" & intbid & ""
                StrSql = StrSql & " and case when '" & strtipo & "' = 'M' then razon_socialp "
                StrSql = StrSql & " else isnull(nombrep,'')+' '+ isnull(apaternop,'')+' '+ isnull(amaternop,'') "
                StrSql = StrSql & " end = '" & strnombre & "'"
            Else
                StrSql = StrSql & " where id_clientep = " & intclientep & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_Clientep(ByVal intbid As Integer, ByVal strpaterno As String, _
    ByVal strmaterno As String, ByVal strnombre As String, _
    ByVal strtipo As String, ByVal strrazon As String, _
    ByVal strtel As String, ByVal strelaboro As String, _
    ByVal strestatus As String, ByVal strcp As String, _
    ByVal strestado As String, ByVal strprovincia As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_clientep),0) + 1 "
            'StrSql = StrSql & " from cliente_prospecto"
            'StrSql = StrSql & " where id_bid=" & intbid & ""
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "InsertaClientep"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsCliep table(id int); "
            StrSql = StrSql & " insert into cliente_prospecto (id_clientep, id_bid, apaternop, amaternop, nombrep, " & vbCrLf
            StrSql = StrSql & " tipopersonap, razon_socialp, telefonop, " & vbCrLf
            StrSql = StrSql & " estatusp, NomElaboroP, cp, estado, provincia) " & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_clientep into @idInsCliep select (Select isnull(max(id_clientep), 0 ) + 1 from cliente_prospecto With(NoLock)  where id_bid=" & intbid & "), " & intbid & ", '" & strpaterno & "','"
            StrSql = StrSql & strmaterno & "','" & strnombre & "','" & strtipo & "','" & strrazon & "','"
            StrSql = StrSql & strtel & "','" & strestatus & "','" & strelaboro & "','" & strcp & "','"
            StrSql = StrSql & strestado & "','" & strprovincia & "'; "
            StrSql += "select * from @idInsCliep; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub modifica_clientep(ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strnombre As String, ByVal strtipo As String, _
    ByVal strrazon As String, ByVal strtel As String, _
    ByVal strelaboro As String, _
    ByVal strestatus As String, ByVal intclientep As Integer, _
    ByVal intbid As Integer, ByVal strcp As String, _
    ByVal strestado As String, ByVal strprovincia As String)

        Try
            StrTransaccion = "ModificaClientep"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cliente_prospecto set apaternop ='" & strpaterno & "',"
            StrSql = StrSql & " amaternop='" & strmaterno & "',"
            StrSql = StrSql & " nombrep='" & strnombre & "',"
            StrSql = StrSql & " tipopersonap='" & strtipo & "',"
            StrSql = StrSql & " razon_socialp='" & strrazon & "',"
            StrSql = StrSql & " telefonop='" & strtel & "',"
            'strsql = strsql & " nomelaborop='" & strelaboro & "',"
            StrSql = StrSql & " estatusp='" & strestatus & "',"
            StrSql = StrSql & " cp='" & strcp & "',"
            StrSql = StrSql & " estado='" & strestado & "',"
            StrSql = StrSql & " provincia ='" & strprovincia & "'"
            StrSql = StrSql & " where id_clientep=" & intclientep & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function consulta_cliente(ByVal intbid As Integer, ByVal strnombre As String) As DataTable

        Try
            StrSql = " select nombre = razon_social , tipo = 'M' "
            'strsql = strsql & " num_cotizacion='', id_clientep = id_cliente, Estatus = case estatus when '1' then 'Activo' else 'Inactivo' end, estatusP = estatus"
            StrSql = StrSql & " from cliente "
            StrSql = StrSql & " where id_bid =" & intbid & ""
            If Not strnombre = "" Then
                StrSql = StrSql & " and razon_social like '%" & strnombre & "%'"
            End If
            StrSql = StrSql & " and (razon_social is null or razon_social<>'')"
            StrSql = StrSql & " and razon_social <>''"
            StrSql = StrSql & " union"
            StrSql = StrSql & " select nombre = isnull(nombre,'')+' '+ isnull(apaterno,'')+' '+ isnull(amaterno,'') , tipo = 'F' "
            'strsql = strsql & " num_cotizacion='', id_clientep = id_cliente, Estatus = case estatus when '1' then 'Activo' else 'Inactivo' end, estatusP = estatus"
            StrSql = StrSql & " from cliente "
            StrSql = StrSql & " where id_bid =" & intbid & ""
            If Not strnombre = "" Then
                StrSql = StrSql & " and isnull(nombre,'')+' '+ isnull(apaterno,'')+' '+ isnull(amaterno,'') like '%" & strnombre & "%'"
            End If
            StrSql = StrSql & " and (nombre is null or nombre<>'')"
            StrSql = StrSql & " order by nombre"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_inf_cliente(ByVal intbid As Integer, ByVal strnombre As String, ByVal strtipo As String) As DataTable

        Try
            StrSql = " select nombre, apaterno, amaterno, fnacimiento,"
            StrSql = StrSql & " rfc, lugar_nac, ocupacion, "
            StrSql = StrSql & " per_fiscal,"
            StrSql = StrSql & " razon_social, rep_legal, contacto,"
            StrSql = StrSql & " telefono, estatus, "
            StrSql = StrSql & " datos_empleado, agencia_empleado,"
            StrSql = StrSql & " cp_cliente, calle_cliente, colonia_cliente,"
            StrSql = StrSql & " ciudad_cliente, estado_cliente ,"
            StrSql = StrSql & " beneficiario_preferente, email, ban_beneficiario, NoExterior, NoInterior"
            StrSql = StrSql & " from cliente "
            StrSql = StrSql & " where id_bid =" & intbid & ""
            'strsql = strsql & " and id_cliente=" & intcliente & ""
            StrSql = StrSql & " and case when '" & strtipo & "' = 'M' then razon_social "
            StrSql = StrSql & " else isnull(nombre,'')+' '+ isnull(apaterno,'')+' '+ isnull(amaterno,'') "
            StrSql = StrSql & " end = '" & strnombre & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function actualiza_cliente(ByVal strnombre As String, ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strfecha As String, ByVal strrfc As String, ByVal strlugar As String, ByVal strocupacion As String, _
    ByVal strfiscal As String, ByVal strrazon As String, ByVal strlegal As String, ByVal strcontacto As String, _
    ByVal strtel As String, ByVal strestatus As String, ByVal strempleado As String, ByVal stragencia As String, _
    ByVal strcp As String, ByVal strdir As String, ByVal strcol As String, ByVal strciudad As String, _
    ByVal strestado As String, ByVal intbid As Integer, ByVal intcliente As Integer, _
    ByVal inttipocliente As Integer, ByVal intbandera As Integer, _
    ByVal strbeneficiario As String, ByVal stremail As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Dim asfecha() As String
        Dim fecha1 As String
        Try

            StrTransaccion = "ActualizaCliente"

            If inttipocliente = 2 Then
                'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
                'StrSql = " select valor = isnull(max(id_cliente),0) + 1 "
                'StrSql = StrSql & " from cliente"
                'StrSql = StrSql & " where id_bid=" & intbid & ""
                'ds = CConecta.AbrirConsulta(StrSql, False)
                'valor = ds.Tables(0).Rows(0)(0)

                StrSql = "set dateformat dmy; " & vbCrLf
                StrSql += " declare @idInsActCli table(id int); "
                StrSql = StrSql & " insert into cliente (id_cliente, id_bid) " & vbCrLf
                StrSql = StrSql & " OUTPUT inserted.id_cliente into @idInsActCli select (Select isnull(max(id_cliente), 0 ) + 1 from cliente With(NoLock) where id_bid=" & intbid & ")," & intbid & "; "
                StrSql += "select * from @idInsActCli; "
                CConecta.Transaccion(StrTransaccion, True, False, True)
                ds = CConecta.AbrirConsulta(StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    valor = ds.Tables(0).Rows(0)(0)
                End If
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

            End If

            If strfiscal <> "2" Then
                asfecha = strfecha.Split("/")
                If intbandera = 1 Then
                    fecha1 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                Else
                    fecha1 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                End If
            End If

            StrSql = "update cliente set nombre='" & strnombre & "',"
            StrSql = StrSql & " apaterno='" & strpaterno & "',"
            StrSql = StrSql & " amaterno ='" & strmaterno & "',"
            StrSql = StrSql & " fnacimiento='" & fecha1 & "',"
            StrSql = StrSql & " rfc='" & strrfc & "',"
            StrSql = StrSql & " lugar_nac='" & strlugar & "',"
            StrSql = StrSql & " ocupacion='" & strocupacion & "',"
            StrSql = StrSql & " per_fiscal='" & strfiscal & "',"
            StrSql = StrSql & " razon_social='" & strrazon & "',"
            StrSql = StrSql & " rep_legal='" & strlegal & "',"
            StrSql = StrSql & " contacto='" & strcontacto & "',"
            StrSql = StrSql & " telefono='" & strtel & "',"
            StrSql = StrSql & " estatus='" & strestatus & "',"
            StrSql = StrSql & " datos_empleado='" & strempleado & "',"
            StrSql = StrSql & " agencia_empleado='" & stragencia & "',"
            StrSql = StrSql & " cp_cliente='" & strcp & "',"
            StrSql = StrSql & " calle_cliente='" & strdir & "',"
            StrSql = StrSql & " colonia_cliente='" & strcol & "',"
            StrSql = StrSql & " ciudad_cliente='" & strciudad & "',"
            StrSql = StrSql & " beneficiario_preferente='" & strbeneficiario & "',"
            StrSql = StrSql & " email='" & stremail & "',"
            StrSql = StrSql & " estado_cliente='" & strestado & "'"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            If inttipocliente = 2 Then
                StrSql = StrSql & " and id_cliente=" & valor & ""
            Else
                StrSql = StrSql & " and id_cliente=" & intcliente & ""
                valor = intcliente
            End If
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_cliente(ByVal intbid As Integer, _
    ByVal strnombre As String, ByVal strpaterno As String, ByVal strmaterno As String, _
    ByVal strfecha As String, ByVal strrfc As String, ByVal strlugar As String, ByVal strocupacion As String, _
    ByVal strfiscal As String, ByVal strrazon As String, ByVal strlegal As String, ByVal strcontacto As String, _
    ByVal strtel As String, ByVal strestatus As String, ByVal strempleado As String, ByVal stragencia As String, _
    ByVal strcp As String, ByVal strdir As String, ByVal strcol As String, ByVal strciudad As String, _
    ByVal strestado As String, ByVal intbandera As Integer, ByVal strbeneficiario As String, _
    ByVal stremail As String, ByVal strbanbeneficiario As String, _
    ByVal strinterior As String, ByVal strexterior As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Dim asfecha() As String
        Dim fecha1 As String

        Try
            fecha1 = strfecha
            If strfiscal <> "2" Then
                asfecha = strfecha.Split("/")
                If intbandera = 1 Then
                    fecha1 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                Else
                    fecha1 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                End If
            End If

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_cliente),0) + 1 "
            'StrSql = StrSql & " from cliente"
            'StrSql = StrSql & " where id_bid=" & intbid & ""
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "InsertaCliente"

            'StrSql = "set dateformat dmy; " & vbCrLf
            StrSql = " declare @idInsCli table(id int); "
            StrSql = StrSql & " insert into cliente (id_cliente, id_bid, nombre, apaterno, amaterno,"
            StrSql = StrSql & " fnacimiento, rfc, lugar_nac, ocupacion, per_fiscal, razon_social,"
            StrSql = StrSql & " rep_legal, contacto, telefono, estatus,"
            StrSql = StrSql & " cp_cliente, calle_cliente, colonia_cliente,"
            StrSql = StrSql & " ciudad_cliente, estado_cliente, agencia_empleado,"
            StrSql = StrSql & " beneficiario_preferente, email, Ban_beneficiario, nointerior, noexterior) "
            StrSql = StrSql & " OUTPUT inserted.id_cliente into @idInsCli select (Select isnull(max(id_cliente), 0 ) + 1 from cliente With(NoLock) where id_bid = " & intbid & ")," & intbid & ", '" & strnombre & "','"
            StrSql = StrSql & strpaterno & "','" & strmaterno & "','"
            StrSql = StrSql & fecha1 & "', '" & strrfc & "','"
            StrSql = StrSql & strlugar & "','" & strocupacion & "','"
            StrSql = StrSql & strfiscal & "','" & strrazon & "','"
            StrSql = StrSql & strlegal & "','" & strcontacto & "','"
            StrSql = StrSql & strtel & "','" & strestatus & "','"
            StrSql = StrSql & strcp & "','" & strdir & "','"
            StrSql = StrSql & strcol & "','" & strciudad & "','"
            StrSql = StrSql & strestado & "','" & stragencia & "','"
            StrSql = StrSql & strbeneficiario & "','" & stremail & "','" & strbanbeneficiario & "','" & strinterior & "', '" & strexterior & "'; "
            StrSql += "select * from @idInsCli; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_version(ByVal intversion As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select distinct vigencia from VERSION"
            StrSql = StrSql & " where id_version=" & intversion & ""
            If intaseguradora > 0 Then
                StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            End If
            'strsql = strsql & " order by id_version desc"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_clienteP_impresion(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select paterno= apaternop, materno= amaternop, nombre= nombrep, "
            StrSql = StrSql & " rs = razon_socialp, rfc = rfcp, tel = telefonop"
            StrSql = StrSql & " from cliente_prospecto"
            StrSql = StrSql & " where id_clientep = " & intcliente & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_cliente_impresion(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select paterno= apaterno, materno= amaterno, nombre, "
            StrSql = StrSql & " rs= razon_social, rfc, tel = telefono"
            StrSql = StrSql & " from cliente"
            StrSql = StrSql & " where id_cliente = " & intcliente & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function descripcion_paquete(ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select descripcion_paquete from abc_paquete"
            StrSql = StrSql & " where id_paquete = " & intpaquete & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_contrato(ByVal intcliente As Integer, ByVal intbid As Integer, _
    ByVal strplazo As String, ByVal strnumpago As String, ByVal Dcontrato As String, _
    ByVal strfechaf As String, ByVal strtipo As String, _
    ByVal strnumcontrato As String, ByVal intaseguradora As Integer, _
    ByVal strbene As String, ByVal dbenganche As Double, ByVal strContactoCob As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Try

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_contrato),0) + 1 "
            'StrSql = StrSql & " from contrato"
            'StrSql = StrSql & " where id_cliente = " & intcliente & ""
            'StrSql = StrSql & " and Id_Bid=" & intbid & ""
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "InsertaContrato"

            'StrSql = "set dateformat dmy; " & vbCrLf
            StrSql = " declare @idInsCon table(id int); " & vbCrLf
            StrSql = StrSql & " insert into contrato (id_contrato, id_cliente, id_bid, " & vbCrLf
            StrSql = StrSql & " plazo_contrato, num_pago, fecha_registro, finicio_contrato, " & vbCrLf
            StrSql = StrSql & " ffinal_contrato, tipo_auto, num_contrato, id_aseguradora, " & vbCrLf
            StrSql = StrSql & " beneficiario_preferente, enganche, ContactoCobranza)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_contrato into @idInsCon select (Select isnull(max(id_contrato), 0 ) + 1 from contrato With(NoLock) where id_cliente =" & intcliente & " and id_bid=" & intbid & "), " & intcliente & "," & intbid & ",'"
            StrSql = StrSql & strplazo & "','" & strnumpago & "',getdate(),'" & Dcontrato & "','"
            StrSql = StrSql & strfechaf & "','" & strtipo & "','" & strnumcontrato & "'," & intaseguradora & ",'"
            StrSql = StrSql & strbene & "', " & dbenganche & ", '" & strContactoCob & "'; "
            StrSql += "select * from @idInsCon; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_cotizacion_p(ByVal intnumcont As Integer, ByVal intcliente As Integer, _
    ByVal intbid As Integer, ByVal intvaonh As Integer, ByVal intplazo As Integer, _
    ByVal dbprecio As Double, ByVal strpol As String, ByVal strelaboro As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_cotizacion),0) + 1 "
            'StrSql = StrSql & " from COTIZACION_CLIENTE_P"
            'StrSql = StrSql & " where id_bid =" & intbid & ""
            'ds = CConecta.AbrirConsulta(StrSql, False, , , True)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "Inserta_cotizacion_p"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idIns table(id int); "
            StrSql = StrSql & " insert into COTIZACION_CLIENTE_P(id_cotizacion, num_contrato, " & vbCrLf
            StrSql = StrSql & " id_clientep, id_bid, Id_Aonh, plazo, precio, tipo_pol, nomelaborop)" & vbCrLf
            StrSql = StrSql & " values(" & valor & ", " & intnumcont & "," & vbCrLf
            StrSql = StrSql & intcliente & "," & intbid & ", " & intvaonh & "," & vbCrLf
            StrSql = StrSql & intplazo & ",'" & dbprecio & "','" & strpol & "','" & strelaboro & "')"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_contrato_coti(ByVal intcliente As Integer, ByVal intbid As Integer, _
        ByVal strplazo As String, ByVal strnumpago As String, ByVal strfechai As String, _
        ByVal strfechaf As String, ByVal strtipo As String, _
        ByVal strnumcontrato As String, ByVal intnumconcob As Integer) As Integer
        Dim valor As Integer
        Dim ds As New DataSet
        Try

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_contrato),0) + 1 "
            'StrSql = StrSql & " from coti_contrato"
            'StrSql = StrSql & " where id_cliente = " & intcliente & ""
            'StrSql = StrSql & " and Id_Bid=" & intbid & ""
            'ds = CConecta.AbrirConsulta(StrSql, False, , , True)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "InsertaContratoCoti"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsConCoti table(id int); "
            StrSql = StrSql & " insert into coti_contrato(id_contrato, id_cliente, id_bid, " & vbCrLf
            StrSql = StrSql & " plazo_contrato, num_pago, fecha_registro, finicio_contrato, " & vbCrLf
            StrSql = StrSql & " ffinal_contrato, tipo_auto, num_contrato, Num_Contratro_Cobertura)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_contrato into @idInsConCoti select (Select isnull(max(id_contrato), 0 ) + 1 from coti_contrato With(NoLock) where id_cliente = " & intcliente & " and Id_Bid=" & intbid & "), " & intcliente & "," & intbid & ",'"
            StrSql = StrSql & strplazo & "','" & strnumpago & "',getdate(),'" & strfechai & "','"
            StrSql = StrSql & strfechaf & "','" & strtipo & "','" & strnumcontrato & "'," & intnumconcob & "; "
            StrSql += "select * from @idInsConCoti; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function PolizaAjustadas(ByVal IdAseguradora As Integer, ByVal IdPrograma As Integer, ByVal tiporpoducto As Integer, _
                                    ByVal Intcontador As Integer) As DataTable
        'PXR = multianual
        'PYR = seguro gratis 1
        'PZR = seguro gratis 2
        'PXR2 = Anual (24 y 36)
        'PXR3 = Anual (48 y 60)
        'DW40 = Seguro gratis (cruzada Qts (poliza 1) y Axa (poliza 2))
        'Intcontador = 0 (poliza 1), Intcontador=1 (poliza 2) 

        'EVI 12/12/2014
        'AXA Conauto -- 61 -- programa = 29
        'Multianual = Tipoproducto = 1
        'Seguro gratis 1 = Tipoproducto = 0
        'Seguro gratis 2 = Tipoproducto = 3

        'Qualitas -- 59 -- programa = 29
        'Tipoproducto = 0 para todos los productos Multianual, Primera y Segunda P�lizas

        'GNP -- 60 -- programa = 29
        'Multianual = Tipoproducto = 1
        'Seguro gratis 1 = Tipoproducto = 0
        'Seguro gratis 2 = Tipoproducto = 3

        Try

            If IdPrograma = 30 Then
                StrSql = "select top 1 maxid = polizainicio"
            Else
                StrSql = "select top 1 maxid = polizainicio, contrato, caratula"
            End If
            StrSql = StrSql & " from PARAMETRO_AJUSTARPOLIZA"
            StrSql = StrSql & " where id_programa = " & IdPrograma & ""
            StrSql = StrSql & " and id_aseguradora =  " & IdAseguradora & ""
            
            Select Case tiporpoducto
                Case 0
                    If Intcontador = 0 Then
                        StrSql = StrSql & " and tipoproducto = 0"
                    Else
                        'EVI 12/12/2014
                        'StrSql = StrSql & " and tipoproducto = 1"
                        StrSql = StrSql & " and tipoproducto = 3"
                    End If
                Case 1 'EVI 12/12/2014
                    'StrSql = StrSql & " and tipoproducto = 2"
                    StrSql = StrSql & " and tipoproducto =  " & tiporpoducto & ""
                Case Else
                    StrSql = StrSql & " and tipoproducto =  " & tiporpoducto & ""
            End Select

            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Max_poliza(ByVal intaseguradora As Integer, ByVal intsubramo As Integer, _
    ByVal bantipopol As Integer, ByVal intbid As Integer, ByVal banEstatusParametro As Integer, _
    ByVal strestatus As String) As DataTable

        Try
            'pasado
            Select Case bantipopol
                Case 1
                    'StrSql = "select convert(bigint, max(id_poliza)) as maxid "
                    StrSql = "select convert(bigint, max(poliza)) + 1 as maxid "
                    StrSql = StrSql & " from poliza "
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    If banEstatusParametro = 1 Then
                        StrSql = StrSql & " and fscstatus='" & strestatus & "'"
                    End If
                Case 2
                    StrSql = "select convert(bigint, max(poliza)) + 1 as maxid "
                    StrSql = StrSql & " from poliza a, abc_paquete p"
                    StrSql = StrSql & " where a.id_aseguradora = p.id_aseguradora"
                    StrSql = StrSql & " and a.id_paquete = p.id_paquete"
                    StrSql = StrSql & " and a.id_aseguradora =" & intaseguradora & ""
                    StrSql = StrSql & " and p.subramo =" & intsubramo & ""
                    If banEstatusParametro = 1 Then
                        StrSql = StrSql & " and fscstatus='" & strestatus & "'"
                    End If
                Case 3
                    StrSql = "select convert(bigint, max(poliza)) + 1 as maxid "
                    StrSql = StrSql & " from poliza "
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    StrSql = StrSql & " and id_bid =" & intbid & ""
                    If banEstatusParametro = 1 Then
                        StrSql = StrSql & " and fscstatus ='" & strestatus & "'"
                    End If
            End Select

            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Poliza(ByVal intpoliza As Long) As DataTable

        Try
            StrSql = "select convert(bigint, poliza) + 1 as maxpoliza from poliza "
            StrSql = StrSql & " where id_poliza = " & intpoliza & ""

            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Max_poliza_ace(ByVal intbid As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            'strsql = "select convert(bigint, max(substring(polizaAce,4,10))) + 1 as maxid "
            StrSql = "select convert(bigint, max(polizaAce)) + 1 as maxid "
            StrSql = StrSql & " from poliza_desempleo "
            StrSql = StrSql & " where id_Bid = " & intbid & ""
            StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function poliza_aseguradora(ByVal intaseguradora As Integer, ByVal intsubramo As Integer, _
    ByVal intbantipopol As Integer, ByVal intbid As Integer, ByVal banestatusparametro As Integer, _
    ByVal strestatus As String) As DataTable

        Try
            Select Case intbantipopol
                Case 1
                    StrSql = "select valor = isnull(convert(bigint,polizainicio),0) + 1 "
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
                Case 2
                    StrSql = "select valor = isnull(convert(bigint,polizainicio),0) + 1 "
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    StrSql = StrSql & " and subramo =" & intsubramo & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
                Case 3
                    StrSql = "select valor = isnull(convert(bigint,polizainicio),0) + 1 "
                    StrSql = StrSql & " from PARAMETRO_BID"
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    StrSql = StrSql & " and id_bid =" & intbid & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
            End Select
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function poliza_aseguradora_Ace(ByVal intaseguradora As Integer, ByVal intbid As Integer, ByVal intbantipopol As Integer) As DataTable

        Try
            Select Case intbantipopol
                Case 1
                    StrSql = "select valor = isnull(convert(bigint,polizainiace),0) + 1 "
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                Case 2
                Case 3
                    StrSql = "select valor = isnull(convert(bigint,polizainiace),0) + 1 "
                    StrSql = StrSql & " from PARAMETRO_BID"
                    StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
                    StrSql = StrSql & " and id_bid =" & intbid & ""
            End Select
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function rango_poliza_parametro(ByVal intaseguradora As Integer, ByVal intsubramo As Integer, _
    ByVal intbantipopol As Integer, ByVal intbid As Integer, ByVal banestatusparametro As Integer, _
    ByVal strestatus As String) As DataTable

        Try
            Select Case intbantipopol
                Case 1
                    StrSql = "select polizainicio, polizafinal"
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora=" & intaseguradora & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
                Case 2
                    StrSql = "select polizainicio, polizafinal"
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora=" & intaseguradora & ""
                    StrSql = StrSql & " and subramo=" & intsubramo & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
                Case 3
                    StrSql = "select polizainicio, polizafinal"
                    StrSql = StrSql & " from PARAMETRO_BID"
                    StrSql = StrSql & " where id_aseguradora=" & intaseguradora & ""
                    StrSql = StrSql & " and id_bid=" & intbid & ""
                    If banestatusparametro = 1 Then
                        StrSql = StrSql & " and estatus='" & strestatus & "'"
                    End If
            End Select
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function rango_poliza_parametro_ace(ByVal intaseguradora As Integer, ByVal intbid As Integer, ByVal intbantipopol As Integer) As DataTable

        Try
            Select Case intbantipopol
                Case 1
                    StrSql = "select polizainiace, polizafinace"
                    StrSql = StrSql & " from parametro"
                    StrSql = StrSql & " where id_aseguradora=" & intaseguradora & ""
                Case 2
                Case 3
                    StrSql = "select polizainiace, polizafinace"
                    StrSql = StrSql & " from PARAMETRO_BID"
                    StrSql = StrSql & " where id_aseguradora=" & intaseguradora & ""
                    StrSql = StrSql & " and id_bid=" & intbid & ""
            End Select
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_rangos_poliza(ByVal intaseguradora As Integer, ByVal intsubramo As Integer) As DataTable

        Try
            StrSql = "select valor = isnull(polizainicio,0) + 1 "
            StrSql = StrSql & " from parametro"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            If Not intaseguradora = 2 Then
                StrSql = StrSql & " and subramo =" & intsubramo & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Max_poliza_coti(ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select convert(bigint, max(poliza)) + 1 as maxid "
            StrSql = StrSql & " from coti_poliza "
            'where id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cargando_cp(ByVal scp As String) As DataTable

        Try
            StrSql = "select id_municipio = 0, nombre_colonia ='-- Seleccione --' union"
            StrSql = StrSql & " SELECT id_municipio, nombre_colonia "
            StrSql = StrSql & " FROM ABC_COLONIA"
            StrSql = StrSql & " WHERE cp = '" & scp & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub inserta_firmaAbajo(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal strisco As String, ByVal strventa As String, _
    ByVal dbprecio As Double, ByVal intSeguro As Integer)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_firmaAbajo"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into FIRMAABAJO (id_poliza, id_bid, isco, tipo_venta, "
            sql = sql & " tipo_poliza, precio_auto)"
            sql = sql & " values (" & intpoliza & "," & intbid & ",'" & strisco & "','"
            sql = sql & strventa & "',"
            If intSeguro = 0 Then
                sql = sql & " 'C',"
            Else
                sql = sql & " 'F',"
            End If
            sql = sql & dbprecio & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_poliza_cliente(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal intcliente As Integer, ByVal intcontrato As Integer)
        Dim sql As String = ""
        Try
            StrTransaccion = "InsertaPolizaCliente"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into CLIENTE_POLIZA (id_cliente, id_poliza, id_bid, Id_Contrato)"
            sql = sql & " values(" & intcliente & ", " & intpoliza & ", " & intbid & ", " & intcontrato & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_poliza_cliente_COTI(ByVal intpoliza As Integer, ByVal intbid As Integer, _
        ByVal intcliente As Integer, ByVal intcontrato As Integer)
        Dim sql As String = ""
        Try
            StrTransaccion = "InsertaPolizaClienteCOTI"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into coti_CLIENTE_POLIZA (id_cliente, id_poliza, id_bid, Id_Contrato)"
            sql = sql & " values(" & intcliente & ", " & intpoliza & ", " & intbid & ", " & intcontrato & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function limresp(ByVal intaseguradora As Integer, ByVal intversion As Integer, ByVal intpaquete As Integer) As DataTable
        Dim sql As String = ""
        Try
            'strsql = "select descripcion_suma from LIMRESP"
            'strsql = strsql & " where id_LIMRESP = 4"
            sql = "select distinct descripcion_suma, bandera_ocupante from LIMRESP"
            sql = sql & " where id_aseguradora = " & intaseguradora & ""
            sql = sql & " and id_version = " & intversion & ""
            sql = sql & " and id_paquete =" & intpaquete & ""
            sql = sql & " and (cobertura = 'Gastos M�dicos (LUC)' or"
            sql = sql & " cobertura = 'Gastos Medicos (LUC)' or"
            sql = sql & " cobertura = 'Gastos M�dicos' or"
            sql = sql & " cobertura = 'Gastos Medicos')"
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function deducpl_vehiculo(ByVal intvehiculo As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = "select deducpl_vehiculo from vehiculo "
    '        strsql = strsql & " where id_vehiculo = " & intvehiculo & ""
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Private Function cobertura_adicional_interiores(ByVal intaseguradora As Integer, ByVal strmodelo As String, ByVal stranio As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select valor_suma, suma, deduc_suma, deduc"
            sql = sql & " from COBERTURA_ADICION_INTERIORES"
            sql = sql & " where id_aseguradora = " & intaseguradora & ""
            sql = sql & " and modelo_proteccion ='" & strmodelo & "'"
            sql = sql & " and anio_proteccion = " & stranio & ""
            sql = sql & " and estatus_proteccion = '1'"
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function deducpl_vehiculo(ByVal intaseguradora As Integer, ByVal strinsco As String, ByVal stranio As Integer, ByVal intaonH As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select deducpl_vehiculo from vehiculo "
            sql = sql & " where id_aseguradora = " & intaseguradora & ""
            sql = sql & " and insco_vehiculo = '" & strinsco & "'"
            sql = sql & " and anio_vehiculo = " & stranio & ""
            sql = sql & " and id_aonh = " & intaonH & ""
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function carga_insco_homologado(ByVal intvehiculo As Integer, _
    ByVal stranio As String, ByVal intaseguradora As Integer, _
    ByVal strestatus As String, ByVal intsubramo As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select insco = insco_vehiculo"
            sql = sql & " from vehiculo"
            sql = sql & " where Id_AonH = " & intvehiculo & ""
            sql = sql & " and anio_vehiculo=" & stranio & ""
            sql = sql & " and id_aseguradora =" & intaseguradora & ""
            sql = sql & " and estatus_vehiculo='" & strestatus & "'"
            sql = sql & " and subramo_vehiculo =" & intsubramo & ""
            sql = sql & " and status_vehiculo='1'"
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_insco_homologado_vehiculo(ByVal intvehiculo As Integer, _
   ByVal stranio As String, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select insco = insco_vehiculo , estatus = estatus_vehiculo"
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where Id_AonH = " & intvehiculo & ""
            StrSql = StrSql & " and anio_vehiculo=" & stranio & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function nombre_beneficiario(ByVal intcontrato As Integer, ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "SELECT BENEFICIARIO_PREFERENTE, enganche FROM contrato"
            sql = sql & " WHERE id_contrato =" & intcontrato & ""
            sql = sql & " and id_cliente =" & intcliente & ""
            sql = sql & " and id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function paquete_aseguradora_subramo(ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer, ByVal intaon As Integer, _
        ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal banunico As Integer, ByVal CadenaPaquete As String) As DataTable

        Try
            'emmanuell 31/10/08 evi
            'strsql = "select distinct id_paquete, descripcion_paquete"
            'strsql = strsql & " from abc_paquete"
            'strsql = strsql & " where estatus ='" & strestatus & "'"
            'strsql = strsql & " and id_aseguradora in (select distinct id_aseguradora "
            'strsql = strsql & " from ABC_PROGRAMA_ASEGURADORA"
            'strsql = strsql & " where estatus_programaaseguradora ='1'"
            'strsql = strsql & " and id_programa = " & intprograma & ")"
            'If intpaquete > 0 Then
            '    strsql = strsql & " and id_paquete=" & intpaquete & ""
            'End If
            ''inicia cambio micha
            'If intsubramo = 0 Then
            '    strsql = strsql & " and subramo in(select distinct v.subramo_vehiculo"
            '    strsql = strsql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            '    strsql = strsql & " WHERE v.id_aseguradora = a.id_aseguradora"
            '    strsql = strsql & " AND a.id_programa =" & intprograma & ""
            '    strsql = strsql & " AND v.Id_AonH = " & intaon & " "
            '    strsql = strsql & " and v.estatus_vehiculo ='" & strestatus & "' "
            '    strsql = strsql & " AND a.estatus_programaaseguradora = '1' "
            '    strsql = strsql & " AND v.status_vehiculo ='1')"
            'Else
            '    strsql = strsql & " and subramo = " & intsubramo & ""
            'End If
            ''fin micha
            ''strsql = strsql & " order by id_paquete, descripcion_paquete"
            'strsql = strsql & " order by descripcion_paquete, id_paquete"
            'emmanuell 31/10/08 evi
            StrSql = "select distinct p.id_paquete, p.descripcion_paquete, p.subramo, p.orden_paquete"
            StrSql = StrSql & " from abc_paquete p"
            If banunico = 1 Then
                StrSql = StrSql & " , COBERTURA c"
                StrSql = StrSql & " where p.descripcion_paquete = c.descripcion_cob"
            Else
                StrSql = StrSql & " where p.estatus ='" & strestatus & "'"
            End If
            StrSql = StrSql & " and p.id_aseguradora in (select distinct id_aseguradora "
            StrSql = StrSql & " from ABC_PROGRAMA_ASEGURADORA"
            StrSql = StrSql & " where estatus_programaaseguradora ='1'"
            StrSql = StrSql & " and id_programa = " & intprograma & ")"
            If intpaquete > 0 Then
                StrSql = StrSql & " and p.id_paquete=" & intpaquete & ""
            Else
                If Not CadenaPaquete = "" Then
                    StrSql = StrSql & " and p.id_paquete in (" & CadenaPaquete & ")"
                End If
            End If
            'inicia cambio micha
            If intsubramo = 0 Then
                StrSql = StrSql & " and p.subramo in(select distinct v.subramo_vehiculo"
                StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
                StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
                StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
                StrSql = StrSql & " AND v.Id_AonH = " & intaon & " "
                StrSql = StrSql & " and v.estatus_vehiculo ='" & strestatus & "' "
                StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
                StrSql = StrSql & " AND v.status_vehiculo ='1')"
            Else
                StrSql = StrSql & " and p.subramo = " & intsubramo & ""
            End If
            'fin micha
            If banunico = 1 Then
                StrSql = StrSql & " and c.num_contrato =" & intcontrato & ""
                StrSql = StrSql & " and c.id_bid = " & intbid & ""
                StrSql = StrSql & " and c.orden_cob ='0'"
            End If
            StrSql = StrSql & " order by p.orden_paquete, p.descripcion_paquete, p.id_paquete"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cuenta_paquete_aseguradora_subramo(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer) As DataTable

        Try
            'emmanuell 31/10/08
            'strsql = "select count(id_paquete)"
            'strsql = strsql & " from abc_paquete"
            'strsql = strsql & " where estatus ='" & strestatus & "'"
            'If intsubramo > 0 Then
            '    strsql = strsql & " and SUBRAMO = " & intsubramo & ""
            'End If
            'strsql = strsql & " and id_aseguradora in (select distinct id_aseguradora "
            'strsql = strsql & " from ABC_PROGRAMA_ASEGURADORA"
            'strsql = strsql & " where estatus_programaaseguradora ='1'"
            'strsql = strsql & " and id_programa = " & intprograma & ")"
            'If intpaquete > 0 Then
            '    strsql = strsql & " and id_paquete=" & intpaquete & ""
            'End If
            'strsql = strsql & " group by id_paquete"
            StrSql = "Select count(id_cobertura)"
            StrSql = StrSql & " from COBERTURA"
            StrSql = StrSql & " where num_contrato = " & intcontrato & ""
            StrSql = StrSql & " And id_bid = " & intbid & ""
            StrSql = StrSql & " and orden_cob ='0'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_poliza_recotizada(ByVal strpoliza As String, ByVal strserie As String, _
    ByVal strrenave As String, ByVal strmotor As String, ByVal strplaca As String, _
    ByVal strfactura As String, ByVal intcotizacion As Integer, ByVal intpaquete As Integer, _
    ByVal tcontrato As Integer, ByVal intcliente As Integer, ByVal intbid As Integer, _
    ByVal strtipopol As String, ByVal intprograma As Integer, ByVal strpolizaACE As String, _
    ByVal strcveFscar As String, ByVal intusuario As Integer, ByVal intaseguradora As Integer) As Integer
        Dim intpoliza As Integer = 0
        Dim dsP As New DataSet
        Dim ds As New DataSet
        Dim dsD As New DataSet
        Dim dsV As New DataSet
        Dim intvida As Integer = 0
        Dim intdesempleo As Integer = 0
        Dim strbeneficiario As String = ""
        Dim intpolizacotiza As Integer = 0
        Try

            StrSql = "select id_poliza from coti_poliza"
            StrSql = StrSql & " WHERE id_cotiza_poliza = " & intcotizacion & ""
            StrSql = StrSql & " and id_paquete =" & intpaquete & ""
            dsP = CConecta.AbrirConsulta(StrSql, True)
            intpolizacotiza = dsP.Tables(0).Rows(0)(0)

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = "select valor = isnull(max(id_poliza),0) + 1 "
            'StrSql = StrSql & " from poliza"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'intpoliza = ds.Tables(0).Rows(0)(0)

            'este se queda pendiente hasta su captura
            Dim dtA As DataTable = nombre_beneficiario(tcontrato, intcliente, intbid)
            If dtA.Rows.Count > 0 Then
                If Not dtA.Rows(0).IsNull("BENEFICIARIO_PREFERENTE") Then
                    strbeneficiario = dtA.Rows(0)("BENEFICIARIO_PREFERENTE")
                End If

            End If

            StrTransaccion = "inserta_polizaRecoti"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsPol table(id int); "
            StrSql = StrSql & " insert into poliza (Id_Poliza, Id_Bid, Id_Aseguradora, Contador_Poliza, Poliza, FVigencia," & vbCrLf
            StrSql = StrSql & " FFin, Fecha_Registro, Producto, Moneda, Socio, Marca, Modelo, Descripcion , Anio, Serie, Id_Uso, " & vbCrLf
            StrSql = StrSql & " Cobertura, Renave, Capacidad, Motor, Placas, Cobaddp1_M, Cobaddp2_M, Cobaddp3_M, Cobaddp4_M, " & vbCrLf
            StrSql = StrSql & " Cobaddp5_M, Cobaddf1_M, Cobaddf1_LC, Cobaddf1_D, Cobaddf1_F, Cobaddf2_M, Cobaddf2_LC, Cobaddf2_D," & vbCrLf
            StrSql = StrSql & " Cobaddf2_F, Cobaddf3_M, Cobaddf3_LC, Cobaddf3_D, Cobaddf3_F, Cobaddf4_M, Cobaddf4_LC, Cobaddf4_D, " & vbCrLf
            StrSql = StrSql & " Cobaddf4_F, Cobaddf5_M, Cobaddf5_LC, Cobaddf5_D, Cobaddf5_F, GastosMed, PrimaNeta, TasaFin, " & vbCrLf
            StrSql = StrSql & " DerPol, Iva, PrimaTotal, Espoliza, Plazo, Benef_Pref, datos_Adic, Fincon, Tipo_Carga, Cve_Zur," & vbCrLf
            StrSql = StrSql & " Sexo, Color, FCoper, PayType, PagFrac, Vend_APat, Vend_AMat, Vend_Nom, Captur_APat, Captur_AMat, " & vbCrLf
            StrSql = StrSql & " Captu_Nom, Camnum01, SubSidyF, SubSidyFC, SubSidyD, Precio_Ctrl, Precio_Vehiculo, Estatus," & vbCrLf
            StrSql = StrSql & " Id_Version, Fscstatus, Part_Theft_Cov, Part_Theft_CovN, Part_Theft_Ded, Part_Theft_DedN, " & vbCrLf
            StrSql = StrSql & " id_Region, Deduc_Pl, SubSidyLF, SubSidyLFC, SubSidyLD, SubSidyLS, Tipo_Poliza, Poliza_Multianual," & vbCrLf
            StrSql = StrSql & " Vigencia_FIniP, Vigencia_FFinp, Insco, Opcsa, Id_Cancelacion, Fecha_Cancelacion, Id_Paquete, " & vbCrLf
            StrSql = StrSql & " Poliza_Generada, Id_Usuario, BanSegVida, BanSegDesempleo, Num_Factura, Recargos, bandera_tipo)" & vbCrLf
            StrSql = StrSql & " select OUTPUT inserted.id_poliza into @idInsPol select (Select isnull(max(id_poliza), 0 ) + 1 from poliza With(NoLock)), Id_Bid, Id_Aseguradora, Contador_Poliza, '" & strpoliza & "', FVigencia,"
            StrSql = StrSql & " FFin, getdate(), Producto, Moneda, Socio, Marca, Modelo, Descripcion, Anio, '" & strserie & "', Id_Uso,"
            StrSql = StrSql & " Cobertura, '" & strrenave & "', Capacidad, '" & strmotor & "','" & strplaca & "', Cobaddp1_M, Cobaddp2_M, Cobaddp3_M, Cobaddp4_M,"
            StrSql = StrSql & " Cobaddp5_M, Cobaddf1_M, Cobaddf1_LC, Cobaddf1_D, Cobaddf1_F, Cobaddf2_M, Cobaddf2_LC, Cobaddf2_D, "
            StrSql = StrSql & " Cobaddf2_F, Cobaddf3_M, Cobaddf3_LC, Cobaddf3_D, Cobaddf3_F, Cobaddf4_M, Cobaddf4_LC, Cobaddf4_D,"
            StrSql = StrSql & " Cobaddf4_F, Cobaddf5_M, Cobaddf5_LC, Cobaddf5_D, Cobaddf5_F, GastosMed, PrimaNeta, TasaFin,"
            StrSql = StrSql & " DerPol, Iva, PrimaTotal, Espoliza, Plazo, '" & strbeneficiario & "', datos_Adic, Fincon, Tipo_Carga, Cve_Zur, "
            StrSql = StrSql & " Sexo, Color, FCoper, PayType, PagFrac, Vend_APat, Vend_AMat, Vend_Nom, Captur_APat, Captur_AMat,"
            StrSql = StrSql & " Captu_Nom, Camnum01, SubSidyF, SubSidyFC, SubSidyD, Precio_Ctrl, Precio_Vehiculo, Estatus,"
            StrSql = StrSql & " Id_Version, Fscstatus, Part_Theft_Cov, Part_Theft_CovN, Part_Theft_Ded, Part_Theft_DedN, "
            StrSql = StrSql & " id_Region, Deduc_Pl, SubSidyLF, SubSidyLFC, SubSidyLD, SubSidyLS, Tipo_Poliza, '" & strpoliza & "',"
            StrSql = StrSql & " Vigencia_FIniP, Vigencia_FFinp, Insco, Opcsa, Id_Cancelacion, Fecha_Cancelacion, Id_Paquete,"
            StrSql = StrSql & " Poliza_Generada, " & intusuario & ", BanSegVida, BanSegDesempleo, '" & strfactura & "', Recargos, '1'"
            StrSql = StrSql & " from coti_poliza" & vbCrLf
            StrSql = StrSql & " WHERE id_cotiza_poliza = " & intcotizacion & "" & vbCrLf
            StrSql = StrSql & " and id_paquete =" & intpaquete & "; "
            StrSql += "select * from @idInsPol; "
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                intpoliza = ds.Tables(0).Rows(0)(0)
            End If

            inserta_poliza_cliente(intpoliza, intbid, intcliente, tcontrato)

            'inserta_firmaAbajo(intpoliza, intbid, strinsco, strventa, strprecio, strseguro)

            StrSql = "insert into firmaabajo(id_poliza, id_bid, isco, tipo_venta, "
            StrSql = StrSql & " tipo_poliza, precio_auto, precio_vida)"
            StrSql = StrSql & " select " & intpoliza & ", id_bid, isco, tipo_venta, "
            StrSql = StrSql & " tipo_poliza, precio_auto, precio_vida"
            StrSql = StrSql & " from coti_firmaabajo"
            StrSql = StrSql & " where id_cotiza_poliza = " & intcotizacion & ""
            StrSql = StrSql & " and id_poliza = " & intpolizacotiza & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)


            If strtipopol = "F" Then
                'en el caso de que sea multianual determinamos el numero de fracciones
                StrSql = "insert into poliza_recibo (Id_Poliza, Id_Bid, Id_Aseguradora, Num_Fraccion, Registro, Fecha_Recibo,"
                StrSql = StrSql & " PNeta_Recibo, Recargo_Recibo, GastosExp_Recibo, Iva_Recibo, Total_Recibo, Fecha_Registro, Periodo_Recibo)"
                StrSql = StrSql & " select " & intpoliza & ",Id_Bid, Id_Aseguradora, Num_Fraccion, Registro, Fecha_Recibo,"
                StrSql = StrSql & " PNeta_Recibo, Recargo_Recibo, GastosExp_Recibo, Iva_Recibo, Total_Recibo, Fecha_Registro, Periodo_Recibo"
                StrSql = StrSql & " from coti_poliza_recibo"
                StrSql = StrSql & " WHERE id_cotiza_poliza = " & intcotizacion & ""
                StrSql = StrSql & " and id_poliza = " & intpolizacotiza & ""
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
            End If

            If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then

                'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
                'desempleo
                'StrSql = "select valor = isnull(max(id_polizaace),0) + 1 "
                'StrSql = StrSql & " from poliza_desempleo"
                ''strsql = strsql & " where id_bid = " & intbid & ""
                'dsD = CConecta.AbrirConsulta(StrSql, True)
                'intdesempleo = dsD.Tables(0).Rows(0)(0)

                StrSql = "set dateformat dmy; " & vbCrLf
                StrSql += " declare @idInsPolD table(id int); "
                StrSql = StrSql & " insert into POLIZA_desempleo (Id_PolizaAce, Id_Aseguradora, Id_Bid, Poliza, " & vbCrLf
                StrSql = StrSql & " PolizaAce, Vigencia_Inicial, Vigencia_Final, Marca, Modelo, Anio, Serie," & vbCrLf
                StrSql = StrSql & " Prima_Desempleo, Plazo_Financiamiento, Insco, Moneda, Estatus_desempleo," & vbCrLf
                StrSql = StrSql & " Fecha_Registro, Id_Version, Contador)" & vbCrLf
                StrSql = StrSql & " select OUTPUT inserted.id_polizaace into @idInsPolD select (Select isnull(max(id_polizaace), 0 ) + 1 from poliza_desempleo With(NoLock)), Id_Aseguradora, "
                StrSql = StrSql & " Id_Bid, '" & strpoliza & "', '" & strpolizaACE & "', Vigencia_Inicial, "
                StrSql = StrSql & " Vigencia_Final, Marca, Modelo, Anio, Serie, Prima_Desempleo, "
                StrSql = StrSql & " Plazo_Financiamiento, Insco, Moneda, Estatus_desempleo,"
                StrSql = StrSql & " Fecha_Registro, Id_Version, Contador"
                StrSql = StrSql & " from COTI_POLIZA_desempleo"
                StrSql = StrSql & " WHERE id_cotiza_poliza = " & intcotizacion & ""
                StrSql = StrSql & " and id_polizaace = " & intpolizacotiza & "; "
                StrSql += "select * from @idInsPolD; "
                ds = CConecta.AbrirConsulta(StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    intdesempleo = ds.Tables(0).Rows(0)(0)
                End If

                'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
                'vida
                'StrSql = "select valor = isnull(max(id_polizavida),0) + 1 "
                'StrSql = StrSql & " from poliza_vida"
                ''strsql = strsql & " where id_bid = " & intbid & ""
                'dsV = CConecta.AbrirConsulta(StrSql, True)
                'intvida = dsV.Tables(0).Rows(0)(0)

                StrSql = "set dateformat dmy; " & vbCrLf
                StrSql += " declare @idInsPolV table(id int); "
                StrSql = StrSql & " insert into POLIZA_VIDA (Id_PolizaVida, Id_Aseguradora, PolizaVida, " & vbCrLf
                StrSql = StrSql & " Vigencia_Inicial, Vigencia_Final, Moneda, Cobertura, Prima_Vida," & vbCrLf
                StrSql = StrSql & " Plazo_Financiamiento, Beneficiario, PrimaNeta_Vida, DerPol_Vida," & vbCrLf
                StrSql = StrSql & " Id_Version, Fecha_Operacion, Contador, Estatus_Vida)" & vbCrLf
                StrSql = StrSql & " select OUTPUT inserted.id_polizavida into @idInsPolV select (Select isnull(max(id_polizavida), 0 ) + 1 from poliza_vida With(NoLock)), Id_Aseguradora, '" & strpoliza & "', Vigencia_Inicial, "
                StrSql = StrSql & " Vigencia_Final, Moneda, Cobertura, Prima_Vida,"
                StrSql = StrSql & " Plazo_Financiamiento, Beneficiario, PrimaNeta_Vida, DerPol_Vida,"
                StrSql = StrSql & " Id_Version, Fecha_Operacion, Contador, Estatus_Vida"
                StrSql = StrSql & " from COTI_POLIZA_VIDA"
                StrSql = StrSql & " WHERE id_cotiza_poliza = " & intcotizacion & ""
                StrSql = StrSql & " and id_polizavida = " & intpolizacotiza & "; "
                StrSql += "select * from @idInsPolV; "
                ds = CConecta.AbrirConsulta(StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    intvida = ds.Tables(0).Rows(0)(0)
                End If

            End If

            If intprograma = 9 Then
                inserta_Fscar(intpoliza, intbid, intaseguradora, intusuario, strcveFscar, strpoliza)
            End If

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return intpoliza

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_poliza(ByVal intbid As Integer, ByVal strtipopol As String, _
    ByVal strseguro As Integer, ByVal contador_m As Integer, ByVal fechaini As String, _
    ByVal NumPlazoPoliza As Integer, ByVal strpaquete As String, _
    ByVal strsubramo As String, ByVal stropcsa As String, ByVal strdeeler As String, _
    ByVal strmoneda As String, ByVal intaonH As Integer, ByVal ArrPolisa() As String, _
    ByVal strserie As String, ByVal intuso As Integer, ByVal strcobertura As String, _
    ByVal strmotor As String, _
    ByVal strcontrato As String, ByVal strprimaneta As String, ByVal strderpol As String, _
    ByVal VLIvaSeguro As String, ByVal strprimatotal As String, ByVal bUnidadfinanciada As String, _
    ByVal strzurich As String, ByVal strcolor As String, ByVal strplazo As String, ByVal arrsub() As String, _
    ByVal strPrecio_Ctrl As String, ByVal strprecio As String, ByVal intversion As Integer, _
    ByVal strestatus As String, ByVal intpaquete As Integer, ByVal poliza As String, _
    ByVal strventa As String, ByVal intcliente As Integer, ByVal intregion As Integer, _
    ByVal intcontratopol As Integer, ByVal tcontrato As Integer, _
    ByVal intbanf As Integer, ByVal fechavalor As String, ByVal intusuario As Integer, _
    ByVal strrenave As String, ByVal intaseguradora As Integer, ByVal stranio As String, _
    ByVal bandeduc As Integer, ByVal intprograma As Integer, ByVal strfincon As String, _
    ByVal polizaAce As String, ByVal strbansegvida As Integer, ByVal strbansegdesempleo As Integer, _
    ByVal strpaqueteA As String, ByVal strfactura As String, ByVal dbiva As Double, _
    ByVal TipoFinanciamiento As String, ByVal Strfracciones As String, ByVal dbrecargo As Double, _
    ByVal banrecargo As Integer, ByVal banfactor As Integer, ByVal strCveFscar As String, _
    ByVal intcotizacionUnica As Long, ByVal plazoFinanciamiento As Integer, _
    ByVal MontoPagoFraccionado As Double, ByVal PorMontoPagoFraccionado As Double, _
    ByVal inttiporecargo As Integer, ByVal intnumrecargo As Integer, _
    ByVal PagoInicial As Double, ByVal PagoSubsecuente As Double, _
    ByVal strcadenaDano As String, ByVal IdTipoCarga As Integer, _
    ByVal tiporpoducto As Integer, ByVal IntContador As Integer, _
    ByVal StrContratoGNP As String, ByVal StrCaratulaGNP As String, Optional ByVal IdCotizaCobertura As Integer = 0) As Integer
        Dim strsql As String
        Dim valor As Integer = 0
        Dim ik As Integer
        Dim acumula_fecha As Integer
        Dim cadena As String
        Dim i As Integer
        Dim res As String = ""
        Dim DblPolizas As Double
        Dim res_tmpplazo As Integer
        Dim ValorDEDUC_PL As Double
        Dim deducpl As Double = 0
        Dim NoPolZurich As String
        Dim Temp_poliza As String
        Dim ds As New DataSet
        Dim banprimatotal As Double = 0
        Dim banprimaneta As Double = 0
        Dim IntValorPol As Integer = 0
        Dim dudc As Double = 0
        Dim acumual_fecha As Double = 0
        Dim dbFactor As Double

        Dim asfecha() As String
        Dim fechaV As String
        Dim fecha1 As String
        Dim fecha2 As String
        Dim fecha3 As String
        Dim fecha4 As String
        Dim fecha5 As String
        Dim fecha6 As String
        Dim cadenafecha As String
        Dim strinsco As String
        Dim banIvaPN As Double = 0

        Dim strmarca As String = ""
        Dim strmodelo As String = ""
        Dim dbenganche As Double = 0

        Dim banocupante As Integer = 0
        Dim PTotal As Double = 0

        Dim ArrDeeler() As String
        Dim Intcontenedor As Long = 0

        Dim cveagente As Integer = 0
        Dim contador As Integer = 0

        Try

            If strseguro = 0 Then
                'contado
                For i = 0 To 4
                    DblPolizas = DblPolizas + IIf(ArrPolisa(i) <> "", ArrPolisa(i), 0)
                Next i
            End If

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'strsql = "select valor = isnull(max(id_poliza),0) + 1 "
            'strsql = strsql & " from poliza"
            ''strsql = strsql & " where id_bid = " & intbid & ""
            'ds = CConecta.AbrirConsulta(strsql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            asfecha = fechaini.Split("/")
            If intbanf = 1 Then
                fecha3 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
            Else
                fecha3 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
            End If

            If tiporpoducto = 1 Or tiporpoducto = 2 Then
                contador = 0
            ElseIf tiporpoducto = 3 Or tiporpoducto = 4 Then
                contador = 2
            End If

            For ik = 0 To contador_m
                If ik = 0 Then
                    'strsql = "set dateformat dmy; " & vbCrLf
                    strsql = " declare @idInsPol table(id int); "
                    strsql = strsql & " insert into poliza (id_poliza, id_bid, Fecha_registro, poliza, contador_poliza, id_aseguradora) " & vbCrLf
                    strsql = strsql & " OUTPUT inserted.id_poliza into @idInsPol select (Select isnull(max(id_poliza), 0 ) + 1 from poliza With(NoLock)), " & intbid & " , getdate(),'" & poliza & "', " & IntContador & "," & intaseguradora & "; "
                    strsql += "select * from @idInsPol; "
                Else
                    'strsql = "set dateformat dmy; " & vbCrLf
                    strsql = " insert into poliza (id_poliza, id_bid, Fecha_registro, poliza, contador_poliza, id_aseguradora) " & vbCrLf
                    strsql = strsql & " values(" & valor & ", " & intbid & " , getdate(),'" & poliza & "', " & IntContador & "," & intaseguradora & ")"
                End If

                StrTransaccion = "InsertaPoliza"
                CConecta.Transaccion(StrTransaccion, True, False, True)
                ds = CConecta.AbrirConsulta(strsql)
                If ds.Tables(0).Rows.Count > 0 Then
                    valor = ds.Tables(0).Rows(0)(0)
                End If
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

                strcadenaDano = Replace(strcadenaDano, "txtIdpolizaD", valor)
                strcadenaDano = Replace(strcadenaDano, "txtLlaveC", 0)
                Intcontenedor = Registra_contenedor_reportes(intbid, poliza, "P", "")
                strcadenaDano = Replace(strcadenaDano, "txtLlave", Intcontenedor)

                contador = IntContador + 1

                If Not intaseguradora = 59 Then 'Programa diferente de Conauto (Qualitas)
                    'Obtiene la clave de agente en base a la aseguradora y al tipo de producto
                    strsql = "select distinct cveagente = isnull(clave_agente,0) "
                    strsql = strsql & " from parametro_ajustarpoliza"
                    strsql = strsql & " where id_programa = " & intprograma & ""
                    strsql = strsql & " and id_aseguradora = " & intaseguradora & ""
                    If intaseguradora = 62 Then
                        If tiporpoducto = 0 Or tiporpoducto = 1 Then
                            strsql = strsql & " and TipoProducto in (0,1)"
                            strsql = strsql & " and Contador = " & contador & ""
                        ElseIf tiporpoducto = 2 Or tiporpoducto = 3 Then
                            strsql = strsql & " and TipoProducto in (2,3)"
                        Else
                            strsql = strsql & " and TipoProducto = " & tiporpoducto & ""
                        End If
                    Else
                        strsql = strsql & " and TipoProducto = " & tiporpoducto & ""
                    End If
                    ds = CConecta.AbrirConsulta(strsql, False)
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Not ds.Tables(0).Rows(0).IsNull(0) Then
                            cveagente = ds.Tables(0).Rows(0)(0)
                        End If
                    End If
                Else
                    Select Case tiporpoducto
                        Case 1  'Multianual 
                            cveagente = 33753
                        Case 0
                            If contador = 1 Then    'Seguro gratis 1
                                cveagente = 33754

                            Else    'Seguro gratis 2 
                                cveagente = 33755
                            End If
                        Case Else
                            cveagente = 0
                    End Select
                End If


                If strseguro = 0 And Not strtipopol = "F" Then
                    'strsql = strsql & " values('A',"
                    'strsql = "set dateformat dmy update poliza set Tipo_poliza='A',"
                    strsql = " update poliza set Tipo_poliza='A',"
                Else
                    'strsql = "set dateformat dmy update poliza set Tipo_poliza='" & strtipopol & "',"
                    strsql = " update poliza set Tipo_poliza='" & strtipopol & "',"
                End If

                If contador_m = 0 Then
                    strsql = strsql & " fvigencia='" & fecha3 & "',"
                    strsql = strsql & " VIGENCIA_FINIP='" & fecha3 & "',"

                    'vida se calcula en base al numero de plazo de todo el financiamiento
                    cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fechaV = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fechaV = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If

                    If intprograma = 1 Or intprograma = 2 Then    'Conauto y Mazda
                        'Que tome los plazos enviados
                        cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    Else    'Conautopcion
                        'NumPlazoPoliza solo para multianual cuando es de contado es a 12
                        If strtipopol = "A" Then
                            'anual
                            cadenafecha = DateAdd(DateInterval.Month, 12, CDate(fechaini))
                        Else
                            'multianual
                            cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                        End If
                    End If

                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha1 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha1 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    strsql = strsql & " VIGENCIA_FFINP='" & fecha1 & "',"

                    If intprograma = 1 Or intprograma = 2 Then    'Conauto y Mazda
                        cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    Else    'Conautopcion
                        cadenafecha = DateAdd(DateInterval.Month, 12, CDate(fechaini))
                    End If
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    strsql = strsql & " ffin='" & fecha5 & "',"

                Else
                    'If ik = 0 Then
                    '    strsql = strsql & " fvigencia='" & fecha3 & "',"
                    '    strsql = strsql & " VIGENCIA_FINIP='" & fecha3 & "',"

                    '    cadenafecha = DateAdd(0, ik + 1, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    'Else
                    '    strsql = strsql & " fvigencia='" & fecha3 & "',"

                    '    cadenafecha = DateAdd(0, ik, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha4 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha4 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FINIP='" & fecha4 & "',"

                    '    cadenafecha = DateAdd(0, ik + 1, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    'End If

                    'cadenafecha = DateAdd(0, contador_m + 1, CDate(fechaini))
                    'asfecha = cadenafecha.Split("/")
                    'If intbanf = 1 Then
                    '    fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    'Else
                    '    fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    'End If
                    'strsql = strsql & " ffin='" & fecha5 & "',"

                    'nuevo evi 17/06/2009
                    strsql = strsql & " fvigencia='" & fecha3 & "',"
                    If ik = 0 Then
                        strsql = strsql & " VIGENCIA_FINIP='" & fecha3 & "',"
                    Else
                        cadenafecha = DateAdd(DateInterval.Year, (ik * 1), CDate(fechaini))
                        asfecha = cadenafecha.Split("/")
                        If intbanf = 1 Then
                            fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                        Else
                            fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                        End If
                        strsql = strsql & " VIGENCIA_FINIP='" & fecha2 & "',"
                    End If
                    If TipoFinanciamiento = "MO" Then
                        'financiado MO 25 y 37
                        If (NumPlazoPoliza = 25) Then
                            If ik = 0 Then
                                cadenafecha = DateAdd(DateInterval.Year, 1, CDate(fechaini))
                            Else
                                cadenafecha = DateAdd(DateInterval.Month, 25, CDate(fechaini))
                            End If
                            asfecha = cadenafecha.Split("/")
                            If intbanf = 1 Then
                                fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                            Else
                                fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                            End If
                            strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                        Else
                            Select Case ik
                                Case 0
                                    cadenafecha = DateAdd(DateInterval.Year, 1, CDate(fechaini))
                                Case 1
                                    cadenafecha = DateAdd(DateInterval.Year, 2, CDate(fechaini))
                                Case 2
                                    cadenafecha = DateAdd(DateInterval.Month, 37, CDate(fechaini))
                            End Select
                            asfecha = cadenafecha.Split("/")
                            If intbanf = 1 Then
                                fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                            Else
                                fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                            End If
                            strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                        End If
                    Else
                        'financiado WOR todos
                        If NumPlazoPoliza <= 12 Then
                            cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                        Else
                            If ik = 0 Then
                                acumual_fecha = NumPlazoPoliza / 12
                            End If
                            If ik <= acumual_fecha Then
                                cadenafecha = DateAdd(DateInterval.Month, ((ik + 1) * 12), CDate(fechaini))
                            Else
                                acumual_fecha = NumPlazoPoliza - ((ik + 1) * 12)
                                cadenafecha = DateAdd(DateInterval.Month, acumual_fecha, CDate(fechaini))
                            End If
                        End If
                        asfecha = cadenafecha.Split("/")
                        If intbanf = 1 Then
                            fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                        Else
                            fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                        End If
                        strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    End If
                    'nuevo evi 17/06/2009
                    cadenafecha = DateAdd(0, contador_m + 1, CDate(fechaini))
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    strsql = strsql & " ffin='" & fecha5 & "',"
                End If
                'strsql = strsql & " PRODUCTO='" & strpaquete & "',"
                strsql = strsql & " PRODUCTO='" & strpaqueteA & "',"
                strsql = strsql & " moneda='" & strmoneda & "',"

                ArrDeeler = strdeeler.Split("-")
                'StrSql = StrSql & " SOCIO='" & strdeeler & "',"
                strsql = strsql & " SOCIO='" & Trim(ArrDeeler(1)) & "',"

                Dim dtVi As DataTable = carga_insco_homologado(intaonH, stranio, intaseguradora, strestatus, strsubramo)
                If dtVi.Rows.Count > 0 Then
                    If Not dtVi.Rows(0).IsNull("insco") Then
                        strinsco = dtVi.Rows(0)("insco")
                    End If
                End If

                strsql = strsql & " insco='" & strinsco & "',"

                If IdTipoCarga = 0 Then
                    strsql = strsql & " Id_TipoCarga = null,"
                Else
                    strsql = strsql & " Id_TipoCarga=" & IdTipoCarga & ","
                End If

                Dim dtv As DataTable = vehiculo_general(strinsco, intversion, strestatus, strsubramo, stranio, intaseguradora, intaonH)
                If dtv.Rows.Count > 0 Then
                    If dtv.Rows(0).IsNull("marca") Then
                        cadena = ""
                        strmarca = ""
                    Else
                        cadena = dtv.Rows(0)("marca")
                        strmarca = cadena
                    End If
                    strsql = strsql & " marca='" & cadena & "',"

                    If dtv.Rows(0).IsNull("modelo_vehiculo") Then
                        cadena = ""
                        strmodelo = ""
                    Else
                        cadena = dtv.Rows(0)("modelo_vehiculo")
                        strmodelo = cadena
                    End If
                    strsql = strsql & " modelo='" & cadena & "',"

                    If dtv.Rows(0).IsNull("descripcion_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = dtv.Rows(0)("descripcion_vehiculo")
                    End If
                    strsql = strsql & " DESCRIPCION='" & cadena & "',"

                    If dtv.Rows(0).IsNull("anio_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = dtv.Rows(0)("anio_vehiculo")
                    End If
                    strsql = strsql & " anio='" & cadena & "',"

                    If dtv.Rows(0).IsNull("plaza_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = CInt(dtv.Rows(0)("plaza_vehiculo"))
                    End If
                    strsql = strsql & " capacidad=" & cadena & ","
                End If
                strsql = strsql & " serie='" & strserie & "',"
                strsql = strsql & " num_factura='" & strfactura & "',"
                strsql = strsql & " id_uso=" & intuso & ","
                strsql = strsql & " COBERTURA=" & CDbl(strcobertura) & ","
                strsql = strsql & " RENAVE='" & strrenave & "',"
                strsql = strsql & " motor='" & strmotor & "',"
                'strsql = strsql & " placas='" & strplacas & "',"
                'auto sustituto 'strsql = strsql & " COBADDP1_M = 0,"
                strsql = strsql & " COBADDP2_M = 0,"
                'ext. resp. civil 'strsql = strsql & " COBADDP3_M = 0,"
                strsql = strsql & " COBADDP4_M = 0,"
                strsql = strsql & " COBADDP5_M = 0,"
                Dim dtc As DataTable = cobertura_bid(intcontratopol, intbid, intaseguradora)
                If dtc.Rows.Count > 0 Then
                    For i = 0 To dtc.Rows.Count - 1
                        If dtc.Rows(i)("descripcion_cob") = "Equipo Especial" Then
                            If strseguro <> 0 Then
                                strsql = strsql & " COBADDF1_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 2) & ","
                            Else
                                strsql = strsql & " COBADDF1_M = " & Math.Round(dtc.Rows(i)("total_cob"), 2) & ","
                            End If
                            strsql = strsql & " COBADDF1_LC =" & IIf(dtc.Rows(i)("sumaaseg_cob") = "", 0, dtc.Rows(i)("sumaaseg_cob")) & ","
                            strsql = strsql & " COBADDF1_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            strsql = strsql & " COBADDF1_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Adaptacion y conv." Or _
                        dtc.Rows(i)("descripcion_cob") = "Adaptaciones y Conversiones" Then
                            If strseguro <> 0 Then
                                strsql = strsql & " COBADDF2_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 2) & ","
                            Else
                                strsql = strsql & " COBADDF2_M = " & Math.Round(dtc.Rows(i)("total_cob"), 2) & ","
                            End If
                            strsql = strsql & " COBADDF2_LC =" & dtc.Rows(i)("sumaaseg_cob") & ","
                            strsql = strsql & " COBADDF2_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            strsql = strsql & " COBADDF2_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Blindaje" Then
                            If strseguro <> 0 Then
                                strsql = strsql & " COBADDF3_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 2) & ","
                            Else
                                strsql = strsql & " COBADDF3_M = " & Math.Round(dtc.Rows(i)("total_cob"), 2) & ","
                            End If
                            strsql = strsql & " COBADDF3_LC =" & IIf(dtc.Rows(i)("sumaaseg_cob") = "", 0, dtc.Rows(i)("sumaaseg_cob")) & ","
                            strsql = strsql & " COBADDF3_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            strsql = strsql & " COBADDF3_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If

                        If dtc.Rows(i)("descripcion_cob") = "Auto Sustituto" Then
                            If strseguro <> 0 Then
                                strsql = strsql & " COBADDP1_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 2) & ","
                            Else
                                strsql = strsql & " COBADDP1_M = " & Math.Round(dtc.Rows(i)("total_cob"), 2) & ","
                            End If
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Ext. Res. Civil" Or _
                        dtc.Rows(i)("descripcion_cob") = "Extension Responsabilidad Civil" Or _
                        dtc.Rows(i)("descripcion_cob") = "Extensi�n Responsabilidad Civil" Then
                            If strseguro <> 0 Then
                                strsql = strsql & " COBADDP3_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 2) & ","
                            Else
                                strsql = strsql & " COBADDP3_M = " & Math.Round(dtc.Rows(i)("total_cob"), 2) & ","
                            End If
                        End If
                    Next
                End If
                Dim dtr As DataTable = limresp(intaseguradora, intversion, intpaquete)
                If dtr.Rows.Count > 0 Then
                    If Not dtr.Rows(0).IsNull("descripcion_suma") Then
                        res = dtr.Rows(0)("descripcion_suma")
                    End If
                    If Not dtr.Rows(0).IsNull("bandera_ocupante") Then
                        banocupante = dtr.Rows(0)("bandera_ocupante")
                    End If
                End If
                If Not res = "" Then
                    If Not intaseguradora = 1 Then
                        If banocupante > 0 Then
                            strsql = strsql & " GASTOSMED=" & Math.Round((res * cadena), 2) & ","
                        Else
                            strsql = strsql & " GASTOSMED=" & res & ","
                        End If
                    Else
                        strsql = strsql & " GASTOSMED=" & res & ","
                    End If
                End If
                strsql = strsql & " TASAFIN=0,"
                strsql = strsql & " DERPOL=" & Math.Round(CDbl(strderpol), 2) & ","

                'evi 12/03/2013 tengo que validarlo para estar seguros
                'If (strtipopol = "A" Or strtipopol = "M") And Not strseguro = 0 Then
                '    banprimatotal = strprimatotal
                'Else
                '    banprimatotal = DblPolizas
                'End If
                banprimatotal = strprimatotal

                PTotal = banprimatotal
                'If strtipopol = "A" Then
                '    strsql = strsql & " PRIMATOTAL = " & Math.Round(CDbl(strprimatotal), 2) & ","
                '    banprimatotal = strprimatotal
                'Else
                '    If strseguro = 0 Then
                '        strsql = strsql & " PRIMATOTAL = " & Math.Round(DblPolizas, 2) & ","
                '        banprimatotal = DblPolizas
                '    Else
                '        strsql = strsql & " PRIMATOTAL =" & Math.Round(CDbl(ArrPolisa(ik)), 2) & ","
                '        banprimatotal = ArrPolisa(ik)
                '    End If
                'End If
                'strsql = strsql & " PRIMANETA=" & Math.Round(((banprimatotal / 1.16) - strderpol), 2) & ","
                'banprimaneta = Math.Round(((banprimatotal / 1.16) - strderpol), 2)
                'strsql = strsql & " IVA=" & Math.Round(((banprimaneta + strderpol) * 0.16), 2) & ","

                'If strtipopol = "F" Then
                '    If banrecargo = 1 Then
                '        banprimaneta = Math.Round((((banprimatotal / (1 + dbiva)) - strderpol) / (1 + dbrecargo)), 2)
                '        banIvaPN = Math.Round(((banprimaneta * dbrecargo) + banprimaneta + strderpol) * dbiva, 2)
                '        strsql = strsql & " Recargos='" & (dbrecargo * 100) & "',"
                '    Else
                '        banprimaneta = Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 2)
                '        banIvaPN = Math.Round((banprimaneta + strderpol) * dbiva, 2)
                '    End If
                'Else
                '    If intaseguradora = 12 Then
                '        Dim dtf As DataTable = Factores(intaseguradora, ik + 1)
                '        If dtf.Rows.Count > 0 Then
                '            dbFactor = dtf.Rows(0)("valor")
                '        End If
                '        banprimaneta = Math.Round(strprimaneta * dbFactor, 2)
                '    Else
                '        banprimaneta = Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 2)
                '    End If
                '    banIvaPN = Math.Round(((banprimaneta + strderpol) * dbiva), 2)
                'End If

                strsql = strsql & " PRIMATOTAL = " & Math.Round(CDbl(banprimatotal), 2) & ","
                'banprimatotal = banprimatotal
                'banprimaneta = Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 2)
                'banIvaPN = Math.Round(((banprimaneta + strderpol) * dbiva), 2)

                ''''''''strsql = strsql & " PRIMANETA=" & Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 2) & ","

                strsql = strsql & " PRIMANETA=" & Math.Round(CDbl(strprimaneta), 2) & ","
                strsql = strsql & " IVA=" & Math.Round(CDbl(VLIvaSeguro), 2) & ","
                strsql = strsql & " ValorIva=" & dbiva & ", "
                strsql = strsql & " Pago_Fraccionado =" & Math.Round(CDbl(MontoPagoFraccionado), 2) & ", "
                strsql = strsql & " PorPago_Fraccionado =" & PorMontoPagoFraccionado & ", "

                'esto es lo nuevo para el pago inicial y subsecuentes
                strsql = strsql & " PagoInicial = " & Math.Round(CDbl(PagoInicial), 2) & ","
                strsql = strsql & " PagoSubsecuente = " & Math.Round(CDbl(PagoSubsecuente), 2) & ","

                'strsql = strsql & " PRIMANETA=" & banprimaneta & ","
                'strsql = strsql & " IVA=" & banIvaPN & ","
                'strsql = strsql & " ValorIva=" & dbiva & ", "
                '''strsql = strsql & " IVA=" & Math.Round(((banprimaneta + strderpol) * dbiva), 2) & ","

                'esto nos indica si la unidad es financiada o no 
                If bUnidadfinanciada = True Then
                    'cuando sea ald va a decir 
                    'strsql = strsql & " ESPOLIZA='F-ALD',"
                    If intprograma <> 1 And intprograma <> 2 Then
                        strsql = strsql & " ESPOLIZA='F-CONAUTO',"
                    Else
                        strsql = strsql & " ESPOLIZA='F-MAZDA',"
                    End If
                Else
                    'al final debe de aparecer C-nombre de la agencia, por el momento lo haremos desde la vista
                    strsql = strsql & " ESPOLIZA='" & strdeeler & "',"
                End If

                strsql = strsql & " PLAZO = " & NumPlazoPoliza & ", "

                'If strtipopol = "A" Then
                '    'strsql = strsql & " PLAZO = " & NumPlazoPoliza & ", "
                '    strsql = strsql & " PLAZO = 12, "
                'Else
                '    If TipoFinanciamiento = "MO" Then
                '        'financiado MO 25 y 37
                '        If NumPlazoPoliza = 25 Then
                '            If ik = 0 Then
                '                strsql = strsql & " PLAZO = 12, "
                '            Else
                '                strsql = strsql & " PLAZO = 13, "
                '            End If
                '        Else
                '            If ik = 0 Then
                '                strsql = strsql & " PLAZO = 12, "
                '            Else
                '                If ik = 1 Then
                '                    strsql = strsql & " PLAZO = 12, "
                '                Else
                '                    strsql = strsql & " PLAZO = 13, "
                '                End If
                '            End If
                '        End If
                '    Else
                '        If strtipopol = "F" Then
                '            strsql = strsql & " PLAZO = " & NumPlazoPoliza & ", "
                '        Else
                '            If strseguro = 0 Then
                '                strsql = strsql & " PLAZO = " & NumPlazoPoliza & ", "
                '            Else
                '                'financiado WOR todos
                '                If ik = 0 Then res_tmpplazo = 12
                '                If ik = 1 Then res_tmpplazo = 12 * 2
                '                If ik = 2 Then res_tmpplazo = 12 * 3
                '                If ik = 3 Then res_tmpplazo = 12 * 4
                '                If ik = 4 Then res_tmpplazo = 12 * 5

                '                If res_tmpplazo <= NumPlazoPoliza Then
                '                    strsql = strsql & " PLAZO = 12 ,"
                '                Else
                '                    strsql = strsql & " PLAZO = " & NumPlazoPoliza - (12 * ik) & ","
                '                End If
                '            End If
                '        End If
                '    End If
                'End If

                'este se queda pendiente hasta su captura
                Dim dtA As DataTable = nombre_beneficiario(tcontrato, intcliente, intbid)
                If dtA.Rows.Count > 0 Then
                    If Not dtA.Rows(0).IsNull("BENEFICIARIO_PREFERENTE") Then
                        strsql = strsql & " BENEF_PREF ='" & dtA.Rows(0)("BENEFICIARIO_PREFERENTE") & "',"
                    End If
                    If Not dtA.Rows(0).IsNull("enganche") Then
                        dbenganche = dtA.Rows(0)("enganche")
                        strsql = strsql & " enganche ='" & dbenganche & "', "
                    End If
                End If

                strsql = strsql & " FINCON ='" & strfincon & "',"

                'If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                '    strsql = strsql & " FINCON='" & strfincon & "',"
                'Else
                '    strsql = strsql & " FINCON='C',"
                'End If
                If IdTipoCarga = 0 Then
                    strsql = strsql & " Tipo_Carga='No Peligrosa',"
                Else
                    Select Case IdTipoCarga
                        Case 1
                            strsql = strsql & " Tipo_Carga='A',"
                        Case 2
                            strsql = strsql & " Tipo_Carga='B',"
                        Case 3
                            strsql = strsql & " Tipo_Carga='C',"
                    End Select
                End If
                strsql = strsql & " Cve_Zur='" & strinsco & "', "
                strsql = strsql & " COLOR='" & strcolor & "', "
                strsql = strsql & " FCOPER=getdate(),"
                strsql = strsql & " PAGFRAC=" & strplazo & ","
                If ik = 0 Then
                    strsql = strsql & " SUBSIDYF  = " & Math.Round(CDbl(arrsub(0)), 2) & ", "
                    strsql = strsql & " SUBSIDYFC  = " & Math.Round(CDbl(arrsub(1)), 2) & ", "
                    strsql = strsql & " SUBSIDYD  = " & Math.Round(CDbl(arrsub(2)), 2) & ", "
                    'emmanuell
                    strsql = strsql & " SUBSIDYLF = " & Math.Round(CDbl(ArrPolisa(9)), 2) & ","
                    strsql = strsql & " SUBSIDYLFC = " & Math.Round(CDbl(ArrPolisa(10)), 2) & ","
                    strsql = strsql & " SUBSIDYLD = " & Math.Round(CDbl(ArrPolisa(11)), 2) & ","
                    strsql = strsql & " SUBSIDYLS = " & Math.Round(CDbl(ArrPolisa(12)), 2) & ","
                Else
                    strsql = strsql & " SUBSIDYF  = 0,"
                    strsql = strsql & " SUBSIDYFC  = 0,"
                    strsql = strsql & " SUBSIDYD = 0,"
                    'emmanuell
                    strsql = strsql & " SUBSIDYLF  = 0,"
                    strsql = strsql & " SUBSIDYLFC  = 0,"
                    strsql = strsql & " SUBSIDYLD = 0,"
                    strsql = strsql & " SUBSIDYLS = 0,"
                End If

                If CDbl(arrsub(0)) > 0 Or CDbl(arrsub(1)) > 0 Or CDbl(arrsub(2)) > 0 Then
                    'subsidios f|fc|d
                    strsql = strsql & " CAMNUM01 = " & Math.Round(PTotal, 2) & ","
                Else
                    strsql = strsql & " CAMNUM01 = 0,"
                End If

                'If ik = 0 Then
                '    If ArrPolisa(5) = "" Or ArrPolisa(5) = 0 Then
                '        strsql = strsql & " CAMNUM01 = 0,"
                '    Else
                '        If Val(ArrPolisa(5)) >= Val(ArrPolisa(ik)) Then
                '            strsql = strsql & " CAMNUM01 = 0,"
                '            strsql = strsql & " SUBSIDYF  = " & Math.Round(CDbl(ArrPolisa(ik)), 2) & ", "
                '        Else
                '            strsql = strsql & " CAMNUM01 = " & Math.Round(CDbl((ArrPolisa(ik) - ArrPolisa(5))), 2) & ","
                '        End If
                '    End If
                'Else
                '    strsql = strsql & " CAMNUM01 = 0,"
                'End If

                strsql = strsql & " PRECIO_CTRL  = " & Math.Round(CDbl(strPrecio_Ctrl), 2) & ","
                strsql = strsql & " precio_vehiculo =" & Math.Round(CDbl(strprecio), 2) & ","
                strsql = strsql & " id_version=" & intversion & ","
                'strsql = strsql & " estatus='" & strestatus & "',"
                strsql = strsql & " FSCSTATUS='" & strestatus & "',"
                strsql = strsql & " OPCSA='" & stropcsa & "',"
                strsql = strsql & " id_paquete=" & intpaquete & ","
                If bandeduc = 1 Then
                    If strestatus = "N" Then
                        'Select Case intprograma
                        '    Case 1 'ALD
                        '        If intpaquete = 3 Then
                        '            strsql = strsql & " DEDUC_PL = " & Math.Round(CDbl(strPrecio_Ctrl * 0.07), 2) & ","
                        '            ValorDEDUC_PL = Math.Round(CDbl(strPrecio_Ctrl * 0.07), 2)
                        '        Else
                        '            strsql = strsql & " DEDUC_PL =" & Math.Round(CDbl(strPrecio_Ctrl * 0.06), 2) & ","
                        '            ValorDEDUC_PL = strPrecio_Ctrl * 0.06
                        '        End If
                        '    Case 3, 5, 7, 8, 9, 19, 25
                        '        '3 mazda
                        '        '5 emision (Aon Primas Nacionales)
                        '        '7 CONTADO (PANEL)
                        '        '8 MAZDA (PANEL)
                        '        Dim dtD As DataTable = determinando_impresion_deducpl(intaseguradora, intpaquete, strsubramo, intprograma)
                        '        If dtD.Rows.Count > 0 Then
                        '            If Not dtD.Rows(0).IsNull("deduc_pl") Then
                        '                dudc = dtD.Rows(0)("deduc_pl")
                        '            End If
                        '        End If
                        '        strsql = strsql & " DEDUC_PL = " & Math.Round(CDbl(strPrecio_Ctrl * dudc), 2) & ","
                        '        ValorDEDUC_PL = Math.Round(CDbl(strPrecio_Ctrl * dudc), 2)
                        'End Select
                    Else
                        Dim dtpl As DataTable = deducpl_vehiculo(intaseguradora, strinsco, stranio, intaonH)
                        If dtpl.Rows.Count > 0 Then
                            If Not dtpl.Rows(0).IsNull("deducpl_vehiculo") Then
                                deducpl = dtpl.Rows(0)("deducpl_vehiculo")
                            End If
                            strsql = strsql & " DEDUC_PL = " & deducpl & ","
                            ValorDEDUC_PL = deducpl
                        End If
                    End If
                Else
                    strsql = strsql & " DEDUC_PL = 0,"
                End If
                'micha

                'manolito
                If intprograma = 25 Then
                    Dim dtCAC As DataTable = cobertura_adicional_interiores(intaseguradora, strmodelo, stranio)
                    If dtCAC.Rows.Count > 0 Then
                        'strsql = strsql & " PART_THEFT_COV ='" & IIf(Mid(strmodelo, 1, 8) = "ECOSPORT", "$30,000", "$50,000") & "',"
                        'strsql = strsql & " PART_THEFT_COVN =" & IIf(Mid(strmodelo, 1, 8) = "ECOSPORT", 30000, 50000) & ","
                        'strsql = strsql & " Part_Theft_Ded ='$3,000',"
                        'strsql = strsql & " Part_Theft_DedN =3000,"
                        strsql = strsql & " PART_THEFT_COV ='" & dtCAC.Rows(0)("valor_suma") & "',"
                        strsql = strsql & " PART_THEFT_COVN = " & dtCAC.Rows(0)("suma") & " , "
                        strsql = strsql & " Part_Theft_Ded ='" & dtCAC.Rows(0)("deduc_suma") & "', "
                        strsql = strsql & " Part_Theft_DedN =" & dtCAC.Rows(0)("deduc") & ", "
                    Else
                        strsql = strsql & " PART_THEFT_COV ='0', "
                        strsql = strsql & " PART_THEFT_COVN =0, "
                        strsql = strsql & " Part_Theft_Ded ='0', "
                        strsql = strsql & " Part_Theft_DedN =0, "
                    End If
                Else
                    strsql = strsql & " PART_THEFT_COV ='0',"
                    strsql = strsql & " PART_THEFT_COVN =0,"
                    strsql = strsql & " Part_Theft_Ded ='0',"
                    strsql = strsql & " Part_Theft_DedN =0,"
                End If

                'evi 27/06/2013
                'se agrega el campo
                If Not tiporpoducto = -1 Then
                    strsql = strsql & " TipoProducto = " & tiporpoducto & ", "
                Else
                    strsql = strsql & " TipoProducto = null, "
                End If

                If Not cveagente = 0 Then
                    strsql = strsql & " Clave_Agente = " & cveagente & ", "
                Else
                    strsql = strsql & " Clave_Agente = null, "
                End If

                strsql = strsql & " plazoFinanciamiento=" & plazoFinanciamiento & ", "
                strsql = strsql & " cadena_impresionD='" & strcadenaDano & "', "
                strsql = strsql & " id_tiporecargo=" & inttiporecargo & ", "
                strsql = strsql & " num_pago =" & intnumrecargo & ", "

                'HM 06/03/2014 se agrega contrato y caratual GNP
                strsql = strsql & " Contrato = '" & StrContratoGNP & "', "
                strsql = strsql & " Caratula = '" & StrCaratulaGNP & "', "

                If intcotizacionUnica > 0 Then
                    strsql = strsql & " id_cotizacion =" & intcotizacionUnica & ", "
                End If
                If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                    strsql = strsql & " bansegvida='" & strbansegvida & "',"
                    strsql = strsql & " bansegdesempleo='" & strbansegdesempleo & "',"
                End If
                strsql = strsql & " id_region=" & intregion & ","
                strsql = strsql & " id_usuario =" & intusuario & ","
                If ik = 0 Then
                    strsql = strsql & " poliza_multianual = '" & poliza & "'"
                    NoPolZurich = poliza
                Else
                    Temp_poliza = poliza & "0" & ik
                    strsql = strsql & " poliza_multianual = '" & Temp_poliza & "'"
                End If
                strsql = strsql & " where id_poliza=" & valor & ""
                StrTransaccion = "ActualizaPoliza"
                CConecta.Transaccion(StrTransaccion, True, False, True)
                CConecta.EjecutaSQL(StrTransaccion, strsql)
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

                strsql += "INSERT INTO poliza_cobertura (Id_Poliza, POLIZA, Id_LimResp, orden, DESCACCESORIOS, IMPDEDPERDPARCIAL, PORCDED, SUMAASEGCOB, PRIMACOBERTURA, CODCOBERTURA, DESCCOBERTURA, CODENDOSO)"
                strsql += " select " & valor & ", '" & poliza & "', Id_Limresp, orden, DESCACCESORIOS, IMPDEDPERDPARCIAL, PORCDED, SUMAASEGCOB, PRIMACOBERTURA, CODCOBERTURA, DESCCOBERTURA, CODENDOSO " & _
                    "from cotiza_cobertura where Id_CotizaCobertura = " & IdCotizaCobertura.ToString() & " AND Id_Aseguradora = " & intaseguradora.ToString() & _
                    " and Id_Paquete = " & intpaquete.ToString() & " AND IdTipoRecargo = " & inttiporecargo & ";"

                StrTransaccion = "ActualizaCoberturas"
                CConecta.Transaccion(StrTransaccion, True, False, True)
                CConecta.EjecutaSQL(StrTransaccion, strsql)
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

                If (intaseguradora = 3 Or intaseguradora = 6) Then
                    strsql = "UPDATE P SET PrimaNeta = c.PrimaNeta, PrimaTotal = c.PrimaTotal, Iva = c.IVA, Pago_Fraccionado = c.RPPF, RC_USA = C.RC_USA_TOTAL, RCUSA_MULTI = c.RC_USA_MULTI, RCUSA_TOTAL = c.RC_USA_TOTAL, FACT_MULTI = c.FACTVIG " & _
                    "FROM POLIZA P INNER JOIN DATA_COTIZA C ON P.Id_Poliza = " & valor & " AND C.ID_COTIZACOBERTURA = " & IdCotizaCobertura.ToString() & " AND c.Id_Aseguradora = " & intaseguradora.ToString() & _
                    " AND c.Id_Paquete = " & intpaquete.ToString() & ";"

                    StrTransaccion = "ActualizaRC_USA"
                    CConecta.Transaccion(StrTransaccion, True, False, True)
                    CConecta.EjecutaSQL(StrTransaccion, strsql)
                    CConecta.Transaccion(StrTransaccion, False, True, False, True)
                End If

                strsql = ""
                strsql = "delete from PARAMETRO_AJUSTARPOLIZA"
                strsql = strsql & " where polizainicio = '" & poliza & "'"
                StrTransaccion = "EliminaPolAxa"
                CConecta.Transaccion(StrTransaccion, True, False, True)
                CConecta.EjecutaSQL(StrTransaccion, strsql)
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

                inserta_poliza_cliente(valor, intbid, intcliente, tcontrato)

                Genera_FraccionesAseguradora(valor, intbid, intaseguradora, PorMontoPagoFraccionado)

                If ik = 0 Then
                    IntValorPol = valor
                End If
                'valor = valor + 1
            Next

            inserta_firmaAbajo(IntValorPol, intbid, strinsco, strventa, strprecio, strseguro)

            If strtipopol = "F" Then
                'en el caso de que sea multianual determinamos el numero de fracciones
                inserta_registro(IntValorPol, intbid, intaseguradora, fechaini, Strfracciones, "0", strderpol)
            End If

            If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                If strbansegdesempleo = 1 Then
                    'desempleo
                    inserta_polizaACE(intaseguradora, intbid, NoPolZurich, polizaAce, _
                    fecha3, fecha1, strmarca, strmodelo, stranio, strserie, ArrPolisa(8), _
                    NumPlazoPoliza, strinsco, strmoneda, intversion)
                End If

                If strbansegvida = 1 Then
                    'vida
                    Dim coberturaVida As Double = 0
                    Dim Benevida As String = "Ford Credit de M�xico S.A. de C.V. Sociedad Financiera de Objeto Limitado"
                    Dim derpolvida As Double = 150
                    Dim primanetaVida As Double = ArrPolisa(7) - derpolvida
                    'If strfincon = "F" And strseguro = 2 Then
                    If strseguro = 2 Then
                        'financiado
                        coberturaVida = ((((strprecio - dbenganche) + banprimatotal) + CDbl(ArrPolisa(8))) + CDbl(ArrPolisa(7)))
                    Else
                        'contado
                        coberturaVida = strprecio - dbenganche
                    End If

                    inserta_polizaVida(intaseguradora, NoPolZurich, fecha3, fechaV, ArrPolisa(7), _
                    NumPlazoPoliza, strmoneda, intversion, coberturaVida, _
                    Benevida, primanetaVida, derpolvida)
                End If
                If intprograma = 9 Then
                    inserta_Fscar(IntValorPol, intbid, intaseguradora, intusuario, strCveFscar, NoPolZurich)
                End If
            End If

            If intcotizacionUnica > 0 Then
                ActCotiUnica(intcotizacionUnica)
            End If

            Return IntValorPol

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ActCotiUnica(ByVal IdCotizacionunica As Long)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_Fscar"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            StrSql = "update COTIZACION_UNICA set idemision = 1, fechaemision = getdate() "
            StrSql = StrSql & " where id_cotizacionUnica =" & IdCotizacionunica & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_Fscar(ByVal intpoliza As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal intusuario As Integer, ByVal strCveFscar As String, ByVal strpoliza As String)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_Fscar"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into poliza_fscar (id_poliza, id_bid, id_aseguradora, id_usuario, "
            sql = sql & " clave_Fscar, poliza)"
            sql = sql & " values(" & intpoliza & ", " & intbid & "," & intaseguradora & " , " & intusuario & ",'"
            sql = sql & strCveFscar & "','" & strpoliza & "')"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_polizaACE(ByVal intaseguradora As Integer, ByVal intbid As Integer, _
    ByVal strpoliza As String, ByVal strpolizaAce As String, ByVal strfechaI As String, _
    ByVal strfechaf As String, ByVal strmarca As String, ByVal strmodelo As String, ByVal intanio As Integer, _
    ByVal strserie As String, ByVal dbprima As Double, ByVal intplazo As Integer, _
    ByVal strinsco As String, ByVal strmoneda As String, ByVal intversion As Integer)
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim sql As String = ""
        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'sql = "select valor = isnull(max(id_polizaace),0) + 1 "
            'sql = sql & " from poliza_desempleo"
            ''strsql = strsql & " where id_bid = " & intbid & ""
            'ds = CConecta.AbrirConsulta(sql, False, , , True)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "inserta_polizaACE"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsPolAce table(id int); "
            sql = sql & " insert into poliza_desempleo (id_polizaACE, id_aseguradora, id_bid, poliza, polizaace," & vbCrLf
            sql = sql & " vigencia_Inicial," & vbCrLf
            sql = sql & " vigencia_final, marca, modelo, anio, serie, " & vbCrLf
            sql = sql & " prima_desempleo, plazo_financiamiento, " & vbCrLf
            sql = sql & " insco, moneda, id_version) " & vbCrLf
            sql = sql & " OUTPUT inserted.id_polizaace into @idInsPolAce select (Select isnull(max(id_polizaace), 0 ) + 1 from poliza_desempleo With(NoLock))," & intaseguradora & ","
            sql = sql & intbid & ", '" & strpoliza & "','" & strpolizaAce & "','"
            sql = sql & strfechaI & "','" & strfechaf & "','" & strmarca & "','"
            sql = sql & strmodelo & "'," & intanio & ",'" & strserie & "',"
            sql = sql & dbprima & ", " & intplazo & ",'" & strinsco & "','"
            sql = sql & strmoneda & "'," & intversion & "; "
            StrSql += "select * from @idInsPolAce; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_polizaVida(ByVal intaseguradora As Integer, _
    ByVal strpoliza As String, ByVal strfechaI As String, _
    ByVal strfechaf As String, ByVal dbprimavida As Double, ByVal intplazo As Integer, _
    ByVal strmoneda As String, ByVal intversion As Integer, ByVal dbcobertura As Double, _
    ByVal strbenef As String, ByVal dbprimanetavida As Double, ByVal dbderpolvida As Double)
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim sql As String = ""
        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'sql = "select valor = isnull(max(id_polizavida),0) + 1 "
            'sql = sql & " from poliza_vida"
            ''strsql = strsql & " where id_bid = " & intbid & ""
            'ds = CConecta.AbrirConsulta(sql, False, , , True)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "inserta_polizaVida"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsPolVida table(id int); "
            sql = sql & " insert into poliza_vida (id_polizavida, id_aseguradora, " & vbCrLf
            sql = sql & " polizavida, vigencia_inicial, vigencia_final, moneda, " & vbCrLf
            sql = sql & " cobertura, prima_vida , plazo_financiamiento, beneficiario, " & vbCrLf
            sql = sql & " primaneta_vida, derpol_vida, id_version)" & vbCrLf
            sql = sql & " OUTPUT inserted.id_polizavida into @idInsPolVida select (Select isnull(max(id_polizavida), 0 ) + 1 from poliza_vida With(NoLock))," & intaseguradora & ",'"
            sql = sql & strpoliza & "','"
            sql = sql & strfechaI & "','" & strfechaf & "','" & strmoneda & "',"
            sql = sql & dbcobertura & "," & dbprimavida & "," & intplazo & ",'"
            sql = sql & strbenef & "'," & dbprimanetavida & ", " & dbderpolvida & "," & intversion & "; "
            StrSql += "select * from @idInsPolVida; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_registro(ByVal intpoliza As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal strfechaF As String, ByVal strfraccion As String, ByVal strTipoRegistro As Char, ByVal dbderpol As Double)
        Dim i As Integer = 0
        Dim arr() As String = strfraccion.Split("|")
        Dim cadenafecha As String = ""
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_registro"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To arr.Length - 1
                Dim arrFraccion() As String = arr(i).Split("*")
                If Not arrFraccion(0) = "" Then
                    cadenafecha = DateAdd(DateInterval.Year, i, CDate(strfechaF))
                    sql = " insert into poliza_recibo (id_poliza, id_bid, id_aseguradora, "
                    sql = sql & " num_fraccion, registro, Fecha_Recibo, Iva_Recibo, Total_Recibo, "
                    sql = sql & " Pneta_recibo, recargo_recibo, periodo_recibo, GastosExp_recibo)"
                    sql = sql & " values(" & intpoliza & " , " & intbid & ", " & intaseguradora & ", "
                    sql = sql & arrFraccion(0) & ",'" & strTipoRegistro & "','" & cadenafecha & "', '"
                    sql = sql & arrFraccion(1) & "','" & arrFraccion(2) & "','"
                    sql = sql & arrFraccion(3) & "','" & arrFraccion(5) & "',"
                    If i = 0 Then
                        sql = sql & arrFraccion(4) & "," & dbderpol & ")"
                    Else
                        sql = sql & arrFraccion(4) & ",0)"
                    End If
                    CConecta.EjecutaSQL(StrTransaccion, sql)
                End If
            Next
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function determina_polizas_VD(ByVal intpoliza As Integer) As DataTable

        Try
            StrSql = "select vida= isnull(bansegvida,0), "
            StrSql = StrSql & " desempleo = isnull(bansegdesempleo,0)"
            StrSql = StrSql & " from poliza "
            StrSql = StrSql & " where id_poliza =" & intpoliza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function imprime_abcreportes(ByVal intprograma As Integer, ByVal strfincon As String, _
    ByVal intpoliza As Integer) As DataTable

        Try
            StrSql = " select id_reporte, nombre_reporte"
            StrSql = StrSql & " from ABC_REPORTE"
            StrSql = StrSql & " where estatus_reporte='1'"
            If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                StrSql = StrSql & "and not id_reporte = 1 "
            End If
            'If intprograma = 9 Then
            '    strsql = strsql & " union"
            '    strsql = strsql & " select id_reporte= 5, nombre_reporte ='Carta Renovaci�n'"
            '    strsql = strsql & " union"
            '    strsql = strsql & " select id_reporte= 6, nombre_reporte ='Recibos'"
            'End If
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                Dim dt As DataTable = determina_polizas_VD(intpoliza)
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("vida") = 1 Then
                        StrSql = StrSql & " union"
                        StrSql = StrSql & " select id_reporte= 3, nombre_reporte ='Vida'"
                    End If
                    If dt.Rows(0)("desempleo") = 1 Then
                        StrSql = StrSql & " union"
                        StrSql = StrSql & " select id_reporte= 4, nombre_reporte ='Desempleo'"
                    End If
                End If
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function valida_vida_desempelo_impresion(ByVal intpoliza As Integer) As DataTable

        Try
            Return determina_polizas_VD(intpoliza)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_reporte_poliza(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal intcliente As Integer, ByVal intcontrato As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select distinct * from VW_REPORTE_POLIZA_CONTADO"
            StrSql = StrSql & " where id_poliza = " & intpoliza & ""
            'strsql = strsql & " and id_bid = " & intbid & ""
            'strsql = strsql & " and id_cliente = " & intcliente & ""
            'strsql = strsql & " and id_contrato = " & intcontrato & ""
            'strsql = strsql & " and id_aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function carga_reporte_poliza_coti(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    'ByVal intcliente As Integer, ByVal intcontrato As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = "select * from VW_REPORTE_POLIZA_COTI"
    '        strsql = strsql & " where id_poliza = " & intpoliza & ""
    '        strsql = strsql & " and id_bid = " & intbid & ""
    '        strsql = strsql & " and id_cliente = " & intcliente & ""
    '        strsql = strsql & " and id_contrato = " & intcontrato & ""
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    Public Function carga_reporte_poliza_coti(ByVal intcotizacion As Integer, ByVal intbid As Integer, _
    ByVal intcliente As Integer, ByVal numcontrato As Integer, _
    ByVal intprograma As Integer, ByVal intanio As Integer, ByVal strfincon As String) As DataTable
        ' ByVal strestatus As String, ByVal intanio As Integer

        Try
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                StrSql = "select distinct *, "
                StrSql = StrSql & " modelo = modelo_vehiculo,"
                StrSql = StrSql & " catalogo= catalogo_vehiculo,"
                StrSql = StrSql & " descripcion_homologado = descripcion_vehiculo,"
                StrSql = StrSql & " anio = anio_vehiculo,"
                StrSql = StrSql & " estatus = estatus_vehiculo"
                StrSql = StrSql & " from VW_CARGA_COTIZACION_CLIENTE"
            Else
                StrSql = "select * from VW_CARGA_COTIZACION_CLIENTE_P"
            End If
            StrSql = StrSql & " where Id_cotizacion = " & intcotizacion & ""
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " and id_clientep = " & intcliente & ""
            StrSql = StrSql & " and num_contrato = " & numcontrato & ""
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                StrSql = StrSql & " and id_programa =" & intprograma & ""
                'strsql = strsql & " and estatus_vehiculo='" & strestatus & "'"
                StrSql = StrSql & " and anio_vehiculo=" & intanio & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function reporte_limpresp(ByVal intversion As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = " select cobertura, descripcion_suma, deducible, orden "
            StrSql = StrSql & " from limresp"
            StrSql = StrSql & " where id_version = " & intversion & ""
            StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function reporte_limpresp_poliza(ByVal intversion As Integer, _
        ByVal intpaquete As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, _
        ByVal intaseguradora As Integer, ByVal strestatus As String) As DataTable

        Try
            StrSql = " select cobertura = descripcion_cob, descripcion_suma = sumaaseg_cob, "
            StrSql = StrSql & " deducible = deducible_cob, total_cob, orden= orden_cob"
            StrSql = StrSql & " from COBERTURA"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            StrSql = StrSql & " and num_contrato = " & intcontrato & ""
            'strsql = strsql & " and id_cobertura <> 0"

            StrSql = StrSql & " and not descripcion_cob in( "
            StrSql = StrSql & " select distinct descripcion_paquete from abc_paquete"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and estatus ='" & strestatus & "')"

            StrSql = StrSql & " and total_cob <> '0'"
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " union"
            StrSql = StrSql & " select cobertura, descripcion_suma, deducible, total_cob = 0, orden "
            StrSql = StrSql & " from limresp"
            StrSql = StrSql & " where id_version = " & intversion & ""
            StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            StrSql = StrSql & " and estatus_limresp='1'"
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_abccancelacion() As DataTable

        Try
            StrSql = " select id_cancelacion=0, descripcion_cancelacion ='-- Seleccione --' union"
            StrSql = StrSql & " select id_cancelacion, descripcion_cancelacion "
            StrSql = StrSql & " from abc_cancelacion"
            StrSql = StrSql & " where estatus_cancelacion = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function dia_cancelacion(ByVal intPrograma As Integer) As String
        Dim arrfecha() As String
        Dim DiasCancelacion As Integer
        Dim ds As New DataSet

        StrSql = "select DiasCancelacion_Programa "
        StrSql = StrSql & " from abc_programa"
        StrSql = StrSql & " where id_programa = " & intPrograma & ""
        StrSql = StrSql & " and estatus_programa = '1'"
        ds = CConecta.AbrirConsulta(StrSql)
        If ds.Tables(0).Rows.Count > 0 Then
            If Not ds.Tables(0).Rows(0).IsNull("DiasCancelacion_Programa") Then
                DiasCancelacion = ds.Tables(0).Rows(0)("DiasCancelacion_Programa")
            End If
        End If

        'Dim strfecha As String = DateAdd(DateInterval.Day, -3, CDate(Now.Date.Day & "/" & Now.Date.Month & "/" & Now.Date.Year))
        Dim strfecha As String = DateAdd(DateInterval.Day, -DiasCancelacion, CDate(Now.Date.Day & "/" & Now.Date.Month & "/" & Now.Date.Year))
        'arrfecha = strfecha.Split("/")
        'strfecha = arrfecha(1) & "/" & arrfecha(0) & "/" & arrfecha(2)
        Return strfecha

    End Function

    Private Function complementa_fecha(ByVal strfecha As String) As String
        Dim fecha As String
        If strfecha.Length = 1 Then
            fecha = "0" & strfecha.Trim
        Else
            fecha = strfecha
        End If
        Return fecha
    End Function

    Public Function carga_cancelacion(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal Intnivel As Integer, Optional ByVal StrCadena As String = "", Optional ByVal StrCriterios As String = "") As DataTable
        'Dim StrFechaActualF As String = ""
        'Dim StrFechaActualI As String = dia_cancelacion(intprograma) & " 00:00:00"

        'If intprograma = 29 Then 'Conauto
        '    'StrFechaActualF = complementa_fecha(Now.Date.Day) + 3 & "/" & complementa_fecha(Now.Date.Month) & "/" & Now.Date.Year & " 23:59:59"
        '    StrFechaActualF = complementa_fecha(Now.Date.Day) & "/" & complementa_fecha(Now.Date.Month) & "/" & Now.Date.Year
        '    StrFechaActualF = DateAdd(DateInterval.Day, 3, CDate(StrFechaActualF)) & " 23:59:59"
        'Else 'conauto opcion
        '    StrFechaActualF = complementa_fecha(Now.Date.Day) & "/" & complementa_fecha(Now.Date.Month) & "/" & Now.Date.Year & " 23:59:59"
        'End If

        Try
            StrCadena = Replace(StrCadena, " ", "%")

            StrSql = "SET DATEFORMAT dmy"
            StrSql = StrSql & " SELECT DISTINCT POLIZA.Poliza, POLIZA.Serie, POLIZA.Motor, POLIZA.Placas, POLIZA.Marca, POLIZA.Modelo, POLIZA.Anio, "
            StrSql = StrSql & " POLIZA.Descripcion, ISNULL(CLIENTE.Nombre, '') + ' ' + ISNULL(CLIENTE.APaterno, '') + ' ' + ISNULL(CLIENTE.AMaterno, '') "
            StrSql = StrSql & " AS NombreCliente, POLIZA.Id_Cancelacion, contrato.num_contrato, POLIZA.Id_Aseguradora "
            StrSql = StrSql & " FROM POLIZA INNER JOIN"
            StrSql = StrSql & " ABC_PROGRAMA_ASEGURADORA ON POLIZA.Id_Aseguradora = ABC_PROGRAMA_ASEGURADORA.Id_Aseguradora INNER JOIN"
            StrSql = StrSql & " CLIENTE_POLIZA ON POLIZA.Id_Poliza = CLIENTE_POLIZA.Id_Poliza INNER JOIN"
            StrSql = StrSql & " CLIENTE ON CLIENTE_POLIZA.Id_Cliente = CLIENTE.Id_Cliente AND CLIENTE_POLIZA.Id_Bid = CLIENTE.Id_Bid "
            StrSql = StrSql & " INNER JOIN CONTRATO ON CLIENTE_POLIZA.Id_Contrato = CONTRATO.Id_Contrato AND "
            StrSql = StrSql & " CLIENTE_POLIZA.Id_Cliente = CONTRATO.Id_cliente AND "
            StrSql = StrSql & " CLIENTE_POLIZA.Id_Bid = CONTRATO.Id_Bid LEFT OUTER JOIN "
            StrSql = StrSql & " POLCANCELADA ON POLIZA.Id_Poliza <> POLCANCELADA.Id_Poliza"
            StrSql = StrSql & " GROUP BY POLIZA.Poliza, POLIZA.Serie, POLIZA.Motor, POLIZA.Placas, POLIZA.Marca, POLIZA.Modelo, POLIZA.Anio, "
            StrSql = StrSql & " POLIZA.Descripcion, POLIZA.Id_Bid, ABC_PROGRAMA_ASEGURADORA.Id_Programa, ISNULL(CLIENTE.Nombre, '') "
            StrSql = StrSql & " + ' ' + ISNULL(CLIENTE.APaterno, '') + ' ' + ISNULL(CLIENTE.AMaterno, ''), POLIZA.Id_Cancelacion, poliza.fecha_registro, "
            StrSql = StrSql & " POLIZA.Id_Cancelacion, contrato.num_contrato, POLIZA.Id_Aseguradora "
            StrSql = StrSql & " HAVING POLIZA.Id_Cancelacion IS NULL"
            StrSql = StrSql & " AND ABC_PROGRAMA_ASEGURADORA.Id_Programa = " & intprograma & ""
            'If Intnivel > 0 Then
            '    StrSql = StrSql & " AND poliza.fecha_registro between  '" & StrFechaActualI & "' AND '" & StrFechaActualF & "'"
            'End If
            If intbid = 34 Then
                StrSql = StrSql & " AND POLIZA.Id_Bid in(34,70)"
            Else
                If intbid > 0 Then
                    StrSql = StrSql & " AND POLIZA.Id_Bid = " & intbid & ""
                End If
            End If
            If StrCriterios <> "" Then
                StrSql = StrSql & " AND ("

                If InStr(StrCriterios, "1") Then
                    StrSql = StrSql & " POLIZA.Serie LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "2") Then
                    StrSql = StrSql & " POLIZA.Motor LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "3") Then
                    StrSql = StrSql & " POLIZA.placas LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "4") Then
                    StrSql = StrSql & " POLIZA.marca LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "5") Then
                    StrSql = StrSql & " POLIZA.modelo LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "6") Then
                    StrSql = StrSql & " ISNULL(CLIENTE.Nombre, '') + ' ' + ISNULL(CLIENTE.APaterno, '') + ' ' + ISNULL(CLIENTE.AMaterno, '') LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "7") Then
                    StrSql = StrSql & " POLIZA.poliza = '" & StrCadena & "' OR "
                End If

                If InStr(StrCriterios, "8") Then
                    Dim IdAseguradora As Integer
                    Dim StrSqlA As String

                    StrSqlA = "select pa.id_aseguradora from abc_aseguradora a, abc_programa_aseguradora pa"
                    StrSqlA = StrSqlA & " where a.id_aseguradora = pa.id_aseguradora"
                    StrSqlA = StrSqlA & " and pa.id_programa = 32"
                    StrSqlA = StrSqlA & " and a.descripcion_aseguradora  = '" & StrCadena & "'"
                    Dim ds As DataSet = CConecta.AbrirConsulta(StrSqlA, False)
                    IdAseguradora = ds.Tables(0).Rows(0)(0)

                    StrSql = StrSql & " POLIZA.Id_Aseguradora = '" & IdAseguradora & "' OR "
                End If

                StrSql = Trim(Left(StrSql, Len(StrSql) - 3))
                StrSql = StrSql & ")"
            End If

            StrSql = StrSql & " ORDER BY POLIZA.Poliza"

            StrSqlSalida = StrSql
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function BuscaPoliza(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable

        Try
            StrCadena = Replace(StrCadena, " ", "%")

            StrSql = "SELECT * "
            StrSql = StrSql & " FROM VW_CONSULTA_POLIZA "
            StrSql = StrSql & " WHERE Id_Programa = " & intprograma & ""
            If intbid > 0 Then
                If intbid = 34 Then
                    StrSql = StrSql & " AND Id_Bid in(34,70)"
                Else
                    StrSql = StrSql & " AND Id_Bid = " & intbid & ""
                End If
            End If

            If StrCriterios <> "" Then
                StrSql = StrSql & " AND ("

                If InStr(StrCriterios, "1") Then
                    StrSql = StrSql & " Serie LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "2") Then
                    StrSql = StrSql & " Motor LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "3") Then
                    StrSql = StrSql & " placas LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "4") Then
                    StrSql = StrSql & " marca LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "5") Then
                    StrSql = StrSql & " modelo LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "6") Then
                    StrSql = StrSql & "nombrecliente LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "7") Then
                    StrSql = StrSql & "poliza = '" & StrCadena & "' OR "
                End If

                If InStr(StrCriterios, "8") Then
                    Dim IdAseguradora As Integer
                    Dim StrSqlA As String

                    StrSqlA = "select pa.id_aseguradora from abc_aseguradora a, abc_programa_aseguradora pa"
                    StrSqlA = StrSqlA & " where a.id_aseguradora = pa.id_aseguradora"
                    StrSqlA = StrSqlA & " and pa.id_programa = 32"
                    StrSqlA = StrSqlA & " and a.descripcion_aseguradora  = '" & StrCadena & "'"
                    Dim ds As DataSet = CConecta.AbrirConsulta(StrSqlA, False)
                    IdAseguradora = ds.Tables(0).Rows(0)(0)

                    StrSql = StrSql & " Id_Aseguradora = '" & IdAseguradora & "' OR "
                End If

                StrSql = Trim(Left(StrSql, Len(StrSql) - 3))
                StrSql = StrSql & ")"
            End If

            StrSqlSalida = StrSql
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    'evi 06/09/2011 cancelacion usuario
    'VW_SCRIPT_CANCELACION
    'VW_CONSULTA_CANCELACION
    Public Function BuscaPoliza_cancelada(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable

        Try
            StrCadena = Replace(StrCadena, " ", "%")

            StrSql = "SELECT nom = case when not nom = '' then nom else nombre end, "
            StrSql = StrSql & " * FROM VW_CONSULTA_CANCELACION"
            StrSql = StrSql & " where Id_Programa = " & intprograma & ""
            If intbid > 0 Then
                If intbid = 34 Then
                    StrSql = StrSql & " AND Id_Bid in(34,70)"
                Else
                    StrSql = StrSql & " AND Id_Bid = " & intbid & ""
                End If
            End If

            If StrCriterios <> "" Then
                StrSql = StrSql & " AND ("

                If InStr(StrCriterios, "1") Then
                    StrSql = StrSql & " Serie LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "2") Then
                    StrSql = StrSql & " Motor LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "3") Then
                    StrSql = StrSql & " placas LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "4") Then
                    StrSql = StrSql & " marca LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "5") Then
                    StrSql = StrSql & " modelo LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "6") Then
                    StrSql = StrSql & "asegurado LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "7") Then
                    StrSql = StrSql & "poliza = '" & StrCadena & "' OR "
                End If

                If InStr(StrCriterios, "8") Then
                    Dim IdAseguradora As Integer
                    Dim StrSqlA As String

                    StrSqlA = "select pa.id_aseguradora from abc_aseguradora a, abc_programa_aseguradora pa"
                    StrSqlA = StrSqlA & " where a.id_aseguradora = pa.id_aseguradora"
                    StrSqlA = StrSqlA & " and pa.id_programa = 32"
                    StrSqlA = StrSqlA & " and a.descripcion_aseguradora  = '" & StrCadena & "'"
                    Dim ds As DataSet = CConecta.AbrirConsulta(StrSqlA, False)
                    IdAseguradora = ds.Tables(0).Rows(0)(0)

                    StrSql = StrSql & " Id_Aseguradora = '" & IdAseguradora & "' OR "
                End If

                StrSql = Trim(Left(StrSql, Len(StrSql) - 3))
                StrSql = StrSql & ")"
            End If

            StrSqlSalida = StrSql
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function valida_vida_desempleo_consulta(ByVal strsqlSalida As String) As DataTable

        Dim formateaCadena As String = strsqlSalida.Replace("*", "id_poliza")
        Try
            StrSql = "select distinct bansegvida, bansegdesempleo "
            StrSql = StrSql & " from poliza"
            StrSql = StrSql & " where id_poliza in (" & formateaCadena & ")"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function contador_polizas(ByVal strpoliza As String, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select contador= count(poliza), tipo_poliza from poliza"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            If intbid > 0 Then
                StrSql = StrSql & " and id_bid=" & intbid & ""
            End If
            StrSql = StrSql & " and not fvigencia is null "
            StrSql = StrSql & " group by poliza, tipo_poliza"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_idpoliza(ByVal strpoliza As String, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select id_poliza, id_aseguradora from poliza"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and not fvigencia is null"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 06/09/2011 cancelacion usuario
    Public Sub inserta_cancelpol(ByVal strIdPoliza As String, ByVal intbid As Integer, ByVal strserie As String, _
    ByVal intcancelacion As Integer, ByVal strtipo As String, ByVal intcontador As Integer, _
    ByVal strpoliza As String, ByVal intaseguradora As Integer, ByVal idusuario As Integer)
        Dim ds As New DataSet
        Dim i As Integer
        Dim baninterface As Integer = 0
        Dim arrpol() As String = strIdPoliza.Split(",")
        Try

            StrSql = "select interface, serie "
            StrSql = StrSql & " from poliza"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and not fvigencia is null"
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                If Not ds.Tables(0).Rows(0).IsNull("interface") Then
                    baninterface = ds.Tables(0).Rows(0)("interface")
                End If
                If Not ds.Tables(0).Rows(0).IsNull("serie") Then
                    strserie = ds.Tables(0).Rows(0)("serie")
                End If
            End If

            StrTransaccion = "InsertaCancelPol"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To intcontador - 1
                If Not arrpol(i) = "" Then
                    StrSql = " insert into POLCANCELADA (id_poliza, id_bid, serie, fecoper,"
                    StrSql = StrSql & " id_cancelacion, tipo_poliza, poliza, id_aseguradora, id_usuario)"
                    StrSql = StrSql & " values (" & arrpol(i) & "," & intbid & ",'" & strserie & "',"
                    StrSql = StrSql & " getdate(), "
                    StrSql = StrSql & intcancelacion & ", '" & strtipo & "', '" & strpoliza & "'," & intaseguradora & ", " & idusuario & ")"
                    CConecta.EjecutaSQL(StrTransaccion, StrSql)
                End If
            Next

            StrSql = " update poliza set id_cancelacion = " & intcancelacion & ","
            StrSql = StrSql & " fecha_cancelacion = getdate(),"
            If baninterface = 1 Then
                'Esta bandera se actualizaba porque se ocupaba para FIAA, pero ya no se envia la interface a FIAA
                'StrSql = StrSql & " interface = 0 ,"
                StrSql = StrSql & " procesada = 1 ,"
            End If
            StrSql = StrSql & " estatus='0'"
            StrSql = StrSql & " where poliza= '" & strpoliza & "'"
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " and not fvigencia is null"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)

            StrSql = "update poliza_vida set estatus_vida='0'"
            StrSql = StrSql & " where polizavida = '" & strpoliza & "'"
            StrSql = StrSql & " and id_aseguradora = " & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)

            StrSql = "update poliza_desempleo set Estatus_desempleo = '0'"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and id_bid= " & intbid & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function consulta_polzas(ByVal strpoliza As String, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select top 1 p.poliza, p.fvigencia, p.ffin, p.fecha_registro, p.descripcion, "
            StrSql = StrSql & " p.modelo, p.marca, p.anio, p.serie, p.renave, p.capacidad, p.primatotal, "
            StrSql = StrSql & " p.primaneta , p.placas, p.motor, p.plazo, p.precio_vehiculo, p.tipo_poliza, "
            StrSql = StrSql & " p.id_usuario, u.nombre, a.descripcion_aseguradora"
            StrSql = StrSql & " from poliza p , usuario u, abc_aseguradora a"
            StrSql = StrSql & " where p.id_usuario = u.id_usuario"
            StrSql = StrSql & " and p.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and poliza ='" & strpoliza & "'"
            StrSql = StrSql & " and id_bid =" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function consulta_cancelacion(ByVal intbid As Integer) As DataTable

        Try
            StrSql = " SELECT DISTINCT POLIZA.Poliza, POLIZA.Serie, POLIZA.Marca, POLIZA.Modelo, POLIZA.Anio, POLIZA.Descripcion, "
            StrSql = StrSql & " ABC_CANCELACION.Descripcion_Cancelacion, ISNULL(POLCANCELADA.USUARIO_NOM, '') "
            StrSql = StrSql & " + ' ' + ISNULL(POLCANCELADA.USUARIO_APAT, '') + ' ' + ISNULL(POLCANCELADA.USUARIO_AMAT, '') AS nom, "
            StrSql = StrSql & " POLIZA.Fecha_Cancelacion, ABC_ASEGURADORA.Descripcion_Aseguradora"
            StrSql = StrSql & " FROM ABC_ASEGURADORA INNER JOIN"
            StrSql = StrSql & " POLIZA ON ABC_ASEGURADORA.Id_Aseguradora = POLIZA.Id_Aseguradora LEFT OUTER JOIN"
            StrSql = StrSql & " ABC_CANCELACION INNER JOIN"
            StrSql = StrSql & " POLCANCELADA ON ABC_CANCELACION.Id_Cancelacion = POLCANCELADA.Id_Cancelacion ON "
            StrSql = StrSql & " POLIZA.Id_Poliza = POLCANCELADA.Id_Poliza"
            StrSql = StrSql & " GROUP BY POLIZA.Poliza, POLIZA.Serie, POLIZA.Marca, POLIZA.Modelo, POLIZA.Anio, POLIZA.Descripcion, POLIZA.Id_Bid, "
            StrSql = StrSql & " ABC_CANCELACION.Descripcion_Cancelacion, ISNULL(POLCANCELADA.USUARIO_NOM, '') "
            StrSql = StrSql & " + ' ' + ISNULL(POLCANCELADA.USUARIO_APAT, '') + ' ' + ISNULL(POLCANCELADA.USUARIO_AMAT, ''), POLIZA.Fecha_Cancelacion, ABC_ASEGURADORA.Descripcion_Aseguradora"
            StrSql = StrSql & " HAVING POLIZA.Serie = 'Cancelada'"
            StrSql = StrSql & " AND POLIZA.Id_Bid = " & intbid & ""
            StrSql = StrSql & " ORDER BY POLIZA.Poliza"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_contenedor_reportes(ByVal intbid As Integer, ByVal strpoliza As String, ByVal strtipo As String) As Integer
        Dim ds As New DataSet
        Dim valor As Integer

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select clave = isnull(max(cve_reporte),0)+1 from contenedor_reporte"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    valor = ds.Tables(0).Rows(0)("clave")
            'End If

            StrTransaccion = "InsertaContReportes"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsRep table(id int); "
            StrSql = StrSql & " insert into contenedor_reporte (cve_reporte, estatus, id_bid, poliza, tipo)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.cve_reporte into @idInsRep select (Select isnull(max(cve_reporte), 0 ) + 1 from contenedor_reporte With(NoLock)),'0'," & intbid & ",'" & strpoliza & "','" & strtipo & "'; "
            StrSql += "select * from @idInsRep; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)
            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_contenedor_reportes_grupo(ByVal valor As Integer, ByVal intbid As Integer, ByVal strpoliza As String, ByVal strtipo As String) As Integer

        Try
            StrTransaccion = "InsertaContRepGrupo"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " insert into contenedor_reporte (cve_reporte, estatus, id_bid, poliza, tipo)"
            StrSql = StrSql & " values(" & valor & ",'0'," & intbid & ",'" & strpoliza & "','" & strtipo & "')"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_tabla_impresion(ByVal llave As Integer) As DataTable

        Try
            StrSql = "select nombre_reporte, url_reporte, url_cartarenovacion, url_recibo, tipo"
            StrSql = StrSql & " from CONTENEDOR_REPORTE"
            StrSql = StrSql & " where cve_reporte = " & llave & ""
            StrSql = StrSql & " and estatus = '1'"
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function Consulta_contenedor(ByVal llave As Integer, ByVal llave1 As Integer) As DataTable

        Try
            StrSql = "select * from contenedor_reporte"
            StrSql = StrSql & " where cve_reporte in(" & llave & "," & llave1 & ")"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function sacando_poliza(ByVal intpoliza As Integer) As DataTable

        Try
            StrSql = " select poliza from poliza"
            StrSql = StrSql & " where id_poliza = " & intpoliza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Datos_aseguradora_poliza(ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = " select * from ABC_ASEGURADORA"
            StrSql = StrSql & " where ID_ASEGURADORA = " & intaseguradora & ""
            StrSql = StrSql & " and estatus_aseguradora ='1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function contenedor_poliza(ByVal strpoliza As String, ByVal strtipo As String, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " select nombre_reporte, url_reporte from contenedor_reporte"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and tipo = '" & strtipo & "'"
            StrSql = StrSql & " and id_bid = " & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function contenedor_poliza_general(ByVal strpoliza As String, ByVal intbid As Integer, ByVal strtipo As String) As DataTable

        Try
            StrSql = " select nombre_reporte, tipo, url_reporte"
            StrSql = StrSql & " from contenedor_reporte"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " and estatus ='1'"
            StrSql = StrSql & " and tipo ='" & strtipo & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function maximo_cotiza() As DataTable

        Try
            StrSql = " select valor = isnull(max(id_cotizacion),0) + 1 from COTIZA_CLIENTE"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_cotiza(ByVal intcotiza As Integer, ByVal intcliente As Integer, _
    ByVal intvehiculo As Integer, ByVal strpaquete As String, ByVal intnum As Integer, _
    ByVal intplazo As Integer, ByVal dbprecio As Double, ByVal strtipo As String, _
    ByVal intbid As Integer, ByVal strtaza As String, ByVal dbenganche As Double, _
    ByVal strtipoplan As String, ByVal intprograma As Integer, _
    ByVal strcontrato As String, ByVal strestatus As String, _
    ByVal intanio As Integer, ByVal strCveFscar As String)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim arr() As String = strpaquete.Split("|")

        Try
            StrTransaccion = "insertaCotiza"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To arr.Length - 1
                Dim arrpaquete() As String = arr(i).Split("*")
                If IsNumeric(arrpaquete(1)) Then
                    StrSql = " insert into COTIZA_CLIENTE(id_cotizacion, id_paquete, "
                    StrSql = StrSql & " id_clientep, id_bid, "
                    StrSql = StrSql & " id_AonH, num_contrato, plazo, "
                    StrSql = StrSql & " precio, tipo_pol, "
                    StrSql = StrSql & " taza, enganche, tipoplan, "
                    StrSql = StrSql & " bansegvida, bansegdesempleo,"
                    StrSql = StrSql & " vida, desempleo, id_programa, contrato, "
                    StrSql = StrSql & " estatus, anio, clave_Fscar)"
                    'strsql = strsql & " estatus, anio)"
                    StrSql = StrSql & " values(" & intcotiza & ", " & arrpaquete(0) & ", "
                    StrSql = StrSql & intcliente & ", " & intbid & ","
                    StrSql = StrSql & intvehiculo & "," & intnum & "," & intplazo & " ,"
                    StrSql = StrSql & dbprecio & ",'" & strtipo & "','"
                    StrSql = StrSql & strtaza & "'," & dbenganche & ",'" & strtipoplan & "','"
                    StrSql = StrSql & arrpaquete(3) & "','" & arrpaquete(4) & "',"
                    StrSql = StrSql & arrpaquete(1) & "," & arrpaquete(2) & ", " & intprograma & ",'"
                    StrSql = StrSql & strcontrato & "','" & strestatus & "'," & intanio & ",'"
                    StrSql = StrSql & strCveFscar & "')"
                    'strsql = strsql & strcontrato & "','" & strestatus & "'," & intanio & ")"
                    CConecta.EjecutaSQL(StrTransaccion, StrSql)
                End If
            Next
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_cotizaEmpleado(ByVal intcotiza As Integer) As DataTable

        Try
            StrSql = " select id_cotizacion from COTIZA_CLIENTE where id_cotizacion=" & intcotiza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_recotizar(ByVal intcliente As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = " SELECT * FROM VW_COTIZA_CLIENTE"
            StrSql = StrSql & " where id_clientep = " & intcliente & ""
            StrSql = StrSql & " and id_bid = " & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_recotizarXCliente(ByVal intcotiza As Integer, ByVal intbid As Integer) As DataTable

        Try
            'strsql = " SELECT * FROM VW_COTIZA_CLIENTE"
            'strsql = strsql & " where id_cotiza = " & intcotiza & ""
            StrSql = "SELECT *"
            StrSql = StrSql & " FROM VW_RECARGA_VARIABLES_RECOTIZA"
            StrSql = StrSql & " WHERE id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " and id_bid =" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_grupoPaquete(ByVal intcotiza As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select distinct id_paquete from COTIZA_CLIENTE"
            StrSql = StrSql & " where id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " and id_bid =" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_PaquetePNeta(ByVal intcotizapoliza As Integer) As DataTable

        Try
            StrSql = "select c.id_poliza, r.total_recibo , c.id_paquete , p.descripcion_paquete, c.subsidyf, c.subsidyfc "
            StrSql = StrSql & " from coti_poliza c, abc_paquete p , coti_poliza_recibo r"
            StrSql = StrSql & " where c.id_paquete = p.id_paquete"
            StrSql = StrSql & " and c.id_aseguradora = p.id_aseguradora "
            StrSql = StrSql & " and c.id_aseguradora = r.id_aseguradora"
            StrSql = StrSql & " and c.id_cotiza_poliza = r.id_cotiza_poliza"
            StrSql = StrSql & " and p.id_aseguradora = r.id_aseguradora"
            StrSql = StrSql & " and c.id_poliza = r.id_poliza"
            StrSql = StrSql & " and c.id_cotiza_poliza =" & intcotizapoliza & ""
            StrSql = StrSql & " order by p.orden_paquete"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_busqueda_cotizacion(ByVal intbid As Integer, ByVal strnombre As String) As DataTable

        Try
            StrSql = "select distinct nombre from VW_BUSQUEDA_COTIZACION"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            If Not strnombre = "" Then
                StrSql = StrSql & " and nombre like '%" & strnombre & "%'"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_datos_clienteP(ByVal intcotiza As Integer) As DataTable

        Try
            StrSql = "select * from VW_BUSQUEDA_COTIZACION"
            StrSql = StrSql & " where id_cotizacion = " & intcotiza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_datos_cotizacion(ByVal intbid As Integer, ByVal strnombre As String, ByVal intcotizacion As Integer) As DataTable

        Try
            StrSql = "select  distinct c.id_cotizacion, p.descripcion, p.anio"
            StrSql = StrSql & " from coti_poliza p, COTIZA_CLIENTE c"
            StrSql = StrSql & " where p.id_cotiza_poliza = c.id_cotizacion "
            StrSql = StrSql & " and c.id_bid = " & intbid & ""
            If strnombre = "" Then
                StrSql = StrSql & " and c.id_cotizacion = " & intcotizacion & ""
            Else
                StrSql = StrSql & " and c.id_cotizacion in ("
                StrSql = StrSql & " select id_cotizacion from VW_BUSQUEDA_COTIZACION"
                StrSql = StrSql & " where nombre = '" & strnombre & "')"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function paquete_aseguradora(ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select distinct paquete, opcsa_paquete"
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where SUBRAMO = " & intsubramo & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and id_paquete=" & intpaquete & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function carga_coberturas(ByVal intaseguradora As Integer, ByVal strestatus As String, _
    'ByVal strinsco As String, ByVal intversion As Integer, ByVal intpaquete As Integer, _
    'ByVal strnumcontrato As String, ByVal intbid As Integer, ByVal intcotiza As Integer, _
    'ByVal intcortemp As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try

    '        strsql = "select descripcion_temp, id_limresp, orden, descripcion_suma, deducible, plaza_vehiculo  , "
    '        strsql = strsql & " descripcion_tipo = case when not id_aseguradora = 1 then case "
    '        strsql = strsql & " when (descripcion_temp = 'Gastos M�dicos' or descripcion_temp = 'Gastos Medicos') "
    '        If intcotiza > 0 Then
    '            strsql = strsql & " then  descripcion_suma +' por Ocup'"
    '        Else
    '            strsql = strsql & " then  convert(varchar(20),(isnull(convert(numeric(10,0),descripcion_suma),0) * isnull(plaza_vehiculo,0))) "
    '        End If
    '        strsql = strsql & " else  descripcion_suma  End end "
    '        If intcotiza > 0 Then
    '            strsql = strsql & " from VW_CARGA_COBERTURAS "
    '        Else
    '            strsql = strsql & " from VW_CARGA_COBERTURAS_final "
    '        End If
    '        strsql = strsql & " where id_aseguradora = " & intaseguradora & ""
    '        strsql = strsql & " AND estatus_vehiculo = '" & strestatus & "'	"
    '        If intcotiza = 0 Then
    '            strsql = strsql & " and insco_vehiculo = '" & strinsco & "'"
    '        End If
    '        strsql = strsql & " and id_version = " & intversion & ""
    '        strsql = strsql & " and id_paquete = " & intpaquete & ""
    '        strsql = strsql & " and id_cobertura_temp = " & intcortemp & ""
    '        strsql = strsql & " and num_contrato =" & strnumcontrato & " "
    '        strsql = strsql & " and id_bid = " & intbid & ""
    '        If intcotiza > 0 Then
    '            strsql = strsql & " and id_cotizacion = " & intcotiza & ""
    '        End If
    '        strsql = strsql & " order by orden"
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function carga_coberturas(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String) As DataTable

        Try

            StrSql = "select distinct id_aseguradora, descripcion_suma, deducible ,orden"
            StrSql = StrSql & " , descripcion_tipo = case when not id_aseguradora = 1 then case "
            'strsql = strsql & " when (cobertura = 'Gastos M�dicos' or cobertura = 'Gastos Medicos' or cobertura = 'Gastos Medicos (LUC)' or cobertura = 'Gastos M�dicos (LUC)') "
            StrSql = StrSql & " when (bandera_ocupante = '1') "
            'paneles contado
            If Not (intprograma = 7 Or intprograma = 8 Or intprograma = 12 Or intprograma = 13) Then
                If intcotiza > 0 Then
                    StrSql = StrSql & " then  '$' + VALORES(descripcion_suma) +' por Ocupante'"
                Else
                    StrSql = StrSql & " then  convert(varchar(20),(isnull(convert(numeric(10,0),descripcion_suma),0) * isnull(plaza_vehiculo,0))) "
                End If
            Else
                StrSql = StrSql & " then  VALORES(descripcion_suma)"
            End If
            StrSql = StrSql & " else  descripcion_suma  End "
            StrSql = StrSql & " else descripcion_suma end"
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                StrSql = StrSql & " from VW_CARGA_COBERTURAS_CLIENTE "
            Else
                If intcotiza > 0 Then
                    StrSql = StrSql & " from VW_CARGA_COBERTURAS "
                Else
                    StrSql = StrSql & " from VW_CARGA_COBERTURAS_final "
                End If
            End If
            StrSql = StrSql & " where estatus = '" & strestatus & "'	"
            StrSql = StrSql & " and id_version = " & intversion & ""
            'strsql = strsql & " and id_paquete = " & intpaquete & ""
            StrSql = StrSql & " and id_paquete in (" & intpaquete & ")"
            'strsql = strsql & " and id_limresp = " & intlimresp & ""
            If intcotiza > 0 Then
                StrSql = StrSql & " and id_cotizacion = " & intcotiza & ""
            End If
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " AND id_aseguradora = " & intaseguradora & ""
            '''''strsql = strsql & " AND cobertura ='" & strcoberturadescripcion & "'"
            'strsql = strsql & " AND replace(cobertura,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
            StrSql = StrSql & " AND (replace(cobertura,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
            StrSql = StrSql & " OR cobertura ='" & strcoberturadescripcion & "')"
            StrSql = StrSql & " AND descripcion_paquete='" & strdescripcion & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function encabezado_coberturas(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intcotiza As Integer, ByVal intbid As Integer, _
    ByVal intprograma As Integer, ByVal strfincon As String, ByVal CadenaPaquete As String) As DataTable

        Try
            'strsql = "select distinct id_limresp, cobertura , orden"
            StrSql = "select distinct cobertura , orden"
            If (intprograma = 4 Or intprograma = 5 Or intprograma = 9) And strfincon = "C" Then
                StrSql = StrSql & " from VW_CARGA_COBERTURAS_CLIENTE"
            Else
                StrSql = StrSql & " from VW_CARGA_COBERTURAS"
            End If
            StrSql = StrSql & " where id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " AND id_bid = " & intbid & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus= '" & strestatus & "'"
            If intpaquete > 0 Then
                StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            Else
                If Not CadenaPaquete = "" Then
                    StrSql = StrSql & " and id_paquete in (" & CadenaPaquete & ")"
                End If
            End If
            StrSql = StrSql & " order by orden, cobertura"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function encabezado_coberturas_final(ByVal strestatus As String, _
   ByVal intversion As Integer, ByVal intbid As Integer, _
   ByVal intcontrato As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select distinct id_limresp, cobertura , orden"
            StrSql = StrSql & " from VW_CARGA_COBERTURAS_FINAL"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus = '" & strestatus & "'"
            StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            StrSql = StrSql & " and num_contrato = " & intcontrato & ""
            If intpaquete > 0 Then
                StrSql = StrSql & " and id_paquete =" & intpaquete & ""
            End If
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function consulta_region_aseguradora(ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select distinct id_region "
            StrSql = StrSql & " from BID_REGION"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and subramo = " & intsubramo & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function tarifac(ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal intpaquete As Integer, ByVal stranio As String, ByVal strmodelo As String, _
    ByVal intregion As Integer, ByVal intplan As Integer, _
    ByVal strtipopol As String, ByVal intaseguradora As Integer, _
    ByVal stropcsa As String) As DataTable

        Try
            'Especificaci�n del campo PLAN en TARIFACT y VEHICULOS
            StrSql = " SELECT distinct t.opcsa_tarifac"
            StrSql = StrSql & " FROM TARIFACT t,  abc_paquete p"
            StrSql = StrSql & " WHERE t.id_paquete = p.id_paquete"
            StrSql = StrSql & " and t.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and t.Moneda_tarifac = " & intmoneda & "  "
            StrSql = StrSql & " AND p.id_PAQUETE = " & intpaquete & ""
            StrSql = StrSql & " AND t.anio_tarifac = '" & stranio & "'"
            StrSql = StrSql & " AND t.MODELO_tarifac = '" & strmodelo & "'  "
            StrSql = StrSql & " AND t.REGION_tarifac = 1"
            If Not stropcsa = "" Then
                StrSql = StrSql & " AND t.OPCSA_tarifac = '" & stropcsa & "'  "
            End If
            If intaseguradora = 12 Then
                StrSql = StrSql & " AND (t.PLAN_tarifac = 2 or t.PLAN_tarifac = 0)"
            Else
                StrSql = StrSql & " AND t.PLAN_tarifac = 2"
            End If
            StrSql = StrSql & " AND t.TIPOPOL_tarifac='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            StrSql = StrSql & " AND t.id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function tariprima(ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal intpaquete As Integer, ByVal stranio As String, ByVal strinsco As String, _
    ByVal intregion As Integer, ByVal intplan As Integer, _
    ByVal strtipopol As String, ByVal intaseguradora As Integer) As DataTable

        Try
            'Especificaci�n del campo PLAN en TARIFACT y VEHICULOS
            StrSql = " SELECT distinct t.opcsa_tariprima"
            StrSql = StrSql & " FROM TARIPRIMA t,  abc_paquete p"
            StrSql = StrSql & " WHERE t.id_paquete = p.id_paquete"
            StrSql = StrSql & " and t.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and t.Moneda_tariprima = " & intmoneda & "  "
            StrSql = StrSql & " AND p.id_PAQUETE = " & intpaquete & ""
            StrSql = StrSql & " AND t.anio_tariprima='" & stranio & "'"
            StrSql = StrSql & " AND t.insco_tariprima = '" & strinsco & "'"
            StrSql = StrSql & " AND t.REGION_tariprima = 1"
            'strsql = strsql & " AND t.OPCSA_tariprima = '" & stropcsa & "'  "
            If intaseguradora <> 12 Then
                StrSql = StrSql & " AND t.PLAN_tariprima = 1"
            Else
                StrSql = StrSql & " AND t.PLAN_tariprima = 2"
            End If
            StrSql = StrSql & " AND t.TIPOPOL_tariprima='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            StrSql = StrSql & " AND t.id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_insco_homologado_inserta(ByVal intvehiculo As Integer, _
   ByVal stranio As String, ByVal intaseguradora As Integer, ByVal strestatus As String, ByVal intsubramo As Integer) As DataTable

        Try
            StrSql = "select insco = insco_vehiculo"
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where Id_AonH = " & intvehiculo & ""
            StrSql = StrSql & " and anio_vehiculo=" & stranio & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and estatus_vehiculo='" & strestatus & "'"
            StrSql = StrSql & " and subramo_vehiculo =" & intsubramo & ""
            StrSql = StrSql & " and status_vehiculo='1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determinando_deducpl_paquete_panel(ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select deduc_pl"
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determinando_precontrol_panel(ByVal intaseguradora As Integer, ByVal strcatalogo As String, ByVal intanio As Integer) As DataTable

        Try
            StrSql = "select precontrol "
            StrSql = StrSql & " from precontrol"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and catalogo_precontrol = '" & strcatalogo & "'"
            StrSql = StrSql & " and anio_precontrol = " & intanio & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determinando_deduc_vehiculo_panel(ByVal intaseguradora As Integer, ByVal intanio As Integer, _
    ByVal intvehiculo As Integer, ByVal strestatus As String) As DataTable

        Try
            StrSql = "select deducpl_vehiculo "
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and id_aonh = " & intvehiculo & ""
            StrSql = StrSql & " and anio_vehiculo = " & intanio & ""
            StrSql = StrSql & " and estatus_vehiculo ='" & strestatus & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'mazda
    Public Function determinando_deducpl(ByVal intaseguradora As Integer, ByVal intpaquete As String, ByVal intsubramo As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select deduc_pl, ban_tipopoliza"
            StrSql = StrSql & " from abc_paquete p , ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " where p.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and a.ban_deduc='1'"
            StrSql = StrSql & " and p.id_aseguradora = " & intaseguradora & ""
            'strsql = strsql & " and p.id_paquete=" & intpaquete & ""
            StrSql = StrSql & " and p.id_paquete in (" & intpaquete & ")"
            StrSql = StrSql & " and p.subramo=" & intsubramo & ""
            StrSql = StrSql & " and a.id_programa =" & intprograma & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'mazda
    Public Function determina_banderas_X_programa(ByVal intprograma As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select ban_deduc, ban_precontrol, ban_rate, ban_constante, "
            StrSql = StrSql & " ban_tipopoliza, ban_multianual, ban_ace, ban_iva, "
            StrSql = StrSql & " ban_estatus, ban_estatus_parametro, recargo, "
            StrSql = StrSql & " ban_recargo, ban_factores, ban_factura, ban_numfactura"
            StrSql = StrSql & " from ABC_PROGRAMA_ASEGURADORA"
            StrSql = StrSql & " where estatus_programaaseguradora ='1'"
            StrSql = StrSql & " and id_programa = " & intprograma & ""
            StrSql = StrSql & " and id_aseguradora = " & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function precontrol(ByVal strcatalogo As String, ByVal stranio As String, ByVal strmoneda As String, ByVal strrate As String) As DataTable

        Try
            StrSql = " Select distinct precontrol"
            StrSql = StrSql & " FROM PRECONTROL  "
            StrSql = StrSql & " WHERE anio_PRECONTROL = '" & stranio & "'  "
            If Not strcatalogo = "" Then
                StrSql = StrSql & " AND CATALOGO_PRECONTROL = '" & strcatalogo & "'"
            End If
            StrSql = StrSql & " AND MONEDA = '" & strmoneda & "'"
            If Not strrate = "" Then
                StrSql = StrSql & " AND INTERESTRATE='" & strrate & "'"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function determinando_impresion_deducpl(ByVal intaseguradora As Integer, ByVal intpaquete As Integer, ByVal intsubramo As Integer, ByVal intprograma As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select deduc_pl, ban_tipopoliza"
            sql = sql & " from abc_paquete p , ABC_PROGRAMA_ASEGURADORA a"
            sql = sql & " where p.id_aseguradora = a.id_aseguradora"
            sql = sql & " and a.ban_deduc='1'"
            sql = sql & " and p.id_aseguradora = " & intaseguradora & ""
            sql = sql & " and p.id_paquete=" & intpaquete & ""
            sql = sql & " and p.subramo=" & intsubramo & ""
            sql = sql & " and a.id_programa =" & intprograma & ""
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function traer_cobertura_DV(ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal nompaquete As String) As DataTable

        Try
            StrSql = " select * from cobertura"
            StrSql = StrSql & " where descripcion_cob = '" & nompaquete & "'"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            'strsql = strsql & " and Id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function recarga_valore_cotizacion_Vida_desempleo(ByVal intprograma As Integer, ByVal intcotiza As Integer) As DataTable

        Try
            StrSql = "select vida = descripcion_paquete+'|'+bansegvida,"
            StrSql = StrSql & " desempleo = descripcion_paquete+'|'+bansegdesempleo"
            StrSql = StrSql & " from VW_BANDERAS_VIDA_DESEMPLEO"
            StrSql = StrSql & " where id_programa = " & intprograma & ""
            StrSql = StrSql & " and id_cotizacion = " & intcotiza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_vida_desempleo(ByVal intcotiza As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select bansegvida , bansegdesempleo"
            StrSql = StrSql & " from COTIZA_CLIENTE"
            StrSql = StrSql & " where id_cotizacion = " & intcotiza & ""
            StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_reporte_poliza_desempleo(ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "SELECT polizaace, FI, FF , prima_desempleo,"
            StrSql = StrSql & " NOMBRE = CASE per_fiscal WHEN 2 THEN isnull(razon_social, '') ELSE isnull(nombre, '') + ' ' + isnull(apaterno, '') + ' ' + isnull(amaterno, '') END ,"
            StrSql = StrSql & " Descripcion = isnull(marca,'')+ ' '+ isnull(modelo,'')+' '+ isnull(descripcion,'')+' '+ convert(varchar(4),isnull(anio,''))+' con No. Serie : '+ isnull(serie,''),"
            StrSql = StrSql & " descripcion_region, poliza"
            StrSql = StrSql & " FROM VW_REPORTE_POLIZA_DESEMPLEO"
            StrSql = StrSql & " where id_poliza = " & intpoliza & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_reporte_poliza_vida(ByVal intpoliza As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select polizavida, vigencia_inicial, vigencia_final, "
            StrSql = StrSql & " NOMBRE = CASE per_fiscal WHEN 2 THEN isnull(razon_social, '') ELSE isnull(nombre, '') + ' ' + isnull(apaterno, '') + ' ' + isnull(amaterno, '') END ,"
            StrSql = StrSql & " direccion = isnull(calle_cliente,'')+' col. '+isnull(colonia_cliente,''), estado_cliente, cp_cliente, "
            StrSql = StrSql & " rfc, telefono, fnacimiento, edad= datediff(yyyy, fnacimiento,getdate()), fi, plazo_financiamiento,"
            StrSql = StrSql & " cobertura, primaneta_vida, derpol_vida, prima_vida"
            StrSql = StrSql & " from VW_REPORTE_POLIZA_vida"
            StrSql = StrSql & " where id_poliza = " & intpoliza & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'busqueda del iva
    Public Function busqueda_iva(ByVal striva As String) As DataTable

        Try
            StrSql = "select distinct nombre_estado, nombre_provincia, iva = (pct_iva/100)"
            StrSql = StrSql & " from parametro_iva"
            StrSql = StrSql & " where codigo_postal ='" & striva & "'"
            StrSql = StrSql & " and estatus_iva= '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_cobertura_iva(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal strcadena As String)
        Dim arrLista() As String = strcadena.Split("|")
        Dim i As Integer = 0
        Try
            StrTransaccion = "actualiza_cobertura_iva"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To arrLista.Length - 1
                Dim arr() As String = arrLista(i).Split("*")
                StrSql = "UPDATE cobertura SET total_cob='" & arr(2) & "'"
                StrSql = StrSql & " WHERE Num_contrato = " & intcontrato & ""
                StrSql = StrSql & " AND id_bid = " & intbid & " "
                StrSql = StrSql & " AND subramo = " & arr(1) & ""
                StrSql = StrSql & " and ID_ASEGURADORA =" & arr(0) & ""
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
            Next
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function Carga_Beneficiarios(ByVal intprograma As Integer) As DataTable

        Try
            StrSql = " select id_beneficiario, nombre_beneficiario "
            StrSql = StrSql & " from ABC_BENEFICIARIO"
            StrSql = StrSql & " where Estatus_Beneficiario  ='1'"
            StrSql = StrSql & " and id_tipo in (select distinct ban_beneficiario "
            StrSql = StrSql & " from abc_programa_aseguradora"
            StrSql = StrSql & " where id_programa = " & intprograma & ")"
            StrSql = StrSql & " order by nombre_beneficiario"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determina_bid_consultas(ByVal intusuario As Integer, ByVal intprograma As Integer, ByVal intmarca As Integer) As DataTable

        Try
            StrSql = "select id_bid = 0, nombre = '-- Seleccione --', empresa ='' union"
            If intprograma = 9 Then
                '''strsql = strsql & " select id_bid, nombre = isnull(bid_ford,'0')+'- ' + isnull(empresa,''), empresa"
                'StrSql = StrSql & " select id_bid, nombre = empresa, empresa"
                StrSql = StrSql & " select id_bid, "
                StrSql = StrSql & " empresa =  LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))), "
                StrSql = StrSql & " nombre =   LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))) "
            Else
                'StrSql = StrSql & " select id_bid, nombre = empresa, empresa"
                StrSql = StrSql & " select id_bid, "
                StrSql = StrSql & " empresa =  LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))), "
                StrSql = StrSql & " nombre =   LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))) "
            End If
            StrSql = StrSql & " from bid"
            StrSql = StrSql & " where estatus = '1'"
            StrSql = StrSql & " and id_bid in (select  id_bid "
            StrSql = StrSql & " from usuario_bid"
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            StrSql = StrSql & " and id_programa = " & intprograma & ")"
            StrSql = StrSql & " and id_marca =" & intmarca & ""
            StrSql = StrSql & " order by empresa"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_periodo_fnacimiento(ByVal strfecha As String, ByVal intbandera As Integer) As DataTable
        Dim asfecha() As String
        Dim fecha1 As String
        Try
            asfecha = strfecha.Split("/")
            If intbandera = 1 Then
                fecha1 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
            Else
                fecha1 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
            End If

            StrSql = "select anio = datediff(year, '" & fecha1 & "',getdate())"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_noserie(ByVal strserie As String) As DataTable

        Try
            'Hmh 02/10/2014
            'Por indicaciones de Erick Avelar (owner del negocio)
            'Se agrego el filtro para que tambien la valide en las p�lizas que esten vigentes y no canceladas.
            StrSql = "set dateformat dmy"
            StrSql = StrSql & " select bandera = isnull(id_cancelacion,0)"
            StrSql = StrSql & " from poliza"
            StrSql = StrSql & " where serie = '" & strserie & "'"
            'StrSql = StrSql & " and getdate() between Fvigencia and FFin"
            'HMH 20141206
            StrSql = StrSql & " and (getdate() between Fvigencia and FFin or Fvigencia > getdate())"
            StrSql = StrSql & " and id_cancelacion is null"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'HM 20/02/2014 se agrega validacion por bid
    Public Function validaBidGrupoIntSerie(ByVal strContrato As String, ByVal strSerie As String) As DataTable
        Try
            StrSql = "select count(isnull(p.id_Poliza,0)) as contador"
            StrSql = StrSql & " from poliza p , cliente_poliza cp, contrato c"
            StrSql = StrSql & " where p.id_poliza = cp.id_poliza"
            StrSql = StrSql & " and p.id_bid = cp.id_bid"
            StrSql = StrSql & " and cp.id_cliente = c.id_cliente"
            StrSql = StrSql & " and cp.id_bid = c.id_bid"
            StrSql = StrSql & " and c.num_contrato = '" & strContrato & "'"
            StrSql = StrSql & " and p.serie = '" & strSerie & "'"
            StrSql = StrSql & " group by c.num_contrato, p.serie"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_minutos_cancelacion(ByVal strpoliza As String) As DataTable

        Try
            StrSql = "select minutos = datediff(minute, fecha_registro, getdate()),"
            StrSql = StrSql & " fecha_registro, getdate() , poliza"
            StrSql = StrSql & " from poliza"
            StrSql = StrSql & " where poliza = '" & strpoliza & "'"
            StrSql = StrSql & " and not fvigencia is null "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_dias_cancelacion(ByVal intMinutos As Long) As DataTable

        Try
            StrSql = "SELECT " & intMinutos & " / (24 * 60) AS dias, (" & intMinutos & " % (24 * 60) / 60) AS horas, (" & intMinutos & " % (24 * 60) % 60) AS minutos"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DiasCancelacion_Programa(ByVal intPrograma As Integer) As DataTable
        Try
            StrSql = "select DiasCancelacion_Programa "
            StrSql = StrSql & " from abc_programa"
            StrSql = StrSql & " where id_programa = " & intPrograma & ""
            StrSql = StrSql & " and estatus_programa = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'EVI 27/06/2010
    Public Function CreaCotizacion(ByVal intversion As Integer, ByVal intbid As Integer, ByVal intprograma As Integer, _
     ByVal intmarca As Integer, ByVal intaonh As Integer, ByVal strnuevo As String, _
     ByVal strmodelo As String, ByVal intanio As Integer, _
     ByVal strfechaContrato As String, ByVal dbprecio As Double, ByVal strtipopersona As String, ByVal IntCampana As Integer, _
     ByVal intplazo As Integer, ByVal intplazovida As Integer, ByVal strfolio As String, ByVal strtipopoliza As String, _
     ByVal strseguroD As String, ByVal strseguroV As String, ByVal strsubramo As String, ByVal numContCobertura As Long, _
     ByVal intcliente As Integer, ByVal intpaquete As Integer, ByVal strfechaVida As String, _
     ByVal intuso As Integer, ByVal dbenganche As Double, ByVal strcadenavida As String, _
     ByVal strcadenadesemp As String, ByVal strcadenacob As String, ByVal strPaqSelCon As String, _
     ByVal strcalculos As String, ByVal strsubsidios As String, ByVal strArrPoliza As String, _
     ByVal strResultado As String, ByVal strMultArrPolisa As String, ByVal dbiva As Double, _
    ByVal strcontrato As String) As Long
        Dim ds As New DataSet
        Dim IntSalida As Long
        Try

            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = "Select Valor = isnull(max(id_cotizacion), 0 ) + 1 from cotizacion"
            'ds = CConecta.AbrirConsulta(StrSql)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    IntSalida = ds.Tables(0).Rows(0)("valor")
            'End If

            StrTransaccion = "IntCotizacion"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsCot table(id int); "
            StrSql = StrSql & " insert into cotizacion (id_Cotizacion, id_version, id_bid, id_programa, " & vbCrLf
            StrSql = StrSql & " id_marca, id_aonh, nuevo, modelo, anio, " & vbCrLf
            StrSql = StrSql & " fecha_contrato, fecha_vida, precio_unidad, tipo_persona, banCampana, " & vbCrLf
            StrSql = StrSql & " plazo, plazovida, foliosolicitud, tipopoliza, segurodanos, segurovida, " & vbCrLf
            StrSql = StrSql & " subramo, num_contratocobertura, id_cliente, id_Paquete, id_uso, enganche, " & vbCrLf
            StrSql = StrSql & " Cadena_Vida, Cadena_Desempleo, Cadena_Cob, PaqSelCon, " & vbCrLf
            StrSql = StrSql & " Calculos, subsidios, ArrPoliza, Resultado, MultArrPoliza, IVa, Contrato)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_cotizacion into @idInsCot select (Select isnull(max(id_cotizacion), 0 ) + 1 from cotizacion With(NoLock))," & intversion & ","
            StrSql = StrSql & intbid & "," & intprograma & "," & intmarca & "," & intaonh & ",'"
            StrSql = StrSql & strnuevo & "','" & strmodelo & "'," & intanio & ", '"
            StrSql = StrSql & strfechaContrato & "','" & strfechaVida & "'," & dbprecio & ",'" & strtipopersona & "','"
            StrSql = StrSql & IntCampana & "'," & intplazo & "," & intplazovida & ",'" & strfolio & "','" & strtipopoliza & "','"
            StrSql = StrSql & strseguroD & "','" & strseguroV & "','" & strsubramo & "'," & numContCobertura & ","
            StrSql = StrSql & intcliente & ", " & intpaquete & "," & intuso & ", " & dbenganche & ",'"
            StrSql = StrSql & strcadenavida & "','" & strcadenadesemp & "','" & strcadenacob & "','" & strPaqSelCon & "','"
            StrSql = StrSql & strcalculos & "','" & strsubsidios & "','" & strArrPoliza & "','" & strResultado & "','" & strMultArrPolisa & "','" & dbiva & "','" & strcontrato & "'; "
            StrSql += "select * from @idInsCot; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                IntSalida = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return IntSalida

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'EVI 27/06/2010
    Public Sub ModificaCotizacion( _
    ByVal intmarca As Integer, ByVal intaonh As Integer, ByVal strnuevo As String, _
    ByVal strmodelo As String, ByVal intanio As Integer, _
    ByVal strfechaContrato As String, ByVal dbprecio As Double, ByVal strtipopersona As String, ByVal IntCampana As Integer, _
    ByVal intplazo As Integer, ByVal intplazovida As Integer, ByVal strfolio As String, ByVal strtipopoliza As String, _
    ByVal strseguroD As String, ByVal strseguroV As String, ByVal strsubramo As String, ByVal numContCobertura As String, _
    ByVal intcliente As Integer, ByVal intpaquete As Integer, ByVal strfechaVida As String, ByVal intuso As Integer, _
    ByVal dbenganche As Double, ByVal strcadenavida As String, ByVal strcadenadesemp As String, _
    ByVal strcadenacob As String, ByVal strPaqSelCon As String, _
    ByVal strcalculos As String, ByVal strsubsidios As String, ByVal strArrPoliza As String, _
    ByVal strResultado As String, ByVal strMultArrPolisa As String, ByVal intcotizacion As Long)
        Try
            StrTransaccion = "IntCotizacion"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            StrSql = "set dateformat dmy"
            StrSql = StrSql & " update cotizacion  set id_marca =" & intmarca & ", "
            StrSql = StrSql & " id_aonh=" & intaonh & ", "
            StrSql = StrSql & " nuevo='" & strnuevo & "', "
            StrSql = StrSql & " modelo='" & strmodelo & "', "
            StrSql = StrSql & " anio=" & intanio & ", "
            StrSql = StrSql & " fecha_contrato='" & strfechaContrato & "', "
            StrSql = StrSql & " fecha_vida='" & strfechaVida & "', "
            StrSql = StrSql & " precio_unidad=" & dbprecio & ", "
            StrSql = StrSql & " tipo_persona='" & strtipopersona & "', "
            StrSql = StrSql & " banCampana='" & IntCampana & "', "
            StrSql = StrSql & " plazo=" & intplazo & ", "
            StrSql = StrSql & " plazovida=" & intplazovida & ", "
            StrSql = StrSql & " foliosolicitud='" & strfolio & "', "
            StrSql = StrSql & " tipopoliza='" & strtipopoliza & "', "
            StrSql = StrSql & " segurodanos='" & strseguroD & "', "
            StrSql = StrSql & " segurovida='" & strseguroV & "', "
            StrSql = StrSql & " subramo='" & strsubramo & "', "
            StrSql = StrSql & " num_contratocobertura=" & numContCobertura & ", "
            StrSql = StrSql & " id_cliente =" & intcliente & ", "
            StrSql = StrSql & " id_paquete =" & intpaquete & ", "
            StrSql = StrSql & " id_uso =" & intuso & ", "
            StrSql = StrSql & " enganche =" & dbenganche & ", "
            StrSql = StrSql & " Cadena_Vida ='" & strcadenavida & "', "
            StrSql = StrSql & " Cadena_Desempleo ='" & strcadenadesemp & "', "
            StrSql = StrSql & " Cadena_Cob='" & strcadenacob & "', "
            StrSql = StrSql & " PaqSelCon='" & strPaqSelCon & "', "
            StrSql = StrSql & " Calculos='" & strcalculos & "', "
            StrSql = StrSql & " subsidios ='" & strsubsidios & "', "
            StrSql = StrSql & " ArrPoliza='" & strArrPoliza & "', "
            StrSql = StrSql & " Resultado='" & strResultado & "', "
            StrSql = StrSql & " MultArrPoliza='" & strMultArrPolisa & "' "
            StrSql = StrSql & " where id_cotizacion = " & intcotizacion & ""
            StrSql = StrSql & "set dateformat mdy"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'EVI 27/06/2010
    Public Function BuscaCotizacion(ByVal intbid As Integer, ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable

        Try
            StrCadena = Replace(StrCadena, " ", "%")

            StrSql = "SELECT * "
            StrSql = StrSql & " FROM VW_COTIZACION "
            StrSql = StrSql & " WHERE Id_Programa = " & intprograma & ""
            StrSql = StrSql & " AND Id_Bid = " & intbid & ""

            If StrCriterios <> "" Then
                StrSql = StrSql & " AND ("

                If InStr(StrCriterios, "1") Then
                    StrSql = StrSql & " descripcion_homologado LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "2") Then
                    StrSql = StrSql & " nom LIKE '%" & StrCadena & "%' OR "
                End If

                If InStr(StrCriterios, "3") Then
                    StrSql = StrSql & " id_cotizacion = " & StrCadena & " OR "
                End If

                StrSql = Trim(Left(StrSql, Len(StrSql) - 3))
                StrSql = StrSql & ")"
            End If

            StrSqlSalida = StrSql
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_tipoPago(ByVal iPrograma As Integer) As DataTable
        Try
            StrSql = "select id_tiporecargo = 0, descripcion_periodo ='-- Seleccione -- ', Orden =0"
            StrSql = StrSql & " Union"
            StrSql = StrSql & " select id_tiporecargo, descripcion_periodo, Orden"
            StrSql = StrSql & " from ABC_TIPORECARGO"
            StrSql = StrSql & " where Descripcion_Periodo LIKE '%' + CASE WHEN exists(select 1 from abc_programa where id_programa = " & iPrograma.ToString() & _
                " and descripcion_programa like '%CONTADO%') THEN 'CONTADO' ELSE '' END + '%' "
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_FactorTipoPago(ByVal idtipopago As Integer) As DataTable
        Try
            StrSql = "select cve =0, num_pago = '-- Seleccione --' "
            StrSql = StrSql & " Union"
            StrSql = StrSql & " select cve = num_pago, num_pago= cast(num_pago as varchar)"
            StrSql = StrSql & " from ABC_FACTORRECARGO"
            StrSql = StrSql & " where id_tiporecargo = " & idtipopago & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'EVI 27/06/2010
    Public Function recarga_cotizacion(ByVal idcotizacion As Long) As DataTable
        Try
            StrSql = "select catalogo_vehiculo = DETERMINA_CATALOGO_COTIZACION(ID_AONH) , * "
            StrSql = StrSql & " from VW_COTIZACION"
            StrSql = StrSql & " where id_cotizacion = " & idcotizacion & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function Registra_contenedor_reportes(ByVal intbid As Integer, ByVal strpoliza As String, ByVal strtipoD As String, ByVal strtipoV As String) As Long
        Dim ds As New DataSet
        Dim valor As Long
        Dim i As Integer = 0
        Dim Max As Integer = 0

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select clave = isnull(max(cve_reporte),0)+1 from contenedor_reporte"
            'ds = CConecta.AbrirConsulta(StrSql)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    valor = ds.Tables(0).Rows(0)("clave")
            'End If

            StrTransaccion = "IntContReportes"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            For i = 0 To Max
                If i = 0 Then
                    StrSql = "set dateformat dmy; " & vbCrLf
                    StrSql += " declare @idInsRep table(id int); "
                    StrSql = StrSql & " insert into contenedor_reporte (cve_reporte, estatus, id_bid, poliza, tipo)"
                    StrSql = StrSql & " OUTPUT inserted.cve_reporte into @idInsRep select (Select isnull(max(cve_reporte), 0 ) + 1 from contenedor_reporte With(NoLock)),'0'," & intbid & ",'" & strpoliza & "','" & IIf(i = 0, strtipoD, strtipoV) & "'; "
                    StrSql += "select * from @idInsRep; "
                Else
                    StrSql = "set dateformat dmy; " & vbCrLf
                    StrSql = StrSql & " insert into contenedor_reporte (cve_reporte, estatus, id_bid, poliza, tipo)"
                    StrSql = StrSql & " values(" & valor & ",'0'," & intbid & ",'" & strpoliza & "','" & IIf(i = 0, strtipoD, strtipoV) & "')"
                End If

                If i = 0 Then
                    ds = CConecta.AbrirConsulta(StrSql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        valor = ds.Tables(0).Rows(0)(0)
                    End If
                End If
            Next

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_Contenedor_reporte(ByVal strpoliza As String, ByVal intbid As Integer) As DataTable
        Try
            StrSql = "select cve_reporte"
            StrSql = StrSql & " from contenedor_reporte"
            StrSql = StrSql & " where poliza = '" & strpoliza & "' "
            StrSql = StrSql & " and id_bid = " & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#Region "Determina las freacciones"
    Private Sub Genera_FraccionesAseguradora(ByVal IdPoliza As Long, ByVal idbid As Integer, ByVal idaseguradora As Integer, ByVal dPorRecargo As Decimal)
        Dim CdFraccion As New CP_FRACCIONES.clsEmision
        Dim sql As String = ""
        Dim SqlRecibo As String = ""
        Dim dt As DataTable
        Dim ds As DataSet
        Dim i As Integer = 0
        Dim vigenciaI As String = ""
        Dim VigenciaF As String = ""
        Dim formaPago As Integer = 0

        Dim ArrSalida1 As Object

        Try
            If (System.Configuration.ConfigurationManager.AppSettings("BanderaFechaServidor").ToString() = "1") Then
                sql = "select	id_aseguradora,convert(varchar(10), cast(vigencia_finip as date), 103) vigencia_finip,convert(varchar(10), cast(vigencia_ffinp as date), 103) vigencia_ffinp, primaneta, "
            Else
                sql = "select id_aseguradora, vigencia_finip, vigencia_ffinp, primaneta, "
            End If

            sql = sql & " RC_USA, "
            sql = sql & " pago_fraccionado, derpol, iva, "
            sql = sql & " primatotal, ivapor = valoriva , id_tiporecargo, "
            sql = sql & " porpago_fraccionado = (isnull(porpago_fraccionado,0)*100)"
            sql = sql & " from poliza"
            sql = sql & " where id_poliza = " & IdPoliza & ""
            dt = CConecta.AbrirConsulta(sql, True)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("id_tiporecargo") > 1 Then
                    Select Case dt.Rows(0)("id_tiporecargo")
                        Case 2 'Anual
                            formaPago = 1
                        Case 3 'Semestral
                            formaPago = 4
                        Case 4 'Trimestral
                            formaPago = 3
                        Case 5 'Mensual
                            formaPago = 2
                    End Select

                    Select Case idaseguradora
                        Case 1, 4
                            ds = CdFraccion.FraccionesQualitas_Conauto(dt.Rows(0)("vigencia_finip"), _
                                dt.Rows(0)("vigencia_ffinp"), dt.Rows(0)("primaneta"), _
                                dt.Rows(0)("pago_fraccionado"), dt.Rows(0)("derpol"), _
                                dt.Rows(0)("iva"), dt.Rows(0)("primatotal"), _
                                formaPago, dt.Rows(0)("ivapor"), _
                                dt.Rows(0)("porpago_fraccionado"))
                        Case 2, 5
                            ds = CdFraccion.FraccionesCHUBB_MZD(dt.Rows(0)("vigencia_finip"), _
                                dt.Rows(0)("vigencia_ffinp"), dt.Rows(0)("primaneta"), _
                                dt.Rows(0)("pago_fraccionado"), dt.Rows(0)("derpol"), _
                                dt.Rows(0)("iva"), dt.Rows(0)("primatotal"), _
                                formaPago, dt.Rows(0)("ivapor"), _
                                dt.Rows(0)("porpago_fraccionado"))
                        Case 3, 6
                            ds = CdFraccion.FraccionesHDI(dt.Rows(0)("vigencia_finip"), _
                                dt.Rows(0)("vigencia_ffinp"), dt.Rows(0)("primaneta"), _
                                dt.Rows(0)("pago_fraccionado"), dt.Rows(0)("derpol"), _
                                dt.Rows(0)("iva"), dt.Rows(0)("primatotal"), _
                                formaPago, dt.Rows(0)("ivapor"), _
                                dPorRecargo * 100)
                    End Select

                    If ds.Tables(0).Rows.Count > 0 Then
                        StrTransaccion = "InsRecibos"
                        CConecta.Transaccion(StrTransaccion, True, False, True)
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            Dim Fecha() As String = Split(ds.Tables(0).Rows(i)("Inicio_Vigencia"), "/")
                            vigenciaI = Fecha(0) & "/" & Fecha(1) & "/" & Fecha(2)
                            Dim Fecha1() As String = Split(ds.Tables(0).Rows(i)("Fin_Vigencia"), "/")
                            VigenciaF = Fecha1(0) & "/" & Fecha1(1) & "/" & Fecha1(2)

                            SqlRecibo = "set dateformat dmy"
                            SqlRecibo = SqlRecibo & " insert into poliza_recibo (id_poliza, id_bid, "
                            SqlRecibo = SqlRecibo & " id_aseguradora, num_fraccion, "
                            SqlRecibo = SqlRecibo & " iniciovigencia_fraccion, "
                            SqlRecibo = SqlRecibo & " Finvigencia_fraccion, "
                            SqlRecibo = SqlRecibo & " pneta_recibo, recargo_recibo, "
                            SqlRecibo = SqlRecibo & " derecho_recibo, iva_recibo, "
                            SqlRecibo = SqlRecibo & " ptotal_recibo, plazo_recibo, "
                            SqlRecibo = SqlRecibo & " total_recibo) values (" & IdPoliza & ", "
                            SqlRecibo = SqlRecibo & idbid & ", " & idaseguradora & ","
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Recibo") & ",'"
                            SqlRecibo = SqlRecibo & vigenciaI & "', '"
                            SqlRecibo = SqlRecibo & VigenciaF & "', "
                            'SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Inicio_Vigencia") & "', '"
                            'SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Fin_Vigencia") & "', "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Prima_Neta") & ", "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Recargo") & ", "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Derecho_Poliza") & ","
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("iva") & ", "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("prima_total") & ", "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Periodo_vigencia") & ", "
                            SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Total_Recibos") & ")"
                            CConecta.EjecutaSQL(StrTransaccion, SqlRecibo)
                            vigenciaI = ""
                            VigenciaF = ""
                        Next
                        CConecta.Transaccion(StrTransaccion, False, True, False, True)
                    End If
                Else
                    Dim Fecha() As String = Split(dt.Rows(0)("vigencia_finip"), "/")
                    vigenciaI = Fecha(0) & "/" & Fecha(1) & "/" & Fecha(2)
                    Dim Fecha1() As String = Split(dt.Rows(0)("vigencia_ffinp"), "/")
                    VigenciaF = Fecha1(0) & "/" & Fecha1(1) & "/" & Fecha1(2)

                    SqlRecibo = "set dateformat dmy"
                    SqlRecibo = SqlRecibo & " insert into poliza_recibo (id_poliza, id_bid, "
                    SqlRecibo = SqlRecibo & " id_aseguradora, num_fraccion, "
                    SqlRecibo = SqlRecibo & " iniciovigencia_fraccion, "
                    SqlRecibo = SqlRecibo & " Finvigencia_fraccion, "
                    SqlRecibo = SqlRecibo & " pneta_recibo, recargo_recibo, "
                    SqlRecibo = SqlRecibo & " derecho_recibo, iva_recibo, "
                    SqlRecibo = SqlRecibo & " ptotal_recibo, plazo_recibo, "
                    SqlRecibo = SqlRecibo & " total_recibo) values (" & IdPoliza & ", "
                    SqlRecibo = SqlRecibo & idbid & ", " & idaseguradora & ", 1,'"
                    SqlRecibo = SqlRecibo & vigenciaI & "', '"
                    SqlRecibo = SqlRecibo & VigenciaF & "', "
                    'SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Inicio_Vigencia") & "', '"
                    'SqlRecibo = SqlRecibo & ds.Tables(0).Rows(i)("Fin_Vigencia") & "', "
                    SqlRecibo = SqlRecibo & dt.Rows(0)("primaneta") & ", "
                    SqlRecibo = SqlRecibo & dt.Rows(0)("pago_fraccionado") & ", "
                    SqlRecibo = SqlRecibo & dt.Rows(0)("derpol") & ","
                    SqlRecibo = SqlRecibo & dt.Rows(0)("iva") & ", "
                    SqlRecibo = SqlRecibo & dt.Rows(0)("primatotal") & ", "
                    SqlRecibo = SqlRecibo & DateDiff(DateInterval.Month, CDate(vigenciaI), CDate(VigenciaF)) & ", "
                    SqlRecibo = SqlRecibo & "1)"

                    CConecta.EjecutaSQL(StrTransaccion, SqlRecibo)
                    vigenciaI = ""
                    VigenciaF = ""
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Determinando calculos de Factores"
    Private Function Factores(ByVal intaseguradora As Integer, ByVal numfactor As Integer) As DataTable
        Dim sql As String = ""
        Try
            sql = "select valor = isnull(factor,0) "
            sql = sql & " from abc_factor"
            sql = sql & " where id_aseguradora = " & intaseguradora & ""
            sql = sql & " and estatus_factor ='1'"
            sql = sql & " and id_factor = " & numfactor & ""
            Return CConecta.AbrirConsulta(sql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "Coberturas de inicio"
    Public Function Carga_aseguradora_cobertura_inicio(ByVal intbid As Integer, _
    ByVal intcontrato As Integer, ByVal intversion As Integer, ByVal strestatus As String) As DataTable

        Try
            StrSql = " SELECT distinct Id_aseguradora, descripcion_aseguradora"
            StrSql = StrSql & " FROM VW_CARGA_COBERTURA_INICIO"
            StrSql = StrSql & " WHERE  id_bid = " & intbid & ""
            StrSql = StrSql & " and num_contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus='" & strestatus & "'"
            StrSql = StrSql & " order by descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cuenta_paquete_aseguradora_subramo_inicio(ByVal intbid As Integer, ByVal intcontrato As Integer) As DataTable

        Try
            'emmanuell 31/10/08
            StrSql = "Select count(id_cobertura)"
            StrSql = StrSql & " from COBERTURA"
            StrSql = StrSql & " where num_contrato = " & intcontrato & ""
            StrSql = StrSql & " And id_bid = " & intbid & ""
            StrSql = StrSql & " and orden_cob ='0'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '''''Public Function paquete_aseguradora_subramo_inicio(ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer, ByVal intaon As Integer, _
    '''''ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal banunico As Integer, ByVal CadenaPaquete As String) As DataTable
    '''''    Dim da As SqlDataAdapter
    '''''    Dim ds As New DataSet
    '''''    Dim strsql As String
    '''''    Try
    '''''        'emmanuell 31/10/08 evi
    '''''        'emmanuell 10/03/10 evi
    '''''        'strsql = "select distinct p.id_paquete, p.descripcion_paquete, p.orden_paquete"
    '''''        strsql = "select distinct a.descripcion_aseguradora, p.id_paquete, p.descripcion_paquete, p.orden_paquete"
    '''''        'strsql = strsql & " from abc_paquete p 
    '''''        strsql = strsql & " from abc_paquete p,  abc_aseguradora a "
    '''''        If banunico = 1 Then
    '''''            strsql = strsql & " , COBERTURA c"
    '''''            strsql = strsql & " where p.id_aseguradora = a.id_aseguradora and p.descripcion_paquete = c.descripcion_cob"
    '''''        Else
    '''''            strsql = strsql & " where p.id_aseguradora = a.id_aseguradora and p.estatus ='" & strestatus & "'"
    '''''        End If
    '''''        strsql = strsql & " and p.id_aseguradora in (select distinct id_aseguradora "
    '''''        strsql = strsql & " from ABC_PROGRAMA_ASEGURADORA"
    '''''        strsql = strsql & " where estatus_programaaseguradora ='1'"
    '''''        strsql = strsql & " and id_programa = " & intprograma & ")"
    '''''        If intpaquete > 0 And CadenaPaquete <> "" Then
    '''''            strsql = strsql & " and p.id_paquete=" & intpaquete & ""
    '''''        Else
    '''''            If Not CadenaPaquete = "" Then
    '''''                strsql = strsql & " and p.id_paquete in (" & CadenaPaquete & ")"
    '''''            End If
    '''''        End If
    '''''        'inicia cambio micha
    '''''        If intsubramo = 0 Then
    '''''            strsql = strsql & " and p.subramo in(select distinct v.subramo_vehiculo"
    '''''            strsql = strsql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
    '''''            strsql = strsql & " WHERE v.id_aseguradora = a.id_aseguradora"
    '''''            strsql = strsql & " AND a.id_programa =" & intprograma & ""
    '''''            strsql = strsql & " AND v.Id_AonH = " & intaon & " "
    '''''            strsql = strsql & " and v.estatus_vehiculo ='" & strestatus & "' "
    '''''            strsql = strsql & " AND a.estatus_programaaseguradora = '1' "
    '''''            strsql = strsql & " AND v.status_vehiculo ='1')"
    '''''        Else
    '''''            strsql = strsql & " and p.subramo = " & intsubramo & ""
    '''''        End If
    '''''        'fin micha
    '''''        If banunico = 1 Then
    '''''            strsql = strsql & " and c.num_contrato =" & intcontrato & ""
    '''''            strsql = strsql & " and c.id_bid = " & intbid & ""
    '''''            strsql = strsql & " and c.orden_cob ='0'"
    '''''        End If
    '''''        'strsql = strsql & " order by p.orden_paquete, p.descripcion_paquete, p.id_paquete"
    '''''        strsql = strsql & " order by a.descripcion_aseguradora , p.orden_paquete, p.descripcion_paquete, p.id_paquete"
    '''''        da = New SqlDataAdapter(strsql, Cnn)
    '''''        da.SelectCommand.CommandType = CommandType.Text
    '''''        da.Fill(ds)
    '''''        Return ds.Tables(0)
    '''''    Catch ex As Exception
    '''''        Throw ex
    '''''    End Try
    '''''End Function
    Public Function paquete_aseguradora_subramo_inicio(ByVal intsubramo As Integer, ByVal strestatus As String, ByVal intpaquete As Integer, ByVal intprograma As Integer, ByVal intaon As Integer, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal banunico As Integer, ByVal CadenaPaquete As String) As DataTable

        Try
            'emmanuell 26/05/10 evi
            StrSql = "SELECT descripcion_aseguradora, id_paquete, descripcion_paquete, orden_paquete"
            If strestatus = "N" Then
                StrSql = StrSql & " FROM VW_VISTACOBERTURA_NUEVO"
            Else
                StrSql = StrSql & " FROM VW_VISTACOBERTURA_SEMINUEVO"
            End If
            StrSql = StrSql & " WHERE id_paquete in (" & CadenaPaquete & ")"
            StrSql = StrSql & " AND id_programa =" & intprograma & ""
            StrSql = StrSql & " AND Id_AonH = " & intaon & " "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function encabezado_coberturas_inicio(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, _
    ByVal strpaquete As String, ByVal Idtipocarga As Integer) As DataTable

        Try
            'strsql = "select distinct id_limresp, cobertura , orden"
            If Idtipocarga = 0 Then
                StrSql = "select distinct cobertura , orden"
            Else
                StrSql = "select cobertura ='Responsabilidad Civil Da�os por la Carga', orden = 98"
                StrSql = StrSql & " union"
                StrSql = StrSql & " select distinct cobertura , orden"
            End If
            StrSql = StrSql & " from VW_CARGA_COBERTURA_INICIO"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus= '" & strestatus & "'"
            If intpaquete > 0 Then
                StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            End If
            StrSql = StrSql & " and num_contrato = " & intcontrato & ""
            If strpaquete <> "" Then
                StrSql = StrSql & " and id_paquete in (" & strpaquete & ")"
            End If
            StrSql = StrSql & " union"
            StrSql = StrSql & " select cobertura = descripcion_cob, orden = 99"
            StrSql = StrSql & " from cobertura"
            StrSql = StrSql & " where Num_Contrato = " & intcontrato & ""
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " and orden_cob <> 0"
            StrSql = StrSql & " and sumaaseg_cob > 0"
            StrSql = StrSql & " order by orden, cobertura"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function encabezado_coberturas_inicio_homologada(ByVal strestatus As String, ByVal intpaquete As Integer, _
    ByVal intversion As Integer, ByVal intbid As Integer, ByVal intcontrato As Integer, ByVal strpaquete As String) As DataTable

        Try
            'strsql = "select distinct id_limresp, cobertura , orden"
            StrSql = "select distinct cobertura = DescripcionCorta_Limresp , orden = Orden_HomologaLimresp, toltip = DescripcionAmplia_Limresp"
            StrSql = StrSql & " from VW_CARGA_COBERTURA_INICIO_HOMOLOGADA"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus= '" & strestatus & "'"
            If intpaquete > 0 And strpaquete <> "" Then
                StrSql = StrSql & " and id_paquete = " & intpaquete & ""
            End If
            StrSql = StrSql & " and num_contrato = " & intcontrato & ""
            If strpaquete <> "" Then
                StrSql = StrSql & " and id_paquete in (" & strpaquete & ")"
            End If
            StrSql = StrSql & " order by Orden_HomologaLimresp, DescripcionCorta_Limresp "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function encabezado_coberturas_final_inicio(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intbid As Integer, _
    ByVal intcontrato As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer) As DataTable

        Try
            StrSql = "select distinct id_limresp, cobertura , orden"
            StrSql = StrSql & " from VW_CARGA_COBERTURAS_FINAL"
            StrSql = StrSql & " where id_bid = " & intbid & ""
            StrSql = StrSql & " and id_version = " & intversion & ""
            StrSql = StrSql & " and estatus = '" & strestatus & "'"
            StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            StrSql = StrSql & " and num_contrato = " & intcontrato & ""
            If intpaquete > 0 Then
                StrSql = StrSql & " and id_paquete =" & intpaquete & ""
            End If
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidaCoberturasExtras(ByVal IdAseguradora As Integer, ByVal strdescripcion As String) As DataTable
        Try
            StrSql = "select id_aseguradora from COBERTURA_ADICIONAL_CUOTA"
            StrSql = StrSql & " where descripcion_cobadic = '" & strdescripcion & "'"
            StrSql = StrSql & " and id_aseguradora = " & IdAseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_MontosCoberturasExtras(ByVal numcontrato As Integer, ByVal idbid As Integer, ByVal strdescripcion As String, ByVal idaseguradora As Integer) As DataTable
        Try
            StrSql = "select id_aseguradora, descripcion_suma = sumaaseg_cob, deducible = deducible_cob, orden = orden_cob, descripcion_tipo = sumaaseg_cob"
            StrSql = StrSql & " from cobertura"
            StrSql = StrSql & " where Num_Contrato = " & numcontrato & ""
            StrSql = StrSql & " and id_bid = " & idbid & ""
            StrSql = StrSql & " and orden_cob <> 0"
            StrSql = StrSql & " and descripcion_cob = '" & strdescripcion & "'"
            StrSql = StrSql & " and id_aseguradora = " & idaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_coberturas_inicio(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String) As DataTable

        Try

            If Not strcoberturadescripcion = "Responsabilidad Civil Da�os por la Carga" Then

                StrSql = "select distinct subramo, id_paquete,  id_aseguradora, descripcion_suma, deducible ,orden"
                StrSql = StrSql & " , descripcion_tipo = case when not id_aseguradora = 1 then case "
                'strsql = strsql & " when (cobertura = 'Gastos M�dicos' or cobertura = 'Gastos Medicos' or cobertura = 'Gastos Medicos (LUC)' or cobertura = 'Gastos M�dicos (LUC)' or cobertura = 'Gastos Medicos Ocupantes') "
                StrSql = StrSql & " when (bandera_ocupante = '1') "
                'If intprograma <> 7 And strcoberturadescripcion <> "Gastos Medicos (LUC)" Then
                'paneles contado
                If Not (intprograma = 7 Or intprograma = 8 Or intprograma = 12 Or intprograma = 13) Then
                    StrSql = StrSql & " then  '$' + VALORES(descripcion_suma) +' por Ocupante'"
                Else
                    StrSql = StrSql & " then  VALORES(descripcion_suma)"
                End If
                StrSql = StrSql & " else  descripcion_suma  End "
                StrSql = StrSql & " else descripcion_suma end"
                StrSql = StrSql & " from VW_CARGA_COBERTURA_INICIO"
                StrSql = StrSql & " where estatus = '" & strestatus & "'	"
                StrSql = StrSql & " and id_version = " & intversion & ""
                'strsql = strsql & " and id_paquete = " & intpaquete & ""
                StrSql = StrSql & " and id_paquete in (" & intpaquete & ")"
                StrSql = StrSql & " and id_bid = " & intbid & ""
                StrSql = StrSql & " AND id_aseguradora = " & intaseguradora & ""
                If intprograma = 9 Then
                    StrSql = StrSql & " AND (replace(cobertura,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                    StrSql = StrSql & " OR replace(cobertura,'Gastos Medicos Ocupantes','Gastos M�dicos')='" & strcoberturadescripcion & "')"
                Else
                    StrSql = StrSql & " AND (replace(cobertura,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                    'StrSql = StrSql & " OR replace(cobertura,'Gastos Medicos Ocupantes','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                    StrSql = StrSql & " OR replace(cobertura,'Gastos Medicos','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                    StrSql = StrSql & " OR cobertura='" & strcoberturadescripcion & "')"
                End If
                StrSql = StrSql & " AND descripcion_paquete='" & strdescripcion & "'"
            Else
                StrSql = "select descripcion_suma ='Amparada', descripcion_tipo='Amparada' ,deducible='', orden =0"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_coberturas_inicio_homologada(ByVal strestatus As String, _
    ByVal intversion As Integer, ByVal intpaquete As String, _
    ByVal intbid As Integer, ByVal intcotiza As Integer, ByVal intaseguradora As Integer, _
    ByVal strcoberturadescripcion As String, ByVal strdescripcion As String, _
    ByVal intprograma As Integer, ByVal strfincon As String, ByVal claveaon As Integer) As DataTable

        Try
            'cambio emmanuell 04/oct/2009

            StrSql = "select distinct id_aseguradora, descripcion_suma, deducible ,orden = Orden_HomologaLimresp"
            StrSql = StrSql & " , descripcion_tipo = "
            StrSql = StrSql & " case when not id_aseguradora = 1 then "
            StrSql = StrSql & " case when (bandera_ocupante = '1') then "
            StrSql = StrSql & " case when PlazaVehiculo(" & intaseguradora & "," & claveaon & ",'" & strestatus & "') > '0' then "
            StrSql = StrSql & " CALCULO_PLAZACANTIDAD(PlazaVehiculo(" & intaseguradora & "," & claveaon & ",'" & strestatus & "'),descripcion_suma)"
            StrSql = StrSql & " else "
            StrSql = StrSql & " '$' + VALORES(descripcion_suma) +' por Ocupante'"
            StrSql = StrSql & " end"
            StrSql = StrSql & " else  descripcion_suma End "
            StrSql = StrSql & " else descripcion_suma end"
            StrSql = StrSql & " from VW_CARGA_COBERTURA_INICIO_HOMOLOGADA"
            StrSql = StrSql & " where estatus = '" & strestatus & "'	"
            StrSql = StrSql & " and id_version = " & intversion & ""
            'strsql = strsql & " and id_paquete = " & intpaquete & ""
            StrSql = StrSql & " and id_paquete in (" & intpaquete & ")"
            StrSql = StrSql & " and id_bid = " & intbid & ""
            StrSql = StrSql & " AND id_aseguradora = " & intaseguradora & ""
            If intprograma = 9 Then
                StrSql = StrSql & " AND (replace(DescripcionCorta_Limresp,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                StrSql = StrSql & " OR replace(DescripcionCorta_Limresp,'Gastos Medicos Ocupantes','Gastos M�dicos')='" & strcoberturadescripcion & "')"
            Else
                'strsql = strsql & " AND DescripcionCorta_Limresp ='" & strcoberturadescripcion & "'"
                StrSql = StrSql & " AND (replace(DescripcionCorta_Limresp,'Gastos Medicos (LUC)','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                StrSql = StrSql & " OR replace(DescripcionCorta_Limresp,'Gastos Medicos Ocupantes','Gastos M�dicos')='" & strcoberturadescripcion & "'"
                StrSql = StrSql & " OR DescripcionCorta_Limresp='" & strcoberturadescripcion & "')"
            End If
            StrSql = StrSql & " AND descripcion_paquete='" & strdescripcion & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Bandera_precio_danos(ByVal intaseguradora As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "SELECT ban_factura "
            StrSql = StrSql & " FROM abc_programa_aseguradora"
            StrSql = StrSql & " where id_programa = " & intprograma & ""
            StrSql = StrSql & " and id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and estatus_programaaseguradora = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region "Guardando Cotizacion"
    Public Sub inserta_poliza_cotizacion(ByVal intbid As Integer, ByVal strtipopol As String, _
    ByVal strseguro As Integer, ByVal contador_m As Integer, ByVal fechaini As String, _
    ByVal NumPlazoPoliza As Integer, ByVal strpaquete As String, _
    ByVal strsubramo As String, ByVal stropcsa As String, ByVal strdeeler As String, _
    ByVal strmoneda As String, ByVal intaonH As Integer, ByVal ArrPolisa() As String, _
    ByVal strserie As String, ByVal intuso As Integer, ByVal strcobertura As String, _
    ByVal strmotor As String, _
    ByVal strcontrato As String, ByVal strprimaneta As String, ByVal strderpol As String, _
    ByVal VLIvaSeguro As String, ByVal strprimatotal As String, ByVal bUnidadfinanciada As String, _
    ByVal strzurich As String, ByVal strcolor As String, ByVal strplazo As String, ByVal arrsub() As String, _
    ByVal strPrecio_Ctrl As String, ByVal strprecio As String, ByVal intversion As Integer, _
    ByVal strestatus As String, ByVal intpaquete As Integer, ByVal poliza As String, _
    ByVal strventa As String, ByVal intcliente As Integer, ByVal intregion As Integer, _
    ByVal intcontratopol As Integer, ByVal tcontrato As Integer, _
    ByVal intbanf As Integer, ByVal fechavalor As String, ByVal intusuario As Integer, _
    ByVal strrenave As String, ByVal intaseguradora As Integer, ByVal stranio As String, _
    ByVal bandeduc As Integer, ByVal intprograma As Integer, ByVal strfincon As String, _
    ByVal polizaAce As String, ByVal strbansegvida As Integer, ByVal strbansegdesempleo As Integer, _
    ByVal strpaqueteA As String, ByVal strfactura As String, ByVal dbiva As Double, _
    ByVal TipoFinanciamiento As String, ByVal Strfracciones As String, ByVal dbrecargo As Double, _
    ByVal banrecargo As Integer, ByVal banfactor As Integer, ByVal ValorCotipoliza As Integer, _
    ByVal strvida As String, ByVal strdesempleo As String, ByVal MulFraccion As String)
        Dim ik As Integer
        Dim acumula_fecha As Integer
        Dim cadena As String
        Dim i As Integer
        Dim res As String = ""
        Dim DblPolizas As Double
        Dim res_tmpplazo As Integer
        Dim ValorDEDUC_PL As Double
        Dim deducpl As Double = 0
        Dim NoPolZurich As String
        Dim Temp_poliza As String
        Dim ds As New DataSet
        Dim banprimatotal As Double = 0
        Dim banprimaneta As Double = 0
        Dim IntValorPol As Integer = 0
        Dim dudc As Double = 0
        Dim acumual_fecha As Double = 0
        Dim dbFactor As Double

        Dim asfecha() As String
        Dim fechaV As String
        Dim fecha1 As String
        Dim fecha2 As String
        Dim fecha3 As String
        Dim fecha4 As String
        Dim fecha5 As String
        Dim fecha6 As String
        Dim cadenafecha As String
        Dim strinsco As String
        Dim banIvaPN As Double = 0

        Dim strmarca As String = ""
        Dim strmodelo As String = ""
        Dim dbenganche As Double = 0

        Dim dsm As New DataSet
        Dim IntMaximo As Integer = 0

        Dim banocupante As Integer = 0

        Try
            StrTransaccion = "inserta_poliza_cotizacion"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            If strseguro = 0 Then
                'contado
                For i = 0 To 4
                    DblPolizas = DblPolizas + IIf(ArrPolisa(i) <> "", ArrPolisa(i), 0)
                Next i
            End If

            asfecha = fechaini.Split("/")
            If intbanf = 1 Then
                fecha3 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
            Else
                fecha3 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
            End If

            For ik = 0 To contador_m

                StrSql = "insert into COTI_POLIZA (id_cotiza_poliza, id_bid, Fecha_registro, poliza, contador_poliza, id_aseguradora, cadenavida, cadenaace) "
                StrSql = StrSql & " values (" & ValorCotipoliza & "," & intbid & " , getdate(),'" & poliza & "', " & ik & "," & intaseguradora & ", '" & strvida & "','" & strdesempleo & "')"
                CConecta.EjecutaSQL(StrTransaccion, StrSql)

                StrSql = "SELECT SCOPE_IDENTITY() AS maximo"
                dsm = CConecta.AbrirConsulta(StrSql, False, , , True)
                IntMaximo = dsm.Tables(0).Rows(0)(0)

                If strseguro = 0 And Not strtipopol = "F" Then
                    'strsql = strsql & " values('A',"
                    StrSql = " update COTI_POLIZA set Tipo_poliza='A',"
                Else
                    StrSql = " update COTI_POLIZA set Tipo_poliza='" & strtipopol & "',"
                End If

                If contador_m = 0 Then
                    StrSql = StrSql & " fvigencia='" & fecha3 & "',"
                    StrSql = StrSql & " VIGENCIA_FINIP='" & fecha3 & "',"

                    'vida se calcula en base al numero de plazo de todo el financiamiento
                    cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fechaV = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fechaV = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If

                    'NumPlazoPoliza solo para multianual cuando es de contado es a 12
                    'cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    If strtipopol = "A" Then
                        'anual
                        cadenafecha = DateAdd(DateInterval.Month, 12, CDate(fechaini))
                    Else
                        'multianual
                        cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    End If
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha1 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha1 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    StrSql = StrSql & " VIGENCIA_FFINP='" & fecha1 & "',"

                    'cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                    cadenafecha = DateAdd(DateInterval.Month, 12, CDate(fechaini))
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    StrSql = StrSql & " ffin='" & fecha5 & "',"

                Else
                    'If ik = 0 Then
                    '    strsql = strsql & " fvigencia='" & fecha3 & "',"
                    '    strsql = strsql & " VIGENCIA_FINIP='" & fecha3 & "',"

                    '    cadenafecha = DateAdd(0, ik + 1, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    'Else
                    '    strsql = strsql & " fvigencia='" & fecha3 & "',"

                    '    cadenafecha = DateAdd(0, ik, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha4 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha4 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FINIP='" & fecha4 & "',"

                    '    cadenafecha = DateAdd(0, ik + 1, CDate(fechaini))
                    '    asfecha = cadenafecha.Split("/")
                    '    If intbanf = 1 Then
                    '        fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    '    Else
                    '        fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    '    End If
                    '    strsql = strsql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    'End If

                    'cadenafecha = DateAdd(0, contador_m + 1, CDate(fechaini))
                    'asfecha = cadenafecha.Split("/")
                    'If intbanf = 1 Then
                    '    fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    'Else
                    '    fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    'End If
                    'strsql = strsql & " ffin='" & fecha5 & "',"

                    'nuevo evi 17/06/2009
                    StrSql = StrSql & " fvigencia='" & fecha3 & "',"
                    If ik = 0 Then
                        StrSql = StrSql & " VIGENCIA_FINIP='" & fecha3 & "',"
                    Else
                        cadenafecha = DateAdd(DateInterval.Year, (ik * 1), CDate(fechaini))
                        asfecha = cadenafecha.Split("/")
                        If intbanf = 1 Then
                            fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                        Else
                            fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                        End If
                        StrSql = StrSql & " VIGENCIA_FINIP='" & fecha2 & "',"
                    End If
                    If TipoFinanciamiento = "MO" Then
                        'financiado MO 25 y 37
                        If (NumPlazoPoliza = 25) Then
                            If ik = 0 Then
                                cadenafecha = DateAdd(DateInterval.Year, 1, CDate(fechaini))
                            Else
                                cadenafecha = DateAdd(DateInterval.Month, 25, CDate(fechaini))
                            End If
                            asfecha = cadenafecha.Split("/")
                            If intbanf = 1 Then
                                fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                            Else
                                fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                            End If
                            StrSql = StrSql & " VIGENCIA_FFINP='" & fecha2 & "',"
                        Else
                            Select Case ik
                                Case 0
                                    cadenafecha = DateAdd(DateInterval.Year, 1, CDate(fechaini))
                                Case 1
                                    cadenafecha = DateAdd(DateInterval.Year, 2, CDate(fechaini))
                                Case 2
                                    cadenafecha = DateAdd(DateInterval.Month, 37, CDate(fechaini))
                            End Select
                            asfecha = cadenafecha.Split("/")
                            If intbanf = 1 Then
                                fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                            Else
                                fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                            End If
                            StrSql = StrSql & " VIGENCIA_FFINP='" & fecha2 & "',"
                        End If
                    Else
                        'financiado WOR todos
                        If NumPlazoPoliza <= 12 Then
                            cadenafecha = DateAdd(DateInterval.Month, NumPlazoPoliza, CDate(fechaini))
                        Else
                            If ik = 0 Then
                                acumual_fecha = NumPlazoPoliza / 12
                            End If
                            If ik <= acumual_fecha Then
                                cadenafecha = DateAdd(DateInterval.Month, ((ik + 1) * 12), CDate(fechaini))
                            Else
                                acumual_fecha = NumPlazoPoliza - ((ik + 1) * 12)
                                cadenafecha = DateAdd(DateInterval.Month, acumual_fecha, CDate(fechaini))
                            End If
                        End If
                        asfecha = cadenafecha.Split("/")
                        If intbanf = 1 Then
                            fecha2 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                        Else
                            fecha2 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                        End If
                        StrSql = StrSql & " VIGENCIA_FFINP='" & fecha2 & "',"
                    End If
                    'nuevo evi 17/06/2009
                    cadenafecha = DateAdd(0, contador_m + 1, CDate(fechaini))
                    asfecha = cadenafecha.Split("/")
                    If intbanf = 1 Then
                        fecha5 = asfecha(1) & "/" & asfecha(0) & "/" & asfecha(2)
                    Else
                        fecha5 = asfecha(0) & "/" & asfecha(1) & "/" & asfecha(2)
                    End If
                    StrSql = StrSql & " ffin='" & fecha5 & "',"
                End If
                'strsql = strsql & " PRODUCTO='" & strpaquete & "',"
                StrSql = StrSql & " PRODUCTO='" & strpaqueteA & "',"
                StrSql = StrSql & " moneda='" & strmoneda & "',"
                StrSql = StrSql & " SOCIO='" & strdeeler & "',"

                Dim dtVi As DataTable = carga_insco_homologado(intaonH, stranio, intaseguradora, strestatus, strsubramo)
                If dtVi.Rows.Count > 0 Then
                    If Not dtVi.Rows(0).IsNull("insco") Then
                        strinsco = dtVi.Rows(0)("insco")
                    End If
                End If

                StrSql = StrSql & " insco='" & strinsco & "',"

                Dim dtv As DataTable = vehiculo_general(strinsco, intversion, strestatus, strsubramo, stranio, intaseguradora, intaonH)
                If dtv.Rows.Count > 0 Then
                    If dtv.Rows(0).IsNull("marca") Then
                        cadena = ""
                        strmarca = ""
                    Else
                        cadena = dtv.Rows(0)("marca")
                        strmarca = cadena
                    End If
                    StrSql = StrSql & " marca='" & cadena & "',"

                    If dtv.Rows(0).IsNull("modelo_vehiculo") Then
                        cadena = ""
                        strmodelo = ""
                    Else
                        cadena = dtv.Rows(0)("modelo_vehiculo")
                        strmodelo = cadena
                    End If
                    StrSql = StrSql & " modelo='" & cadena & "',"

                    If dtv.Rows(0).IsNull("descripcion_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = dtv.Rows(0)("descripcion_vehiculo")
                    End If
                    StrSql = StrSql & " DESCRIPCION='" & cadena & "',"

                    If dtv.Rows(0).IsNull("anio_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = dtv.Rows(0)("anio_vehiculo")
                    End If
                    StrSql = StrSql & " anio='" & cadena & "',"

                    If dtv.Rows(0).IsNull("plaza_vehiculo") Then
                        cadena = ""
                    Else
                        cadena = CInt(dtv.Rows(0)("plaza_vehiculo"))
                    End If
                    StrSql = StrSql & " capacidad=" & cadena & ","
                End If
                StrSql = StrSql & " serie='" & strserie & "',"
                StrSql = StrSql & " num_factura='" & strfactura & "',"
                StrSql = StrSql & " id_uso=" & intuso & ","
                StrSql = StrSql & " COBERTURA=" & CDbl(strcobertura) & ","
                StrSql = StrSql & " RENAVE='" & strrenave & "',"
                StrSql = StrSql & " motor='" & strmotor & "',"
                'strsql = strsql & " placas='" & strplacas & "',"
                'auto sustituto 'strsql = strsql & " COBADDP1_M = 0,"
                StrSql = StrSql & " COBADDP2_M = 0,"
                'ext. resp. civil 'strsql = strsql & " COBADDP3_M = 0,"
                StrSql = StrSql & " COBADDP4_M = 0,"
                StrSql = StrSql & " COBADDP5_M = 0,"
                Dim dtc As DataTable = cobertura_bid(intcontratopol, intbid, intaseguradora)
                If dtc.Rows.Count > 0 Then
                    For i = 0 To dtc.Rows.Count - 1
                        If dtc.Rows(i)("descripcion_cob") = "Equipo Especial" Then
                            If strseguro <> 0 Then
                                StrSql = StrSql & " COBADDF1_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 4) & ","
                            Else
                                StrSql = StrSql & " COBADDF1_M = " & Math.Round(dtc.Rows(i)("total_cob"), 4) & ","
                            End If
                            StrSql = StrSql & " COBADDF1_LC =" & IIf(dtc.Rows(i)("sumaaseg_cob") = "", 0, dtc.Rows(i)("sumaaseg_cob")) & ","
                            StrSql = StrSql & " COBADDF1_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            StrSql = StrSql & " COBADDF1_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Adaptacion y conv." Then
                            If strseguro <> 0 Then
                                StrSql = StrSql & " COBADDF2_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 4) & ","
                            Else
                                StrSql = StrSql & " COBADDF2_M = " & Math.Round(dtc.Rows(i)("total_cob"), 4) & ","
                            End If
                            StrSql = StrSql & " COBADDF2_LC =" & dtc.Rows(i)("sumaaseg_cob") & ","
                            StrSql = StrSql & " COBADDF2_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            StrSql = StrSql & " COBADDF2_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Blindaje" Then
                            If strseguro <> 0 Then
                                StrSql = StrSql & " COBADDF3_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 4) & ","
                            Else
                                StrSql = StrSql & " COBADDF3_M = " & Math.Round(dtc.Rows(i)("total_cob"), 4) & ","
                            End If
                            StrSql = StrSql & " COBADDF3_LC =" & IIf(dtc.Rows(i)("sumaaseg_cob") = "", 0, dtc.Rows(i)("sumaaseg_cob")) & ","
                            StrSql = StrSql & " COBADDF3_D = '" & dtc.Rows(i)("desc_cob") & "',"
                            StrSql = StrSql & " COBADDF3_F ='" & dtc.Rows(i)("numfact_cob") & "',"
                        End If

                        If dtc.Rows(i)("descripcion_cob") = "Auto Sustituto" Then
                            If strseguro <> 0 Then
                                StrSql = StrSql & " COBADDP1_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 4) & ","
                            Else
                                StrSql = StrSql & " COBADDP1_M = " & Math.Round(dtc.Rows(i)("total_cob"), 4) & ","
                            End If
                        End If
                        If dtc.Rows(i)("descripcion_cob") = "Ext. Res. Civil" Then
                            If strseguro <> 0 Then
                                StrSql = StrSql & " COBADDP3_M = " & Math.Round(dtc.Rows(i)("total_cob") / (contador_m + 1), 4) & ","
                            Else
                                StrSql = StrSql & " COBADDP3_M = " & Math.Round(dtc.Rows(i)("total_cob"), 4) & ","
                            End If
                        End If
                    Next
                End If
                Dim dtr As DataTable = limresp(intaseguradora, intversion, intpaquete)
                If dtr.Rows.Count > 0 Then
                    If Not dtr.Rows(0).IsNull("descripcion_suma") Then
                        res = dtr.Rows(0)("descripcion_suma")
                    End If
                    If Not dtr.Rows(0).IsNull("bandera_ocupante") Then
                        banocupante = dtr.Rows(0)("bandera_ocupante")
                    End If
                End If
                If Not res = "" Then
                    If Not intaseguradora = 1 Then
                        If banocupante > 0 Then
                            StrSql = StrSql & " GASTOSMED=" & Math.Round((res * cadena), 4) & ","
                        Else
                            StrSql = StrSql & " GASTOSMED=" & res & ","
                        End If
                    Else
                        StrSql = StrSql & " GASTOSMED=" & res & ","
                    End If
                End If
                StrSql = StrSql & " TASAFIN=0,"
                StrSql = StrSql & " DERPOL=" & Math.Round(CDbl(strderpol), 4) & ","
                If strtipopol = "A" Then
                    StrSql = StrSql & " PRIMATOTAL = " & Math.Round(CDbl(strprimatotal), 4) & ","
                    banprimatotal = strprimatotal
                Else
                    If strseguro = 0 Then
                        StrSql = StrSql & " PRIMATOTAL = " & Math.Round(DblPolizas, 4) & ","
                        banprimatotal = DblPolizas
                    Else
                        StrSql = StrSql & " PRIMATOTAL =" & Math.Round(CDbl(ArrPolisa(ik)), 4) & ","
                        banprimatotal = ArrPolisa(ik)
                    End If
                End If
                'strsql = strsql & " PRIMANETA=" & Math.Round(((banprimatotal / 1.16) - strderpol), 4) & ","
                'banprimaneta = Math.Round(((banprimatotal / 1.16) - strderpol), 4)
                'strsql = strsql & " IVA=" & Math.Round(((banprimaneta + strderpol) * 0.16), 4) & ","

                If strtipopol = "F" Then
                    If banrecargo = 1 Then
                        banprimaneta = Math.Round((((banprimatotal / (1 + dbiva)) - strderpol) / (1 + dbrecargo)), 4)
                        banIvaPN = Math.Round(((banprimaneta * dbrecargo) + banprimaneta + strderpol) * dbiva, 4)
                        StrSql = StrSql & " Recargos='" & (dbrecargo * 100) & "',"
                    Else
                        banprimaneta = Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 4)
                        banIvaPN = Math.Round((banprimaneta + strderpol) * dbiva, 4)
                    End If
                Else
                    If intaseguradora = 12 Then
                        Dim dtf As DataTable = Factores(intaseguradora, ik + 1)
                        If dtf.Rows.Count > 0 Then
                            dbFactor = dtf.Rows(0)("valor")
                        End If
                        banprimaneta = Math.Round(strprimaneta * dbFactor, 4)
                    Else
                        banprimaneta = Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 4)
                    End If
                    banIvaPN = Math.Round(((banprimaneta + strderpol) * dbiva), 4)
                End If

                'strsql = strsql & " PRIMANETA=" & Math.Round(((banprimatotal / (1 + dbiva)) - strderpol), 4) & ","
                StrSql = StrSql & " PRIMANETA=" & banprimaneta & ","
                StrSql = StrSql & " IVA=" & banIvaPN & ","
                'strsql = strsql & " IVA=" & Math.Round(((banprimaneta + strderpol) * dbiva), 4) & ","

                'esto nos indica si la unidad es financiada o no 
                If bUnidadfinanciada = True Then
                    'cuando sea ald va a decir 
                    'strsql = strsql & " ESPOLIZA='F-ALD',"
                    StrSql = StrSql & " ESPOLIZA='F-Ford Credit',"
                Else
                    'al final debe de aparacer C-nombre de la agencia, por el momento lo aremos desde la vista
                    StrSql = StrSql & " ESPOLIZA='" & strdeeler & "',"
                End If

                If strtipopol = "A" Then
                    'strsql = strsql & " PLAZO = " & NumPlazoPoliza & ", "
                    StrSql = StrSql & " PLAZO = 12, "
                Else
                    If TipoFinanciamiento = "MO" Then
                        'financiado MO 25 y 37
                        If NumPlazoPoliza = 25 Then
                            If ik = 0 Then
                                StrSql = StrSql & " PLAZO = 12, "
                            Else
                                StrSql = StrSql & " PLAZO = 13, "
                            End If
                        Else
                            If ik = 0 Then
                                StrSql = StrSql & " PLAZO = 12, "
                            Else
                                If ik = 1 Then
                                    StrSql = StrSql & " PLAZO = 12, "
                                Else
                                    StrSql = StrSql & " PLAZO = 13, "
                                End If
                            End If
                        End If
                    Else
                        If strtipopol = "F" Then
                            StrSql = StrSql & " PLAZO = " & NumPlazoPoliza & ", "
                        Else
                            If strseguro = 0 Then
                                StrSql = StrSql & " PLAZO = " & NumPlazoPoliza & ", "
                            Else
                                'financiado WOR todos
                                If ik = 0 Then res_tmpplazo = 12
                                If ik = 1 Then res_tmpplazo = 12 * 2
                                If ik = 2 Then res_tmpplazo = 12 * 3
                                If ik = 3 Then res_tmpplazo = 12 * 4
                                If ik = 4 Then res_tmpplazo = 12 * 5

                                If res_tmpplazo <= NumPlazoPoliza Then
                                    StrSql = StrSql & " PLAZO = 12 ,"
                                Else
                                    StrSql = StrSql & " PLAZO = " & NumPlazoPoliza - (12 * ik) & ","
                                End If
                            End If
                        End If
                    End If
                End If

                'este se queda pendiente hasta su captura
                Dim dtA As DataTable = nombre_beneficiario(tcontrato, intcliente, intbid)
                If dtA.Rows.Count > 0 Then
                    If Not dtA.Rows(0).IsNull("BENEFICIARIO_PREFERENTE") Then
                        StrSql = StrSql & " BENEF_PREF ='" & dtA.Rows(0)("BENEFICIARIO_PREFERENTE") & "',"
                    End If
                    If Not dtA.Rows(0).IsNull("enganche") Then
                        dbenganche = dtA.Rows(0)("enganche")
                    End If
                End If

                If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                    StrSql = StrSql & " FINCON='" & strfincon & "',"
                Else
                    StrSql = StrSql & " FINCON='C',"
                End If
                StrSql = StrSql & " Tipo_Carga='No Peligrosa',"
                StrSql = StrSql & " Cve_Zur='" & strinsco & "', "
                StrSql = StrSql & " COLOR='" & strcolor & "', "
                StrSql = StrSql & " FCOPER=getdate(),"
                StrSql = StrSql & " PAGFRAC=" & strplazo & ","
                If ik = 0 Then
                    StrSql = StrSql & " SUBSIDYF  = " & Math.Round(CDbl(arrsub(0)), 4) & ", "
                    StrSql = StrSql & " SUBSIDYFC  = " & Math.Round(CDbl(arrsub(1)), 4) & ", "
                    StrSql = StrSql & " SUBSIDYD  = " & Math.Round(CDbl(arrsub(2)), 4) & ", "
                    'emmanuell
                    StrSql = StrSql & " SUBSIDYLF = " & Math.Round(CDbl(ArrPolisa(9)), 4) & ","
                    StrSql = StrSql & " SUBSIDYLFC = " & Math.Round(CDbl(ArrPolisa(10)), 4) & ","
                    StrSql = StrSql & " SUBSIDYLD = " & Math.Round(CDbl(ArrPolisa(11)), 4) & ","
                    StrSql = StrSql & " SUBSIDYLS = " & Math.Round(CDbl(ArrPolisa(12)), 4) & ","
                Else
                    StrSql = StrSql & " SUBSIDYF  = 0,"
                    StrSql = StrSql & " SUBSIDYFC  = 0,"
                    StrSql = StrSql & " SUBSIDYD = 0,"
                    'emmanuell
                    StrSql = StrSql & " SUBSIDYLF  = 0,"
                    StrSql = StrSql & " SUBSIDYLFC  = 0,"
                    StrSql = StrSql & " SUBSIDYLD = 0,"
                    StrSql = StrSql & " SUBSIDYLS = 0,"
                End If

                If ik = 0 Then
                    If ArrPolisa(5) = "" Or ArrPolisa(5) = 0 Then
                        StrSql = StrSql & " CAMNUM01 = 0,"
                    Else
                        If Val(ArrPolisa(5)) >= Val(ArrPolisa(ik)) Then
                            StrSql = StrSql & " CAMNUM01 = 0,"
                            StrSql = StrSql & " SUBSIDYF  = " & Math.Round(CDbl(ArrPolisa(ik)), 4) & ", "
                        Else
                            StrSql = StrSql & " CAMNUM01 = " & Math.Round(CDbl((ArrPolisa(ik) - ArrPolisa(5))), 4) & ","
                        End If
                    End If
                Else
                    StrSql = StrSql & " CAMNUM01 = 0,"
                End If

                StrSql = StrSql & " PRECIO_CTRL  = " & Math.Round(CDbl(strPrecio_Ctrl), 4) & ","
                StrSql = StrSql & " precio_vehiculo =" & Math.Round(CDbl(strprecio), 4) & ","
                StrSql = StrSql & " id_version=" & intversion & ","
                'strsql = strsql & " estatus='" & strestatus & "',"
                StrSql = StrSql & " FSCSTATUS='" & strestatus & "',"
                StrSql = StrSql & " OPCSA='" & stropcsa & "',"
                StrSql = StrSql & " id_paquete=" & intpaquete & ","
                If bandeduc = 1 Then
                    If strestatus = "N" Then
                        Select Case intprograma
                            Case 1 'ALD
                                If intpaquete = 3 Then
                                    StrSql = StrSql & " DEDUC_PL = " & Math.Round(CDbl(strPrecio_Ctrl * 0.07), 4) & ","
                                    ValorDEDUC_PL = Math.Round(CDbl(strPrecio_Ctrl * 0.07), 4)
                                Else
                                    StrSql = StrSql & " DEDUC_PL =" & Math.Round(CDbl(strPrecio_Ctrl * 0.06), 4) & ","
                                    ValorDEDUC_PL = strPrecio_Ctrl * 0.06
                                End If
                            Case 3, 5, 7, 8, 9
                                '3 mazda
                                '5 emision (Aon Primas Nacionales)
                                '7 CONTADO (PANEL)
                                '8 MAZDA (PANEL)
                                Dim dtD As DataTable = determinando_impresion_deducpl(intaseguradora, intpaquete, strsubramo, intprograma)
                                If dtD.Rows.Count > 0 Then
                                    If Not dtD.Rows(0).IsNull("deduc_pl") Then
                                        dudc = dtD.Rows(0)("deduc_pl")
                                    End If
                                End If
                                StrSql = StrSql & " DEDUC_PL = " & Math.Round(CDbl(strPrecio_Ctrl * dudc), 4) & ","
                                ValorDEDUC_PL = Math.Round(CDbl(strPrecio_Ctrl * dudc), 4)
                        End Select
                    Else
                        Dim dtpl As DataTable = deducpl_vehiculo(intaseguradora, strinsco, stranio, intaonH)
                        If dtpl.Rows.Count > 0 Then
                            If Not dtpl.Rows(0).IsNull("deducpl_vehiculo") Then
                                deducpl = dtpl.Rows(0)("deducpl_vehiculo")
                            End If
                            StrSql = StrSql & " DEDUC_PL = " & deducpl & ","
                            ValorDEDUC_PL = deducpl
                        End If
                    End If
                Else
                    StrSql = StrSql & " DEDUC_PL = 0,"
                End If
                'micha

                If intprograma = 5 Or intprograma = 9 Then
                    StrSql = StrSql & " PART_THEFT_COV ='" & IIf(Mid(strmodelo, 1, 8) = "ECOSPORT", "$30,000", "$50,000") & "',"
                    StrSql = StrSql & " PART_THEFT_COVN =" & IIf(Mid(strmodelo, 1, 8) = "ECOSPORT", 30000, 50000) & ","
                    StrSql = StrSql & " Part_Theft_Ded ='$3,000',"
                    StrSql = StrSql & " Part_Theft_DedN =3000,"
                Else
                    StrSql = StrSql & " PART_THEFT_COV ='0',"
                    StrSql = StrSql & " PART_THEFT_COVN =0,"
                    StrSql = StrSql & " Part_Theft_Ded ='0',"
                    StrSql = StrSql & " Part_Theft_DedN =0,"
                End If

                StrSql = StrSql & " id_region=" & intregion & ","
                StrSql = StrSql & " id_usuario =" & intusuario & ","
                If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                    StrSql = StrSql & " bansegvida='" & strbansegvida & "',"
                    StrSql = StrSql & " bansegdesempleo='" & strbansegdesempleo & "',"
                End If
                If ik = 0 Then
                    StrSql = StrSql & " poliza_multianual = '" & poliza & "'"
                    NoPolZurich = poliza
                Else
                    Temp_poliza = poliza & "0" & ik
                    StrSql = StrSql & " poliza_multianual = '" & Temp_poliza & "'"
                End If
                StrSql = StrSql & " where id_cotiza_poliza=" & ValorCotipoliza & ""
                StrSql = StrSql & " and id_poliza = " & IntMaximo & ""
                CConecta.EjecutaSQL(StrTransaccion, StrSql)

                'inserta_poliza_cliente(valor, intbid, intcliente, tcontrato)
            Next

            inserta_firmaAbajo_coti(ValorCotipoliza, IntMaximo, intbid, strinsco, strventa, strprecio, strseguro)

            If strtipopol = "F" Then
                'en el caso de que sea multianual determinamos el numero de fracciones
                inserta_registro_coti(ValorCotipoliza, IntMaximo, intbid, intaseguradora, fechaini, MulFraccion, "0", strderpol)
            End If

            If intprograma = 4 Or intprograma = 5 Or intprograma = 9 Then
                If strbansegdesempleo = 1 Then
                    'desempleo
                    inserta_polizaACE_coti(ValorCotipoliza, IntMaximo, intaseguradora, intbid, NoPolZurich, polizaAce, _
                                    fecha3, fecha1, strmarca, strmodelo, stranio, strserie, ArrPolisa(8), _
                                    NumPlazoPoliza, strinsco, strmoneda, intversion)
                End If

                If strbansegvida = 1 Then
                    'vida
                    Dim coberturaVida As Double = 0
                    Dim Benevida As String = "Ford Credit de M�xico S.A. de C.V. Sociedad Financiera de Objeto Limitado"
                    Dim derpolvida As Double = 150
                    Dim primanetaVida As Double = ArrPolisa(7) - derpolvida
                    If strfincon = "F" Then
                        'financiado
                        coberturaVida = ((((strprecio - dbenganche) + DblPolizas) + CDbl(ArrPolisa(8))) + CDbl(ArrPolisa(7)))
                    Else
                        'contado
                        coberturaVida = strprecio - dbenganche
                    End If

                    inserta_polizaVida_coti(ValorCotipoliza, IntMaximo, intaseguradora, NoPolZurich, fecha3, fechaV, ArrPolisa(7), _
                            NumPlazoPoliza, strmoneda, intversion, coberturaVida, _
                            Benevida, primanetaVida, derpolvida)
                End If
            End If

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_firmaAbajo_coti(ByVal intcotipoliza As Integer, ByVal intpoliza As Integer, ByVal intbid As Integer, _
    ByVal strisco As String, ByVal strventa As String, _
    ByVal dbprecio As Double, ByVal intSeguro As Integer)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_firmaAbajo_coti"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into COTI_FIRMAABAJO (Id_Cotiza_poliza, id_poliza, id_bid, isco, tipo_venta, "
            sql = sql & " tipo_poliza, precio_auto)"
            sql = sql & " values (" & intcotipoliza & "," & intpoliza & "," & intbid & ",'" & strisco & "','"
            sql = sql & strventa & "',"
            If intSeguro = 0 Then
                sql = sql & " 'C',"
            Else
                sql = sql & " 'F',"
            End If
            sql = sql & dbprecio & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_registro_coti(ByVal intcotiza As Integer, ByVal intpoliza As Integer, ByVal intbid As Integer, ByVal intaseguradora As Integer, ByVal strfechaF As String, ByVal strfraccion As String, ByVal strTipoRegistro As Char, ByVal dbderpol As Double)
        Dim i As Integer = 0
        Dim arr() As String = strfraccion.Split("|")
        Dim cadenafecha As String = ""
        Dim sql As String = ""
        Try

            StrTransaccion = "inserta_registro_coti"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To arr.Length - 1
                Dim arrFraccion() As String = arr(i).Split("*")
                If Not arrFraccion(0) = "" Then
                    cadenafecha = DateAdd(DateInterval.Year, i, CDate(strfechaF))
                    sql = " insert into coti_poliza_recibo (id_cotiza_poliza, id_poliza, id_bid, id_aseguradora, "
                    sql = sql & " num_fraccion, registro, Fecha_Recibo, Iva_Recibo, Total_Recibo, "
                    sql = sql & " Pneta_recibo, recargo_recibo, periodo_recibo, GastosExp_recibo)"
                    sql = sql & " values(" & intcotiza & "," & intpoliza & " , " & intbid & ", " & intaseguradora & ", "
                    sql = sql & arrFraccion(0) & ",'" & strTipoRegistro & "','" & cadenafecha & "', '"
                    sql = sql & arrFraccion(1) & "','" & arrFraccion(2) & "','"
                    sql = sql & arrFraccion(3) & "','" & arrFraccion(5) & "',"
                    If i = 0 Then
                        sql = sql & arrFraccion(4) & "," & dbderpol & ")"
                    Else
                        sql = sql & arrFraccion(4) & ",0)"
                    End If
                    CConecta.EjecutaSQL(StrTransaccion, sql)
                End If
            Next

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_polizaACE_coti(ByVal intcotipoliza As Integer, ByVal intpoliza As Integer, ByVal intaseguradora As Integer, ByVal intbid As Integer, _
    ByVal strpoliza As String, ByVal strpolizaAce As String, ByVal strfechaI As String, _
    ByVal strfechaf As String, ByVal strmarca As String, ByVal strmodelo As String, ByVal intanio As Integer, _
    ByVal strserie As String, ByVal dbprima As Double, ByVal intplazo As Integer, _
    ByVal strinsco As String, ByVal strmoneda As String, ByVal intversion As Integer)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_polizaACE_coti"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into coti_poliza_desempleo (Id_Cotiza_poliza, id_polizaACE, id_aseguradora, id_bid, poliza, polizaace,"
            sql = sql & " vigencia_Inicial,"
            sql = sql & " vigencia_final, marca, modelo, anio, serie, "
            sql = sql & " prima_desempleo, plazo_financiamiento, "
            sql = sql & " insco, moneda, id_version) values(" & intcotipoliza & "," & intpoliza & "," & intaseguradora & ","
            sql = sql & intbid & ", '" & strpoliza & "','" & strpolizaAce & "','"
            sql = sql & strfechaI & "','" & strfechaf & "','" & strmarca & "','"
            sql = sql & strmodelo & "'," & intanio & ",'" & strserie & "',"
            sql = sql & dbprima & ", " & intplazo & ",'" & strinsco & "','"
            sql = sql & strmoneda & "'," & intversion & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub inserta_polizaVida_coti(ByVal intcotipoliza As Integer, ByVal intpoliza As Integer, ByVal intaseguradora As Integer, _
   ByVal strpoliza As String, ByVal strfechaI As String, _
   ByVal strfechaf As String, ByVal dbprimavida As Double, ByVal intplazo As Integer, _
   ByVal strmoneda As String, ByVal intversion As Integer, ByVal dbcobertura As Double, _
   ByVal strbenef As String, ByVal dbprimanetavida As Double, ByVal dbderpolvida As Double)
        Dim sql As String = ""
        Try
            StrTransaccion = "inserta_polizaVida_coti"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            sql = "insert into coti_poliza_vida (Id_Cotiza_poliza, id_polizavida, id_aseguradora, "
            sql = sql & " polizavida, vigencia_inicial, vigencia_final, moneda, "
            sql = sql & " cobertura, prima_vida , plazo_financiamiento, beneficiario, "
            sql = sql & " primaneta_vida, derpol_vida, id_version)values(" & intcotipoliza & "," & intpoliza & "," & intaseguradora & ",'"
            sql = sql & strpoliza & "','"
            sql = sql & strfechaI & "','" & strfechaf & "','" & strmoneda & "',"
            sql = sql & dbcobertura & "," & dbprimavida & "," & intplazo & ",'"
            sql = sql & strbenef & "'," & dbprimanetavida & ", " & dbderpolvida & "," & intversion & ")"
            CConecta.EjecutaSQL(StrTransaccion, sql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Public Function FechaFinalFracciones(ByVal strfecha As String, ByVal intplazo As Integer) As DataTable
        Try
            StrSql = "" 'set dateformat ymd"
            StrSql = StrSql & " select fecha = dateadd(month, " & intplazo & ", '" & strfecha & "')"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            If ex.Message.ToString.ToUpper.Contains("DATETIME") Then
                StrSql = "set dateformat ymd"
                StrSql = StrSql & " select fecha = convert(varchar(30), dateadd(month, " & intplazo & ", '" & strfecha & "'),101)"
                Return CConecta.AbrirConsulta(StrSql, True)
            Else
                Throw ex
            End If
        End Try

    End Function

    Public Function carga_tipocarga(ByVal Idprograma As Integer, ByVal strmodelo As String) As DataTable
        Try
            StrSql = "SELECT id_tipocarga = 0, descripcion_tipocarga  = '-- Seleccione -- '"
            StrSql = StrSql & " UNION"
            StrSql = StrSql & " SELECT v.id_tipocarga, v.descripcion_tipocarga "
            StrSql = StrSql & " FROM VW_CARGATIPOCARGA v, abc_programa_aseguradora a"
            StrSql = StrSql & " where a.id_aseguradora = v.id_aseguradora"
            StrSql = StrSql & " and a.id_programa= " & Idprograma & ""
            StrSql = StrSql & " and v.MODELO ='" & strmodelo & "'"
            StrSql = StrSql & " group by v.id_tipocarga, v.descripcion_tipocarga "
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DatosAseguradora(ByVal IdAseguradora As Integer) As DataTable
        Try
            StrSql = "select descripcion_aseguradora, url_imagenpeque "
            StrSql = StrSql & " from abc_aseguradora"
            StrSql = StrSql & " where id_aseguradora = " & IdAseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidaExclusion(ByVal IdPrograma As Integer, ByVal strmodelo As String, ByVal intanio As Integer, ByVal strestatus As String) As DataTable
        Try
            StrSql = "select e.id_aseguradora "
            StrSql = StrSql & " from ABC_EXCLUSION e, abc_programa_aseguradora a"
            StrSql = StrSql & " where a.id_programa =" & IdPrograma & ""
            StrSql = StrSql & " and e.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and e.modelo_vehiculo = '" & strmodelo & "'"
            StrSql = StrSql & " and e.anio_vehiculo = " & intanio & ""
            StrSql = StrSql & " and e.estatus_vehiculo = '" & strestatus & "'"
            StrSql = StrSql & " group by e.id_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 12/12/2013 se agrega funciones de recotizacion
    Public Function recarga_marca(ByVal intmarca As Integer, ByVal intprograma As Integer, ByVal strestatus As String, ByVal IdSubMarca As Integer) As DataTable

        Try
            StrSql = " SELECT id_marca = a.id_submarca  , marca = m.Marca "
            StrSql = StrSql & " FROM ABC_MARCA_agrupador a, vehiculo v, ABC_MARCA M, ABC_PROGRAMA_ASEGURADORA p "
            StrSql = StrSql & " where a.id_aseguradora = v.id_aseguradora"
            StrSql = StrSql & " and a.id_marca = v.id_marca"
            StrSql = StrSql & " and a.id_submarca = v.id_submarca"
            StrSql = StrSql & " and v.Id_submarca = m.Id_Marca "
            StrSql = StrSql & " AND v.id_aseguradora = p.id_aseguradora "
            StrSql = StrSql & " and a.id_marca = " & intmarca & ""
            StrSql = StrSql & " AND p.id_programa =" & intprograma & ""
            StrSql = StrSql & " and v.estatus_vehiculo ='" & strestatus & "'"
            StrSql = StrSql & " and v.id_submarca = " & IdSubMarca & ""
            StrSql = StrSql & " and a.estatus_agrupador ='1' "
            StrSql = StrSql & " AND p.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            If strestatus = "N" Then
                StrSql = StrSql & " AND m.Estatus_Marca = '1' "
            End If
            StrSql = StrSql & " group by a.id_submarca , m.Marca "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Recarga_modelo(ByVal intmarca As Integer, ByVal Strestatusauto As String, ByVal intprograma As Integer, ByVal intmarcabase As Integer, ByVal StrModelo As String) As DataTable

        Try
            StrSql = " SELECT modelo_vehiculo = v.Modelo_vehiculo"
            StrSql = StrSql & " FROM vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " AND v.Id_Marca = " & intmarcabase & ""
            StrSql = StrSql & " AND v.Id_subMarca = " & intmarca & ""
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & Strestatusauto & "'"
            StrSql = StrSql & " AND v.modelo_vehiculo = '" & StrModelo & "'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " AND v.id_aonh in(select Id_AonH"
            StrSql = StrSql & " from homologacion"
            StrSql = StrSql & " where Estatus_Homologado=1)"
            StrSql = StrSql & " group by v.modelo_vehiculo"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Recarga_anio(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAnio As Integer) As DataTable
        Try
            StrSql = " SELECT anio_vehiculo = v.anio_vehiculo"
            StrSql = StrSql & " FROM vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""
            StrSql = StrSql & " AND v.Modelo_vehiculo ='" & strmodelo & "'"
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & StrEstatusAuto & "'"
            StrSql = StrSql & " AND v.anio_vehiculo =" & IntAnio & ""
            StrSql = StrSql & " AND v.status_vehiculo= '1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " group by v.anio_vehiculo"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Recarga_descripcion(ByVal intmarca As Integer, ByVal strmodelo As String, ByVal stranio As String, ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAonh As Integer) As DataTable

        Try
            StrSql = " SELECT id_vehiculo = h.Id_AonH, descripcion_vehiculo = h.Descripcion_Homologado "
            StrSql = StrSql & " FROM HOMOLOGACION h, ABC_PROGRAMA_ASEGURADORA a , vehiculo v"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora "
            StrSql = StrSql & " AND v.id_aonh = h.id_aonh"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            If Not (intprograma = 9 And StrEstatusAuto = "U") Then
                StrSql = StrSql & " AND v.id_marca= h.id_marca"
            End If
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""
            StrSql = StrSql & " AND v.modelo_vehiculo ='" & strmodelo & "'"
            StrSql = StrSql & " AND v.anio_vehiculo = '" & stranio & "'"
            StrSql = StrSql & " AND v.estatus_vehiculo ='" & StrEstatusAuto & "'"
            StrSql = StrSql & " AND v.id_aonh =" & IntAonh & ""
            StrSql = StrSql & " AND h.Estatus_Homologado = '1'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " group by Descripcion_Homologado, h.Id_AonH"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Recarga_Catalogo(ByVal StrEstatusAuto As String, ByVal intprograma As Integer, ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer, ByVal IntAonh As Integer) As DataTable

        Try
            StrSql = "select catalogo_vehiculo = v.catalogo_vehiculo "
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a"
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " and v.estatus_vehiculo ='" & StrEstatusAuto & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " AND v.status_vehiculo ='1'"
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & " "
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & " "
            StrSql = StrSql & " AND v.Id_AonH =" & IntAonh & ""
            StrSql = StrSql & " group by v.catalogo_vehiculo"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function recargaDatos12meses(ByVal IdCotizacionUnica As Int16, ByVal bandTipo As String) As DataTable
        Try
            If bandTipo = "C" Then
                StrSql = "EXEC RECARGA12MESESCOTIZACIONCONTRATADO " & IdCotizacionUnica & ""
            Else
                StrSql = "EXEC RECARGA12MESESCOTIZACION " & IdCotizacionUnica & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ReCargaCotizaPanelAseguradora(ByVal IdCotizacionUnica As Integer) As DataTable
        Try
            StrSql = "select a.Id_Aseguradora, a.descripcion_aseguradora"
            StrSql = StrSql & " from COTIZACION_PANEL cp, COTIZACION_UNICA cu, abc_aseguradora a "
            StrSql = StrSql & " where cp.Id_CotizacionPanel = cu.Id_CotizacionPanel"
            StrSql = StrSql & " and cp.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and cu.Id_CotizacionUnica = " & IdCotizacionUnica & ""
            StrSql = StrSql & " group by a.Id_Aseguradora, a.descripcion_aseguradora"
            StrSql = StrSql & " order by a.Id_Aseguradora"
            'HM 30062014
            'StrSql = StrSql & " order by a.descripcion_aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ReCargaCotizaPanel(ByVal IdCotizacionUnica As Integer, ByVal IdAseguradora As Integer) As DataTable
        Try
            StrSql = "select a.Id_Aseguradora, a.descripcion_aseguradora, a.url_imagenpeque, Id_TipoRecargo,  "
            StrSql = StrSql & " PT = isnull(PrimaTotal,0), PTC = isnull(PrimaConsecutiva,0), subramo, IdPaquete, PTC2 = isnull(PrimaConsecutiva2,0)"
            StrSql = StrSql & " from COTIZACION_PANEL cp, COTIZACION_UNICA cu, abc_aseguradora a "
            StrSql = StrSql & " where cp.Id_CotizacionPanel = cu.Id_CotizacionPanel"
            StrSql = StrSql & " and cp.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and cu.Id_CotizacionUnica = " & IdCotizacionUnica & ""
            StrSql = StrSql & " and a.Id_Aseguradora = " & IdAseguradora & ""
            StrSql = StrSql & " order by IdPaquete, Id_TipoRecargo asc"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Recarga12MesesAseg(ByVal IdCotizacionUnica As Integer) As DataTable
        Try
            StrSql = "select cp.*"
            StrSql = StrSql & " from COTIZACION_PANEL_12meses cp, COTIZACION_UNICA cu"
            StrSql = StrSql & " where cp.Id_Cotizacion12meses = cu.Id_Cotizacion12meses"
            StrSql = StrSql & " and cu.Id_CotizacionUnica = " & IdCotizacionUnica & ""
            StrSql = StrSql & " and cp.banderacompra = 1"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'HM 13/02/2014 Valida inf de cotizacion unica
    Public Function ValidandoInfInicial(ByVal IdCotizacionUnica As Integer) As DataTable
        Try
            StrSql = "select grupointegrante, noserie"
            StrSql = StrSql & " from COTIZACION_UNICA "
            StrSql = StrSql & " where id_cotizacionunica = " & IdCotizacionUnica & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtiene_datos_administrador(ByVal intPrograma As Integer) As DataTable
        Try
            StrSql = "select distinct u.Nombre, u.email, u.telefono from usuario u, usuario_bid ub"
            StrSql = StrSql & " where u.id_usuario = ub.id_usuario"
            StrSql = StrSql & " and u.estatus_usuario = '1'"
            StrSql = StrSql & " and u.bandera_administrador = '1'"
            StrSql = StrSql & " and ub.id_programa = " & intPrograma & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

