Imports System.Data.SqlClient
Public Class S_CdPrincipal
    Private StrSql As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New S_CdGlobal

    Public Sub New()
    End Sub

    Public Function valida_acceso(ByVal strlogin As String) As DataTable
        Try
            StrSql = "SELECT * FROM USUARIO" & vbCrLf '------------------
            StrSql = StrSql & " WHERE username = '" & strlogin & "'" & vbCrLf
            ' StrSql = StrSql & " AND contrasena = '" & strpass & "'"
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Login_acceso(ByVal strlogin As String) As DataTable
        Try
            StrSql = " SELECT distinct * FROM SOLICITUD_USUARIO " & vbCrLf
            StrSql = StrSql & "WHERE username = '" & strlogin & "'" & vbCrLf
            'StrSql = StrSql & "AND Contrasena = '" & strcontrasena & "'"
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function autentificaUsuario(ByVal user As String, ByVal password As String) As DataTable
        Dim ds As DataSet = New DataSet
        Dim cadena As String = "SELECT Id_Usuario, Nombre, Login, Email, Empresa, Consulta, Cancela, VistoBueno, " & _
        "Administracion, Transferencia, USUARIO.Id_Perfil, ABC_PERFIL.Descripcion  FROM USUARIO " & _
        "INNER JOIN ABC_PERFIL ON USUARIO.Id_Perfil = ABC_PERFIL.Id_Perfil " & _
        "Where Login = '" & user & "' " & _
        "AND Contrasena = '" & password & "' " & _
        "and USUARIO.Estatus='1'"
        'Dim parametros(1) As SqlParameter
        Return CConecta.AbrirConsulta(StrSql, True, True)

    End Function

    Public Function Agencias(ByVal IdUsuario As Integer) As DataTable
        Try
            StrSql = "SELECT Bid_Aon='0', Empresa='-- Seleccione--' UNION" & vbCrLf
            StrSql = StrSql & "SELECT Bid_Aon, Empresa FROM VW_SOLICITUD_USUARIO_BID" & vbCrLf
            StrSql = StrSql & "WHERE Id_Usuario = " & IdUsuario
            ' StrSql = StrSql & "WHERE ID_USUARIOSOLICITUD = " & IdUsuario
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function TodasAgencias() As DataTable
        Try
            StrSql = "SELECT Bid_Aon='0', Empresa='-- Seleccione--' UNION" & vbCrLf
            StrSql = StrSql & "SELECT Bid_Aon, Empresa FROM BID" & vbCrLf
            StrSql = StrSql & "WHERE Estatus= 1" & vbCrLf
            'StrSql = StrSql & "SELECT Bid_Aon, Empresa FROM E_BID" & vbCrLf
            'StrSql = StrSql & "WHERE Estatus_Bid = 1" & vbCrLf
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Guardar_Archivos(ByVal IdArchivoSolicitud As Integer, ByVal NombreArchivo As String, _
    ByVal RutaArchivo As String, ByVal IdUsuario As Integer, ByVal IdSolicitud As Integer)
        Dim IdArchivo As Long
        Dim ds As New DataSet
        Dim bandera As Boolean = False
        StrTransaccion = "IntArchivo"
        Try

            If IdArchivoSolicitud = 0 Then
                StrSql = "select Id_ArchivoSolicitud from solicitud_archivo" & vbCrLf
                StrSql = StrSql & "where Id_Solicitud = " & IdSolicitud
                Dim dt As DataTable = CConecta.AbrirConsulta(StrSql, True, False)
                If dt.Rows.Count = 0 Then
                    StrSql = "select valor = isnull(max(Id_ArchivoSolicitud),0) + 1 from solicitud_archivo" & vbCrLf
                    ds = CConecta.AbrirConsulta(StrSql, , False)
                    IdArchivoSolicitud = ds.Tables(0).Rows(0)(0)
                Else
                    IdArchivoSolicitud = dt.Rows(0).Item("Id_ArchivoSolicitud")
                End If

                StrSql = "select valor = isnull(max(id_archivo),0) + 1 from solicitud_archivo" & vbCrLf
                StrSql = StrSql & "where Id_Solicitud = " & IdSolicitud
                ds = CConecta.AbrirConsulta(StrSql, , False)
                IdArchivo = ds.Tables(0).Rows(0)(0)

                bandera = True
            Else
                StrSql = "select valor = isnull(max(id_archivo),0) + 1 from solicitud_archivo" & vbCrLf
                StrSql = StrSql & "where Id_ArchivoSolicitud = " & IdArchivoSolicitud
                ds = CConecta.AbrirConsulta(StrSql, , False)
                IdArchivo = ds.Tables(0).Rows(0)(0)
            End If

            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "insert into solicitud_archivo (Id_ArchivoSolicitud, Id_Archivo, Nombre_Archivo, Url_Archivo, Id_Solicitud, Fecha_Archivo, Id_UsuarioSolicitud)"
            StrSql = StrSql & " values(" & IdArchivoSolicitud & ", '" & IdArchivo & "','"
            If bandera = True Then
                StrSql = StrSql & NombreArchivo & "','" & RutaArchivo & "'," & IdSolicitud & ",getdate()," & IdUsuario & ")"
            Else
                StrSql = StrSql & NombreArchivo & "','" & RutaArchivo & "',NULL,getdate()," & IdUsuario & ")"
            End If


            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return IdArchivo

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Actualiza_Archivo(ByVal IdArchivoSolicitud As Integer, ByVal IdSolicitud As Integer)
        StrTransaccion = "IntArchivo"
        Try

            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "update solicitud_archivo set Id_Solicitud = " & IdSolicitud & ""
            StrSql = StrSql & " where Id_ArchivoSolicitud = " & IdArchivoSolicitud & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Eliminar_Archivos(ByVal IdSolicitud As Integer, ByVal IdArchivo As Integer)
        StrTransaccion = "IntArchivo"
        Try
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "delete solicitud_archivo where Id_ArchivoSolicitud = " & IdSolicitud & "" & vbCrLf
            StrSql = StrSql & " and Id_Archivo = " & IdArchivo & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Obtiene_IdArchivoSolicitud(ByVal IdSolicitud As Integer) As DataTable
        Try
            If IdSolicitud = 0 Then
                StrSql = "SELECT isnull(max(Id_ArchivoSolicitud),'0') + 1 Id_ArchivoSolicitud FROM solicitud_archivo"
            Else
                StrSql = "SELECT * FROM solicitud_archivo where Id_Solicitud = " & IdSolicitud
            End If

            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Obtiene_Archivos(ByVal IdArchivoSolicitud As Integer, ByVal IdSolicitud As Integer) As DataTable
        Try
            If IdArchivoSolicitud = 0 Then
                StrSql = "SELECT * FROM solicitud_archivo where id_Solicitud = " & IdSolicitud
            Else
                StrSql = "SELECT * FROM solicitud_archivo where id_ArchivoSolicitud = " & IdArchivoSolicitud
            End If

            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Eliminar_TablaTemporal(ByVal IdSesion As String)
        StrTransaccion = "IntArchivo"
        Try
            StrSql = "delete solicitud_reporte_temporal where IdSesion = '" & IdSesion & "'"
            CConecta.EjecutaSQLNoTrans(StrSql)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inserta_InformacionTemporal(ByVal datSolicitud As Integer, ByVal datTipoSolicitud As String, _
    ByVal datPoliza As String, ByVal datSerie As String, ByVal datContrato As String, ByVal datCliente As String, _
    ByVal datAseguradora As String, ByVal datFECSOLICITADO As String, ByVal datStatus As String, _
    ByVal datFechaStatus As String, ByVal datUSUARIOSOLICITANTE As String, ByVal datNSSTATUS As String, _
    ByVal datFECCERRADO As String, ByVal datNSPENDIENTE As String, ByVal datNSTRAMITE As String, _
    ByVal datNSCERRADO As String, ByVal datFECPENDIENTE As String, ByVal datFECTRAMITE As String, _
    ByVal datFECCANCELADO As String, ByVal datDETALLE As String, ByVal datSLA As String, _
    ByVal datConformeSLA As String, ByVal datColor As String, ByVal IdSesion As String, _
    ByVal datDistribuidor As String, ByVal datNuevaPoliza As String, ByVal datBandera As String)
        StrTransaccion = "IntArchivo"
        Try

            StrSql = "insert into solicitud_reporte_temporal values(" & datSolicitud & ",'" & datTipoSolicitud & "'," & vbCrLf
            StrSql = StrSql & "'" & datPoliza & "','" & datSerie & "','" & datContrato & "','" & datCliente & "'," & vbCrLf
            StrSql = StrSql & "'" & datAseguradora & "','" & datFECSOLICITADO & "','" & datStatus & "'," & vbCrLf
            StrSql = StrSql & "'" & datFechaStatus & "','" & datUSUARIOSOLICITANTE & "','" & datNSSTATUS & "'," & vbCrLf
            StrSql = StrSql & "'" & datFECCERRADO & "','" & datNSPENDIENTE & "','" & datNSTRAMITE & "'," & vbCrLf
            StrSql = StrSql & "'" & datNSCERRADO & "','" & datFECPENDIENTE & "','" & datFECTRAMITE & "'," & vbCrLf
            StrSql = StrSql & "'" & datFECCANCELADO & "','" & datDETALLE & "','" & datSLA & "'," & vbCrLf
            StrSql = StrSql & "'" & datConformeSLA & "','" & datColor & "','" & IdSesion & "','" & datDistribuidor & "'," & vbCrLf
            StrSql = StrSql & "'" & datNuevaPoliza & "','" & datBandera & "','0')"
            CConecta.EjecutaSQLNoTrans(StrSql)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Obtiene_IdReporte() As DataTable
        Try
            StrSql = "SELECT isnull(max(Id_Reporte),'0') + 1 Id_Reporte FROM solicitud_reportes"
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Verifica_Reporte(ByVal IdReporte As Integer) As DataTable
        Try
            StrSql = "SELECT * FROM solicitud_reportes" & vbCrLf
            StrSql = StrSql & "where Id_Reporte = " & IdReporte
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Obtiene_UsuarioBid(ByVal IdUsuario As Integer) As DataTable
        Try
            StrSql = "SELECT * FROM VW_SOLICITUD_USUARIO_BID" & vbCrLf
            StrSql = StrSql & " WHERE Id_Usuario = " & IdUsuario & ""
            'StrSql = StrSql & " WHERE Id_usuarioSolicitud = " & IdUsuario & ""
            Return CConecta.AbrirConsulta(StrSql, True, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Actualiza_Bandera(ByVal IdSolicitud As Integer, ByVal Bandera As String)
        StrTransaccion = "Bandera"
        Try

            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "update solicitud_reporte_temporal set Bandera = '" & Bandera & "'"
            StrSql = StrSql & " where IdSolicitud = " & IdSolicitud & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Actualiza_NuevaPoliza(ByVal IdSolicitud As Integer, ByVal NuevaPoliza As String)
        StrTransaccion = "NuevaPoliza"
        Try

            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "update solicitud_reporte_temporal set NuevaPoliza = '" & NuevaPoliza & "'"
            StrSql = StrSql & " where IdSolicitud = " & IdSolicitud & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function IniciaSesion(ByVal idUser As Integer) As Boolean
        Dim cadena As String
        Dim parametros() As SqlParameter
        'Dim trans As SqlTransaction

        Try
            cadena = "SP_InicioSesion"

            ReDim parametros(0)
            parametros(0) = New SqlParameter("@IdUser", SqlDbType.Int, 10)
            parametros(0).Value = idUser
            CConecta.EjecutaStored(cadena, parametros)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function TerminaSesion(ByVal idUser As Integer) As Boolean
        Dim cadena As String
        Dim parametros() As SqlParameter
        'Dim trans As SqlTransaction

        Try
            cadena = "SP_FinSesion"

            ReDim parametros(0)
            parametros(0) = New SqlParameter("@IdUser", SqlDbType.Int, 10)
            parametros(0).Value = idUser
            CConecta.EjecutaStored(cadena, parametros)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
