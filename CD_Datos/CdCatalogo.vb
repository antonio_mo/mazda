Imports CD_Datos.CdSql
Imports StoreProcedure
Public Class CdCatalogo
    Private StrSql As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New CdSql

    Public Sub New()
    End Sub

    Public Function menu_administrador_padre(IdMarca As Integer) As DataTable

        Try
            StrSql = "select id_menu, ancestro, nombre_menu,"
            StrSql = StrSql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            StrSql = StrSql & " status"
            StrSql = StrSql & " from menu_administrador"
            StrSql = StrSql & " where ancestro = 0"
            If IdMarca = 5 Then
                StrSql = StrSql & " and id_menu in (1,3,5,7)"
            End If
            StrSql = StrSql & " and status ='1'"
            StrSql = StrSql & " order by orden_menu"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function menu_administrador_hijo(ByVal ancestro As Integer) As DataTable

        Try
            strsql = "select id_menu, ancestro, nombre_menu,"
            strsql = strsql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            strsql = strsql & " status"
            strsql = strsql & " from menu_administrador"
            strsql = strsql & " where ancestro = 0 union"
            strsql = strsql & " select id_menu, ancestro, nombre_menu,"
            strsql = strsql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            strsql = strsql & " status"
            strsql = strsql & " from menu_administrador"
            strsql = strsql & " where ancestro = " & ancestro & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'MENU administrador subhijo
    Public Function menu_administrador_subhijo(ByVal ancestro As Integer, ByVal hijo As Integer, _
    ByVal subhijo As Integer) As DataTable

        Try
            strsql = "select id_menu, ancestro, nombre_menu,"
            strsql = strsql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            strsql = strsql & " status"
            strsql = strsql & " from menu_administrador"
            strsql = strsql & " where ancestro = 0 union"
            strsql = strsql & " select id_menu, ancestro, nombre_menu,"
            strsql = strsql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            strsql = strsql & " status"
            strsql = strsql & " from menu_administrador"
            strsql = strsql & " where ancestro = " & hijo & " union"
            strsql = strsql & " select id_menu, ancestro, nombre_menu,"
            strsql = strsql & " case when url_menu is null then '' else isnull(url_menu,'') end as url,"
            strsql = strsql & " status"
            strsql = strsql & " from menu_administrador"
            strsql = strsql & " where ancestro = " & subhijo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cbo_corporativo_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
       
        Try
            strsql = " select id_bid=0, empresa='-- Seleccione -- ' union"
            strsql = strsql & " select id_bid, empresa"
            strsql = strsql & " from bid"
            strsql = strsql & " where estatus = 1"
            'If intnivel <> 0 Then
            strsql = strsql & " and id_bid not in ("
            strsql = strsql & " select distinct id_bid"
            strsql = strsql & " from usuario_bid"
            strsql = strsql & " where id_usuario =" & intusuario & ")"
            'End If
            strsql = strsql & " order by empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cbo_corporativo_usuario_bid(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable
        
        Try
            strsql = " select id_bid=0, empresa='-- Seleccione -- ' union"
            'strsql = strsql & " select id_bid, empresa"
            strsql = strsql & " select id_bid , empresa = bid_aon +' - '+ empresa"
            strsql = strsql & " from bid"
            strsql = strsql & " where estatus = 1"
            'If intnivel <> 0 Then
            strsql = strsql & " and id_bid not in ("
            strsql = strsql & " select distinct id_bid"
            strsql = strsql & " from usuario_bid"
            strsql = strsql & " where id_usuario =" & intusuario & ")"
            'End If
            strsql = strsql & " order by empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_usuario(ByVal intnivel As Integer, ByVal strlogin As String, _
    ByVal strcontrasena As String, ByVal strestatus As String, ByVal stremail As String, _
    ByVal intbid As Integer, ByVal strregistro As Integer, ByVal strnombre As String) As Integer
        'ejecutivo = 0 no es ejecutivo de siniestro
        Dim valor As Integer
        Dim ds As New DataSet
        Dim ArrParametro(0) As String

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor = isnull(max(id_usuario),0)+1 from usuario"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsUsu table(id int); "
            StrSql = StrSql & " insert into usuario (id_usuario, id_nivel, login, contrasena, " & vbCrLf
            StrSql = StrSql & " fecha_registro, estatus_usuario, bandera_registro, " & vbCrLf
            StrSql = StrSql & " email, nombre)" & vbCrLf
            'StrSql = StrSql & " values(" & valor & ", " & intnivel & ",'" & strlogin & "','"
            StrSql = StrSql & " OUTPUT inserted.id_usuario into @idInsUsu select (Select isnull(max(id_usuario), 0 ) + 1 from usuario With(NoLock)), " & intnivel & ",'" & strlogin & "','" & vbCrLf
            StrSql = StrSql & strcontrasena & "', getdate(), '" & strestatus & "','" & vbCrLf
            StrSql = StrSql & strregistro & "','" & stremail & "','" & strnombre & "'; "
            StrSql += "select * from @idInsUsu; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            'CConecta.EjecutaSQL(StrTransaccion, StrSql)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            If intnivel <> 1 Then
                StrSql = "insert into usuario_bid (id_usuario, id_bid)"
                StrSql = StrSql & " values(" & valor & ", " & intbid & ")"
            Else
                'ArrParametro(0) = valor
                StrSql = "EXEC CARGA_BID_USUARIO " & valor

                'If cStore.ExecuteSpNonQuery("CARGA_BID_USUARIO ", ArrParametro, cnn, trans) = False Then
                '    trans.Rollback()
                'End If
            End If

            StrTransaccion = "InsertaUsuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'valida usuario
    Public Function valida_Usuario(ByVal strlogin As String, ByVal strpass As String) As DataTable

        Try
            StrSql = " select usuario = isnull(id_usuario,0)"
            StrSql = StrSql & " from usuario"
            StrSql = StrSql & " where login='" & strlogin & "'"
            StrSql = StrSql & " and contrasena='" & strpass & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_login(ByVal strlogin As String) As DataTable

        Try
            StrSql = " select usuario = isnull(id_usuario,0)"
            StrSql = StrSql & " from usuario"
            StrSql = StrSql & " where login='" & strlogin & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_Usuario_corporativo(ByVal intusuario As Integer, ByVal intcorporativo As Integer) As DataTable

        Try
            StrSql = " Select usuario = u.id_usuario"
            StrSql = StrSql & " from usuario u, usuario_bid b"
            StrSql = StrSql & " where u.id_usuario = b.id_usuario"
            StrSql = StrSql & " and u.id_usuario=" & intusuario & ""
            StrSql = StrSql & " and b.id_bid=" & intcorporativo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtiene_idusuario() As DataTable

        Try
            StrSql = " select isnull(max(id_usuario),0) as Id_Usuario from Usuario"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub modifica_usuario(ByVal intusuario As Integer, _
     ByVal intnivel As Integer, ByVal strlogin As String, _
     ByVal strestatus As String, ByVal stremail As String, _
     ByVal strregistro As Integer, ByVal strnombre As String, ByVal strIdEmpleado As String, ByVal iTypeAccount As Integer)

        Try
            StrTransaccion = "modifica_usuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update usuario set id_nivel=" & intnivel & ", "
            StrSql = StrSql & " login='" & strlogin & "', "
            StrSql = StrSql & " estatus_usuario='" & strestatus & "', "
            StrSql = StrSql & " email='" & stremail & "',"
            StrSql = StrSql & " bandera_registro='" & strregistro & "', "
            StrSql = StrSql & " nombre= '" & strnombre & "', "
            StrSql = StrSql & " id_empleado= '" & strIdEmpleado & "',"
            StrSql = StrSql & " TypeAccount = " & iTypeAccount.ToString() & ""
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'USUARIO
    Public Function listado_usuario(ByVal intbid As Integer, ByVal intbandera As Integer) As DataTable

        Try
            StrSql = " select distinct u.id_usuario, nombre_completo = b.empresa,"
            StrSql = StrSql & " email, estatus = case estatus_usuario when '1' then 'Activo'"
            StrSql = StrSql & " when '0' then 'Inactivo' End, nivel=n.descripcion_nivel, n.niveles"
            StrSql = StrSql & " from usuario u, bid b, ABC_NIVEL n, usuario_bid ub"
            StrSql = StrSql & " where u.id_usuario = ub.id_usuario"
            StrSql = StrSql & " and ub.id_bid = b.id_bid"
            StrSql = StrSql & " and n.Id_Nivel = U.Id_nivel"
            If intbandera = 1 Then
                StrSql = StrSql & " and ub.id_bid = " & intbid & ""
            End If
            StrSql = StrSql & " ORDER BY empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function buscar_usuario_grd(ByVal strUsuario As String, ByVal intbid As Integer, ByVal intnivel As Integer) As DataTable

        Try
            If intnivel = 0 Then
                StrSql = " select distinct u.id_usuario, nombre_completo = u.nombre,"
                StrSql = StrSql & " email, estatus = case estatus_usuario when '1' then 'Activo'"
                StrSql = StrSql & " when '0' then 'Inactivo' End, nivel=n.descripcion_nivel, n.niveles"
                StrSql = StrSql & " from usuario u, bid b, ABC_NIVEL n, usuario_bid ub"
                StrSql = StrSql & " where u.id_usuario = ub.id_usuario"
                StrSql = StrSql & " and ub.id_bid = b.id_bid"
                StrSql = StrSql & " and n.Id_Nivel = U.Id_nivel"
                StrSql = StrSql & " and u.nombre like '%" & strUsuario & "%'"
                StrSql = StrSql & " ORDER BY u.nombre"
                'Else
                '    StrSql = " select distinct u.id_usuario, nombre_completo = u.nombre,"
                '    StrSql = StrSql & " email, estatus = case estatus_usuario when '1' then 'Activo'"
                '    StrSql = StrSql & " when '0' then 'Inactivo' End, nivel=n.descripcion_nivel, n.niveles"
                '    StrSql = StrSql & " from usuario u, bid b, ABC_NIVEL n, usuario_bid ub"
                '    StrSql = StrSql & " where u.id_usuario = ub.id_usuario"
                '    StrSql = StrSql & " and ub.id_bid = b.id_bid"
                '    StrSql = StrSql & " and n.Id_Nivel = U.Id_nivel"
                '    StrSql = StrSql & " and ub.id_bid = " & intbid & ""
                '    StrSql = StrSql & " and u.nombre like '%" & strUsuario & "%'"
                '    StrSql = StrSql & " ORDER BY u.nombre"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_grd_bid_user(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = "select distinct u.id_usuario, b.id_bid, b.bid_ford, b.bid_aon, b.empresa"
            StrSql = StrSql & " from bid b, usuario_bid u"
            StrSql = StrSql & " where b.id_bid = u.id_bid"
            StrSql = StrSql & " and u.id_usuario =" & intusuario & ""
            StrSql = StrSql & " order by empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_niveles() As DataTable

        Try
            StrSql = " select id_nivel=0, descripcion_nivel='-- Seleccione --' union"
            StrSql = StrSql & " select id_nivel, descripcion_nivel"
            StrSql = StrSql & " from abc_nivel"
            StrSql = StrSql & " where estatus_nivel='1'"
            StrSql = StrSql & " order by descripcion_nivel"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_datos_usuario(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = " select * from usuario "
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_corporativo_usuario(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = " select distinct b.empresa, b.id_bid"
            StrSql = StrSql & " from usuario_bid ub, bid b"
            StrSql = StrSql & " where ub.id_bid = b.id_bid"
            StrSql = StrSql & " and ub.id_usuario =" & intusuario & ""
            StrSql = StrSql & " order by b.empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cuenta_corporativo_usuario(ByVal intusuario As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select * from usuario_bid"
            StrSql = StrSql & " where  id_bid = " & intbid & ""
            StrSql = StrSql & " and id_usuario =" & intusuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub inserta_usuario_corporativo(ByVal intusuario As Integer, ByVal intbid As Integer)

        Try
            StrTransaccion = "InsertaUsuarioCorp"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "insert into usuario_bid (id_usuario, id_bid)"
            StrSql = StrSql & " values(" & intusuario & ", " & intbid & ")"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function carga_grd_bid() As DataTable

        Try
            StrSql = " select b.id_bid, b.bid_ford, b.bid_aon, "
            StrSql = StrSql & " b.empresa, b.estatus, m.marca,"
            StrSql = StrSql & " est = case when b.estatus = '1' then 'Activo' "
            StrSql = StrSql & " else 'Inactivo' end, m.id_marca"
            StrSql = StrSql & " from bid b, abc_marca m"
            StrSql = StrSql & " where b.id_marca = m.id_marca"
            StrSql = StrSql & " and m.estatus_marca= '1'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_marca() As DataTable

        Try
            StrSql = " select id_marca=0, marca='-- Seleccion --' union"
            StrSql = StrSql & " select id_marca, marca"
            StrSql = StrSql & " from abc_marca"
            StrSql = StrSql & " where estatus_marca ='1'"
            StrSql = StrSql & " order by marca"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_datos_distribuidor(IdBid As Integer) As DataTable

        Try
            StrSql = " select * from bid"
            StrSql = StrSql & " where id_bid = " & IdBid & ""

            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_bid(ByVal intbidC As Integer, ByVal strbidf As String, ByVal strbida As String, _
    ByVal strempresa As String, ByVal strestatus As String, ByVal intmarca As Integer) As Integer
        Dim valor As Integer
        Dim ds As New DataSet

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor =isnull(max(id_bid),0) +1 from bid"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "inserta_bid"

            StrSql = "set dateformat dmy; "
            StrSql += " declare @idInsBid table(id int); "
            StrSql = StrSql & " insert into bid (id_bid, Bid_Conauto, bid_ford, bid_aon, empresa, empresa_original, estatus, id_marca)"
            StrSql = StrSql & " OUTPUT inserted.id_bid into @idInsBid select (Select isnull(max(id_bid), 0 ) + 1 from bid With(NoLock))," & intbidC & ",'" & strbidf & "','"
            StrSql = StrSql & strbida & "','" & strempresa & "','" & strempresa & "','" & strestatus & "',"
            StrSql = StrSql & intmarca & "; "
            StrSql += "select * from @idInsBid; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_bid(ByVal intbid As Integer, ByVal intbidC As Integer, ByVal strbidf As String, ByVal strbida As String, _
    ByVal strempresa As String, ByVal strestatus As String, ByVal intmarca As Integer)

        Try
            StrTransaccion = "actualiza_bid"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update bid set bid_Conauto = " & intbidC & ", "
            StrSql = StrSql & " bid_ford= '" & strbidf & "', "
            StrSql = StrSql & " bid_aon= '" & strbida & "', "
            StrSql = StrSql & " empresa = '" & strempresa & "',"
            StrSql = StrSql & " empresa_original = '" & strempresa & "',"
            StrSql = StrSql & " estatus = '" & strestatus & "',"
            StrSql = StrSql & " id_marca = " & intmarca & ""
            StrSql = StrSql & " where id_bid = " & intbid & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function carga_tablas(ByVal intbitacora As Integer) As DataTable

        Try
            StrSql = " select id_tabla = 0, descripcion_tabla_txt='-- Seleccione --', orden = 0 union"
            StrSql = StrSql & " select id_tabla, descripcion_tabla_txt , orden"
            StrSql = StrSql & " from ABC_NOMBRE_TABLA"
            StrSql = StrSql & " where id_tabla not in ("
            StrSql = StrSql & " select distinct id_tabla "
            StrSql = StrSql & " from BITACORA_SCRIPT_TABLAS"
            StrSql = StrSql & " where id_bitacora = " & intbitacora & ")"
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function numero_bitacora_max() As DataTable

        Try
            StrSql = " select valor = isnull(max(id_bitacora),0) + 1  from BITACORA_SCRIPT_TABLAS"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub inserta_bitacora_tabla(ByVal intbitacora As Integer, ByVal inttabla As Integer, ByVal strurl As String)

        Try
            StrTransaccion = "inserta_bitacora_tabla"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " insert into BITACORA_SCRIPT_TABLAS (id_bitacora, id_tabla, "
            StrSql = StrSql & " url_bitacora, estatus_bitacora)"
            StrSql = StrSql & " values(" & intbitacora & ", " & inttabla & ",'"
            StrSql = StrSql & strurl & "','1')"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function carga_tb_bitacora_script(ByVal intbitacora As Integer) As DataTable

        Try
            StrSql = " select id_tabla, id_bitacora , descripcion_tabla_txt"
            StrSql = StrSql & " from VW_CONSULTA_SCRIPT_TABLA"
            StrSql = StrSql & " where id_bitacora = " & intbitacora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_contrasena(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = " select contrasena from usuario where id_usuario=" & intusuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Actualiza_contrasena(ByVal intusuario As Integer, ByVal strNuevoPass As String, Optional ByVal strBanderaRegistro As Boolean = False)

        Try
            StrTransaccion = "Actualiza_contrasena"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "Update usuario set contrasena='" & strNuevoPass & "'"
            If (strBanderaRegistro) Then
                StrSql = StrSql & ", bandera = 1"
            End If
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function cargar_estados() As DataTable

        Try
            StrSql = "select Id_Estado = 0, Nombre_Estado = '--Seleccionar--' UNION"
            StrSql = StrSql & " select distinct Id_Estado, Nombre_Estado from parametro_iva"
            StrSql = StrSql & " order by Id_Estado"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_grd_codigopostal(ByVal strCodigo As String) As DataTable

        Try
            StrSql = " select *, est = case when estatus_iva = '1' then 'Activo' "
            StrSql = StrSql & " else 'Inactivo' end"
            StrSql = StrSql & " from parametro_iva"
            StrSql = StrSql & " where Codigo_Postal = '" & strCodigo & "'"
            StrSql = StrSql & " and estatus_iva = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_codigopostal(ByVal intestado As Integer, ByVal strestado As String, _
    ByVal strcodigopostal As String, ByVal strprovincia As String, _
    ByVal strmunicipio As String, ByVal strcolonia As String, _
    ByVal strpctiva As String, ByVal strestatus As String) As Integer
        Dim valor As Integer
        Dim ds As New DataSet

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select valor =isnull(max(id_iva),0) +1 from parametro_iva"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "inserta_codigopostal"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsCP table(id int); "
            StrSql = StrSql & " insert into parametro_iva (id_iva, Id_Estado, Nombre_Estado, Codigo_postal," & vbCrLf
            StrSql = StrSql & " Nombre_Provincia, Municipio, Colonia, Pct_Iva, Estatus_Iva)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_iva into @idInsCP select (Select isnull(max(id_iva), 0 ) + 1 from parametro_iva With(NoLock)), " & intestado & ", '" & vbCrLf
            StrSql = StrSql & strestado & "', '" & strcodigopostal & "', '" & strprovincia & "', '" & vbCrLf
            StrSql = StrSql & strmunicipio & "', '" & strcolonia & "', '" & strpctiva & "', '" & vbCrLf
            StrSql = StrSql & strestatus & "'; "
            StrSql += "select * from @idInsCP; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            'CConecta.EjecutaSQL(StrTransaccion, StrSql)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function actualiza_codigopostal(ByVal intiva As Integer, _
    ByVal intestado As Integer, ByVal strestado As String, _
    ByVal strcodigopostal As String, ByVal strprovincia As String, _
    ByVal strmunicipio As String, ByVal strcolonia As String, _
    ByVal strpctiva As String, ByVal strestatus As String) As Integer

        Try
            StrTransaccion = "actualiza_codigopostal"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "update parametro_iva set Id_Estado = " & intestado & ", "
            StrSql = StrSql & " Nombre_Estado = '" & strestado & "', "
            StrSql = StrSql & " Codigo_postal = '" & strcodigopostal & "',"
            StrSql = StrSql & " Nombre_Provincia = '" & strprovincia & "',"
            StrSql = StrSql & " Municipio = '" & strmunicipio & "',"
            StrSql = StrSql & " Colonia = '" & strcolonia & "',"
            StrSql = StrSql & " Pct_Iva = '" & strpctiva & "',"
            StrSql = StrSql & " Estatus_Iva = '" & strestatus & "'"
            StrSql = StrSql & " where Id_Iva = " & intiva & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function


#Region "datos de los usuario"

    Public Function carga_grd_usuarios(ByVal intnivel As Integer, ByVal intbid As Integer, ByVal strnombre As String, ByVal intMarca As Integer) As DataTable

        Try
            StrSql = "select distinct u.id_usuario, u.nombre, n.descripcion_nivel,  estatus = case when u.estatus_usuario = '1' then 'Activo' else 'Inactivo' end"
            StrSql = StrSql & " from usuario u, abc_nivel n "
            StrSql = StrSql & " where u.id_nivel = n.id_nivel"
            'If intnivel > 0 Then
            If intMarca = 1 Then
                'StrSql = StrSql & " and u.id_usuario in (select id_usuario from usuario_bid where id_bid =" & intbid & " and Id_programa in (29,30,31))"
                StrSql = StrSql & "     "
            Else
                'StrSql = StrSql & " and u.id_usuario in (select id_usuario from usuario_bid where id_bid =" & intbid & " and Id_programa = 32)"
                StrSql = StrSql & " and u.id_usuario in (select id_usuario from usuario_bid where Id_programa = 32)"
            End If

            'End If
            StrSql = StrSql & " and u.nombre like '%" & strnombre & "%'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_grd_bid_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intMarca As Integer) As DataTable

        Try
            'StrSql = "select distinct id_bid, nombre = m.marca +' - '+ empresa "
            'StrSql = StrSql & " from bid b, abc_marca m"
            'StrSql = StrSql & " where b.id_marca = m.id_marca"
            'If intnivel > 0 Then
            '    StrSql = StrSql & " and b.id_bid in( select distinct id_bid from usuario_bid where id_usuario =" & intusuario & ")"
            'End If
            'StrSql = StrSql & " and b.estatus = '1'"
            StrSql = " select distinct id_bid, nombre = isnull(bid_aon,'') +' - '+ isnull(empresa,'') , empresa"
            StrSql = StrSql & " from bid"
            StrSql = StrSql & " where estatus = '1'"
            StrSql = StrSql & " and Id_Marca = " & intMarca & ""
            If intnivel > 0 Then
                StrSql = StrSql & " and id_bid in( select distinct id_bid from usuario_bid where id_usuario =" & intusuario & ")"
            End If
            StrSql = StrSql & " order by empresa"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_grd_programa(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intMarca As Integer) As DataTable

        Try
            StrSql = " select distinct a.id_programa , a.descripcion_programa"
            StrSql = StrSql & " from abc_programa a, usuario_bid b"
            StrSql = StrSql & " where estatus_programa ='1'"
            StrSql = StrSql & " and a.id_programa = b.id_programa"
            If intnivel > 0 Then
                StrSql = StrSql & " and b.id_usuario = " & intusuario & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_datos_usuario_niveles(ByVal intnivel As Integer) As DataTable

        Try
            StrSql = "select id_nivel = 0, descripcion_nivel ='-- Seleccione --'"
            StrSql = StrSql & " union"
            StrSql = StrSql & " select id_nivel, descripcion_nivel"
            StrSql = StrSql & " from abc_nivel"
            StrSql = StrSql & " where estatus_nivel ='1'"
            If intnivel > 0 Then
                StrSql = StrSql & " and not niveles in(0,3)"
            End If
            StrSql = StrSql & " order by descripcion_nivel"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_datos_login_nombre(ByVal strlogin As String, ByVal strnombre As String) As Integer
        Dim ds1 As New DataTable
        Dim ds2 As New DataTable
        Dim strsql As String
        Dim valor As Integer = 0
        Try
            strsql = "select * from usuario"
            strsql = strsql & " where login ='" & strlogin & "'"
            ds1 = CConecta.AbrirConsulta(strsql, True)

            strsql = "select * from usuario"
            strsql = strsql & " where nombre ='" & strnombre & "'"
            ds2 = CConecta.AbrirConsulta(strsql, True)

            'If ds1.Tables("Login").Rows.Count > 0 Then
            If ds1.Rows.Count > 0 Then
                valor = 1
            Else
                'If ds2.Tables("Nombre").Rows.Count > 0 Then
                If ds2.Rows.Count > 0 Then
                    valor = 2
                End If
            End If

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_contrasena(ByVal strpass As String) As Integer
        Dim ds As New DataTable
        Dim Rvalor As Integer = 0
        Try
            StrSql = "select valor = count(id_usuario)"
            StrSql = StrSql & " from usuario"
            StrSql = StrSql & " where contrasena ='" & strpass & "'"
            ds = CConecta.AbrirConsulta(StrSql, True)
            If ds.Rows.Count > 0 Then
                If ds.Rows(0)("valor") > 0 Then
                    Rvalor = 1
                End If
            End If
            Return Rvalor
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_datos_usuario(ByVal intnivel As Integer, ByVal strlogin As String, ByVal strencriptado As String, _
    ByVal strestatus As String, ByVal strbandera As String, ByVal stremail As String, ByVal strnombre As String, ByVal strIdEmpleado As String, ByVal iTypeAccount As Integer) As Integer
        Dim valor As Integer = 0
        Dim ds As New DataSet

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = "select valor = isnull(max(id_usuario),0)+1 from usuario"
            'ds = CConecta.AbrirConsulta(StrSql)
            'valor = ds.Tables(0).Rows(0)(0)

            StrTransaccion = "inserta_datos_usuario"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsDU table(id int); "
            StrSql = StrSql & " insert into usuario (id_usuario, id_nivel, login, contrasena, fecha_registro, " & vbCrLf
            StrSql = StrSql & " estatus_usuario, bandera_registro, email, nombre, bandera_css, id_empleado, TypeAccount)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.id_usuario into @idInsDU select (Select isnull(max(id_usuario), 0 ) + 1 from usuario With(NoLock)), " & intnivel & ",'" & strlogin & "', '" & strencriptado & "', getdate(),'"
            StrSql = StrSql & strestatus & "','" & strbandera & "','" & stremail & "','" & strnombre & "','1', '" & strIdEmpleado & "', " & iTypeAccount.ToString() & "; "
            StrSql += "select * from @idInsDU; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            'CConecta.EjecutaSQL(StrTransaccion, StrSql)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub inserta_bid_programa_usuario(ByVal intusuario As Integer, ByVal strprog As String, ByVal strbid As String)
        Dim i, j As Integer
        Dim arrProg() As String = strprog.Split(",")
        Dim arrbid() As String = strbid.Split(",")
        Dim strsql As String = ""
        Try

            StrTransaccion = "inserta_bid_programa_usuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            For i = 0 To arrProg.Length - 1
                For j = 0 To arrbid.Length - 1
                    'evi 18/12/2014
                    'strsql = "insert into usuario_bid (id_usuario, id_bid, id_programa)"
                    'strsql = strsql & " values(" & intusuario & "," & arrbid(j) & "," & arrProg(i) & ")"
                    strsql = " EXEC INSERTA_BID_USUARIO " & intusuario & "," & arrbid(j) & "," & arrProg(i) & ""
                    CConecta.EjecutaSQL(StrTransaccion, strsql)
                Next
            Next

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function carga_usuario_bid_detalle(ByVal intusuario As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select distinct u.id_bid, nombre = m.marca +' - '+ b.empresa "
            StrSql = StrSql & " from bid b, abc_marca m, usuario_bid u"
            StrSql = StrSql & " where b.id_marca = m.id_marca"
            StrSql = StrSql & " and u.id_bid = b.id_bid"
            StrSql = StrSql & " and b.estatus = '1'"
            StrSql = StrSql & " and m.estatus_marca ='1'"
            StrSql = StrSql & " and u.id_usuario = " & intusuario & ""
            StrSql = StrSql & " and u.id_programa = " & intprograma & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub elimina_programa_usuario(ByVal intusuario As Integer, ByVal intprograma As Integer)

        Try

            StrTransaccion = "elimina_programa_usuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "delete from usuario_bid"
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            StrSql = StrSql & " and id_programa = " & intprograma & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub elimina_bid_usuario(ByVal intusuario As Integer, ByVal intprograma As Integer, ByVal intbid As Integer)

        Try
            StrTransaccion = "elimina_bid_usuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "delete from usuario_bid"
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            StrSql = StrSql & " and id_programa = " & intprograma & ""
            StrSql = StrSql & " and id_bid =" & intbid & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Encriptar Masivo contraseņa"

    Public Function carga_usuarios() As DataTable

        Try
            StrSql = "select id_usuario, login, nombre, contrasena, email from usuario where id_usuario >=165"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Actualiza_masivo_excriptasion(ByVal strcadena As String)
        Dim arr() As String
        Dim user() As String
        Dim i As Integer = 0
        Try
            StrTransaccion = "Actualiza_masivo_excriptasion"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            arr = strcadena.Split("|")
            For i = 0 To arr.Length - 1
                user = arr(i).Split("*")
                StrSql = "update usuario set contrasena ='" & user(1) & "'"
                StrSql = StrSql & " where id_usuario = " & user(0) & ""
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
            Next

            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

    Public Function carga_aseguradoras() As DataTable

        Try
            StrSql = "select id_aseguradora, nombre_completo from abc_aseguradora"
            StrSql = StrSql & " where estatus_aseguradora = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_tablas() As DataTable

        Try
            StrSql = "select tablas='abc_paquete' union" & vbCrLf
            StrSql = StrSql & "select tablas='abc_uso' union" & vbCrLf
            StrSql = StrSql & "select tablas='bid_region' union" & vbCrLf
            StrSql = StrSql & "select tablas='cobertura_adicion_interiores' union" & vbCrLf
            StrSql = StrSql & "select tablas='cobertura_adicional_cuota' union" & vbCrLf
            StrSql = StrSql & "select tablas='cobertura_adicional_prima' union" & vbCrLf
            StrSql = StrSql & "select tablas='constante' union" & vbCrLf
            StrSql = StrSql & "select tablas='desempleo' union" & vbCrLf
            StrSql = StrSql & "select tablas='factvigencia' union" & vbCrLf
            StrSql = StrSql & "select tablas='limresp' union" & vbCrLf
            'strsql = strsql & "select tablas='parametro' union" & vbCrLf
            'strsql = strsql & "select tablas='parametro_bid' union" & vbCrLf
            StrSql = StrSql & "select tablas='precontrol' union" & vbCrLf
            StrSql = StrSql & "select tablas='promocion' union" & vbCrLf
            StrSql = StrSql & "select tablas='pormocion_excluida' union" & vbCrLf
            StrSql = StrSql & "select tablas='seguro_vida' union" & vbCrLf
            StrSql = StrSql & "select tablas='subsidios' union" & vbCrLf
            StrSql = StrSql & "select tablas='tariderpol' union" & vbCrLf
            StrSql = StrSql & "select tablas='tarifact' union" & vbCrLf
            StrSql = StrSql & "select tablas='tariprima' union" & vbCrLf
            StrSql = StrSql & "select tablas='vehiculo'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtener_maxArchivo(ByVal intUsuario As String) As DataTable

        Try
            StrSql = "SELECT max(id_bitacora) + 1 id_bitacora FROM ABC_Bitacora_tablas"
            'StrSql = StrSql & " WHERE Id_usuario = " & intUsuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function obtener_Archivo(ByVal intBitacora As Integer, ByVal intUsuario As Integer) As DataTable

        Try
            StrSql = "SELECT * FROM ABC_Bitacora_Tablas"
            StrSql = StrSql & " WHERE Id_Bitacora > " & intBitacora & ""
            StrSql = StrSql & " AND Id_usuario = " & intUsuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
