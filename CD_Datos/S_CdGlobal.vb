Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Public Class S_CdGlobal
    Private Shared StrConexion As String = ""
    Private Shared cnnBase As SqlConnection = New SqlConnection
    Private Shared cnnTran As SqlTransaction

    Private Sub AsignaBase()
        StrConexion = CC.General.Configuraciones.Seguridad.Decrypt(System.Configuration.ConfigurationManager.AppSettings(CC.General.Diccionario.AppSettings.CONN_STRING))
    End Sub

    Public Function AbrirConsulta(ByVal StrSql As String, Optional ByVal DataTable As Boolean = False, Optional ByVal ConectarBase As Boolean = True, Optional ByVal DesConectarBase As Boolean = True, Optional ByVal ConsultaTrans As Boolean = False)
        Dim ds As New DataSet
        Try
            If ConsultaTrans = False Then
                If ConectarBase = True Then
                    ConectaBase()
                End If

                Dim da As SqlDataAdapter
                da = New SqlDataAdapter(StrSql, cnnBase)
                da.SelectCommand.CommandType = CommandType.Text
                'Dim da As New SqlDataAdapter
                'Dim cmd As New SqlCommand(StrSql, cnnBase)
                'cmd.CommandTimeout = 30
                'da.SelectCommand = cmd
                da.Fill(ds)
                If DesConectarBase = True Then
                    DesConectaBase()
                End If
            Else
                Dim da As New SqlDataAdapter
                Dim cmd As New SqlCommand(StrSql, cnnBase, cnnTran)
                cmd.CommandType = CommandType.Text
                da.SelectCommand = cmd
                da.Fill(ds)
            End If
            If DataTable = True Then
                'Regresa un datatable
                Return ds.Tables(0)
            Else
                'Regresa un Dataset
                Return ds
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ConectaBase()
        On Error Resume Next
        AsignaBase()
        cnnBase = New SqlConnection(StrConexion)
        cnnBase.Open()
    End Function

    Private Shared Function DesConectaBase()
        On Error Resume Next
        cnnBase.Close()
    End Function

    Public Function Transaccion(ByVal StrTransaccion As String, Optional ByVal BTran As Boolean = False, Optional ByVal CTran As Boolean = False, Optional ByVal ConectarBase As Boolean = True, Optional ByVal DesConectarBase As Boolean = False)
        If ConectarBase = True Then
            ConectaBase()
        End If
        If BTran = True Then
            cnnTran = cnnBase.BeginTransaction(IsolationLevel.ReadUncommitted, StrTransaccion)
        End If
        If CTran = True Then
            cnnTran.Commit()
        End If
        If DesConectarBase = True Then
            DesConectaBase()
        End If
    End Function
    Public Function EjecutaSQLNoTrans(ByVal StrSql As String) As Boolean
        Try
            If Not cnnBase.State = ConnectionState.Open Then
                cnnBase.Open()
            End If
            Dim cnnCmd As New SqlCommand(StrSql, cnnBase)
            cnnCmd.CommandType = CommandType.Text
            cnnCmd.ExecuteNonQuery()
            If Not cnnBase.State = ConnectionState.Closed Then
                cnnBase.Close()
            End If
            EjecutaSQLNoTrans = True
        Catch ex As Exception
            EjecutaSQLNoTrans = False
            If Not cnnBase.State = ConnectionState.Closed Then
                cnnBase.Close()
            End If
            Throw ex
        End Try
    End Function

    Public Function EjecutaSQL(ByVal StrTransaccion As String, ByVal StrSql As String, Optional ByRef FilasAfectadas As Integer = 0) As Boolean
        Try
            Dim cnnCmd As New SqlCommand(StrSql, cnnBase, cnnTran)
            cnnCmd.CommandType = CommandType.Text
            FilasAfectadas = cnnCmd.ExecuteNonQuery()
        Catch ex As Exception
            EjecutaSQL = False
            cnnTran.Rollback(StrTransaccion)
            Throw ex
        End Try
    End Function

    Public Function EjecutaStored(ByVal nameSP As String, ByVal parameters As SqlParameter()) As DataSet
        Try
            Dim ds As New DataSet

            If (Not cnnBase.State = ConnectionState.Open) Then
                ConectaBase()
            End If

            Dim StrSql As String = nameSP & " "
            For x As Integer = 0 To parameters.Length - 1
                StrSql += parameters(x).ParameterName & "=" & parameters(x).Value() & ","
            Next

            Dim da = New SqlDataAdapter(StrSql.Substring(0, StrSql.Length - 1), cnnBase)
            da.Fill(ds)

            If (Not cnnBase.State = ConnectionState.Closed) Then
                DesConectaBase()
            End If

            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
