Imports CD_Datos.CdSql
Imports System.IO

Public Class CdGeneraLayout
    Private StrSql As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New CdSql

    Public Sub New()
    End Sub

    'EVI 15/12/2014
    Public Function GeneraLayout(ByVal IdPrograma As Integer, ByVal TipoLayout As Integer, ByVal Strscript As String, ByVal Strlogs As String, ByVal Baninterface As Integer) As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim strRegistro As String = ""
        Dim swFile As StreamWriter, swErrLog As StreamWriter
        Dim aIdPoliza As ArrayList = New ArrayList
        Dim StrSalida As String = ""
        Dim Strlog As String = ""
        'Dim StrSalida As String = "Emision de interfases_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
        'Dim Strlog As String = "Error Emision de interfases" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"
        Dim Aseguradoras As String = ""
        Try

            If IdPrograma = 29 Then 'Conauto
                '2015 se agrega validacion donde las polizas de seguro gratis se le agrege el correo de la segunda polizas
                If TipoLayout = 1 Then  'Polizas Altas
                    StrSalida = "Emision de interfases 29ConautoPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 29ConautoPolAlta " & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Bid, Id_Poliza, Poliza, endoso, Fecha_Registro, Insco, Anio, Descripcion, Placas, Num_Contrato, " & _
                    "Vigencia_FIniP, Vigencia_FFinp, uso, Num_Pago, Periodo, Motor, Serie, Renave, Capacidad, " & _
                    "Descripcion_TipoCarga, Precio_Vehiculo, PrimaNeta, DerPol, Pago_Fraccionado, Descuentos, " & _
                    "Iva, Recargos, SesionComision, Comision, PrimaTotal, Razon_Social, Nombre, APaterno, AMaterno, " & _
                    "Per_Fiscal, Calle_Cliente, NoInterior, NoExterior, Colonia_Cliente, Estado_Cliente, " & _
                    "Ciudad_Cliente,  Cp_Cliente, Rfc, Per_Fiscal, Id_Aseguradora, Id_Paquete, Telefono, " & _
                    "FNacimiento, Email = CASE WHEN isnull(Contador_Poliza,0) = 0 Then case when FEC.EMAIL_INTERFACE_CONAUTO29(Id_Cotizacion) <> '' " & _
                    "then FEC.EMAIL_INTERFACE_CONAUTO29(Id_Cotizacion) else email end Else Email End, " & _
                    "Clave_Agente, Plazo, Subramo, TipoProducto, Contador_Poliza, " & _
                    "Benef_Pref, Sexo, Estado_Civil, Marca, Id_TipoRecargo, Caratula, Contrato, Tarifa, Cobertura, Modelo, Id_Uso " & _
                    "FROM vw_carga_interfase_conauto29 " & _
                    "WHERE Id_Aseguradora in (59,60,61) " & _
                    "AND Interface = 0 "

                ElseIf TipoLayout = 2 Then  'Polizas Canceladas
                    StrSalida = "Emision de interfases 29ConautoPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 29ConautoPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Aseguradora, Id_Poliza, Poliza, Vigencia_FIniP, Vigencia_FFinp, Fecha_Registro, Fecha_Cancelacion, Descripcion_Cancelacion " & _
                    "FROM vw_carga_interfase_conauto29 " & _
                    "WHERE Id_Aseguradora in (59,60,61) " & _
                    "AND Interface_cancelacion = 0 " & _
                    "AND Id_Cancelacion = 1 " & _
                    "AND Fecha_Cancelacion is not null "

                    'solo son datos de pruebas
                    '"AND Id_Poliza in (2343,2344,2345,2346,2347)"

                End If

            ElseIf IdPrograma = 30 Then     'Conautopcion
                If TipoLayout = 1 Then  'Polizas Altas
                    StrSalida = "Emision de interfases 30ConautopcionPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 30ConautopcionPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Bid, Id_Poliza, Poliza, endoso, Insco, Anio, Descripcion, Placas, Num_Contrato, " & _
                    "Vigencia_FIniP, Vigencia_FFinp, uso, Num_Pago, Periodo, Motor, Serie, Renave, Capacidad, " & _
                    "Descripcion_TipoCarga, Precio_Vehiculo, PrimaNeta, DerPol, Pago_Fraccionado, Descuentos, " & _
                    "Iva, Recargos, SesionComision, Comision, PrimaTotal, Razon_Social, Nombre, APaterno, AMaterno, " & _
                    "Per_Fiscal, Calle_Cliente, NoInterior, NoExterior, Colonia_Cliente, Estado_Cliente, " & _
                    "Ciudad_Cliente, Cp_Cliente, Rfc, Per_Fiscal, Id_Aseguradora, Id_Paquete, Telefono, " & _
                    "FNacimiento, Email, Clave_Agente, Plazo, Subramo, TipoProducto, fecha_cancelacion, Id_TipoRecargo " & _
                    "Contador_Poliza, Id_TipoRecargo, Caratula, Contrato, Tarifa, Cobertura, Modelo " & _
                    "FROM vw_carga_interfase_conauto " & _
                    "WHERE Id_Aseguradora in (62,63) " & _
                    "AND Interface = 0"

                ElseIf TipoLayout = 2 Then  'Polizas Canceladas
                    StrSalida = "Emision de interfases 30ConautopcionPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 30ConautopcionPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Aseguradora, Id_Poliza, Poliza, Vigencia_FIniP, Vigencia_FFinp, Fecha_Registro, Fecha_Cancelacion, Descripcion_Cancelacion " & _
                    "FROM vw_carga_interfase_conauto " & _
                    "WHERE Id_Aseguradora in (62,63) " & _
                    "AND Interface_cancelacion = 0 " & _
                    "AND Fecha_Cancelacion is not null "

                    '"AND Id_Cancelacion = 1 " & _

                End If
            ElseIf IdPrograma = 31 Then     'Arrendadora
                If TipoLayout = 1 Then  'Polizas Altas
                    StrSalida = "Emision de interfases 31ArrendadoraPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 31ArrendadoraPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Bid, Id_Poliza, Poliza, endoso, Insco, Anio, Descripcion, Placas, Num_Contrato, " & _
                    "Vigencia_FIniP, Vigencia_FFinp, uso, Num_Pago, Periodo, Motor, Serie, Renave, Capacidad, " & _
                    "Descripcion_TipoCarga, Precio_Vehiculo, PrimaNeta, DerPol, Pago_Fraccionado, Descuentos, " & _
                    "Iva, Recargos, SesionComision, Comision, PrimaTotal, Razon_Social, Nombre, APaterno, AMaterno, " & _
                    "Per_Fiscal, Calle_Cliente, NoInterior, NoExterior, Colonia_Cliente, Estado_Cliente, " & _
                    "Ciudad_Cliente,  Cp_Cliente, Rfc, Per_Fiscal, Id_Aseguradora, Id_Paquete, Telefono, " & _
                    "FNacimiento, Email, Clave_Agente, Plazo, Subramo, TipoProducto, fecha_cancelacion, Id_TipoRecargo " & _
                    "Contador_Poliza, Id_TipoRecargo, Caratula, Contrato, Tarifa, Cobertura, Modelo " & _
                    "FROM vw_carga_interfase_conauto " & _
                    "WHERE Id_Aseguradora in (64,65) " & _
                    "AND Interface = 0"

                ElseIf TipoLayout = 2 Then  'Polizas Canceladas
                    StrSalida = "Emision de interfases 31ArrendadoraPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 31ArrendadoraPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Aseguradora, Id_Poliza, Poliza, Vigencia_FIniP, Vigencia_FFinp, Fecha_Registro, Fecha_Cancelacion, Descripcion_Cancelacion " & _
                    "FROM vw_carga_interfase_conauto " & _
                    "WHERE Id_Aseguradora in (64,65) " & _
                    "AND Interface_cancelacion = 0 " & _
                    "AND Fecha_Cancelacion is not null "

                    '"AND Id_Cancelacion = 1 " & _
                End If

            ElseIf IdPrograma = 32 Then 'Mazda
                '2015 se agrega validacion donde las polizas de seguro gratis se le agrege el correo de la segunda polizas
                If TipoLayout = 1 Then  'Polizas Altas
                    StrSalida = "Emision de interfases 32MazdaPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 32MazdaPolAlta " & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Bid, Id_Poliza, Poliza, endoso, Fecha_Registro, Insco, Anio, Descripcion, Placas, Num_Contrato, " & _
                    "Vigencia_FIniP, Vigencia_FFinp, uso, Num_Pago, Periodo, Motor, Serie, Renave, Capacidad, " & _
                    "Descripcion_TipoCarga, Precio_Vehiculo, PrimaNeta, DerPol, Pago_Fraccionado, Descuentos, " & _
                    "Iva, Recargos, SesionComision, Comision, PrimaTotal, Razon_Social, Nombre, APaterno, AMaterno, " & _
                    "Per_Fiscal, Calle_Cliente, NoInterior, NoExterior, Colonia_Cliente, Estado_Cliente, " & _
                    "Ciudad_Cliente,  Cp_Cliente, Rfc, Per_Fiscal, Id_Aseguradora, Id_Paquete, Telefono, " & _
                    "FNacimiento, " & _
                    "Email = CASE WHEN isnull(Contador_Poliza,0) = 0 Then case when FEC.EMAIL_INTERFACE_CONAUTO29(Id_Cotizacion) <> '' " & _
                    "then FEC.EMAIL_INTERFACE_CONAUTO29(Id_Cotizacion) else email end Else Email End, " & _
                    "Clave_Agente, Plazo, Subramo, TipoProducto, Contador_Poliza, " & _
                    "Benef_Pref, Sexo, Estado_Civil, Marca, Id_TipoRecargo, Caratula, Contrato, Tarifa, Cobertura, Modelo, Id_Uso " & _
                    "FROM vw_carga_interfase_conauto29 " & _
                    "WHERE Id_Aseguradora in (66,67,68) " & _
                    "AND Interface = 0 "

                ElseIf TipoLayout = 2 Then  'Polizas Canceladas
                    StrSalida = "Emision de interfases 32MazdaPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                    Strlog = "Error Emision de interfases 32MazdaPolCan_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second & ".log"

                    StrSql = "SELECT Id_Aseguradora, Id_Poliza, Poliza, Vigencia_FIniP, Vigencia_FFinp, Fecha_Registro, Fecha_Cancelacion, Descripcion_Cancelacion " & _
                    "FROM vw_carga_interfase_conauto29 " & _
                    "WHERE Id_Aseguradora in (66,67,68) " & _
                    "AND Interface_cancelacion = 0 " & _
                    "AND Id_Cancelacion = 1 " & _
                    "AND Fecha_Cancelacion is not null "

                End If
            End If

            'swFile = New StreamWriter("D:\Aplicaciones\CotizadorContado_Conauto\Script\" & String.Format("{0:yyyyMMdd}", System.DateTime.Now) & ".txt")
            'swErrLog = New StreamWriter("D:\Aplicaciones\CotizadorContado_Conauto\Script\" & String.Format("{0:yyyyMMdd}", System.DateTime.Now) & ".log")

            'PC Pruebas
            'swFile = New StreamWriter("C:\Aon\Aplicaciones\CotizadorContado_Conauto Seguridad\Script\" & StrSalida & ".txt")
            'swErrLog = New StreamWriter("C:\Aon\Aplicaciones\CotizadorContado_Conauto Seguridad\ArchivosLog\" & Strlog & ".log")

            swFile = New StreamWriter("D:\Aplicaciones\CotizadorContado_Conauto\Script\" & StrSalida & ".txt")
            swErrLog = New StreamWriter("D:\Aplicaciones\CotizadorContado_Conauto\ArchivosLog\" & Strlog & ".log")

            'EVI 15/12/2014
            'swFile = New StreamWriter(Strscript & StrSalida & ".txt")
            'swErrLog = New StreamWriter(Strlogs & Strlog & ".log")

            dt = CConecta.AbrirConsulta(StrSql, True)
            For Each dr In dt.Rows
                If IdPrograma = 29 Then 'Conauto
                    If TipoLayout = 1 Then
                        strRegistro = ArmaRegistroConautoPolAltas(dr, IdPrograma)
                    ElseIf TipoLayout = 2 Then
                        strRegistro = ArmaRegistroConautoPolBajas(dr, IdPrograma)
                    End If
                ElseIf IdPrograma = 30 Then 'Conautopcion
                    If TipoLayout = 1 Then
                        strRegistro = ArmaRegistroConautopcion(dr, IdPrograma)
                    ElseIf TipoLayout = 2 Then
                        strRegistro = ArmaRegistroConautopcionPolBajas(dr, IdPrograma)
                    End If
                ElseIf IdPrograma = 31 Then 'Arrendadora
                    If TipoLayout = 1 Then
                        strRegistro = ArmaRegistroArrendadora(dr, IdPrograma)
                    ElseIf TipoLayout = 2 Then
                        strRegistro = ArmaRegistroArrendadoraPolBajas(dr, IdPrograma)
                    End If
                ElseIf IdPrograma = 32 Then 'Mazda
                    If TipoLayout = 1 Then
                        strRegistro = ArmaRegistroMazdaPolAltas(dr, IdPrograma)
                    ElseIf TipoLayout = 2 Then
                        strRegistro = ArmaRegistroMazdaPolBajas(dr, IdPrograma)
                    End If
                End If

                If strRegistro.Substring(0, 5) <> "error" Then
                    aIdPoliza.Add(dr("Id_Poliza"))
                    swFile.WriteLine(strRegistro)
                Else
                    swErrLog.WriteLine(strRegistro)
                End If
            Next

            'No ha informaci�n
            If dt.Rows.Count = 0 Then
                StrSalida = ""
            End If

            swFile.Close()
            swErrLog.Close()

            StrSql = ""
            'EVI 15/12/2014
            StrSql = UpdateString(aIdPoliza, TipoLayout, Baninterface)

            If StrSql.Length > 0 Then
                StrTransaccion = "actualizascript"
                CConecta.Transaccion(StrTransaccion, True, False, True)
                'Descomentariar para pasar a producci�n.
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
                CConecta.Transaccion(StrTransaccion, False, True, False, True)
            End If

            Return StrSalida

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'EVI 15/12/2014
#Region "Genera layout Conauto"

    Private Function ArmaRegistroConautoPolAltas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim dtP As DataTable
        Dim i As Integer
        Dim Meses As Integer
        Dim Estado As String
        Dim Ciudad As String
        Dim bandAseguradora As Integer
        Dim SeguroGratis As Integer = 0

        Try
            'Programa (29:Conauto, 30:Conautopcion)
            registro &= IdPrograma & "|"

            If IdPrograma = 29 Then 'Conauto
                If dr("Id_Aseguradora") = 59 Then   'Qualitas
                    registro &= "3" & "|"
                ElseIf dr("Id_Aseguradora") = 60 Then   'Gnp
                    registro &= "1" & "|"
                ElseIf dr("Id_Aseguradora") = 61 Then   'Axa
                    registro &= "2" & "|"
                End If
            End If

            registro &= dr("Poliza") & "|"

            'Numero poliza anterior
            registro &= "" & "|"
            'Numero de vigencia
            registro &= "" & "|"
            'Numero de endoso
            registro &= dr("endoso") & "|"
            'Numero de termino
            registro &= "" & "|"
            'Numero de vigencia termino
            registro &= "" & "|"
            'punto de venta
            registro &= "" & "|"
            'Referencia (Bid, Grupo e Integrante)
            If Len(dr("Num_Contrato")) = 9 Then
                registro &= "0" & dr("Num_Contrato") & "|"
            Else
                registro &= dr("Num_Contrato") & "|"
            End If
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'N�mero de pagos
            registro &= dr("Num_Pago") & "|"
            'Clave forma de pago
            'HM 18/03/2014 Plazo de 12 meses = 0 (Contado) y Plazo diferente a 12 meses = 360 (Anual)
            'Forma de Pago
            If dr("Id_Aseguradora") = 59 Then   'Qualitas
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 60 Then   'Gnp
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 61 Then   'Axa
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            End If

            'Clave de Producto
            registro &= "FNVO" & "|"
            'Prima Neta
            registro &= String.Format("{0:0.00}", dr("PrimaNeta")) & "|"
            'Descuentos coberturas
            registro &= "0" & "|"
            'Recargos coberturas
            registro &= "0" & "|"
            'Derechos de p�liza
            registro &= String.Format("{0:0.00}", dr("DerPol")) & "|"
            'Tasa de financiamiento (Axa, Qualitas, Gnp)
            registro &= String.Format("{0:0.00}", dr("Pago_Fraccionado")) & "|"
            'Total descuentos generales
            registro &= String.Format("{0:0.00}", dr("Descuentos")) & "|"
            'IVA
            registro &= String.Format("{0:0.00}", dr("Iva")) & "|"
            'Recargos generales
            registro &= String.Format("{0:0.00}", dr("Recargos")) & "|"
            'Prima total
            registro &= String.Format("{0:0.00}", dr("PrimaTotal")) & "|"
            'Sesion Comision
            registro &= String.Format("{0:0.00}", dr("SesionComision")) & "|"
            'Comision
            registro &= String.Format("{0:0.00}", dr("Comision")) & "|"
            'Prima neta primer recibo
            registro &= "0" & "|"
            'Prima neta recibos subsecuentes
            registro &= "0" & "|"
            'Prorrateo del derecho
            registro &= "0" & "|"
            'C�ratula
            registro &= "0" & "|"
            'Contrato
            registro &= "0" & "|"
            'Tipo de p�liza
            registro &= "0" & "|"
            'Datos del contratante
            registro &= "" & "|"
            'Beneficiario preferente
            registro &= dr("Benef_Pref") & "|"
            'Clave del agente
            registro &= dr("Clave_Agente") & "|"
            'Moneda
            registro &= "NACIONAL" & "|"

            'Determina si es seguro gratis de acuerdo al programa y al tipo de producto
            If dr("Id_Aseguradora") = "61" Then
                If dr("TipoProducto") = 1 Then
                    SeguroGratis = 1
                Else
                    SeguroGratis = 0
                End If
            End If

            'Clave tarifa
            If dr("Id_Aseguradora") = "59" Then  'Qualitas
                registro &= "5432^5432^5432" & "|"
            ElseIf dr("Id_Aseguradora") = "60" Then  'Gnp
                If IsDBNull(dr("Tarifa")) Then
                    If dr("SubRamo") = 1 Then
                        registro &= "TA6111" & "|"
                    Else
                        registro &= "TA5193" & "|"
                    End If
                ElseIf dr("Tarifa") = "" Then
                    If dr("SubRamo") = 1 Then
                        registro &= "TA6111" & "|"
                    Else
                        registro &= "TA5193" & "|"
                    End If
                Else
                    registro &= dr("Tarifa") & "|"
                End If
            ElseIf dr("Id_Aseguradora") = "61" Then  'Axa 
                If IsDBNull(dr("Tarifa")) Then
                    registro &= "" & "|"
                Else
                    registro &= dr("Tarifa") & "|"
                End If
                'If dr("SubRamo") = 1 Then   'Auto
                '    'If dr("TipoProducto") = 1 Then
                '    'Seguro Gratis
                '    If dr("TipoProducto") = 0 Or dr("TipoProducto") = 1 Or _
                '    dr("TipoProducto") = 2 Or dr("TipoProducto") = 3 Or _
                '    dr("TipoProducto") = 8 Then
                '        'verifica si las polizas son cruzadas
                '        StrSql = "SELECT * FROM Poliza WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
                '        StrSql = StrSql & "AND Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                '        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                '        StrSql = StrSql & "Order by Contador_Poliza"
                '        dtP = CConecta.AbrirConsulta(StrSql, True)

                '        If dtP.Rows.Count = 0 Then
                '            If dtP.Rows(i).Item("Id_Aseguradora") = "61" Then
                '                bandAseguradora = 1
                '            Else
                '                bandAseguradora = 0
                '            End If
                '        End If

                '        If bandAseguradora = 0 Then
                '            If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                '                registro &= "TA6111" & "|"
                '            Else    'si poliza 2 = Axa
                '                registro &= "TA6111" & "|"
                '            End If
                '        Else    'Polizas de la misma aseguradora Axa
                '            registro &= "TA6111" & "|"
                '        End If
                '    Else    'Multianual
                '        registro &= "TA6110" & "|"
                '    End If
                'Else    'Cami�n
                '    'If dr("TipoProducto") = 1 Then  
                '    'Seguro Gratis
                '    If dr("TipoProducto") = 0 Or dr("TipoProducto") = 1 Or _
                '    dr("TipoProducto") = 2 Or dr("TipoProducto") = 3 Or _
                '    dr("TipoProducto") = 8 Then
                '        'verifica si las polizas son cruzadas
                '        StrSql = "SELECT * FROM Poliza WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
                '        StrSql = StrSql & "AND Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                '        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                '        StrSql = StrSql & "Order by Contador_Poliza"
                '        dtP = CConecta.AbrirConsulta(StrSql, True)

                '        If dtP.Rows.Count = 0 Then
                '            If dtP.Rows(i).Item("Id_Aseguradora") = "61" Then
                '                bandAseguradora = 1
                '            Else
                '                bandAseguradora = 0
                '            End If
                '        End If

                '        If bandAseguradora = 0 Then
                '            If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                '                registro &= "TA5193" & "|"
                '            Else    'si poliza 2 = Axa
                '                registro &= "TA5193" & "|"
                '            End If
                '        Else    'Polizas de la misma aseguradora Axa
                '            registro &= "TA5193" & "|"
                '        End If
                '    Else    'Multianual
                '        registro &= "TA5192" & "|"
                '    End If
                'End If
                'registro &= dr("Insco") & "|"
            End If

            'Observaciones
            registro &= "" & "|"
            'Nombre
            If dr("Per_Fiscal") = 2 Then
                registro &= dr("Razon_Social") & "|||"
            Else
                registro &= dr("Nombre") & "|"
                registro &= dr("APaterno") & "|"
                registro &= dr("AMaterno") & "|"
            End If
            'Domicilio
            registro &= dr("Calle_Cliente")
            registro = Trim(registro & " " & dr("NoExterior"))
            registro = Trim(registro & " " & dr("NoInterior")) & "|"
            'Colonia
            registro &= dr("Colonia_Cliente") & "|"
            'Estado
            Estado = dr("Estado_Cliente")
            Estado = Replace(Estado, "�", "A")
            Estado = Replace(Estado, "�", "E")
            Estado = Replace(Estado, "�", "I")
            Estado = Replace(Estado, "�", "O")
            Estado = Replace(Estado, "�", "U")
            registro &= Estado & "|"

            'Ciudad
            Ciudad = dr("Ciudad_Cliente")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "�", "E")
            Ciudad = Replace(Ciudad, "�", "I")
            Ciudad = Replace(Ciudad, "�", "O")
            Ciudad = Replace(Ciudad, "�", "U")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "Ñ", "�")
            Ciudad = Replace(Ciudad, "Ê", "U")
            Ciudad = Replace(Ciudad, "É", "E")
            registro &= Ciudad & "|"

            'C�digo postal
            registro &= dr("Cp_Cliente") & "|"
            'Tel�fono
            registro &= dr("Telefono") & "|"
            'RFC Titular
            registro &= dr("Rfc") & "|"
            'Tipo de Persona
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"
            'Fecha de nacimiento
            registro &= dr("FNacimiento") & "|"
            'Correo
            registro &= dr("Email") & "|"
            'Estado civil
            registro &= dr("Estado_Civil") & "|"
            'Sexo
            registro &= dr("Sexo") & "|"
            'RFC Conductor
            registro &= "" & "|"
            'Kilometros recorridos del vehiculo
            registro &= "" & "|"
            'Marca del vehiculo
            registro &= dr("Marca") & "|"
            'Modelo del vehiculo
            registro &= dr("Anio") & "|"
            'Descripcion del vehiculo
            registro &= dr("Descripcion") & "|"
            'Capacidad del vehiculo
            registro &= dr("Capacidad") & "|"
            'Placas
            registro &= dr("Placas") & "|"
            'N�mero de motor
            registro &= dr("Motor") & "|"
            'N�mero de serie
            registro &= dr("Serie") & "|"
            'Transmisi�n
            registro &= "" & "|"
            'RFV del auto
            registro &= "" & "|"
            'Suma asegurada
            registro &= String.Format("{0:0.00}", dr("Precio_Vehiculo")) & "|"
            'Clave aseguradora
            registro &= dr("Insco") & "|"
            'Clase del vehiculo
            If dr("SubRamo") = 1 Then
                registro &= "2" & "|"
            Else
                registro &= "4" & "|"
            End If
            'Servicio
            If dr("SubRamo") = 1 Then
                registro &= "A" & "|"
            Else
                If SeguroGratis = 1 Then
                    registro &= "A" & "|"
                Else
                    registro &= "P" & "|"
                End If
            End If
            'Uso
            registro &= dr("uso") & "|"
            'Tipo de carga
            'AXA
            If dr("Id_Aseguradora") = 61 Then
                If dr("id_uso") = 1 Then
                    registro &= "" & "|"
                Else
                    'evi 18/12/2014 SG1 y SG2 no lleva carga
                    If dr("tipoproducto") = 0 Then
                        registro &= "" & "|"
                    Else
                        registro &= "B" & "|"
                    End If
                End If
            Else
                'Qualitas
                If dr("Id_Aseguradora") = 59 Then
                    If dr("tipoproducto") = 0 Then
                        registro &= "" & "|"
                    Else
                        If dr("SubRamo") = 1 Then
                            registro &= "" & "|"
                        Else
                            registro &= "B" & "|"
                        End If
                    End If
                Else
                    'GNP
                    'evi 18/12/2014 SG1 y SG2 no lleva carga
                    If dr("tipoproducto") = 0 Then
                        registro &= "" & "|"
                    Else
                        If dr("subramo") >= 2 Then
                            registro &= "B" & "|"
                        Else
                            registro &= "" & "|"
                        End If
                    End If
                End If
            End If

            'Tonelaje
            registro &= "" & "|"
            'l�nea de vehiculo
            registro &= "" & "|"
            'Origen
            registro &= "" & "|"
            'Paquete
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Estado circulaci�n
            registro &= "" & "|"
            'Tipo de emisi�n
            registro &= "" & "|"
            'Fecha de cancelacion
            registro &= "" & "|"
            'Renave
            registro &= dr("Renave") & "|"
            'Parametro1
            registro &= "01" & "|"
            'Parametro2
            registro &= "01" & "|"

            StrSql = "SELECT * FROM limresp WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
            StrSql = StrSql & "AND Id_Paquete = " & dr("Id_Paquete") & vbCrLf
            StrSql = StrSql & "Order by Id_Paquete "
            dtC = CConecta.AbrirConsulta(StrSql, True)

            Dim bandera As Integer = 0

            If dtC.Rows.Count = 0 Then
                'Suma rc
                registro &= "0" & "|"
                'Suma gm
                registro &= "0" & "|"
                'Ded dm
                registro &= "0" & "|"
                'Ded rt
                registro &= "0" & "|"
                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 59 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 60 Then   'Gnp
                    registro &= "0" & "|"
                ElseIf dr("Id_Aseguradora") = 61 Then   'Axa
                    registro &= "0" & "|"
                End If

                'Suma rc complementaria
                registro &= "0"
            Else
                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc
                    If dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL" Or dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL (LUC)" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma gm
                    If dtC.Rows(i).Item("Cobertura") = "GASTOS MEDICOS OCUPANTES" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded dm
                    If dtC.Rows(i).Item("Cobertura") = "DA�OS MATERIALES" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded rt
                    If dtC.Rows(i).Item("Cobertura") = "ROBO TOTAL" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 59 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 60 Then   'Gnp
                    registro &= "0" & "|"
                ElseIf dr("Id_Aseguradora") = 61 Then   'Axa
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc complementaria
                    If dtC.Rows(i).Item("Cobertura") = "RC COMPLEMENTARIA" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Plazo
                registro &= dr("Plazo") & "|"

                'Subramo
                registro &= dr("Subramo") & "|"

                'TipoProducto
                registro &= dr("TipoProducto") & "|"

                'Contador Poliza
                registro &= dr("Contador_Poliza") & "|"

                'Sexo
                registro &= dr("Sexo") & "|"

                'Estado_Civil
                registro &= dr("Estado_Civil") & "|"

                'Marca
                registro &= dr("Marca") & "|"

                'Forma de Pago
                registro &= dr("Id_TipoRecargo") & "|"

                'Caratula
                registro &= dr("Caratula") & "|"

                'Contrato
                registro &= dr("Contrato")

            End If

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

    Private Function ArmaRegistroConautoPolBajas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim i As Integer
        Dim Meses As Integer

        Try
            'Programa (29:Conauto)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 59 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 60 Then   'Gnp
                registro &= "1" & "|"
            ElseIf dr("Id_Aseguradora") = 61 Then   'Axa
                registro &= "2" & "|"
            End If

            registro &= dr("Poliza") & "|"
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'Fecha de Cancelaci�n
            registro &= dr("Fecha_Cancelacion") & "|"
            'Descripci�n de Cancelaci�n
            registro &= dr("Descripcion_Cancelacion")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

#End Region

    'EVI 15/12/2014
#Region "Genera layout Conautopcion"

    Private Function ArmaRegistroConautopcion(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim dtP As DataTable
        Dim i As Integer
        Dim Meses As Integer
        Dim Estado As String
        Dim Ciudad As String
        Dim bandAseguradora As Integer
        Dim SeguroGratis As Integer = 0

        Try
            'Programa (29:Conauto, 30:Conautopcion)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 62 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 63 Then   'Axa
                registro &= "2" & "|"
            End If
            registro &= dr("Poliza") & "|"
            registro &= dr("endoso") & "|"
            registro &= dr("Insco") & "|"
            registro &= dr("Anio") & "|"
            registro &= dr("Descripcion") & "|"
            registro &= dr("Placas") & "|"
            'Referencia (Bid, Grupo e Integrante)
            If Len(dr("Num_Contrato")) = 9 Then
                registro &= "0" & dr("Num_Contrato") & "|"
            Else
                registro &= dr("Num_Contrato") & "|"
            End If
            registro &= dr("Vigencia_FIniP") & "|"
            registro &= dr("Vigencia_FFinp") & "|"
            registro &= dr("uso") & "|"
            registro &= dr("Num_Pago") & "|"

            'HM 06/02/2014
            'HM 18/03/2014 Plazo de 12 meses = 0 (Contado) y Plazo diferente a 12 meses = 360 (Anual)
            'Forma de Pago
            If dr("Id_Aseguradora") = 62 Then   'Qualitas
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                    'registro &= dr("Periodo") & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 63 Then   'Axa
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
                'Meses = DateDiff(DateInterval.Month, dr("Vigencia_FFinp"), dr("Vigencia_FIniP"))
                'registro &= dr("Periodo") & "|"
            End If

            registro &= dr("Motor") & "|"
            registro &= dr("Serie") & "|"
            registro &= dr("Renave") & "|"
            'registro &= dr("Capacidad") & "|"
            'registro &= IIf(IsDBNull(dr("Descripcion_TipoCarga")), "Carga A", dr("Descripcion_TipoCarga")) & "|"
            registro &= String.Format("{0:0.00}", dr("PrimaNeta")) & "|"
            registro &= String.Format("{0:0.00}", dr("DerPol")) & "|"
            registro &= String.Format("{0:0.00}", dr("Pago_Fraccionado")) & "|"
            registro &= String.Format("{0:0.00}", dr("Descuentos")) & "|"
            registro &= String.Format("{0:0.00}", dr("Iva")) & "|"
            registro &= String.Format("{0:0.00}", dr("Recargos")) & "|"
            registro &= String.Format("{0:0.00}", dr("SesionComision")) & "|"
            registro &= String.Format("{0:0.00}", dr("Comision")) & "|"
            registro &= String.Format("{0:0.00}", dr("PrimaTotal")) & "|"
            If dr("Per_Fiscal") = 2 Then
                registro &= dr("Razon_Social") & "|||"
            Else
                registro &= dr("Nombre") & "|"
                registro &= dr("APaterno") & "|"
                registro &= dr("AMaterno") & "|"
            End If
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"
            registro &= dr("Calle_Cliente")
            registro = Trim(registro & " " & dr("NoExterior"))
            registro = Trim(registro & " " & dr("NoInterior")) & "|"
            registro &= dr("Colonia_Cliente") & "|"

            'Estado
            Estado = dr("Estado_Cliente")
            Estado = Replace(Estado, "�", "A")
            Estado = Replace(Estado, "�", "E")
            Estado = Replace(Estado, "�", "I")
            Estado = Replace(Estado, "�", "O")
            Estado = Replace(Estado, "�", "U")
            registro &= Estado & "|"

            'Ciudad
            Ciudad = dr("Ciudad_Cliente")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "�", "E")
            Ciudad = Replace(Ciudad, "�", "I")
            Ciudad = Replace(Ciudad, "�", "O")
            Ciudad = Replace(Ciudad, "�", "U")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "Ñ", "�")
            Ciudad = Replace(Ciudad, "Ê", "U")
            Ciudad = Replace(Ciudad, "É", "E")
            registro &= Ciudad & "|"

            registro &= dr("Cp_Cliente") & "|"
            registro &= dr("Rfc") & "|"
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"

            registro &= dr("Telefono") & "|"
            registro &= dr("FNacimiento") & "|"
            registro &= dr("Email") & "|"

            'Determina si es seguro gratis de acuerdo al programa y al tipo de producto
            If dr("Id_Aseguradora") = "61" Then    'Conauto
                If dr("TipoProducto") = 1 Then
                    SeguroGratis = 1
                Else
                    SeguroGratis = 0
                End If
            ElseIf dr("Id_Aseguradora") = "63" Then    'Conautopcion
                If dr("TipoProducto") = 0 Or dr("TipoProducto") = 1 Or _
                dr("TipoProducto") = 2 Or dr("TipoProducto") = 3 Or dr("TipoProducto") = 8 Then
                    SeguroGratis = 1
                Else
                    SeguroGratis = 0
                End If
            End If


            If dr("Id_Aseguradora") = "62" Then  'Qualitas
                'registro &= "53496022" & "|"
                'registro &= "5349^5349^5349" & "|"
                registro &= "5432^5432^5432" & "|"
            ElseIf dr("Id_Aseguradora") = "63" Then  'Axa 
                If dr("SubRamo") = 1 Then   'Auto
                    If SeguroGratis = 1 Then  'Seguro Gratis
                        'Verifica si las polizas son cruzadas
                        StrSql = "SELECT * FROM Poliza WHERE Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                        StrSql = StrSql & "AND Id_Bid = '" & dr("Id_Bid") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Cobertura = '" & dr("Cobertura") & "'" & vbCrLf
                        StrSql = StrSql & "AND Modelo = '" & dr("Modelo") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Contador_Poliza <> '" & dr("Contador_Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND id_cancelacion is null" & vbCrLf
                        StrSql = StrSql & "Order by Contador_Poliza"
                        dtP = CConecta.AbrirConsulta(StrSql, True)

                        If dtP.Rows.Count > 0 Then
                            If dtP.Rows(i).Item("Id_Aseguradora") = "63" Then
                                bandAseguradora = 1
                            Else
                                bandAseguradora = 0
                            End If

                            If bandAseguradora = 0 Then
                                If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                                    registro &= "TA6111" & "|"
                                Else    'si poliza 2 = Axa
                                    registro &= "TA6111" & "|"
                                End If
                            Else    'Polizas de la misma aseguradora Axa
                                registro &= "TA6110" & "|"
                            End If
                        Else
                            registro &= dr("Tarifa") & "|"
                        End If

                    Else    'Multianual
                        registro &= "TA6110" & "|"
                    End If
                Else    'Cami�n
                    If SeguroGratis = 1 Then  'Seguro Gratis
                        'Verifica si las polizas son cruzadas
                        StrSql = "SELECT * FROM Poliza WHERE Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                        StrSql = StrSql & "AND Id_Bid = '" & dr("Id_Bid") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Cobertura = '" & dr("Cobertura") & "'" & vbCrLf
                        StrSql = StrSql & "AND Modelo = '" & dr("Modelo") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Contador_Poliza <> '" & dr("Contador_Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND id_cancelacion is null" & vbCrLf
                        StrSql = StrSql & "Order by Contador_Poliza"
                        dtP = CConecta.AbrirConsulta(StrSql, True)

                        If dtP.Rows.Count > 0 Then
                            If dtP.Rows(i).Item("Id_Aseguradora") = "63" Then
                                bandAseguradora = 1
                            Else
                                bandAseguradora = 0
                            End If

                            If bandAseguradora = 0 Then 'Poliza cruzada
                                If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                                    registro &= "TA5193" & "|"
                                Else    'si poliza 2 = Axa
                                    registro &= "TA5193" & "|"
                                End If
                            Else    'Polizas de la misma aseguradora Axa
                                registro &= "TA5192" & "|"
                            End If
                        Else
                            registro &= dr("Tarifa") & "|"
                        End If

                    Else    'Multianual
                        registro &= "TA5192" & "|"
                    End If
                End If
                'registro &= dr("Insco") & "|"
            End If
            registro &= dr("Clave_Agente") & "|"
            'Dato1
            registro &= "01" & "|"
            'Dato2
            registro &= "01" & "|"
            'Suma asegurada
            registro &= String.Format("{0:0.00}", dr("Precio_Vehiculo")) & "|"

            StrSql = "SELECT * FROM limresp WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
            StrSql = StrSql & "AND Id_Paquete = " & dr("Id_Paquete") & vbCrLf
            StrSql = StrSql & "Order by Id_Paquete "
            dtC = CConecta.AbrirConsulta(StrSql, True)

            Dim bandera As Integer = 0

            If dtC.Rows.Count = 0 Then
                'Suma rc
                registro &= "0" & "|"
                'Suma gm
                registro &= "0" & "|"
                'Ded dm
                registro &= "0" & "|"
                'Ded rt
                registro &= "0" & "|"
                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 62 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 63 Then   'Axa
                    registro &= "0" & "|"
                End If
                'registro &= "0" & "|"

                'Suma rc complementaria
                registro &= "0"
            Else
                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc
                    If dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma gm
                    If dtC.Rows(i).Item("Cobertura") = "GASTOS MEDICOS OCUPANTES" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded dm
                    If dtC.Rows(i).Item("Cobertura") = "DA�OS MATERIALES" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded rt
                    If dtC.Rows(i).Item("Cobertura") = "ROBO TOTAL" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 62 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 63 Then   'Axa
                    registro &= "0" & "|"
                End If
                'registro &= "0" & "|"

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc complementaria
                    If dtC.Rows(i).Item("Cobertura") = "RC COMPLEMENTARIA" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

            End If

            'Plazo
            registro &= dr("Plazo") & "|"

            'Subramo
            registro &= dr("Subramo")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

    Private Function ArmaRegistroConautopcionPolBajas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim i As Integer
        Dim Meses As Integer

        Try
            'Programa (30:Conautopcion)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 62 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 63 Then   'Axa
                registro &= "2" & "|"
            End If

            registro &= dr("Poliza") & "|"
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'Fecha de Cancelaci�n
            registro &= dr("Fecha_Cancelacion") & "|"
            'Descripci�n de Cancelaci�n
            registro &= dr("Descripcion_Cancelacion")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

#End Region

#Region "Genera layout Arrendadora"

    Private Function ArmaRegistroArrendadora(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim dtP As DataTable
        Dim i As Integer
        Dim Meses As Integer
        Dim Estado As String
        Dim Ciudad As String
        Dim bandAseguradora As Integer
        Dim SeguroGratis As Integer = 0

        Try
            'Programa (29:Conauto, 30:Conautopcion)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 64 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 65 Then   'Axa
                registro &= "2" & "|"
            End If
            registro &= dr("Poliza") & "|"
            registro &= dr("endoso") & "|"
            registro &= dr("Insco") & "|"
            registro &= dr("Anio") & "|"
            registro &= dr("Descripcion") & "|"
            registro &= dr("Placas") & "|"
            'Referencia (Bid, Grupo e Integrante)
            If Len(dr("Num_Contrato")) = 9 Then
                registro &= "0" & dr("Num_Contrato") & "|"
            Else
                registro &= dr("Num_Contrato") & "|"
            End If
            registro &= dr("Vigencia_FIniP") & "|"
            registro &= dr("Vigencia_FFinp") & "|"
            registro &= dr("uso") & "|"
            registro &= dr("Num_Pago") & "|"

            'HM 06/02/2014
            'HM 18/03/2014 Plazo de 12 meses = 0 (Contado) y Plazo diferente a 12 meses = 360 (Anual)
            'Forma de Pago
            If dr("Id_Aseguradora") = 64 Then   'Qualitas
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                    'registro &= dr("Periodo") & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 65 Then   'Axa
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
                'Meses = DateDiff(DateInterval.Month, dr("Vigencia_FFinp"), dr("Vigencia_FIniP"))
                'registro &= dr("Periodo") & "|"
            End If

            registro &= dr("Motor") & "|"
            registro &= dr("Serie") & "|"
            registro &= dr("Renave") & "|"
            'registro &= dr("Capacidad") & "|"
            'registro &= IIf(IsDBNull(dr("Descripcion_TipoCarga")), "Carga A", dr("Descripcion_TipoCarga")) & "|"
            registro &= String.Format("{0:0.00}", dr("PrimaNeta")) & "|"
            registro &= String.Format("{0:0.00}", dr("DerPol")) & "|"
            registro &= String.Format("{0:0.00}", dr("Pago_Fraccionado")) & "|"
            registro &= String.Format("{0:0.00}", dr("Descuentos")) & "|"
            registro &= String.Format("{0:0.00}", dr("Iva")) & "|"
            registro &= String.Format("{0:0.00}", dr("Recargos")) & "|"
            registro &= String.Format("{0:0.00}", dr("SesionComision")) & "|"
            registro &= String.Format("{0:0.00}", dr("Comision")) & "|"
            registro &= String.Format("{0:0.00}", dr("PrimaTotal")) & "|"
            If dr("Per_Fiscal") = 2 Then
                registro &= dr("Razon_Social") & "|||"
            Else
                registro &= dr("Nombre") & "|"
                registro &= dr("APaterno") & "|"
                registro &= dr("AMaterno") & "|"
            End If
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"
            registro &= dr("Calle_Cliente")
            registro = Trim(registro & " " & dr("NoExterior"))
            registro = Trim(registro & " " & dr("NoInterior")) & "|"
            registro &= dr("Colonia_Cliente") & "|"

            'Estado
            Estado = dr("Estado_Cliente")
            Estado = Replace(Estado, "�", "A")
            Estado = Replace(Estado, "�", "E")
            Estado = Replace(Estado, "�", "I")
            Estado = Replace(Estado, "�", "O")
            Estado = Replace(Estado, "�", "U")
            registro &= Estado & "|"

            'Ciudad
            Ciudad = dr("Ciudad_Cliente")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "�", "E")
            Ciudad = Replace(Ciudad, "�", "I")
            Ciudad = Replace(Ciudad, "�", "O")
            Ciudad = Replace(Ciudad, "�", "U")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "Ñ", "�")
            Ciudad = Replace(Ciudad, "Ê", "U")
            Ciudad = Replace(Ciudad, "É", "E")
            registro &= Ciudad & "|"

            registro &= dr("Cp_Cliente") & "|"
            registro &= dr("Rfc") & "|"
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"

            registro &= dr("Telefono") & "|"
            registro &= dr("FNacimiento") & "|"
            registro &= dr("Email") & "|"

            'Determina si es seguro gratis de acuerdo al programa y al tipo de producto
            If dr("Id_Aseguradora") = "65" Then    'Conauto
                If dr("TipoProducto") = 1 Then
                    SeguroGratis = 1
                Else
                    SeguroGratis = 0
                End If
            End If


            If dr("Id_Aseguradora") = "64" Then  'Qualitas
                'registro &= "53496022" & "|"
                'HM 18/03/2014
                'registro &= "5349^5349^5349" & "|"
                registro &= "5432^5432^5432" & "|"
            ElseIf dr("Id_Aseguradora") = "65" Then  'Axa 
                If dr("SubRamo") = 1 Then   'Auto
                    If SeguroGratis = 1 Then  'Seguro Gratis
                        'Verifica si las polizas son cruzadas
                        StrSql = "SELECT * FROM Poliza WHERE Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                        StrSql = StrSql & "AND Id_Bid = '" & dr("Id_Bid") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Cobertura = '" & dr("Cobertura") & "'" & vbCrLf
                        StrSql = StrSql & "AND Modelo = '" & dr("Modelo") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Contador_Poliza <> '" & dr("Contador_Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND id_cancelacion is null" & vbCrLf
                        StrSql = StrSql & "Order by Contador_Poliza"
                        dtP = CConecta.AbrirConsulta(StrSql, True)

                        If dtP.Rows.Count > 0 Then
                            If dtP.Rows(i).Item("Id_Aseguradora") = "65" Then
                                bandAseguradora = 1
                            Else
                                bandAseguradora = 0
                            End If

                            If bandAseguradora = 0 Then
                                If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                                    registro &= "TA6111" & "|"
                                Else    'si poliza 2 = Axa
                                    registro &= "TA6111" & "|"
                                End If
                            Else    'Polizas de la misma aseguradora Axa
                                registro &= "TA6110" & "|"
                            End If
                        Else
                            registro &= dr("Tarifa") & "|"
                        End If

                    Else    'Multianual
                        registro &= "TA6110" & "|"
                    End If
                Else    'Cami�n
                    If SeguroGratis = 1 Then  'Seguro Gratis
                        'Verifica si las polizas son cruzadas
                        StrSql = "SELECT * FROM Poliza WHERE Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                        StrSql = StrSql & "AND Id_Bid = '" & dr("Id_Bid") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Cobertura = '" & dr("Cobertura") & "'" & vbCrLf
                        StrSql = StrSql & "AND Modelo = '" & dr("Modelo") & "'" & vbCrLf
                        'StrSql = StrSql & "AND Contador_Poliza <> '" & dr("Contador_Poliza") & "'" & vbCrLf
                        StrSql = StrSql & "AND id_cancelacion is null" & vbCrLf
                        StrSql = StrSql & "Order by Contador_Poliza"
                        dtP = CConecta.AbrirConsulta(StrSql, True)

                        If dtP.Rows.Count > 0 Then
                            If dtP.Rows(i).Item("Id_Aseguradora") = "65" Then
                                bandAseguradora = 1
                            Else
                                bandAseguradora = 0
                            End If

                            If bandAseguradora = 0 Then 'Poliza cruzada
                                If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                                    registro &= "TA5193" & "|"
                                Else    'si poliza 2 = Axa
                                    registro &= "TA5193" & "|"
                                End If
                            Else    'Polizas de la misma aseguradora Axa
                                registro &= "TA5192" & "|"
                            End If
                        Else
                            registro &= dr("Tarifa") & "|"
                        End If

                    Else    'Multianual
                        registro &= "TA5192" & "|"
                    End If
                End If
                'registro &= dr("Insco") & "|"
            End If
            registro &= dr("Clave_Agente") & "|"
            'Dato1
            registro &= "01" & "|"
            'Dato2
            registro &= "01" & "|"
            'Suma asegurada
            registro &= String.Format("{0:0.00}", dr("Precio_Vehiculo")) & "|"

            StrSql = "SELECT * FROM limresp WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
            StrSql = StrSql & "AND Id_Paquete = " & dr("Id_Paquete") & vbCrLf
            StrSql = StrSql & "Order by Id_Paquete "
            dtC = CConecta.AbrirConsulta(StrSql, True)

            Dim bandera As Integer = 0

            If dtC.Rows.Count = 0 Then
                'Suma rc
                registro &= "0" & "|"
                'Suma gm
                registro &= "0" & "|"
                'Ded dm
                registro &= "0" & "|"
                'Ded rt
                registro &= "0" & "|"
                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 64 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 65 Then   'Axa
                    registro &= "0" & "|"
                End If
                'registro &= "0" & "|"

                'Suma rc complementaria
                registro &= "0"
            Else
                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc
                    If dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma gm
                    If dtC.Rows(i).Item("Cobertura") = "GASTOS MEDICOS OCUPANTES" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded dm
                    If dtC.Rows(i).Item("Cobertura") = "DA�OS MATERIALES" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded rt
                    If dtC.Rows(i).Item("Cobertura") = "ROBO TOTAL" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 64 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 65 Then   'Axa
                    registro &= "0" & "|"
                End If
                'registro &= "0" & "|"

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc complementaria
                    If dtC.Rows(i).Item("Cobertura") = "RC COMPLEMENTARIA" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

            End If

            'Plazo
            registro &= dr("Plazo") & "|"

            'Subramo
            registro &= dr("Subramo")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

    Private Function ArmaRegistroArrendadoraPolBajas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim i As Integer
        Dim Meses As Integer

        Try
            'Programa (31:Arrendadora)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 64 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 65 Then   'Axa
                registro &= "2" & "|"
            End If

            registro &= dr("Poliza") & "|"
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'Fecha de Cancelaci�n
            registro &= dr("Fecha_Cancelacion") & "|"
            'Descripci�n de Cancelaci�n
            registro &= dr("Descripcion_Cancelacion")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

#End Region

#Region "Genera layout Mazda"

    Private Function ArmaRegistroMazdaPolAltas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = ""
        Dim dtC As DataTable
        Dim dtP As DataTable
        Dim dtB As DataTable
        Dim i As Integer
        Dim Meses As Integer
        Dim Estado As String
        Dim Ciudad As String
        Dim bandAseguradora As Integer
        Dim SeguroGratis As Integer = 0

        Try
            'Programa (32:Mazda)
            registro &= IdPrograma & "|"

            If IdPrograma = 32 Then 'Mazda
                If dr("Id_Aseguradora") = 66 Then   'Qualitas
                    registro &= "3" & "|"
                ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                    registro &= "4" & "|"
                ElseIf dr("Id_Aseguradora") = 67 Then   'Axa
                    registro &= "2" & "|"
                End If
            End If

            registro &= dr("Poliza") & "|"

            'Numero poliza anterior
            registro &= "" & "|"
            'Numero de vigencia
            registro &= "" & "|"
            'Numero de endoso
            registro &= dr("endoso") & "|"
            'Numero de termino
            registro &= "" & "|"
            'Numero de vigencia termino
            registro &= "" & "|"
            'punto de venta
            registro &= "" & "|"
            'Referencia (Bid, Grupo e Integrante)
            If Len(dr("Num_Contrato")) = 9 Then
                registro &= "0" & dr("Num_Contrato") & "|"
            Else
                registro &= dr("Num_Contrato") & "|"
            End If
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'N�mero de pagos
            registro &= dr("Num_Pago") & "|"
            'Clave forma de pago
            'HM 18/03/2014 Plazo de 12 meses = 0 (Contado) y Plazo diferente a 12 meses = 360 (Anual)
            'Forma de Pago
            If dr("Id_Aseguradora") = 66 Then   'Qualitas
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            ElseIf dr("Id_Aseguradora") = 67 Then   'Axa
                If dr("Plazo") = 12 Then
                    registro &= "0" & "|"
                Else
                    registro &= "360" & "|"
                End If
            End If

            'Clave de Producto
            registro &= "FNVO" & "|"
            'Prima Neta
            registro &= String.Format("{0:0.00}", dr("PrimaNeta")) & "|"
            'Descuentos coberturas
            registro &= "0" & "|"
            'Recargos coberturas
            registro &= "0" & "|"
            'Derechos de p�liza
            registro &= String.Format("{0:0.00}", dr("DerPol")) & "|"
            'Tasa de financiamiento (Axa, Qualitas, Gnp)
            registro &= String.Format("{0:0.00}", dr("Pago_Fraccionado")) & "|"
            'Total descuentos generales
            registro &= String.Format("{0:0.00}", dr("Descuentos")) & "|"
            'IVA
            registro &= String.Format("{0:0.00}", dr("Iva")) & "|"
            'Recargos generales
            registro &= String.Format("{0:0.00}", dr("Recargos")) & "|"
            'Prima total
            registro &= String.Format("{0:0.00}", dr("PrimaTotal")) & "|"
            'Sesion Comision
            registro &= String.Format("{0:0.00}", dr("SesionComision")) & "|"
            'Comision
            registro &= String.Format("{0:0.00}", dr("Comision")) & "|"
            'Prima neta primer recibo
            registro &= "0" & "|"
            'Prima neta recibos subsecuentes
            registro &= "0" & "|"
            'Prorrateo del derecho
            registro &= "0" & "|"
            'C�ratula
            registro &= "0" & "|"
            'Contrato
            registro &= "0" & "|"
            'Tipo de p�liza
            registro &= "0" & "|"
            'Datos del contratante
            registro &= "" & "|"
            'Beneficiario preferente
            registro &= dr("Benef_Pref") & "|"
            'Clave del agente
            registro &= dr("Clave_Agente") & "|"
            'Moneda
            registro &= "NACIONAL" & "|"

            'Determina si es seguro gratis de acuerdo al programa y al tipo de producto
            If dr("Id_Aseguradora") = "67" Then
                If dr("TipoProducto") = 1 Then
                    SeguroGratis = 1
                Else
                    SeguroGratis = 0
                End If
            End If

            'Clave tarifa
            If dr("Id_Aseguradora") = "66" Then  'Qualitas
                registro &= "5432^5432^5432" & "|"
            ElseIf dr("Id_Aseguradora") = "68" Then  'Hdi
                If IsDBNull(dr("Tarifa")) Then
                    If dr("SubRamo") = 1 Then
                        registro &= "TA6111" & "|"
                    Else
                        registro &= "TA5193" & "|"
                    End If
                ElseIf dr("Tarifa") = "" Then
                    If dr("SubRamo") = 1 Then
                        registro &= "TA6111" & "|"
                    Else
                        registro &= "TA5193" & "|"
                    End If
                Else
                    registro &= dr("Tarifa") & "|"
                End If
            ElseIf dr("Id_Aseguradora") = "67" Then  'Axa 
                If IsDBNull(dr("Tarifa")) Then
                    registro &= "" & "|"
                Else
                    registro &= dr("Tarifa") & "|"
                End If
                'If dr("SubRamo") = 1 Then   'Auto
                '    'If dr("TipoProducto") = 1 Then
                '    'Seguro Gratis
                '    If dr("TipoProducto") = 0 Or dr("TipoProducto") = 1 Or _
                '    dr("TipoProducto") = 2 Or dr("TipoProducto") = 3 Or _
                '    dr("TipoProducto") = 8 Then
                '        'verifica si las polizas son cruzadas
                '        StrSql = "SELECT * FROM Poliza WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
                '        StrSql = StrSql & "AND Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                '        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                '        StrSql = StrSql & "Order by Contador_Poliza"
                '        dtP = CConecta.AbrirConsulta(StrSql, True)

                '        If dtP.Rows.Count = 0 Then
                '            If dtP.Rows(i).Item("Id_Aseguradora") = "61" Then
                '                bandAseguradora = 1
                '            Else
                '                bandAseguradora = 0
                '            End If
                '        End If

                '        If bandAseguradora = 0 Then
                '            If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                '                registro &= "TA6111" & "|"
                '            Else    'si poliza 2 = Axa
                '                registro &= "TA6111" & "|"
                '            End If
                '        Else    'Polizas de la misma aseguradora Axa
                '            registro &= "TA6111" & "|"
                '        End If
                '    Else    'Multianual
                '        registro &= "TA6110" & "|"
                '    End If
                'Else    'Cami�n
                '    'If dr("TipoProducto") = 1 Then  
                '    'Seguro Gratis
                '    If dr("TipoProducto") = 0 Or dr("TipoProducto") = 1 Or _
                '    dr("TipoProducto") = 2 Or dr("TipoProducto") = 3 Or _
                '    dr("TipoProducto") = 8 Then
                '        'verifica si las polizas son cruzadas
                '        StrSql = "SELECT * FROM Poliza WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
                '        StrSql = StrSql & "AND Poliza <> '" & dr("Poliza") & "'" & vbCrLf
                '        StrSql = StrSql & "AND Serie = '" & dr("Serie") & "'" & vbCrLf
                '        StrSql = StrSql & "Order by Contador_Poliza"
                '        dtP = CConecta.AbrirConsulta(StrSql, True)

                '        If dtP.Rows.Count = 0 Then
                '            If dtP.Rows(i).Item("Id_Aseguradora") = "61" Then
                '                bandAseguradora = 1
                '            Else
                '                bandAseguradora = 0
                '            End If
                '        End If

                '        If bandAseguradora = 0 Then
                '            If dr("Contador_poliza") = "0" Then 'si poliza 1 = Axa
                '                registro &= "TA5193" & "|"
                '            Else    'si poliza 2 = Axa
                '                registro &= "TA5193" & "|"
                '            End If
                '        Else    'Polizas de la misma aseguradora Axa
                '            registro &= "TA5193" & "|"
                '        End If
                '    Else    'Multianual
                '        registro &= "TA5192" & "|"
                '    End If
                'End If
                'registro &= dr("Insco") & "|"
            End If

            'Observaciones
            registro &= "" & "|"
            'Nombre
            If dr("Per_Fiscal") = 2 Then
                registro &= dr("Razon_Social") & "|||"
            Else
                registro &= dr("Nombre") & "|"
                registro &= dr("APaterno") & "|"
                registro &= dr("AMaterno") & "|"
            End If
            'Domicilio
            registro &= dr("Calle_Cliente") & "|"
            'No. Interior
            registro &= dr("NoInterior") & "|"
            'No. Exterior
            registro &= dr("NoExterior") & "|"
            'Colonia
            registro &= dr("Colonia_Cliente") & "|"
            'Estado
            Estado = dr("Estado_Cliente")
            Estado = Replace(Estado, "�", "A")
            Estado = Replace(Estado, "�", "E")
            Estado = Replace(Estado, "�", "I")
            Estado = Replace(Estado, "�", "O")
            Estado = Replace(Estado, "�", "U")
            registro &= Estado & "|"

            'Ciudad
            Ciudad = dr("Ciudad_Cliente")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "�", "E")
            Ciudad = Replace(Ciudad, "�", "I")
            Ciudad = Replace(Ciudad, "�", "O")
            Ciudad = Replace(Ciudad, "�", "U")
            Ciudad = Replace(Ciudad, "�", "A")
            Ciudad = Replace(Ciudad, "Ñ", "�")
            Ciudad = Replace(Ciudad, "Ê", "U")
            Ciudad = Replace(Ciudad, "É", "E")
            registro &= Ciudad & "|"

            'C�digo postal
            registro &= dr("Cp_Cliente") & "|"
            'Tel�fono
            registro &= dr("Telefono") & "|"
            'RFC Titular
            registro &= dr("Rfc") & "|"
            'Tipo de Persona
            registro &= IIf(dr("Per_Fiscal") = 2, 2, 1) & "|"
            'Fecha de nacimiento
            registro &= dr("FNacimiento") & "|"
            'Correo
            registro &= dr("Email") & "|"
            'Estado civil
            registro &= dr("Estado_Civil") & "|"
            'Sexo
            registro &= dr("Sexo") & "|"
            'RFC Conductor
            registro &= "" & "|"
            'Kilometros recorridos del vehiculo
            registro &= "" & "|"
            'Marca del vehiculo
            registro &= dr("Marca") & "|"
            'Modelo del vehiculo
            registro &= dr("Anio") & "|"
            'Descripcion del vehiculo
            registro &= dr("Descripcion") & "|"
            'Capacidad del vehiculo
            registro &= dr("Capacidad") & "|"
            'Placas
            registro &= dr("Placas") & "|"
            'N�mero de motor
            registro &= dr("Motor") & "|"
            'N�mero de serie
            registro &= dr("Serie") & "|"
            'Transmisi�n
            registro &= "" & "|"
            'RFV del auto
            registro &= "" & "|"
            'Suma asegurada
            registro &= String.Format("{0:0.00}", dr("Precio_Vehiculo")) & "|"
            'Clave aseguradora
            registro &= dr("Insco") & "|"
            'Clase del vehiculo
            If dr("SubRamo") = 1 Then
                registro &= "2" & "|"
            Else
                registro &= "4" & "|"
            End If
            'Servicio
            If dr("SubRamo") = 1 Then
                registro &= "A" & "|"
            Else
                If SeguroGratis = 1 Then
                    registro &= "A" & "|"
                Else
                    registro &= "P" & "|"
                End If
            End If
            'Uso
            registro &= dr("uso") & "|"
            'Tipo de carga

            'AXA
            If dr("Id_Aseguradora") = 67 Then
                If dr("id_uso") = 1 Then
                    registro &= "" & "|"
                Else
                    'evi 18/12/2014 SG1 y SG2 no lleva carga
                    If dr("tipoproducto") = 0 Then
                        registro &= "" & "|"
                    Else
                        registro &= "B" & "|"
                    End If
                End If
            ElseIf dr("Id_Aseguradora") = 66 Then   'Qualitas
                If dr("tipoproducto") = 0 Then
                    registro &= "" & "|"
                Else
                    If dr("SubRamo") = 1 Then
                        registro &= "" & "|"
                    Else
                        registro &= "B" & "|"
                    End If
                End If
            ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                'evi 18/12/2014 SG1 y SG2 no lleva carga
                If dr("tipoproducto") = 0 Then
                    registro &= "" & "|"
                Else
                    If dr("subramo") >= 2 Then
                        registro &= "B" & "|"
                    Else
                        registro &= "" & "|"
                    End If
                End If
            End If

            'Tonelaje
            registro &= "" & "|"
            'l�nea de vehiculo
            registro &= "" & "|"
            'Origen
            registro &= "" & "|"
            'Paquete
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Equipo especial
            registro &= "" & "|"
            'Estado circulaci�n
            registro &= "" & "|"
            'Tipo de emisi�n
            registro &= "" & "|"
            'Fecha de cancelacion
            registro &= "" & "|"

            registro &= dr("Renave") & "|"
            registro &= "01" & "|"
            registro &= "01" & "|"

            StrSql = "SELECT * FROM limresp WHERE Id_Aseguradora = " & dr("Id_Aseguradora") & vbCrLf
            StrSql = StrSql & "AND Id_Paquete = " & dr("Id_Paquete") & vbCrLf
            StrSql = StrSql & "Order by Id_Paquete "
            dtC = CConecta.AbrirConsulta(StrSql, True)

            Dim bandera As Integer = 0

            If dtC.Rows.Count = 0 Then
                'Suma rc
                registro &= "0" & "|"
                'Suma gm
                registro &= "0" & "|"
                'Ded dm
                registro &= "0" & "|"
                'Ded rt
                registro &= "0" & "|"
                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 66 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                    registro &= "0" & "|"
                ElseIf dr("Id_Aseguradora") = 67 Then   'Axa
                    registro &= "0" & "|"
                End If

                'Suma rc complementaria
                registro &= "0"
            Else
                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc
                    If dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL" Or dtC.Rows(i).Item("Cobertura") = "RESPONSABILIDAD CIVIL (LUC)" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma gm
                    If dtC.Rows(i).Item("Cobertura") = "GASTOS MEDICOS OCUPANTES" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded dm
                    If dtC.Rows(i).Item("Cobertura") = "DA�OS MATERIALES" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Ded rt
                    If dtC.Rows(i).Item("Cobertura") = "ROBO TOTAL" Then
                        registro &= dtC.Rows(i).Item("Deducible") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Suma acc a ocup
                'HMH 23/01/2014
                If dr("Id_Aseguradora") = 66 Then   'Qualitas
                    registro &= "50000" & "|"
                ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                    registro &= "0" & "|"
                ElseIf dr("Id_Aseguradora") = 67 Then   'Axa
                    registro &= "0" & "|"
                End If

                bandera = 0
                For i = 0 To dtC.Rows.Count - 1
                    'Suma rc complementaria
                    If dtC.Rows(i).Item("Cobertura") = "RC COMPLEMENTARIA" Then
                        registro &= dtC.Rows(i).Item("Descripcion_Suma") & "|"
                        bandera = 1
                        Exit For
                    End If
                Next
                If bandera = 0 Then
                    registro &= "0" & "|"
                End If

                'Plazo
                registro &= dr("Plazo") & "|"

                'Subramo
                registro &= dr("Subramo") & "|"

                'TipoProducto
                registro &= dr("TipoProducto") & "|"

                'Contador Poliza
                registro &= dr("Contador_Poliza") & "|"

                'Sexo
                registro &= dr("Sexo") & "|"

                'Estado_Civil
                registro &= dr("Estado_Civil") & "|"

                'Marca
                registro &= dr("Marca") & "|"

                'Forma de Pago
                registro &= dr("Id_TipoRecargo") & "|"

                'Caratula
                registro &= dr("Caratula") & "|"

                'Contrato
                registro &= dr("Contrato") & "|"

                StrSql = "SELECT * FROM Bid WHERE Id_Bid = " & dr("Id_Bid")
                dtB = CConecta.AbrirConsulta(StrSql, True)

                'BidAon
                registro &= dtB.Rows(0).Item("Bid_Aon") & "|"

                'Empresa
                registro &= dtB.Rows(0).Item("Empresa")

            End If

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

    Private Function ArmaRegistroMazdaPolBajas(ByVal dr As DataRow, ByVal IdPrograma As Integer) As String
        Dim registro As String = String.Empty
        Dim dtC As DataTable
        Dim i As Integer
        Dim Meses As Integer

        Try
            'Programa (32:Mazda)
            registro &= IdPrograma & "|"

            If dr("Id_Aseguradora") = 66 Then   'Qualitas
                registro &= "3" & "|"
            ElseIf dr("Id_Aseguradora") = 68 Then   'Hdi
                registro &= "1" & "|"
            ElseIf dr("Id_Aseguradora") = 67 Then   'Axa
                registro &= "2" & "|"
            End If

            registro &= dr("Poliza") & "|"
            'Fecha de Inicio de Vigencia
            registro &= dr("Vigencia_FIniP") & "|"
            'Fecha de Fin de Vigencia
            registro &= dr("Vigencia_FFinp") & "|"
            'Fecha de Emisi�n
            registro &= dr("Fecha_Registro") & "|"
            'Fecha de Cancelaci�n
            registro &= dr("Fecha_Cancelacion") & "|"
            'Descripci�n de Cancelaci�n
            registro &= dr("Descripcion_Cancelacion")

        Catch ex As Exception
            registro = "error: " + ex.Message
        End Try
        Return registro
    End Function

#End Region

    Private Function UpdateString(ByVal aIdPoliza As ArrayList, ByVal TipoLayout As Integer, ByVal Baninterface As Integer) As String
        Dim strUpdate As String = String.Empty

        If aIdPoliza.Count > 0 Then
            Dim IdPoliza As Long

            If TipoLayout = 1 Then
                strUpdate = "UPDATE POLIZA SET Interface = " & Baninterface & " " & _
                            "WHERE Interface = 0 " & _
                            "AND Id_Poliza IN ( 0"

            ElseIf TipoLayout = 2 Then  'P�lizas canceladas
                strUpdate = "UPDATE POLIZA SET Interface_Cancelacion = " & Baninterface & " " & _
                            "WHERE Interface_Cancelacion = 0 " & _
                            "AND Id_Poliza IN ( 0"
            End If

            For Each IdPoliza In aIdPoliza
                strUpdate &= "," & IdPoliza
            Next
            strUpdate &= ")"

        End If

        Return strUpdate
    End Function

    Public Function GetInterfaceSAARS(ByVal dtFecha As DateTime, ByVal iTipo As Integer) As String
        Dim strSQL As String = String.Empty
        Dim StrSalida As String = String.Empty
        Dim Strlog As String = String.Empty

        Select Case iTipo
            Case 0
                strSQL = "select convert(varchar(5), pa.id_programa) + '|' /*IdPrograma*/ + case p.Id_Aseguradora when 59 Then '3' when 60 Then '1' when 61 then '2' when 62 Then '3' when 63 then '2' " & _
                    "when 64 Then '3' when 65 then '2' end + '|' + p.Poliza + '|||0||||' + " & _
                    "case when len(c.Num_Contrato) = 9 then '0' else '' end + c.Num_Contrato + '|' + convert(varchar(20), p.Vigencia_FIniP, 103) + '|' + convert(varchar, p.Vigencia_FFinp, 103) + '|' + " & _
                    "convert(varchar, p.Fecha_Registro, 103) + '|' + convert(varchar, p.Num_Pago) + '|' + case p.Plazo when 12 then '0' else '360' end + '|FNVO|' + " & _
                    "convert(varchar, cast(p.PrimaNeta as decimal(18,2))) + '|0|0|' + convert(varchar, cast(p.DerPol as decimal(18,2))) + '|' + " & _
                    "convert(varchar, cast(p.Pago_Fraccionado as decimal(18,2))) + '|0|' + convert(varchar, cast(p.Iva as decimal(18,2))) + '|' + " & _
                    "convert(varchar, cast(p.Recargos as decimal(18,2))) + '|' + convert(varchar, cast(p.PrimaTotal as decimal(18,2))) + '|0|0|' + " & _
                    "'0|0|0|0|0|0||' + p.Benef_Pref + '|' + isnull(p.Clave_Agente,'') + '|NACIONAL|' + " & _
                    "case p.Id_Aseguradora when 59 then '5432^5432^5432' when 60 then case when ISNULL(p.TARIFA, '') = '' then case WHEN pq.SubRamo = '1' then 'TA6111' " & _
                    "ELSE 'TA5193' END ELSE P.TARIFA END WHEN 61 THEN case when ISNULL(p.TARIFA, '') = '' then '' ELSE P.TARIFA END " & _
                    "when 62 then '5432^5432^5432' when 63 then case pq.SubRamo when 1 then case when p.TipoProducto in (0, 1, 2, 3, 8) Then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND p.id_cancelacion is null) then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND pa.id_cancelacion is null and pa.Id_Aseguradora = 63) then 'TB4103' else 'TA6111' end " & _
                    "else ISNULL(p.TARIFA, '') end else 'TB4103' End else case when p.TipoProducto in (0, 1, 2, 3, 8) Then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND p.id_cancelacion is null) then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND pa.id_cancelacion is null and pa.Id_Aseguradora = 63) then '5192' else '5193' end " & _
                    "else ISNULL(p.TARIFA, '') end else 'TA5192' End End " & _
                    "when 64 then '5432^5432^5432' when 65 then case pq.SubRamo when 1 then case when p.TipoProducto in (0, 1, 2, 3, 8) Then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND p.id_cancelacion is null) then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND pa.id_cancelacion is null and pa.Id_Aseguradora = 63) then 'TB4103' else 'TA6111' end " & _
                    "else ISNULL(p.TARIFA, '') end else 'TB4103' End else case when p.TipoProducto in (0, 1, 2, 3, 8) Then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND p.id_cancelacion is null) then " & _
                    "case when exists(SELECT 1 FROM Poliza pa WHERE pa.Poliza = p.poliza AND pa.Serie = p.serie AND pa.Id_Bid = p.id_bid AND pa.Cobertura = p.cobertura " & _
                    "AND pa.Modelo = p.modelo AND pa.Contador_Poliza <>  p.Contador_Poliza AND pa.id_cancelacion is null and pa.Id_Aseguradora = 63) then '5192' else '5193' end " & _
                    "else ISNULL(p.TARIFA, '') end else 'TA5192' End End end + '||' + " & _
                    "CASE CL.Per_Fiscal WHEN 2 THEN CL.Razon_Social + '|||' ELSE CL.Nombre + '|' + CL.APaterno + '|' + CL.AMaterno + '|' END + " & _
                    "cl.Calle_Cliente + ' ' + cl.NoExterior + ' ' + cl.NoInterior + '|' + cl.Colonia_Cliente + '|' + " & _
                    "replace(replace(replace(replace(replace(cl.Estado_Cliente, '�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U') + '|' + " & _
                    "replace(replace(replace(replace(replace(replace(replace(replace(replace(cl.Ciudad_Cliente, " & _
                    "'�', 'A'), '�', 'E'), '�', 'I'), '�', 'O'), '�', 'U'), '�', 'A'), 'Ñ', '�'), 'Ê', 'U'), 'É', 'E') + '|' + " & _
                    "ISNULL(cl.Cp_Cliente, '') + '|' + ISNULL(cl.Telefono, '') + '|' + ISNULL(cl.Rfc, '') + '|' + case cl.per_fiscal when 2 then '2' else '1' end + '|' + " & _
                    "convert(varchar, cl.FNacimiento, 103) + '|' + ISNULL(cl.Email, '') + '|' + ISNULL(cl.Estado_Civil, '') + '|' + ISNULL(cl.Sexo, '') + '|||' + p.Marca + '|' + " & _
                    "p.Anio + '|' + p.Descripcion + '|' + p.Capacidad + '|' + ISNULL(p.Placas, '') + '|' + ISNULL(p.Motor, '') + '|' + " & _
                    "p.Serie + '|||' + convert(varchar, cast(p.Precio_Vehiculo as decimal(18,2))) + '|' + p.Insco + '|' + case pq.SubRamo when 1 then '2' else '4' end + '|' + " & _
                    "case pq.subramo when 1 then 'A' else case when p.Id_Aseguradora = 61 and p.TipoProducto = 1 then 'A' ELSE 'P' END END + '|01|' + " & _
                    "CASE P.ID_ASEGURADORA WHEN 61 THEN case p.Id_Uso when 1 then '' else case p.TipoProducto when 0 then '' else 'B' end end " & _
                    "ELSE  case p.Id_Aseguradora when 59 then case p.TipoProducto when 0 then '' else case pq.SubRamo when 1 then '' else 'B' end end " & _
                    "else case TipoProducto when 0 then '' else case when pq.SubRamo >= 2 then 'B' else '' end end end END + '|' + '|||||||||||||' + ISNULL(P.Renave, '') + '|01|01|' + " & _
                    "case when NOT EXISTS(SELECT 1 FROM limresp WHERE Id_Aseguradora = P.ID_ASEGURADORA AND Id_Paquete = pq.Id_Paquete) then '0|0|0|0|' + " & _
                    "case p.Id_Aseguradora when 59 then '50000' else '0' end +'|0' else " & _
                    "CASE WHEN EXISTS(SELECT 1 FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('RESPONSABILIDAD CIVIL', 'RESPONSABILIDAD CIVIL (LUC)')) THEN " & _
                    "(SELECT TOP 1 Descripcion_Suma FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('RESPONSABILIDAD CIVIL', 'RESPONSABILIDAD CIVIL (LUC)') order by l.Id_Paquete) ELSE '0' END + '|' + " & _
                    "CASE WHEN EXISTS(SELECT 1 FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete and l.Cobertura IN ('GASTOS MEDICOS OCUPANTES')) THEN " & _
                    "(SELECT TOP 1 Descripcion_Suma FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('GASTOS MEDICOS OCUPANTES') order by l.Id_Paquete) ELSE '0' END + '|' + " & _
                    "CASE WHEN EXISTS(SELECT 1 FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete and l.Cobertura IN ('DA�OS MATERIALES')) THEN " & _
                    "(SELECT TOP 1 Deducible FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('DA�OS MATERIALES') order by l.Id_Paquete) ELSE '0' END + '|' + " & _
                    "CASE WHEN EXISTS(SELECT 1 FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete and l.Cobertura IN ('ROBO TOTAL')) THEN " & _
                    "(SELECT TOP 1 Deducible FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('ROBO TOTAL') order by l.Id_Paquete) ELSE '0' END + '|' + case p.Id_Aseguradora when 59 then '50000' else '0' end + '|' + " & _
                    "CASE WHEN EXISTS(SELECT 1 FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete and l.Cobertura IN ('RC COMPLEMENTARIA')) THEN " & _
                    "(SELECT TOP 1 Descripcion_Suma FROM limresp l WHERE l.Id_Aseguradora = P.ID_ASEGURADORA AND l.Id_Paquete = pq.Id_Paquete " & _
                    "and l.Cobertura IN ('RC COMPLEMENTARIA') order by l.Id_Paquete) ELSE '0' END + '|' + " & _
                    "p.Plazo + '|' + pq.SubRamo + '|' + CONVERT(VARCHAR, p.TipoProducto) + '|' + ISNULL(CONVERT(VARCHAR, p.Contador_Poliza), '') + '|' + " & _
                    "ISNULL(p.Sexo, '') + '|' + ISNULL(cl.Estado_Civil, '') +'|' + p.Marca + '|' + CONVERT(VARCHAR, P.Id_TipoRecargo) + '|' + P.Caratula + '|' + P.Contrato End AS Cadena " & _
                    "FROM CLIENTE_POLIZA cp INNER JOIN CONTRATO c ON cp.Id_Contrato = c.Id_Contrato AND cp.Id_Cliente = c.Id_cliente AND cp.Id_Bid = c.Id_Bid " & _
                    "INNER JOIN POLIZA p ON cp.Id_Poliza = p.Id_Poliza AND cp.Id_Bid = p.Id_Bid INNER Join ABC_PROGRAMA_ASEGURADORA pa ON p.id_aseguradora = pa.id_aseguradora " & _
                    "INNER JOIN ABC_TIPORECARGO r ON p.Id_TipoRecargo = r.Id_TipoRecargo INNER JOIN CLIENTE cl ON cp.Id_Cliente = cl.Id_Cliente AND cp.Id_Bid = cl.Id_Bid " & _
                    "INNER JOIN ABC_PAQUETE pq ON p.Id_Aseguradora = pq.Id_Aseguradora AND p.Id_Paquete = pq.Id_Paquete LEFT OUTER JOIN ABC_CANCELACION cc ON p.Id_Cancelacion = cc.Id_Cancelacion " & _
                    "LEFT OUTER JOIN ABC_TIPOCARGA t ON p.Id_TipoCarga = t.Id_TipoCarga WHERE convert(date, p.Fecha_Registro) = '" & dtFecha.ToString("yyyy-MM-dd") & "' " & _
                    "and p.Id_Aseguradora in (59, 60, 61, 62, 63, 64, 65)"

                StrSalida = "Interfases ConautoPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                Strlog = "Interfases ConautoPolAlta_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
            Case 1
                strSQL = "select convert(varchar, A.Id_Programa) + '|' + case p.Id_Aseguradora when 59 Then '3' when 60 then '1' when 61 then '2' when 62 then '3' when 63 then '2' " & _
                    "when 64 then '3' when 65 then '2' end + '|' + p.poliza + '|' + convert(varchar, p.Vigencia_FIniP, 103) + '|' + convert(varchar, p.Vigencia_FFinp, 103) + '|' + " & _
                    "convert(varchar, p.Fecha_REGISTRO, 103) + '|' + convert(varchar, p.Fecha_Cancelacion, 103) + '|' + cc.Descripcion_Cancelacion as Cadena FROM FEC.CLIENTE_POLIZA cp " & _
                    "INNER JOIN FEC.CONTRATO c ON cp.Id_Contrato = c.Id_Contrato AND cp.Id_Cliente = c.Id_cliente AND cp.Id_Bid = c.Id_Bid " & _
                    "INNER JOIN FEC.POLIZA p ON cp.Id_Poliza = p.Id_Poliza AND cp.Id_Bid = p.Id_Bid " & _
                    "INNER JOIN FEC.ABC_TIPORECARGO tr ON p.Id_TipoRecargo = tr.Id_TipoRecargo " & _
                    "INNER JOIN FEC.CLIENTE cl ON cp.Id_Cliente = cl.Id_Cliente AND cp.Id_Bid = cl.Id_Bid " & _
                    "INNER JOIN FEC.ABC_PAQUETE pq ON p.Id_Aseguradora = pq.Id_Aseguradora AND p.Id_Paquete = pq.Id_Paquete " & _
                    "LEFT OUTER JOIN FEC.ABC_CANCELACION cc ON p.Id_Cancelacion = cc.Id_Cancelacion " & _
                    "LEFT OUTER JOIN FEC.ABC_TIPOCARGA tc ON p.Id_TipoCarga = tc.Id_TipoCarga INNER JOIN FEC.ABC_PROGRAMA_ASEGURADORA A ON P.Id_Aseguradora = A.Id_Aseguradora " & _
                    "where convert(date, p.Fecha_Cancelacion) = '" & dtFecha.ToString("yyyy-MM-dd") & "' and p.Id_Aseguradora in (59,60,61, 62, 63, 64, 65) "

                StrSalida = "Interfases ConautoPolBaja_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
                Strlog = "Interfases ConautoPolBaja_" & Now.Date.Day & "_" & Now.Date.Month & "_" & Now.Date.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & "_" & Date.Now.Second
        End Select




        Dim swFile As New StreamWriter(System.Configuration.ConfigurationManager.AppSettings("Script").ToString() & StrSalida & ".txt")
        Dim swErrLog As New StreamWriter(System.Configuration.ConfigurationManager.AppSettings("ArchivosLog").ToString() & Strlog & ".log")
        Dim dt As DataTable = CConecta.AbrirConsulta(strSQL, True)
        Dim dr As DataRow
        For Each dr In dt.Rows
            swFile.WriteLine(dr("Cadena").ToString())
        Next

        'No ha informaci�n
        If dt.Rows.Count = 0 Then
            StrSalida = ""
        End If

        swFile.Close()
        swErrLog.Close()

        GetInterfaceSAARS = StrSalida
    End Function
End Class
