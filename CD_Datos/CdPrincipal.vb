Imports CD_Datos.CdSql
Public Class CdPrincipal
    Private StrSql As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New CdSql

    Public Sub New()
    End Sub

    Public Function actualiza_valor(ByVal a As Integer, ByVal b As Integer, ByVal c As Integer, ByVal d As Integer, ByVal id As Integer, ByVal intid As Integer)

        Try
            StrTransaccion = "actualiza_valor"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            strsql = " update ALG_1 set a=" & a & ", b=" & b & ", C=" & c & ", d=" & d & ", band=1 WHERE id=" & intid
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ordena_algoritmo() As DataTable
        
        Try
            strsql = "SELECT TOP 1 ID FROM ALG_1 WHERE BAND=1 ORDER BY ID DESC"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function TraerMaxalgoritmo(ByVal intid As Integer) As DataTable
        
        Try
            strsql = "select * from alg_1 where id =" & intid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function poliza_valor(ByVal intpoliza As Integer) As DataTable
        
        Try
            strsql = "select poliza from poliza where id_poliza =" & intpoliza & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function maximo_valor() As DataTable
        
        Try
            strsql = "select top 1 * from alg_1 order by id desc"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Login_acceso(ByVal strlogin As String, ByVal strcontrasena As String) As DataTable

        Try
            StrSql = " SELECT distinct * FROM VW_LOGIN_ACCESO"
            strsql = strsql & " WHERE Login = '" & strlogin & "'"
            StrSql = StrSql & " AND Contrasena='" & strcontrasena & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Login_acceso_admin(ByVal intusuario As Integer, ByVal intbid As Integer) As DataTable
       
        Try
            StrSql = " SELECT *, 0 id_empleado FROM VW_LOGIN_ACCESO"
            strsql = strsql & " WHERE id_usuario =" & intusuario & ""
            strsql = strsql & " and id_bid =" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function registra_accesos_negados(ByVal strlogin As String) As DataTable
       
        Try
            StrSql = " select usuario = count(id_usuario) from usuario where login='" & strlogin & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Update_Usuario_Bloqueado(ByVal strlogin As String)

        Try
            StrTransaccion = "UpdateUsuarioBloqueado"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            strsql = " update usuario set Bandera_Registro='2'"
            strsql = strsql & " where login='" & strlogin & "'"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function menu_portada(ByVal strtipo As Char, ByVal intcontador As Integer, ByVal intprograma As Integer, ByVal intnivel As Integer, ByVal iIdNivel As Integer) As DataTable

        Try
            StrSql = " select id_portada, nombre_indice, url_indice, url_web, tipo, target"
            StrSql = StrSql & " from menu_portada"
            StrSql = StrSql & " where estatus_indice = 1"
            If intnivel <> 5 Then
                If strtipo = "E" Then
                    StrSql = StrSql & " and tipo <> 'A'"
                End If
                If intcontador <= 1 Then
                    StrSql = StrSql & " and not id_portada = 9"
                End If

                If intprograma = 30 Or intprograma = 31 Then
                    StrSql = StrSql & " and not id_portada = 2"
                End If

                If intnivel = 2 Then
                    StrSql = StrSql & " and not id_portada = 4"
                    'StrSql = StrSql & " and id_portada in(1,2,3,5,6,8,9,10)"
                End If
            Else
                StrSql = StrSql & " and id_portada in(1,2,3,4,5,6,8,10)"
            End If
            StrSql = StrSql & " and (SoloNivel is null or CHARINDEX(SoloNivel, '" & iIdNivel & "') > 0)"
            StrSql = StrSql & " order by orden_indice"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function PermisosMenu(ByVal iNivel As Integer, ByVal iPantalla As Integer) As DataTable
        Try
            StrSql = " select id_portada, nombre_indice, SoloNivel"
            StrSql = StrSql & " from menu_portada"
            StrSql = StrSql & " where (SoloNivel is null or CHARINDEX(SoloNivel, '" & iNivel & "') > 0)"
            StrSql = StrSql & " and id_portada = " & iPantalla.ToString()
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function poliza_inicio() As DataTable

        Try
            StrSql = "select poliza from parametro"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_version(ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select distinct id_version "
            StrSql = StrSql & " from version v, ABC_PROGRAMA_ASEGURADORA p"
            StrSql = StrSql & " where v.id_aseguradora = p.id_aseguradora"
            StrSql = StrSql & " and p.id_programa = " & intprograma & ""
            StrSql = StrSql & " and p.estatus_programaaseguradora ='1'"
            StrSql = StrSql & " and v.estatus_vigencia='1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function creando_cp(ByVal scp As String) As DataTable

        Try
            StrSql = "SELECT * FROM ABC_COLONIA WHERE cp = '" & scp & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function cargando_cp(ByVal scp As String) As DataTable

        Try
            StrSql = "select id_municipio = 0, nombre_colonia ='-- Seleccione --' union"
            StrSql = StrSql & " SELECT id_municipio, nombre_colonia "
            StrSql = StrSql & " FROM ABC_COLONIA"
            StrSql = StrSql & " WHERE cp = '" & scp & "'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_estado(ByVal intestado As Integer) As DataTable

        Try
            StrSql = " SELECT * FROM ABC_ESTADO WHERE id_ESTADO = " & intestado & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_ciudad(ByVal intciuadad As Integer, ByVal intestado As Integer) As DataTable

        Try
            StrSql = " SELECT * FROM ABC_CIUDAD WHERE id_estado= " & intestado & " AND id_CIUDAD = " & intciuadad & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_login(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = " select id_bid from USUARIO_BID"
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_bid_admin(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = "select id_bid, empresa "
            StrSql = StrSql & " from bid"
            StrSql = StrSql & " where id_bid in ("
            StrSql = StrSql & " Select id_bid"
            StrSql = StrSql & " from VW_LOGIN_ACCESO"
            StrSql = StrSql & " where id_usuario =" & intusuario & ")"
            StrSql = StrSql & " and estatus='1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_marca_admin(ByVal intusuario As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = " select id_marca = 0, marca='-- Seleccione --' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select distinct id_marca, marca"
            StrSql = StrSql & " from VW_LOGIN_ACCESO"
            StrSql = StrSql & " where id_usuario =" & intusuario & ""
            StrSql = StrSql & " and id_programa =" & intprograma & ""
            StrSql = StrSql & " order by id_marca"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_distribuidor_admin(ByVal intusuario As Integer, ByVal intmarca As Integer, ByVal intprograma As Integer) As DataTable

        Try
            'StrSql = " select id_bid = 0, empresa='-- Seleccione --', nombre='' "
            'StrSql = StrSql & " union"
            ''strsql = strsql & " select distinct id_bid, empresa = isnull(bid_ford,'0')+'- ' + isnull(empresa,''), nombre =empresa "
            'StrSql = StrSql & " select distinct id_bid, empresa , nombre =empresa "
            'StrSql = StrSql & " from bid where id_bid in ("
            'StrSql = StrSql & " select distinct id_bid "
            'StrSql = StrSql & " from VW_LOGIN_ACCESO"
            'StrSql = StrSql & " where id_usuario =" & intusuario & ""
            'StrSql = StrSql & " and id_marca =" & intmarca & ""
            'StrSql = StrSql & " and id_programa =" & intprograma & ")"
            'StrSql = StrSql & " order by nombre"

            StrSql = " select id_bid = 0, empresa='-- Seleccione --', nombre='' "
            StrSql = StrSql & " union"
            'strsql = strsql & " select distinct id_bid, empresa = isnull(bid_ford,'0')+'- ' + isnull(empresa,''), nombre =empresa "
            StrSql = StrSql & " select distinct id_bid, "
            StrSql = StrSql & " empresa =  LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))), "
            StrSql = StrSql & " nombre = LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))) "
            StrSql = StrSql & " from bid where id_bid in ("
            StrSql = StrSql & " select distinct id_bid "
            StrSql = StrSql & " from VW_LOGIN_ACCESO"
            StrSql = StrSql & " where id_usuario =" & intusuario & ""
            StrSql = StrSql & " and id_marca =" & intmarca & ""
            StrSql = StrSql & " and id_programa =" & intprograma & ")"
            StrSql = StrSql & " order by nombre"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_contenedor_reportes(ByVal strcadena As String) As String
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim valorInicial As String = ""
        Dim arr() As String = strcadena.Split(",")
        Dim i As Integer = 0

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select clave = isnull(max(cve_reporte),0) + 1 from contenedor"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    valor = ds.Tables(0).Rows(0)("clave")
            'End If

            StrTransaccion = "InsertaContReportes"

            For i = 0 To arr.Length - 1
                If i = 0 Then
                    StrSql = "set dateformat dmy; " & vbCrLf
                    StrSql += " declare @idInsRep table(id int); "
                    StrSql = StrSql & " insert into contenedor (cve_reporte, estatus, id_programa)" & vbCrLf
                    StrSql = StrSql & " OUTPUT inserted.cve_reporte into @idInsRep select (Select isnull(max(cve_reporte), 0 ) + 1 from contenedor With(NoLock)),'0'," & arr(i) & "; "
                    StrSql += "select * from @idInsRep; "
                Else
                    StrSql = "set dateformat dmy; " & vbCrLf
                    StrSql += " declare @idInsRep table(id int); "
                    StrSql = StrSql & " insert into contenedor (cve_reporte, estatus, id_programa)" & vbCrLf
                    StrSql = StrSql & " Values (" & valor & ",'0'," & arr(i) & ")"
                End If
                CConecta.Transaccion(StrTransaccion, True, False, True)
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
                If i = 0 Then
                    ds = CConecta.AbrirConsulta(StrSql)
                    If ds.Tables(0).Rows.Count > 0 Then
                        valor = ds.Tables(0).Rows(0)(0)
                    End If
                End If
                CConecta.Transaccion(StrTransaccion, False, True, False, True)

                If valorInicial = "" Then
                    valorInicial = valor & ","
                Else
                    valorInicial = valorInicial & valor & ","
                End If
                valor = valor + 1
            Next

            valorInicial = "(" & Mid(valorInicial, 1, valorInicial.Length - 1) & ")"
            Return valorInicial

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Consulta_contenedor(ByVal llave As Integer) As DataTable

        Try
            StrSql = "select * from contenedor_reporte"
            StrSql = StrSql & " where cve_reporte = " & llave & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Consulta_contenedor_script(ByVal llave As String) As DataTable

        Try
            StrSql = "select distinct estatus from contenedor "
            StrSql = StrSql & " where cve_reporte in " & llave & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Consulta_pinta_descarga(ByVal llave As String, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select id_programa, nombre_reporte, url_reporte, url_recibo"
            StrSql = StrSql & " from contenedor "
            StrSql = StrSql & " where cve_reporte in " & llave & ""
            StrSql = StrSql & " and id_programa =" & intprograma & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertaBanderaUsuario(ByVal intusuario As Integer)

        Try
            StrTransaccion = "InsertaBanderaUsuario"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " Update usuario set bandera_registro='1'"
            StrSql = StrSql & " where id_usuario = " & intusuario & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'mazda
    Public Function fecha_servidor() As DataTable

        Try
            StrSql = "set dateformat dmy"
            StrSql = StrSql & " select bandera = case when getdate() <= '31/12/2009' then '0' else '1' end"
            StrSql = StrSql & " set dateformat mdy"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_accesso(ByVal strlogin As String, ByVal strpass As String, ByVal intMarca As Integer) As DataTable

        Try
            StrSql = "SELECT id_usuario, nombre, descripcion_nivel , programa = count(distinct isnull(id_programa,0))"
            StrSql = StrSql & " FROM VW_LOGIN_ACCESO"
            StrSql = StrSql & " WHERE login = '" & strlogin & "'"
            StrSql = StrSql & " AND Contrasena='" & strpass & "'"
            StrSql = StrSql & " group by id_usuario, nombre, descripcion_nivel "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function pinta_menu_usuario(ByVal intusuario As Integer) As DataTable

        Try
            StrSql = "select id_programa, descripcion_programa, imagen_url, Orden_Programa"
            StrSql = StrSql & " FROM VW_LOGIN_ACCESO"
            StrSql = StrSql & " WHERE id_usuario = " & intusuario & ""
            StrSql = StrSql & " group by id_programa, descripcion_programa, imagen_url, Orden_Programa"
            StrSql = StrSql & " order by Orden_Programa"
            'StrSql = StrSql & " order by descripcion_programa"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function determina_bandera_programa(ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select ban_deduc, ban_precontrol, ban_rate, ban_constante, ban_tipopoliza, Ban_Multianual"
            StrSql = StrSql & " from ABC_PROGRAMA_ASEGURADORA"
            StrSql = StrSql & " where estatus_programaaseguradora ='1'"
            StrSql = StrSql & " and id_programa = " & intprograma & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function programas_interface_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable

        Try
            If intnivel = 0 Or intnivel = 3 Then
                StrSql = "select p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u "
                StrSql = StrSql & " where p.estatus_programa = '1' "
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " order by descripcion_programa"
            Else
                StrSql = "select p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u"
                StrSql = StrSql & " where p.id_programa = b.id_programa"
                StrSql = StrSql & " and u.id_usuario = b.id_usuario"
                StrSql = StrSql & " and u.id_usuario = " & intusuario & ""
                StrSql = StrSql & " and p.estatus_programa = '1'"
                StrSql = StrSql & " and u.estatus_usuario = '1'"
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " order by descripcion_programa"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function programas_Reporte_usuario(ByVal intusuario As Integer, ByVal intnivel As Integer) As DataTable

        Try
            If intnivel = 0 Then
                StrSql = "select id_programa =0, descripcion_programa ='-- Seleccione --' union"
                StrSql = StrSql & " select p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u "
                StrSql = StrSql & " where p.estatus_programa = '1' "
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " order by descripcion_programa"
            Else
                StrSql = "select id_programa =0, descripcion_programa ='-- Seleccione --' union"
                StrSql = StrSql & " select p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u"
                StrSql = StrSql & " where p.id_programa = b.id_programa"
                StrSql = StrSql & " and u.id_usuario = b.id_usuario"
                StrSql = StrSql & " and u.id_usuario = " & intusuario & ""
                StrSql = StrSql & " and p.estatus_programa = '1'"
                StrSql = StrSql & " and u.estatus_usuario = '1'"
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " order by descripcion_programa"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function inserta_contenedor(ByVal intprograma As Integer) As String
        Dim ds As New DataSet
        Dim valor As Integer = 0
        Dim i As Integer = 0

        Try
            'HMH 20150417 Esta consulta se incluyo en el insert porque esta casuando conflicto
            'StrSql = " select clave = isnull(max(cve_reporte),0) + 1 from contenedor"
            'ds = CConecta.AbrirConsulta(StrSql, False)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    valor = ds.Tables(0).Rows(0)("clave")
            'End If

            StrTransaccion = "inserta_contenedor"

            StrSql = "set dateformat dmy; " & vbCrLf
            StrSql += " declare @idInsRep table(id int); "
            StrSql = StrSql & " insert into contenedor (cve_reporte, estatus, id_programa)" & vbCrLf
            StrSql = StrSql & " OUTPUT inserted.cve_reporte into @idInsRep select (Select isnull(max(cve_reporte), 0 ) + 1 from contenedor With(NoLock)),'0'," & intprograma & "; "
            StrSql += "select * from @idInsRep; "
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            ds = CConecta.AbrirConsulta(StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                valor = ds.Tables(0).Rows(0)(0)
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            Return valor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ImpReporte(ByVal intllave As Integer) As DataTable

        Try
            StrSql = "select nombre_reporte, url_reporte"
            StrSql = StrSql & " from contenedor"
            StrSql = StrSql & " where cve_reporte = " & intllave & ""
            StrSql = StrSql & " and estatus = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 12/12/2013 se agrega datos cliente cotizacion conauto
    Public Function CargaDatosClienteCotiza(ByVal IdCotizacion As Long) As DataTable
        Try
            StrSql = "select  datoscliente, grupointegrante, noserie, observaciones "
            StrSql = StrSql & " from COTIZACION_UNICA"
            StrSql = StrSql & " where id_CotizacionUnica = " & IdCotizacion & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub ActualizaCotizacionCliente(ByVal StrCliente As String, ByVal StrSerie As String, ByVal StrObs As String, ByVal IdCotizacion As Long)
        Try
            StrTransaccion = "UpdCotiCli"

            StrSql = " update COTIZACION_UNICA set datoscliente ='" & StrCliente & "', "
            'StrSql = StrSql & " grupointegrante='" & StrGrupo & "', "
            StrSql = StrSql & " noserie ='" & StrSerie & "', "
            StrSql = StrSql & " observaciones='" & StrObs & "',"
            StrSql = StrSql & " idconfirmacion=1,"
            StrSql = StrSql & " fecha_confirmacion= getdate() "
            StrSql = StrSql & " where id_CotizacionUnica =" & IdCotizacion & ""
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'evi 12/12/2013 se agrega datos cliente cotizacion conauto
    Public Function UrlCotizacion(ByVal IdCotizacion As Long) As DataTable
        Try
            StrSql = "select  urlcotizacion "
            StrSql = StrSql & " from COTIZACION_UNICA"
            StrSql = StrSql & " where id_CotizacionUnica = " & IdCotizacion & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidaCotizacion(ByVal IdCotizacion As Long, ByVal IdBid As Integer, ByVal IdPrograma As Integer) As DataTable
        Try
            StrSql = "select  *, confirmacion = isnull(idconfirmacion,0), Emision = isnull(IdEmision,0)"
            StrSql = StrSql & " from COTIZACION_UNICA"
            StrSql = StrSql & " where id_Programa  = " & IdPrograma & ""
            StrSql = StrSql & " and id_CotizacionUnica = " & IdCotizacion & ""
            StrSql = StrSql & " and id_bid = " & IdBid & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SubRamoPanel(ByVal IdCotizacion As Long) As DataTable
        Try
            StrSql = "select subramo, idpaquete"
            StrSql = StrSql & " from COTIZACION_PANEL cp, COTIZACION_UNICA cu "
            StrSql = StrSql & " where cp.id_cotizacionpanel = cu.id_cotizacionpanel"
            StrSql = StrSql & " and cu.id_CotizacionUnica = " & IdCotizacion & ""
            StrSql = StrSql & " group by subramo, idpaquete"

            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function BuscaCotizacion(ByVal intprograma As Integer, ByVal StrCadena As String, ByVal StrCriterios As String) As DataTable
        Try
            StrCadena = Replace(StrCadena, " ", "%")

            StrSql = "select noCotizacion= right('00000' + cast(cu.id_cotizacionunica as varchar),5), cu.id_cotizacionunica, cu.id_bid, b.empresa, "
            'StrSql = StrSql & " confirmada = case when not IdConfirmacion is null then 'Confirmado' "
            'StrSql = StrSql & " else case when  not idemision is null then 'Emitida' else '' end end "
            StrSql = StrSql & " confirmada = case when Id_Programa = 29 then "
            StrSql = StrSql & " case when isnull(meses_segurogratis,0) = 0 then 'No necesita confirmaci�n' else "
            StrSql = StrSql & " case when not IdConfirmacion is null then 'Confirmado'  else case when  not idemision is null "
            StrSql = StrSql & " then 'Emitida' else '' end end  end end "
            StrSql = StrSql & " from COTIZACION_UNICA cu, bid b "
            StrSql = StrSql & " WHERE cu.id_bid = b.id_bid "
            StrSql = StrSql & " and Id_Programa = " & intprograma & ""

            If StrCriterios <> "" Then
                StrSql = StrSql & " AND ("

                If InStr(StrCriterios, "0") Then
                    If IsNumeric(StrCadena) Then
                        StrSql = StrSql & " id_cotizacionunica = " & StrCadena & " OR "
                    Else
                        StrSql = StrSql & " id_cotizacionunica = 0 OR "
                    End If
                End If

                If InStr(StrCriterios, "1") Then
                    StrSql = StrSql & " empresa LIKE '%" & StrCadena & "%' OR "
                End If

                StrSql = Trim(Left(StrSql, Len(StrSql) - 3))
                StrSql = StrSql & ")"
            End If

            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'HM 13/02/2014 agregando leyenda administrador
    Public Sub ConfirmaCotizacionAdmin(ByVal idusuario As Integer, ByVal IdCotizacion As Integer, ByVal strobs As String)
        Try
            StrTransaccion = "UpdCotiAdm"

            StrSql = " update COTIZACION_UNICA set idconfirmacion =1, "
            StrSql = StrSql & " fecha_confirmacion = getdate(), "
            StrSql = StrSql & " id_usuario = " & idusuario & ", "
            'HM 13/02/2014 agregando leyenda administrador
            StrSql = StrSql & " observaciones = observaciones + '<br>Administrador : ' + '" & strobs & "' "
            StrSql = StrSql & " where id_CotizacionUnica =" & IdCotizacion & ""
            CConecta.Transaccion(StrTransaccion, True, False, True)
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function carga_region() As DataTable

        Try
            StrSql = " select id_region = 0, region='-- Seleccione --' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=1 , region='AGUASCALIENTES' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=2 , region='BAJA CALIFORNIA NORTE' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=3 , region='BAJA CALIFORNIA SUR' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=4 , region='CAMPECHE' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=5 , region='COAHUILA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=6 , region='COLIMA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=7 , region='CHIAPAS' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=8 , region='CHIHUAHUA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=9 , region='DISTRITO FEDERAL' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=10 , region='DURANGO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=11 , region='ESTADO DE MEXICO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=12 , region='GUANAJUATO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=13 , region='GUERRERO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=14 , region='HIDALGO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=15 , region='JALISCO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=16 , region='MICHOAC�N' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=17 , region='MORELOS' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=18 , region='NAYARIT' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=19 , region='NUEVO LE�N' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=20 , region='OAXACA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=21 , region='PUEBLA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=22 , region='QUER�TARO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=23 , region='QUINTANA ROO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=24 , region='SAN LUIS POTOS�' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=25 , region='SINALOA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=26 , region='SONORA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=27 , region='TABASCO' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=28 , region='TAMAULIPAS' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=29 , region='TLAXCALA' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=30 , region='VERACRUZ' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=31 , region='YUCAT�N' "
            StrSql = StrSql & "  union"
            StrSql = StrSql & " select id_region=32 , region='ZACATECAS' "
            StrSql = StrSql & " order by id_region"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_bid() As DataTable

        Try
            'StrSql = " select b.id_bid, b.bid_ford, b.bid_aon, "
            'StrSql = StrSql & " b.empresa, b.estatus, m.marca,"
            'StrSql = StrSql & " est = case when b.estatus = '1' then 'Activo' "
            'StrSql = StrSql & " else 'Inactivo' end, m.id_marca"
            'StrSql = StrSql & " from bid b, abc_marca m"
            'StrSql = StrSql & " where b.id_marca = m.id_marca"
            'StrSql = StrSql & " and m.estatus_marca= '1'"

            StrSql = " select id_bid = 0, empresa='-- Seleccione --', nombre='' "
            StrSql = StrSql & " union"
            StrSql = StrSql & " select distinct id_bid, "
            StrSql = StrSql & " empresa =  LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))), "
            StrSql = StrSql & " nombre = LTRIM(RTRIM(RTRIM(LTRIM(ISNULL(BID.Bid_Ford, ''))) + ' - ' + RTRIM(LTRIM(ISNULL(BID.Empresa, ''))))) "
            StrSql = StrSql & " from bid where id_bid in ("
            StrSql = StrSql & " select distinct id_bid "
            StrSql = StrSql & " from bid)"
            'StrSql = StrSql & " where id_usuario =" & intusuario & ""
            'StrSql = StrSql & " and id_marca =" & intmarca & ""
            'StrSql = StrSql & " and id_programa =" & intprograma & ")"
            StrSql = StrSql & " order by nombre"

            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub inserta_bid_region(ByVal strprog As String, ByVal intbid As Integer, ByVal intregion As Integer, Region As String, Factor As Integer, CteFactor As Integer)

        Try
            Dim i, j, k, m, p As Integer
            Dim ds As New DataSet
            Dim strAseguradoras As String
            Dim strAsegConauto As String = "59,60,61"
            Dim strAsegConautopcion As String = "62,63"
            Dim strAsegArrendadora As String = "64,65"
            Dim strSubramo As String = "1,2,3"
            Dim strEstatus As String = "N,U"

            Dim arrProg() As String = strprog.Split(",")
            Dim arrSubramo() As String = strSubramo.Split(",")
            Dim arrEstatus() As String = strEstatus.Split(",")
            Dim arrAsegConauto() As String = strAsegConauto.Split(",")
            Dim arrAsegConautopcion() As String = strAsegConautopcion.Split(",")
            Dim arrAsegArrendadora() As String = strAsegConautopcion.Split(",")
            Dim arrAseguradoras() As String

            StrTransaccion = "inserta_bid_region"
            CConecta.Transaccion(StrTransaccion, True, False, True)


            For i = 0 To arrProg.Length - 1
                If arrProg(i) = 29 Then
                    StrSql = "delete from bid_region where id_aseguradora in (" & strAsegConauto & ") and id_bid = " & intbid & ""
                ElseIf arrProg(i) = 30 Then
                    StrSql = "delete from bid_region where id_aseguradora in (" & strAsegConautopcion & ") and id_bid = " & intbid & ""
                Else
                    StrSql = "delete from bid_region where id_aseguradora in (" & strAsegArrendadora & ") and id_bid = " & intbid & ""
                End If
                CConecta.EjecutaSQL(StrTransaccion, StrSql)
            Next

            For i = 0 To arrProg.Length - 1
                StrSql = " select id_aseguradora from abc_programa_aseguradora where id_programa = " & arrProg(i) & ""
                ds = CConecta.AbrirConsulta(StrSql, False)
                If ds.Tables(0).Rows.Count > 0 Then
                    For p = 0 To ds.Tables(0).Rows.Count - 1
                        If p = 0 Then
                            strAseguradoras = ds.Tables(0).Rows(p)("id_aseguradora")
                        Else
                            strAseguradoras = strAseguradoras & "," & ds.Tables(0).Rows(p)("id_aseguradora")
                        End If
                    Next
                End If
                arrAseguradoras = strAseguradoras.Split(",")

                For m = 0 To arrAseguradoras.Length - 1
                    For j = 0 To arrSubramo.Length - 1
                        For k = 0 To arrEstatus.Length - 1
                            StrSql = "insert into bid_region (id_aseguradora, id_bid, id_region, Subramo, Estatus, Descripcion_Region, Factor, CteFactor)"
                            StrSql = StrSql & " values(" & arrAseguradoras(m) & "," & intbid & "," & intregion & "," & arrSubramo(j) & ",'" & arrEstatus(k) & "','" & Region & "'," & Factor & "," & CteFactor & ")"
                            CConecta.EjecutaSQL(StrTransaccion, StrSql)
                        Next
                    Next
                Next
            Next
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function CargaGridBidRegion(intBid As Integer, intRegion As Integer, strprog As String) As DataTable

        Try
            Dim ds As DataSet
            Dim i, p As Integer
            Dim arrProg() As String = strprog.Split(",")
            Dim strAseguradoras As String = ""

            For i = 0 To arrProg.Length - 1
                StrSql = " select id_aseguradora from abc_programa_aseguradora where id_programa = " & arrProg(i) & ""
                ds = CConecta.AbrirConsulta(StrSql, False)
                If ds.Tables(0).Rows.Count > 0 Then
                    For p = 0 To ds.Tables(0).Rows.Count - 1
                        If p = 0 Then
                            If i = 0 Then
                                strAseguradoras = ds.Tables(0).Rows(p)("id_aseguradora")
                            Else
                                strAseguradoras = strAseguradoras & "," & ds.Tables(0).Rows(p)("id_aseguradora")
                            End If
                        Else
                            strAseguradoras = strAseguradoras & "," & ds.Tables(0).Rows(p)("id_aseguradora")
                        End If
                    Next
                End If
            Next

            StrSql = "SELECT pa.Id_Programa, pa.Id_Programa, a.Id_Aseguradora, a.Descripcion_Aseguradora, b.Id_Bid," & vbCrLf
            StrSql = StrSql & " b.Empresa, br.Id_Region, br.SubRamo, br.Estatus, br.Descripcion_Region," & vbCrLf
            StrSql = StrSql & " br.Factor, br.CteFactor, p.Descripcion_Programa" & vbCrLf
            StrSql = StrSql & " FROM ABC_ASEGURADORA a INNER JOIN" & vbCrLf
            StrSql = StrSql & " BID_REGION br ON a.Id_Aseguradora = br.Id_ASeguradora INNER JOIN" & vbCrLf
            StrSql = StrSql & " ABC_PROGRAMA_ASEGURADORA pa ON a.Id_Aseguradora = pa.Id_Aseguradora INNER JOIN" & vbCrLf
            StrSql = StrSql & " ABC_PROGRAMA p ON pa.Id_Programa = p.Id_Programa LEFT OUTER JOIN" & vbCrLf
            StrSql = StrSql & " BID b ON br.Id_Bid = b.Id_Bid" & vbCrLf
            StrSql = StrSql & " WHERE a.Id_Aseguradora IN (" & strAseguradoras & ")" & vbCrLf
            StrSql = StrSql & " AND b.Id_Bid = " & intBid & "" & vbCrLf
            StrSql = StrSql & " AND br.Id_Region = " & intRegion & ""

            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function VerificaInformacionBid(intBid As Integer, intBidFord As Integer, strBidAon As String) As DataTable

        Try

            StrSql = "SELECT * from Bid" & vbCrLf
            StrSql = StrSql & " WHERE (Bid_Conauto = '" & intBid & "' OR " & vbCrLf
            StrSql = StrSql & " bid_ford = '" & intBidFord & "' OR " & vbCrLf
            StrSql = StrSql & " bid_aon = '" & strBidAon & "')"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function programas(ByVal intusuario As Integer, ByVal intnivel As Integer, ByVal intmarca As Integer) As DataTable

        Try
            If intnivel = 0 Then
                StrSql = "select p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u "
                StrSql = StrSql & " where p.estatus_programa = '1'"
                If intMarca = 1 Then
                    StrSql = StrSql & " and p.id_programa in (29,30,31)"
                Else
                    StrSql = StrSql & " and p.id_programa = 32"
                End If
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa "
                StrSql = StrSql & " order by p.id_programa, p.descripcion_programa"
            Else
                StrSql = "select p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " from abc_programa p, usuario_bid b, usuario u"
                StrSql = StrSql & " where p.id_programa = b.id_programa"
                StrSql = StrSql & " and u.id_usuario = b.id_usuario"
                StrSql = StrSql & " and u.id_usuario = " & intusuario & ""
                StrSql = StrSql & " and p.estatus_programa = '1'"
                StrSql = StrSql & " and u.estatus_usuario = '1'"
                If intMarca = 1 Then
                    StrSql = StrSql & " and p.id_programa = 29,30,31"
                Else
                    StrSql = StrSql & " and p.id_programa = 32"
                End If
                StrSql = StrSql & " group by p.id_programa, p.descripcion_programa"
                StrSql = StrSql & " order by p.id_programa, p.descripcion_programa"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_nuevoacceso(ByVal intusuario As String, ByVal strpass As String) As DataTable

        Try
            StrSql = "SELECT id_usuario, nombre, descripcion_nivel , programa = count(distinct isnull(id_programa,0))"
            StrSql = StrSql & " FROM VW_LOGIN_ACCESO"
            StrSql = StrSql & " WHERE id_usuario = " & intusuario & ""
            StrSql = StrSql & " AND contrasena = '" & strpass & "'"
            StrSql = StrSql & " group by id_usuario, nombre, descripcion_nivel "
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Verifica_Vigencia_Password(ByVal idUsuario As Integer) As Boolean
        Dim resultado As Boolean = False
        Dim dt As DataTable
        Try
            StrSql = "DECLARE  @FECHAANT DATE;"
            StrSql = StrSql & " SET @FECHAANT=(SELECT [LastPasswordChangedDate]"
            StrSql = StrSql & " FROM [FEC].[USUARIO]"
            StrSql = StrSql & " WHERE [Id_Usuario]=" & idUsuario & ");"
            StrSql = StrSql & " SELECT DATEDIFF(DAY,@FECHAANT,GETDATE()) [Dias transcurridos]"

            dt = CConecta.AbrirConsulta(StrSql, True)
            If Convert.ToInt32(dt.Rows(0)(0).ToString) <= 91 Then
                resultado = True
            End If
            Return resultado
        Catch ex As Exception

        End Try
    End Function

    Public Function Cargar_InformacionMarca(ByVal IdMarca As Integer) As DataTable

        Try
            StrSql = "SELECT * FROM ABC_Marca"
            StrSql = StrSql & " WHERE Id_Marca = " & IdMarca & ""
            StrSql = StrSql & " AND Estatus_Marca = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Cargar_InformacionPrograma(ByVal IdPrograma As Integer) As DataTable

        Try
            StrSql = "SELECT * FROM ABC_Programa"
            StrSql = StrSql & " WHERE Id_Programa = " & IdPrograma & ""
            StrSql = StrSql & " AND Estatus_Programa = '1'"
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
