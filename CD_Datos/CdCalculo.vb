Imports CD_Datos.CdSql
Imports System.Configuration
Public Class CdCalculo
    Private StrSql As String = ""
    Private StrTransaccion As String = ""
    Private CConecta As New CdSql

    Public Sub New()
    End Sub

    Public Function promocion(ByVal intpaquete As Integer, ByVal stranio As String, _
        ByVal strmodelo As String, ByVal strtipopol As String, _
        ByVal intaseguradora As Integer, ByVal intprograma As Integer) As DataTable
        
        Try
            'Especificación del campo PLAN en TARIFACT y VEHICULOS
            strsql = " SELECT valor_promocion, modelo_salida , bandera_promocion"
            strsql = strsql & " FROM promocion"
            strsql = strsql & " where getdate() between vigencia_inicio and vigencia_fin"
            strsql = strsql & " AND id_paquete = " & intpaquete & ""
            strsql = strsql & " AND anio_promocion = '" & stranio & "'"
            strsql = strsql & " AND modelo_promocion = '" & strmodelo & "'  "
            strsql = strsql & " AND tipo_poliza ='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            strsql = strsql & " AND id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function tarifac(ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal intpaquete As Integer, ByVal stranio As String, ByVal strmodelo As String, _
    ByVal intregion As Integer, ByVal intplan As Integer, ByVal strtipopol As String, _
    ByVal intaseguradora As Integer, ByVal intprograma As Integer, ByVal stropcsa As String) As DataTable
        
        Try
            'Especificación del campo PLAN en TARIFACT y VEHICULOS
            strsql = " SELECT distinct t.FACTPRIM_tarifac, t.FactDm_Tarifac, t.opcsa_tarifac"
            strsql = strsql & " FROM TARIFACT t,  abc_paquete p"
            strsql = strsql & " WHERE t.id_paquete = p.id_paquete"
            strsql = strsql & " and t.id_aseguradora = p.id_aseguradora"
            strsql = strsql & " and t.Moneda_tarifac = " & intmoneda & "  "
            strsql = strsql & " AND p.id_PAQUETE = " & intpaquete & ""
            strsql = strsql & " AND t.anio_tarifac = '" & stranio & "'"
            strsql = strsql & " AND t.MODELO_tarifac = '" & strmodelo & "'  "
            strsql = strsql & " AND t.REGION_tarifac = 1"
            If Not stropcsa = "" Then
                strsql = strsql & " AND t.OPCSA_tarifac = '" & stropcsa & "'  "
            End If
            If intprograma = 9 Or intprograma = 11 Then
                strsql = strsql & " AND (t.PLAN_tarifac = 2 or t.PLAN_tarifac = 0)"
            Else
                strsql = strsql & " AND t.PLAN_tarifac = 2"
            End If
            strsql = strsql & " AND t.TIPOPOL_tarifac='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            strsql = strsql & " AND t.id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function tariprima(ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal intpaquete As Integer, ByVal stranio As String, ByVal strinsco As String, _
    ByVal intregion As Integer, ByVal intplan As Integer, _
    ByVal strtipopol As String, ByVal intaseguradora As Integer) As DataTable
       
        Try
            'Especificación del campo PLAN en TARIFACT y VEHICULOS
            strsql = " SELECT distinct t.prima_tariprima, t.opcsa_tariprima"
            strsql = strsql & " FROM TARIPRIMA t,  abc_paquete p"
            strsql = strsql & " WHERE t.id_paquete = p.id_paquete"
            strsql = strsql & " and t.id_aseguradora = p.id_aseguradora"
            strsql = strsql & " and t.Moneda_tariprima = " & intmoneda & "  "
            strsql = strsql & " AND p.id_PAQUETE = " & intpaquete & ""
            strsql = strsql & " AND t.anio_tariprima='" & stranio & "'"
            strsql = strsql & " AND t.insco_tariprima = '" & strinsco & "'"
            strsql = strsql & " AND t.REGION_tariprima = 1"
            'strsql = strsql & " AND t.OPCSA_tariprima = '" & stropcsa & "'  "
            If intaseguradora <> 12 Then
                strsql = strsql & " AND t.PLAN_tariprima = 1"
            Else
                strsql = strsql & " AND t.PLAN_tariprima = 2"
            End If
            strsql = strsql & " AND t.TIPOPOL_tariprima='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            strsql = strsql & " AND t.id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function constante(ByVal intmoneda As Integer, ByVal intsubramo As Integer, _
    ByVal intplazas As Integer, ByVal stropcsa As String, ByVal intaseguradora As Integer, _
    ByVal strestatus As String, ByVal ban_EstatusConst As Integer, _
    ByVal intprograma As Integer, ByVal intmarca As Integer) As DataTable
        
        Try
            strsql = " SELECT CONSTANTE_constante "
            strsql = strsql & " FROM constante"
            strsql = strsql & " WHERE MONEDA_constante = " & intmoneda & ""
            strsql = strsql & " AND SUBRAMO_constante = " & intsubramo & ""
            strsql = strsql & " AND PLAZA_constante = " & intplazas & ""
            strsql = strsql & " AND OPCSA_constante = '" & stropcsa & "'"
            strsql = strsql & " AND id_Aseguradora=" & intaseguradora & ""
            If ban_EstatusConst = 1 Then
                strsql = strsql & " AND estatus ='" & strestatus & "'"
            End If
            'emmanuell multimarca
            'If intprograma = 9 Then
            If intmarca = 1 Then
                strsql = strsql & " AND id_marca =" & intmarca & ""
            Else
                strsql = strsql & " AND id_marca is null"
            End If
            'End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function uso(ByVal intsubramo As Integer, ByVal struso As String, _
    ByVal intaseguradora As Integer) As DataTable
        
        Try
            strsql = " select f1, f2 "
            strsql = strsql & " from abc_uso "
            strsql = strsql & " WHERE SUBRAMO = " & intsubramo & ""
            strsql = strsql & " AND USO = '" & struso & "'"
            strsql = strsql & " AND id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function subsidio(ByVal intmoneda As Integer, ByVal strcatalogo As String, _
    ByVal intanio As Integer, ByVal intregion As Integer, ByVal intplan As Integer, _
    ByVal intpaquete As Integer, ByVal strestatus As String, ByVal strfincon As String, _
    ByVal stropcsa As String, ByVal intprograma As Integer, ByVal intaseguradora As Integer) As DataTable
        
        Try
            strsql = " SELECT SUBSIDY_F,SUBSIDY_FC,SUBSIDY_D,SUBSIDY_FLG,  "
            strsql = strsql & " SUBSIDY_LF, SUBSIDY_LFC, SUBSIDY_LD, SUBSIDY_LS"
            strsql = strsql & " FROM SUBSIDIOS"
            strsql = strsql & " WHERE MONEDA_subsidios = " & intmoneda & ""
            strsql = strsql & " AND CATALOGO_subsidios = '" & strcatalogo & "'  "
            strsql = strsql & " AND anio_subsidios = " & intanio & "  "
            If intprograma = 5 Or intprograma = 9 Then
                strsql = strsql & " AND REGION_subsidios =1"
                strsql = strsql & " AND PLAN_subsidios = 2"
            Else
                strsql = strsql & " AND REGION_subsidios = " & intregion & "  "
                strsql = strsql & " AND PLAN_subsidios = " & intplan & ""
            End If
            strsql = strsql & " AND id_paquete = " & intpaquete & ""
            strsql = strsql & " AND ESTATUS_subsidios='" & strestatus & "'  "
            strsql = strsql & " AND FINCON = '" & strfincon & "'"
            strsql = strsql & " AND OPCSA_subsidios ='" & stropcsa & "'"
            strsql = strsql & " AND Id_aseguradora =" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Fvigencias(ByVal intmoneda As Integer, _
    ByVal intvigencia As Integer, ByVal strtipopol As String, _
    ByVal intaseguradora As Integer) As DataTable
        Try
            'evi 14/11/2013 se agrega bandera para conauto-opcion
            StrSql = " SELECT FACTVIG, FactVigMultianual, FactVigCruzada"
            StrSql = StrSql & " FROM FACTVIGENCIA "
            StrSql = StrSql & " WHERE MONEDA = " & intmoneda & ""
            StrSql = StrSql & " AND VIGENCIA =" & intvigencia & ""
            StrSql = StrSql & " AND TIPOPOL='" & IIf(strtipopol = "F", "M", strtipopol) & "'"
            StrSql = StrSql & " AND id_Aseguradora=" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function precontrol(ByVal strcatalogo As String, ByVal stranio As String, ByVal strmoneda As String, ByVal strrate As String, ByVal intaseguradora As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = " Select distinct precontrol"
            StrSql = StrSql & " FROM PRECONTROL  "
            If intaseguradora > 0 Then
                StrSql = StrSql & " WHERE id_aseguradora =" & intaseguradora & ""
            Else
                StrSql = StrSql & " WHERE id_aseguradora in (select distinct id_aseguradora from abc_programa_aseguradora where id_programa = " & intprograma & ")"
            End If
            StrSql = StrSql & " AND CATALOGO_PRECONTROL = '" & strcatalogo & "'"
            StrSql = StrSql & " AND anio_PRECONTROL = '" & stranio & "'  "
            StrSql = StrSql & " AND MONEDA = '" & strmoneda & "'"
            If Not strrate = "" Then
                StrSql = StrSql & " AND INTERESTRATE='" & strrate & "'"
            End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Determina_Region_factor(ByVal intaseguradora As Integer, _
    ByVal intbid As Integer, ByVal intregion As Integer, ByVal strramo As String, _
    ByVal strestatus As String) As DataTable

        Try
            StrSql = " Select factor, ctefactor "
            StrSql = StrSql & " FROM BID_REGION   "
            StrSql = StrSql & " WHERE id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " AND id_bid = " & intbid & " "
            StrSql = StrSql & " AND id_region = " & intregion & ""
            StrSql = StrSql & " AND subramo = '" & strramo & "'"
            StrSql = StrSql & " AND Estatus ='" & strestatus & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function tariderpol(ByVal strmoneda As String, ByVal intsubramo As Integer, _
    ByVal intaseguradora As Integer, ByVal intprograma As Integer, ByVal intmarca As Integer) As DataTable

        Try
            StrSql = " SELECT DERPOL "
            StrSql = StrSql & " FROM tariderpol"
            StrSql = StrSql & " WHERE MONEDA_TARIDERPOL = '" & strmoneda & "'"
            StrSql = StrSql & " AND SUBRAMO_TARIDERPOL  = " & intsubramo & ""
            StrSql = StrSql & " AND id_Aseguradora=" & intaseguradora & ""
            'emmanuell multimarca
            'If intprograma = 9 Then
            StrSql = StrSql & " AND id_marca=" & intmarca & ""
            'End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function paquete(ByVal intpaquete As Integer) As DataTable
    '    Dim da As SqlDataAdapter
    '    Dim ds As New DataSet
    '    Dim strsql As String
    '    Try
    '        strsql = "select subramo, paquete"
    '        strsql = strsql & " from abc_paquete"
    '        strsql = strsql & " where id_paquete = " & intpaquete & ""
    '        da = New SqlDataAdapter(strsql, Cnn)
    '        da.SelectCommand.CommandType = CommandType.Text
    '        da.Fill(ds)
    '        Return ds.Tables(0)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function determina_iva_parametro_aseguradora(ByVal strcp As String, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select iva = (pct_iva/100)"
            StrSql = StrSql & " from parametro_iva"
            StrSql = StrSql & " where codigo_postal='" & strcp & "'"
            'strsql = strsql & " and id_aseguradora = " & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function paquete(ByVal intprograma As Integer, ByVal strestatus As String, ByVal intaonv As Integer, ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer, _
    Optional ByVal PaqSelCon As String = "", Optional ByVal idtipoprograma As Integer = -1) As DataTable

        Try
            'strsql = "select distinct P.id_paquete, P.subramo, P.paquete, a.Id_Aseguradora, p.descripcion_paquete "
            'strsql = strsql & " from abc_paquete P, ABC_PROGRAMA_ASEGURADORA A, vehiculo v"
            'strsql = strsql & " WHERE p.id_aseguradora = a.id_aseguradora"
            'strsql = strsql & " and v.id_aseguradora = a.id_aseguradora "
            'strsql = strsql & " AND a.id_programa =" & intprograma & ""
            ''strsql = strsql & " AND p.SUBRAMO = " & intsubramo & ""
            'strsql = strsql & " AND p.estatus = '" & strestatus & "'"
            'strsql = strsql & " AND a.estatus_programaaseguradora = '1'"
            'strsql = strsql & " and v.id_aonh = " & intaonv & ""
            'inicio micha
            StrSql = "select distinct P.id_paquete, P.subramo, P.paquete, a.Id_Aseguradora, "
            StrSql = StrSql & " p.descripcion_paquete , v.catalogo_vehiculo ,v.subramo_vehiculo"
            StrSql = StrSql & " , p.orden_paquete, p.opcsa_paquete"
            StrSql = StrSql & " from abc_paquete P, ABC_PROGRAMA_ASEGURADORA A, vehiculo v"
            StrSql = StrSql & " WHERE p.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and P.subramo = v.subramo_vehiculo "
            StrSql = StrSql & " and v.id_aseguradora = a.id_aseguradora "
            StrSql = StrSql & " AND a.id_programa =" & intprograma & ""
            StrSql = StrSql & " AND p.estatus = '" & strestatus & "'"
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1'"
            StrSql = StrSql & " and v.id_aonh = " & intaonv & ""

            If intprograma = 9 Then
                StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
                StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""
            End If

            'emmanuell multimarca
            'strsql = strsql & " AND v.id_marca = p.id_marca"
            'StrSql = StrSql & " AND v.id_marca = " & intmarca & ""
            StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " AND v.id_submarca = " & intmarca & ""

            If idtipoprograma > -1 Then
                StrSql = StrSql & " AND p.tipoprograma =" & idtipoprograma & ""
            End If

            StrSql = StrSql & " and p.subramo in (select distinct v.subramo_vehiculo"
            StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
            StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
            StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
            StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
            If intprograma = 9 Then
                StrSql = StrSql & " AND v.id_marca = " & intmarcaoriginal & ""
                If intmarcaoriginal = 1 Then
                    StrSql = StrSql & " AND p.deduc_pl > 0"
                Else
                    StrSql = StrSql & " AND p.deduc_pl = 0"
                End If
            End If
            StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
            StrSql = StrSql & " AND v.status_vehiculo ='1')"
            If Not PaqSelCon = "" Then
                StrSql = StrSql & " AND id_paquete in(" & PaqSelCon & ")"
            End If
            'HMH
            If intprograma = 30 Or intprograma = 31 Then
                StrSql = StrSql & " AND id_paquete <=12"
            End If
            'strsql = strsql & " order by p.descripcion_paquete"
            StrSql = StrSql & " order by p.orden_paquete, p.descripcion_paquete"
            'fin micha
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function consulta_region(ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal intbid As Integer) As DataTable

        Try
            StrSql = "select distinct id_region "
            StrSql = StrSql & " from BID_REGION"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and subramo = " & intsubramo & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function paquete_aseguradora(ByVal intsubramo As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select distinct id_paquete, subramo, paquete"
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where SUBRAMO = " & intsubramo & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'HMH
    Public Function paquete_aseguradora_unico(ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal intpaquete As Integer, ByVal strestatus As String, ByVal tipoPrograma As Integer) As DataTable

        Try
            StrSql = "select distinct id_paquete, subramo, paquete, descripcion_paquete, opcsa_paquete"
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where SUBRAMO = " & intsubramo & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            If Not strestatus = "" Then
                StrSql = StrSql & " and estatus = '" & strestatus & "'"
            End If
            If tipoPrograma <= 0 Then
                StrSql = StrSql & " and id_paquete =" & intpaquete & ""
            Else
                StrSql = StrSql & " and tipoprograma =" & tipoPrograma & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_insco_homologado(ByVal intvehiculo As Integer, _
    ByVal stranio As String, ByVal intaseguradora As Integer, ByVal strestatus As String) As DataTable

        Try
            'strsql = "select insco = insco_vehiculo, id_marca, catalogo_vehiculo"
            StrSql = "select insco = insco_vehiculo, id_marca = id_submarca, catalogo_vehiculo"
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where Id_AonH = " & intvehiculo & ""
            StrSql = StrSql & " and anio_vehiculo =" & stranio & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and estatus_vehiculo ='" & strestatus & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function datos_Vehiculo(ByVal intaseguradora As Integer, _
    ByVal strinsco As String, ByVal stranio As String, ByVal intprograma As Integer, _
    ByVal intmarca As Integer, ByVal intmarcaoriginal As Integer) As DataTable

        Try
            StrSql = "select plaza_vehiculo"
            StrSql = StrSql & " from vehiculo"
            StrSql = StrSql & " where insco_vehiculo='" & strinsco & "'"
            StrSql = StrSql & " and anio_vehiculo='" & stranio & "'"
            StrSql = StrSql & " and id_aseguradora=" & intaseguradora & ""
            'emmanuell multimarca
            'If intprograma = 9 Then
            StrSql = StrSql & " and id_marca = " & intmarcaoriginal & ""
            StrSql = StrSql & " and id_submarca = " & intmarca & ""
            'End If
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function recalculacalcula_coberturas(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer) As DataTable

        Try
            StrSql = " select totalC = (isnull(sum(total_cob),0) * 1.16)"
            StrSql = StrSql & " from cobertura"
            StrSql = StrSql & " where Not id_cobertura = 0"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function calcula_coberturas(ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal intaseguradora As Integer, ByVal strestatus As String) As DataTable

        Try
            'nuevo
            StrSql = " Select totales = isnull(sum(total_cob), 0)"
            StrSql = StrSql & " from cobertura"
            StrSql = StrSql & " where Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            StrSql = StrSql & " and id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and not descripcion_cob in( "
            StrSql = StrSql & " select distinct descripcion_paquete "
            StrSql = StrSql & " from abc_paquete"
            StrSql = StrSql & " where id_aseguradora =" & intaseguradora & ""
            StrSql = StrSql & " and subramo =" & intsubramo & ""
            StrSql = StrSql & " and estatus = '" & strestatus & "')"

            'menos antinguo
            'strsql = " Select totales = isnull(sum(total_cob), 0)"
            'strsql = strsql & " from cobertura"
            'strsql = strsql & " where Not id_cobertura = 0"
            'strsql = strsql & " and Num_Contrato=" & intcontrato & ""
            'strsql = strsql & " and id_bid=" & intbid & ""
            'strsql = strsql & " and subramo=" & intsubramo & ""
            'strsql = strsql & " and id_aseguradora =" & intaseguradora & ""

            '''''muy antiguo
            '''''strsql = " select total_cob"
            '''''strsql = strsql & " from cobertura"
            '''''strsql = strsql & " where id_cobertura = 0"
            '''''strsql = strsql & " and Num_Contrato=" & intcontrato & ""
            '''''strsql = strsql & " and id_bid=" & intbid & ""
            '''''strsql = strsql & " and subramo=" & intsubramo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function calculando_total_coberturas(ByVal intcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer) As DataTable

        Try
            StrSql = " select total = isnull(sum(total_cob),0) from cobertura "
            StrSql = StrSql & " where Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_cobertura_totales(ByVal dbtotal As Double, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal intaseguradora As Integer)

        Try
            StrTransaccion = "ActualizaCobTot"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set total_cob=" & dbtotal & ""
            StrSql = StrSql & " where id_cobertura = 0"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            StrSql = StrSql & " and Id_Aseguradora=" & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_aCero(ByVal intcontrato As Integer, ByVal intbid As Integer)
        Try
            StrTransaccion = "ActualizaCobCero"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set total_cob=0 "
            StrSql = StrSql & " where Num_Contrato =" & intcontrato & ""
            StrSql = StrSql & " and id_bid =" & intbid & ""
            StrSql = StrSql & " and id_cap =0"
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_totales_fscar(ByVal dbtotal As Double, _
   ByVal intcontrato As Integer, ByVal intbid As Integer, _
   ByVal intsubramo As Integer, ByVal nompaquete As String, _
    ByVal intaseguradora As Integer)

        Try
            StrTransaccion = "ActualizaCobTotfscar"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set total_cob=" & dbtotal & ""
            StrSql = StrSql & " where descripcion_cob = '" & nompaquete & "'"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            StrSql = StrSql & " and Id_Aseguradora=" & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_totales_panel(ByVal dbtotal As Double, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal intsubramo As Integer, ByVal nompaquete As String, _
    ByVal intaseguradora As Integer)

        Try
            StrTransaccion = "ActualizaCobTotPanel"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set total_cob=" & dbtotal & ""
            StrSql = StrSql & " where descripcion_cob = '" & nompaquete & "'"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            StrSql = StrSql & " and subramo=" & intsubramo & ""
            StrSql = StrSql & " and Id_Aseguradora=" & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_cobertura_totales_fscar_VD(ByVal dbtotal As Double, _
    ByVal intcontrato As Integer, ByVal intbid As Integer, _
    ByVal nompaquete As String, ByVal inttipo As Integer, _
    ByVal intprograma As Integer, ByVal intaonv As Integer, _
    ByVal strestatus As String, ByVal intsubramo As Integer)

        Try
            StrTransaccion = "ActualizaCobTotfscarVD"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura set "
            If inttipo = 1 Then
                'vida
                StrSql = StrSql & " vida=" & dbtotal & ""
            Else
                'desempleo
                StrSql = StrSql & " desempleo=" & dbtotal & ""
            End If
            StrSql = StrSql & " where descripcion_cob = '" & nompaquete & "'"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            'inicia micha
            If intsubramo = 0 Then
                'strsql = strsql & " and subramo=" & intsubramo & ""
                StrSql = StrSql & " and subramo in (select distinct v.subramo_vehiculo"
                StrSql = StrSql & " from vehiculo v, ABC_PROGRAMA_ASEGURADORA a "
                StrSql = StrSql & " WHERE v.id_aseguradora = a.id_aseguradora"
                StrSql = StrSql & " AND a.id_programa =" & intprograma & " "
                StrSql = StrSql & " AND v.Id_AonH = " & intaonv & ""
                StrSql = StrSql & " and v.estatus_vehiculo = '" & strestatus & "'"
                StrSql = StrSql & " AND a.estatus_programaaseguradora = '1' "
                StrSql = StrSql & " AND v.status_vehiculo ='1')"
            Else
                StrSql = StrSql & " and subramo=" & intsubramo & ""
            End If
            'fin micha
            'strsql = strsql & " and Id_Aseguradora=" & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub actualiza_numplazo_cob(ByVal intnumplazo As Integer, ByVal intcontrato As Integer, ByVal intbid As Integer)

        Try
            StrTransaccion = "ActualizaNumplazoCob"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = " update cobertura SET NUM_PLAZO =" & intnumplazo & ""
            StrSql = StrSql & " where Not id_cobertura = 0"
            StrSql = StrSql & " and Num_Contrato=" & intcontrato & ""
            StrSql = StrSql & " and id_bid=" & intbid & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function realiza_calculo_plazo_cobertura(ByVal numcontrato As Integer, ByVal intbid As Integer, ByVal intprograma As Integer) As DataTable

        Try
            StrSql = "select valida = isnull(ID_CAP,0), * from cobertura "
            StrSql = StrSql & " where Num_Contrato = " & numcontrato & ""
            StrSql = StrSql & " And id_bid = " & intbid & " "
            StrSql = StrSql & " And id_aseguradora in( select distinct id_aseguradora from abc_programa_aseguradora where id_programa =" & intprograma & ")"
            StrSql = StrSql & " And ORDEN_COB > 0"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function valida_COBERTURA_ADICIONAL_PRIMA(ByVal intCOBADIP As Integer) As Integer

        Try
            StrSql = "select ID_COBADIP from COBERTURA_ADICIONAL_PRIMA "
            StrSql = StrSql & " WHERE ID_COBADIP = " & intCOBADIP & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub actualiza_coberturas_plazo(ByVal dbtotal As Double, ByVal intcobertura As Integer, _
    ByVal numcontrato As Integer, ByVal intbid As Integer, ByVal intsubramo As Integer, ByVal intaseguradora As Integer)

        Try
            StrTransaccion = "ActualizaCobPlazo"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            StrSql = "UPDATE  COBERTURA  SET TOTAL_COB =" & dbtotal & ""
            StrSql = StrSql & " WHERE ID_COBERTURA = " & intcobertura & ""
            StrSql = StrSql & " AND NUM_CONTRATO = " & numcontrato & ""
            StrSql = StrSql & " AND ID_BID = " & intbid & ""
            StrSql = StrSql & " AND SUBRAMO = " & intsubramo & ""
            StrSql = StrSql & " AND ID_ASEGURADORA = " & intaseguradora & ""
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'mazda
    Public Function determina_banderas_X_programa(ByVal intprograma As Integer, ByVal intaseguradora As Integer) As DataTable

        Try
            StrSql = "select ban_deduc, ban_precontrol, ban_rate, ban_constante, ban_tipopoliza, "
            StrSql = StrSql & " ban_multianual, ban_ace, ban_vida, ban_iva, ban_estatus,"
            StrSql = StrSql & " recargo, ban_recargo, ban_factores, ban_doblecalculo,"
            StrSql = StrSql & " Ban_RecargoFraccion"
            StrSql = StrSql & " from ABC_PROGRAMA_ASEGURADORA"
            StrSql = StrSql & " where estatus_programaaseguradora ='1'"
            StrSql = StrSql & " and id_programa = " & intprograma & ""
            StrSql = StrSql & " and id_aseguradora = " & intaseguradora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function seguro_vida(ByVal intaseguradora As Integer, ByVal strmoneda As String, _
    ByVal intplazo As Integer, ByVal intregion As Integer) As DataTable

        Try
            StrSql = "SELECT factvida "
            StrSql = StrSql & " FROM seguro_vida "
            StrSql = StrSql & " WHERE id_aseguradora =" & intaseguradora & ""
            'strsql = strsql & " And id_version =" & intversion & ""
            StrSql = StrSql & " And MONEDA_vida = '" & strmoneda & "'"
            StrSql = StrSql & " And PLAZO_vida = " & intplazo & " "
            StrSql = StrSql & " And id_REGION = " & intregion & ""
            StrSql = StrSql & " And estatus_vida='1'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function Desempelo(ByVal intaseguradora As Integer, ByVal intmarca As Integer, _
    ByVal strmodelo As String, ByVal intanio As Integer, ByVal strmoneda As String) As DataTable

        Try
            StrSql = "SELECT MENS "
            StrSql = StrSql & " FROM DESEMPLEO "
            StrSql = StrSql & " WHERE id_aseguradora =" & intaseguradora & ""
            'strsql = strsql & " And id_version =" & intversion & ""
            StrSql = StrSql & " And id_marca= '" & intmarca & "'"
            StrSql = StrSql & " And modelo_desempleo = '" & strmodelo & "' "
            StrSql = StrSql & " And anio_desempleo = " & intanio & ""
            StrSql = StrSql & " And moneda_desempleo='" & strmoneda & "'"
            StrSql = StrSql & " And estatus_desempleo='1'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function Maximo_Cotizacion_Poliza() As Integer

        Try
            StrSql = "select valor = isnull(max(id_cotiza_poliza),0) + 1 "
            StrSql = StrSql & " from COTI_POLIZA"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function valida_auto_cotizacion(ByVal intaseguradora As Integer, ByVal intmarca As Integer, _
    ByVal strsubramo As String, ByVal strinsco As String, ByVal intaon As Integer, ByVal strestatus As String, _
    ByVal intanio As Integer, ByVal intsubmarca As Integer) As DataTable

        Try
            StrSql = " select id_vehiculo from vehiculo "
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and id_marca =" & intmarca & ""
            StrSql = StrSql & " and id_submarca =" & intsubmarca & ""
            StrSql = StrSql & " and subramo_vehiculo = '" & strsubramo & "'"
            StrSql = StrSql & " and insco_vehiculo ='" & strinsco & "'"
            StrSql = StrSql & " and id_aonh = " & intaon & ""
            StrSql = StrSql & " and estatus_vehiculo = '" & strestatus & "'"
            StrSql = StrSql & " and anio_vehiculo = " & intanio & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function carga_plazo(ByVal IdPrograma As Integer, ByVal opcion As String) As DataTable
        Try
            StrSql = "select id_plazo = 0, plazo  ='-- Seleccione --'"
            StrSql = StrSql & " union"
            StrSql = StrSql & " select id_plazo = p.plazo, plazo = cast(p.plazo as varchar)"
            StrSql = StrSql & " from ABC_PLAZO p, abc_programa_aseguradora a"
            StrSql = StrSql & " where p.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and a.id_programa = " & IdPrograma & ""
            If opcion <> "" Then
                StrSql = StrSql & " and p.plazo in (" & opcion & ")"
            End If
            StrSql = StrSql & " group by p.plazo"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidaPagoFraccionadoAseguradora(ByVal IdAseguradora As Integer, ByVal idtipo As Integer) As DataTable
        Try
            StrSql = "SELECT Id_Tiporecargo "
            StrSql = StrSql & " from ABC_FACTORRECARGO_VALIDA"
            StrSql = StrSql & " where id_aseguradora = " & IdAseguradora & ""
            StrSql = StrSql & " and Id_Tiporecargo = " & idtipo & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Valor_Pago_fraccionado(ByVal IdAseguradora As Integer, ByVal idtipo As Integer, ByVal numplazo As Integer) As Double
        Dim dt As DataTable
        Dim valor As Double = 0
        Dim sql As String = ""
        Try
            sql = "select recargo"
            sql = sql & " from ABC_FACTORRECARGO"
            sql = sql & " where id_aseguradora = " & IdAseguradora & ""
            sql = sql & " and Id_Tiporecargo = " & idtipo & ""
            sql = sql & " and num_pago = " & numplazo & ""
            dt = CConecta.AbrirConsulta(sql, True)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("recargo") Then
                    valor = dt.Rows(0)("recargo")
                End If
            End If
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#Region " Determinando calculos de Factores y tipo de carga"

    Public Function Recargos_tipocarga(ByVal IdAseguradora As Integer, ByVal IdTipoCarga As Integer, ByVal strmodelo As String) As DataTable
        Try
            StrSql = "SELECT recargo_tipocarga "
            StrSql = StrSql & " FROM VW_CARGATIPOCARGA"
            StrSql = StrSql & " WHERE id_aseguradora =" & IdAseguradora & ""
            StrSql = StrSql & " and MODELO ='" & strmodelo & "'"
            StrSql = StrSql & " AND Id_TipoCarga =" & IdTipoCarga & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Factores(ByVal intaseguradora As Integer, ByVal numfactor As Integer) As DataTable

        Try
            StrSql = "select valor = isnull(factor,0) "
            StrSql = StrSql & " from abc_factor"
            StrSql = StrSql & " where id_aseguradora = " & intaseguradora & ""
            StrSql = StrSql & " and estatus_factor ='1'"
            StrSql = StrSql & " and id_factor = " & numfactor & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

    Public Function Inst_Cotizacion_panel_12meses(ByVal strCadenaInsert As String, ByVal bandTipo As String) As Integer
        Dim Maxvalor As Integer = 0
        'Dim dt As DataTable
        Dim i As Integer
        Dim arr() As String
        Dim arr1() As String
        Dim ds As New DataSet
        Try

            'Esta consulta se incluye en el insert ya que estaba causando conflicto
            'If bandTipo = "C" Then
            '    StrSql = "select valor = isnull(max(Id_Cotizacion12mesesC),0) + 1 "
            '    StrSql = StrSql & " from COTIZACION_PANEL_12MESESCONTRATADO"
            'Else
            '    StrSql = "select valor = isnull(max(Id_Cotizacion12meses),0) + 1 "
            '    StrSql = StrSql & " from COTIZACION_PANEL_12MESES"
            'End If
            'dt = CConecta.AbrirConsulta(StrSql, True)
            'If dt.Rows.Count > 0 Then
            '    If Not dt.Rows(0).IsNull("valor") Then
            '        Maxvalor = dt.Rows(0)("valor")
            '    End If
            'End If

            StrTransaccion = "InsPanel12"

            arr = strCadenaInsert.Split("|")
            If arr.Length > 0 Then
                For i = 0 To arr.Length - 1
                    arr1 = arr(i).Split("*")
                    If bandTipo = "C" Then
                        If i = 0 Then
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql += " declare @idIns table(id int); "
                            StrSql = StrSql & " insert into COTIZACION_PANEL_12MESESCONTRATADO (Id_Cotizacion12mesesC,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, banderacompra)" & vbCrLf
                            StrSql = StrSql & " OUTPUT inserted.Id_Cotizacion12mesesC into @idIns select (Select isnull(max(Id_Cotizacion12mesesC), 0 ) + 1 from COTIZACION_PANEL_12MESESCONTRATADO With(NoLock))," & CInt(arr1(0)) & ",1," & vbCrLf
                            StrSql = StrSql & CInt(arr1(2)) & ",0," & CInt(arr1(1)) & ",'" & arr1(4) & "',0; "
                            StrSql += "select * from @idIns; "
                        Else
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql = StrSql & " insert into COTIZACION_PANEL_12MESESCONTRATADO (Id_Cotizacion12mesesC,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, banderacompra)" & vbCrLf
                            StrSql = StrSql & "values (" & Maxvalor & "," & CInt(arr1(0)) & ",1,"
                            StrSql = StrSql & CInt(arr1(2)) & ",0," & CInt(arr1(1)) & ",'" & arr1(4) & "',0)"
                        End If
                    Else
                        If i = 0 Then
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql += " declare @idIns table(id int); "
                            StrSql = StrSql & " insert into COTIZACION_PANEL_12MESES (Id_Cotizacion12meses,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, banderacompra)" & vbCrLf
                            StrSql = StrSql & " OUTPUT inserted.Id_Cotizacion12meses into @idIns select (Select isnull(max(Id_Cotizacion12meses), 0 ) + 1 from COTIZACION_PANEL_12MESES With(NoLock))," & CInt(arr1(0)) & ",1," & vbCrLf
                            StrSql = StrSql & CInt(arr1(2)) & ",0," & CInt(arr1(1)) & ",'" & arr1(4) & "',0; "
                            StrSql += "select * from @idIns; "
                        Else
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql = StrSql & " insert into COTIZACION_PANEL_12MESES (Id_Cotizacion12meses,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, banderacompra)" & vbCrLf
                            StrSql = StrSql & "values (" & Maxvalor & "," & CInt(arr1(0)) & ",1,"
                            StrSql = StrSql & CInt(arr1(2)) & ",0," & CInt(arr1(1)) & ",'" & arr1(4) & "',0)"
                        End If
                    End If
                    CConecta.Transaccion(StrTransaccion, True, False, True)
                    CConecta.EjecutaSQL(StrTransaccion, StrSql)
                    If i = 0 Then
                        ds = CConecta.AbrirConsulta(StrSql)
                        If ds.Tables(0).Rows.Count > 0 Then
                            Maxvalor = ds.Tables(0).Rows(0)(0)
                        End If
                    End If
                    CConecta.Transaccion(StrTransaccion, False, True, False, True)
                Next
            End If

            Return Maxvalor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Update_Cotizacion_panel_12meses(ByVal IdCotizacion12meses As Long, ByVal strCadenaInsert As String, ByVal bandTipo As String) As DataTable
        Dim dt As DataTable
        Dim i As Integer
        Dim arr() As String
        Dim arr1() As String
        Try
            StrTransaccion = "UpdPanel12"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            arr = strCadenaInsert.Split("|")
            If arr.Length > 0 Then
                If bandTipo = "C" Then
                    'StrSql = "update COTIZACION_PANEL_12mesesContratado set banderacompra = 1"
                    'StrSql = StrSql & " where id_cotizacion12meses = " & IdCotizacion12meses & " "
                    'StrSql = StrSql & " and id_aseguradora = " & CInt(arr1(0)) & ""
                    For i = 0 To arr.Length - 1
                        arr1 = arr(i).Split("*")

                        StrSql = "update COTIZACION_PANEL_12mesesContratado set primatotal = " & CDbl(arr1(2)) & ", "
                        StrSql = StrSql & " primaconsecutiva = 0, "
                        StrSql = StrSql & " idpaquete = " & CInt(arr1(1)) & " "
                        StrSql = StrSql & " where id_cotizacion12meses = " & IdCotizacion12meses & " "
                        StrSql = StrSql & " and id_aseguradora = " & CInt(arr1(0)) & ""
                        CConecta.EjecutaSQL(StrTransaccion, StrSql)
                    Next

                Else
                    For i = 0 To arr.Length - 1
                        arr1 = arr(i).Split("*")

                        StrSql = "update cotizacion_panel_12meses set primatotal = " & CDbl(arr1(2)) & ", "
                        StrSql = StrSql & " primaconsecutiva = 0, "
                        StrSql = StrSql & " idpaquete = " & CInt(arr1(1)) & " "
                        StrSql = StrSql & " where id_cotizacion12meses = " & IdCotizacion12meses & " "
                        StrSql = StrSql & " and id_aseguradora = " & CInt(arr1(0)) & ""
                        CConecta.EjecutaSQL(StrTransaccion, StrSql)
                    Next
                End If
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            If bandTipo = "C" Then
                StrSql = "select * from COTIZACION_PANEL_12mesesContratado "
                StrSql = StrSql & " where id_cotizacion12meses =" & IdCotizacion12meses & ""
                StrSql = StrSql & " and id_aseguradora =" & CInt(arr1(0)) & ""
            Else
                StrSql = "select * from cotizacion_panel_12meses "
                StrSql = StrSql & " where id_cotizacion12meses =" & IdCotizacion12meses & ""
                StrSql = StrSql & " and id_aseguradora =" & CInt(arr1(0)) & ""
            End If
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Upd_Cotizacion_panel_12meses(ByVal IdCotizacion As Integer, ByVal IdAseguradora As Integer, ByVal bandTipo As String)
        Try
            StrTransaccion = "UpdPanel12"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            If bandTipo = "C" Then
                StrSql = "update COTIZACION_PANEL_12mesesContratado set banderacompra = 1"
                StrSql = StrSql & " where id_cotizacion12mesesC = " & IdCotizacion & ""
                StrSql = StrSql & " and id_aseguradora = " & IdAseguradora & ""
            Else
                StrSql = "update COTIZACION_PANEL_12meses set banderacompra = 1"
                StrSql = StrSql & " where id_cotizacion12meses = " & IdCotizacion & ""
                StrSql = StrSql & " and id_aseguradora = " & IdAseguradora & ""
            End If
            CConecta.EjecutaSQL(StrTransaccion, StrSql)
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function Promocion12meses(ByVal IdCotizacion As Integer) As DataTable
        Try
            StrSql = "SELECT * "
            StrSql = StrSql & " FROM COTIZACION_PANEL_12meses"
            StrSql = StrSql & " where id_cotizacion12meses = " & IdCotizacion & ""
            StrSql = StrSql & " and banderacompra = 1"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Inst_Cotizacion_panel(ByVal strCadenaInsert As String, ByVal IdCotizaCobertura As Integer) As Integer

        Dim Maxvalor As Long = 0
        Dim dt As DataTable
        Dim i As Integer
        Dim arr() As String
        Dim arr1() As String
        Dim ds As New DataSet
        Try
            'Esta consulta se incluye en el insert ya que estaba causando conflicto
            'StrSql = "select valor = isnull(max(Id_CotizacionPanel),0) + 1 "
            'StrSql = StrSql & " from COTIZACION_PANEL"
            'dt = CConecta.AbrirConsulta(StrSql, True)
            'If dt.Rows.Count > 0 Then
            '    If Not dt.Rows(0).IsNull("valor") Then
            '        Maxvalor = dt.Rows(0)("valor")
            '    End If
            'End If

            StrTransaccion = "InsPanel"

            arr = strCadenaInsert.Split("|")
            If arr.Length > 0 Then
                For i = 0 To arr.Length - 1
                    arr1 = arr(i).Split("*")
                    If CInt(arr1(7)) = 1 Then

                        If i = 0 Or Maxvalor = 0 Then
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql += " declare @idIns table(id int); "
                            StrSql = StrSql & " insert into COTIZACION_PANEL (Id_CotizacionPanel,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, PrimaConsecutiva2)" & vbCrLf
                            'StrSql = StrSql & "values (" & Maxvalor & "," & CInt(arr1(0)) & "," & CInt(arr1(1)) & ", "
                            StrSql = StrSql & " OUTPUT inserted.Id_CotizacionPanel into @idIns select (Select isnull(max(Id_CotizacionPanel), 0 ) + 1 from COTIZACION_PANEL With(NoLock))," & CInt(arr1(0)) & "," & CInt(arr1(1)) & ", " & vbCrLf
                            StrSql = StrSql & CDbl(arr1(2)) & "," & CDbl(arr1(3)) & "," & CInt(arr1(4)) & ",'" & arr1(5) & "'," & CDbl(arr1(6)) & "; "
                            StrSql += "select * from @idIns; update cotiza_cobertura_id set Id_CotizacionPanel = (select * from @idIns) where Id_CotizaCobertura = " & IdCotizaCobertura & ";"

                            ds = CConecta.AbrirConsulta(StrSql)
                            If ds.Tables(0).Rows.Count > 0 Then
                                Maxvalor = ds.Tables(0).Rows(0)(0)
                            End If
                        Else
                            StrSql = "set dateformat dmy; " & vbCrLf
                            StrSql = StrSql & " insert into COTIZACION_PANEL (Id_CotizacionPanel,id_aseguradora, Id_TipoRecargo," & vbCrLf
                            StrSql = StrSql & " PrimaTotal, PrimaConsecutiva, IdPaquete, Subramo, PrimaConsecutiva2)" & vbCrLf
                            StrSql = StrSql & "values (" & Maxvalor & "," & CInt(arr1(0)) & "," & CInt(arr1(1)) & ", " & vbCrLf
                            StrSql = StrSql & CDbl(arr1(2)) & "," & CDbl(arr1(3)) & "," & CInt(arr1(4)) & ",'" & arr1(5) & "'," & CDbl(arr1(6)) & ");"

                            CConecta.Transaccion(StrTransaccion, True, False, True)
                            CConecta.EjecutaSQL(StrTransaccion, StrSql)
                        End If

                        'If i = 0 Or Maxvalor = 0 Then
                        '    ds = CConecta.AbrirConsulta(StrSql)
                        '    If ds.Tables(0).Rows.Count > 0 Then
                        '        Maxvalor = ds.Tables(0).Rows(0)(0)
                        '    End If
                        'End If
                        CConecta.Transaccion(StrTransaccion, False, True, False, True)
                    End If
                Next
            End If

            Return Maxvalor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Act_Cotizacion_panel(ByVal IdCotizacionPanel As Long, ByVal strCadenaInsert As String) As DataTable
        Dim i As Integer
        Dim arr() As String
        Dim arr1() As String
        Try

            StrTransaccion = "UpdPanel"
            CConecta.Transaccion(StrTransaccion, True, False, True)

            arr = strCadenaInsert.Split("|")
            If arr.Length > 0 Then
                For i = 0 To arr.Length - 1
                    arr1 = arr(i).Split("*")
                    If CInt(arr1(7)) = 1 Then
                        StrSql = "update cotizacion_panel set PrimaTotal =" & CLng(arr1(2)) & ", "
                        StrSql = StrSql & " PrimaConsecutiva =" & CLng(arr1(3)) & ", "
                        StrSql = StrSql & " PrimaConsecutiva2 =" & CLng(arr1(6)) & ", "
                        StrSql = StrSql & " IdPaquete =" & CInt(arr1(4)) & " "
                        StrSql = StrSql & " where Id_CotizacionPanel =" & IdCotizacionPanel & ""
                        StrSql = StrSql & " and id_aseguradora =" & CInt(arr1(0)) & ""
                        CConecta.EjecutaSQL(StrTransaccion, StrSql)
                    End If
                Next
            End If
            CConecta.Transaccion(StrTransaccion, False, True, False, True)

            StrSql = "select * from cotizacion_panel "
            StrSql = StrSql & " where Id_CotizacionPanel =" & IdCotizacionPanel & ""
            StrSql = StrSql & " and id_aseguradora =" & CInt(arr1(0)) & ""
            Return CConecta.AbrirConsulta(StrSql, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaCtoizaPanel(ByVal idcotizacion As Integer, ByVal IdAseguradora As Integer) As DataTable
        Try
            StrSql = "SELECT C.* FROM COTIZACION_PANEL C "
            StrSql = StrSql & " where C.id_cotizacionpanel = " & idcotizacion & " and C.Id_Aseguradora = " & IdAseguradora & _
                " order by C.Id_Aseguradora, C.IdPaquete,  C.Id_TipoRecargo "
            'StrSql = "SELECT *"
            'StrSql = StrSql & " FROM COTIZACION_PANEL "
            'StrSql = StrSql & " where id_cotizacionpanel = " & idcotizacion & ""
            'StrSql = StrSql & " and Id_Aseguradora = " & IdAseguradora & ""
            'StrSql = StrSql & " order by Id_Aseguradora, Id_TipoRecargo"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaCtoizaPanelAseguradora(ByVal idcotizacion As Integer) As DataTable
        Try
            StrSql = "SELECT Id_Aseguradora"
            StrSql = StrSql & " FROM COTIZACION_PANEL "
            StrSql = StrSql & " where id_cotizacionpanel = " & idcotizacion & ""
            StrSql = StrSql & " group by Id_Aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaTipoRecargo() As DataTable
        Try
            StrSql = "select id_tiporecargo, descripcion_periodo, Orden"
            StrSql = StrSql & " from ABC_TIPORECARGO"
            StrSql = StrSql & " order by orden"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaTipoRecargo(ByVal iIdAseguradora As Integer, ByVal iPrograma As Integer) As DataTable
        Try
            StrSql = "select id_tiporecargo, replace(replace(descripcion_periodo, 'Contado', '" & IIf(iPrograma = 2, "Contado", "Financiado") & " pago único'), 'Anual', 'Financiado pago anual') descripcion_periodo, Orden"
            StrSql = StrSql & " from ABC_TIPORECARGO tr inner join ABC_PAQUETE p on 1=1 where Id_Aseguradora = " & iIdAseguradora.ToString()
            StrSql = StrSql & " and Descripcion_Periodo LIKE '%' + CASE WHEN exists(select 1 from abc_programa where id_programa = " & iPrograma & " and descripcion_programa like '%CONTADO%') THEN 'CONTADO' ELSE '' END + '%' "
            StrSql = StrSql & " order by Id_Paquete, Id_TipoRecargo"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaPaquete(ByVal iIdAseguradora As Integer) As DataTable
        Try
            StrSql = "select id_paquete, descripcion_paquete "
            StrSql = StrSql & " from ABC_PAQUETE p where Id_Aseguradora = " & iIdAseguradora.ToString()
            StrSql = StrSql & " order by Id_Paquete"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaTipoRecargo_AutoOpcion(ByVal idcotizacionPanel As Integer, ByVal idaseguradora As Integer) As DataTable
        Try
            StrSql = "select t.id_tiporecargo, t.descripcion_periodo, t.Orden"
            StrSql = StrSql & " from ABC_TIPORECARGO t, COTIZACION_PANEL p"
            StrSql = StrSql & " where id_cotizacionpanel = " & idcotizacionPanel & ""
            StrSql = StrSql & " and id_aseguradora = " & idaseguradora & ""
            StrSql = StrSql & " and t.id_tiporecargo = p.id_tiporecargo"
            StrSql = StrSql & " order by t.orden"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Carga_AbcPromocion(ByVal IdAseguradora As Integer, ByVal strmodelo As String) As DataTable
        Try
            StrSql = "SELECT modelo = isnull(modelo_nuevo,'')"
            StrSql = StrSql & " FROM ABC_PROMOCION"
            StrSql = StrSql & " where id_aseguradora = " & IdAseguradora & ""
            StrSql = StrSql & " and modelo_original = '" & strmodelo & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'evi 10/12/2013 se agrega cotizacion_unica
    'HM 20/02/2014 se agrega fecha de inicio vigencia
    Public Function inserta_cotizacion_unica(ByVal IdCotiza12 As Integer, ByVal IdCotiza As Integer, ByVal IntContrato As Integer, ByVal IdBid As Integer, ByVal IdAonh As Integer, _
                                             ByVal StrModelo As String, ByVal IntAnio As Integer, ByVal DbSA As Double, ByVal IntPlazo As Integer, ByVal IntPlazorestante As Integer, _
                                             ByVal IdMarca As Integer, ByVal IdPrograma As Integer, ByVal Exclusion As Integer, ByVal IdPersona As Integer, ByVal strestatus As String, _
                                             ByVal StrFecIni As String, ByVal StrModeloC As String, ByVal IntanioC As Integer, ByVal DbSAC As Double, ByVal DifSA As Double, _
                                             ByVal IntCuotas As Integer, ByVal IntMesesTranscurridos As Integer, ByVal IntMesesSeguroGratis As Integer, ByVal IdAonhC As Integer, _
                                             ByVal IdCotiza12C As Integer, ByVal strCodigoPostal As String) As Long
        Dim Maxvalor As Long = 0
        Dim dt As New DataTable
        Try

            StrTransaccion = "InsCotUni"

            Dim sysFormat As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            Dim sysUIFormat As String = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern
            Dim arrFechaIni As String()
            If ConfigurationManager.AppSettings("Bandera_Fecha").ToString() = "1" Then
                arrFechaIni = StrFecIni.Split("/")
                StrFecIni = arrFechaIni(1) & "/" & arrFechaIni(0) & "/" & arrFechaIni(2)
            End If

            'StrSql = "set dateformat dmy Exec INS_COTIZACION_UNICA " & IdCotiza12 & "," & IdCotiza & "," & IntContrato & ", " & IdBid & ", " & IdAonh & ",'" & StrModelo & "', " & IntAnio & ", " & DbSA & "," & IntPlazo & "," & IntPlazorestante & "," & IdMarca & "," & IdPrograma & "," & Exclusion & "," & IdPersona & ",'" & strestatus & "','" & StrFecIni & "','" & StrModeloC & "'," & IntanioC & "," & DbSAC & "," & DifSA & "," & IntCuotas & "," & IntMesesTranscurridos & "," & IntMesesSeguroGratis & ", " & IdAonhC & ", " & IdCotiza12C
            StrSql = " Exec INS_COTIZACION_UNICA " & IdCotiza12 & "," & IdCotiza & "," & IntContrato & ", " & IdBid & ", " & IdAonh & ",'" & StrModelo & "', " & IntAnio & ", " & DbSA & "," & IntPlazo & "," & IntPlazorestante & "," & IdMarca & "," & IdPrograma & "," & Exclusion & "," & IdPersona & ",'" & strestatus & "','" & StrFecIni & "','" & StrModeloC & "'," & IntanioC & "," & DbSAC & "," & DifSA & "," & IntCuotas & "," & IntMesesTranscurridos & "," & IntMesesSeguroGratis & ", " & IdAonhC & ", " & IdCotiza12C & " , '" & strCodigoPostal & "'"
            Try
                dt = CConecta.AbrirConsulta(StrSql, True)
            Catch ex As Exception
                arrFechaIni = StrFecIni.Split("/")
                StrFecIni = arrFechaIni(1) & "/" & arrFechaIni(0) & "/" & arrFechaIni(2)
                StrSql = " Exec INS_COTIZACION_UNICA " & IdCotiza12 & "," & IdCotiza & "," & IntContrato & ", " & IdBid & ", " & IdAonh & ",'" & StrModelo & "', " & IntAnio & ", " & DbSA & "," & IntPlazo & "," & IntPlazorestante & "," & IdMarca & "," & IdPrograma & "," & Exclusion & "," & IdPersona & ",'" & strestatus & "','" & StrFecIni & "','" & StrModeloC & "'," & IntanioC & "," & DbSAC & "," & DifSA & "," & IntCuotas & "," & IntMesesTranscurridos & "," & IntMesesSeguroGratis & ", " & IdAonhC & ", " & IdCotiza12C & " , '" & strCodigoPostal & "'"
                dt = CConecta.AbrirConsulta(StrSql, True)
            End Try

            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0).IsNull("MAXIMO") Then
                    Maxvalor = dt.Rows(0)("MAXIMO")
                End If
            End If
            Return Maxvalor

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'HMH
    Public Function Prima12meses(ByVal idcotizacion12meses As Long) As DataTable
        Try
            StrSql = "select c.*, a.descripcion_aseguradora, a.url_imagenPeque "
            StrSql = StrSql & " from COTIZACION_PANEL_12MESES c, abc_aseguradora a "
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.Id_Cotizacion12meses = " & idcotizacion12meses & " "
            StrSql = StrSql & " And c.banderacompra = 1 "
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function PrimaMultianual(ByVal idcotizacionmultianul As Long, ByVal IdASeguardora As Integer) As DataTable
        Try
            StrSql = "select c.*, a.descripcion_aseguradora, a.url_imagenPeque "
            StrSql = StrSql & " from COTIZACION_PANEL c, abc_aseguradora a"
            StrSql = StrSql & " where c.id_aseguradora = a.id_aseguradora"
            StrSql = StrSql & " and c.id_cotizacionpanel = " & idcotizacionmultianul & " "
            StrSql = StrSql & " And c.id_aseguradora = " & IdASeguardora & ""
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CalculoSP(ByVal sSpName As String, ByVal sParameters As String, iCotizacion As Integer, ByRef IdCotizaCobertura As Integer) As String
        Dim sCadenaResp As String = ""
        Dim dsResp As New DataSet()
        Dim drResp As DataRow
        Try
            dsResp = CConecta.EjecutaSP(sSpName, sParameters)
            For Each drResp In dsResp.Tables(0).Rows
                sCadenaResp = sCadenaResp & drResp(0).ToString()
            Next
            If iCotizacion = 1 Then
                sCadenaResp = sCadenaResp.Substring(0, sCadenaResp.Length - 1) & "$" & dsResp.Tables(1).Rows(0)(0).ToString()
            Else
                sCadenaResp = sCadenaResp.Substring(0, sCadenaResp.Length - 1) & "$"
                For Each drResp In dsResp.Tables(1).Rows
                    sCadenaResp = sCadenaResp & drResp(0).ToString()
                Next
                sCadenaResp = sCadenaResp.Substring(0, sCadenaResp.Length - 1)
            End If

            IdCotizaCobertura = dsResp.Tables(2).Rows(0)(0)

            Return sCadenaResp
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Accesorio_Calculo(ByVal IdAseguradora As Integer, ByVal strmodelo As String) As DataTable
        Try
            StrSql = "SELECT * "
            StrSql = StrSql & " FROM ABC_Accesorio_Calculo"
            StrSql = StrSql & " WHERE id_aseguradora =" & IdAseguradora & ""
            StrSql = StrSql & " and Modelo_Calculo ='" & strmodelo & "'"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CargaCobertura(ByVal iIdAseguradora As Integer, ByVal iIdPaquete As Integer) As DataTable
        Try
            StrSql = "select cobertura, descripcion_suma,deducible "
            StrSql = StrSql & " from LIMRESP p where Id_Aseguradora = " & iIdAseguradora.ToString()
            StrSql = StrSql & " and Id_Paquete = " & iIdPaquete & ""
            StrSql = StrSql & " order by Id_Aseguradora"
            Return CConecta.AbrirConsulta(StrSql, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
