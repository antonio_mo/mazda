﻿Public Class CdSeguridad
    Private Const nameConnection As String = "cnx"
    Private Const C_PassPhrase As String = "Pas5pr@se"
    Private Const C_SaltValue As String = "s@1tValue"
    Private Const C_HashAlgorithm As String = "SHA1"
    Private Const C_PasswordIterations As Integer = 2
    Private Const C_InitVector As String = "@1B2c3D4e5F6g7H8"
    Private Const C_KeySize As Integer = 256

    Public Shared Function ObtenerCadenaConexion() As String
        Return j.GAM.Utility.CCrypto.Decrypt(System.Configuration.ConfigurationManager.AppSettings("StrConexion").ToString(), C_PassPhrase,
            C_SaltValue, C_HashAlgorithm, C_PasswordIterations, C_InitVector, C_KeySize)
    End Function
    
End Class
