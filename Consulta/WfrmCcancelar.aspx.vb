Imports CN_Negocios

Partial Class WfrmCcancelar
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalidar As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                'carga_cancelacion()
                If Session("nivel") = 0 Then
                    valida_deeler()
                Else
                    pnl_deeler.Visible = False
                End If
            End If

            If Session("Programa") = 32 Then
                chkLCriterio.Visible = False
                chkLCriterioM.Visible = True
            Else
                chkLCriterio.Visible = True
                chkLCriterioM.Visible = False
            End If
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub valida_deeler()
        Dim dt As DataTable = ccliente.determina_bid_consultas(Session("usuario"), Session("programa"), Session("intmarcabid"))
        If dt.Rows.Count > 0 Then
            cbo_deeler.DataSource = dt
            cbo_deeler.DataTextField = "nombre"
            cbo_deeler.DataValueField = "id_bid"
            cbo_deeler.DataBind()
            pnl_deeler.Visible = True
        Else
            pnl_deeler.Visible = False
        End If
    End Sub

    'Public Sub carga_cancelacion()
    '    Dim dt As DataTable = ccliente.consulta_cancelacion(Session("bid"))
    '    Try
    '        If dt.Rows.Count > 0 Then
    '            grd_cancelacion.DataSource = dt
    '            grd_cancelacion.DataBind()
    '        End If
    '    Catch ex As Exception
    '        MsgBox.ShowMessage(ex.Message)
    '    End Try
    'End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        carga_consulta()
    End Sub

    Public Sub carga_consulta()
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try
            StrCriterios = ""
            If Session("Programa") = 32 Then
                For i = 0 To chkLCriterioM.Items.Count - 1
                    If chkLCriterioM.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterioM.Items(i).Value) & "|"
                    End If
                Next
            Else
                For i = 0 To chkLCriterio.Items.Count - 1
                    If chkLCriterio.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                    End If
                Next
            End If
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If

            If pnl_deeler.Visible = False Then
                intbid = Session("bid")
            Else
                If cbo_deeler.SelectedValue = 0 Then
                    'intbid = Session("bid")
                    intbid = 0
                Else
                    intbid = cbo_deeler.SelectedValue
                End If
            End If


            dt = ccliente.BuscaPoliza_cancelada(intbid, Session("programa"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                grd_cancelacion.DataSource = dt
                grd_cancelacion.DataBind()
            Else
                MsgBox.ShowMessage("No hay registros con esos criterios de busqueda")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cancelacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cancelacion.SelectedIndexChanged

    End Sub
End Class
