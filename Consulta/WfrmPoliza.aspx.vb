Imports CN_Negocios

Partial Class WfrmPoliza
    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private ccliente As New CnCotizador
    Private cvalidar As New CnPrincipal

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina

        Try
            If Session("usuario") = "" Or Session("usuario") = Nothing Then
                Response.Write("<script>window.open('../Principal/wfrmlogooff.aspx','_parent');</script>")
            End If

            If Not Page.IsPostBack Then
                pnl_datos.Visible = False
                limpia()
                If Session("nivel") = 0 Then
                    valida_deeler()
                Else
                    pnl_deeler.Visible = False
                End If
            End If
            'paneles Contado
            If Session("programa") = 7 Or Session("programa") = 8 _
            Or Session("programa") = 12 Or Session("programa") = 13 Then
                lbl_aseguradora.Visible = True
                lbl_aseguradorad.Visible = True
            Else
                lbl_aseguradora.Visible = False
                lbl_aseguradorad.Visible = False
            End If

            If Session("Programa") = 32 Then
                chkLCriterio.Visible = False
                chkLCriterioM.Visible = True
            Else
                chkLCriterio.Visible = True
                chkLCriterioM.Visible = False
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Public Sub valida_deeler()
        Dim dt As DataTable = ccliente.determina_bid_consultas(Session("usuario"), Session("programa"), Session("intmarcabid"))
        If dt.Rows.Count > 0 Then
            cbo_deeler.DataSource = dt
            cbo_deeler.DataTextField = "nombre"
            cbo_deeler.DataValueField = "id_bid"
            cbo_deeler.DataBind()
            pnl_deeler.Visible = True
        Else
            pnl_deeler.Visible = False
        End If
    End Sub

    'Public Sub carga_grd()
    '    Dim dt As DataTable = ccliente.carga_cancelacion(Session("bid"), Session("programa"))
    '    If dt.Rows.Count > 0 Then
    '        grd_cancelacion.DataSource = dt
    '        grd_cancelacion.DataBind()
    '    End If
    'End Sub

    Public Sub limpia()
        lbl_poliza.Text = ""
        lbl_finicio.Text = ""
        lbl_ffin.Text = ""
        lbl_fregistro.Text = ""
        lbl_descripcion.Text = ""
        lbl_modelo.Text = ""
        lbl_marca.Text = ""
        lbl_anio.Text = ""
        lbl_serie.Text = ""
        lbl_renave.Text = ""
        lbl_capacidad.Text = ""
        lbl_ptotal.Text = ""
        lbl_pneta.Text = ""
        lbl_placa.Text = ""
        lbl_motor.Text = ""
        lbl_plazo.Text = ""
        lbl_precio.Text = ""
        lbl_seguro.Text = ""
        lbl_user.Text = ""
        lbl_aseg.Text = ""
    End Sub

    Public Sub consulta_vehiculo(ByVal strpoliza As String, ByVal intbid As Integer)
        Dim Total As Double
        'Dim dt As DataTable = ccliente.consulta_polzas(strpoliza, Session("bid"))
        Dim dt As DataTable = ccliente.consulta_polzas(strpoliza, intbid)
        If dt.Rows.Count > 0 Then
            lbl_poliza.Text = strpoliza
            If dt.Rows(0).IsNull("descripcion_aseguradora") Then
                lbl_aseg.Text = ""
            Else
                lbl_aseg.Text = dt.Rows(0)("descripcion_aseguradora")
            End If
            If dt.Rows(0).IsNull("fvigencia") Then
                lbl_finicio.Text = ""
            Else
                lbl_finicio.Text = dt.Rows(0)("fvigencia")
            End If
            If dt.Rows(0).IsNull("ffin") Then
                lbl_ffin.Text = ""
            Else
                lbl_ffin.Text = dt.Rows(0)("ffin")
            End If
            If dt.Rows(0).IsNull("fecha_registro") Then
                lbl_fregistro.Text = ""
            Else
                lbl_fregistro.Text = dt.Rows(0)("fecha_registro")
            End If
            If dt.Rows(0).IsNull("descripcion") Then
                lbl_descripcion.Text = ""
            Else
                lbl_descripcion.Text = dt.Rows(0)("descripcion")
            End If
            If dt.Rows(0).IsNull("modelo") Then
                lbl_modelo.Text = ""
            Else
                lbl_modelo.Text = dt.Rows(0)("modelo")
            End If
            If dt.Rows(0).IsNull("marca") Then
                lbl_marca.Text = ""
            Else
                lbl_marca.Text = dt.Rows(0)("marca")
            End If
            If dt.Rows(0).IsNull("anio") Then
                lbl_anio.Text = ""
            Else
                lbl_anio.Text = dt.Rows(0)("anio")
            End If
            If dt.Rows(0).IsNull("serie") Then
                lbl_serie.Text = ""
            Else
                lbl_serie.Text = dt.Rows(0)("serie")
            End If
            If dt.Rows(0).IsNull("renave") Then
                lbl_renave.Text = ""
            Else
                lbl_renave.Text = dt.Rows(0)("renave")
            End If
            If dt.Rows(0).IsNull("capacidad") Then
                lbl_capacidad.Text = ""
            Else
                lbl_capacidad.Text = dt.Rows(0)("capacidad")
            End If
            If dt.Rows(0).IsNull("primatotal") Then
                lbl_ptotal.Text = ""
            Else
                Total = dt.Rows(0)("primatotal")
                lbl_ptotal.Text = Format(Total, "##,##0.00")
            End If
            If dt.Rows(0).IsNull("primaneta") Then
                lbl_pneta.Text = ""
            Else
                Total = dt.Rows(0)("primaneta")
                lbl_pneta.Text = Format(Total, "##,##0.00")
            End If
            If dt.Rows(0).IsNull("placas") Then
                lbl_placa.Text = ""
            Else
                lbl_placa.Text = dt.Rows(0)("placas")
            End If
            If dt.Rows(0).IsNull("motor") Then
                lbl_motor.Text = ""
            Else
                lbl_motor.Text = dt.Rows(0)("motor")
            End If
            If dt.Rows(0).IsNull("plazo") Then
                lbl_plazo.Text = ""
            Else
                lbl_plazo.Text = dt.Rows(0)("plazo")
            End If
            If dt.Rows(0).IsNull("precio_vehiculo") Then
                lbl_precio.Text = ""
            Else
                lbl_precio.Text = Format(dt.Rows(0)("precio_vehiculo"), "$#,###,##0.00")
            End If
            If dt.Rows(0).IsNull("tipo_poliza") Then
                lbl_seguro.Text = ""
            Else
                If dt.Rows(0)("tipo_poliza") = "A" Then
                    lbl_seguro.Text = "Anual"
                Else
                    lbl_seguro.Text = "MultiAnual"
                End If
            End If

            'If Session("nivel") = 0 Then
            lbl_user.Visible = True
            lbl_usuario.Visible = True
            If dt.Rows(0).IsNull("nombre") Then
                lbl_user.Text = ""
            Else
                lbl_user.Text = dt.Rows(0)("nombre")
            End If
            'Else
            '    lbl_user.Visible = False
            '    lbl_usuario.Visible = False
            'End If

            'hp_poliza.NavigateUrl = "WfrmPoliza.aspx?pol=" & lbl_poliza.Text & " &tipo=P"
            'hp_cotizacion.NavigateUrl = "WfrmPoliza.aspx?pol=" & lbl_poliza.Text & " &tipo=C"

        End If
    End Sub

    'Private Sub grd_cancelacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cancelacion.SelectedIndexChanged
    '    lbl_poliza.Text = grd_cancelacion.Items(grd_cancelacion.SelectedIndex).Cells(1).Text
    '    consulta_vehiculo()
    '    pnl_datos.Visible = True
    'End Sub

    Private Sub grd_cancelacion_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grd_cancelacion.ItemCommand
        Dim strpoliza As String = e.Item.Cells(1).Text
        Dim intbid As Integer = e.Item.Cells(11).Text
        Dim strtipo As String = ""
        Try
            Select Case e.CommandName
                Case "Da�os"
                    strtipo = "P"
                Case "Vida"
                    strtipo = "V"
                Case "Desempleo"
                    strtipo = "D"
            End Select
            If Not strtipo = "" Then

                'HM 07/08/2014 
                'Se agrego validaci�n de si el componente esta visible o no para los casos de perfil emisor
                'en donde tienen asignado solo un distribuidor o bid
                If cbo_deeler.Visible = False Then
                    intbid = Session("bid")
                Else
                    If Not cbo_deeler.SelectedItem.Value = "0" Then
                        intbid = Session("bid")
                    End If
                End If

                Dim dt As DataTable = ccliente.contenedor_poliza_general(strpoliza, intbid, strtipo)
                If dt.Rows.Count > 0 Then
                    Dim url As String = ""
                    If Not dt.Rows(0).IsNull("url_reporte") Then
                        url = dt.Rows(0)("url_reporte")
                        ''''url = "C:/Inetpub/wwwroot/CotizadorContado_Conauto_QA/" & url
                        'url = "D:/Aplicaciones/CotizadorContado_Conauto/" & url
                        'sd.FileName = "sdfsd4543dfgxcvnbbvdfg4576834vjhjkscvx76"
                        'sd.File = url
                        'sd.ShowDialogBox()

                        url = ConfigurationSettings.AppSettings("Raiz") & url
                        If System.IO.File.Exists(url) Then
                            Dim nombre As String = "sdfsd4543dfgxcvnbbvdfg4576834vjhjkscvx76.pdf"
                            Response.Buffer = False
                            Response.Clear()
                            Response.ClearContent()
                            Response.ClearHeaders()
                            Response.ContentType = "application/pdf"
                            Response.AddHeader("Content-Disposition", "attachment; filename=""" & nombre & "")
                            Response.TransmitFile(url)
                            Response.End()
                        Else
                            MsgBox.ShowMessage("Favor de consultar a su administrador,\n la caratula de la p�liza no se genero")
                        End If
                    End If
                    Else
                        MsgBox.ShowMessage("Favor de consultar a su administrador,\n la caratula de la p�liza no se genero")
                    End If
                    carga_consulta()
                Else
                    consulta_vehiculo(strpoliza, intbid)
                    pnl_datos.Visible = True
                End If
        Catch ex As IndexOutOfRangeException
            MsgBox.ShowMessage("Favor de consultar a su administrador,\n la caratula de la p�liza no se genero")
        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        carga_consulta()
    End Sub

    Public Sub carga_consulta()
        Dim dt As DataTable
        Dim StrCriterios As String
        Dim Seleccion As Boolean
        Dim i As Integer
        Dim intbid As Integer = 0
        Try
            StrCriterios = ""
            If Session("Programa") = 32 Then
                For i = 0 To chkLCriterioM.Items.Count - 1
                    If chkLCriterioM.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterioM.Items(i).Value) & "|"
                    End If
                Next
            Else
                For i = 0 To chkLCriterio.Items.Count - 1
                    If chkLCriterio.Items(i).Selected = True Then
                        Seleccion = True
                        StrCriterios = StrCriterios & UCase(chkLCriterio.Items(i).Value) & "|"
                    End If
                Next
            End If
            txtCriterio.Text = cvalidar.valida_cadena(txtCriterio.Text)

            If txtCriterio.Text = "" And Seleccion = True Then
                MsgBox.ShowMessage("Escriba la cadena de b�squeda")
                Exit Sub
            End If

            If txtCriterio.Text <> "" And Seleccion = False Then
                MsgBox.ShowMessage("Selecccione un criterio de b�squeda")
                Exit Sub
            End If

            If pnl_deeler.Visible = False Then
                intbid = Session("bid")
            Else
                If cbo_deeler.SelectedValue = 0 Then
                    'intbid = Session("bid")
                    intbid = 0
                Else
                    intbid = cbo_deeler.SelectedValue
                End If
            End If

            dt = ccliente.BuscaPoliza(intbid, Session("programa"), txtCriterio.Text, StrCriterios)
            If dt.Rows.Count > 0 Then
                grd_cancelacion.DataSource = dt
                grd_cancelacion.DataBind()
                pnl_datos.Visible = False
            Else
                MsgBox.ShowMessage("No hay registros con esos criterios de busqueda")
            End If

        Catch ex As Exception
            MsgBox.ShowMessage(ex.Message)
        End Try
    End Sub

    Private Sub grd_cancelacion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_cancelacion.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or _
        e.Item.ItemType = ListItemType.AlternatingItem Then
            If ccliente.banVida = 0 Then
                grd_cancelacion.Columns(9).Visible = False
            Else
                grd_cancelacion.Columns(9).Visible = True
            End If
            If ccliente.bandes = 0 Then
                grd_cancelacion.Columns(10).Visible = False
            Else
                grd_cancelacion.Columns(10).Visible = True
            End If
        End If
    End Sub

    Private Sub grd_cancelacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grd_cancelacion.SelectedIndexChanged

    End Sub
End Class
