<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmPoliza.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmPoliza" %>
<%@ Register TagPrefix="ontranet" Namespace="Ontranet.WebControls" Assembly="Ontranet.WebControls.SaveDialog" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="negrita_marco_color" vAlign="middle" align="center" width="2%" height="20"><asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Consulta de 
							P�lizas</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD class="negrita" colSpan="4"><asp:panel id="pnl_deeler" runat="server" Width="100%">
							<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="obligatorio" width="25%">Seleccione Distribuidor :</TD>
									<TD width="50%">
										<asp:DropDownList id="cbo_deeler" runat="server" Width="100%" CssClass="combos_small"></asp:DropDownList></TD>
									<TD width="25%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="Header_Gris">B�squeda
					</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="15%"><asp:textbox id="txtCriterio" tabIndex="1" runat="server" Width="100%" CssClass="combos_small"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="80%" colSpan="2"><asp:checkboxlist id="chkLCriterio" runat="server" Width="488px" CssClass="combos_small" RepeatColumns="4"
										RepeatDirection="Horizontal" Height="56px">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="2">Motor</asp:ListItem>
										<asp:ListItem Value="3">Placas</asp:ListItem>
										<asp:ListItem Value="4">Marca</asp:ListItem>
										<asp:ListItem Value="5">Modelo</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist>
									<asp:checkboxlist id="chkLCriterioM" runat="server" CssClass="combos_small" Width="164px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="2">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3"><asp:button id="cmdBuscar" runat="server" CssClass="boton" Height="24px" Text="Buscar"></asp:button></TD>
				</TR>
				<TR>
					<TD width="25%" colSpan="4"><asp:datagrid id="grd_cancelacion" runat="server" Width="100%" CssClass="datagrid" AllowSorting="True"
							CellPadding="1" AutoGenerateColumns="False">
							<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
							<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
									HeaderText="Seleccionar" CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
									<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:BoundColumn DataField="Poliza" HeaderText="P&#243;liza">
									<ItemStyle Wrap="False" Width="15%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Num_Contrato" HeaderText="No. Contrato">
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Serie" HeaderText="Serie">
									<ItemStyle Wrap="False" Width="15%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Marca" HeaderText="Marca">
									<ItemStyle Wrap="False" Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Modelo" ReadOnly="True" HeaderText="Modelo">
									<ItemStyle Wrap="False" Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="anio" ReadOnly="True" HeaderText="A&#241;o">
									<ItemStyle Wrap="False" Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="descripcion" ReadOnly="True" HeaderText="Descripci&#243;n">
									<ItemStyle Wrap="False" Width="20%"></ItemStyle>
								</asp:BoundColumn>
								<asp:ButtonColumn Text="&lt;img src='..\Imagenes\menu\page_down.gif' border='0'&gt;" HeaderText="Reimprimir P&#243;liza"
									CommandName="Da&#241;os">
									<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
								</asp:ButtonColumn>
								<asp:ButtonColumn Text="&lt;img src='..\Imagenes\menu\page_down.gif' border='0'&gt;" HeaderText="Reimprimir Vida"
									CommandName="Vida">
									<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
								</asp:ButtonColumn>
								<asp:ButtonColumn Text="&lt;img src='..\Imagenes\menu\page_down.gif' border='0'&gt;" HeaderText="Reimprimir Desempleo"
									CommandName="Desempleo">
									<ItemStyle HorizontalAlign="Center" Width="5%" VerticalAlign="Middle"></ItemStyle>
								</asp:ButtonColumn>
								<asp:BoundColumn Visible="False" DataField="id_bid" HeaderText="id_bid"></asp:BoundColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
					<TD class="obligatorio"></TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:panel id="pnl_datos" runat="server" Width="100%">
							<TABLE id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="negrita" align="right" width="20%">P�liza :</TD>
									<TD width="20%">
										<asp:Label id="lbl_poliza" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right" width="20%">Fecha Fin :</TD>
									<TD width="20%">
										<asp:Label id="lbl_ffin" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD width="20%"></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Fecha Inicio :</TD>
									<TD>
										<asp:Label id="lbl_finicio" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Fecha de Registro :</TD>
									<TD>
										<asp:Label id="lbl_fregistro" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Marca :</TD>
									<TD>
										<asp:Label id="lbl_marca" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Descripci�n :</TD>
									<TD class="obligatorio" colspan="2" rowspan="2" valign="top">
                                        <asp:Label ID="lbl_descripcion" runat="server" CssClass="small" Width="100%"></asp:Label>
                                    </TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Modelo :</TD>
									<TD>
										<asp:Label id="lbl_modelo" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD colSpan="1">
										&nbsp;</TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">A�o :</TD>
									<TD>
										<asp:Label id="lbl_anio" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Renave:</TD>
									<TD>
										<asp:Label id="lbl_renave" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">No. Serie :</TD>
									<TD>
										<asp:Label id="lbl_serie" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Capacidad :</TD>
									<TD>
										<asp:Label id="lbl_capacidad" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Motor :</TD>
									<TD>
										<asp:Label id="lbl_motor" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Prima Neta :</TD>
									<TD>
										<asp:Label id="lbl_pneta" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Placas :</TD>
									<TD>
										<asp:Label id="lbl_placa" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Prima Total :</TD>
									<TD>
										<asp:Label id="lbl_ptotal" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Plazo Financiamiento :</TD>
									<TD>
										<asp:Label id="lbl_plazo" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Seguro de Da�os :</TD>
									<TD>
										<asp:Label id="lbl_seguro" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">Precio Vehiculo :</TD>
									<TD>
										<asp:Label id="lbl_precio" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita" align="right">Aseguradora :</TD>
									<TD>
										<asp:Label id="lbl_aseg" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD class="negrita" align="right">
										<asp:Label id="lbl_aseguradora" runat="server" Width="100%" CssClass="negrita" align="right">Aseguradora :</asp:Label></TD>
									<TD>
										<asp:Label id="lbl_aseguradorad" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD class="negrita"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right">
										<asp:Label id="lbl_usuario" runat="server" Width="100%" CssClass="negrita" align="right">Usuario :</asp:Label></TD>
									<TD vAlign="top" colSpan="2">
										<asp:Label id="lbl_user" runat="server" Width="100%" CssClass="small"></asp:Label></TD>
									<TD vAlign="top" align="left"></TD>
									<TD vAlign="top" align="left"></TD>
								</TR>
								<TR>
									<TD vAlign="top"></TD>
									<TD vAlign="top" colSpan="2"></TD>
									<TD vAlign="top" align="left"></TD>
									<TD vAlign="top" align="left"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD></TD>
									<TD></TD>
									<TD align="right"></TD>
									<TD align="right"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:msgbox id="MsgBox" style="Z-INDEX: 104; LEFT: 360px; POSITION: absolute; TOP: 936px" runat="server"></cc1:msgbox><ONTRANET:SAVEDIALOG id="sd" runat="server"></ONTRANET:SAVEDIALOG></form>
	</body>
</HTML>
