<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WfrmCcancelar.aspx.vb" Inherits="Cotizador_Mazda_Retail.WfrmCcancelar" %>
<%@ Register TagPrefix="cc1" Namespace="MsgBox" Assembly="MsgBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="..\JavaScript\Jscripts.js"></script>
		<LINK href="..\Css\Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<HR class="lineas" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 40px" width="100%">
			<TABLE id="Table4" style="Z-INDEX: 110; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 24px"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="2%" height="20" class="negrita_marco_color" vAlign="middle" align="center">
						<asp:image id="Image2" runat="server" ImageUrl="..\Imagenes\cob.gif"></asp:image></TD>
					<TD class="Interiro_tabla" width="98%" colSpan="3"><FONT size="+0">&nbsp;Consulta de 
							Cancelaci�n de P�liza</FONT></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 48px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
					<TD width="25%"></TD>
				</TR>
				<TR>
					<TD colSpan="4">
						<asp:panel id="pnl_deeler" runat="server" Width="100%">
							<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD class="obligatorio" width="25%">Seleccione Distribuidor :</TD>
									<TD width="50%" colSpan="2">
										<asp:DropDownList id="cbo_deeler" runat="server" Width="100%" CssClass="combos_small"></asp:DropDownList></TD>
									<TD width="25%"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="Header_Gris">B�squeda
					</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD class="obligatorio" vAlign="top" align="right">Por Criterio :&nbsp;</TD>
					<TD vAlign="top" colSpan="3">
						<TABLE id="Table3" style="HEIGHT: 16px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD vAlign="top" width="15%">
									<asp:textbox id="txtCriterio" tabIndex="1" runat="server" CssClass="combos_small" Width="100%"
										MaxLength="50"></asp:textbox></TD>
								<TD class="negrita" vAlign="top" align="right" width="5%"></TD>
								<TD vAlign="top" width="80%" colSpan="2">
									<asp:checkboxlist id="chkLCriterio" runat="server" CssClass="combos_small" Width="488px" RepeatColumns="4"
										RepeatDirection="Horizontal" Height="56px">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="2">Motor</asp:ListItem>
										<asp:ListItem Value="3">Placas</asp:ListItem>
										<asp:ListItem Value="4">Marca</asp:ListItem>
										<asp:ListItem Value="5">Modelo</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist>
									<asp:checkboxlist id="chkLCriterioM" runat="server" CssClass="combos_small" Width="164px" Height="56px"
										RepeatDirection="Horizontal" RepeatColumns="2">
										<asp:ListItem Value="7">P&#243;liza</asp:ListItem>
										<asp:ListItem Value="8">Aseguradora</asp:ListItem>
										<asp:ListItem Value="1">Serie</asp:ListItem>
										<asp:ListItem Value="6">Asegurado</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="negrita" vAlign="top"></TD>
					<TD vAlign="top" colSpan="3">
						<asp:button id="cmdBuscar" runat="server" CssClass="boton" Text="Buscar" Height="24px"></asp:button></TD>
				</TR>
				<TR>
					<TD width="25%" colSpan="4"></TD>
				</TR>
				<TR>
					<TD colSpan="4">
						<asp:datagrid id="grd_cancelacion" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="1"
							AllowSorting="True" CssClass="datagrid">
							<FooterStyle Wrap="False" CssClass="footer"></FooterStyle>
							<SelectedItemStyle Wrap="False" CssClass="SelectedItems"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="AlternatingItems"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="NormalItems"></ItemStyle>
							<HeaderStyle Wrap="False" CssClass="Interiro_tabla_centro"></HeaderStyle>
							<Columns>
								<asp:EditCommandColumn Visible="False" ButtonType="LinkButton" UpdateText="&lt;img src='..\Imagenes\general\grid01.gif' border=0&gt;"
									HeaderText="Sel." CancelText="&lt;img src='..\Imagenes\general\grid02.gif' border=0&gt;" EditText="&lt;img src='..\Imagenes\general\modif.gif' border=0&gt;">
									<ItemStyle Wrap="False" HorizontalAlign="Center" Width="5%"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:BoundColumn DataField="Poliza" HeaderText="P&#243;liza"></asp:BoundColumn>
								<asp:BoundColumn DataField="Num_Contrato" HeaderText="No. Contrato">
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Descripcion_Cancelacion" HeaderText="Mot. Cancelaci&#243;n"></asp:BoundColumn>
								<asp:BoundColumn DataField="nom" HeaderText="Cancelador"></asp:BoundColumn>
								<asp:BoundColumn DataField="Fecha_cancelacion" HeaderText="Fecha Cancelaci&#243;n"></asp:BoundColumn>
								<asp:BoundColumn DataField="Marca" HeaderText="Marca"></asp:BoundColumn>
								<asp:BoundColumn DataField="Modelo" ReadOnly="True" HeaderText="Modelo">
									<ItemStyle Width="12%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Num_Contrato" HeaderText="No. Contrato"></asp:BoundColumn>
								<asp:BoundColumn DataField="anio" ReadOnly="True" HeaderText="A&#241;o">
									<ItemStyle Width="10%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="descripcion" ReadOnly="True" HeaderText="Descripci&#243;n">
									<ItemStyle Width="18%"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="Descripcion_Aseguradora" HeaderText="Aseguradora"></asp:BoundColumn>
								<asp:ButtonColumn Visible="False" Text="&lt;img src='..\Imagenes\general\trash.bmp' border='0'&gt;"
									HeaderText="Del." CommandName="Delete"></asp:ButtonColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD align="center"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
			<cc1:MsgBox id="MsgBox" style="Z-INDEX: 104; LEFT: 368px; POSITION: absolute; TOP: 544px" runat="server"></cc1:MsgBox>
		</form>
	</body>
</HTML>
