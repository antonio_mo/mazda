'dteTo = String.Format("{0:yyyy-MM}-{1:00}", Date.Today, Date.DaysInMonth(Year(Date.Today), Month(Date.Today)) ) 

Public Class Utilerias

    Public Function Prorrateo(ByVal IdAseguradora As Integer, ByVal TipRec As String, ByVal periodo As Integer, ByVal bisiesto As Integer, ByVal FInicial As Date, ByVal vigencia As Date) As String
        'TipRec= "C" (Recibo completo),"F"(Recibo Fraccion)o "D"(Dias Totales)
        'periodo= "15","30","90" o "180" (Es modificable para cualquier periodo)
        'bisiesto="360" o "365"
        Dim dias, diasB, EnDias As Integer
        Dim Recibos, recibosB As Integer
        Dim cont As Integer

        If periodo <> 15 And periodo <> 30 And periodo <> 90 And periodo <> 180 And periodo <> 360 Then
            'MsgBox "Error en duraci�n del  periodo"
            Exit Function
        Else
            Select Case TipRec
                Case "C", "F"
                    If bisiesto = 365 Then
                        recibosB = A�oBisiesto(IdAseguradora, TipRec, "completo", FInicial, vigencia)
                        diasB = A�oBisiesto(IdAseguradora, TipRec, "fraccion", FInicial, vigencia)
                    Else
                        Recibos = CalculaA�o1(IdAseguradora, TipRec, "completo", FInicial, vigencia)
                        dias = CalculaA�o1(IdAseguradora, TipRec, "fraccion", FInicial, vigencia)
                    End If

                Case "D"
                    If bisiesto = 365 Then
                        EnDias = A�oBisiesto(IdAseguradora, TipRec, "completo", FInicial, vigencia)
                    Else
                        EnDias = CalculaA�o1(IdAseguradora, TipRec, "completo", FInicial, vigencia)
                    End If

            End Select

            If TipRec = "D" Then
                Prorrateo = Str(EnDias)
            Else
                Select Case periodo
                    Case 30
                        If bisiesto = 365 Then
                            If TipRec = "C" Then
                                Prorrateo = Str(recibosB)
                            Else
                                Prorrateo = Str(diasB)
                            End If
                        Else
                            If TipRec = "C" Then
                                Prorrateo = Str(Recibos)
                            Else
                                Prorrateo = Str(dias)
                            End If
                        End If

                    Case Else
                        dias = Recibos * 30 + dias
                        diasB = recibosB * 30 + diasB
                        If bisiesto = 365 Then
                            If TipRec = "C" Then
                                Prorrateo = Str(Int(diasB / periodo))
                            Else
                                Prorrateo = Str(diasB Mod periodo)
                            End If
                        Else
                            If TipRec = "C" Then
                                Prorrateo = Str(Int(dias / periodo))
                            Else
                                Prorrateo = Str(dias Mod periodo)
                            End If
                        End If

                End Select
            End If
        End If
    End Function

    Public Function A�oBisiesto(ByVal IdAseguradora As Integer, ByVal TipRec As String, ByVal respuesta As String, ByVal FInicial As Date, ByVal vigencia As Date) As Integer
        'respuesta=fraccion/completo
        Dim Dia, EnDias As Integer
        Dim Mes As Integer
        Dim A�o As Integer
        Dim Fecha As Date


        'If IsNull(FInicial) _
        If FInicial = Nothing _
          Or (Day(FInicial) > Day(vigencia) And Month(FInicial) >= Month(vigencia) And Year(FInicial) >= Year(vigencia)) _
          Or (Month(FInicial) > Month(vigencia) And Year(FInicial) >= Year(vigencia)) _
          Or (Year(FInicial) > Year(vigencia)) _
        Then
            ' MsgBox "Se intercambiaron las fechas", 48, "Editor "
            Fecha = vigencia
            vigencia = FInicial
            FInicial = Fecha
        End If

        Select Case IdAseguradora
            Case 14
                Select Case TipRec
                    Case "D"
                        A�oBisiesto = IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia))
                    Case "C"
                        A�oBisiesto = Fix(IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia)) / 30)
                    Case "F"
                        A�oBisiesto = IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia)) Mod 30
                End Select
            Case 50        '10/mar/2006, para zurich inclu� estos calculos
                Select Case TipRec
                    Case "D"
                        A�oBisiesto = IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia))
                    Case "C"
                        A�oBisiesto = Fix(IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia)) / (365 / 12))
                    Case "F"
                        A�oBisiesto = IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia)) - (Fix(IIf(DateDiff("d", FInicial, vigencia) > 365, 365, DateDiff("d", FInicial, vigencia)) / (365 / 12)) * (365 / 12))
                End Select

            Case Else
                If TipRec = "D" Then
                    'DateDiff(DateInterval.Year, vigencia, FInicial)
                    A�oBisiesto = DateDiff(DateInterval.Day, FInicial, vigencia)

                    Select Case IdAseguradora   '07/may/2007 para qualitas solo hay a�os de 365
                        Case 53, 19, 1                 '08/may/2007 para atlas tambi�n 365
                            A�o = Year(FInicial)
                            While A�o <= Year(vigencia)
                                If A�o Mod 4 = 0 Then
                                    Fecha = "29/02/" & A�o
                                    If Fecha >= FInicial And Fecha <= vigencia Then
                                        A�oBisiesto = A�oBisiesto - 1
                                    End If
                                End If
                                A�o = A�o + 1
                            End While
                    End Select

                Else
                    If (Day(FInicial) > Day(vigencia)) Then
                        Dia = Str(Day(FInicial))
                        Select Case Str(Month(FInicial))
                            Case 2
                                If (Year(FInicial) Mod 4 = 0) Then
                                    Dia = ((29 - Dia) + Day(vigencia))
                                    If Dia >= 29 Then
                                        Mes = Month(FInicial)
                                    Else
                                        Mes = Month(FInicial) + 1
                                    End If
                                Else
                                    Dia = ((28 - Dia) + Day(vigencia))
                                    If Dia >= 28 Then
                                        Mes = Month(FInicial)
                                    Else
                                        Mes = Month(FInicial) + 1
                                    End If
                                End If

                            Case 1, 3, 5, 7, 8, 10, 12
                                Dia = ((31 - Dia) + Day(vigencia))
                                If Dia >= 31 Then
                                    Mes = Month(FInicial)
                                Else
                                    Mes = Month(FInicial) + 1
                                End If

                            Case 4, 6, 9, 11
                                Dia = ((30 - Dia) + Day(vigencia))
                                If Dia >= 30 Then
                                    Mes = Month(FInicial)
                                Else
                                    Mes = Month(FInicial) + 1
                                End If

                        End Select

                        If respuesta = "fraccion" Then
                            A�oBisiesto = Dia
                        Else
                            If (Mes > Month(vigencia)) Then
                                Mes = (12 - Mes) + Month(vigencia)
                                If Mes >= 12 Then
                                    A�o = Year(FInicial)
                                Else
                                    A�o = Year(FInicial) + 1
                                End If
                                A�oBisiesto = Mes + (12 * (Year(vigencia) - A�o))

                            Else
                                Mes = Month(vigencia) - Mes
                                A�o = Year(FInicial)
                                A�oBisiesto = Mes + (12 * (Year(vigencia) - A�o))

                            End If
                        End If
                    Else
                        Dia = Str(Day(vigencia) - Day(FInicial))
                        If respuesta = "fraccion" Then
                            '19/oct/2005 coloque esta condicion porque salian fraciones cuando era fin de mes en meses que no tienen 31 d�as
                            If (Day(FInicial) + 1) < Day(FInicial) Then
                                A�oBisiesto = 0
                            Else
                                A�oBisiesto = Dia
                            End If
                        Else
                            If (Month(FInicial) > Month(vigencia)) Then
                                Mes = (12 - Month(FInicial)) + Month(vigencia)
                                A�o = Year(FInicial) + 1
                                A�oBisiesto = Mes + (12 * (Year(vigencia) - A�o))
                            Else
                                Mes = Month(vigencia) - Month(FInicial)
                                A�o = Year(FInicial)
                                A�oBisiesto = Mes + (12 * (Year(vigencia) - A�o))
                            End If
                        End If
                    End If
                End If
        End Select
    End Function

    Public Function CalculaA�o1(ByVal IdAseguradora As Integer, ByVal TipRec As String, ByVal respuesta As String, ByVal FInicial As Date, ByVal vigencia As Date) As Integer
        'respuesta=completo/fraccion
        Dim Dia, EnDias As Integer
        Dim Mes As Integer
        Dim A�o As Integer
        Dim cambio As Integer
        Dim Fecha As Date
        Dim DiaFf2 As Integer

        'If IsNull(FInicial) _
        If FInicial = Nothing _
          Or (Day(FInicial) > Day(vigencia) And Month(FInicial) >= Month(vigencia) And Year(FInicial) >= Year(vigencia)) _
          Or (Month(FInicial) > Month(vigencia) And Year(FInicial) >= Year(vigencia)) _
          Or (Year(FInicial) > Year(vigencia)) _
        Then
            'MsgBox "Se intercambiaron las fechas", 48, "Editor "
            Fecha = vigencia
            vigencia = FInicial
            FInicial = Fecha
        End If

        If Month(vigencia) = 2 Then
            If (Year(vigencia) Mod 4 = 0) Then
                If Day(vigencia) = 29 Then
                    DiaFf2 = 30
                Else
                    DiaFf2 = Day(vigencia)
                End If
            Else
                If Day(vigencia) = 28 Then
                    DiaFf2 = 30
                Else
                    DiaFf2 = Day(vigencia)
                End If
            End If
        Else
            DiaFf2 = Day(vigencia)
        End If

        If (IIf((Day(FInicial) + 1) < Day(FInicial) And (Day(vigencia) + 1) < Day(vigencia), Day(vigencia), Day(FInicial)) > DiaFf2) Then
            Dia = Str(Day(FInicial))

            Select Case Str(Month(FInicial))
                Case 2
                    If (Year(FInicial) Mod 4 = 0) Then
                        If Day(FInicial) = 29 Then
                            Dia = Day(FInicial) + 1
                        End If
                    Else
                        If Day(FInicial) = 28 Then
                            Dia = Day(FInicial) + 2
                        End If
                    End If
                Case 1, 3, 5, 7, 8, 10, 12
                    If Day(FInicial) = 31 Then
                        Dia = Day(FInicial) - 1
                    End If
            End Select

            Select Case Str(Month(vigencia))
                Case 2
                    If (Year(vigencia) Mod 4 = 0) Then
                        If Day(vigencia) = 29 Then
                            Dia = (Day(vigencia) + 1) - Dia
                        Else ''01/03/2004 para yadira c.
                            If Day(vigencia) = 28 And (IdAseguradora = 29 Or IdAseguradora = 30) Then 'para reporte de yadira c.
                                Dia = 30 - Dia
                                Mes = Month(FInicial)
                            Else
                                Dia = Str((30 - Dia) + Day(vigencia))
                                If Dia >= 30 Then
                                    Mes = Month(FInicial)
                                Else
                                    Mes = Month(FInicial) + 1
                                End If
                            End If
                        End If
                    Else
                        If Day(vigencia) = 28 Then
                            Dia = (Day(vigencia) + 2) - Dia
                        Else
                            Dia = Str((30 - Dia) + Day(vigencia))
                            If Dia >= 30 Then
                                Mes = Month(FInicial)
                            Else
                                Mes = Month(FInicial) + 1
                            End If
                        End If
                    End If
                Case Else
                    Dia = Str((30 - Dia) + Day(vigencia))
                    If Dia >= 30 Then
                        Mes = Month(FInicial)
                    Else
                        Mes = Month(FInicial) + 1
                    End If

            End Select

            If respuesta = "fraccion" Then
                CalculaA�o1 = Dia
                EnDias = Dia
            Else
                If (Mes > Month(vigencia)) Then
                    Mes = Str((12 - Mes) + (Month(vigencia)))
                    If Mes >= 12 Then
                        A�o = Year(FInicial)
                    Else
                        A�o = Year(FInicial) + 1
                    End If
                    Mes = Mes + (12 * (Year(vigencia) - A�o))
                    CalculaA�o1 = Mes
                    EnDias = Dia + (Mes * 30)
                Else
                    Mes = Str(Month(vigencia) - Mes)
                    A�o = Year(FInicial)
                    Mes = Mes + (12 * (Year(vigencia) - A�o))
                    CalculaA�o1 = Mes
                    EnDias = Dia + (Mes * 30)
                End If
            End If
            '*
        Else

            Select Case Str(Month(vigencia))
                Case 2
                    If (Year(vigencia) Mod 4 = 0) Then
                        If Day(vigencia) = 29 Then
                            cambio = Day(vigencia) + 1
                        Else '08/01/2004
                            If Day(vigencia) = 28 And (IdAseguradora = 29 Or IdAseguradora = 30) Then ' para la aseg 29 (polizas de Yadira C.) no existe el d�a 29 de feb
                                cambio = 30
                            Else
                                cambio = Day(vigencia)
                            End If
                        End If
                    Else
                        If Day(vigencia) = 28 Then
                            cambio = Day(vigencia) + 2
                        Else
                            cambio = Day(vigencia)
                        End If
                    End If
                Case 1, 3, 5, 7, 8, 10, 12
                    If Day(vigencia) = 31 Then
                        cambio = Day(vigencia) - 1
                    Else
                        cambio = Day(vigencia)
                    End If
                Case Else
                    cambio = Day(vigencia)
            End Select

            Select Case Str(Month(FInicial))
                Case 2
                    If (Year(FInicial) Mod 4 = 0) Then
                        If Day(FInicial) = 29 Then
                            If cambio = 29 Then
                                Dia = cambio
                                FInicial = DateAdd("d", 1, FInicial)
                            Else
                                Dia = cambio - (Day(FInicial) + 1)
                            End If
                        Else
                            '**05/abr/2004
                            If Day(FInicial) = 28 And (IdAseguradora = 29 Or IdAseguradora = 30) Then
                                Dia = 0
                            Else
                                '**
                                Dia = cambio - Day(FInicial)
                            End If
                        End If
                    Else
                        If Day(FInicial) = 28 Then
                            If (cambio = 28 Or cambio = 29) Then
                                Dia = cambio
                                FInicial = DateAdd("d", 1, FInicial)
                            Else
                                Dia = cambio - (Day(FInicial) + 2)
                            End If
                        Else
                            Dia = cambio - Day(FInicial)
                        End If
                    End If
                Case 1, 3, 5, 7, 8, 10, 12
                    If Day(FInicial) = 31 Then
                        Dia = cambio - (Day(FInicial) - 1)
                    Else
                        Dia = cambio - Day(FInicial)
                    End If

                Case Else
                    Dia = cambio - Day(FInicial)
            End Select

            If respuesta = "fraccion" Then
                CalculaA�o1 = Dia
                EnDias = Dia
            Else
                If (Month(FInicial) > Month(vigencia)) Then
                    Mes = Str((12 - Month(FInicial)) + (Month(vigencia)))
                    A�o = Year(FInicial) + 1
                    Mes = Mes + (12 * (Year(vigencia) - A�o))
                    CalculaA�o1 = Mes
                    EnDias = Dia + (Mes * 30)
                Else
                    Mes = Str(Month(vigencia) - Month(FInicial))
                    A�o = Year(FInicial)
                    Mes = Mes + (12 * (Year(vigencia) - A�o))
                    CalculaA�o1 = Mes
                    EnDias = Dia + (Mes * 30)
                End If

            End If
        End If

        If TipRec = "D" Then
            CalculaA�o1 = EnDias
        End If
    End Function

    Public Function Fecha(ByVal Dia As Integer, ByVal Mes As Integer, ByVal A�o As Integer) As Date
        If A�o < 0 Then A�o = 1998
        If Mes < 1 Then
            Mes = 12 - Mes
            A�o = A�o - 1
        End If
        If Dia < 1 Or Dia > 31 Then Dia = 1
        If A�o < 100 And A�o > 50 Then A�o = 1900 + A�o
        If A�o <= 50 Then A�o = 2000 + A�o
        If Mes > 12 Then
            A�o = A�o + Int(Mes / 12)
            Mes = Mes Mod 12
        End If
        DateDiff(DateInterval.Hour, Now, Now)
        Fecha = DateAdd("d", Dia - 30, Fecha)
        Fecha = DateAdd("m", Mes, Fecha)
        Fecha = DateAdd("yyyy", A�o - 1900, Fecha)
    End Function

    Public Function Determinamos_Bisiesto(ByVal intplazo As Integer, ByVal strfechavigencia As String) As Integer
        Dim strsql As String
        Dim bantipo As Byte
        Dim validaanio As Integer
        Dim validaanioActual As Integer
        Dim Anio As Integer
        Dim AuxAnio As Integer
        Dim mes As Integer
        Dim dia As Integer
        Dim bansalida As Byte
        Dim FechaPlazo As String
        Dim Aniodiferencia As Integer
        Dim DiasDiferencia As Integer
        Dim i As Integer
        Dim Intsalida As Integer
        Dim plazoI As Integer
        Dim FechaPlazoInicial As String
        Dim FechaValida As String

        bantipo = 0
        FechaPlazo = DateAdd("m", intplazo, strfechavigencia)
        FechaPlazoInicial = FechaPlazo
        If intplazo <= 12 Then
            Aniodiferencia = DateDiff("yyyy", strfechavigencia, FechaPlazo)
        Else
            'If (intplazo Mod 12) <> 0 Then Aniodiferencia = Aniodiferencia + 1
            DiasDiferencia = DateDiff("d", strfechavigencia, FechaPlazo)
            Aniodiferencia = DiasDiferencia / 365
            If (intplazo Mod 12) > 0 Then
                If (DiasDiferencia Mod 365) <> 0 Then Aniodiferencia = Aniodiferencia + 1
            End If
            'If (intplazo Mod 12) <> 0 Then Aniodiferencia = Aniodiferencia + 1
        End If

        If Aniodiferencia = 0 Then Aniodiferencia = 1

        For i = 1 To Aniodiferencia
            If i = 1 Then
                plazoI = DateDiff("d", strfechavigencia, FechaPlazo)
            Else
                plazoI = DateDiff("d", FechaPlazo, FechaPlazoInicial)
            End If
            If plazoI < 365 Then
                'FechaPlazo = DateAdd("d", plazoI, strfechavigencia)
                FechaPlazo = DateAdd("d", plazoI, FechaPlazo)
            Else
                ''''FechaPlazo = DateAdd("yyyy", i, strfechavigencia)
                validaanioActual = String.Format("{0:yyyy}", CDate(strfechavigencia)) Mod 100
                If plazoI > 365 And IIf(validaanioActual = 0, IIf(String.Format("{0:yyyy}", CDate(strfechavigencia)) Mod 400 = 0, 1, 0), IIf(String.Format("{0:yyyy}", CDate(strfechavigencia)) Mod 4 = 0, 1, 0)) = 1 Then
                    FechaPlazo = Format(DateAdd("yyyy", i - 1, strfechavigencia), "yyyy/mm/dd")
                Else
                    FechaPlazo = Format(DateAdd("yyyy", i, strfechavigencia), "yyyy/mm/dd")
                End If
            End If

            dia = Mid(FechaPlazo, 1, 2)
            mes = Mid(FechaPlazo, 4, 2)
            Anio = Mid(FechaPlazo, 7, 4)
            validaanio = Anio Mod 100

            If validaanio = 0 Then
                If (Anio Mod 400) = 0 Then
                    bansalida = 1
                Else
                    bansalida = 0
                End If
            Else
                If (Anio Mod 4) = 0 Then
                    bansalida = 1
                Else
                    bansalida = 0
                End If
            End If
            If bansalida = 1 Then
                FechaValida = "29/02/" & Anio
                If DateDiff("d", FechaValida, FechaPlazoInicial) >= 0 Then
                    bansalida = 1
                Else
                    bansalida = 0
                End If
            End If

            If i = 1 Then
                Intsalida = bansalida
                AuxAnio = Anio
            Else
                If Not Anio = AuxAnio Then
                    Intsalida = Intsalida + bansalida
                    AuxAnio = Anio
                End If
            End If
        Next

        Determinamos_Bisiesto = Intsalida
    End Function


    'Public Function DiasMes(NoMes As Integer, Anu As Integer)
    '  Select Case NoMes
    '    Case 1, 3, 5, 7, 8, 10, 12
    '      DiasMes = 31
    '    Case 2
    '      If Anu Mod 4 = 0 Then DiasMes = 29 Else DiasMes = 28
    '    Case 4, 6, 9, 11
    '      DiasMes = 30
    '  End Select
    'End Function


End Class
