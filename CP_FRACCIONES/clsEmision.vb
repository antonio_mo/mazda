Imports System
Imports System.Data

'Las funciones Int y Fix quitan la parte fraccionaria de Number y devuelven el valor entero resultante.
'Int(PrimaDiaria)
'Fix(PrimaDiaria)

Public Class clsEmision
    Private DtResultado As New DataTable
    Dim CCliente As New Utilerias

    Dim IdAseguradora As Integer
    Dim InicioVigencia As String
    Dim FinVigencia As String
    Dim FechaAlta As String
    Dim InicioVigenciaRecibo As String
    Dim FinVigenciaRecibo As String
    Dim arrFracciones(0, 0) As String
    Dim i As Integer
    Dim PrimaDiaria As Double
    Dim PrimaNeta As Double
    Dim Recargos As Double
    Dim Derechos As Double
    Dim Iva As Double
    Dim PrimaTotal As Double
    Dim FormaPago As Integer
    Dim DiasPago As Integer
    Dim DiasCobrar As Integer
    Dim pCompletos As Integer
    Dim pFracciones As Integer
    Dim NoRecibo As Integer

    Dim tmpDia As String
    Dim tmpMes As String

    Dim PNPrimerRecibo As Double
    Dim DPPrimerRecibo As Double
    Dim PFPrimerRecibo As Double
    Dim CCPrimerRecibo As Double
    Dim IMPPrimerRecibo As Double
    Dim PTPrimerRecibo As Double

    Dim Trabajo01 As Integer
    Dim Anio As Integer
    Dim Mes As Integer
    Dim Dia As Integer
    Dim Trabajo01R As String
    Dim �UTrabajo02R As String
    Dim �UAnioAux2 As Long
    Dim �UMesAux2 As Long
    Dim �UDiaAux2 As Long
    Dim �UVarTrabajo03 As Long
    Dim �UVarTrabajo04 As Long
    Dim �UVarTrabajo05 As Integer
    Dim �UVarTrabajo06 As Integer
    Dim �UVarTrabajo07 As Double
    Dim �UVarTrabajo08 As Integer
    Dim �UVarTrabajo09 As String
    Dim �UAnioAux As Integer
    Dim �UMesAux As Integer
    Dim �UAnioDias As Long
    Dim �UMesDias As Long
    Dim �UDICOREIN As Integer
    Dim �UDiaAux3 As Integer
    Dim �UAnioMas As Integer
    Dim �UAnioBis As Integer
    Dim �UPmaTotGen As Double
    Dim �UPmaNetGen As Double
    Dim �UDerPolGen As Double
    Dim �URecPagGen As Double
    Dim �UIvaGen As Double
    Dim �UImpGen As Double
    Dim �UCesComGen As Double
    Dim �UVigForPag As Integer
    Dim �URegular As Integer
    Dim �UVarAux As Double
    Dim �UPriNet As Double
    Dim �URecPag As Double
    Dim �UIva As Double
    Dim �UPriTot As Double
    Dim �UPima As Double
    Dim �UCesComs As Double
    Dim �UCesCom As Double
    Dim �UDerPol As Double
    Dim �UPoliza As Double
    Dim �URecaargos As Double
    Dim �UImpuesto1 As Double
    Dim �UPriNetIni As Double
    Dim �UDerPolIni As Double
    Dim �URecPagIni As Double
    Dim �UIvaIni As Double
    Dim �UPriTotIni As Double
    Dim �UCesComIni As Double
    Dim �UImpIni As Double
    Dim �UFechaIniSigRec As String
    Dim �UFechaCobSigRec As String
    Dim �UFechaCbrPmeRec As String
    Dim �UPerCobRecSub As Integer
    Dim �UNumRecGen As Integer

    Dim �UI2 As Integer
    Dim �UI3 As Integer
    Dim �UPriNetSub As Double
    Dim �UDerPolSub As Double
    Dim �URecPagSub As Double
    Dim �UIvaSub As Double
    Dim �UPriTotSub As Double
    Dim �UCesComSub As Double
    Dim �UFecIniAlf As String
    Dim �UFecIni As String
    Dim �UAniIni As Integer
    Dim �UMesIni As Integer
    Dim �UDiaIni As Integer
    Dim �UPerTra As Integer
    Dim �UPolFecVigFin As String
    Dim �UAAFecFin As Integer
    Dim �UMMFecFin As Integer
    Dim �UDDFecFin As Integer
    Dim �UDia31 As Integer
    Dim �UDiaAux As Integer
    Dim �UDiaTra As Integer
    Dim �UMesTra As Integer
    Dim �UAniMas As Integer
    Dim �UMesDia As Integer
    Dim �UFecIniA As String
    Dim �UFecIniB As String
    Dim �UAniSB As Integer
    Dim �UMesSB As Integer
    Dim �UDiasB As Integer
    Dim �UDisADI As Integer
    Dim �UAniB As Integer
    Dim �UAniBis As Integer

    Dim �UPorCIva As Double
    Dim �UImpuesto As Double
    Dim �UPmaTotSub As Double
    Dim �UPrima As Double
    Dim �URecargos As Double
    Dim �UPrimaTotal1 As Double

    Public Sub New()
    End Sub

    Public Function FraccionesAXA_Daimler(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
  ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
  ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                If CantMesesTotal < DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            'Frac12Meses = Math.Round(CantMesesTotal / DiasPago, 2)

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    nDerechosFrac = Derechos
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 2)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesAXA_Conauto(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
  ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
  ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim nDerechosResta As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If CantMesesTotal > DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            'Frac12Meses = Math.Round(CantMesesTotal / DiasPago, 2)

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            'nDerechosFrac = Derechos
            nDerechosResta = Derechos

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    'nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    nDerechosFrac = (Derechos / CantCuotCal)
                    'nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nRecargoFrac = Math.Round((PorRecargo / 100) / CantCuotCal, 2)
                    'nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nIvaFrac = Math.Round(Iva / CantCuotCal, 2)
                    'nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    nPTFrac = Math.Round(PrimaTotal / CantCuotCal, 2)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nDerechosFrac = Math.Round(nDerechosResta, 2)
                    nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 2)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nDerechosResta = nDerechosResta - nDerechosFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesGNP_Conauto(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
   ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
   ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If CantMesesTotal > DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            'Frac12Meses = Math.Round(CantMesesTotal / DiasPago, 2)

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            nDerechosFrac = Derechos

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nDerechosFrac = (Derechos / CantCuotCal)
                    nRecargoFrac = Math.Round((nPNFrac + nDerechosFrac) * PorRecargo / 100, 2)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nDerechosFrac = (Derechos / CantCuotCal)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 2)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesGnp(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double) As DataSet

        Dim ds As New DataSet
        Dim Dif�UPriNetSub As Double = 0
        Dim Dif�URecPagSub As Double = 0
        Dim Dif�UDerPolSub As Double = 0
        Dim Dif�UIvaSub As Double = 0
        Dim Dif�UPmaTotSub As Double = 0
        Try

            'InicioVigencia = Mid(strFechaInicio, 7, 4) & "/" & Mid(strFechaInicio, 4, 2) & "/" & Mid(strFechaInicio, 1, 2)
            'FinVigencia = Mid(strFechaFin, 7, 4) & "/" & Mid(strFechaFin, 4, 2) & "/" & Mid(strFechaFin, 1, 2)

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin

            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            �UPorCIva = PorcentajeIva
            IdAseguradora = 1

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 360
                Case 2  'Mensual
                    DiasPago = 30
                Case 3  'Trimestral
                    DiasPago = 90
                Case 4  'Semestral
                    DiasPago = 180
            End Select

            'DiasCobrar = Prorrateo(IdAseguradora, "D", DiasPago, 360, CDate(InicioVigencia), CDate(FinVigencia))
            pCompletos = CCliente.Prorrateo(IdAseguradora, "C", DiasPago, 360, CDate(InicioVigencia), CDate(FinVigencia))
            'Dias que faltan para acompletar el mes
            pFracciones = CCliente.Prorrateo(IdAseguradora, "F", DiasPago, 360, CDate(InicioVigencia), CDate(FinVigencia))

            If pCompletos = 0 Then
                pCompletos = 1
                'incio nuevo
            Else
                If pFracciones > 0 Then
                    pCompletos = pCompletos + 1
                End If
                'fin nuevo
            End If

            ReDim arrFracciones(pCompletos, 9)

            Call CalculoVigenciaRecibo()

            'Calcula el costo diario
            PrimaDiaria = PrimaTotal / �UVarTrabajo05

            Call CalculaImportePrimerRecibo()

            �UI2 = �UNumRecGen + 1
            �UI3 = pCompletos

            If pCompletos > 1 Then
                For i = �UI2 To �UI3
                    �UNumRecGen = �UNumRecGen + 1
                    GeneraRecibos()
                Next
            End If

            Dif�UPriNetSub = 0
            Dif�URecPagSub = 0
            Dif�UDerPolSub = 0
            Dif�UIvaSub = 0
            Dif�UPmaTotSub = 0
            For i = 0 To UBound(arrFracciones) - 1
                Dif�UPriNetSub = Dif�UPriNetSub + CDbl(arrFracciones(i, 2))
                Dif�URecPagSub = Dif�URecPagSub + CDbl(arrFracciones(i, 3))
                Dif�UDerPolSub = Dif�UDerPolSub + CDbl(arrFracciones(i, 4))
                Dif�UIvaSub = Dif�UIvaSub + CDbl(arrFracciones(i, 5))
                Dif�UPmaTotSub = Dif�UPmaTotSub + CDbl(arrFracciones(i, 6))
            Next i
            arrFracciones(0, 2) = Format(CDbl(arrFracciones(0, 2)) + (PrimaNeta - Dif�UPriNetSub), "####0.00")
            arrFracciones(0, 3) = Format(CDbl(arrFracciones(0, 3)) + (Recargos - Dif�URecPagSub), "####0.00")
            arrFracciones(0, 4) = Format(CDbl(arrFracciones(0, 4)) + (Derechos - Dif�UDerPolSub), "####0.00")
            arrFracciones(0, 5) = Format(CDbl(arrFracciones(0, 5)) + (Iva - Dif�UIvaSub), "####0.00")
            arrFracciones(0, 6) = Format(CDbl(arrFracciones(0, 6)) + (PrimaTotal - Dif�UPmaTotSub), "####0.00")

            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesGnp = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesZurich(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                If CantMesesTotal < DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            'Frac12Meses = Math.Round(CantMesesTotal / DiasPago, 2)

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    nDerechosFrac = Derechos
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = nPNResta
                    nRecargoFrac = nRecargoResta
                    nPTFrac = nPTResta
                    nIvaFrac = nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesZurich_Multi(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, _
    ByVal PorRecargo As Double, ByVal Plazo As Integer) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nDerpolRest As Double
            Dim nIvaRest As Double
            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            Dim NumFracciones As Integer
            Dim residuo As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = Plazo
            NumFracciones = Math.DivRem(Plazo, DiasPago, residuo)
            residuo = IIf(residuo <> 0, 1, 0)
            NumFracciones = NumFracciones + residuo
            CantCuotCal = NumFracciones

            nPNMes = Math.Round(((PrimaNeta / Plazo) * DiasPago), 2)
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nDerpolRest = Derechos
            nIvaRest = Iva
            nMesesResta = CantMesesTotal
            
            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    nPNFrac = Math.Round(nPNMes, 2)
                    nRecargoFrac = 0
                    nDerechosFrac = Math.Round((Derechos / NumFracciones), 2)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nPTFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac), 2)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    'nDerechosFrac = Math.Round((Derechos / NumFracciones), 2)
                    nDerechosFrac = Math.Round(nDerpolRest, 2)
                    'nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nIvaFrac = Math.Round(nIvaRest, 2)
                    'nPTFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac), 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = Math.Round((nPNResta - nPNFrac), 2)
                nRecargoResta = Math.Round((nRecargoResta - nRecargoFrac), 2)
                nDerpolRest = Math.Round((nDerpolRest - nDerechosFrac), 2)
                nIvaRest = Math.Round((nIvaRest - nIvaFrac), 2)
                nPTResta = Math.Round((nPTResta - nPTFrac), 2)
                nMesesResta = Math.Round((nMesesResta - nMeses), 2)

                Debug.WriteLine("PN: " & nPNResta)
                Debug.WriteLine("Derpol: " & nDerpolRest)
                Debug.WriteLine("IVA: " & nIvaRest)
                Debug.WriteLine("PT: " & nPTResta)

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesZurich_Daimler(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strprimaneta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try
            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim modFrac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            Dim DVC As Integer
            Dim DFC As Double
            Dim DerIva As Double
            Dim PTAux As Double

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strprimaneta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago

            Pos = 0

            DVC = 1188
            DFC = 365.3333

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select


            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                If CantMesesTotal < DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            DerIva = Derechos * (1 + PorcentajeIva)
            PTAux = PrimaTotal - DerIva
            PTAux = (PTAux * DFC) / DVC
            PTAux = ((PTAux / (1 + PorcentajeIva)) / (1 + (PorRecargo / 100)))

            nPNMes = PTAux 'PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    nDerechosFrac = Derechos
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    nPNFrac = Math.Round(nPNMes, 4) 'Round(nPNMes * nMeses, 2)
                    nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 4)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 4)
                    nPTFrac = Math.Round(nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac, 4)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 4)
                    nRecargoFrac = Math.Round(nRecargoResta, 4)
                    nPTFrac = Math.Round(nPTResta, 4)
                    nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 4)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesQualitas_Conauto(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nIvaResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim nDerechosResta As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If CantMesesTotal > DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            'Frac12Meses = Math.Round(CantMesesTotal / DiasPago, 2)
            nPNResta = PrimaNeta
            nPNMes = nPNResta / CantMesesTotal
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal
            nIvaResta = Iva

            'nDerechosFrac = Derechos
            nDerechosResta = Derechos

            Dim IvaDerechoFrac As Double = (Derechos * 0.16) / CantCuotCal
            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    'nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nPNFrac = Math.Round((PrimaNeta) / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    nDerechosFrac = nDerechosResta
                    'nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nRecargoFrac = Math.Round((PrimaNeta * (PorRecargo / 100) / CantCuotCal), 2)
                    nIvaFrac = IIf(i = 0, Math.Round(Iva / CantCuotCal, 2) + (IvaDerechoFrac * (CantCuotCal - 1)), Math.Round(Iva / CantCuotCal, 2) - IvaDerechoFrac)
                    'nIvaFrac = Math.Round(Iva / CantCuotCal, 2)
                    nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    'nPTFrac = Math.Round(PrimaTotal / CantCuotCal, 2)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    'nMeses = nMesesResta
                    ''nPNFrac = Math.Round(nPNResta, 2)
                    'nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    ''nDerechosFrac = (Derechos / CantCuotCal)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    ''nRecargoFrac = Math.Round(nRecargoResta, 2)
                    'nRecargoFrac = Math.Round((PorRecargo / 100) / CantCuotCal, 2)
                    ''nPTFrac = Math.Round(nPTResta, 2)
                    'nPTFrac = Math.Round(PrimaTotal / CantCuotCal, 2)
                    ''nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 2)
                    'nIvaFrac = Math.Round(Iva / CantCuotCal, 2)
                    'dFecVigFinFrac = FinVigencia

                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nDerechosFrac = Math.Round(nDerechosResta, 2)
                    nIvaFrac = Math.Round(nIvaResta, 2)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nDerechosResta = nDerechosResta - nDerechosFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses
                nIvaResta = nIvaResta - nIvaFrac

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function


    Public Function FraccionesQualitas_Daimler(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strprimaneta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try
            Dim CantMesesTotal As Integer
            Dim nCantMesesTot As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim modFrac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            Dim DiasPeriodo As Integer
            Dim primadias As Double
            Dim diastotal As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strprimaneta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago

            Pos = 0


            DiasPeriodo = 360

            pCompletos = CCliente.Prorrateo(1, "D", DiasPeriodo, 365, CDate(InicioVigencia), CDate(FinVigencia))
            'validamos si tiene a�o bisiesto y si es asi le sumamos el dia
            pCompletos = pCompletos + (CCliente.Determinamos_Bisiesto(39, InicioVigencia))

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If Frac12Meses < DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nMesesResta = CantMesesTotal

            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    nDerechosFrac = Derechos
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    If CCliente.Determinamos_Bisiesto(12, dFecVigIniFrac) = 1 Then
                        DiasPeriodo = 366
                    Else
                        DiasPeriodo = 365
                    End If
                    diastotal = diastotal + DiasPeriodo

                    nMeses = DiasPago
                    nPNFrac = Math.Round((PrimaNeta / pCompletos) * DiasPeriodo, 4)
                    nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 4)
                    nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 4)
                    nPTFrac = Math.Round(nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac, 4)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 4)
                    nRecargoFrac = Math.Round(nRecargoResta, 4)
                    nPTFrac = Math.Round(nPTResta, 4)
                    nIvaFrac = Math.Round(nPTFrac - (nPNFrac + nRecargoFrac + nDerechosFrac), 4)
                    dFecVigFinFrac = FinVigencia
                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nPTResta = nPTResta - nPTFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try

    End Function


    Public Function FraccionesHDI(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim nDerechosResta As Double
            Dim nIvaResta As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If CantMesesTotal > DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nIvaResta = Iva
            nMesesResta = CantMesesTotal
            nDerechosResta = Derechos

            Dim IvaDerechoFrac As Double = (Derechos * 0.16) / CantCuotCal
            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    'nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    nDerechosFrac = nDerechosResta
                    'nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nRecargoFrac = Math.Round(Math.Round((PrimaNeta * (PorRecargo / 100)), 2) / CantCuotCal, 2)
                    'nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nIvaFrac = Math.Round(IIf(i = 0, Math.Round(Iva / CantCuotCal, 2) + (IvaDerechoFrac * (CantCuotCal - 1)), Math.Round(Iva / CantCuotCal, 2) - IvaDerechoFrac), 2)
                    'nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    nPTFrac = nPNFrac + nDerechosFrac + nRecargoFrac + nIvaFrac 'Math.Round(PrimaTotal / CantCuotCal, 2)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    'nMeses = nMesesResta
                    'nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    'nRecargoFrac = Math.Round((PorRecargo / 100) / CantCuotCal, 2)
                    'nPTFrac = Math.Round(PrimaTotal / CantCuotCal, 2)
                    'nIvaFrac = Math.Round(Iva / CantCuotCal, 2)
                    'dFecVigFinFrac = FinVigencia

                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nDerechosFrac = Math.Round(nDerechosResta, 2)
                    nIvaFrac = Math.Round(nIvaResta, 2)
                    dFecVigFinFrac = FinVigencia

                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nDerechosResta = nDerechosResta - nDerechosFrac
                nPTResta = nPTResta - nPTFrac
                nIvaResta = nIvaResta - nIvaFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Public Function FraccionesCHUBB_MZD(ByVal strFechaInicio As String, ByVal strFechaFin As String, _
    ByVal strPrimaNeta As Double, ByVal strRecargo As Double, ByVal strDerechos As Double, ByVal strIva As Double, _
    ByVal strPrimaTotal As Double, ByVal intFormaPago As Integer, ByVal PorcentajeIva As Double, ByVal PorRecargo As Double) As DataSet

        Dim ds As New DataSet
        Try

            Dim CantMesesTotal As Integer
            Dim MesesUltFrac As Integer
            Dim Frac12Meses As Integer
            Dim CantCuotCal As Integer
            Dim nPNMes As Double
            Dim nPNResta As Double
            Dim nRecargoResta As Double
            Dim nPTResta As Double
            Dim nMesesResta As Integer
            Dim nMeses As Integer
            Dim nDerechosFrac As Double
            Dim nDerechosResta As Double
            Dim nIvaResta As Double
            Dim dFecVigIniFrac As String
            Dim dFecVigFinFrac As String

            Dim nPNFrac As Double
            Dim nRecargoFrac As Double
            Dim nIvaFrac As Double
            Dim nPTFrac As Double
            Dim resFraccion As Double
            Dim Pos As Integer

            InicioVigencia = strFechaInicio
            FinVigencia = strFechaFin
            PrimaNeta = strPrimaNeta
            Recargos = strRecargo
            Derechos = strDerechos
            Iva = strIva
            PrimaTotal = strPrimaTotal
            FormaPago = intFormaPago
            Pos = 0

            Select Case FormaPago
                Case 1  'Anual
                    DiasPago = 12
                Case 2  'Mensual
                    DiasPago = 1
                Case 3  'Trimestral
                    DiasPago = 3
                Case 4  'Semestral
                    DiasPago = 6
            End Select

            CantMesesTotal = DateDiff("m", InicioVigencia, FinVigencia)
            MesesUltFrac = CantMesesTotal Mod DiasPago

            If CantMesesTotal < DiasPago Then
                Frac12Meses = 1
            Else
                resFraccion = CantMesesTotal / DiasPago
                Pos = InStr(resFraccion, ".")
                If Pos = 0 Then
                    Frac12Meses = CInt(resFraccion)
                Else
                    Frac12Meses = Mid(resFraccion, 1, InStr(resFraccion, ".") - 1)
                End If
            End If

            If MesesUltFrac = 0 Then
                CantCuotCal = Frac12Meses
            Else
                'If CantMesesTotal < DiasPago Then
                If CantMesesTotal > DiasPago Then
                    CantCuotCal = Frac12Meses
                Else
                    CantCuotCal = Frac12Meses + 1
                End If
            End If

            nPNMes = PrimaNeta / CantMesesTotal
            nPNResta = PrimaNeta
            nRecargoResta = Recargos
            nPTResta = PrimaTotal
            nIvaResta = Iva
            nMesesResta = CantMesesTotal
            nDerechosResta = Derechos

            Dim IvaDerechoFrac As Double = (Derechos * 0.16) / CantCuotCal
            ReDim arrFracciones(CantCuotCal, 9)
            For i = 0 To CantCuotCal - 1
                nDerechosFrac = 0
                If i = 0 Then
                    dFecVigIniFrac = InicioVigencia
                Else
                    dFecVigIniFrac = dFecVigFinFrac
                End If

                If i <> CantCuotCal - 1 Then
                    nMeses = DiasPago
                    'nPNFrac = Math.Round(nPNMes * nMeses, 2)
                    nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    nDerechosFrac = nDerechosResta
                    'nRecargoFrac = Math.Round(nPNFrac * PorRecargo / 100, 2)
                    nRecargoFrac = Math.Round((PrimaNeta * (PorRecargo / 100)) / CantCuotCal, 2)
                    'nIvaFrac = Math.Round((nPNFrac + nRecargoFrac + nDerechosFrac) * PorcentajeIva, 2)
                    nIvaFrac = IIf(i = 0, Math.Round(Iva / CantCuotCal, 2) + (IvaDerechoFrac * (CantCuotCal - 1)), Math.Round(Iva / CantCuotCal, 2) - IvaDerechoFrac)
                    'nPTFrac = nPNFrac + nRecargoFrac + nDerechosFrac + nIvaFrac
                    nPTFrac = nPNFrac + nDerechosFrac + nRecargoFrac + nIvaFrac 'Math.Round(PrimaTotal / CantCuotCal, 2)
                    dFecVigFinFrac = DateAdd("m", nMeses, dFecVigIniFrac)
                    'Select add_months(dFecVigIniFrac,nMeses) into dFecVigFinFrac from dual
                Else 'ultima fraccion
                    'nMeses = nMesesResta
                    'nPNFrac = Math.Round(PrimaNeta / CantCuotCal, 2)
                    'nDerechosFrac = (Derechos / CantCuotCal)
                    'nRecargoFrac = Math.Round((PorRecargo / 100) / CantCuotCal, 2)
                    'nPTFrac = Math.Round(PrimaTotal / CantCuotCal, 2)
                    'nIvaFrac = Math.Round(Iva / CantCuotCal, 2)
                    'dFecVigFinFrac = FinVigencia

                    nMeses = nMesesResta
                    nPNFrac = Math.Round(nPNResta, 2)
                    nRecargoFrac = Math.Round(nRecargoResta, 2)
                    nPTFrac = Math.Round(nPTResta, 2)
                    nDerechosFrac = Math.Round(nDerechosResta, 2)
                    nIvaFrac = Math.Round(nIvaResta, 2)
                    dFecVigFinFrac = FinVigencia

                End If

                nPNResta = nPNResta - nPNFrac
                nRecargoResta = nRecargoResta - nRecargoFrac
                nDerechosResta = nDerechosResta - nDerechosFrac
                nPTResta = nPTResta - nPTFrac
                nIvaResta = nIvaResta - nIvaFrac
                nMesesResta = nMesesResta - nMeses

                arrFracciones(i, 0) = i + 1
                arrFracciones(i, 1) = CantCuotCal
                arrFracciones(i, 2) = nPNFrac
                arrFracciones(i, 3) = nRecargoFrac
                arrFracciones(i, 4) = nDerechosFrac
                arrFracciones(i, 5) = nIvaFrac
                arrFracciones(i, 6) = nPTFrac
                arrFracciones(i, 7) = dFecVigIniFrac
                arrFracciones(i, 8) = dFecVigFinFrac
            Next


            crear_ds_Salida()
            ds.Tables.Add(carga_datos_Salida())
            Return ds
            'FraccionesZurich = arrFracciones

            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function


    Private Function CalculoVigenciaRecibo()
        Try

            CalculoVigenciaDias()

            �UVarTrabajo06 = DiasPago

            �UDICOREIN = �UVarTrabajo05 - (�UVarTrabajo06 * (pCompletos - 1))

            �UTrabajo02R = InicioVigencia

            '  �UAnioAux2 = Mid(InicioVigencia, 1, 4)
            '  �UMesAux2 = Mid(InicioVigencia, 6, 2)
            '  �UDiaAux2 = Mid(InicioVigencia, 9, 2)

            �UDiaAux2 = Mid(InicioVigencia, 1, 2)
            �UMesAux2 = Mid(InicioVigencia, 4, 2)
            �UAnioAux2 = Mid(InicioVigencia, 7, 4)

            �UDiaAux3 = �UDiaAux2

            If �UMesAux2 = 2 Then
                If �UDiaAux3 >= 28 Then
                    �UDiaAux3 = 30
                End If
            Else
                If �UDiaAux3 >= 30 Then
                    �UDiaAux3 = 30
                End If
            End If

            �UDiaAux3 = �UDiaAux3 + �UDICOREIN

            If �UDiaAux3 > 360 Then
                �UAnioMas = �UDiaAux3 / 360
                �UMesDia = �UDiaAux3 Mod 360
            Else
                �UAnioMas = 0
                �UMesDia = �UDiaAux3
            End If
            �UDiaAux3 = �UMesDia

            �UAnioAux2 = �UAnioAux2 + �UAnioMas

            If �UDiaAux3 >= 30 Then
                �UVarTrabajo08 = �UDiaAux3 / 30
                �UVarTrabajo09 = �UDiaAux3 Mod 30
            Else
                �UVarTrabajo08 = 0
                �UVarTrabajo09 = �UDiaAux3
            End If

            �UMesAux2 = �UMesAux2 + �UVarTrabajo08

            �UDiaAux3 = �UVarTrabajo09

            If �UDiaAux3 = 0 Then
                �UMesAux2 = �UMesAux2 - 1
                �UDiaAux3 = �UDia31
            End If

            If �UMesAux2 > 12 Then
                �UMesAux2 = �UMesAux2 - 12
                �UAnioAux2 = �UAnioAux2 + 1
            End If

            If �UMesAux2 = 2 Then
                If �UDiaAux3 >= 28 Then
                    �UAnioAux = �UAnioAux2 / 4
                    �UAnioBis = �UAnioAux2 Mod 4
                    If �UAnioBis = 0 Then
                        �UDiaAux3 = 29
                    Else
                        �UDiaAux3 = 28
                    End If
                End If
            Else
                If �UDiaAux3 >= 30 Then
                    If �UMesAux2 = 4 Or �UMesAux2 = 6 Or �UMesAux2 = 9 Or �UMesAux2 = 11 Then
                        �UDiaAux3 = 30
                    End If
                End If
            End If

            �UDiaAux2 = �UDiaAux3

            If Len(Trim(�UMesAux2)) = 1 Then
                tmpMes = "0" & �UMesAux2
            Else
                tmpMes = �UMesAux2
            End If

            If Len(Trim(�UDiaAux2)) = 1 Then
                tmpDia = "0" & �UDiaAux2
            Else
                tmpDia = �UDiaAux2
            End If

            �UTrabajo02R = CStr(tmpDia & "/" & tmpMes & "/" & �UAnioAux2)

            FinVigenciaRecibo = CStr(�UTrabajo02R)


            Exit Function

        Catch ex As Exception
            Throw ex
            Exit Function
        End Try
    End Function

    Private Sub CalculoVigenciaDias()

        �UTrabajo02R = InicioVigencia
        '  �UAnioAux2 = CInt(Mid(�UTrabajo02R, 1, 4))
        '  �UMesAux2 = CInt(Mid(�UTrabajo02R, 6, 2))
        '  �UDiaAux2 = CInt(Mid(�UTrabajo02R, 9, 2))

        �UDiaAux2 = CInt(Mid(�UTrabajo02R, 1, 2))
        �UMesAux2 = CInt(Mid(�UTrabajo02R, 4, 2))
        �UAnioAux2 = CInt(Mid(�UTrabajo02R, 7, 4))
        Call FechaComercial()

        �UVarTrabajo03 = �UAnioDias + �UMesDias + �UDiaAux2

        �UTrabajo02R = FinVigencia
        '  �UAnioAux2 = CInt(Mid(�UTrabajo02R, 1, 4))
        '  �UMesAux2 = CInt(Mid(�UTrabajo02R, 6, 2))
        '  �UDiaAux2 = CInt(Mid(�UTrabajo02R, 9, 2))

        �UDiaAux2 = CInt(Mid(�UTrabajo02R, 1, 2))
        �UMesAux2 = CInt(Mid(�UTrabajo02R, 4, 2))
        �UAnioAux2 = CInt(Mid(�UTrabajo02R, 7, 4))

        �UDia31 = �UDiaAux2
        Call FechaComercial()

        �UVarTrabajo04 = �UAnioDias + �UMesDias + �UDiaAux2

        �UVarTrabajo05 = �UVarTrabajo04 - �UVarTrabajo03

        If �UVarTrabajo05 < 0 Then
            �UVarTrabajo05 = 1
        End If

    End Sub

    Private Sub FechaComercial()

        �UAnioDias = �UAnioAux2 * 360
        �UMesDias = �UMesAux2 * 30

        If �UMesAux2 = 2 Then
            If �UMesAux2 >= 28 Then
                �UDiaAux2 = 30
            End If
        Else
            If �UDiaAux2 > 30 Then
                �UDiaAux2 = 30
            End If
        End If
        '�UDIAAIX2

    End Sub

    Private Sub CalculaImportePrimerRecibo()
        Try

            �UPmaTotGen = PrimaTotal
            �UPmaNetGen = PrimaNeta
            �UDerPolGen = Derechos
            �URecPagGen = Recargos
            �UImpGen = Iva
            �UCesComGen = 0

            �UVigForPag = pCompletos * DiasPago

            If �UVarTrabajo05 = �UVigForPag Then
                �URegular = 1
            Else
                �URegular = 2
            End If

            If �URegular = 2 Then
                'PrimaDiaria = Mid(PrimaDiaria, 1, 4)
                PrimaDiaria = Trunca(PrimaDiaria, 2)
                �UVarAux = PrimaDiaria * �UVarTrabajo06
                �UVarTrabajo07 = �UVarAux / �UPmaTotGen

                '�UPriNet = �UPmaNetGen * �UVarTrabajo07
                �UPriNet = Trunca(�UPmaNetGen * �UVarTrabajo07, 2)
                �UPriNetSub = �UPriNet
                �UPrima = �UPriNet
                '�UDerPol = �UDerPolGen * �UVarTrabajo07
                �UDerPol = Trunca(�UDerPolGen * �UVarTrabajo07, 2)
                �UDerPolSub = �UDerPol
                �UPoliza = �UDerPol
                '�URecPag = �URecPagGen * �UVarTrabajo07
                �URecPag = Trunca(�URecPagGen * �UVarTrabajo07, 2)
                �URecPagSub = �URecPag
                �URecargos = �URecPag
                '�UCesCom = �UCesComGen * �UVarTrabajo07
                �UCesCom = Trunca(�UCesComGen * �UVarTrabajo07, 2)
                �UCesComSub = �UCesCom
                �UCesComs = �UCesCom
            Else
                '�UPriNet = �UPmaNetGen / pCompletos
                �UPriNet = Trunca(�UPmaNetGen / pCompletos, 2)
                �UPriNetSub = �UPriNet
                �UPrima = �UPriNet
                '�UDerPol = �UDerPolGen / pCompletos
                �UDerPol = Trunca(�UDerPolGen / pCompletos, 2)
                �UDerPolSub = �UDerPol
                �UPoliza = �UDerPol
                '�URecPag = �URecPagGen / pCompletos
                �URecPag = Trunca(�URecPagGen / pCompletos, 2)
                �URecPagSub = �URecPag
                �URecargos = �URecPag
                '�UCesCom = �UCesComGen / pCompletos
                �UCesCom = Trunca(�UCesComGen / pCompletos, 2)
                �UCesComSub = �UCesCom
                �UCesComs = �UCesCom
            End If

            �UImpuesto = Math.Round((�UPrima + �UCesComs + �UPoliza + �URecargos) * �UPorCIva, 2)
            �UIvaSub = �UImpuesto
            �UImpuesto1 = �UImpuesto

            �UPrimaTotal1 = �UPrima + �UCesComs + �UPoliza + �URecargos + �UImpuesto1
            �UPmaTotSub = �UPrimaTotal1

            '�UPriNetIni = �UPmaNetGen - (Math.Round(�UPrima, 2) * (pCompletos - 1))
            �UPriNetIni = �UPmaNetGen - (Trunca(�UPrima, 2) * (pCompletos - 1))
            PNPrimerRecibo = �UPriNetIni

            �UDerPolIni = �UDerPolGen - (�UPoliza * (pCompletos - 1))
            DPPrimerRecibo = �UDerPolIni

            �URecPagIni = �URecPagGen - (�URecargos * (pCompletos - 1))
            PFPrimerRecibo = �URecPagIni

            �UCesComIni = �UCesComGen - (�UCesComs * (pCompletos - 1))
            CCPrimerRecibo = �UCesComIni

            '�UImpIni = (�UPriNetIni + �UCesComIni + �UDerPolIni + �URecPagIni) * �UPorCIva
            �UImpIni = Trunca((�UPriNetIni + �UCesComIni + �UDerPolIni + �URecPagIni) * �UPorCIva, 2)
            IMPPrimerRecibo = �UImpIni

            '�UPriTotIni = �UPriNetIni + �UCesComIni + �UDerPolIni + �URecPagIni + �UImpIni
            �UPriTotIni = Trunca(�UPriNetIni + �UCesComIni + �UDerPolIni + �URecPagIni + �UImpIni, 2)
            PTPrimerRecibo = �UPriTotIni

            �UFechaIniSigRec = FinVigenciaRecibo
            �UFechaCobSigRec = FinVigenciaRecibo
            �UFechaCbrPmeRec = InicioVigencia
            �UPerCobRecSub = DiasPago
            �UNumRecGen = 1
            '�UNumRecGen = pCompletos - 1

            arrFracciones(0, 0) = 1
            arrFracciones(0, 1) = pCompletos
            arrFracciones(0, 2) = Format(PNPrimerRecibo, "####0.00")
            arrFracciones(0, 3) = Format(PFPrimerRecibo, "####0.00")
            arrFracciones(0, 4) = Format(DPPrimerRecibo, "####0.00")
            arrFracciones(0, 5) = Format(IMPPrimerRecibo, "####0.00")
            arrFracciones(0, 6) = Format(PTPrimerRecibo, "####0.00")
            arrFracciones(0, 7) = InicioVigencia
            arrFracciones(0, 8) = FinVigenciaRecibo

            Exit Sub

        Catch ex As Exception
            Throw ex
            Exit Sub
        End Try
    End Sub

    Private Sub GeneraRecibos()
        Try

            InicioVigenciaRecibo = �UFechaIniSigRec

            NoRecibo = �UNumRecGen

            �UPmaTotGen = �UPmaTotSub
            �UPmaNetGen = �UPriNetSub
            �UDerPolGen = �UDerPolSub
            �URecPagGen = �URecPagSub
            �UIvaGen = �UIvaSub
            �UCesComGen = �UCesComSub

            �UFecIniAlf = InicioVigenciaRecibo
            �UPerTra = �UPerCobRecSub
            �UPolFecVigFin = FinVigencia
            �UDDFecFin = Mid(�UPolFecVigFin, 1, 2)
            �UDia31 = �UDDFecFin

            CalculaFecha()

            If Len(Trim(�UMesSB)) = 1 Then
                tmpMes = "0" & �UMesSB
            Else
                tmpMes = �UMesSB
            End If

            If Len(Trim(�UDiasB)) = 1 Then
                tmpDia = "0" & �UDiasB
            Else
                tmpDia = �UDiasB
            End If

            �UFecIni = CStr(tmpDia & "/" & tmpMes & "/" & �UAniSB)

            �UFecIniA = �UFecIni
            �UFecIniB = �UFecIni

            FinVigenciaRecibo = �UFecIniB

            'Se guardan la informaci�n de cada recibo en el arreglo
            arrFracciones(NoRecibo - 1, 0) = NoRecibo
            arrFracciones(NoRecibo - 1, 1) = pCompletos
            arrFracciones(NoRecibo - 1, 2) = Format(�UPriNetSub, "####0.00")
            arrFracciones(NoRecibo - 1, 3) = Format(�URecPagSub, "####0.00")
            arrFracciones(NoRecibo - 1, 4) = Format(�UDerPolSub, "####0.00")
            arrFracciones(NoRecibo - 1, 5) = Format(�UIvaSub, "####0.00")
            arrFracciones(NoRecibo - 1, 6) = Format(�UPmaTotSub, "####0.00")
            arrFracciones(NoRecibo - 1, 7) = InicioVigenciaRecibo
            arrFracciones(NoRecibo - 1, 8) = FinVigenciaRecibo

            �UFechaIniSigRec = FinVigenciaRecibo

            Exit Sub

        Catch ex As Exception
            Throw ex
            Exit Sub
        End Try
    End Sub

    Private Sub CalculaFecha()
        Try

            '  �UAniIni = Mid(InicioVigenciaRecibo, 1, 4)
            '  �UMesIni = Mid(InicioVigenciaRecibo, 6, 2)
            '  �UDiaIni = Mid(InicioVigenciaRecibo, 9, 2)

            �UDiaIni = Mid(InicioVigenciaRecibo, 1, 2)
            �UMesIni = Mid(InicioVigenciaRecibo, 4, 2)
            �UAniIni = Mid(InicioVigenciaRecibo, 7, 4)

            �UDiaAux = �UDiaIni

            If �UMesIni = 2 Then
                If �UDiaIni >= 28 Then
                    �UDiaIni = 30
                End If
            Else
                If �UDiaIni > 30 Then
                    �UDiaIni = 30
                End If
            End If

            �UDiaTra = �UPerTra + �UDiaIni
            If �UDiaTra > 360 Then
                �UAniMas = �UDiaTra / 360
                �UMesDia = �UDiaTra Mod 360
            Else
                �UAniMas = 0
                �UMesDia = �UDiaTra
            End If
            �UDiaTra = �UMesDia

            �UAniIni = �UAniIni + �UAniMas

            If �UDiaTra >= 30 Then
                �UMesTra = �UDiaTra / 30
                �UDisADI = �UDiaTra Mod 30
            Else
                �UMesTra = 0
                �UDisADI = �UDiaTra
            End If
            �UDiaIni = �UDisADI

            �UMesIni = �UMesIni + �UMesTra

            If �UDiaIni = 0 Then
                �UDiaIni = �UDia31
                �UMesIni = �UMesIni - 1
            End If

            If �UMesIni > 12 Then
                �UMesIni = �UMesIni - 12
                �UAniIni = �UAniIni + 1
            End If
            If �UMesIni = 2 Then
                �UAniB = �UAniIni / 4
                �UAniBis = �UAniIni Mod 4
                If �UDiaIni >= 28 Then
                    If �UAniBis = 0 Then
                        �UDiaIni = 29
                    Else
                        �UDiaIni = 28
                    End If
                End If
            Else
                If �UDiaIni > 30 Then
                    If �UMesIni = 4 Or �UMesIni = 6 Or �UMesIni = 9 Or �UMesIni = 11 Then
                        �UDiaIni = 30
                    End If
                End If
            End If

            �UDiasB = �UDiaIni
            �UMesSB = �UMesIni
            �UAniSB = �UAniIni

            Exit Sub

        Catch ex As Exception
            Throw ex
            Exit Sub
        End Try
    End Sub

    'Devuelve el numero pasado por valor con el numero de decimales indicados en nDec
    Private Function Trunca(ByVal Num As Double, Optional ByVal nDec As Integer = 0) As Double
        Dim numTemp As String
        Dim Base10 As String
        Dim Pos As Integer

        'Convertimos en cadena el valor para hacer la busqueda del .

        numTemp = Trim(Str(Num))
        If InStr(1, numTemp, "E") > 0 Then
            Pos = InStr(1, numTemp, "E")
            Base10 = Mid(numTemp, Pos, Len(numTemp))
            numTemp = Mid(numTemp, 1, Pos - 1)
        End If
        Pos = InStr(1, numTemp, ".")
        'Consideramos que ndec no fue indicado y ponemos por default 2 decimales
        If nDec = 0 Then nDec = 2

        If Pos > 0 Then
            If Base10 <> "" Then nDec = nDec - CInt(Mid(Base10, InStr(1, Base10, "E") + 2, Len(Base10)))
            Trunca = Mid(numTemp, 1, Pos + nDec) & Base10
        Else
            Trunca = Num & Base10
        End If

    End Function


    Private Function crea_estructura_datatable(ByVal banvalor As Integer, ByVal strcadena As String)
        Dim columna As New DataColumn
        Select Case banvalor
            Case 1
                columna.DataType = System.Type.GetType("System.String")
            Case 2
                columna.DataType = System.Type.GetType("System.Int64")
            Case 3
                columna.DataType = System.Type.GetType("System.Double")
            Case 4
                columna.DataType = System.Type.GetType("System.Array")
            Case 5
                columna.DataType = System.Type.GetType("System.DateTime")
        End Select
        columna.AllowDBNull = False
        columna.Caption = strcadena
        columna.ColumnName = strcadena
        DtResultado.Columns.Add(columna)
    End Function


    Private Sub crear_ds_Salida()
        crea_estructura_datatable(2, "Recibo")
        crea_estructura_datatable(3, "Prima_Neta")
        crea_estructura_datatable(3, "Recargo")
        crea_estructura_datatable(3, "Derecho_Poliza")
        crea_estructura_datatable(3, "iva")
        crea_estructura_datatable(3, "prima_total")
        crea_estructura_datatable(5, "Inicio_Vigencia")
        crea_estructura_datatable(5, "Fin_Vigencia")
        crea_estructura_datatable(2, "Periodo_vigencia")
        crea_estructura_datatable(2, "Total_Recibos")
    End Sub

    Private Function carga_datos_Salida() As DataTable
        Dim dr As DataRow
        Dim i As Integer = 0
        If UBound(arrFracciones) >= 0 Then
            For i = 0 To UBound(arrFracciones) - 1
                If Not arrFracciones(i, 0) Is Nothing Then
                    dr = DtResultado.NewRow
                    dr = DtResultado.NewRow
                    dr("Recibo") = arrFracciones(i, 0)
                    dr("Prima_Neta") = arrFracciones(i, 2)
                    dr("Recargo") = arrFracciones(i, 3)
                    dr("Derecho_Poliza") = arrFracciones(i, 4)
                    dr("iva") = arrFracciones(i, 5)
                    dr("prima_total") = arrFracciones(i, 6)
                    dr("Inicio_Vigencia") = CDate(arrFracciones(i, 7))
                    dr("Fin_Vigencia") = CDate(arrFracciones(i, 8))
                    dr("Periodo_vigencia") = DateDiff(DateInterval.Month, CDate(arrFracciones(i, 7)), CDate(arrFracciones(i, 8)))
                    dr("Total_Recibos") = arrFracciones(i, 1)
                    DtResultado.Rows.Add(dr)
                End If
            Next
        End If
        Return DtResultado
    End Function

End Class
