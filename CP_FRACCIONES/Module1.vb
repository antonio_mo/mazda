Imports CP_FRACCIONES

Module Module1
    Dim Fraccion As New clsEmision
    Dim i As Integer = 0

    Public Sub main()
        'GNP()
        'ZURICH()
        'ZURICH_daimler()
        'QUALITAS_Daimler()
        'QUALITAS_Conatuo()
        'ZURICH_muti()
        'HDI()

    End Sub

    Public Sub GNP()
        Try
            Dim ds As DataSet = Fraccion.FraccionesGnp("26/10/2011", "30/06/2012", 1481.07, 0, 225.0, 272.97, 1979.04, 2, 0.16)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ZURICH_muti()
        Try
            Dim ds As DataSet = Fraccion.FraccionesZurich_Multi("01/01/2013", "01/03/2016", 29569, 0, 652, 4835, 35057, 1, 0.16, 0, 38)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ZURICH()
        Try
            Dim ds As DataSet = Fraccion.FraccionesZurich("18/11/2010", "18/11/2013", 57927.31, 3475.64, 400, 9888.47, 71691.41, 1, 0.16, 6)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ZURICH_daimler()
        Try
            Dim ds As DataSet = Fraccion.FraccionesZurich_Daimler("07/11/2011", "07/02/2015", 41599.93, 2616.64, 350, 7130.65, 51697.22, 1, 0.16, 6.29)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub QUALITAS_Conatuo()
        Try
            Dim ds As DataSet = Fraccion.FraccionesQualitas_Conauto("12/03/2014", "12/03/2017", 10426.68, 0, 420, 1846.91, 13390.09, 1, 0.16, 0)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub QUALITAS_Daimler()
        Try
            'Dim ds As DataSet = Fraccion.FraccionesQualitas_Daimler("07/11/2011", "07/02/2015", 48586.38, 7433.72, 400, 9027.22, 65447.31, 1, 0.16, 15.3)
            Dim ds As DataSet = Fraccion.FraccionesQualitas_Daimler("16/01/2012", "16/04/2015", 14431.64, 2208.0409, 300, 2662.35, 19650.03, 1, 0.16, 15.3)
            'Dim ds As DataSet = Fraccion.FraccionesQualitas_Daimler("07/11/2011", "07/02/2015", 48586.38, 7433.72, 400, 9027.22, 65447.31, 1, 0.16, 15.3)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub HDI()
        Try
            Dim ds As DataSet = Fraccion.FraccionesHDI("01/01/2016", "01/10/2020", 47421.35, 3177.23, 460, 8169.37, 59227.95, 3, 0.16, 6.7)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If Not ds.Tables(0).Rows(i)(0) Is Nothing Then
                        Console.WriteLine("Recibo :" & ds.Tables(0).Rows(i)("Recibo"))
                        Console.WriteLine("Prima_Neta :" & ds.Tables(0).Rows(i)("Prima_Neta"))
                        Console.WriteLine("Recargo :" & ds.Tables(0).Rows(i)("Recargo"))
                        Console.WriteLine("Derecho_Poliza :" & ds.Tables(0).Rows(i)("Derecho_Poliza"))
                        Console.WriteLine("iva :" & ds.Tables(0).Rows(i)("iva"))
                        Console.WriteLine("prima_total :" & ds.Tables(0).Rows(i)("prima_total"))
                        Console.WriteLine("Inicio_Vigencia :" & ds.Tables(0).Rows(i)("Inicio_Vigencia"))
                        Console.WriteLine("Fin_Vigencia :" & ds.Tables(0).Rows(i)("Fin_Vigencia"))
                        Console.WriteLine("Periodo_vigencia :" & ds.Tables(0).Rows(i)("Periodo_vigencia"))
                        Console.WriteLine("Total_Recibos :" & ds.Tables(0).Rows(i)("Total_Recibos"))
                        Console.WriteLine(" ")
                    End If
                Next
            End If
            Console.ReadLine()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Module
